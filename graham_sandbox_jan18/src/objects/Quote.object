<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CreatePdf</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>EmailQuote</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>NewQuote</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SyncQuote</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>AccountId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AdditionalName</fullName>
    </fields>
    <fields>
        <fullName>BillingName</fullName>
    </fields>
    <fields>
        <fullName>Comments_to_Show_on_Quote__c</fullName>
        <description>Comments you would like added on the customer quotation.</description>
        <externalId>false</externalId>
        <label>Comments to Show on Quote</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ContactId</fullName>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <value>001F000000eLI7p</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ContractId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Customer_Contact_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Customer Contact Name</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Quotes</relationshipLabel>
        <relationshipName>Quotes1</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Customer_Email__c</fullName>
        <externalId>false</externalId>
        <formula>Customer_Contact_Name__r.Email</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer Email</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_Phone_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Customer_Contact_Name__r.Phone</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer Phone Number</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Delivery_Time__c</fullName>
        <externalId>false</externalId>
        <label>Delivery Time</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>2 to 4 weeks</fullName>
                    <default>false</default>
                    <label>2 to 4 weeks</label>
                </value>
                <value>
                    <fullName>4 to 6 weeks</fullName>
                    <default>false</default>
                    <label>4 to 6 weeks</label>
                </value>
                <value>
                    <fullName>6 to 8 weeks</fullName>
                    <default>true</default>
                    <label>6 to 8 weeks</label>
                </value>
                <value>
                    <fullName>greater than 2 months</fullName>
                    <default>false</default>
                    <label>greater than 2 months</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Department__c</fullName>
        <externalId>false</externalId>
        <formula>Customer_Contact_Name__r.DepartmentLink__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Department</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description</fullName>
    </fields>
    <fields>
        <fullName>Discount</fullName>
    </fields>
    <fields>
        <fullName>Discount_Display_Options__c</fullName>
        <externalId>false</externalId>
        <label>Discount Display Options</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Show discount for EACH list price item</fullName>
                    <default>false</default>
                    <label>Show discount for EACH list price item</label>
                </value>
                <value>
                    <fullName>Show each list price item &amp; a total discount amount</fullName>
                    <default>false</default>
                    <label>Show each list price item &amp; a total discount amount</label>
                </value>
                <value>
                    <fullName>Show total list price &amp; total discount amount</fullName>
                    <default>false</default>
                    <label>Show total list price &amp; total discount amount</label>
                </value>
                <value>
                    <fullName>Show total price only</fullName>
                    <default>false</default>
                    <label>Show total price only</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Email</fullName>
    </fields>
    <fields>
        <fullName>ExpirationDate</fullName>
    </fields>
    <fields>
        <fullName>Fax</fullName>
    </fields>
    <fields>
        <fullName>GrandTotal</fullName>
    </fields>
    <fields>
        <fullName>Incoterms__c</fullName>
        <externalId>false</externalId>
        <label>Incoterms</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>CIP</fullName>
                    <default>true</default>
                    <label>CIP</label>
                </value>
                <value>
                    <fullName>DDP</fullName>
                    <default>false</default>
                    <label>DDP</label>
                </value>
                <value>
                    <fullName>EXW</fullName>
                    <default>false</default>
                    <label>EXW</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>IsSyncing</fullName>
    </fields>
    <fields>
        <fullName>LineItemCount</fullName>
    </fields>
    <fields>
        <fullName>Name</fullName>
    </fields>
    <fields>
        <fullName>OpportunityId</fullName>
        <type>MasterDetail</type>
    </fields>
    <fields>
        <fullName>Other_Incoterms__c</fullName>
        <externalId>false</externalId>
        <label>Other Incoterms</label>
        <length>255</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Instructions__c</fullName>
        <description>Other instructions you want to provide to the Customer Service associate</description>
        <externalId>false</externalId>
        <label>Other Instructions</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Other_Language__c</fullName>
        <externalId>false</externalId>
        <label>Other Language</label>
        <length>45</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Other_Payment_Terms__c</fullName>
        <externalId>false</externalId>
        <label>Other Payment Terms</label>
        <length>100</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerId</fullName>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Payment_Conditions__c</fullName>
        <externalId>false</externalId>
        <label>Payment Conditions</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>After Delivery</fullName>
                    <default>false</default>
                    <label>After Delivery</label>
                </value>
                <value>
                    <fullName>After AB Sciex Installation</fullName>
                    <default>false</default>
                    <label>After AB Sciex Installation</label>
                </value>
                <value>
                    <fullName>After Customer Acceptance</fullName>
                    <default>false</default>
                    <label>After Customer Acceptance</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Payment_Terms__c</fullName>
        <externalId>false</externalId>
        <label>Payment Terms</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>30 Days</fullName>
                    <default>false</default>
                    <label>30 Days</label>
                </value>
                <value>
                    <fullName>45 Days</fullName>
                    <default>false</default>
                    <label>45 Days</label>
                </value>
                <value>
                    <fullName>60 Days</fullName>
                    <default>false</default>
                    <label>60 Days</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Phone</fullName>
    </fields>
    <fields>
        <fullName>QuoteToName</fullName>
    </fields>
    <fields>
        <fullName>Quote_Language__c</fullName>
        <externalId>false</externalId>
        <label>Quote Language</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>true</sorted>
                <value>
                    <fullName>English</fullName>
                    <default>true</default>
                    <label>English</label>
                </value>
                <value>
                    <fullName>French</fullName>
                    <default>false</default>
                    <label>French</label>
                </value>
                <value>
                    <fullName>German</fullName>
                    <default>false</default>
                    <label>German</label>
                </value>
                <value>
                    <fullName>Italian</fullName>
                    <default>false</default>
                    <label>Italian</label>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                    <label>Other</label>
                </value>
                <value>
                    <fullName>Spanish</fullName>
                    <default>false</default>
                    <label>Spanish</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Salutation__c</fullName>
        <externalId>false</externalId>
        <label>Salutation</label>
        <length>30</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShippingHandling</fullName>
    </fields>
    <fields>
        <fullName>ShippingName</fullName>
    </fields>
    <fields>
        <fullName>Status</fullName>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Subtotal</fullName>
    </fields>
    <fields>
        <fullName>Tax</fullName>
    </fields>
    <fields>
        <fullName>TotalPrice</fullName>
    </fields>
    <fields>
        <fullName>VAT_Exempt__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check the box if this order is VAT exempt</description>
        <externalId>false</externalId>
        <label>VAT Exempt</label>
        <type>Checkbox</type>
    </fields>
    <listViews>
        <fullName>mine</fullName>
        <columns>QUOTE.NAME</columns>
        <columns>OPPORTUNITY.NAME</columns>
        <columns>QUOTE.ISSYNCING</columns>
        <columns>QUOTE.EXPIRATIONDATE</columns>
        <columns>QUOTE.SUBTOTAL</columns>
        <columns>QUOTE.TOTALPRICE</columns>
        <filterScope>Mine</filterScope>
        <label>My Quotes</label>
    </listViews>
    <searchLayouts>
        <customTabListAdditionalFields>QUOTE.NAME</customTabListAdditionalFields>
        <customTabListAdditionalFields>OPPORTUNITY.NAME</customTabListAdditionalFields>
        <customTabListAdditionalFields>QUOTE.ISSYNCING</customTabListAdditionalFields>
        <customTabListAdditionalFields>QUOTE.EXPIRATIONDATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>QUOTE.SUBTOTAL</customTabListAdditionalFields>
        <customTabListAdditionalFields>QUOTE.TOTALPRICE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>QUOTE.NAME</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>OPPORTUNITY.NAME</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>QUOTE.ISSYNCING</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>QUOTE.EXPIRATIONDATE</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>QUOTE.SUBTOTAL</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>QUOTE.TOTALPRICE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QUOTE.NAME</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>OPPORTUNITY.NAME</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QUOTE.ISSYNCING</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QUOTE.EXPIRATIONDATE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QUOTE.SUBTOTAL</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QUOTE.TOTALPRICE</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>QUOTE.PHONE</lookupPhoneDialogsAdditionalFields>
        <searchResultsAdditionalFields>QUOTE.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>OPPORTUNITY.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>QUOTE.ISSYNCING</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>QUOTE.EXPIRATIONDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>QUOTE.SUBTOTAL</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>QUOTE.TOTALPRICE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
