<apex:page controller="printPackage" showHeader="false" sidebar="false">
	<apex:stylesheet value="{!URLFOR($Resource.CIPStyleSheet,'printStyleSheet.css')}"/>
	<!--[if IE 8]>
	<style type="text/css">
		/* ie8 hack */
            /* bof charts */ 
			.jqplot-table-legend {
				font-family: Arial, Helvetica, sans-serif;
				font-size:10px;
			}
		
			.jqplot-highlighter-tooltip {
				font-family: Arial, Helvetica, sans-serif;
				font-size:12px;
				background: rgba(255,238,190,1);
			}
			
			.jqplot-axis.jqplot-xaxis
			{
			    min-height: 75px;
			}
			
			.jqplot-xaxis-tick
			{
			    font-size: 9pt !important;
			     width:             120px;
			     height:            20px;
			
			     -moz-transform:    rotate(-45deg);
			     -o-transform:      rotate(-45deg);
			     -webkit-transform: rotate(-45deg);
			     transform:         rotate(-45deg);
			     
			
			   /* IE8+ - must be on one line, unfortunately */ 
			   -ms-filter: "progid:DXImageTransform.Microsoft.Matrix(M11=0.7071067811865483, M12=0.7071067811865467, M21=-0.7071067811865467, M22=0.7071067811865483, SizingMethod='auto expand')";
			   
			   /* IE6 and 7 */ 
			   filter: progid:DXImageTransform.Microsoft.Matrix(
			            M11=0.7071067811865483,
			            M12=0.7071067811865467,
			            M21=-0.7071067811865467,
			            M22=0.7071067811865483,
			            SizingMethod='auto expand');
			
			
			   /*
			    * To make the transform-origin be the middle of
			    * the object.    Note: These numbers
			    * are approximations.  For more accurate results,
			    * use Internet Explorer with this tool.
			    */
			}
			
			.jqplot-xaxis-tick
			{
			   margin-left: 8px; 
			   margin-top: -42px;
			}
			/* eof charts */
	</style>
	<![endif]-->
	<style type="text/css" media="print">
		body{font-size:13px; background:#fff;}
		.hide-on-print{display:none;}
		.page{position: relative; width:auto; height:auto; padding:0; left:0; right:0; top:0; bottom:0;}			
	</style>
	<apex:includeScript value="{!$Resource.Jquery_DefaultJs}"/>
	<apex:stylesheet value="{!URLFOR($Resource.jqPlot,'jquery.jqplot.min.css')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'excanvas.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'jquery.jqplot.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'plugins/jqplot.barRenderer.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'plugins/jqplot.canvasTextRenderer.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'plugins/jqplot.canvasAxisTickRenderer.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'plugins/jqplot.canvasAxisLabelRenderer.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'plugins/jqplot.categoryAxisRenderer.min.js')}"/>
	<apex:includeScript value="{!URLFOR($Resource.jqPlot, 'plugins/jqplot.pointLabels.min.js')}"/>
	
    <script type="text/javascript">
    	var j$ = jQuery.noConflict();
		
        var consolidatePlotCanvas = function(objId) {
			var newCanvas = document.createElement("canvas");				
			if (!newCanvas.getContext && window.G_vmlCanvasManager) window.G_vmlCanvasManager.initElement(newCanvas);
			
			newCanvas.width = j$("#" + objId).width();
			newCanvas.height = j$("#" + objId).height() + 20;
			var baseOffset = j$("#" + objId).offset();

			j$("#" + objId).children().each(
			function() {
				// for the div's with the X and Y axis
				if (j$(this)[0].tagName.toLowerCase() == 'div') {
					// X axis is built with canvas
					j$(this).children("canvas").each(
					function() {
						var offset = j$(this).offset();
						newCanvas.getContext("2d").drawImage(this,
						offset.left - baseOffset.left,
						offset.top - baseOffset.top);
					});
					// Y axis got div inside, so we get the text and draw it on
					// the canvas
					j$(this).children("div").each(
					function() {
						var offset = j$(this).offset();
						var context = newCanvas.getContext("2d");
						context.font = j$(this).css('font-style') + " "
						+ j$(this).css('font-size') + " "
						+ j$(this).css('font-family');
						if (j$(this).attr('class') == 'jqplot-xaxis-tick')
						{
							context.save();
							context.translate(offset.left - baseOffset.left + 30, offset.top - baseOffset.top + 20);
							context.rotate(-0.611); //-35 degrees
							context.textAlign = "center";
							context.fillText(j$(this).html(), 0, 0);
							context.restore();
						}
						else
						{
							var marginTop = (j$(this).attr('class') == 'jqplot-xaxis-label')? 30 : 10;
							context.fillText(j$(this).html(), offset.left
							- baseOffset.left, offset.top
							- baseOffset.top + marginTop);
						}
					});
				}
				// all other canvas from the chart
				else if (j$(this)[0].tagName.toLowerCase() == 'canvas') {
					var offset = j$(this).offset();
					newCanvas.getContext("2d").drawImage(this,
					offset.left - baseOffset.left,
					offset.top - baseOffset.top);
				}
			});

			// add the point labels
			j$("#" + objId).children(".jqplot-point-label").each(
			function() {
				var offset = j$(this).offset();
				var context = newCanvas.getContext("2d");
				context.font = j$(this).css('font-style') + " "
				+ j$(this).css('font-size') + " "
				+ j$(this).css('font-family');
				context.fillText(j$(this).html(), offset.left - baseOffset.left,
				offset.top - baseOffset.top + 10);
			});

			// add the rectangles
			j$("#" + objId + " *").children(".jqplot-table-legend-swatch").each(
			function() {
				var offset = j$(this).offset();
				var context = newCanvas.getContext("2d");
				context.fillStyle = (j$(this).css('background-color'));
				context.fillRect(offset.left - baseOffset.left, offset.top
				- baseOffset.top, 15, 15);
			});

			// add the legend
			j$("#" + objId + " *").children(".jqplot-table-legend td:last-child").each(
			function() {
				var offset = j$(this).offset();
				var context = newCanvas.getContext("2d");
				context.font = j$(this).css('font-style') + " "
				+ j$(this).css('font-size') + " "
				+ j$(this).css('font-family');
				context.fillText(j$(this).html(), offset.left - baseOffset.left,
				offset.top - baseOffset.top + 15);
			});
			return newCanvas;
		}
		
		var replaceChartWithImg = function(objId)
		{
			var consolidatedCanvas = consolidatePlotCanvas(objId);
			
			if (consolidatedCanvas.toDataURL)
			{
			    j$('#' + objId).empty();
				var img = document.createElement('img');
				img.setAttribute('src', consolidatedCanvas.toDataURL());
				j$('#' + objId).append(img);
				j$('#' + objId).height('auto');
				return true;
			}
			return false;
		}
    
        function printThisPage()
        {
            window.print();
        }
        function closeThisPage()
        {
        	window.close();
        }
    </script>
	
	<!-- Begin page Specifications -->
	
	<div id="printBtns" class="hide-on-print">
	    <div>
	        <input type="button" value="close" onclick="closeThisPage();"/>
	    </div>	
	    <div>
	        <input type="button" value="print" onclick="printThisPage();"/>
	    </div>
	</div>
	
	<div class="page">
		<div class="pageHeader">
			<div class="pageHeaderWrapper clearfix">
		    	<div class="imgContainer">
	            	<apex:image value="{!URLFOR($Resource.ABsciexlogo)}" height="49px" width="180px"/>
	            </div>
	            <div class="pageTitle">Specifications</div>
	            <div class="pageTitle subtitle">{!absciexProductName} Vs {!relatedProductName}</div>
            </div>
		</div>

		<div class="centeredContent">
	        <table class="productImagesTable">
	             <tr>
	                 <th><apex:image width="125px" height="90px"  value="{!product1image}"/></th>
	                 <th><apex:image width="125px" height="90px" value="{!product2image}"/></th>
	             </tr>
	             <tr>
	                 <th><apex:outputText value="{!absciexProductName}"/></th>
	                 <th><apex:outputText value="{!relatedProductName}"/></th>
	             </tr>
	        </table>
        </div>

		<h1>Specifications</h1>

        <table class="table">
            <tr>
                <th style="width:33%">Product Features</th>
                <th style="width:33%"><apex:outputText value="{!absciexProductName}"/></th>
                <th style="width:33%"><apex:outputText value="{!relatedProductName}"/></th>
            </tr>
            <apex:repeat value="{!prodList}" var="pro">
            <tr>
                <td><apex:outputText value="{!pro.ProductMetaName }"/></td>
                <td><apex:outputText value="{!pro.Product1Value }"/></td>
                <td><apex:outputText value="{!pro.Product2Value }"/></td>
            </tr>
            </apex:repeat>
       </table>
       
       <div class="pageFooter">&copy; 2013 AB Sciex Proprietary &amp; Confidential for Internal Use Only</div>

	</div>
	
	<!-- End page Specifications -->

    <div style="page-break-after:always;"></div>
	
	<!-- Begin page Win/Loss Report -->
	
	<div class="page">
	
		<div class="pageHeader">
			<div class="pageHeaderWrapper clearfix">
		    	<div class="imgContainer">
	            	<apex:image value="{!URLFOR($Resource.ABsciexlogo)}" height="49px" width="180px"/>
	            </div>
	            <div class="pageTitle">Win/Loss Report</div>
	            <div class="pageTitle subtitle">{!absciexProductName} Vs {!relatedProductName}</div>
            </div>
		</div>
		
		<div class="centeredContent">
			<table class="chartTable">
				<tr>
					<td width="50%">
						<div class="cipTitle">Opportunities Win Rate</div>
						<div id="oppWinRate" style="width: 100%; height: 300px;"></div>
					</td>
	
					<td width="50%">
						<div class="cipTitle">Lost Order Total</div>
						<div id="lostOrderTotal" style="width: 100%; height: 300px;"></div>
					</td>
				</tr>
			</table>
		</div>

          <script type="text/javascript">           
          //Opportunities Win Rate
          var chart1, chart2;

          (function(){
		    var rawData = '{!WinLostChartData}';
			var parsedData = JSON.parse(rawData);
			var chartData = [];
			var ticks = [];
			var labels = ["{!productFamily}","{!relatedProductName}"];

			if (parsedData.data.length)
			{
				chartData.push([]); //Product 1 data
				chartData.push([]); //Product 2 data

				for (var i=0, item=null; item = parsedData.data[i]; i++)
				{
					chartData[0].push(parseInt(item.p1));
					chartData[1].push(parseInt(item.p2));
					ticks.push(item.label);
				}
			}

            function tooltipContentEditor(str, seriesIndex, pointIndex, plot) {
		        return plot.series[seriesIndex]["label"] + ", " + plot.data[seriesIndex][pointIndex] + "%";
            }

			chart1 = j$.jqplot('oppWinRate', chartData, {
						stackSeries: true,
						seriesColors:['#106cbf', '#7ccc45', '#ea6f00', '#b33fde', '#f3d913', '#ae2e15', '#6de7be'],
						grid: {
							drawGridlines: false,
							shadow: false
						},
						highlighter: {
							show: true,
							showTooltip: true,
							tooltipContentEditor: tooltipContentEditor
						},
						seriesDefaults:{
							renderer: j$.jqplot.BarRenderer,
							pointLabels: {show: false},
							shadow: false
						},
						series:[
						{label: labels[0]},
						{label: labels[1]}
						],
						axesDefaults: {
							//               tickRenderer: j$.jqplot.CanvasAxisTickRenderer,
							labelOptions: {
								fontFamily: 'Arial, Helvetica, sans-serif',
								fontSize: '10pt',
								fontWeight: 'bold'
							},
							tickOptions: {
								fontFamily: 'Arial, Helvetica, sans-serif',
								angle: -35,
								fontSize: '8pt'
							}
						},
						axes: {
							xaxis: {
								label:'Order Date',
								//                  labelRenderer: j$.jqplot.CanvasAxisLabelRenderer,
								renderer: j$.jqplot.CategoryAxisRenderer,
								ticks: ticks
							},
							yaxis: {
								label:'Record Count',
								labelRenderer: j$.jqplot.CanvasAxisLabelRenderer,

								padMin: 0,
								tickInterval: 20,
								min: 0,
								max: 100,
								tickOptions: {
									angle: 0,
									formatString:'%d%'
								}
							}
						},
						legend: {
							show: true,
							location: 'e',
							placement: 'outsideGrid'
						}
					});
				})();

			

        //Lost Order Total
          (function(){
		    var rawData = '{!LostChartData}';
			var parsedData = JSON.parse(rawData);
			var chartData = [];
			var ticks = [];
			var labels = ["Software","Demo Process", "Other", "Performance", "Price", "Product Offering", "Relationship"];

			if (parsedData.data.length)
			{
				chartData.push([]); //Count
				chartData.push([]); //DemoProcessCount
				chartData.push([]); //OtherCount
				chartData.push([]); //PerformanceProcessCount
				chartData.push([]); //PriceCount
				chartData.push([]); //ProductOfferingCount
				chartData.push([]); //RelationshipCount

				for (var i=0, item=null; item = parsedData.data[i]; i++)
				{
					chartData[0].push(parseInt(item.Count));
					chartData[1].push(parseInt(item.DemoProcessCount));
					chartData[2].push(parseInt(item.OtherCount));
					chartData[3].push(parseInt(item.PerformanceProcessCount));
					chartData[4].push(parseInt(item.PriceCount));
					chartData[5].push(parseInt(item.ProductOfferingCount));
					chartData[6].push(parseInt(item.RelationshipCount));
					ticks.push(item.QuarterName);
				}
			}
            function tooltipContentEditor(str, seriesIndex, pointIndex, plot) {
		        return plot.series[seriesIndex]["label"] + ", " + plot.data[seriesIndex][pointIndex] + "%";
            }
			chart2 = j$.jqplot('lostOrderTotal', chartData, {
		    stackSeries: true,
		    seriesColors:['#106cbf', '#7ccc45', '#ea6f00', '#b33fde', '#f3d913', '#ae2e15', '#6de7be'],
            grid: {
	    	    drawGridlines: false,
	    	    shadow: false
            },
            highlighter: {
                show: true,
                showTooltip: true,
                tooltipContentEditor: tooltipContentEditor
            },
		    seriesDefaults:{
		      renderer: j$.jqplot.BarRenderer,
		      pointLabels: {show: false},
		      shadow: false
		    },
		    series:[
		           {label: labels[0]},
		           {label: labels[1]},
		           {label: labels[2]},
		           {label: labels[3]},
		           {label: labels[4]},
		           {label: labels[5]},
		           {label: labels[6]}
		     ],
            axesDefaults: {
               // tickRenderer: j$.jqplot.CanvasAxisTickRenderer,
               labelOptions: {
                  fontFamily: 'Arial, Helvetica, sans-serif',
                  fontSize: '10pt',
                  fontWeight: 'bold'
               },
               tickOptions: {
                  fontFamily: 'Arial, Helvetica, sans-serif',
                  angle: -35,
                  fontSize: '8pt'
               }
            },
		    axes: {
               xaxis: {
                  label:'Order Date',
                  //labelRenderer: j$.jqplot.CanvasAxisLabelRenderer,
                  renderer: j$.jqplot.CategoryAxisRenderer,
                  ticks: ticks
               },
               yaxis: {
                  label:'Sum of Opportunity Count',
                  labelRenderer: j$.jqplot.CanvasAxisLabelRenderer,
                  padMin: 0,
                  tickInterval: 20,
                  min: 0,
                  max: 100,
                  tickOptions: {
                     angle: 0,
                     formatString:'%d%'
                  }
               }
            },
            legend: {
               show: true,
               location: 'e',
               placement: 'outsideGrid'
               }
            });
         })();
         
          

        var replaced = replaceChartWithImg('oppWinRate') && replaceChartWithImg('lostOrderTotal');
        
        if (!replaced)
        {
            j$(window).resize(function(){
	            if (window.winLossTabResizeTimeout) clearTimeout(window.winLossTabResizeTimeout);
	            window.winLossTabResizeTimeout = setTimeout(function(){
		            chart1.replot( { resetAxes: true } );
		            chart2.replot( { resetAxes: true } );
	            }, 100);
            });        
        } 

        </script>


		<table class="table">
			<tr>
				<th style="width: 13%">
					<apex:outputText value="Opportunity Name" />
				</th>
				<th style="width: 9%">
					<apex:outputText value="Won/Lost" />
				</th>
				<th style="width: 9%">
					<apex:outputText value="Winning Model" />
				</th>
				<th style="width: 9%">
					<apex:outputText value="Vertical" />
				</th>
				<th style="width: 13%">
					<apex:outputText value="Application" />
				</th>
				<th style="width: 36%">
					<apex:outputText value="Outcome Detail" />
				</th>
				<th style="width: 11%">
					<apex:outputText value="Record Type" />
				</th>
			</tr>
			<apex:repeat value="{!Opportunities}" var="opp">
			<tr>
				<td>
					<apex:outputText value="{!opp.name}" />
				</td>
				<td>
					<apex:outputText value="{!opp.Opportunity_Won_Lost__c }" />
				</td>
				<td>
					<apex:outputText value="{!opp.Competitor_Instrument_Type__c }" />
				</td>
				<td>
					<apex:outputText value="{!opp.Market_Vertical__c}" />
				</td>
				<td>
					<apex:outputText value="{!opp.Primary_Application__c}" />
				</td>
				<td>
					<apex:outputText value="{!opp.Lost_Order_Detail__c}" />
				</td>
				<td>
					<apex:outputText value="{!opp.RecordType.Name}" />
				</td>
			</tr>
			</apex:repeat>
		</table>
		
		<div class="pageFooter">&copy; 2013 AB Sciex Proprietary &amp; Confidential for Internal Use Only</div>
		
	</div>
	
	<!-- End page Win/Loss Report -->

    <div style="page-break-after:always;"></div>
    
    <!-- Begin page Product Positioning -->
    
    <div class="page">
    
		<div class="pageHeader">
			<div class="pageHeaderWrapper clearfix">
		    	<div class="imgContainer">
	            	<apex:image value="{!URLFOR($Resource.ABsciexlogo)}" height="49px" width="180px"/>
	            </div>
	            <div class="pageTitle">Win/Loss Report</div>
	            <div class="pageTitle subtitle">{!absciexProductName} Vs {!relatedProductName}</div>
            </div>
		</div>

		<h1>Competitive Positioning</h1>
		<h2>
			<apex:outputText styleclass="freeform" style="float:left;">{!freeformText1}</apex:outputText>
		</h2>

		<table class="table">
			<apex:outputText rendered="{!IF(postList != null, IF(postList.size > 0, TRUE, FALSE), FALSE)}">
			<tr>
				<th width="33%">
					<apex:outputText value="Customer Need" rendered="{!IF(postList != null, IF(postList.size > 0, TRUE, FALSE), FALSE)}" />
				</th>
				<th width="33%">
					<apex:outputText value="What Thermo will say" rendered="{!IF(postList != null, IF(postList.size > 0, TRUE, FALSE), FALSE)}" />
				</th>
				<th width="34%">
					<apex:outputText value="We respond with" rendered="{!IF(postList != null, IF(postList.size > 0, TRUE, FALSE), FALSE)}" />
				</th>
			</tr>
			<tr>
				<td>
					<table class="table">
						<apex:variable value="{!1}" var="rowNum" />
						<apex:repeat value="{!postList}" var="pos">
							<tr>
								<td valign="top">
									<apex:outputText value="{!FLOOR(rowNum)}. " />
								</td>
								<td valign="top">
									<apex:outputText escape="false" value="{!pos.Competitor_s_Need_Short__c}" />
									<apex:variable var="rowNum" value="{!rowNum + 1}" />
								</td>
							</tr>
						</apex:repeat>
					</table>
					<table class="table">
						<apex:variable value="{!1}" var="rowNum" />
						<tr>
							<th colspan="2">
								<apex:outputText value="Our Product Features" rendered="{!IF(postList != null, IF(postList.size > 0, TRUE, FALSE), FALSE)}" />
							</th>
						</tr>
						<apex:repeat value="{!postList}" var="pos">
						<tr>
							<td valign="top">
								<apex:outputText value="{!FLOOR(rowNum)}. " />
							</td>
							<td valign="top">
								<apex:outputText escape="false" value="{!pos.Key_Features__c}" />
								<apex:variable var="rowNum"	value="{!rowNum + 1}" />
							</td>
						</tr>
						</apex:repeat>
					</table>
				</td>
				<td>					
					<table class="table">
						<apex:variable value="{!1}" var="rowNum" />
						<apex:repeat value="{!postList}" var="pos">
							<tr>
								<td valign="top">
									<apex:outputText value="{!FLOOR(rowNum)}. " />
								</td>
								<td valign="top">
									<apex:outputText escape="false"	value="{!pos.Competitor_Say__c}" />
									<apex:variable var="rowNum" value="{!rowNum + 1}" />
								</td>
							</tr>
						</apex:repeat>
					</table>
				</td>
				<td>					
					<table class="table">					
						<apex:variable value="{!1}" var="rowNum" />
						<apex:repeat value="{!postList}" var="pos">
						<tr>
							<td>
								<apex:outputText value="{!FLOOR(rowNum)}. " />
							</td>
							<td>
								<apex:outputText escape="false"	value="{!pos.Respond_With__c}" />
								<apex:variable var="rowNum"	value="{!rowNum + 1}" />
							</td>
						</tr>
						</apex:repeat>
					</table>
				</td>
			</tr>
			</apex:outputText>
		</table>
		
		<h2>{!freeformtext}</h2>

		<div class="pageFooter">&copy; 2013 AB Sciex Proprietary &amp; Confidential for Internal Use Only</div>
		
	</div>
    
    <!-- End page Product Positioning -->
    
    <div style="page-break-after:always;"></div>
    
    <!-- Begin page Demo Strategy -->
    
	<div class="page">
		<div class="pageHeader">
			<div class="pageHeaderWrapper clearfix">
		    	<div class="imgContainer">
		           	<apex:image value="{!URLFOR($Resource.ABsciexlogo)}" height="49px" width="180px"/>
				</div>
	            <div class="pageTitle">Demo Strategy</div>
	            <div class="pageTitle subtitle">{!absciexProductName} Vs {!relatedProductName}</div>
			</div>
		</div>
		
		<h1>Demo Strategy</h1>
		<h2>
			<apex:outputText styleclass="freeform" style="float:left;">{!freeformText1}</apex:outputText>
		</h2>
		
		<table class="table">
			<tr>
				<th style="width:33%">
					<apex:outputText value="Product Features" />
				</th>
				<th style="width:33%">
					<apex:outputText value="What to push at demo" />
				</th>
				<th style="width:33%">
					<apex:outputText value="What to avoid" />
				</th>
			 </tr>
			 <tr>
				<td>
					<apex:outputText value="{!dms.Product_Features__c}" escape="false"/>
				</td>
				<td>
					<apex:outputText value="{!dms.What_to_push_at_demo__c}" escape="false"/>
				</td>
				<td>
					<apex:outputText value="{!dms.What_to_avoid__c}" escape="false"/>
				</td>
			 </tr>
		</table>

		<h2>{!freeformtext}</h2>
		
		<div class="pageFooter">&copy; 2013 AB Sciex Proprietary &amp; Confidential for Internal Use Only</div>

	</div>
    
    <!-- End page Demo Strategy -->
    
    <div style="page-break-after:always;"></div>
    
    <!-- Begin page Summary -->
    
	<div class="page">
		<div class="pageHeader">
			<div class="pageHeaderWrapper clearfix">
		    	<div class="imgContainer">
		           	<apex:image value="{!URLFOR($Resource.ABsciexlogo)}" height="49px" width="180px"/>
				</div>
	            <div class="pageTitle">Summary</div>
	            <div class="pageTitle subtitle">{!absciexProductName} Vs {!relatedProductName}</div>
			</div>
		</div>
		
		<h1>Summary</h1>
		<h2>
			<apex:outputText styleclass="freeform" style="float:left;">{!freeformText1}</apex:outputText>
		</h2>
		
		<table class="table tableSummary">
			<apex:variable value="{!1}" var="rowNum"/>
			<apex:repeat value="{!sumList}" var="sum">
			<tr>
				<td>
					<apex:outputText value="{!FLOOR(rowNum)}. "/>
					<apex:variable var="rowNum" value="{!rowNum + 1}"/>
				</td>
				<td>
					<apex:outputText value="{!sum.Summary__c}" escape="false"/>
				</td>
			</tr>
			</apex:repeat>
		</table>

    	<h2>{!freeformtext}</h2>
		
		<div class="pageFooter">&copy; 2013 AB Sciex Proprietary &amp; Confidential for Internal Use Only</div>
		
	</div>
    
    <!-- End page Summary -->
    <div style="page-break-after:always;"></div>  
    
</apex:page>