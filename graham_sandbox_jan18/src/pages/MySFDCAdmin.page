<apex:page showHeader="true" sidebar="true" controller="MySFDCAdminController" action="{!init}" tabStyle="My_SFDC__tab">
	<apex:pageMessages id="pageMessages"/>
	<apex:form >
	<apex:pageBlock title="Manage Conversion Rates for {!region}" id="mainPageBlock" rendered="{!editConversionRates}">
		<apex:pageBlockTable value="{!conversionRates}" var="cr">
			<apex:column >
				<apex:facet name="header"></apex:facet>
				<apex:commandButton value="Edit" rendered="{!cr.renderEditButton}" action="{!editConversionRate}" reRender="mainPageBlock,pageMessages">
					<apex:param assignTo="{!selectedConversionRateId}" name="selectedConversionRateId" value="{!cr.conversionRate.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Save" rendered="{!cr.renderSaveButton}" action="{!saveConversionRate}" reRender="mainPageBlock,pageMessages">
					<apex:param assignTo="{!selectedConversionRateId}" name="selectedConversionRateId" value="{!cr.conversionRate.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Cancel" rendered="{!cr.renderCancelButton}" action="{!cancelEditConversionRate}" reRender="mainPageBlock,pageMessages">
					<apex:param assignTo="{!selectedConversionRateId}" name="selectedConversionRateId" value="{!cr.conversionRate.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Add New" rendered="{!cr.renderNewButton}" action="{!addConversionRate}" reRender="mainPageBlock,pageMessages">
				</apex:commandButton>
			</apex:column>
			<apex:column >
				<apex:facet name="header">{!$ObjectType.Conversion_Rate__c.fields.Opportunity_Type__c.label}</apex:facet>
				<apex:outputField value="{!cr.conversionRate.Opportunity_Type__c}" rendered="{!AND(NOT(cr.editMode),NOT(ISBLANK(cr.conversionRate)))}"/>
				<apex:inputField value="{!cr.conversionRate.Opportunity_Type__c}" rendered="{!AND(cr.editMode, NOT(ISBLANK(cr.conversionRate)))}"/>
			</apex:column>
			<apex:column >
				<apex:facet name="header">{!$ObjectType.Conversion_Rate__c.fields.Conversion_Rate__c.label}</apex:facet>
				<apex:outputField value="{!cr.conversionRate.Conversion_Rate__c}" rendered="{!AND(NOT(cr.editMode),NOT(ISBLANK(cr.conversionRate)))}"/>
				<apex:inputField value="{!cr.conversionRate.Conversion_Rate__c}" rendered="{!AND(cr.editMode, NOT(ISBLANK(cr.conversionRate)))}"/>
			</apex:column>
		</apex:pageBlockTable>
	</apex:pageBlock>

	<apex:pageBlock title="Manage Quotas" id="quotasPageBlock" rendered="{!editQuotas}">
		<apex:pageBlockTable value="{!quotas}" var="q">
			<apex:column >
				<apex:facet name="header"></apex:facet>
				<apex:commandButton value="Edit" rendered="{!q.renderEditButton}" action="{!editQuota}" reRender="quotasPageBlock,pageMessages">
					<apex:param assignTo="{!selectedQuotaId}" name="selectedQuotaId" value="{!q.quota.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Delete" rendered="{!q.renderDeleteButton}" action="{!deleteQuota}" reRender="quotasPageBlock,pageMessages">
					<apex:param assignTo="{!selectedQuotaId}" name="selectedQuotaId" value="{!q.quota.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Save" rendered="{!q.renderSaveButton}" action="{!saveQuota}" reRender="quotasPageBlock,pageMessages">
					<apex:param assignTo="{!selectedQuotaId}" name="selectedQuotaId" value="{!q.quota.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Cancel" rendered="{!q.renderCancelButton}" action="{!cancelEditQuota}" reRender="quotasPageBlock,pageMessages">
					<apex:param assignTo="{!selectedQuotaId}" name="selectedQuotaId" value="{!q.quota.Id}"/>
				</apex:commandButton>
				<apex:commandButton value="Add New" rendered="{!q.renderNewButton}" action="{!addQuota}" reRender="quotasPageBlock,pageMessages">
				</apex:commandButton>
			</apex:column>
			<apex:column >
				<apex:facet name="header">{!$ObjectType.Quota__c.fields.Fiscal_Year__c.label}</apex:facet>
				<apex:outputField value="{!q.quota.Fiscal_Year__c}" rendered="{!AND(NOT(q.editMode),NOT(ISBLANK(q.quota)))}"/>
				<apex:inputField value="{!q.quota.Fiscal_Year__c}" rendered="{!AND(q.editMode, NOT(ISBLANK(q.quota)))}"/>
			</apex:column>
			<apex:column >
				<apex:facet name="header">{!$ObjectType.Quota__c.fields.Fiscal_Quarter__c.label}</apex:facet>
				<apex:outputField value="{!q.quota.Fiscal_Quarter__c}" rendered="{!AND(NOT(q.editMode),NOT(ISBLANK(q.quota)))}"/>
				<apex:inputField value="{!q.quota.Fiscal_Quarter__c}" rendered="{!AND(q.editMode, NOT(ISBLANK(q.quota)))}"/>
			</apex:column>
			<apex:column rendered="{!renderTypeColumn}" >
				<apex:facet name="header">{!$ObjectType.Quota__c.fields.Type__c.label}</apex:facet>
				<apex:outputField value="{!q.quota.Type__c}" rendered="{!AND(NOT(q.editMode),NOT(ISBLANK(q.quota)))}"/>
				<apex:inputField value="{!q.quota.Type__c}" rendered="{!AND(q.editMode, NOT(ISBLANK(q.quota)))}"/>
			</apex:column>
			<apex:column >
				<apex:facet name="header">{!$ObjectType.Quota__c.fields.Quota__c.label}</apex:facet>
				<apex:outputField value="{!q.quota.Quota__c}" rendered="{!AND(NOT(q.editMode),NOT(ISBLANK(q.quota)))}"/>
				<apex:inputField value="{!q.quota.Quota__c}" rendered="{!AND(q.editMode, NOT(ISBLANK(q.quota)))}"/>
			</apex:column>
		</apex:pageBlockTable>
	</apex:pageBlock>
	</apex:form>
</apex:page>