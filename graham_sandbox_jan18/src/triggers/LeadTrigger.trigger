/*
 *  LeadTrigger
 *  
 *      Trigger class that implements Trigger Framework.
 *          https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *          
 *      According to the Trigger Framework, all trigger logic should be in trigger handler class.
 * 
 *  Created by Yong Chen/Daniel Lachcik  on 2016-10-03
 *
 *  [Modification history]
 *  [Name] [Date] Description
 *
 */
trigger LeadTrigger on Lead (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
system.debug('Fired LeadTrigger');  
    new LeadTriggerHandler().run();
    
    
}