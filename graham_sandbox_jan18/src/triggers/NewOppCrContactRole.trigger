/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : December, 2012
Description    : This trigger (based on Primary_Contact__c field if not null) will update Opportunity Contact Role related list with the 
           Primary Contact and Primary Campaign Source (with the most recent one for the Primary Contact) in Opportunity after creation
History        :

*/
trigger NewOppCrContactRole on Opportunity (after insert) 
{
    ID OppId;
    ID PrimaryContactID;
    ID ContactID;   
    Boolean NoPrimaryContact;
    list<OpportunityContactRole> ContactIDPR = new list<OpportunityContactRole>();
    list<Opportunity> Opp = new list<Opportunity>();
    List<CampaignMember> CampaignMemberID = new List<CampaignMember>();    
    List<Campaign> CampaignID = new List<Campaign>();    
    List<Id> MemID = new List<Id>();    
    for(Opportunity Opps : Trigger.new)
    {
      OppId = Opps.Id;
      PrimaryContactID = Opps.Primary_Contact__c; 
    }    
            //This condition is necessary if the Opp is created through the Lead
            if(PrimaryContactID != null)    
            {   
                //Create and insert a OpportunityContactRole considering ID Primary Contact and IsPrimary = true
                OpportunityContactRole ContactRole = new OpportunityContactRole();
                ContactRole.OpportunityId = OppId;
                ContactRole.ContactId = PrimaryContactID;
                ContactRole.IsPrimary = true;
                ContactRole.Role = 'Other';
                Insert ContactRole;        
        
                
                //Campaign check availability at the contact, and if there is inserted in Primary Campaign Source
                ContactIDPR = [Select ContactId From OpportunityContactRole Where OpportunityId =:OppId And IsPrimary = true];
                ContactID = [Select Id From Contact Where Id=: ContactIDPR[0].ContactId].Id;            
                Opp = [Select Id, CampaignId, Primary_Contact__c From Opportunity Where Id=: OppId];
                CampaignMemberID = [SELECT CampaignId From CampaignMember Where ContactId =: ContactID];
                if(CampaignMemberID.size() != 0)
                {
                   for(CampaignMember a: CampaignMemberID)
                   {
                    MemID.add(a.CampaignId);
                   }                
                   CampaignID = [Select StartDate, Id From Campaign Where Id IN: MemID Order by StartDate DESC limit 1];
                   //Feb 9/2014: added Opp[0].CampaignId == null condition so that Campaign once set sticks
                   If(CampaignID.size() > 0 && Opp[0].CampaignId == null) 
                   {
                    Opp[0].CampaignId = CampaignID[0].id;
                   }
                 update  Opp;  
                }    
            }               
    
}