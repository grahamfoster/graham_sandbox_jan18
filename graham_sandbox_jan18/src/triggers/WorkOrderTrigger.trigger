/*
 *  WorkOrderTrigger
 *  
 *      Trigger class that implements Trigger Framework.
 *          https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *          
 *      According to the Trigger Framework, all trigger logic should be in trigger handler class.
 * 
 *  Based on Framework by Yong Chen on 2016-03-08
 *
 *  Created by Brett Moore 2016-05-20
 *   
 *  [Modification history]
 *  [Name] [Date] Description
 *
 */
trigger WorkOrderTrigger on SVMXC__Service_Order__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    new WorkOrderTriggerHandler().run();
}