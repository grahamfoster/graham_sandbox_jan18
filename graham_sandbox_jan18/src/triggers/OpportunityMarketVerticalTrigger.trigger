/**
 ** @author Reid Beckett, Cloudware Connections
**/
trigger OpportunityMarketVerticalTrigger on Opportunity (before insert, before update, after update) {
    if(Trigger.isBefore) {
	    OpportunityMarketVerticalTriggerHandler.onInsertUpdate(Trigger.new);
    }else{
    	OpportunityMarketVerticalTriggerHandler.onAfterUpdate();
    }
}