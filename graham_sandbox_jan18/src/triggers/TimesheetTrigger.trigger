/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Oct 24/2013
 **
 ** Trigger on the timesheet record.
 ** 1) Validate that the start date is a Monday
**/
trigger TimesheetTrigger on Timesheet__c (before insert, before update) {
	TimesheetTriggerHandler.onBefore(Trigger.oldMap, Trigger.new);
}