/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 27/2015
**
** Trigger on Parts Order object
** When a Parts Order record (managed custom object) is created or edited, if the record type = “RMA” and the order status = “Submitted” then set the line status of all line items to “Submitted”
** 
**/
trigger PartsOrderTrigger on SVMXC__RMA_Shipment_Order__c (after update) 
{
    new PartsOrderTriggerHandler().onAfter();
}