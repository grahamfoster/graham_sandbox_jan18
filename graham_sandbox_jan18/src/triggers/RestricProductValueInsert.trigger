trigger RestricProductValueInsert on Product_Value__c (before insert) {
	string productmetaids; 
    string productids;
    List<Product_Value__c> productValueList;
    Product_Value__c productvalue = trigger.new[0];
    {
        productmetaids = (productvalue.Product_Meta__c);
        productids = (productvalue.Product__c);     
    }
    if(productmetaids != null){
        
        productValueList = [SELECT id, Product_Meta__c, Product__c from Product_Value__c where Product_Meta__c = : productmetaids];  
    }
    if(!productvalueList.isEmpty())
    { 
        for(Product_Value__c prodvalue: productValueList)
        {
            if( prodvalue.Product_Meta__c == productmetaids && prodvalue.Product__c == productids)
            {
                Trigger.new[0].addError('You Cannot create more than one Product value in the same Product Feature in a particular Product');
            }
            
        }
        
    }
}