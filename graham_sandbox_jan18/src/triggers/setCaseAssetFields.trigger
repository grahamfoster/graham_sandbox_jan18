trigger setCaseAssetFields on Case (before insert, before update) {

    // Get record type for CIC Case
    RecordType cicRecordType = [select id from RecordType where name = 'CIC Case'];

    // If CIC case record type has not been setup, then end trigger
    if (cicRecordType == null)
        return;

    Id cicRecordTypeId = cicRecordType.Id;  


    Set<Id> assetIds = new Set<Id>();

            
    // Get a list of asset Ids for CIC cases
    for (Case c: Trigger.New){    
        if (c.AssetId != null && c.RecordTypeId == cicRecordTypeId )
            assetIds.add(c.AssetId);
    }

    // Get asset and product data for assets   
    // Get active contract for each asset
    Map<Id, Asset> assets = null;
    
    if (assetIds.isEmpty()){
        assets = new Map<Id, Asset>();
    }
    else{        
        assets = new Map<Id, Asset>([select Name, Status, SerialNumber, Equipment_Number__c, Product2Id, AccountId, 
            (select Name  from Contracts__r where Contract_Status__c = 'Active' limit 1) 
            from Asset where id in :assetIds]);
    }
  
 
    // Loop through each case, populate or clear asset/product fields
    for (Case c: Trigger.New){

        // Exit loop if this is not a CIC Case
        if (c.RecordTypeId != cicRecordTypeId )
            continue;
            
        // First, clear asset fields
        c.Case_Asset_Name__c = null;
        c.Case_Serial_Number__c = null;    
        c.Case_Equipment_Number__c = null;                 
        c.Case_Contract_Number__c = null;              
        c.Case_Asset_Status__c = null;   
        c.Case_Asset_Account__c = null;  
               
        if (c.Assetid != null){
        // There's an asset, get asset info        
            Asset caseAsset = assets.get(c.Assetid);
            
            if (caseAsset!=null){
            
                // Populate asset fields                
                c.Case_Asset_Name__c = caseAsset.Name;
                c.Case_Serial_Number__c = caseAsset.SerialNumber;        
                c.Case_Equipment_Number__c = caseAsset.Equipment_Number__c; 
                c.Case_Asset_Status__c = caseAsset.Status; 
                c.Case_Asset_Account__c = caseAsset.AccountId; 

                // Populate product field with product from from asset
                c.Product__c = caseAsset.Product2Id;
                
                // Populate contract field with active contract number
                // Get contract object
                Contract [] assetContracts  = caseAsset.Contracts__r;
                
                if (assetContracts!=null){     
                    for (Contract assetContract : assetContracts) {
                        c.Case_Contract_Number__c = assetContract.Name;  
                    }
                }               
           }
        }
    }
}