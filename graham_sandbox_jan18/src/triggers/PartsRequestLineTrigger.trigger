/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 25/2015
**
** For new Parts Request Lines (managed custom object), if the status of the Parts Request parent = “Submitted” then update the line status picklist to “Submitted”
** 
**/
trigger PartsRequestLineTrigger on SVMXC__Parts_Request_Line__c (before insert) 
{
	new PartsRequestLineTriggerHandler().onBefore();
}