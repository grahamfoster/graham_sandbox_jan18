/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 12/2017
**
** Trigger on Asset object, introduced to update related Contacts after SCIEXNow fields updates
** 
**/
trigger AssetTrigger on Asset (after insert, after update, after delete, after undelete) 
{
	new AssetTriggerHandler().run();
}