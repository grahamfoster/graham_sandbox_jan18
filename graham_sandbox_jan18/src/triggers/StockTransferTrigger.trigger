/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 27/2015
**
** When a Stock Transfer record (managed custom object) is created or edited, if the stock transfer pick list = “Submitted” then set the status of all line items to “Submitted”
** 
**/
trigger StockTransferTrigger on SVMXC__Stock_Transfer__c (after update) 
{
	new StockTransferTriggerHandler().onAfter();
}