trigger LiveAgentSessionTrigger on LiveAgentSession (after insert, after update, before insert, before update)  
{ 
	new LiveAgentSessionTriggerHandler().run();
}