trigger CampaignMemberTrigger on CampaignMember (after delete, after insert) {
	if(Trigger.isAfter && Trigger.isInsert) {
		CampaignMemberTriggerHandler.onAfterInsert(Trigger.new);
	}
	if(Trigger.isAfter && Trigger.isDelete) {
		CampaignMemberTriggerHandler.onAfterDelete(Trigger.old);
	}
}