/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 25/2015
**
** When fields modified, copy back to related cases
** 
**/
trigger InstalledProductTrigger on SVMXC__Installed_Product__c (after update, after delete, after undelete, after insert) 
{
    new InstalledProductTriggerHandler().run();
}