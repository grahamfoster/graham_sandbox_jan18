trigger updateQuoteReportData on QuoteReportData__c (before insert) {
	System.debug('*** QuoteReportTrigger is fired...');
	
	//Get product line map
	List<Product_Line__c> pList = [Select p.Name, p.Desc__c From Product_Line__c p Where p.Name != null Limit 999];
	Map<String,String> pMap = new Map<String,String>();
	for(Product_Line__c p : pList){
		pMap.put(p.Name, p.Desc__c);
	}
	//Get IGOR map
	List<IGOR_Item_Class__c> iList = [Select i.Name, i.IGOR_Item_Class_Description__c From IGOR_Item_Class__c i Where i.Name != null Limit 999];
	Map<String,String> iMap = new Map<String,String>();
	for(IGOR_Item_Class__c igor : iList){
		iMap.put(igor.Name, igor.IGOR_Item_Class_Description__c);
	}
	
	// ODBG FIX : retrieve only what we need not all the matrix
	Set<String> statuss = new Set<String>();
	for(QuoteReportData__c data : Trigger.new){
		if(data.Status__c <> null){
			statuss.add(data.Status__c);
		}
	
	}
	
	//Get Status map
	//List<Quoting_SAP_Country_Matrix__c> sList = [SELECT q.Value__c, q.Description__c FROM Quoting_SAP_Country_Matrix__c q Where q.Value__c != null  Order By q.Value__c Limit 400];
	Map<String,String> sMap = new Map<String,String>();
	for(Quoting_SAP_Country_Matrix__c status : [SELECT q.Value__c, q.Description__c FROM Quoting_SAP_Country_Matrix__c q Where q.Value__c in:statuss  Order By q.Value__c Limit 400]){
		sMap.put(status.Value__c, status.Description__c);		
	}
	
	// ODBG FIX : retrieve only what we need not all the matrix
	
	//Perform update to report
	for(QuoteReportData__c data : Trigger.new){
		if(data.Product_Line__c <> null ){
			data.Product_Line_Desc__c = pMap.get(data.Product_Line__c);
		}
		if(data.IGOR_Class_Desc__c <> null){
			data.IGOR_Class_Desc__c = iMap.get(data.IGOR_Class_Desc__c);
		}
		if(data.Status__c <> null){
			data.Status__c = sMap.get(data.Status__c);			
		}
	}
}