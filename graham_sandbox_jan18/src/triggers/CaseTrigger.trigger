/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 24/2015
**
** Trigger on Case object, introduced to enhance the Process Builder flows
** 
**/
trigger CaseTrigger on Case (after insert, after update, before insert, before update) 
{
    new CaseTriggerHandler().run();
}