trigger OpportunityStrategyTrigger on Opportunity (before insert, after insert, before update, after update, after delete, after undelete) {
    if(Trigger.isInsert){
        if(Trigger.isBefore){
	        OpportunityStrategyTriggerHandler.onBeforeInsert(Trigger.new);
        } else {
        	OpportunityStrategyTriggerHandler.onAfterInsert(Trigger.new);
        }
    }
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
	        OpportunityStrategyTriggerHandler.onBeforeUpdate(Trigger.new, Trigger.oldMap);
        } else{
        	OpportunityStrategyTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    if(Trigger.isDelete){
        OpportunityStrategyTriggerHandler.onDelete(Trigger.old);
    }
    if(Trigger.isUndelete){
        OpportunityStrategyTriggerHandler.onUndelete(Trigger.new);
    }
}