/**
 *  OP02 - When the related account is changed and the opportunity has a related department, 
 *         remove the department unless the department is related to the new account.
 */
trigger updateOpportunityTrigger on Opportunity (before update) {

    //first get all the record types of the contacts being updated.
    Set<ID> recordTypeIDs = new Set<ID>();
    for (Opportunity op : Trigger.new) {
        recordTypeIDs.add(op.RecordTypeId);
    }    
    Map<ID,RecordType> recordTypes = new Map<ID,RecordType>([select name,id from RecordType where id in :recordTypeIDs]);
    
    Opportunity oldOpp = null;
    RecordType rt = null;
    String rtName = null;
    for (Opportunity op : Trigger.new) {
        oldOpp = Trigger.oldMap.get(op.Id);
        rt = recordTypes.get(op.RecordTypeId);
        rtName = rt.name.toLowerCase().substring(0,2); //compare only the first two letters of the record type name
        
        //if the account changed and the record type name begins with 'EU' remove related department.
        if (op.AccountId != oldOpp.AccountId && rtName=='eu') {
            op.Department__c = null;      
        }           
    }
}