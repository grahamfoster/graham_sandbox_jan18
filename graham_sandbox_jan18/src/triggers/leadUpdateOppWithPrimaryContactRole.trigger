/*
* File Name  : leadUpdateOppWithPrimaryContactRole
* Description: Trigger to update Opportunities created  during Lead conversion with Contact role isPrimary = TRUE
* @author    : Evgeny Soloho
* Modification Log
================================================================================
* Ver    Date        Author                Modification 
--------------------------------------------------------------------------------
* 1.0    02/09/2012  Evgeny Soloho         Initial
*/

trigger leadUpdateOppWithPrimaryContactRole on Lead (after update) {

    List<Id> conAcctId = new List<Id>(); // List of the converted Account IDs
    List<Id> conOppId = new List<Id>();  // List of the converted Opportunity IDs
    Map<Id,String> leadStatus = new Map<Id,String>(); // Map of the converted Contact ID and the Lead Status
    Map<Id,Id> leadContactMap = new Map<Id,Id>(); // Map of the Lead ID and the converted contact Id 
    Map<Id,Id> leadAccountMap = new Map<Id,Id>(); // Map of the Lead ID and the converted account Id 

    for(Lead lead : Trigger.new) {
        if (lead.IsConverted) {
            leadStatus.put(lead.ConvertedContactId,lead.Status);
            conAcctId.add(lead.ConvertedAccountId);
            if (lead.ConvertedOpportunityId != null) {
                conOppId.add(lead.ConvertedOpportunityId);
            }
        leadContactMap.put(lead.ID,lead.ConvertedContactId);
        leadAccountMap.put(lead.ID,lead.ConvertedAccountId);
        }
    }


    
    // If an opportunity was created during conversion, need to assign the contact role as primary
    List<OpportunityContactRole> oppConRole = [select ContactID from OpportunityContactRole where OpportunityId IN :conOppId];
    for ( OpportunitycontactRole cr :oppConRole) {
        cr.IsPrimary = true;
    }
    update oppConRole;


}