trigger SVMX_WorkDetail_After on SVMXC__Service_Order_Line__c (after insert, after update) 
{
	String profileName = [Select Name from Profile where Id = :UserInfo.getProfileId() limit 1].get(0).Name;

    if (!SVMX_WorkDetailRecursiveSaveHelper.isAlreadyRunPostWorkDetails() || System.Test.isRunningTest()) 
    {
        SVMX_WorkDetailRecursiveSaveHelper.setAlreadyRunPostWorkDetails();
        
		if (!(profileName.equalsIgnoreCase('Informatica')))
		{
			
			if (Trigger.isInsert)
			{
				List<SVMX_Work_Detail_Utility> wduList = new List<SVMX_Work_Detail_Utility>();
				for (SVMXC__Service_Order_Line__c wd : Trigger.new)
				{
					SVMX_Work_Detail_Utility wdu = new SVMX_Work_Detail_Utility(wd);
					wduList.add(wdu);
				}

				wduList.sort();

				List<SVMXC__Service_Order_Line__c> sortedWDList = new List<SVMXC__Service_Order_Line__c>();

				for (SVMX_Work_Detail_Utility wdu : wduList)
					sortedWDList.add(wdu.wd);

				SVMX_WorkOrder_Helper.insertDailyTravel(sortedWDList);
			}

			if (Trigger.isUpdate)
				SVMX_WorkOrder_Helper.checkUpdatedTravel(Trigger.new, Trigger.oldMap);
		}
	}
}