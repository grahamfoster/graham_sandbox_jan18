trigger CampaignStrategyTrigger on Campaign (after insert, after update) {
    if(Trigger.isInsert){
        CampaignStrategyTriggerHandler.onInsert(Trigger.new);
    }else if(Trigger.isUpdate){
        CampaignStrategyTriggerHandler.onUpdate(Trigger.new, Trigger.oldMap);
    }
}