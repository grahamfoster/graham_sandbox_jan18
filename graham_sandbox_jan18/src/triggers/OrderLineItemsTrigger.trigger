// ===========================================================================
// Object: OrderLineItemsTriggerHandler
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Handles Order Line Item trigger logic
// ===========================================================================
// Changes: 2017-02-18 Reid Beckett
//           Class created
// ===========================================================================
trigger OrderLineItemsTrigger on Order_Line_Items__c (after insert, after update, after delete, after undelete, before insert, before update, before delete) 
{
    new OrderLineItemsTriggerHandler().run();
}