/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Oct 3/2015
 **
 ** Trigger on the Opportunity object to rollup totals to the Account
**/
trigger OpportunityRollupTrigger on Opportunity (after insert, after update, after undelete, after delete) 
{
//    new OpportunityRollupTriggerHandler().onAfter();
}