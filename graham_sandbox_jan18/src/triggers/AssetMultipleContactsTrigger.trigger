trigger AssetMultipleContactsTrigger on Asset_MultipleContacts__c (after insert, after update, after delete, after undelete) {
	new AssetMultipleContactsTriggerHandler().run();
}