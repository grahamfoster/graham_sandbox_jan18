trigger OpportunityContactRoleShadowTrigger on Opportunity_Contact_Role_Shadow__c (before insert, before update) {
	new OpportunityContactRoleShadowHelper().onBeforeShadow();
}