/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : January, 2013
Description    : This trigger (based on Primary_Contact__c field if not null) will update Opportunity Contact Role related list with the 
                 Primary Contact and Primary Campaign Source (with the most recent one for the Primary Contact) in Opportunity during the update

History        :

*/
trigger UpdateOppContactRole on Opportunity (before update) 
{
    ID OppId;
    ID PrimaryContactID;
    ID ContactID;   
    List<Id> MemID = new List<Id>();       
    Opportunity Opp = new Opportunity();  

    OpportunityContactRole ContactIDPR = new OpportunityContactRole();    
    List<CampaignMember> CampaignMemberID = new List<CampaignMember>();  
    Campaign CampaignID = new Campaign();  
    //Dont run on bulk updates, just single updates  
    if(Trigger.new.size() == 1) 
        {            
    Opp =  Trigger.new.get(0);
    //System.debug('OppASDASDAD: ' + Opp.Primary_Contact__c);
    //if(Opp.Name.contains('Test Opp_') == true && Opp.Primary_Contact__c != null)
    //Feb 9/2014: Opp.CampaignId==null condition added so that Campaign once set sticks
    if(Opp.Primary_Contact__c != null && Opp.CampaignId == null)
        {    
        CampaignMemberID = [Select ContactId, CampaignId From CampaignMember Where ContactId =: Opp.Primary_Contact__c limit 10000];
        //System.debug('IDSASDASDAD' + CampaignMemberID);
        //System.debug('ASDASDAD' + CampaignMemberID.size());
        //Campaign check availability at the contact, and if there is change in the Primary Campaign Source
        if(CampaignMemberID.size()>0)
        {
            for(CampaignMember a:CampaignMemberID)
            {
                MemID.add(a.CampaignId);
            }
            CampaignID = [Select StartDate, Id From Campaign Where Id IN: MemID Order by StartDate DESC limit 1];
            Opp.CampaignId = CampaignID.Id;
        }
        else
        {
            Opp.CampaignId = null;
        }
    //Here we check if existing OpportunityContactRole, if Primary Contact is already there, then put him IsPrimary = true, if not then create a new one with IsPrimary = true
        if ([select count() from OpportunityContactRole where OpportunityId =:Opp.Id and ContactId =: Opp.Primary_Contact__c] == 0) 
        {    
              OpportunityContactRole ContactRole = new OpportunityContactRole();
              ContactRole.OpportunityId = Opp.Id;
              ContactRole.ContactId = Opp.Primary_Contact__c;
              ContactRole.IsPrimary = true;
              insert ContactRole;
         }
         else
         {
           ContactIDPR = [Select ContactId from OpportunityContactRole where OpportunityId =:Opp.Id and ContactId =: Opp.Primary_Contact__c limit 1];
           ContactIDPR.IsPrimary = true;
           update ContactIDPR;
                
         }
       }  
    }    

}