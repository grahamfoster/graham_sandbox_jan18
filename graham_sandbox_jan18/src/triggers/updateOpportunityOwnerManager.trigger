/*
* File Name  : updateOpportunityOwnerManager
* Description: Trigger to update Opportunity Owner Manager in custom Manager__c field
* @author    : Evgeny Soloho
* Modification Log
================================================================================
* Ver    Date        Author                Modification 
--------------------------------------------------------------------------------
* 1.0    03/15/2012  Evgeny Soloho         Initial
*/

trigger updateOpportunityOwnerManager on Opportunity (before insert, before update ){
  
  //Set of unique records to holds User Ids
  Set<Id> setUserIds = new Set<Id>();
  
  //List to hold Users Ids
  List<User> lstUser = new List<User>();
  
  //Mar to hold key value pairs of User Id and User Object
  Map<Id, User> mapUserObj = new Map<Id, User>();

  //For each opportunity being inserted or updated add User Id value in in Set of User Ids.
  for(Opportunity oppObj : Trigger.new){
    if(oppObj.OwnerId != null){
      setUserIds.add(oppObj.OwnerId);
    }
  }
  
  //Fetch all Users whose Id is in the Set and put them in the List
  lstUser = [select Id, Name, ManagerId from User where Id in :setUserIds Limit 1000];
  if(lstUser.size() == 0){
    return;  
  }
  
  //Add these fetched Users in a Map <User Id, User object>
  for(User usrObj : lstUser){
    mapUserObj.put(usrObj.Id, usrObj);  
  }
  
  //for each opportunity being inserted get User from the map and update the field values
  for(Opportunity oppObj : Trigger.new){
    //get User object
    if(oppObj.OwnerId != null){
      if(mapUserObj.containsKey(oppObj.OwnerId)){
        //map opportunity fields to User fields
        oppObj.Manager__c = mapUserObj.get(oppObj.OwnerId).ManagerId;
      }  
    }
  }
}