trigger SetProductFieldGroupings on Product2 (before insert, before update) 
{
    // switching through all possible entries 
    for(Product2 currentProduct : trigger.new)
    {
        // check for existence of Plan Code first
        if(currentProduct.Top200__c != NULL)
        {
            Product_Field_Groupings__c pfg = Product_Field_Groupings__c.getAll().get(currentProduct.Top200__c);
            if(pfg != null)
            {
                currentProduct.Brand__c = pfg.Product_Brand__c;
                currentProduct.Family   = pfg.Product_Family__c;
                currentProduct.SVMXC__Product_Line__c = pfg.Product_Line__c;
                currentProduct.Product_Family_Range_Grouping__c = pfg.Product_Family_Range_Grouping__c;
            }
        }
    }
}