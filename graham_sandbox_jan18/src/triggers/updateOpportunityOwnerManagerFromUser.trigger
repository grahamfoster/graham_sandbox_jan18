/*
* File Name  : updateOpportunityOwnerManagerFromUser
* Description: Trigger to update Opportunity Owner Manager in custom Manager__c field
* when Owner's Manager changed on User object level
* @author    : Evgeny Soloho
* Modification Log
================================================================================
* Ver    Date        Author                Modification 
--------------------------------------------------------------------------------
* 1.0    03/19/2012  Evgeny Soloho         Initial
*/

trigger updateOpportunityOwnerManagerFromUser on User (after update) {
    //List to hold Opportunities Ids
    List<Opportunity> lstOpps = new List<Opportunity>();
    // only fires on single User update through UI
    if (Trigger.new.size() == 1) { 
        User u = Trigger.new[0];
        User u_old = new User();
        u_old = Trigger.oldMap.get(u.Id);
        // only fires if Owner's Manager changed
        if (u.ManagerID != u_old.ManagerID) {
            //Fetch all Opprtunities whose User Id is in Trigger.new[0]
            lstOpps = [select Id, Manager__c from Opportunity where OwnerID = :u.ID Limit 50000];
                if(lstOpps.size() == 0){
                    return;  
                }
            //Update all Opprtunities whose User Id is in Trigger.new[0]    
            for (Opportunity o : lstOpps){
                if (u.ManagerID!=null) {o.Manager__c = u.ManagerID;} 
                    else {o.Manager__c = null;}
            }
            Database.update(lstOpps , false);
        }
    }
}