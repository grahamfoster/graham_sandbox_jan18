trigger OnboardingContactTrigger on OnBoarding_Contacts__c (after insert) 
{
	new OnboardingContactTriggerHandler().run();
}