/* ******************************************************************************************************************
** Module Name   : UpdatePackageRating
** Description   : Trigger to Upadte the rating field on Package according to the Ratinig Management related list
** Throws        : NA
** Calls         : NA
** Test Class    :  
** 
** Organization  : Algoworks
**
** Revision History:-
** Version             Date            Author           WO#         Description of Action
** 1.0                15/7/2012        Atulit                         Initial Version
***************************************************************************************************************** */
trigger UpdatePackageRating on Rating_Management__c (after insert, after update, after delete) {
    set<id> packageIds = new set<id>();
    List<Rating_Management__c> RatingManageList;
    
    if(trigger.isDelete){
        for(Rating_Management__c RatingManage: Trigger.old) {
            packageIds.add(RatingManage.Package__c);    
        }
    }   
    if(!trigger.isDelete){
        for(Rating_Management__c RatingManage: Trigger.New) {
            packageIds.add(RatingManage.Package__c);
        }
    }
    system.debug('packageIds....'+packageIds);
    
    List<AggregateResult> agr = [SELECT Package__c, AVG(Rating__c) avgRating FROM Rating_Management__c WHERE (Package__c IN : packageIds and Package__c != NULL) AND Rating__c > 0 GROUP BY Package__c ];
    Map<Id,Decimal> RatingCountMap = new Map<id,Decimal>(); 
    for(AggregateResult ag: agr){
        RatingCountMap.put((Id)ag.get('Package__c'),(Decimal)ag.get('avgRating'));
    }
    
    System.debug('RatingCountMap....'+RatingCountMap);
    List<Package__c> packageList = [SELECT id, Rating__c from Package__c where id in : packageIds];
    system.debug('packageList....'+packageList);
    for(Package__c pack: packageList)
    {
        pack.Rating__c = Double.valueof(RatingCountMap.get(pack.Id));       
    }
    try{
        update packageList; 
    }
    catch(Exception Ex) {if(Ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, Product Category of AB Sciex Product and Competitor Product should be same in the Package.')){string ss = Ex.getMessage().substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,',': []');trigger.new[0].addError(ss);}
          
    }   
}