trigger OracleOrderTrigger on Oracle_Order__c (after update, after insert, before insert, before update) 
{
	new OracleOrderTriggerHandler().run();
}