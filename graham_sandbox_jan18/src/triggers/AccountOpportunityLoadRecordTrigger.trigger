trigger AccountOpportunityLoadRecordTrigger on Account_Opportunity_Load_Record__c (before insert, before update) {
    
    Account_Opportunity_Load_Record__c[] lst = new Account_Opportunity_Load_Record__c[]{};
    for(Account_Opportunity_Load_Record__c aolRec : Trigger.new) {
        if(aolRec.Processing_Status__c == 'New') lst.add(aolRec);
    }

    new GLFileLoadHelper().processRecords(lst);
}