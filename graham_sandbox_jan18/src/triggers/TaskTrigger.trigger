/*
 *	TaskTrigger
 *	
 *		Trigger class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 * 
 * 	Created by Graham Foster on 2016-30-09
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

trigger TaskTrigger on Task (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new TaskTriggerHandler_2().run();
}