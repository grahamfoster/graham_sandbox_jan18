trigger campaignMemberAfter on CampaignMember (after delete, after insert, after update) 
{
	CampaignMemberHelper.updateRollupCountsOnCampaign(Trigger.new, Trigger.old, Trigger.oldMap, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete, Trigger.isUndelete);
}