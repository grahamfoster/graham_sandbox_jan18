trigger SVMX_WorkDetail_Before on SVMXC__Service_Order_Line__c (before insert, before update) 
{
	String profileName = [Select Name from Profile where Id = :UserInfo.getProfileId() limit 1].get(0).Name;

    if (!SVMX_WorkDetailRecursiveSaveHelper.isAlreadyRunPreWorkDetails() || System.Test.isRunningTest()) 
    {
        SVMX_WorkDetailRecursiveSaveHelper.setAlreadyRunPreWorkDetails();

		if (!(profileName.equalsIgnoreCase('Informatica')))
		{
			SVMX_WorkDetail_Helper.checkProductStock(Trigger.new, Trigger.oldMap);
			SVMX_WorkOrder_Helper.setupTravelLines(Trigger.new, Trigger.oldMap);
		}
		
	}
}