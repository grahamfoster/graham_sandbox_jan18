trigger CustomerOnboardTrigger on Customer_Onboard__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) 
{
	new CustomerOnboardTriggerHandler().run();
}