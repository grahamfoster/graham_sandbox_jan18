trigger SVMX_WorkOrder_After on SVMXC__Service_Order__c (after insert, after update) {

    if (!SVMX_WorkOrderRecursiveSaveHelper.isAlreadyRunPostWorkOrder() || System.Test.isRunningTest()) 
    {
        SVMX_WorkOrderRecursiveSaveHelper.setAlreadyRunPostWorkOrder();
		SVMX_WorkOrder_Helper.performLocationBasedEntitlement(Trigger.new);
	}

}