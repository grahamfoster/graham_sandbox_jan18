/*	UserTrigger
 *	
 *		Trigger class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 * 
 * 	Created by Daniel Lachcik on 2017-06-28
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
trigger UserTrigger on User (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new UserTriggerHandler().run();
}