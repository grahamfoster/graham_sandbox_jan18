trigger LMSCourseEnrollmentTrigger on LMS_Course_Enrollment__c (after insert, after update) 
{
	new LMSCourseEnrollmentTriggerHandler().run();
}