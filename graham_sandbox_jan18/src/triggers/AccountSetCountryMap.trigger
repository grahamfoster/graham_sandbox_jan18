trigger AccountSetCountryMap on Account (before insert,before update) {
//DELETE ME
System.debug('Init');    
    public static List<Country_Mapping__c> allCountries = new List <Country_Mapping__c>([Select Name, id,Permutations__c, Country_Code__c from Country_Mapping__c]);
    public Map<String,Country_Mapping__c> countryAlias = new Map<String,Country_Mapping__c>();  //Map of (Permutation), (Country_Mapping__C)    

    public ID countryID;
    public String countryName;
     
System.debug('START');
        // create map of (Permutation), (Country_Mapping__C Id)
        for(Country_Mapping__c country :allCountries){
            // add country code, we will be treating it the same way as permutations
            countryAlias.put(country.Country_Code__c.toUpperCase(),country);

            // split all permutations
            if(!String.isBlank(country.Permutations__c)){
                if(country.Permutations__c.contains(';')){
                    for(String alias : country.Permutations__c.split(';')) {
                        if(!String.isBlank(alias.trim())) 
                            countryAlias.put(alias.trim().toUpperCase(),country);
                    }
                } else {
                            countryAlias.put(country.Permutations__c.trim().toUpperCase(),country);
                }
            }       
        }
        for(Account record : Trigger.new){
            if(record.BillingCountry != null){  
                countryID = findCountry(record.BillingCountry.trim().toUpperCase());
                if(countryID != NULL){   //Country FOUND --> Populate the Country and Country Mapping fields
                    record.Country_Mapping__c = countryID;
                    countryName = findCountryName(record.BillingCountry.trim().toUpperCase());
                    if(String.isNotBlank(countryName)){
                    record.BillingCountry = countryName;
                    }
                } 
                if(countryID == NULL){ // Country NOT FOUND --> NULL out the Country and Country Mapping fields
                record.Country_Mapping__c = NULL;
                record.BillingCountry = NULL;
                }
                
            }
            if(record.BillingCountry == null){  // Country BLANK --> NULL out the Country and Country Mapping fields
                record.Country_Mapping__c = NULL;
                record.BillingCountry = NULL;
            }
        }         
System.debug('STOP');        
    
    public ID findCountry(String s){
        if(countryAlias != null && countryAlias.containsKey(s)){
            return countryAlias.get(s).id;

        } else {
            return null;
        }
    }
    
    public String findCountryName(String s){
        if(countryAlias != null && countryAlias.containsKey(s)){
            return countryAlias.get(s).name;
        } else {
            return null;
        }
    }

}