/*
 *	CaseContactTrigger
 *	
 *		Trigger class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 * 
 * 	Created by Graham Foster on 2017-01-06
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

trigger CaseContactTrigger on Case_Contact__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new CaseContactTriggerHandler().run();
}