trigger RAPID_SVMXC_BeforeInsertUpdate_Update_Location on SVMXC__Installed_Product__c (before insert, before update) {

    String profileName = [Select Name from Profile where Id = :UserInfo.getProfileId() limit 1].get(0).Name;

    set<id> locationIds = new set<id>();
    SVMXC__Installed_Product__c[] productsToProcess = new
    SVMXC__Installed_Product__c[]{};
    
//    if (!(profileName.equalsIgnoreCase('Informatica')))      
//      {
      
       if (trigger.isinsert) {
         for (SVMXC__Installed_Product__c p : trigger.new) {
           if (p.SVMXC__site__c != null) {
             locationIds.add(p.SVMXC__site__c);
             productsToProcess.add(p);
           }
         }
       } else if (trigger.isupdate) {
         for (SVMXC__Installed_Product__c p : trigger.new) {
           if (trigger.oldmap.get(p.id).SVMXC__site__c !=trigger.newmap.get(p.id).SVMXC__site__c) 
              {
             if (p.SVMXC__site__c== null) {
               p.SVMXC__street__c = '';
               p.SVMXC__city__c = '';
               p.SVMXC__state__c = '';
               p.SVMXC__zip__c = '';
               p.SVMXC__country__c = '';
             } else {
               locationIds.add(p.SVMXC__site__c);
               productsToProcess.add(p);
             }
           }
         }
       }

       map<id, SVMXC__Site__c> locationMap = new map<id, SVMXC__Site__c>([select id, SVMXC__street__c, SVMXC__city__c, SVMXC__state__c, SVMXC__zip__c, SVMXC__country__c from SVMXC__Site__c where id in :locationIds]);

       for (SVMXC__Installed_Product__c p : productsToProcess) {
         p.SVMXC__street__c = locationMap.get(p.SVMXC__site__c).SVMXC__street__c;
         p.SVMXC__city__c = locationMap.get(p.SVMXC__site__c).SVMXC__city__c;
         p.SVMXC__state__c = locationMap.get(p.SVMXC__site__c).SVMXC__state__c;
         p.SVMXC__zip__c = locationMap.get(p.SVMXC__site__c).SVMXC__zip__c;
         p.SVMXC__country__c =
         locationMap.get(p.SVMXC__site__c).SVMXC__country__c;
       }
//     }

}