/*
 *	AccountTrigger
 *	
 *		Trigger class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 * 
 * 	Created by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
trigger AccountTrigger on Account (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new AccountTriggerHandler().run();
}