trigger SVMX_WorkOrder_Before on SVMXC__Service_Order__c (before insert, before update) {

    if (!SVMX_WorkOrderRecursiveSaveHelper.isAlreadyRunPreWorkOrder() || System.Test.isRunningTest()) 
    {
        SVMX_WorkOrderRecursiveSaveHelper.setAlreadyRunPreWorkOrder();
		SVMX_WorkOrder_Helper.checkStopEntitlement(Trigger.new);
	}

}