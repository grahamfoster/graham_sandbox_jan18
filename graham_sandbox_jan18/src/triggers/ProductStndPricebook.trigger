trigger ProductStndPricebook on Product2 (after insert) {
   
/* 
 * There are currently 39 Price Books in system, only one of them is standard.
 * There are currently 26 currency types in system.
 * For users to be able to add a Price Book to a Product with a specific currency
 * type, the product should be connected to the standard Price Book with this
 * specific currency type that user wants to use; For users to be able to connect
 * with any currency type, the product should be connected to the standard Price
 * Book with all 26 currency types.
 *
 * This trigger is to make sure when ONLY a SysAdmin OR Informatica user inserts a product, 
 * the product will be connected to the standard Price Book with all 26 currency types.
 * 
 * A product is connected to a Price Book by PricebookEntry.
 */

	List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
	String MyProfileName = PROFILE[0].Name;    
    
    //New Group to allow for non Admin users to be able to create Product Bundles with all currencies automatically created
    Group PrdMgr = [Select ID, Name FROM Group WHERE Name = 'Product Managers' LIMIT 1];
    List<GroupMember> GrpMember = [Select ID, GroupID, UserorGroupID from GroupMember 
                                   where GroupID =: PrdMgr.ID and UserorGroupID =:userinfo.getUserId() LIMIT 1]; 
    
    if(MyProfileName == 'Informatica' || GrpMember.size() > 0)  {  
	
        
    //System.debug('Trigger start: '+System.Now());

	// Find standard price book. There should be only one. Id = 01sA0000000Pat0IAC
	String standardPBId = [Select Id From Pricebook2 where IsStandard = true][0].Id;

	// get Currency Types
	CurrencyType[] activeCT = [SELECT Id,IsActive,IsoCode FROM CurrencyType WHERE IsActive=TRUE ORDER BY IsoCode];
    
	// define PricebookEntry list
	List<PricebookEntry> toInsert = new List<PricebookEntry>();

	// -----------------------------------------------------
	// Add stardard PricebookEntry
	// -----------------------------------------------------
	// Trigger.New contains all the records that were inserted in insert or update triggers.
	for(Product2 p : Trigger.New) {
		// for each new product, loop through currencytypes, create pricebookentry 
		for (CurrencyType ct : activeCT) {
				toInsert.add(new PricebookEntry (
				CurrencyIsoCode = ct.IsoCode,
				Product2Id = p.Id, 
				Pricebook2Id = standardPBId,
				IsActive = True, 
				UnitPrice = 0 
				));
		}
	}
	//System.debug('isInsert ='+toInsert.size());
	
	// -----------------------------------------------------
	// Insert
	// -----------------------------------------------------
	if (toInsert.size() > 0) {
		insert toInsert;
	}
	//System.debug('Trigger end: '+System.Now());
    }
	
}