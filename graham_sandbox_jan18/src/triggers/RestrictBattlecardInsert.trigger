/* ******************************************************************************************************************
** Module Name   : RestrictBattlecardInsert
** Description   : Trigger to restrict user from creating more than one battle card in same package
** Throws        : NA
** Calls         : NA
** Test Class    :  
** 
** Organization  : Algoworks
**
** Revision History:-
** Version             Date            Author           WO#         Description of Action
** 1.0                15/7/2012        Amardeep                         Initial Version
***************************************************************************************************************** */
trigger RestrictBattlecardInsert on Battlecard__c (before insert) 
{
	/*
    set<id> packageids = new set<id>(); 
    set<id> Battlecardids = new set<id>();
    List<Battlecard__c> battlecardList;
    for(Battlecard__c battlecard :trigger.New)
    {
        packageids.add(battlecard.Package__c);
        Battlecardids.add(battlecard.id);
    }
    if(!packageids.isEmpty())   {
        
        battlecardList=[SELECT id, Package__c from Battlecard__c where Package__c in :packageids];  
    }
    if(!battlecardList.isEmpty())
    { 
        for(id pckg: packageids)
        { 
            for(Battlecard__c battle: battlecardList)
            {
                if(pckg == battle.Package__c)
                {
                    Trigger.new[0].addError('You Cannot create more than one battlecard in the same Package');
                }
                
            }
        }
    }
    
     Trigger.new[0].addError('You Cannot create more than one battlecard in the same Package');*/
}