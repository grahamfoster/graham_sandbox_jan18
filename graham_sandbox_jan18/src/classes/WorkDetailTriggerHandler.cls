/*
 *	WorkDetailTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2016-05-20 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class WorkDetailTriggerHandler extends TriggerHandler {

    public WorkDetailTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void beforeInsert() {
		linkToCase();
        findPartPrice();
	}
	override protected void beforeUpdate() {
		linkToCase();        
		findPartPrice();
	}
    override protected void afterInsert() {

	}
	override protected void afterUpdate() {

	}
	override protected void afterUndelete() {
    
	}
	override protected void afterDelete() {
    
	}

    public void linktoCase(){
        // Loop through all records and collect Parent Work Order IDs
        List <ID> woIds = new List <ID>();
        For( SVMXC__Service_Order_Line__c debrief : (List<SVMXC__Service_Order_Line__c>)Trigger.new){
            woIds.add(debrief.SVMXC__Service_Order__c);            
        }
        // Query all needed WOs
        Map <Id, SVMXC__Service_Order__c> workOrders = new Map <Id,SVMXC__Service_Order__c>([Select id, SVMXC__Case__c FROM SVMXC__Service_Order__c WHERE id IN :woIds]);
		        
		// Loop through all records and update the SVMXC__Case__c field
        For( SVMXC__Service_Order_Line__c debrief : (List<SVMXC__Service_Order_Line__c>)Trigger.new){
           debrief.SVC_Case__c = workOrders.get(debrief.SVMXC__Service_Order__c).SVMXC__Case__c;           
        }     
    }
    
    public  void findPartPrice(){
        // Loop through all records and collect Part and Pricebook IDs
        List <ID> pbIds = new List <ID>();
        List <ID> partIds = new List <ID>();
        For( SVMXC__Service_Order_Line__c debrief : (List<SVMXC__Service_Order_Line__c>)Trigger.new){
            if(debrief.Line_Sub_Type__c == 'Material'){            
                pbIds.add(debrief.FSH_Part_Pricebook__c);
                partIds.add(debrief.SVMXC__Product__c);
            }           
        }
        if(pbIds.size() > 0 && partIds.size() >0){
            // Query all needed PBEs         
        	List <PriceBookEntry> PBEs = new List <PriceBookEntry>([SELECT id, PRICEBOOK2ID,CURRENCYISOCODE,PRODUCT2ID, NAME,UNITPRICE, ProductCode FROM PriceBookEntry WHERE PRICEBOOK2ID IN :pbIds AND PRODUCT2ID IN :partIds]);           
            // Loop for each trigger record
            For( SVMXC__Service_Order_Line__c debrief : (List<SVMXC__Service_Order_Line__c>)Trigger.new){
                // find correct PBE
                for(PricebookEntry Entry :PBEs){ 
                    if(Entry.Pricebook2Id == debrief.FSH_Part_Pricebook__c && Entry.CurrencyIsoCode == debrief.CurrencyIsoCode && Entry.Product2Id == debrief.SVMXC__Product__c){
						debrief.SVMXC__Actual_Price2__c = Entry.UnitPrice;                        
                    }
                }
            }
        }
    }
}