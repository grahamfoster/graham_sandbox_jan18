public class QuoteLineEmailController {

    public id QuoteID {get;set;}

public SBQQ__Quote__c getQuotee() {
    return QuoteId == null? null:
        [Select Name, ID, Subtotal_Before_Trade_In__c, Trade_In__c, Total_After_Trade_In__c, Opportunity_ID_Number__c,
                    Special_Incentive_Total__c, SBQQ__NetAmount__c, Net_Amount_Internal__c,
                    Internal_External_Difference__c, Approved__c, CurrencyIsoCode
         FROM SBQQ__Quote__c 
         WHERE ID = :QuoteID]; 
             
}    
    
public list<SBQQ__QuoteLine__c> getQuoteLines() {

        list<SBQQ__QuoteLine__c> QLI = [SELECT  Name, SBQQ__ProductCode__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, SBQQ__Bundled__c,
                                          Discounts__c, SBQQ__NetPrice__c, SBQQ__NetTotal__c, Internal_Discounts__c, Internal_Discount_Type__c, CurrencyIsoCode, 
                                          Net_Unit_Price_Internal__c, SBQQ__Optional__c, Net_Total_Internal__c, Internal_External_Difference__c, Tier__c,
                                          Trade_In_Discount_Grouping__c, Order_Admin_Description__c, Product_Name_and_Description__c, SBQQ__Number__c
                                        FROM SBQQ__QuoteLine__c 
                                        WHERE SBQQ__Quote__c = :QuoteID ORDER by SBQQ__Number__c];

        return QLI;                    
        
//        List<SBQQ__QuoteLine__c> result = [SELECT     Name, SBQQ__Optional__c, Trade_In_Discount_Grouping__c
//                                          FROM SBQQ__QuoteLine__c 
//                                        WHERE SBQQ__Quote__c = :QuoteID ORDER by Name];
//      return result;    
    }
   
public Boolean showTradeInSection {
    get {
      List<SBQQ__QuoteLine__c> all_qli = getQuoteLines();
      Boolean result = false;
      for (SBQQ__QuoteLine__c qli : all_qli) {
        if (qli.Trade_In_Discount_Grouping__c == 'Additional Discounts') {
          result = true;
          break;
        }
      }
      return result;
    }
    private set;
  }

public Boolean showOptionalSection {
        get {
      List<SBQQ__QuoteLine__c> all_qli = getQuoteLines();
      Boolean result = false;
      for (SBQQ__QuoteLine__c qli : all_qli) {
        if (qli.SBQQ__Optional__c == TRUE) {
          result = true;
          break;
        }
      }
      return result;
    }
    private set;
  }   
    
public list<SBQQ__QuoteLine__c> getCSVQuoteLines() {

        list<SBQQ__QuoteLine__c> CSVQLI = [SELECT   Name, SBQQ__ProductCode__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, Order_Number__c, 
                                        Price_List_Name__c, PRICE_MODF_NAME2__c, PRICE_MODF_NUMBER2__c, 
                                        User_Description__c, Internal_Discount__c, SBQQ__Number__c, Discount_for_Approvals__c
                                        FROM SBQQ__QuoteLine__c 
                                        WHERE (NOT SBQQ__ProductCode__c LIKE '%Bundle') 
                                           AND SBQQ__Quote__c = :QuoteID ORDER by SBQQ__Number__c];

        return CSVQLI;                     
    }
    
}

    
// ORDER ADMIN TEMPLATE LINE ITEM HEADERS    
// Part # (Product Code)                SBQQ__ProductCode__c
// Description                          Product_Name_and_Description__c
// QTY                                  SBQQ__Quantity__c
// Unit Price                           SBQQ__ListPrice__c
// Disc. (Discount) CUSTOMER FACING     Discounts__c
// Net Price        CUSTOMER FACING     SBQQ__NetPrice__c
// Extended         CUSTOMER FACING     SBQQ__NetTotal__c
// Disc. (number)   Internal            Internal_Discounts__c
// Type (% or AMT)  Internal            Internal_Discount_Type__c
// Net Price        Internal            Net_Unit_Price_Internal__c
// Extended         Internal            Net_Total_Internal__c
// Int/Ext Diff.                        Internal_External_Difference__c
// Tier                                 Tier__c