/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 29/2014
**
** Test coverage for ServiceOpportunitySearchController
** 
**/
@isTest(SeeAllData=true)
public class ServiceOpportunitySearchTests {
	private static Pricebook2 pricebook;
	private static List<PricebookEntry> pricebookEntries;
	private static List<PricebookEntry> testPricebookEntries;
	
	public static testMethod void test1_results() {
		setUp();
		ServiceOpportunitySearchController c = new ServiceOpportunitySearchController();
		c.serialNumber = 'X';
		c.doSearch();
		boolean b = c.displayResultsPageBlock;
		Pager p = c.pager;
	}

	public static testMethod void test2_results_sort() {
		setUp();
		ServiceOpportunitySearchController c = new ServiceOpportunitySearchController();
		c.serialNumber = 'X';
		c.doSearch();
		c.setPageNumber(1);
		c.setPageSize(50);
		integer p = c.getPageSize();
		p = c.getPageNumber();
		c.setSortColumn1('Name');
		c.setSortDirection1('asc');
		c.sortAction();
		c.previousAction();
		c.nextAction();

		c.getReportOpps();
		c.getReport();
		c.getTotalNumberOfRecords();
		c.getNumberOfPages();
		c.getHasNextPage();
		c.getHasPreviousPage();
		c.getSortColumn1();
		c.getSortDirection1();
		c.getSortColumn2();
		c.getSortDirection2();
		c.setSortColumn2('Name');
		c.setSortDirection2('asc');
	}

	public static testMethod void test3_err_no_criteria() {
		setUp();
		ServiceOpportunitySearchController c = new ServiceOpportunitySearchController();
		c.doSearch();
	}

	public static testMethod void test4_err_no_results() {
		setUp();
		ServiceOpportunitySearchController c = new ServiceOpportunitySearchController();
		c.serialNumber = 'Y';
		c.doSearch();
	}

	private static void setUp() {
		createTestPricebook();
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;

        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'USD', 
        	StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;

        OpportunityLineItem[] lineItems = new OpportunityLineItem[]{
        	new OpportunityLineItem(OpportunityId = opp.Id, UnitPrice = 1000, Quantity = 1, Serial_Number__c = 'X123', PricebookEntryId = testPricebookEntries[0].Id)
        };
        insert lineItems;
	}

    private static void createTestPricebook(){
        Pricebook2 stdPricebook = [select Id from Pricebook2 where IsStandard = true limit 1];

        pricebook = new Pricebook2(Name = 'Test Price Book', CurrencyIsoCode = 'USD', IsActive = true);
        insert pricebook; 
        
        List<Product2> testProducts = new List<Product2> {
            new Product2(Name = 'Test product 1', ProductCode = 'test-1', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 2', ProductCode = 'test-2', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 3', ProductCode = 'test-3', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 4', ProductCode = 'test-4', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 5', ProductCode = 'test-5', IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert testProducts;
        
        pricebookEntries = new List<PricebookEntry>();
        for(Product2 tp : testProducts) {
            pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
        }
        insert pricebookEntries;

        testPricebookEntries = new List<PricebookEntry>();
        for(Product2 tp : testProducts) {
            testPricebookEntries.add(new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
        }
        insert testPricebookEntries;
    }

}