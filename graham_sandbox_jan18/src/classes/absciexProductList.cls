public with sharing class absciexProductList {

   List<Product_Category__c> prcat {get; set;}
   List<Product2> pro {get; set;}
   Set<Id> prcId = new Set<Id>();
   public string procId {get; set;}
   public map<String,list<ProdWrapper>> productMap{get;set;}
   public string selectedLevel1 {get; set;}
   public boolean clickedCell {get; set;}
   public string ProductImage {get; set;}
   public string ProductName {get; set;}
   public string prodcatId {get; set;}
   public string productFamily {get; set;}
    
   public absciexProductList(){
        clickedCell = false;
        prcat = new List<Product_Category__c>(); 
        prcat = [select Id, Name, Product_Category_Name__c from Product_Category__c Where isActive__c =: true Order By Product_Category_Name__c];
        System.debug('>>>>>'+prcat);
        for(Product_Category__c pr: prcat){
            prcId.add(pr.Id);
        }
        System.debug('>>>>>'+prcID);
        
        /*List<Manufacturer__c> mfr = [Select Id, Name From Manufacturer__c Where Name =: 'AB Sciex' AND isActive__c =: true];
        pro = new List<Product2>();
        if(mfr.size()>0){
            
            pro = [Select Id, Name, Product_Category__c, Image__c From Product2 Where isActive =: true AND Manufacturer__c =: mfr[0].Id AND Product_Category__c In: prcId];
        
        } */
        pro = [Select Id, Name, Family, Product_Category__c, Image__c From Product2 Where isActive =: true AND (RecordType.Name =: 'AB Sciex' OR RecordType.Name =: 'NA PSM')AND Product_Category__c In: prcId];
        
        productMap = new map<String,list<ProdWrapper>>();
        for(Product_Category__c pr: prcat){
            list<ProdWrapper> prodList = new list<ProdWrapper>();
            for(Product2 prod : pro){ 
               if(pr.Id == prod.Product_Category__c ){
                   prodWrapper proWrap = new prodWrapper();
                   proWrap.ProductCategoryName = pr.Product_Category_Name__c; 
                   proWrap.ProductName = prod.Name;
                   proWrap.ProductImage = prod.Image__c;
                   proWrap.ProductCategId = pr.Id;
                   proWrap.productId = prod.Id;
                   proWrap.Family = prod.Family;
                   prodList.add(prowrap);
               }
            }
                productMap.put(pr.Product_Category_Name__c,prodList); 

        }
   }
   
   public void parentpage(){
      clickedcell = true;
      System.debug(procId); 
      System.debug(clickedCell);
      //return null; 
      //pagereference pageref = new pagereference('/apex/absciexProductList?id='+procId);
      //pageref.setredirect(true);
      //return pageref;
         
   }
  
    public class ProdWrapper {
        public String ProductCategoryName{get;set;}
        public String ProductName{get;set;}
        public String ProductImage{get;set;}
        public Id ProductCategId{get;set;}
        public Id productId{get;set;}
        public String Family{get;set;}
    }
   
   
}