public class SMC_Merge_Opps {
    
    public List<OpportunityLineItem> mergeLines;
    public List<Opportunity> delOpps; 
   
	public SMC_Merge_Opps() {  

system.debug('******************** - START');        
        Map<Id,Opportunity> OppMap = new Map<Id,Opportunity>([SELECT id, LastModifiedBy.id, CreatedBy.id, Contract_Number__c, Previous_Contract__c FROM Opportunity WHERE Previous_Contract__c != '' AND CreatedDate = LAST_90_DAYS ORDER BY Contract_Number__c]);
        Map<String,ID> existingOpps = new Map<String,ID>();			// Parent Contract#, Existing Opportunity ID		
        Map<ID,ID> dupOpps = new Map<ID,ID>();						// ID of Oportunity entries removed, ID of the Opportunity to be used instead
		this.mergeLines = new List<OpportunityLineItem>();
        this.delOpps = new List<Opportunity>();
        
        for(Opportunity o :OppMap.values()){
            if(o.LastModifiedById == o.CreatedById){
            	if(!existingOpps.containsKey(o.Contract_Number__c) ){
               		existingOpps.put(o.Contract_Number__c, o.Id);
            	} else {
system.debug('******************** - dupe');                    
               		dupOpps.put(o.id,existingOpps.get(o.Contract_Number__c));
                    delOpps.add(o);
            	}
            }
        }
        
        Map<Id,OpportunityLineItem> OppLineMap = new Map<Id,OpportunityLineItem>([SELECT id, OpportunityId, ProductCode, Quantity, Type__c, Serial_Number__c, Model__c, PricebookEntryId, UnitPrice   From OpportunityLineItem WHERE OpportunityId In :dupOpps.keySet() ]);
        
        for(OpportunityLineItem ol :OppLineMap.values()){
            if(dupOpps.containsKey(ol.OpportunityId)){
                OpportunityLineItem tempOpp = new OpportunityLineItem(OpportunityId = dupOpps.get(ol.OpportunityId),Quantity = ol.Quantity, Type__c = ol.Type__c, Serial_Number__c = ol.Serial_Number__c, Model__c = ol.Model__c, PricebookEntryId = ol.PricebookEntryId, UnitPrice = ol.UnitPrice);    
                mergeLines.add(tempOpp);
            }
        }
system.debug('******************** - START DML');		
 		        insert mergeLines;
                delete delOpps;

system.debug('******************** - END');
    }
}