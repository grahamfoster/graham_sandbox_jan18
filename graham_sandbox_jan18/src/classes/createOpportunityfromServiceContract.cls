/**
 ** @author Brett Moore
 ** @created Nov 24/2015 
 ** @Revision  Dec 22/2015 - Complete Re-write
 ** @Last Revision Feb 17/2016 - Owner Exception handling for CE Opportunities
 ** Class to generate contract Opportunities based on expiring contacts
**/

public class createOpportunityfromServiceContract {
    private Map <ID,SVMXC__Service_Contract__c > ContractMap;                       // All Queried Service/Maintenance Records
    private Map<ID,SVMXC__Service_Contract_Products__c  >  ContractProductMap;      // All Queried Service/Maintenance Line Item Records
    private Map <ID,Opportunity> OpportunityMap;                                    // Ongoing Map of all created Opportunities
    private List <OpportunityLineItem> LineItem;                                    // list of all Line Items (accross all created Opportunities)
    private List<SVMXC__Service_Contract__c> completedSMCs;                         // List of Service Contracts that have been successfully run
    private Map<Id,Id> AccountPricebookMap;                                         // Account ID and Assoc. PriceBook ID
    private Map<Id,Id> AccountSalesRepMap;                                          // Account ID and Assoc. Sales Rep (User) ID
    private Map<Id,Id> AccountCESalesRepMap;                                        // Account ID and Assoc. CE Sales Rep (User) ID
    private Map<String,c2oSettings__c> settingsMap;
    private Map<String,c2oExceptions__c> CE_Exceptions;
    private Map<String,c2oExceptions__c> NAM_Exceptions;
    private Map<ID,Account> accountNames;
    private Map<ID,SVMXC__Installed_Product__c> InstalledProducts;
    private Map<ID,String> ProductCodes;
    private Map <ID,Map<Id, PricebookEntry>> PBEbyPB;
    private List <Opportunity> reparentOpps;										// For reparenting Opportunity owner Exceptions    
    private Map<ID,ID> reparentSMCLine;												// ID of the original Line Item Contract, ID of the new Line Item Contract        

    
    public createOpportunityfromServiceContract(Map<Id, SVMXC__Service_Contract__c> initialQuery) {    
        // Initialize Data maps
        this.settingsMap = new Map<String,c2oSettings__c>();  // <Contract_Type , Object>
       	this.CE_Exceptions = new Map<String,c2oExceptions__c>();
        this.NAM_Exceptions = new Map<String,c2oExceptions__c>();
        customSettings(settingsMap);
        this.ContractMap = initialQuery;
        this.ContractProductMap = new Map<ID, SVMXC__Service_Contract_Products__c  >([SELECT Id, SVMXC__Service_Contract__c, SVMXC__PRODUCT__C, SVC_ITEM_MASTER_NAME__C,SVMXC__INSTALLED_PRODUCT__C, SVMXC__INSTALLED_PRODUCT__R.name from SVMXC__Service_Contract_Products__c WHERE isDeleted = FALSE AND SVMXC__Service_Contract__c in :ContractMap.keySet()]);
        // Contracts created on the previous Pricebook object 'ABSX WARRANTY' need to be updated to the new product entries
        Map<Id,Product2> Product2ids =new Map<ID, Product2>([SELECT id, ProductCode FROM Product2 WHERE ProductCode LIKE 'ABSX WARRANTY' OR ProductCode LIKE 'ABSX ASSURANCE 0PM' OR ProductCode LIKE 'ST 8X5']);
        Id oldMaster;   // ID of legacy Product
        Id altMaster;   // ID of replacement Product
// Feb 29 edit
        ID CEMaster;    // ID of replacement Product for CE
        for (Product2 p : Product2ids.values()){
            if(p.ProductCode == 'ABSX WARRANTY'){
                oldMaster=p.id;
            }else if (p.ProductCode == 'ABSX ASSURANCE 0PM') {
                altMaster=p.id;
            } else {
                CEMaster=p.Id;
            }
        }
// Feb 29 edit          
        List<ID> MasterItems = new List<ID>();
        List<ID> InstalledProductNames = new List <ID>();
        for(SVMXC__Service_Contract_Products__c SMCP :ContractProductMap.values()){         // Get IDs for Contract Products and Line Items          
// system.debug('!!!!!!!!!!!!!!!!!!!!!!- ' + SMCP.SVMXC__INSTALLED_PRODUCT__r.name );          
            if(SMCP.SVC_Item_Master_Name__c == OldMaster){
				String Model = 'None';
                If( string.isNotBlank(SMCP.SVMXC__INSTALLED_PRODUCT__r.name)){
                    String fullString = SMCP.SVMXC__INSTALLED_PRODUCT__r.name;
                	Model = fullString.substringBefore('-').trim();             
                }          
                if(CE_Exceptions.containsKey(model)){
					SMCP.SVC_Item_Master_Name__c = CEMaster;
                	ContractProductMap.put(SMCP.id,SMCP);
                } else {
                	SMCP.SVC_Item_Master_Name__c = altMaster;
                	ContractProductMap.put(SMCP.id,SMCP);
            	}
            }
            InstalledProductNames.add(SMCP.SVMXC__Installed_Product__c);
            MasterItems.add(SMCP.SVC_Item_Master_Name__c);              
        }    
        List<Id> AccountList = new List<ID>();
        for(SVMXC__Service_Contract__c SMC :ContractMap.values()){                          // Get IDs for Contract Products and Line Items
            AccountList.add(SMC.SVMXC__Company__c);
        }      
        this.accountNames = new Map<ID,Account>([SELECT Id, Name,CurrencyISOCode from Account WHERE Id IN :AccountList]); 
        this.InstalledProducts = new Map<ID,SVMXC__Installed_Product__c>([SELECT id, Name FROM SVMXC__Installed_Product__c WHERE id IN:InstalledProductNames ]);
        Map<ID,ID> OT2A = new Map<ID,ID>(getOT2A(AccountList));
        Map<ID,Map<String,ID>> Territories = new Map<ID,Map<String,ID>>(getTerritories(OT2A.values())); 
        this.AccountPricebookMap = new Map<Id,Id>();
        this.AccountSalesRepMap = new Map<Id,Id>();
        this.AccountCESalesRepMap = new Map<Id,Id>();
        // Map Pricebooks 
        // Use Object-to-Territory map to determine correct Pricebook and Owner for each Opportunity
        for(ID  O2T : OT2A.Keyset()){
            if(String.isNotEmpty(Territories.get(OT2A.get(O2T)).get('Default_Service_Pricebook__c'))) AccountPricebookMap.put(O2T,Territories.get(OT2A.get(O2T)).get('Default_Service_Pricebook__c' ) );
            if(String.isNotEmpty(Territories.get(OT2A.get(O2T)).get('Default_Service_Sales_Rep__c'))) AccountSalesRepMap.put(O2T,Territories.get(OT2A.get(O2T)).get('Default_Service_Sales_Rep__c'));
            if(String.isNotEmpty(Territories.get(OT2A.get(O2T)).get('DEFAULT_CE_SERVICE_SALES_REP__C'))) AccountCESalesRepMap.put(O2T,Territories.get(OT2A.get(O2T)).get('DEFAULT_CE_SERVICE_SALES_REP__C'));
        }
        Map<ID,PricebookEntry> PBEMap = new Map <ID,PricebookEntry>([SELECT id, Name, PRICEBOOK2ID, PRODUCTCODE, CurrencyISOCode,PRODUCT2ID, UnitPrice, ISACTIVE FROM PricebookEntry WHERE  (ISACTIVE = TRUE AND PRICEBOOK2ID  IN :AccountPricebookMap.values() AND PRODUCTCODE LIKE '%|%') OR (ISACTIVE = TRUE AND PRICEBOOK2ID  IN :AccountPricebookMap.values() AND PRODUCT2ID IN :MasterItems)]);
        this.ProductCodes = new Map<ID,String>();
        this.PBEbyPB = new Map <ID,Map<Id, PricebookEntry>>();
        for(PricebookEntry Entry :PBEMap.values()){                                         // Map of all Pricebook Entries needed
            ProductCodes.put(Entry.Product2Id,Entry.ProductCode);
            if(PBEbyPB.get(Entry.Pricebook2id) == null) {               
                PBEbyPB.put(Entry.Pricebook2Id, new Map<Id, PricebookEntry>{Entry.id => Entry});
            }
            else {                                
                PBEbyPB.get(Entry.Pricebook2id).put(Entry.id, Entry);
            }
        }
        this.OpportunityMap = new Map < ID , Opportunity>();
        this.LineItem = new List <OpportunityLineItem>();      
        this.completedSMCs = new List <SVMXC__Service_Contract__c>();  
 
        //
        // Begin Opportunity Creation
        // 
        For(SVMXC__Service_Contract__c SMC :ContractMap.values()){                  // Iterate through each Service Contract
            CreateOpp(SMC);
        }
        this.reparentOpps = new List<Opportunity>();
        if(!OpportunityMap.isEmpty()){
           try{
                insert OpportunityMap.values();    
                if(!completedSMCs.isEmpty()) update completedSMCs;
            } catch(DmlException e) {
               System.debug('!@! ***The following exception has occurred: ' + e.getMessage());
                System.debug('!@! ***Opportunities: ' + OpportunityMap);
               for(Integer i=0; i< e.getNumDml();i++){
                    System.debug('!@! ***Record: ' + e.getDmlId(i));
                    System.debug('!@! ***Field: ' + e.getDmlFieldNames(i));
               }               
            }
        } 
        For(ID oppKey :OpportunityMap.keySet()){                                    // Iterate through each Contract Line Item
            CreateOppProduct(oppKey, ContractMap.get(oppKey).SVC_CATEGORY__C);  
        }
        if(!LineItem.isEmpty()) insert LineItem; 
        set<Opportunity> tempSet = new set<Opportunity>();
        list<Opportunity> result = new list<Opportunity>();
        tempSet.addAll(reparentOpps);
        result.addAll(tempSet);
        if(!result.isEmpty()) update result;
    }


    private void customSettings(Map<String, c2oSettings__c> s){    //   Query all custom settings, and populate them as a Map <Contract_Type , Object>
        List<c2oSettings__c> allsettings = new List<c2oSettings__c>([SELECT Id, Name, Contract_Type__c, Naming_Convention__C, Product_Type__c, RecordType__c, Stage_Name__c FROM c2oSettings__c WHERE isDeleted = FALSE ]);  
        List<c2oExceptions__c> allExceptions = new List<c2oExceptions__c>([SELECT Id, Name, Exception_Type__c, SalesRepID__c FROM c2oExceptions__c WHERE isDeleted = FALSE ]);  
        for (c2oSettings__c i :allsettings){
            s.put(i.Contract_Type__c,i);
        }
        for(c2oExceptions__c ii :allExceptions){
            if(ii.Exception_Type__c == 'CE'){
                CE_Exceptions.put(ii.Name, ii);
            }else{
 //               NAM_Exceptions.put(ii.Name, ii);
            }
        }
    }   
    
    private Map<ID,ID> getOT2A(List<ID> AccountList) {    
        List<ObjectTerritory2Association> OT2AList = new List<ObjectTerritory2Association>([SELECT ObjectId, Territory2Id FROM ObjectTerritory2Association WHERE ObjectId IN :AccountList]);
        Map<ID,ID> OT2A = new Map<ID,ID>();
        For (ObjectTerritory2Association x :OT2AList){
            OT2A.put(x.ObjectID, x.Territory2Id);
        }
        return OT2A;
    }
   
    public Map<ID,Map<String,ID>>  getTerritories(List<ID> TerritoryList) {    
        List<Territory2> TerList = new List<Territory2>([SELECT id, Name, Default_Service_Pricebook__c, Default_Service_Sales_Rep__C, Default_CE_Service_Sales_Rep__C FROM Territory2 where id IN :TerritoryList ]);
        Map<ID,Map<String,ID>> Territories = new Map<ID,Map<String,ID>>();
        if (!Test.isRunningTest()){
            For (Territory2 y :TerList){
                Territories.put(y.Id,new Map<String, ID>{'Default_Service_Pricebook__c' => y.Default_Service_Pricebook__c,'Default_Service_Sales_Rep__c' => y.Default_Service_Sales_Rep__c,'DEFAULT_CE_SERVICE_SALES_REP__C' => y.Default_CE_Service_Sales_Rep__c} );
            }
        } else {                                //  Territory2 is not available to populate with test data, so manually populate during test execution
            List<User> u = new List<User> ([SELECT Id, Name From User WHERE ISACTIVE = TRUE Limit 1 ]);
            Integer i = 0;
            List<Pricebook2> pbs = [Select id,name from Pricebook2 where ISSTANDARD=false];
            for(Territory2 y :TerList){
                Territories.put(y.Id,new Map<String, ID>{'Default_Service_Sales_Rep__c' => u.get(0).id,'Default_Service_Pricebook__c' => pbs.get(i).id,'DEFAULT_CE_SERVICE_SALES_REP__C' => u.get(0).id} );
                if(i < pbs.size()){ i++;}else{ i=0;}
            }   
        }    
        return Territories;
    }
    
    public void CreateOpp(SVMXC__Service_Contract__c SMC){                          // Converts a single contract into a new Opportunity
        Opportunity newOpp = new Opportunity();
        // Determine Pricebook for new Opportunity
        if (String.isNotBlank(AccountPricebookMap.get(SMC.SVMXC__Company__c))){ 
            newOpp.Pricebook2Id = AccountPricebookMap.get(SMC.SVMXC__Company__c);
        } 
        // Determine Owner for new Opportunity
        if (String.isNotBlank(AccountSalesRepMap.get(SMC.SVMXC__Company__c))){  newOpp.OwnerId = AccountSalesRepMap.get(SMC.SVMXC__Company__c);}  
        if(settingsMap.containsKey(SMC.SVC_CATEGORY__C)){
            newOpp.Product_Type__c = settingsMap.get(SMC.SVC_CATEGORY__C).Product_Type__C;
            newOpp.StageName =  settingsMap.get(SMC.SVC_CATEGORY__C).STAGE_NAME__C;
            newOpp.RecordTypeId =  settingsMap.get(SMC.SVC_CATEGORY__C).RECORDTYPE__C;       
            string nameCode = settingsMap.get(SMC.SVC_CATEGORY__C).Naming_Convention__C;  
            newOpp.Contract_Number__c = SMC.Parent_Contract_Number__c;
            newOpp.AccountId = SMC.SVMXC__Company__c;
            String formatDate = SMC.SVMXC__End_Date__c.year() +'';

            if(SMC.SVMXC__End_Date__c.month()>9){
                formatDate = formatDate + SMC.SVMXC__End_Date__c.month();
            } else{
                formatDate = formatDate + '0' + SMC.SVMXC__End_Date__c.month();
            }
            if(SMC.SVMXC__End_Date__c.day()>9){
                formatDate = formatDate + SMC.SVMXC__End_Date__c.day();
            } else{
                formatDate = formatDate + '0' + SMC.SVMXC__End_Date__c.day();
            }
            newOpp.Name = formatDate + '_' +nameCode + '_' +  accountNames.get(SMC.SVMXC__Company__c).Name.left(100) ;            
            newOpp.Contract_Expiry_Date__c = SMC.SVMXC__End_Date__c;
            newOpp.CloseDate = SMC.SVMXC__End_Date__c;
            newOpp.Primary_Contact__c = SMC.SVMXC__Contact__c;  
            newOpp.CurrencyIsoCode = accountNames.get(SMC.SVMXC__Company__c).CurrencyIsoCode;
            newOpp.Previous_Contract__c = SMC.id;

            if(validOpp(newOpp)){
                OpportunityMap.put(SMC.ID, newOpp);
                SMC.Renewal_Opportunity_Created__c = TRUE;
                completedSMCs.add(SMC);
            }
        } 
        else{
            system.debug('*****- Was unable to find custom settings for Contract type: ' + SMC.SVC_CATEGORY__C);
        }
    }    
    
     public void CreateOppProduct(ID SMCid, String cType){                                  // Add Product Line Items to Supplied Opportunity
         For(SVMXC__Service_Contract_Products__c SMCProd :ContractProductMap.values()){
            if(SMCProd.SVMXC__Service_Contract__c == SMCid){      
                OpportunityLineItem oppLine = new OpportunityLineItem();
                oppLine.OpportunityId = OpportunityMap.get(SMCid).id;
                if(cType.contains('Warranty')){oppLine.Type__c = 'Warranty Conversion';}else {oppLine.Type__c = 'Contract Renewal';}
                oppLine.Quantity = 1;                
                if(String.isNotBlank(SMCProd.SVMXC__Installed_Product__c)) {
                    PricebookEntry cPBE = getPBE(SMCProd);
                    String fullString = InstalledProducts.get(SMCProd.SVMXC__Installed_Product__c).Name;
                    oppLine.Serial_Number__c = fullString.substringAfter('-').trim();
                    oppLine.Model__c = fullString.substringBefore('-').trim();
                    oppLine.PricebookEntryId = cPBE.Id;
                    oppLine.UnitPrice = cPBE.UnitPrice;                  
                    if(validLine(oppLine)){
                        LineItem.add(oppLine);
                        checkCE_Exceptions( oppLine.OpportunityId, oppLine.Model__c);
                    }else{ 
                        System.debug('@@@@  ****  Line Item Skipped! ' + fullString + '  : Opportunity: ' + oppLine.OpportunityId );
                        System.debug('@@@@  ****  SMCid: ' + SMCid );
                        System.debug('@@@@  ****  Line Item: ' + oppLine );
                    }
                }
            }
        }
    }
   public PricebookEntry getPBE(SVMXC__Service_Contract_Products__c contractProduct){                  // Use Plan type and Covered Product to determine Pricebook Entry
        PricebookEntry cPBE = new PricebookEntry();
//**** NOTE: Following is string level processing, and requires the current Installed Product name format to remain unchanged
        String fullString = InstalledProducts.get(ContractProduct.SVMXC__Installed_Product__c).Name;
        String Model = fullString.substringBefore('-').trim();
        String Serial = fullString.substringAfter('-').trim();
        ID MasterID = ContractProduct.SVC_Item_Master_Name__c;
        String MasterName = ProductCodes.get(MasterID);        
        String pCode = MasterName + '|' + Model;
        String currcy = OpportunityMap.get(contractProduct.SVMXC__Service_Contract__c).CurrencyISOCode;
        ID PBId = OpportunityMap.get(contractProduct.SVMXC__Service_Contract__c).Pricebook2Id;   
       For(PricebookEntry cEntry :PBEbyPB.get(PBId).values()){
           if(cEntry.ProductCode == pCode ){            
                if(cEntry.CurrencyIsoCode == currcy){
                     cPBE = (PricebookEntry)cEntry;
                     break;
                }
            }                       
        }
        return cPBE;
    }   
    
    public boolean validOpp(Opportunity o){
        if( string.isNotBlank(o.RecordTypeId) && string.isNotBlank(o.AccountId)
         && string.isNotBlank(o.Pricebook2Id) && string.isNotBlank(o.CurrencyIsoCode)
         && string.isNotBlank(o.Previous_Contract__c)){
            return TRUE; 
         } else {
System.debug('******  -- Failed Opportunity: ' + o);                
            return FALSE;  
         }
    }  
        
    public boolean validLine(OpportunityLineItem ol){    
      

        
        if( string.isNotBlank(ol.OpportunityId) && string.isNotBlank(ol.PricebookEntryId)){
            return TRUE; 
         } else {
            return FALSE;  
         }
    } 
    
    private void checkCE_Exceptions(id Opp,string model){
        if(CE_Exceptions.containsKey(model)){
            Opportunity tempOpp = new Opportunity();
            tempOpp.Id = Opp;
			// re-index opportunity map
			Map<ID,Opportunity> tempMap = new Map<ID,Opportunity>();
            for (Opportunity o :OpportunityMap.values()){
                tempMap.put(o.id,o );
            }
            if (String.isNotBlank(AccountCESalesRepMap.get(tempMap.get(Opp).AccountId)))
            {  
                tempOpp.OwnerId = AccountCESalesRepMap.get(tempMap.get(Opp).AccountId);
            }  
            reparentOpps.add(tempOpp);
        }
    } 
    
}