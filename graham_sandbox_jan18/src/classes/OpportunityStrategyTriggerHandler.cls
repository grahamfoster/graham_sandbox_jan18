public class OpportunityStrategyTriggerHandler {
    public Opportunity[] opportunities {get;set;}
    public Map<String,Strategy_ROI__c> campaignStrategyMap {get;set;}
    
    public OpportunityStrategyTriggerHandler(Opportunity[] opportunities){
        this.opportunities = opportunities;
        this.campaignStrategyMap = new Map<String,Strategy_ROI__c>();
    }
    
    public void loadStrategies(){
        Set<Id> campaignIds = new Set<Id>();
        campaignStrategyMap = new Map<String,Strategy_ROI__c>();
        for(Opportunity opp : opportunities){
            if(opp.CampaignId != null && opp.Market_Vertical__c != null && opp.Market_Vertical__c != ''){
                campaignIds.add(opp.CampaignId);
            }
        }
        
        Strategy_ROI__c[] strategyROIs = [select Id, Campaign__c, Vertical__c, of_Opportunities__c, of_Leads__c from Strategy_ROI__c where Campaign__c in :campaignIds];
        for(Strategy_ROI__c sroi : strategyROIs){
            String k = sroi.Campaign__c + ':' + sroi.Vertical__c;
            campaignStrategyMap.put(k, sroi);
        }
        system.debug('campaignStrategyMap :: '+campaignStrategyMap);
    }
    
    public void onBeforeInsertUpdate(){
        for(Opportunity opp : opportunities){
            if(opp.CampaignId != null && opp.Market_Vertical__c != null && opp.Market_Vertical__c != ''){
                //set the Strategy_ROI__c lookup
                String k = opp.CampaignId + ':' + opp.Market_Vertical__c;
                system.debug('campaignStrategyMap :: '+campaignStrategyMap);
                system.debug('onBeforeInsertUpdate :: k='+k);
                system.debug('map has ' + campaignStrategyMap.get(k));
                if(campaignStrategyMap.containsKey(k)){
                    Strategy_ROI__c sroi = campaignStrategyMap.get(k);
                    if(sroi != null){
                        system.debug('setting Strategy_ROI__c='+sroi.Id);
                        opp.Strategy_ROI__c = sroi.Id;
                    }
                }else {
                    opp.Strategy_ROI__c = null;
                }
            }
        }
    }
    
    public void onAfterInsert(){
        Set<Id> sroiIds = new Set<Id>();
        for(Opportunity opp : opportunities){
            if(opp.CampaignId != null && opp.Market_Vertical__c != null && opp.Market_Vertical__c != ''){
                String k = opp.CampaignId + ':' + opp.Market_Vertical__c;
                if(campaignStrategyMap.containsKey(k)){
                    Strategy_ROI__c sroi = campaignStrategyMap.get(k);
                    if(sroi != null){
                        sroiIds.add(sroi.Id);
                    }
                }
            }
        }
        
        updateCounts(sroiIds);
    }    
    
    public static void onBeforeInsert(Opportunity[] opportunities){
        OpportunityStrategyTriggerHandler handler = new OpportunityStrategyTriggerHandler(opportunities);
        handler.loadStrategies();
        handler.onBeforeInsertUpdate();
    }
    
    public static void onAfterInsert(Opportunity[] opportunities){
        OpportunityStrategyTriggerHandler handler = new OpportunityStrategyTriggerHandler(opportunities);
        handler.loadStrategies();
        handler.onAfterInsert();
    }

    public static void onBeforeUpdate(Opportunity[] opportunities, Map<Id, Opportunity> oldMap){
        OpportunityStrategyTriggerHandler handler = new OpportunityStrategyTriggerHandler(opportunities);
        handler.loadStrategies();
        handler.onBeforeInsertUpdate();
    }

    public static void onAfterUpdate(Opportunity[] opportunities, Map<Id, Opportunity> oldMap){
        OpportunityStrategyTriggerHandler handler = new OpportunityStrategyTriggerHandler(opportunities);
        handler.loadStrategies();
        handler.onAfterUpdate(oldMap);
    }
    
    public void onAfterUpdate(Map<Id, Opportunity> oldMap){
        Set<Id> strategyROIIds = new Set<Id>();
        Set<String> keys = new Set<String>();
        
        for(Opportunity opp : opportunities){
            Opportunity oldOpp = oldMap.get(opp.Id);
            if(opp.CampaignId != null && oldOpp.Market_Vertical__c != opp.Market_Vertical__c || oldOpp.Amount != opp.Amount
                || oldOpp.Strategy_ROI__c != opp.Strategy_ROI__c) {
                strategyROIIds.add(oldOpp.Strategy_ROI__c);
                String k = opp.CampaignId + ':' + opp.Market_Vertical__c;
                keys.add(k);
            }
        }
        
        for(String k : campaignStrategyMap.keySet()){
            if(keys.contains(k)) strategyROIIds.add(campaignStrategyMap.get(k).Id);
        }
        
        system.debug('updateCounts::'+strategyROIIds);
        updateCounts(strategyROIIds);
    }
    
    public void updateCounts(Set<Id> sroiIds){
        Strategy_ROI__c[] srois = [select Id, of_Opportunities__c, of_Leads__c, (select Id, Amount from Opportunities__r), (select Id from Leads__r) from Strategy_ROI__c where Id in :sroiIds];
        for(Strategy_ROI__c sroi : srois){
            sroi.of_Opportunities__c = sroi.Opportunities__r.size();
            decimal roi = 0;
            for(Opportunity opp : sroi.Opportunities__r){
                if(opp.Amount != null) roi += opp.Amount;
            }
            sroi.Return__c = roi;
        }
        update srois;
    }        

    public static void onDelete(Opportunity[] opportunities){
        OpportunityStrategyTriggerHandler handler = new OpportunityStrategyTriggerHandler(opportunities);
        handler.loadStrategies();
        handler.onAfterInsert();
    }

    public static void onUndelete(Opportunity[] opportunities){
        OpportunityStrategyTriggerHandler handler = new OpportunityStrategyTriggerHandler(opportunities);
        handler.loadStrategies();
        handler.onAfterInsert();
    }

}