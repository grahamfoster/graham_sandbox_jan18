/*
 *	Test_Dataload_AddStandardPbE4Product
 *	
 *	Test class for Dataload_AddStandardPbE4Product.
 * 
 * 	Created by Yong Chen on 2015-09-15
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
private class Test_Dataload_AddStandardPbE4Product {

    @isTest static void myUnitTest() {
    	
 
		// get Currency Type count, and PricebookEntry count of the Product
		Integer countCT = [SELECT count() FROM CurrencyType WHERE IsActive=TRUE ];

        //String standardPBId = [Select Id From Pricebook2 where IsStandard = true][0].Id;
        String standardPBId = (String)Test.getStandardPricebookId();

		// ---------------------------------------------------------------------
		// prepare data
		// ---------------------------------------------------------------------
	 	Product2 p = new Product2(Name='YongTest_dataload', IsActive=true, CurrencyIsoCode='USD');
		insert p;
		PricebookEntry pbe = new PricebookEntry (
				CurrencyIsoCode = 'USD',
				Product2Id = p.Id, 
				Pricebook2Id = standardPBId,
				IsActive = True, 
				UnitPrice = 0 
				);
		insert pbe;
		
		//PricebookEntry pbe = [SELECT Id FROM PricebookEntry WHERE Product2Id=:p.Id LIMIT 1][0];
		//delete pbe;



		// ---------------------------------------------------------------------
    	// Run the batch Apex
		// ---------------------------------------------------------------------
        
		Integer batchSize = 20;
		Test.startTest();
		//Id batchInstanceId = Database.executeBatch(new Dataload_AddStandardPbE4Product(standardPBId),batchSize);
		Id batchInstanceId = Database.executeBatch(new Dataload_AddStandardPbE4Product(standardPBId));
		Test.stopTest();
		

		
		// ---------------------------------------------------------------------
		// Verify the PricebookEntry count of this Product should be the same as 
		// Currency Type count   
		// ---------------------------------------------------------------------
	 	//Product2 p = [Select Id From Product2 limit 1];
		Integer countPBE = [SELECT count() FROM PricebookEntry WHERE Product2Id=:p.Id ];
		System.assertEquals(countCT,countPBE);
	 	
        
    }
}