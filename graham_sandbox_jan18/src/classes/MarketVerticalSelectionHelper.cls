public class MarketVerticalSelectionHelper {
	/**
	 ** @param
	**/
	public static String getMarketVertical(String institutionType, String marketSegment, String application, String routineTesting){
		Market_Vertical_Config__c[] mvcs = [select Market_Vertical__c from Market_Vertical_Config__c where
			Institution_Type__c = :institutionType and Market_Segment__c = :marketSegment and Application__c = :application and Routine_Testing__c = :routineTesting];
		if(mvcs.size() > 0) {
			return mvcs[0].Market_Vertical__c;
		}else{
			return null;
		}
	}
	
	public static Map<String,String> getMarketVertical(Set<String> keys){
		system.debug('keys='+keys);
		Market_Vertical_Config__c[] mvcs = [select Key__c, Market_Vertical__c from Market_Vertical_Config__c where
			Key__c in :keys];
		Map<String,String> m = new Map<String, String>();
		
		for(Market_Vertical_Config__c mvc : mvcs) {
			m.put(mvc.Key__c.toLowerCase(), mvc.Market_Vertical__c);
		}
		
		return m;
	}

	public static testMethod void testGet1() {
		Market_Vertical_Config__c mvc = new Market_Vertical_Config__c(Market_Vertical__c = 'Food/Beverage', Routine_testing__c = 'Yes',
			Application__c = 'Allergen testing', Market_Segment__c = 'Agriculture', Institution_Type__c = 'Academic');
		insert mvc;
		
		System.assertEquals('Food/Beverage', MarketVerticalSelectionHelper.getMarketVertical('Academic', 'Agriculture', 'Allergen testing', 'Yes'));
		System.assertEquals(null, MarketVerticalSelectionHelper.getMarketVertical('Academic', 'Agriculture', 'Allergen testing', 'No'));
	}
	
	@isTest(SeeAllData=true)
	public static void testCoverage() {
		Set<String> institutionTypes = new Set<String>();
		for(Schema.PicklistEntry ple : Account.Institution_Type__c.getDescribe().getPicklistValues()) {
			institutionTypes.add(ple.getValue());
		} 
		system.debug('institutionTypes='+institutionTypes);

		Set<String> marketSegments = new Set<String>();
		for(Schema.PicklistEntry ple : Opportunity.Market_Segment__c.getDescribe().getPicklistValues()) {
			marketSegments.add(ple.getValue());
		} 
		system.debug('marketSegments='+marketSegments);

		Set<String> applications = new Set<String>();
		for(Schema.PicklistEntry ple : Opportunity.Primary_Application__c.getDescribe().getPicklistValues()) {
			applications.add(ple.getValue());
		} 
		system.debug('applications='+applications);

		Set<String> routineTestings = new Set<String>();
		for(Schema.PicklistEntry ple : Opportunity.Routine_testing__c.getDescribe().getPicklistValues()) {
			routineTestings.add(ple.getValue());
		} 
		system.debug('routineTestings='+routineTestings);
		
		Map<String, Market_Vertical_Config__c> theMap = new Map<String, Market_Vertical_Config__c>();
		for(String instType : institutionTypes) {
			for(String marketSegment : marketSegments) {
				for(String application : applications) {
					for(String routineTesting : routineTestings) {
						String key = instType + '::' + marketSegment + '::' + application + '::' + routineTesting;
						theMap.put(key,null);
					}
				}
			}
		} 
		system.debug('There are ' + theMap.keySet().size() + ' possible combinations of the 4 inputs');
		
		Market_Vertical_Config__c[] extras = new Market_Vertical_Config__c[]{};
		Market_Vertical_Config__c[] mvcs = [select Routine_testing__c, Application__c, Market_Segment__c, Institution_Type__c from Market_Vertical_Config__c];
		for(Market_Vertical_Config__c mvc : mvcs) {
			String key = mvc.Institution_Type__c + '::' + mvc.Market_Segment__c + '::' + mvc.Application__c + '::' + mvc.Routine_testing__c;
			if(!theMap.containsKey(key)) {
				extras.add(mvc);
			}else{
				theMap.put(key, mvc);
			}
		}
		
		System.debug('There are ' + extras.size() + ' of configs that don\'t match picklist values');
		integer missing = 0;
		for(Market_vertical_config__c mvc : theMap.values()) {
			if(mvc == null) missing++;
		}
		System.debug('There are ' + missing + ' combinations not covered by the configs');
	}
}