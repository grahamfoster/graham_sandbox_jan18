public with sharing class DebugOppClass 
{
	public list<Opportunity> ASD
	{
		get
		{
          list<Opportunity>  Opport = [SELECT Amount, Market_Vertical__c,StageName,Mgr_Upside__c, In_Forecast_Mgr__c,ABS_New_Customer__c, Owner.Name, Probability, Name  FROM Opportunity WHERE Amount != null 
                                                                AND CloseDate = THIS_QUARTER
                                                                AND StageName !='Dead/Cancelled' order by StageName]; 	
             return Opport;                                                   		
		}
		set;
	}
	
	public Integer Koll
	{
		get
		{
			return ASD.size();
		}
		set;
	}
	

}