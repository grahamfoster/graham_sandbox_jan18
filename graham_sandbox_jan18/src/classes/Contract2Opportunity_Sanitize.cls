/**
 * @author Brett Moore
 * @created 
 * @Revision  
 * @Last Revision 
 * 
 * Class to populate Opportunities Line Items based on associated Service Contract Line Items
**/
public class Contract2Opportunity_Sanitize {

	// Load custom Logger
    public CustomLogger.Logger productLog = new CustomLogger.Logger();
	// Initialize Data Maps
    public Map<Id, Opportunity_IB__c> myOppIBs; 
    public Map<Id, OpportunityLineItem> opplinesById = new Map<Id, OpportunityLineItem>();
//    public Map<Id, SVMXC__INSTALLED_PRODUCT__c> IBsById = new Map<Id, SVMXC__INSTALLED_PRODUCT__c>();
//    public Map<Id, SVMXC__Service_Contract__c> consById;
    public Map<Id, SVMXC__Service_Contract_Products__c> conlinesById = new Map<Id, SVMXC__Service_Contract_Products__c>();
    public List<String> serials = new List<String>();
    public Opportunity theOpportunity;
    
    public void init(Opportunity o){
        //Query all sanitization data needed for this Opportunity 
        this.myOppIBs = new Map<Id, Opportunity_IB__c>([SELECT id,Opportunity__c, Covered_Product__c,Serial_Number__c,Model__c,Expiration__c, status__c, contract__c FROM Opportunity_IB__c WHERE Opportunity__c = :theOpportunity.Id ]);
        for (Opportunity_IB__c ib : myOppIBs.values()){ 
        	if(String.isNotBlank(ib.Serial_Number__c)) serials.add(ib.Serial_Number__c);	// compile a list of serial numbers
        }
 		this.opplinesById = new Map<Id,OpportunityLineItem>([SELECT id, OpportunityId, Opportunity.StageName, Opportunity.CONTRACT_EXPIRY_DATE__C, Product2Id, ProductCode, Covered_Product__c, Model__c, Opportunity_IB__c, Serial_Number__c, Type__c  FROM OpportunityLineItem WHERE Serial_Number__c = :serials AND Opportunity.CONTRACT_EXPIRY_DATE__C > TODAY AND  Opportunity.StageName != 'Deal Lost' AND  Opportunity.StageName != 'Dead/Cancelled']);            
        this.conlinesById  = new Map<Id, SVMXC__Service_Contract_Products__c>([SELECT id, SVMXC__SERVICE_CONTRACT__C, SVMXC__END_DATE__C,SVMXC__INSTALLED_PRODUCT__C, SVC_CONTRACT_SUBLINE_STATUS__C,Model__c, Serial_Number__c FROM SVMXC__Service_Contract_Products__c WHERE Serial_Number__c IN :serials  AND  SVC_CONTRACT_SUBLINE_STATUS__C IN('ACTIVE','SIGNED','MULTIPLE YEAR - GOT PO') AND SVC_Contract_Applicability__c IN('Future','Current')]);        
    }
    public Contract2Opportunity_Sanitize(Map<Id, Opportunity> initialQuery) {  
        // Initialize custom Logging for Contracts
		productLog.loglevel = 'Error';
		productLog.process = 'Sanitize Opportunities';
		//distill the Map sent from the batch processor into a single record
		this.theOpportunity = initialQuery.values().get(0);
        // Initialize Comparison data
        init(theOpportunity); 
        
        // Iterate through each line item
  	    for (Opportunity_IB__c ib : myOppIBs.values()){ 
			scanIB(ib);
        }
        
        if(theOpportunity.c2o_AutoGeneration_Status__c == 'Created'){
        	theOpportunity.c2o_AutoGeneration_Status__c = 'Sanitized';
        }
        updateOIBs();
        update theOpportunity;
    }
   

    Public void updateOIBs(){
    	if(myOppIBs.size() >0 ) update myOppIBs.values();    
    }   
    
    public void scanIB(Opportunity_IB__c ib){
		clearAll(ib.id);
        ib.Similar_Opportunities__c = onOtherOpps(ib);
		ib.Similar_Contracts__c = onOtherCons(ib);
		ib.On_Opportunity__c = isOnOpportunity(ib);
		ib.Last_Scan__c = datetime.now();
        ib.Multi_Year__c = multiYear(ib);
    }    
    public void clearAll(Id myID){
        myOppIBs.get(myID).Similar_Contracts__c = 0;   
        myOppIBs.get(myID).Similar_Opportunities__c = 0;
        myOppIBs.get(myID).Multi_Year__c = FALSE;
    }
  
    
	public Integer onOtherOpps(Opportunity_IB__c ib){        
        Integer result =0;
        Boolean exact = false;
        Boolean Model = false;
        Boolean SN = false;
        if(opplinesById.size()>0){
        	for(OpportunityLineItem li :opplinesById.values()){
                if(li.OpportunityId != ib.Opportunity__c && ib.Model__c == li.Model__c && ib.Serial_Number__c == li.Serial_Number__c && ib.Expiration__c.isSameDay(li.Opportunity.CONTRACT_EXPIRY_DATE__C) ){
					exact = TRUE;                                        
                } else if(li.OpportunityId != ib.Opportunity__c && ib.Model__c == li.Model__c && ib.Serial_Number__c == li.Serial_Number__c) {
                    Model = TRUE;                                       
                } else if (li.OpportunityId != ib.Opportunity__c && ib.Serial_Number__c == li.Serial_Number__c){
                    SN = TRUE;                                      
                }    
	        }        
        }
        if(exact){
            result = 3;   
        } else if (Model){
            result = 2;   
        } else if (SN){
            result = 1;   
        }
        return result;
    }
    
    public Integer onOtherCons(Opportunity_IB__c ib){
        Integer result =0;
        Boolean exact = false;
        Boolean Model = false;
        Boolean SN = false;
        if(conlinesById.size()>0){
            for(SVMXC__Service_Contract_Products__c cl :conlinesById.values()){
                if(ib.contract__c != cl.SVMXC__Service_Contract__c && ib.Model__c == cl.Model__c && ib.Serial_Number__c == cl.Serial_Number__c && ib.Expiration__c.isSameDay(cl.SVMXC__END_DATE__C) && (cl.SVC_Contract_Subline_Status__c == 'ACTIVE' || cl.SVC_Contract_Subline_Status__c == 'SIGNED' )   ){
					exact = TRUE;                                        
                } else if(ib.contract__c != cl.SVMXC__Service_Contract__c  && ib.Model__c == cl.Model__c && ib.Serial_Number__c == cl.Serial_Number__c && (cl.SVC_Contract_Subline_Status__c == 'ACTIVE' || cl.SVC_Contract_Subline_Status__c == 'SIGNED' ) ) {
                    Model = TRUE;                                       
                } else if(ib.contract__c != cl.SVMXC__Service_Contract__c  && ib.Serial_Number__c == cl.Serial_Number__c && (cl.SVC_Contract_Subline_Status__c == 'ACTIVE' || cl.SVC_Contract_Subline_Status__c == 'SIGNED' ) ){
                    SN = TRUE;                                      
                }    
	        }        
        }
        if(exact){
            result = 3;   
        } else if (Model){
            result = 2;   
        } else if (SN){
            result = 1;   
        }
        return result;
    }

    public boolean multiYear(Opportunity_IB__c ib){
        boolean result = FALSE;
        if(ib.Status__c == 'MULTIPLE YEAR - GOT PO' ){
             result = TRUE; 
        } else if(conlinesById.size()>0){
            for(SVMXC__Service_Contract_Products__c cl :conlinesById.values()){
                if(ib.contract__c != cl.SVMXC__Service_Contract__c && ib.Model__c == cl.Model__c && ib.Serial_Number__c == cl.Serial_Number__c &&  cl.SVC_Contract_Subline_Status__c == 'SIGNED'   ){
					 result = TRUE;   
                }    
	        }        
        }
        return result;
    }
    public boolean isOnOpportunity(Opportunity_IB__c ib){
        boolean result = FALSE;
        if(opplinesById.size()>0){
        	for(OpportunityLineItem li :opplinesById.values()){
                if(String.isNotBlank(li.Opportunity_IB__c) && li.Opportunity_IB__c == ib.Id ) result = TRUE;
            }
        }
        return result;
    }
}
/*
 * Code for invoking via Annonymus execution
 * 
   Map<Id, Opportunity> initialQuery = new Map<Id, Opportunity>([	
		SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,
			c2o_CE_Opportunity__c, Account_Region__c,Previous_Contract__c
  		FROM  
			Opportunity 
		WHERE 
			id = ' ' 
		]);

   new Contract2Opportunity_Sanitize(initialQuery);
 *
 */
/* OR
  String Query = 'SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c, c2o_CE_Opportunity__c, Account_Region__c,Previous_Contract__c FROM Opportunity WHERE c2o_AutoGeneration_Status__c = \'Linked\'';
  Integer scope = 1;
  String process = 'Sanitize';
  Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
  id batchInstanceId = database.executebatch(b,scope);
 * 
 */