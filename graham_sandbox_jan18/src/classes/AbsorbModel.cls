// ===========================================================================
// Object: AbsorbModel
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Mirrors the model of the Absorb LMS API model, to construct JSON and receive JSON
// ===========================================================================
// Changes: 2016-01-30 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class AbsorbModel 
{
	public class UserCreate extends User {
		public String Password;
	}
	
	public virtual class User 
	{
		public transient String Id;
		public String Username; //required
		//public String Id; //Not required?
		public String DepartmentId; //required
		public String FirstName; //required
		public String MiddleName;
		public String LastName; //required
		public String EmailAddress; //required
		public String Phone;
		public String Address;
		public String City;
		public String PostalCode;
		public String ProvinceId; //UID
		public String CountryId; //UID
		//custom fields
		public UserCustomFields CustomFields;

		public User(){
			this.CustomFields = new UserCustomFields();
		}

		public String[] validate()
		{
			String[] errors = new String[]{};
			//if(String.isBlank(Id)) errors.add('Id is required');
			if(String.isBlank(Username)) errors.add('Username is required');
			if(String.isBlank(DepartmentId)) errors.add('DepartmentId is required');
			if(String.isBlank(FirstName)) errors.add('FirstName is required');
			if(String.isBlank(LastName)) errors.add('LastName is required');
			if(String.isBlank(EmailAddress)) errors.add('EmailAddress is required');
			return errors;
		}
	}

	public class UserCustomFields
	{
		public transient String MarketVertical;
		public transient String PrimaryWorkflow;
		public transient String LearnerLevel;
		public transient String Product;
		public transient String TrainingPartNumber;
		public transient String SalesOrderNumber;
		public transient Boolean Entitlement;
		
		public String String1 {
			get {
				return Product;
			}
		}

		public String String2 {
			get {
				return TrainingPartNumber;
			}
		}

		public String String3 {
			get {
				return MarketVertical;
			}
		}

		public String String4 {
			get {
				return PrimaryWorkflow;
			}
		}

		public String String5 {
			get {
				return LearnerLevel;
			}
		}

		public String String6 {
			get {
				return SalesOrderNumber;
			}
		}

		public Boolean Bool1 {
			get {
				return Entitlement;
			}
		}

		/*
		Product (String1)
		Training Part Number (String2)
		Market Vertical (String3)
		Primary Workflow (String4)
		Learner Level (String5)
		Sales Order Number (String6)
 		*/
	}

	public class Course implements Comparable
	{
		public String Id {get;set;}
		public String Name {get;set;}
		public String Description {get;set;}
		public String ExternalId {get;set;}
		public String CategoryId {get;set;}
		public Integer ActiveStatus {get;set;}
		public String Notes {get;set;}
		public String Vendor {get;set;}
		public String Goals {get;set;}
        public String Audience {get;set;}

		public Integer compareTo(Object that)
		{
			Course thatCourse = (Course)that;
			return this.Name.compareTo(thatCourse.Name);
		}
	}

	public class Department
	{
		public String Id {get;set;}
		public String Name {get;set;}
	}

	public class CourseEnrollment
	{
		public String Id {get;set;}
		public String CourseId {get;set;}
		public String CourseName {get;set;}
		public String CourseVersionId {get;set;}
		public String UserId {get;set;}
		public String SessionId {get;set;}
		public String CourseEnrollmentId {get;set;}
		public String FullName {get;set;}
		public Integer Status {get;set;}
		public Decimal Progress {get;set;}
		public Decimal Score {get;set;}
		public String DateStarted {get;set;}
		public String DateStartedDt {
			get {
				//2016-02-12T22:52:43.33 is GMT
				if(String.isBlank(DateStarted) || DateStarted.length() < 20) return null;
				Integer yr = Integer.valueOf(DateStarted.substring(0,4));
				Integer mo = Integer.valueOf(DateStarted.substring(5,7));
				Integer dd = Integer.valueOf(DateStarted.substring(8,10));
				Integer hr = Integer.valueOf(DateStarted.substring(11,13));
				Integer min = Integer.valueOf(DateStarted.substring(14,16));
				Integer sec = Integer.valueOf(DateStarted.substring(17,19));
				return DateTime.newInstanceGmt(yr, mo, dd, hr, min, sec).format();
			}
		}
		public String enrollmentStatus {
			get {
				if(Status == null) return null;
				Map<Integer,String> enrollmentStatusMap = new Map<Integer,String>{
					0 => 'Not Started',
					1 => 'In Progress',
					2 => 'Pending Approval',
					3 => 'Complete',
					4 => 'Not Complete',
					5 => 'Failed',
					6 => 'Declined',
					7 => 'Pending Evaluation Required',
					8 => 'On Wait List',
					9 => 'Absent',
					10 => 'Not Applicable'
				};
				return enrollmentStatusMap.containsKey(Status) ? enrollmentStatusMap.get(Status) : String.valueOf(Status);
			}
		}
	}

	public class CourseFilter
	{
		public String id;
		public String categoryId;
		public Date fromDt;
		public Date toDt;
		public String userId;
	}

	public class CreateUserResponse
	{
		public String Id;
		public String Username;
	}

	public class Version 
	{

	}

	public class Country
	{
		public String Id;
		public String CountryCode;
		public String Name;
	}

	public class Province
	{
		public String Id;
		public String Name;
	}

	public class Session {
		public String Id;
		public String Name;
		public String Description;
		public String[] InstructorIds;
		public String[] SessionScheduleIds;
		public String EnrollmentStartDate;
		public String EnrollmentEndDate;
		public DateTime EnrollmentStartDateDt {
			get {
				return parseGMTDateAsDateTime(EnrollmentStartDate);
			}
		}
		public DateTime EnrollmentEndDateDt {
			get {
				return parseGMTDateAsDateTime(EnrollmentEndDate);
			}
		}
	}

	//DateStart=2016-06-07T12:00:00
	//This is 8am EDT (i.e. time in GMT)
	public class SessionSchedule {
		public String Id;
		public String SessionId;
		public String VenueId;
		public String VenueName;
		public String DateStart;
		public String DateEnd;
		public String TimeZoneId;
		public DateTime DateStartDt {
			get {
				return parseGMTDateAsDateTime(DateStart);
			}
		}
		public DateTime DateEndDt {
			get {
				return parseGMTDateAsDateTime(DateEnd);
			}
		}
	}

	public static DateTime parseGMTDateAsDateTime(String dtString)
	{
		if(String.isBlank(dtString) || dtString.length() < 19) return null;
		Integer yr = Integer.valueOf(dtString.substring(0,4));
		Integer mo = Integer.valueOf(dtString.substring(5,7));
		Integer dd = Integer.valueOf(dtString.substring(8,10));
		Integer hr = Integer.valueOf(dtString.substring(11,13));
		Integer min = Integer.valueOf(dtString.substring(14,16));
		Integer sec = Integer.valueOf(dtString.substring(17,19));
		return DateTime.newInstanceGmt(yr, mo, dd, hr, min, sec);
	}
}