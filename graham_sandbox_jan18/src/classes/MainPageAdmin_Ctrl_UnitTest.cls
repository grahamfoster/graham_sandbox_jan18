@isTest
public with sharing class MainPageAdmin_Ctrl_UnitTest {
    static TestMethod void MainPageAdmin_Ctrl_UnitTest1(){
        Product_Category__c prodCategRecObj = Util.CreateProductCategory();
        ApexPages.currentPage().getParameters().put('deleteCatID',prodCategRecObj.Id);
        Specifications_Meta__c speciMetaRecObj = Util.CreateProductfeatureCategory(prodCategRecObj.id);
        Product_Meta__c prrodMetaRecObj = Util.CreateProductfeature(speciMetaRecObj.Id);
        Manufacturer__c ManufObj = Util.createManufacturerABX(); 
        Product2 prodObj = Util.createProduct2(ManufObj.id, prodCategRecObj.Id) ;
        
        MainPageAdmin_Ctrl MainPageAdminObj = new MainPageAdmin_Ctrl();
        Product_Category__c prodCategRecObj1 = Util.CreateProductCategory();
        Specifications_Meta__c speciMetaRecObj1 = Util.CreateProductfeatureCategory(prodCategRecObj1.id);
        Product_Meta__c prrodMetaRecObj1 = Util.CreateProductfeature(speciMetaRecObj1.Id);
        Manufacturer__c ManufObj1 = Util.createManufacturerABX(); 
        Product2 prodObj1 = Util.createProduct2(ManufObj1.id, prodCategRecObj1.Id) ;
        
        
        ApexPages.currentPage().getParameters().put('tabfocus','2');
        ApexPages.currentPage().getParameters().put('prodtab','2');
        MainPageAdminObj.MainPageAdmin_Ctrl_Action(); 
        MainPageAdminObj.getproductMap();
        MainPageAdminObj.val();
        
        MainPageAdminObj.sortProduct();
        MainPageAdminObj.getSortDirection() ;
       
        MainPageAdminObj.setSortDirection('ASC');
        MainPageAdminObj.hiddenProductId = prodObj.id;
        MainPageAdminObj.delSelProduct();
        
       
    }
    
}