@isTest
private class AccountRevBatchable_Test {

   public static String EVERY_JAN_1 = '0 0 5 1 1 ?';

   public static testMethod void test_schedulable(){
      Test.startTest();
		AccountRevenueSchedulable.scheduleJob('Account Revenue Rollups', EVERY_JAN_1);
        System.assertEquals('0 0 5 1 1 ?', EVERY_JAN_1);

      Test.stopTest();
   }
    
   static testMethod void test_schedulable1(){
    AccountRevenueSchedulable.scheduleJob();
}
    
static testmethod void test_keyaccount() {
       // Create a test account
       Account Acct = new Account(Name='AccountTest Key Account',
                                   BillingCountry='Canada',
                                   YTD_Revenue_Generated__c = 55000,
                                   PY_Revenue_Generated__c = 0);
       insert Acct;
	  
            AccountRevBatchable ARB = new AccountRevBatchable();
        Test.startTest();
		Database.executeBatch(ARB);
		Test.stopTest();  
       
       Acct = [select Id, PY_Revenue_Generated__c, YTD_Revenue_Generated__c, Name from Account where Id =: Acct.Id];
       System.assertEquals(55000, Acct.PY_Revenue_Generated__c );
       System.assertEquals(0, Acct.YTD_Revenue_Generated__c);
    }     
}