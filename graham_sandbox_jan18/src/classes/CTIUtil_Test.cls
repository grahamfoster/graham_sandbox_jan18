@isTest 
private class CTIUtil_Test {

	@isTest
	private static void test_newFeedItem()
	{
		Task t1 = new Task(Subject = 'Test call 1');
		insert t1;
		FeedItem f1 = new FeedItem(ParentId = t1.Id, Body = 'Test Body');
		insert f1;

		CTIUtil.newFeedItem(new Set<Id>{t1.Id});

		List<Task> deletedTask = [SELECT Id FROM Task WHERE Id = :t1.Id];
		System.assertEquals(0, deletedTask.size());

		List<Task> newTask = [SELECT Id FROM Task WHERE Subject = 'Test call 1'];
		System.assertEquals(1, newTask.size());
		System.assertNotEquals(t1.Id, newTask[0].Id);

		List<FeedItem> newFeedItem = [SELECT Id FROM FeedItem WHERE ParentId = :newTask[0].Id];
		System.assertEquals(1, newFeedItem.size());
	}
}