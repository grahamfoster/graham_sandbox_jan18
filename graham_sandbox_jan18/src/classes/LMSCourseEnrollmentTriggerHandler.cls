// ===========================================================================
// Object: LMSCourseEnrollmentTriggerHandler
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Refactored from LMSCourseEnrollmentTrigger on July 6/2016
// ===========================================================================
// Changes: 2016-06-16 Reid Beckett
//           Class created
// ===========================================================================
public class LMSCourseEnrollmentTriggerHandler extends TriggerHandler
{
	override protected void afterInsert()
	{
		setCaseLookups();
	} 

	override protected void afterUpdate() 
	{
		//when updated by UI in SFDC, sync to the LMS enrollment
		updateLMSEnrollment();
		setCaseLookups();
	}

	private void setCaseLookups()
	{
		//Sync to the case
		//1) According to LMS_Course_Type__c value (1,2, or 3) populate one of 3 lookup fields on the Case
		//2) Find the Case with the ContactId of matching user, set the lookup value to this course.

		Set<Id> contactIds = new Set<Id>();
		Set<Id> completedEnrollmentIds = new Set<Id>();
		for(LMS_Course_Enrollment__c e : (List<LMS_Course_Enrollment__c>)Trigger.new)
		{
			contactIds.add(e.LMS_User__c);
			LMS_Course_Enrollment__c oldObj = null;
			if(Trigger.isUpdate) {
				oldObj = (LMS_Course_Enrollment__c)Trigger.oldMap.get(e.Id);
			}
			if((oldObj == null || oldObj.LMS_Status__c != e.LMS_Status__c) && e.LMS_Status__c == 'Complete') {
				//update the related pre-elearning case
				completedEnrollmentIds.add(e.Id);
			}
		} 

		if(!completedEnrollmentIds.isEmpty())
		{
			Map<Id, Case> caseUpdates = new Map<Id,Case>();
			//find the related cases
			for(Case caseObj : [select Id, Pre_elearning_for_Field_Service_Engineer__c from Case where Pre_elearning_for_Field_Service_Engineer__c in :completedEnrollmentIds and Pre_Install_Course_Complete__c != true])
			{
				caseObj.Pre_Install_Course_Complete__c = true;
				caseUpdates.put(caseObj.Id, caseObj);
			}
			if(!caseUpdates.isEmpty()) update caseUpdates.values();
		}

		if(!contactIds.isEmpty())
		{
			Map<Id, List<Case>> casesByContact = new Map<Id, List<Case>>();
			Date cutoffDate = Date.today().addMonths(-3); //only want cases created in the last 3 months
			for(Case caseObj : [select Id, ContactId from Case where RecordType.Name = 'Training' and ContactId in :contactIds and CreatedDate >= :cutoffDate])
			{
				if(caseObj.ContactId != null) {
					if(casesByContact.containsKey(caseObj.ContactId)) casesByContact.get(caseObj.ContactId).add(caseObj);
					else casesByContact.put(caseObj.ContactId, new List<Case>{ caseObj });
				}
			}

			if(!casesByContact.isEmpty()) 
			{
				Set<Id> courseIds = new Set<Id>();
				for(LMS_Course_Enrollment__c e : (List<LMS_Course_Enrollment__c>)Trigger.new)
				{
					if(casesByContact.containsKey(e.LMS_User__c))
						courseIds.add(e.LMS_Course__c);
				} 

				if(!courseIds.isEmpty())
				{
					Map<Id, Case> caseUpdates = new Map<Id, Case>();
					Map<Id,LMS_Course__c> courseMap = new Map<Id,LMS_Course__c>([select Id, LMS_Course_Type__c, Course_Name__c from LMS_Course__c where Id in :courseIds]);
					for(LMS_Course_Enrollment__c e : (List<LMS_Course_Enrollment__c>)Trigger.new)
					{
						if(courseMap.containsKey(e.LMS_Course__c))
						{
							LMS_Course__c lmsCourse = courseMap.get(e.LMS_Course__c);
							if(casesByContact.containsKey(e.LMS_User__c)) 
							{
								List<Case> casesList = casesByContact.get(e.LMS_User__c);
								if(!String.isBlank(lmsCourse.LMS_Course_Type__c))
								{
									if(casesByContact.containsKey(e.LMS_User__c)) 
									{
										for(Case caseObj : casesList) {
											if(lmsCourse.LMS_Course_Type__c == '1') {
												//populate lookup field #1
												caseObj.Pre_elearning_for_Field_Service_Engineer__c = e.Id;
												if(!caseUpdates.containsKey(caseObj.Id)) caseUpdates.put(caseObj.Id, caseObj);
												else caseUpdates.get(caseObj.Id).Pre_elearning_for_Field_Service_Engineer__c = e.Id;
											}else if(lmsCourse.LMS_Course_Type__c == '2') {
												//populate lookup field #2
												caseObj.Field_Service_Engineer_On_site_Training__c = e.Id;
												if(!caseUpdates.containsKey(caseObj.Id)) caseUpdates.put(caseObj.Id, caseObj);
												else caseUpdates.get(caseObj.Id).Field_Service_Engineer_On_site_Training__c = e.Id;
											}else if(lmsCourse.LMS_Course_Type__c == '3') {
												//populate lookup field #3
												caseObj.FAS_On_site_Training__c = e.Id;
												if(!caseUpdates.containsKey(caseObj.Id)) caseUpdates.put(caseObj.Id, caseObj);
												else caseUpdates.get(caseObj.Id).FAS_On_site_Training__c = e.Id;
											}
										}
									}
								}
								for(Case caseObj : casesList) {
									if(lmsCourse.Course_Name__c != null && 
                                       (lmsCourse.Course_Name__c.toLowerCase().contains('learning path') ||
                                       lmsCourse.Course_Name__c.toLowerCase().contains('sciexuniversity - success fundamentals program'))) {
										//populate lookup field #4
										caseObj.Learning_Path__c = e.Id;
										if(!caseUpdates.containsKey(caseObj.Id)) caseUpdates.put(caseObj.Id, caseObj);
										else caseUpdates.get(caseObj.Id).Learning_Path__c = e.Id;
									}
								}
							}
						}
					}
					if(!caseUpdates.isEmpty()) update caseUpdates.values();
				}
			}
		}
	}

	private void updateLMSEnrollment()
	{
		//when updated by UI in SFDC
		if(Trigger.new.size() == 1 && !System.isBatch() && Trigger.isUpdate) 
		{
			Map<String,Integer> enrollmentStatusMap = new Map<String,Integer>{
				'Not Started' => 0,
				'In Progress' => 1,
				'Pending Approval' => 2,
				'Complete' => 3,
				'Not Complete' => 4,
				'Failed' => 5,
				'Declined' => 6,
				'Pending Evaluation Required' => 7,
				'On Wait List' => 8,
				'Absent' => 9,
				'Not Applicable' => 10
			};
			for(LMS_Course_Enrollment__c e : (List<LMS_Course_Enrollment__c>)Trigger.new) 
			{
				LMS_Course_Enrollment__c oldObj = (LMS_Course_Enrollment__c)Trigger.oldMap.get(e.Id);
				if(oldObj.LMS_Status__c != e.LMS_Status__c || oldObj.LMS_Progress__c != e.LMS_Progress__c)
				{
					LMS_Course__c lmsCourse = [select LMS_Course_ID__c from LMS_Course__c where Id = :e.LMS_Course__c ];
					Contact lmsUser = [select LMS_User_ID__c from Contact where Id = :e.LMS_User__c];
					AbsorbAPIClient.updateEnrollmentFuture(lmsUser.LMS_User_ID__c, e.LMS_Enrollment_ID__c, lmsCourse.LMS_Course_ID__c, enrollmentStatusMap.get(e.LMS_Status__c), e.LMS_Progress__c); 
				}
			}
		}
	}	
}