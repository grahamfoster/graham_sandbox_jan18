/**
* @author graham.foster@sciex.com
* @description Generic class used to replace workflow rules for mapping String Values from a source field to 
* a target field as defined in Value Mapping custom settings
*/ 
public class ValueMapperAction {

	List<Value_Mapper__c> objectValueMaps;
	List<Value_Mapper__c> masterValueMaps;
	Set<String> sourceFields;
	Set<String> masterSourceFields;

	/**
	* @description Constructor for class - uses the sobject name to get all of
	* the value mapper objects and source field names for the trigger object
	* @param sObjectName The name of the SObject so that the Value Mappings can be filtered
	*/ 
	public ValueMapperAction(String sObjectName)
	{
		List<Value_Mapper__c> valueMaps = Value_Mapper__c.getall().values();
		objectValueMaps = new List<Value_Mapper__c>();
		MasterValueMaps = new List<Value_Mapper__c>();
		sourceFields = new Set<String>();
		masterSourceFields = new Set<String>();
		for(Value_Mapper__c v : valueMaps)
		{
			if(v.sObjectType__c	== sObjectName)
			{
				if(v.isMaster__c)
				{
					MasterValueMaps.add(v);
					masterSourceFields.add(v.Source_Field__c);
				} else
				{
					objectValueMaps.add(v);
					sourceFields.add(v.Source_Field__c);
				}
				
			}
		}
	}

	/**
	* @description public method to call to map the values for a particular
	* trigger (before update or insert call)
	*/ 
	public void MapValues()
	{
		
		for(SObject s : (List<SObject>)Trigger.new)
		{
			System.debug('**GF** Mapping Values for:' + s);
			for(String str : sourceFields)
			{
				if(isChanged(s, str))
				{
					System.debug('**GF** Mapping Changed Value:' + str);
					newValue(objectValueMaps, str, (String)s.get(str), s);
				}
			}
			for(String str : masterSourceFields)
			{
				if(isChanged(s, str))
				{
					System.debug('**GF** Mapping Changed Value:' + str);
					newValue(MasterValueMaps, str, (String)s.get(str), s);
				}
			}
		}

	}

	/**
	* @description Checks if the field (according to the value map) has been changed
	* @param newSObj the new trigger variable
	* @param fieldName the API name of the field which we need to test for changes
	* @return Boolean - has fieldName been changed
	*/ 
	public Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }

	/**
	* @description adds the new value to the sObject from the trigger
	* @param sourceField the controlling field in the mapping
	* @param sourceValue the value of that controlling field
	* @param trgObject the target sObject that we need to set the value for
	*/ 
	private void newValue(List<Value_Mapper__c> vMaps, String sourceField, String sourceValue, SObject trgObject)
	{
		for (Value_Mapper__c v : vMaps)
		{
			System.debug('**GF** valueMapper Target Value:' + v.Target_Value__c);
			System.debug('**GF** valueMapper Target Value isNumber:' + v.Target_Value__c.isNumeric());
			if(v.Source_Field__c == sourceField && v.Source_Value__c == sourceValue)
			{
				if(v.Output_DataType__c.toLowerCase() == 'string')
				{
					trgObject.put(v.Target_Field__c,v.Target_Value__c);
				} else if (v.Output_DataType__c.toLowerCase() == 'boolean')
				{
					try{	        
						if(v.Target_Value__c.toLowerCase() != 'true' && v.Target_Value__c.toLowerCase() != 'false')
						{
							throw new valueException('Invalid Boolean in Value Map (' + v.Target_Value__c + ') When ' + sourceField + ' is changed to ' + sourceValue);
						} else {trgObject.put(v.Target_Field__c,v.Target_Value__c.toLowerCase() == 'true');}
					}
					catch (valueException  e){
						trgObject.addError(e.getMessage());
					}
					
				} else if (v.Output_DataType__c.toLowerCase() == 'integer')
				{
					try{	        
						if(!v.Target_Value__c.isNumeric())
						{
							throw new valueException('Invalid Integer in Value Map (' + v.Target_Value__c + ') When ' + sourceField + ' is changed to ' + sourceValue);
						} else {trgObject.put(v.Target_Field__c, Integer.valueOf(v.Target_Value__c));}
					}
					catch (valueException  e){
						trgObject.addError(e.getMessage());
					}

				} else if (v.Output_DataType__c.toLowerCase() == 'decimal')
				{
					decimal d;
					try 
					{
						d = Decimal.valueOf(v.Target_Value__c);
						trgObject.put(v.Target_Field__c, d);
					} catch (TypeException e) 
					{
						trgObject.addError('Invalid Decimal in Value Map (' + v.Target_Value__c + ') When ' + sourceField + ' is changed to ' + sourceValue);
					}
				}
				
			}
		}
	}

	/**
	* @description returns true if the custom setting has some definitions for the
	* sObject which started the trigger
	*/ 
	public Boolean mapsExistForObject
	{
		get { return objectValueMaps.size() > 0; }
	}

	public class valueException extends Exception{}

}