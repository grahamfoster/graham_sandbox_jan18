/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 12/2017
**
** Handler for Asset trigger logic
** When an Asset is updated and one of the SCIEXNow fields changes to true, then
** update the related Contact
**/
public class AssetTriggerHandler extends TriggerHandler 
{
    @TestVisible
    static Boolean firstRun = TRUE;
    
	override protected void afterInsert() {
        if(firstRun == TRUE){
        	updateRelatedContactsContentEntitlement();
            firstRun = FALSE;
        }
    }
    
	override protected void afterUpdate() {
        if(firstRun == TRUE){        
        	updateRelatedContactsContentEntitlement();
            firstRun = FALSE;
        }            
    }

    private void updateRelatedContactsContentEntitlement() {
        logEventStart();

		Set<Id> assetIds = collectIds();
        
        //Aug 15/2017 - query all the linked contacts, then query ALL the assets linked to those contact
        Set<Id> linkedContactIds = new Set<Id>();
        for(Asset_MultipleContacts__c contactLink : [
            select Id, Contact__c, Contact__r.LMS_Content_Entitled__c, Asset__c from Asset_MultipleContacts__c 
            where Asset__c in :assetIds and Contact__c != null]) 
        {
            linkedContactIds.add(contactLink.Contact__c);
        }            

        Map<Id, Map<Id, Asset>> contactsAssets = new Map<Id, Map<Id, Asset>>();
        Set<Id> allAssetIds = new Set<Id>();
        for(Asset_MultipleContacts__c assetForContactLink : [
            select Id, Contact__c, Contact__r.LMS_Content_Entitled__c, Asset__c from Asset_MultipleContacts__c 
            where Asset__c != null and Contact__c in :linkedContactIds]) 
        {
            allAssetIds.add(assetForContactLink.Asset__c);
            if(contactsAssets.containsKey(assetForContactLink.Contact__c)) {
                contactsAssets.get(assetForContactLink.Contact__c).put(assetForContactLink.Asset__c, new Asset());
            }else{
                contactsAssets.put(assetForContactLink.Contact__c, new Map<Id, Asset>{ assetForContactLink.Asset__c => new Asset() });
            }
        }
        
        //query all the assets
        Map<Id, Asset> allAssetsById = new Map<Id, Asset>([
            select Id, SCIEXNow_Hardware_Entitled__c, SCIEXNow_Software_Entitled__c, SCIEXNOWSoftware_Upgrade_Entitled__c 
            from Asset where Id in :allAssetIds]);

        //update the full asset into the contactsAssets
        for(Map<Id, Asset> assetMap : contactsAssets.values()) {
            for(Id assetId : assetMap.keySet()){
            	if(allAssetsById.containsKey(assetId)) assetMap.put(assetId, allAssetsById.get(assetId));            
            }
        }
        
        //update all the linked contacts
        Map<Id, Contact> contactUpdates = new Map<Id, Contact>();
        for(Id contactId : contactsAssets.keySet()) {
            Map<Id, Asset> assetMap = contactsAssets.get(contactId);
            Boolean anyEntitled = false;
            for(Asset assetObj : assetMap.values()) {
                if(isContentEntitled(assetObj)) {
                    anyEntitled = true;
                }
            }
            contactUpdates.put(contactId, new Contact(Id = contactId, LMS_Content_Entitled__c = anyEntitled));
        }
        
        
        //query the linked contacts
        //update the related contacts
        /*
        Map<Id, Contact> contactUpdates = new Map<Id, Contact>();
        for(Asset_MultipleContacts__c contactLink : [select Id, Contact__c, Contact__r.LMS_Content_Entitled__c, Asset__c from Asset_MultipleContacts__c where Asset__c in :assetIds and Contact__c != null]) {
            Asset ast = (Asset)getTriggeredSObject(contactLink.Asset__c);
            Boolean isAssetEntitled = isContentEntitled(ast);
            Boolean existingContactEntitled = contactLink.Contact__r.LMS_Content_Entitled__c;
            system.debug('isAssetEntitled='+isAssetEntitled+';existingContactEntitled='+existingContactEntitled);
            if(!contactUpdates.containsKey(contactLink.Contact__c)) {
                //first one, put entry in map
                if(isAssetEntitled != existingContactEntitled) {
                	contactUpdates.put(contactLink.Contact__c, new Contact(Id = contactLink.Contact__c, LMS_Content_Entitled__c = isAssetEntitled));
                }
            }else {
                //existing one, only update if this Asset is a true (i.e. only need any one to be true)
                Contact c = contactUpdates.get(contactLink.Contact__c);
                if(isAssetEntitled && !existingContactEntitled) {
                    c.LMS_Content_Entitled__c = true;
                    contactUpdates.put(c.Id, c);
                }
            }
        }
		*/
        Map<Id, Contact> existingContacts = new Map<Id,Contact>([select Id, Content_Eligible_Approved__c from Contact where Id in :contactUpdates.keySet()]);
        for(Id contactId : contactUpdates.keySet()) {
            if(existingContacts.containsKey(contactId)) {
                Contact existingContact = existingContacts.get(contactId);
                if(existingContact.Content_Eligible_Approved__c != null && existingContact.Content_Eligible_Approved__c) {
                    contactUpdates.get(contactId).LMS_Content_Entitled__c = true;
                }
            }            
        }
        
        system.debug(JSON.serializePretty(contactUpdates.values()));
        
        if(!contactUpdates.isEmpty()) update contactUpdates.values();

        logEventEnd();
    }
    
    private void logEventStart() {
        String event = null;
		if(Trigger.isInsert) event = 'INSERT';
		else if(Trigger.isUpdate) event = 'UPDATE';

        system.debug('*********** START ' + event);
    }
    
    private void logEventEnd() {
        String event = null;
		if(Trigger.isInsert) event = 'INSERT';
		else if(Trigger.isUpdate) event = 'UPDATE';

        system.debug('*********** END ' + event);
    }

    private Boolean isContentEntitled(Asset ast) {
        return ast.SCIEXNow_Hardware_Entitled__c || ast.SCIEXNow_Software_Entitled__c || ast.SCIEXNOWSoftware_Upgrade_Entitled__c;
    }

    private Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }
    
    private Set<Id> collectIds() {
        return Trigger.isDelete ? Trigger.oldMap.keySet() : Trigger.newMap.keySet();
    }
    
    private sObject getTriggeredSObject(Id sobjId) {
        return Trigger.isDelete ? Trigger.oldMap.get(sobjId) : Trigger.newMap.get(sobjId);
    }
        


    private Set<Id> collectParentIds(List<sObject> sobjs, String parentIdField)
    {
        Set<Id> ids = new Set<Id>();
        for(sObject sobj : sobjs)
        {
            if(sobj.get(parentIdField) != null) ids.add((Id)sobj.get(parentIdField));
        }
        return ids;
    }

}