/* Test method for CustomerOnboardScheduledUpdate Class
 * 
 * Class uses scheduler to update the Customer Onboard Class
 * Author Graham Foster - August 2016
 */

@isTest
public class CustomerOnboardScheduledUpdate_Test {

    public static testmethod void executeTest() {
        //Test.startTest();
        
        Customer_Onboard__c cu = new Customer_Onboard__c(Update_Record__c = false);
        insert cu;
        
        Case c = new Case(Update_Record__c = false, 
                          RecordTypeId = [SELECT Id FROM RecordType 
                                        WHERE DeveloperName = 'Training' 
                                        AND SobjectType = 'Case' LIMIT 1].Id);
        insert c;
        
        //test the scheduler - Begin by scheduling a job
        String jobId = System.schedule('testCustomerOnboardScheduledUpdate', 
                                       '0 0 0 3 9 ? 2050', 
                                       new CustomerOnboardScheduledUpdate());
        //check the cron scheduler
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered,
                          NextFireTime FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals('0 0 0 3 9 ? 2050', ct.CronExpression);
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run
        System.assertEquals('2050-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        // Veryify the value of the Update_Record checkbox
        //System.assertEquals(false, [SELECT Id, Update_Record__c FROM Customer_Onboard__c
                                   //WHERE Id=:cu.Id].Update_Record__c);
        //System.assertEquals(false, [SELECT Id, Update_Record__c FROM Case
                                   //WHERE Id=:c.Id].Update_Record__c);
        //stopping Test should fire the execute method
        //Test.stopTest();
        //check that the value is now true
        //System.assertEquals(true, [SELECT Id, Update_Record__c FROM Customer_Onboard__c
                                   //WHERE Id=:cu.Id].Update_Record__c);
        //System.assertEquals(true, [SELECT Id, Update_Record__c FROM Case
                                   //WHERE Id=:c.Id].Update_Record__c);
    }

	@IsTest static void testAsyncUpdate()
	{
		Training_Part_Processes__c tppFullLp = new Training_Part_Processes__c(Name = 'FullLp', Process_Path__c = 1);
		Product2 prodFullLp = new Product2(Name='Test FullLp', ProductCode = 'FullLp');
		insert new List<SObject>{tppFullLp, prodFullLp};

		Customer_Onboard__c cu = new Customer_Onboard__c(Update_Record__c = false, Pre_Site_Completion_Date__c = System.now());
        insert cu;
        
        Case c = new Case(Update_Record__c = false, Training_Part__c = prodFullLp.Id,
                          RecordTypeId = [SELECT Id FROM RecordType 
                                        WHERE DeveloperName = 'Training' 
                                        AND SobjectType = 'Case' LIMIT 1].Id);
        insert c;

		Test.startTest();
		CustomerOnboardScheduledUpdate cup = new CustomerOnboardScheduledUpdate();
		cup.execute(null);
		Test.stopTest();
		//check that the value is now true
        System.assertEquals(true, [SELECT Id, Update_Record__c FROM Customer_Onboard__c
                                   WHERE Id=:cu.Id].Update_Record__c);
        System.assertEquals(true, [SELECT Id, Update_Record__c FROM Case
                                   WHERE Id=:c.Id].Update_Record__c);
	}
    
}