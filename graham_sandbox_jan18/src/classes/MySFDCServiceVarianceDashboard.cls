/**
** @author Reid Beckett, Cloudware Connections
** @created Mar 10/2015
**
** Service-only dashboard showing a combo chart of the variance of Actuals vs Quota and the win rate
** 
**/
public class MySFDCServiceVarianceDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {
	public MySFDCQuota quotas {get;set;}

	public override GoogleGaugeChartData getGoogleGaugeChartData()
	{
		return null;
	}
	
	public String getReportTitle()
	{
		return 'Actuals vs. Quotas Variance';
	}

	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	protected override String criteria() {
		String crit = 'OwnerId in :userIds and IsClosed = true';
		crit += ' and RecordType.Name = \'Service\'';
		String marketVertical = filter.marketVertical;
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				crit += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				crit += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				crit += ' and Market_Vertical__c = :marketVertical';
			}
		}

		String productType = filter.productType;
		if(!String.isBlank(productType)) {
			crit += ' and Product_Type__c = :productType';
		}

		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry in :country';
			}
		}
		
		return crit;		
	}

	public GoogleComboChartData getGoogleComboChartData()
	{
		GoogleComboChartData gcd = new GoogleComboChartData('quota_variance');
		gcd.title = 'Quota vs. Actuals Variance ' + String.valueOf(Date.today().year());
		gcd.hName = 'Fiscal Quarter';
		gcd.vName = 'Variance (%)';
		//gcd.hLabels = new List<String>{ 'Q1', 'Q2', 'Q3', 'Q4' };
		gcd.vLabels = new List<String>{ 
			'Actual vs Quota Variance (%)', 'Win Rate' 
		};
		gcd.data = new List<List<Double>> {
			new List<Double>(),new List<Double>(),new List<Double>(),new List<Double>()
		};
		gcd.colors = new List<String>{
			'green', '#ff0000'
		};
		gcd.numberFormat = '##.##';

		Period currentQuarter = MySFDCUtil.getCurrentQuarter();
		Period nextQuarter = MySFDCUtil.getNextQuarter();
		Period thirdQuarter = MySFDCUtil.getThirdQuarter();
		Period fourthQuarter = MySFDCUtil.getFourthQuarter();
		
		Date startDate = currentQuarter.StartDate;
		Date endDate = fourthQuarter.EndDate;
		
		Set<Id> userIds = new Set<Id> { filter.runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(filter.runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		gcd.hLabels = new List<String>{
			'Q' + String.valueOf(currentQuarter.Number) + '/' + currentQuarter.FiscalYearSettings.Name,
			'Q' + String.valueOf(nextQuarter.Number) + '/' + nextQuarter.FiscalYearSettings.Name,
			'Q' + String.valueOf(thirdQuarter.Number) + '/' + thirdQuarter.FiscalYearSettings.Name,
			'Q' + String.valueOf(fourthQuarter.Number) + '/' + fourthQuarter.FiscalYearSettings.Name
		};
		
		Map<Integer, Double> wonMap = new Map<Integer, Double>();
		Map<Integer, Double> lostMap = new Map<Integer, Double>();

		String marketVertical = filter.marketVertical;
		String productType = filter.productType;
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);

		String soql = 'select IsWon, CloseDate, convertCurrency(Amount) from Opportunity '+
			'where CloseDate >= :startDate and CloseDate <= :endDate and ' + criteria();

		for(Opportunity opp : Database.query(soql))
		{
			Double total = 0;
			Integer mapKey = -1;
			if(opp.CloseDate >= currentQuarter.StartDate && opp.CloseDate <= currentQuarter.EndDate) {
				mapKey = 1;
			}else if(opp.CloseDate >= nextQuarter.StartDate && opp.CloseDate <= nextQuarter.EndDate) {
				mapKey = 2;
			}else if(opp.CloseDate >= thirdQuarter.StartDate && opp.CloseDate <= thirdQuarter.EndDate) {
				mapKey = 3;
			}else if(opp.CloseDate >= fourthQuarter.StartDate && opp.CloseDate <= fourthQuarter.EndDate) {
				mapKey = 4;
			}
			if(opp.IsWon)
			{
				if(wonMap.containsKey(mapKey)) total = wonMap.get(mapKey);
				if(opp.Amount != null) total += opp.Amount;
				wonMap.put(mapKey, total);
			}else{
				if(lostMap.containsKey(mapKey)) total = lostMap.get(mapKey);
				if(opp.Amount != null) total += opp.Amount;
				lostMap.put(mapKey, total);
			}
		}

		Decimal currentQuarterQuota = quotas.currentQuarterQuota;
		Decimal nextQuarterQuota = quotas.nextQuarterQuota;
		Decimal thirdQuarterQuota = quotas.thirdQuarterQuota;
		Decimal fourthQuarterQuota = quotas.fourthQuarterQuota;

		if(!String.isBlank(filter.productType)) 
		{
			currentQuarterQuota = quotas.currentQuarterQuotaByType.get(filter.productType);
			if(currentQuarterQuota == null) currentQuarterQuota = 0;
			nextQuarterQuota = quotas.nextQuarterQuotaByType.get(filter.productType);
			if(nextQuarterQuota == null) nextQuarterQuota = 0;
			thirdQuarterQuota = quotas.thirdQuarterQuotaByType.get(filter.productType);
			if(thirdQuarterQuota == null) thirdQuarterQuota = 0;
			fourthQuarterQuota = quotas.fourthQuarterQuotaByType.get(filter.productType);
			if(fourthQuarterQuota == null) fourthQuarterQuota = 0;
		}

		Double actual1 = wonMap.containsKey(1) ? wonMap.get(1) : 0;
		Double quota1 = currentQuarterQuota != null ? currentQuarterQuota : 0;
		Double variance1 = calculateVariance(actual1, quota1);
		Double lost1 = lostMap.containsKey(1) ? lostMap.get(1) : 0;
		Double winRate1 = calculateWinRate(actual1, lost1);
		gcd.data[0] = new List<Double> { variance1, winRate1 };

		Double actual2 = wonMap.containsKey(2) ? wonMap.get(2) : 0;
		Double quota2 = nextQuarterQuota != null ? nextQuarterQuota : 0;
		Double variance2 = calculateVariance(actual2, quota2);
		Double lost2 = lostMap.containsKey(2) ? lostMap.get(2) : 0;
		Double winRate2 = calculateWinRate(actual2, lost2);
		gcd.data[1] = new List<Double> { variance2, winRate2 };

		Double actual3 = wonMap.containsKey(3) ? wonMap.get(3) : 0;
		Double quota3 = thirdQuarterQuota != null ? thirdQuarterQuota : 0;
		Double variance3 = calculateVariance(actual3, quota3);
		Double lost3 = lostMap.containsKey(3) ? lostMap.get(3) : 0;
		Double winRate3 = calculateWinRate(actual3, lost3);
		gcd.data[2] = new List<Double> { variance3, winRate3 };

		Double actual4 = wonMap.containsKey(4) ? wonMap.get(4) : 0;
		Double quota4 = fourthQuarterQuota != null ? fourthQuarterQuota : 0;
		Double variance4 = calculateVariance(actual4, quota4);
		Double lost4 = lostMap.containsKey(4) ? lostMap.get(4) : 0;
		Double winRate4 = calculateWinRate(actual4, lost4);
		gcd.data[3] = new List<Double> { variance4, winRate4 };

		return gcd;
	}

	private Decimal calculateVariance(Double actual, Double quota)
	{
		if(quota == 0) return 0;
		return ((actual - quota)/quota)*100;
	}

	private Decimal calculateWinRate(Double won, Double lost)
	{
		if(won + lost == 0) return 0;
		return (won/(won + lost))*100;
	}
}