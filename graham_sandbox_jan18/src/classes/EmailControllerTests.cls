@isTest private class EmailControllerTests {

    static Contact CreateFakeContact() {
        Contact contact = new Contact();
        contact.FirstName = 'Test';
        contact.LastName = 'Test';
        contact.Email = 'test@sciex.com';
        contact.Phone = '9056609005';
        contact.MailingStreet = '71 Four Valley Drive';
        contact.MailingCity = 'Toronto';
        contact.MailingState = 'Ontario';
        contact.MailingCountry = 'Canada';
        contact.MailingPostalCode = 'L4K 4V8';
        insert contact;
        return contact;
    }

    static LicenseActivationSettings__c CreateFakeTestSettings() {
        LicenseActivationSettings__c settings = new LicenseActivationSettings__c();
        System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
        settings.Name = UserInfo.getOrganizationId();
        settings.ActivateLicenseServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/ActivateLicense';
        settings.ActivationDetailsServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/ActivationDetails';
        settings.GetTrialDetailsServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/GetTrialDetails';
        insert settings;
        return settings;
    }
    
    static Software_Activation__c CreateFakeSoftwareActivation(Contact contact) {
        Software_Activation__c activation = new Software_Activation__c();
        activation.Contact__c = contact.Id;
        activation.Activation_ID__c = '';
        insert activation;
        return activation;
    }

    static testMethod void testEmailControllerConstructor() {
        test.startTest();
        EmailController controller = new EmailController();
        test.stopTest();
    }
    
    static testMethod void testEmail() {
        RestRequest request = new RestRequest();
        RestContext.request = request;
        RestResponse response = new RestResponse();
        RestContext.response = response;
        request.params.put('activationId', '');
        Contact contact = CreateFakeContact();
        CreateFakeSoftwareActivation(contact);
        CreateFakeTestSettings();
        LicenseActivationMockJSONGenerator mockServlet = new LicenseActivationMockJSONGenerator(
            '{"success":false,"activated":true,"message":"The license key was already activated by test@sciex.com.","licenseText":"Test","licenseFilename":"SCIEXOS1.2.lic",' +
            '"licenseFileLocation":"Test","productName":"SCIEX OS Full","productVersion":"1.2","shipToEmail":"test@sciex.com",' +
            '"expirationDate":"13/06/2017","fulfillmentID":"FID_13be406e_15ace9eab2f__7ffa","hostID":"012345678901"}',
            200
        );
        Test.setMock(HttpCalloutMock.class, mockServlet);
        
        test.startTest();
        EmailController.email();
        test.stopTest();
    }
}