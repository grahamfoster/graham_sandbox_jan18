public class QuoteLineItemController {
  String errorMessage;
  Quote_Line_Item__c p;
  ApexPages.StandardController c;
  
  String quoteId = '';
  
  //---------------------------------------------------//
  //-------------GETTERS N SETTERS---------------------//
  //------------- Product Search ----------------------//
  
  public void setQuoteId(String value){   this.quoteId = value;  }
  public String getQuoteId(){    return this.quoteId;  }
  public String getErrorMessage(){
    return this.errorMessage;
   }
    
  //---------------------------------------------------//
  //-------------GENERIC FUNCTIONS---------------------//
  //---------------------------------------------------//
    
  public quoteLineItemController(ApexPages.StandardController myController) {
    c = myController;
    p = (Quote_Line_Item__c)c.getRecord();
    quoteId = ApexPages.currentPage().getParameters().get('quoteId');
    //if (p.Id == null) { setDefaultValuesOnNewQuote();}
  }
  public void saveProduct() {
    c.save();  //save controller/everything
    p = (Quote_Line_Item__c)c.getRecord();  //refresh our object
  }
  
  
  //---------------------------------------------------//
  //--------------- ACTIONS ---------------------------//
  //---------------------------------------------------//  
  public PageReference QuoteRefreshSingleLineItemPricingDRAFT(){
    //p = The quote line item that is in context for this page
    try {
      AbQuotingUtils.refreshSingleLineItemPricingInCDI( p.Quote__c, p, AbConstants.DRAFT_PRICE_UPDATE);
      return null;
    } catch (QuotingAuthenticationException authException) {
      //System.debug('QUOTING: QuotingAuthenticationException refreshingPricing for a single line item.');
      errorMessage = authException.getMessage();
      PageReference loginPageRef = Page.QuoteLogin;
      loginPageRef.getParameters().put(AbConstants.RETURN_URL_FROM_LOGIN,Page.QuoteProductSelect.getUrl()+'?Id='+p.Quote__c);
      return loginPageRef;

    } catch (Exception ex){
      //System.debug('QUOTING: error refreshing pricing: ' + ex);
      errorMessage = 'Error refreshing pricing: ' + ex.getMessage();
      return null;
    }
  }
  
  //---------------------------------------------------//
  //-------------PAGE TRANSITIONS----------------------//
  //---------------------------------------------------//
    
  public PageReference QuoteSaveDiscount() {
    saveProduct();
    System.debug('---------------------------------------------------');    
    System.debug('QUOTING: saving discount - returning to prod select for quote: ' + quoteId);
    System.debug('---------------------------------------------------');    
    return QuoteProductSelect();
  }
  public PageReference QuoteSaveMaterials() {
    if (p.Product_Desc_Override__c) {
        if (p.New_Description__c.length() > 40) {
            errorMessage = 'Product Description can not be longer than 40 characters.';
            return null;
        }   
    }
    saveProduct();
    System.debug('---------------------------------------------------');
    System.debug('---------------------------------------------------');    
    System.debug('QUOTING: saving materials - returning to prod select for quote: ' + quoteId);
    System.debug('---------------------------------------------------');
    System.debug('---------------------------------------------------');        
    return QuoteProductSelect();
  }
  
  public PageReference QuoteProductSelect() {
    
    PageReference pg = QuotePageRedirects.ProductSelect();
    pg.getParameters().put('id',quoteId);
    return pg.setRedirect(true);
  }

}