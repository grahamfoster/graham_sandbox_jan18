/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created July 24/2013
 **
 ** General Salesforce utilities that may be useful in any project
**/
public with sharing class SfdcUtil {
    private static Cache cacher = new Cache();
	
    public static RecordType getRecordType(String sobjectType, String developerName)
    {
        if(cacher.contains('RecordType', developerName)) return (RecordType) cacher.get('RecordType', developerName);

        for(RecordType rt : [select Id, Name, DeveloperName from RecordType where IsActive = true and sobjectType = :sobjectType])
        {
            cacher.put('RecordType', rt.DeveloperName, rt);
        }
        return (RecordType)cacher.get('RecordType', developerName);
    }

    public static String leftPad(String s, String padChar, integer numChars){
		if(s.length() >= numChars){
			return s;
		}else{
			while(s.length() < numChars){
				s = padChar + s;
			}
			return s;
		}
	}


	//Graham Foster adding isChangedMethod for use in triggers
	public static Boolean isChanged(sObject newSObj, Map<Id, SObject> oldMap, String fieldName, Boolean isInsertTrigger)
    {
        if(isInsertTrigger) return true;
        else {
            sObject oldSobj = oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }

	public static Boolean isInformatica()
	{
		return UserInfo.getUserName().indexOf('informatica.robot') >= 0;
	}


	/**
	 ** When catching a DML exception, add the messages to the pageMessages
	**/
    public static void addDMLExceptionMessagesToPage(DMLException e) {
        for(integer i = 0; i < e.getNumDml(); i++){
        	if(e.getDmlType(i) == StatusCode.DUPLICATE_VALUE) {
            	//ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, '<a href="/' + e.getDmlId(i) + '">'+e.getDmlId(i)+'</a>'));
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getDmlMessage(i)));
        	}else{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getDmlMessage(i)));
        	}
        }
    }

	/**
	 ** When catching a general exception, add the message to the pageMessages
	**/
    public static void addExceptionMessageToPage(Exception e) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
    }
    
	/**
	 ** Add an error message to the page (RED)
	**/
    public static void addErrorMessageToPage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, msg));
    }

	/**
	 ** Add a confirm message to the page (GREEN)
	**/
    public static void addConfirmMessageToPage(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, msg));
    }

    /**
    ** Get the Schema.DescribeFieldResult for a field.  Example, Util.getDescribeFieldResult(Opportunity.Name)
    **/
    public static Schema.DescribeFieldResult getDescribeFieldResult(Schema.SObjectField sobjectField){
    	return sobjectField.getDescribe();
    }

    /**
    ** Get the Schema.DescribeFieldResult for a field.  Example, Util.getFieldLength(Opportunity.Name)
    **/
    public static Integer getFieldLength(Schema.SObjectField sobjectField){
    	return getDescribeFieldResult(sobjectField).getLength();
    }

    /** generic cache implementation **/
    class Cache 
    {
	    private Map<String, Map<String, Object>> cacheMap;
        
        public Cache()
        {
            this.cacheMap = new Map<String, Map<String,Object>>();
        }
    	
        public void put(String cacheId, String key, Object obj) 
        {
            if(!cacheMap.containsKey(cacheId)) cacheMap.put(cacheId, new Map<String, Object>());
            cacheMap.get(cacheId).put(key, obj);
        }

        public Object get(String cacheId, String key) 
        {
            if(!cacheMap.containsKey(cacheId)) return null;
            else return cacheMap.get(cacheId).get(key);
        }        
        
        public Boolean contains(String cacheId, String key)
        {
            return cacheMap.containsKey(cacheId) && cacheMap.get(cacheId).containsKey(key);
        }
    }

}