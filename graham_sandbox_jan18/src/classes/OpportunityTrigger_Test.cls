/*
 *	OpportunityTrigger_Test
 *	
 *	Test class for OpportunityTrigger and OpportunityTriggerHandler.
 *
 *	If there are other test classes related to OpportunityTrigger, please document it here (as comments).
 * 
 * 	Created by Graham Foster on 2016-08-30
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
public class OpportunityTrigger_Test {
    
    // IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] OpportunityTrigger_Test() [Begin]');
    	Opportunity iudu = new Opportunity(Name = 'Test Task', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25));
		insert iudu;
		update iudu;
		delete iudu;
		undelete iudu;
        system.debug('[TEST] TaskTrigger_Test() [Begin]');
     }
    
    @IsTest
	public static void test_createCOBOforStandardOpportunity()
	{
		Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', 
                                Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
		
		Contact c = new Contact(FirstName = 'John', LastName = 'Doe',
                                AccountId = a.Id);
        insert c;
		Id EMEAOpp = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('EU ABS Opportunity').getRecordTypeId();
		Id AMEROpp = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('NA PSM ABS').getRecordTypeId();
        Opportunity o = new Opportunity(Name = 'TestOpp', Product_System__c = 'LC MS', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id, RecordTypeId = EMEAOpp, LC_Vendor__c = 'Test');
		Opportunity o1 = new Opportunity(Name = 'TestOpp1', Product_System__c = 'LC MS', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id,
                                       RecordTypeId = AMEROpp, LC_Vendor__c = 'Test');
		Opportunity o2 = new Opportunity(Name = 'TestOpp2', Product_System__c = 'LC MS', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id,
                                       RecordTypeId = EMEAOpp, LC_Vendor__c = 'Test');
		Opportunity o3 = new Opportunity(Name = 'TestOpp3', Product_System__c = 'MS only', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id,
                                       RecordTypeId = AMEROpp, LC_Vendor__c = 'Test');
		Opportunity o4 = new Opportunity(Name = 'TestOpp4', Product_System__c = 'MS only', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id,
                                       RecordTypeId = EMEAOpp, MS_Product__c = 'TestMSProduct', LC_Vendor__c = 'Test');
		Opportunity o5 = new Opportunity(Name = 'TestOpp5', Product_System__c = 'TEST', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id,
                                       RecordTypeId = AMEROpp, LC_Vendor__c = 'Test');
        insert new List<Opportunity>{o,o1,o2,o3,o4,o5};

		Customer_Onboard__c cobo = new Customer_Onboard__c(Opportunity__c = o.Id, Region__c = 'Test Region');
        insert cobo;

		Test.startTest();
		o.In_Forecast_Mgr__c = true;
		o1.In_Forecast_Mgr__c = true;
		o2.Mgr_Upside__c = true;
		o3.In_Forecast_Mgr__c = true;
		o4.Mgr_Upside__c = true;
		o5.Mgr_Upside__c = true;
		update new List<Opportunity>{o,o1,o2,o3,o4,o5};
		Test.stopTest();

		List<Customer_Onboard__c> cobos = [SELECT Id, Opportunity__c, Region__c, Opportunity_Account__c, Opportunity_MS__c, Primary_Contact_from_Opportunity__c 
		FROM Customer_Onboard__c WHERE Opportunity__c = :o.Id OR Opportunity__c = :o1.Id OR Opportunity__c = :o2.Id OR Opportunity__c = :o3.Id 
		OR Opportunity__c = :o4.Id OR Opportunity__c = :o5.Id];

		//check that there are 6 cobos (so an extra one wasnt created for o)
		System.assertEquals(5, cobos.size());

		//make sure there was not a cobo created for o5 because of the product system
		System.assertEquals(0,[SELECT COUNT() FROM Customer_Onboard__c WHERE Opportunity__c = :o5.Id]);

		Set<Id> coboIds = new Set<Id>();
		
		for(Customer_Onboard__c cob : cobos)
		{
			if(cob.Opportunity__c == o.Id)
			{
				System.assertEquals('Test Region', cob.Region__c);
				coboIds.add(cob.Id);
			} else if(cob.Opportunity__c == o1.Id)
			{
				System.assertEquals('AMERICAS', cob.Region__c);
				coboIds.add(cob.Id);
			} else if(cob.Opportunity__c == o2.Id)
			{
				System.assertEquals('EMEAI', cob.Region__c);
				coboIds.add(cob.Id);
			} else if(cob.Opportunity__c == o3.Id)
			{
				System.assertEquals(a.Id, cob.Opportunity_Account__c);
				coboIds.add(cob.Id);
			} else if(cob.Opportunity__c == o4.Id)
			{
				System.assertEquals('TestMSProduct', cob.Opportunity_MS__c);
				coboIds.add(cob.Id);
			} 
		}
		//make sure that we tested 5 cobos
		System.assertEquals(5,coboIds.size());
	}

	@IsTest
	public static void test_updateCOBOonOppOwnerChange()
	{
		Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', 
                                Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
		
		Contact c = new Contact(FirstName = 'John', LastName = 'Doe',
                                AccountId = a.Id);
        insert c;
		
		Opportunity o = new Opportunity(Name = 'TestOpp', Product_System__c = 'LC MS', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Shippable_Date__c = Date.newInstance(2040, 12, 25),
										AccountId = a.Id, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test');
		insert o;

		Customer_Onboard__c cobo = new Customer_Onboard__c(Opportunity__c = o.Id, Region__c = 'Test Region');
        insert cobo;

		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u1 = new User(Alias = 'usr1MGR', Email='usr1M@testorg.test', EmailEncodingKey='UTF-8', LastName='User1Manager', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='usr1M@testorg.test');
		insert u1;
		User u = new User(Alias = 'usr1', Email='usr1@testorg.test', EmailEncodingKey='UTF-8', LastName='User1', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='usr1@testorg.test', ManagerId = u1.Id);
		
		insert u;

		Test.startTest();
			o.OwnerId = u.Id;
			update o;
		Test.stopTest();
		cobo = [SELECT OwnerId, Sales_Manager__c, Sales_Representative__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals(u.Id, cobo.OwnerId);
		System.assertEquals(u.Id, cobo.Sales_Representative__c);
		System.assertEquals(u1.Id, cobo.Sales_Manager__c);

	}

	@IsTest static void test_reopenCOBOWhenNotLost()
	{
		Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', 
                                Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;

		Contact c = new Contact(FirstName = 'John', LastName = 'Doe',
                                AccountId = a.Id);
        insert c;
        Opportunity o = new Opportunity(Name = 'TestOpp', Product_System__c = 'LC MS', StageName = 'Deal Lost', 
                                        CloseDate = Date.newInstance(2040, 12, 25), AccountId = a.Id, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test'
                                       );
        insert o;
        
        Customer_Onboard__c cobo = new Customer_Onboard__c(Opportunity__c = o.Id, Status__c = 'Lost/Cancelled');
        insert cobo;
        //reopes the opp and this should 'New' the cobo
        o.StageName = 'Recognition of Needs';
        update o;
        cobo = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
        //verify that its status has been changed
        system.assertEquals('New', cobo.Status__c);
	}


    public static testmethod void test_CloseCOBOWhenOppLost(){
        //make some records
        Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', 
                                Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
        String s = [SELECT Sales_Region__c FROM Account WHERE Id=:a.Id][0].Sales_Region__c;
        system.debug('**GF** Account Sales Region:' + s);
		Contact c = new Contact(FirstName = 'John', LastName = 'Doe',
                                AccountId = a.Id);
        insert c;
        Opportunity o = new Opportunity(Name = 'TestOpp', Product_System__c = 'LC MS', StageName = 'Recognition of Needs', 
                                        CloseDate = Date.newInstance(2040, 12, 25), Account = a, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test'
                                       );
        insert o;
        
        Customer_Onboard__c cobo = new Customer_Onboard__c(Opportunity__c = o.Id);
        insert cobo;
        //close the opp and this should kill the cobo
        o.StageName = 'Deal Lost';
        update o;
        List<Customer_Onboard__c> cobo2 = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
        //verify that its status has been changed
        system.assertEquals('Lost/Cancelled', cobo2[0].Status__c);
    }

    //cover the check that there is a primary learner on Fast record type Opp
    public static testmethod void test_validateContactRoles_fail() {
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada');
        insert a;

        Opportunity opp = new Opportunity(RecordTypeId = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Fast').getRecordTypeId(), 
            AccountId = a.Id, Name = 'Test Opp', CloseDate = Date.today().addDays(1), StageName = 'Recognition of Needs', LC_Vendor__c = 'Test'
        );
        insert opp;

		createTrainingLineItem(opp.Id);

        //attempt to close opp without a contact role of primary learner
        opp.StageName = 'Deal Won';
        try {
            update opp;
            system.assert(false);
        }catch(DmlException e) {

        }
    }

    //cover the check that there is a primary learner on Fast record type Opp
    public static testmethod void test_validateContactRoles_success() {
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada');
        insert a;

        Contact c = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert c;

        Opportunity opp = new Opportunity(RecordTypeId = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Fast').getRecordTypeId(), 
            AccountId = a.Id, Name = 'Test Opp', CloseDate = Date.today().addDays(1), StageName = 'Recognition of Needs', Product_System__c = 'SCIEXUniversity Training'
        );
        insert opp;

        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = opp.Id, Role = 'Primary Learner', ContactId = c.Id);
        insert ocr;

        //attempt to close opp WITH a contact role of primary learner, should succeed
        opp.StageName = 'Deal Won';
        update opp;
        system.assert(true);
    }
    
    //Test for when a Fast Opportunity is closed and a cobo doesn't already exists, create it.
	public static testmethod void test_createCOBOForFastOpportunityWhenOppWon_newCOBO(){
        Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
        
        Asset ast = new Asset(Name = 'Test Asset 1', AccountId = a.Id, SerialNumber = '123456');
        insert ast;

        Account a2 = new Account(Name = 'TestAccount 2', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a2;

        Asset ast2 = new Asset(Name = 'Test Asset 2', AccountId = a2.Id, SerialNumber = '123456');
        insert ast2;

		Contact c = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert c;
        
        Id fastRecordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
        Opportunity o = new Opportunity(Name = 'TestOpp', StageName = 'Recognition of Needs', 
			RecordTypeId = fastRecordTypeId, /*Serial_Number__c = '123456', */ Asset__c = ast.Id,                                       
			CloseDate = Date.today().addYears(1), AccountId = a.Id, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test'    
		);
        insert o;
        
        //Insert line item with Igor_Class__c = H
        createTrainingLineItem(o.Id);
        
        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = o.Id, Contactid = c.Id, Role = 'Primary Learner');
        insert ocr;

        Test.startTest();
        //win the opp
        o.StageName = 'Deal Won';
        update o;
        
        List<Customer_Onboard__c> cobos = [SELECT Id, Status__c, Asset__c FROM Customer_Onboard__c WHERE Opportunity__c = :o.Id];
        system.assertEquals(1, cobos.size());
        system.assertEquals('New', cobos[0].Status__c);
        system.assertEquals(ast.Id, cobos[0].Asset__c);
        //system.assertEquals('Requested', [select Portal_Status__c from Contact where Id = :c.Id].Portal_Status__c);

        Test.stopTest();
    }

    //Test for when a Fast Opportunity is closed and a cobo doesn't already exists, create it.
	public static testmethod void test_createCOBOForFastOpportunityWhenOppWon_newCOBO2(){
        Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
        
        Asset ast = new Asset(Name = 'Test Asset 1', AccountId = a.Id, SerialNumber = '123456');
        insert ast;

        Account a2 = new Account(Name = 'TestAccount 2', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a2;

        Asset ast2 = new Asset(Name = 'Test Asset 2', AccountId = a2.Id, SerialNumber = '123456');
        insert ast2;

		Contact c = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert c;
        
        Id fastRecordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
        Opportunity o2 = new Opportunity(Name = 'TestOpp', StageName = 'Recognition of Needs', 
			RecordTypeId = fastRecordTypeId, Serial_Number__c = '123456',                                       
			CloseDate = Date.today().addYears(1), AccountId = a.Id, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test'     
		);
        insert o2;
        
        //Insert line item with Igor_Class__c = H
        createTrainingLineItem(o2.Id);

        OpportunityContactRole ocr2 = new OpportunityContactRole(OpportunityId = o2.Id, Contactid = c.Id, Role = 'Primary Learner');
        insert ocr2;
	
        //win the opp
        Test.startTest();
        o2.StageName = 'Deal Won';
        update o2;
        Test.stopTest();

        List<Customer_Onboard__c> cobos2 = [SELECT Id, Status__c, Asset__c FROM Customer_Onboard__c WHERE Opportunity__c = :o2.Id];
        system.assertEquals(1, cobos2.size());
        system.assertEquals('New', cobos2[0].Status__c);
        system.assertEquals(ast.Id, cobos2[0].Asset__c);
    }

    //Test for when a Fast Opportunity is closed and a cobo already exists, do not create a second one.
	public static testmethod void test_createCOBOForFastOpportunityWhenOppWon_existingCOBO(){
        Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
        
        Asset ast = new Asset(Name = 'Test Asset 1', AccountId = a.Id, SerialNumber = '123456');
        insert ast;

        Account a2 = new Account(Name = 'TestAccount 2', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a2;

        Asset ast2 = new Asset(Name = 'Test Asset 2', AccountId = a2.Id, SerialNumber = '123456');
        insert ast2;

		Contact c = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert c;
        
        Id fastRecordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
        Opportunity o = new Opportunity(Name = 'TestOpp', StageName = 'Recognition of Needs', 
			RecordTypeId = fastRecordTypeId, Serial_Number__c = '123456',                                        
			CloseDate = Date.today().addYears(1), AccountId = a.Id, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test'
		);
        insert o;

        //Insert line item with Igor_Class__c = H
        createTrainingLineItem(o.Id);

        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = o.Id, Contactid = c.Id, Role = 'Primary Learner');
        insert ocr;
        
        o = [select Id, OwnerId, Opportunity_ID_Number__c, MS_Product__c, Primary_Contact__c, Manager__c from Opportunity where Id = :o.Id];
        
        //create COBOs before winning it.
        Customer_Onboard__c cobo = new Customer_Onboard__c();
        cobo.Opportunity__c = o.Id;
        cobo.OwnerId = o.OwnerId;
        cobo.Opp_Order_No__c = o.Opportunity_ID_Number__c;
        cobo.Opportunity_Account__c = a.Id;
        cobo.Opportunity_MS__c = o.MS_Product__c;
        cobo.Primary_Contact_from_Opportunity__c = o.Primary_Contact__c;
        cobo.Region__c = a.Sales_Region__c;
        cobo.Sales_Manager__c = o.Manager__c;
        cobo.Sales_Representative__c = o.OwnerId;
        cobo.Status__c = 'New';
        insert cobo;
		
        Test.startTest();
        //win the opp
        o.StageName = 'Deal Won';
        update o;
        Test.stopTest();
        
        List<Customer_Onboard__c> cobos = [SELECT Id, Status__c, Asset__c FROM Customer_Onboard__c WHERE Opportunity__c = :o.Id];
        system.assertEquals(1, cobos.size());
        system.assertEquals(cobo.Id, cobos[0].Id);
        system.assertEquals('New', cobos[0].Status__c);
    }
	
    //Test for when a Fast Opportunity is closed and a cobo already exists, do not create a second one and also has a contact don't
    //create a second contact
    public static testmethod void test_createCOBOForFastOpportunityWhenOppWon_existingCOBOAndContact(){
        Country_Mapping__c cm = new Country_Mapping__c(Name = 'Canada', Sales_Region__c = 'AMERICAS', Country_Code__c = 'CA');
        insert cm;
        
        Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a;
        
        Asset ast = new Asset(Name = 'Test Asset 1', AccountId = a.Id, SerialNumber = '123456');
        insert ast;

        Account a2 = new Account(Name = 'TestAccount 2', BillingCountry = 'Canada', Country_Mapping__c = cm.Id, Sales_Region__c = 'AMERICAS');
        insert a2;

        Asset ast2 = new Asset(Name = 'Test Asset 2', AccountId = a2.Id, SerialNumber = '123456');
        insert ast2;

		Contact c = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert c;
        
        Id fastRecordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
        Opportunity o = new Opportunity(Name = 'TestOpp', StageName = 'Recognition of Needs', 
			RecordTypeId = fastRecordTypeId, Serial_Number__c = '123456',                                        
			CloseDate = Date.today().addYears(1), AccountId = a.Id, Primary_Contact__c = c.Id, LC_Vendor__c = 'Test'
		);
        insert o;
        
        //Insert line item with Igor_Class__c = H
        createTrainingLineItem(o.Id);

        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = o.Id, Contactid = c.Id, Role = 'Primary Learner');
        insert ocr;

        o = [select Id, OwnerId, Opportunity_ID_Number__c, MS_Product__c, Primary_Contact__c, Manager__c from Opportunity where Id = :o.Id];
        
        //create COBOs before winning it.
        Customer_Onboard__c cobo = new Customer_Onboard__c();
        cobo.Opportunity__c = o.Id;
        cobo.OwnerId = o.OwnerId;
        cobo.Opp_Order_No__c = o.Opportunity_ID_Number__c;
        cobo.Opportunity_Account__c = a.Id;
        cobo.Opportunity_MS__c = o.MS_Product__c;
        cobo.Primary_Contact_from_Opportunity__c = o.Primary_Contact__c;
        cobo.Region__c = a.Sales_Region__c;
        cobo.Sales_Manager__c = o.Manager__c;
        cobo.Sales_Representative__c = o.OwnerId;
        cobo.Status__c = 'New';
        insert cobo;
        
        OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Type__c = ocr.Role, Role__c = ocr.Role, OnBoard_Contact__c = ocr.ContactId, Customer_Onboard__c = cobo.Id);
        insert obc;
        
        //win the opp
        Test.startTest();
        o.StageName = 'Deal Won';
        update o;
        Test.stopTest();
        
        List<Customer_Onboard__c> cobos = [SELECT Id, Status__c, Asset__c FROM Customer_Onboard__c WHERE Opportunity__c = :o.Id];
        system.assertEquals(1, cobos.size());
        system.assertEquals(cobo.Id, cobos[0].Id);
        system.assertEquals('New', cobos[0].Status__c);
    }

    @testSetup
    public static void setUp(){
        //  Product2
        List<Product2> prod = new List<Product2>{
            new Product2(Name='ABSX WARRANTY',ProductCode='ABSX WARRANTY', Igor_Class__c = 'H')
		};
        insert prod;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode='USD');
		insert pbe;
    }
    
    private static OpportunityLineItem createTrainingLineItem(Id oppId) {
        OpportunityLineItem oli = new OpportunityLineItem(OpportunityId = oppId, Igor_Class__c = 'H', PricebookEntryId = getTestPricebookEntryId(), Quantity = 1, UnitPrice = 10000);
        insert oli;
        return oli;
    }
    
    private static Id getTestPricebookEntryId() {
        ID stdPBID = Test.getStandardPricebookId();
        return [select Id from PricebookEntry where CreatedById = :UserInfo.getUserId() and Pricebook2Id = :stdPBID and IsActive = true LIMIT 1].Id;
    }
}