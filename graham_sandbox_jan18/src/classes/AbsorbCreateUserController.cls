// ===========================================================================
// Object: AbsorbCreateUserController
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: controller for the AbsorbCreateUser VF page
// ===========================================================================
// Changes: 2016-02-10 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class AbsorbCreateUserController 
{
	private Contact contactSobj;
	
	public AbsorbCreateUserController(ApexPages.StandardController stdController) 
	{
		this.contactSobj = AbsorbUtil.findContact(stdController.getId());
	}

	public PageReference backToContact()
	{
		return new PageReference('/' + contactSobj.Id).setRedirect(true);
	}

	public PageReference createUser()
	{
		if(this.contactSobj.LMS_User_ID__c != null) {
			SfdcUtil.addErrorMessageToPage('LMS User ID should not be set');
			return null;
		}
		AbsorbAPIClient.IAbsorbAPIClient absorbClient = null;
		try {
			absorbClient = AbsorbAPIClient.getInstance();
			AbsorbModel.User u = AbsorbUtil.createAbsorbUser(this.contactSobj);
			AbsorbModel.CreateUserResponse creatUserResponse = absorbClient.createUser(u);
			this.contactSobj.LMS_User_ID__c = creatUserResponse.Id;
			this.contactSobj.LMS_User_Created_Date__c = DateTime.now();
			if(String.isBlank(this.contactSobj.LMS_Username__c)) this.contactSobj.LMS_Username__c = this.contactSobj.Email;
			update this.contactSobj;
			SfdcUtil.addConfirmMessageToPage('Absorb LMS User created successfully: Id = ' + creatUserResponse.Id);
		}catch(Exception e){
			AbsorbLogger.error(e.getMessage() + '\n' + e.getStackTraceString());
			SfdcUtil.addErrorMessageToPage(e.getMessage());
		}finally{
			if(absorbClient != null) absorbClient.close();
		}
		return null;
	}
}