global class SchedulableSMC2Opportunity implements Schedulable{
	
    global void execute(SchedulableContext sc) {
        
        String Query = 'SELECT id, Ownerid, SVC_Oracle_Status__c, CurrencyISOCode, SVMXC__COMPANY__C, SVMXC__CONTACT__C, SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__C, SVC_ITEM_MASTER_NAME__C,SVC_SALESPERSON__C,SVC_CATEGORY__C,Renewal_Opportunity_Created__c From SVMXC__Service_Contract__c WHERE Renewal_Opportunity_Created__c=false AND SVMXC__Active__c = TRUE AND SVMXC__END_DATE__C = THIS_FISCAL_YEAR';
		Integer scope = 50;
       
        BatchSMContract2Opportunity b = new BatchSMContract2Opportunity(Query);
		id batchInstanceId = database.executebatch(b,scope);
	}
}