/**
* @author graham.foster@sciex.com.smax
* @description Controller class for AssignToConsole Visualforce page which assigns 
* the ownership of cases using the Custom Console Componenet
* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/60227585/AssignToConsoleExtension
*/ 
global with sharing class AssignToConsoleExtension {

	public AssignToConsoleExtension() {}

	/**
	* @description Assigns the case passed in the argument to the executing user (as owner)
	* @param caseId The Id of the case which needs to be re-assigned
	* @return The modified case
	*/ 
	@RemoteAction
    global static Case assignToSelf(Id caseId){
        Case c = new Case(Id = caseId, OwnerId = UserInfo.getUserId());
        update c;
		//have a look for an active call for myself and if there is one assign it to this case
		Task updateTask = CTIUtil.CurrentCallTask();
		if(updateTask != null)
        {
            updateTask.WhatId = caseId;
			update updateTask;
		}
        return c;
    }
    
	/**
	* @description Assigns the case passed in the argument to the CXC Queue 
	* (as defined in the Account.Country_Mapping__r.CXC_Queue__c)
	* @param caseId The Id of the case which needs to be re-assigned
	* @return The modified case
	*/ 
    @RemoteAction
    global static Case assignToCXAgent(Id caseId){
        Case cO = [SELECT Id, Account.Country_Mapping__r.CXC_Queue__c FROM Case WHERE Id = :caseId LIMIT 1];
        if(!String.isEmpty(cO.Account.Country_Mapping__r.CXC_Queue__c))
        {
          	Group queueId = [SELECT Id FROM Group WHERE Name = :cO.Account.Country_Mapping__r.CXC_Queue__c AND Type = 'Queue' LIMIT 1];
        	Case c = new Case(Id = caseId, OwnerId = queueId.Id);
        	update c;
        	return c;  
        }
        return null;
    }
    
	/**
	* @description Assigns the case passed in the argument to the Office Queue 
	* (as defined in the Account.Country_Mapping__r.Office_Queue__c)
	* @param caseId The Id of the case which needs to be re-assigned
	* @return The modified case
	*/ 
    @RemoteAction
    global static Case assignToCXOffice(Id caseId){
        Case cO = [SELECT Id, Account.Country_Mapping__r.Office_Queue__c FROM Case WHERE Id = :caseId LIMIT 1];
        if(!String.isEmpty(cO.Account.Country_Mapping__r.Office_Queue__c))
        {
          	Group queueId = [SELECT Id FROM Group WHERE Name = :cO.Account.Country_Mapping__r.Office_Queue__c AND Type = 'Queue' LIMIT 1];
        	Case c = new Case(Id = caseId, OwnerId = queueId.Id);
        	update c;
        	return c;  
        }
        return null;
    }
    
	/**
	* @description Assigns the case passed in the argument to the TAC Queue 
	* (as defined in the Account.Country_Mapping__r.TAC_Queue__c)
	* @param caseId The Id of the case which needs to be re-assigned
	* @return The modified case
	*/ 
    @RemoteAction
    global static Case assignToTAC(Id caseId){
        Case cO = [SELECT Id, Account.Country_Mapping__r.TAC_Queue__c FROM Case WHERE Id = :caseId LIMIT 1];
        if(!String.isEmpty(cO.Account.Country_Mapping__r.TAC_Queue__c))
        {
          	Group queueId = [SELECT Id FROM Group WHERE Name = :cO.Account.Country_Mapping__r.TAC_Queue__c AND Type = 'Queue' LIMIT 1];
        	Case c = new Case(Id = caseId, OwnerId = queueId.Id);
        	update c;
        	return c;  
        }
        return null;
    }
    
	/**
	* @description Assigns the case passed in the argument to the SAS Queue 
	* (as defined in the Account.Country_Mapping__r.SAS_Queue__c)
	* @param caseId The Id of the case which needs to be re-assigned
	* @return The modified case
	*/ 
    @RemoteAction
    global static Case assignToSAS(Id caseId){
        Case cO = [SELECT Id, Account.Country_Mapping__r.SAS_Queue__c FROM Case WHERE Id = :caseId LIMIT 1];
        if(!String.isEmpty(cO.Account.Country_Mapping__r.SAS_Queue__c))
        {
          	Group queueId = [SELECT Id FROM Group WHERE Name = :cO.Account.Country_Mapping__r.SAS_Queue__c AND Type = 'Queue' LIMIT 1];
        	Case c = new Case(Id = caseId, OwnerId = queueId.Id);
        	update c;
        	return c;  
        }
        return null;
    }
    
	/**
	* @description Assigns the case passed in the argument to the Spam Queue 
	* @param caseId The Id of the case which needs to be re-assigned
	* @return The modified case
	*/ 
    @RemoteAction
    global static Case assignToSpam(Id caseId){
        Case c = new Case(Id = caseId, OwnerId = '00GF0000004vCDqMAM');
        update c;
        return c;
    }

}