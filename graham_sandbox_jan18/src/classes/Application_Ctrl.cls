public class Application_Ctrl {
    public Application__c applicationRecord{get; set;}   
    public Boolean isErrorMsg{get;set;} 
    public Boolean isErrorMsgdiv{get;set;}
    public Boolean isErrorMsgimageupload{get;set;}
    public Boolean isErrorMsgimageext{get;set;}
    
     
     //===============================================   
    public string attachid{get;set;}
    public transient Attachment attach {get;set;}
    public transient blob file1{get;set;}
    public String fname{get;set;}   
    public string contentype{get;set;} 
    
    
    public Application_Ctrl(ApexPages.StandardController controller) 
    {   
        isErrorMsg = true; 
        applicationRecord = (Application__c)controller.getRecord();        
    }    
    /**** Extension save method *****/    
    public void doSave() {
        try{
        	if(file1 == null){
	        	isErrorMsg = true;
	        	if(String.isNotEmpty(applicationRecord.Application_Name__c)) {
	            	upsert applicationRecord;
	            	isErrorMsg = false;
	            	 isErrorMsgdiv=false;
	                  isErrorMsgimageext=false;
	                  isErrorMsgimageupload=false;
	        	}
	        	   else {
	                 system.debug('second block');
	                 isErrorMsg = true; 
	                 isErrorMsgdiv=true;
	                  isErrorMsgimageext=false;
	                  isErrorMsgimageupload=false;
	                 //ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ' Application Name: You must enter a value'));
	                 
	             }     
        	}
        	 if(file1 != null){    
                attach = new Attachment();
                attach.Name = fname ;
                attach.ContentType = contentype;
                attach.body=file1; 
                if (attach.Body.size() > 2000000 ) {
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Maximum Size Limit Exceeds! You can not upload more than 2MB.'));
                    isErrorMsgimageupload=true;
                   
                }   
                if(attach != null && attach.Body.size() < 2000000 && (attach.ContentType == 'image/jpg' || attach.ContentType =='image/jpeg' || attach.ContentType == 'image/png' || attach.ContentType == 'image/gif' )){ 
             		try{
             			if(applicationRecord != null && String.isNotEmpty(applicationRecord.Application_Name__c)){
	                    upsert applicationRecord;
             		    }
             		       attach.ParentId = applicationRecord.Id;     
                            upsert attach;
                            attachid = attach.id;
                         }
                         catch (DMLException e) {
                             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'+e.getMessage()));
                            
                         }finally {
                            attach = new Attachment(); 
                         }
                        system.debug('attachid....' +attachid);
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Application Saved successfully'));
                 }else{            
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Only .jpg/.jpeg/.png/.gif types of file for Image are supported.'));
                    isErrorMsgimageext=true;
                    file1 = null; 
                    attach=new Attachment();
                    
                 }
             			
             	if(attachid != null && attachid.trim() != '' && applicationRecord.id !=null)
                {
                    applicationRecord.Image__c = '/servlet/servlet.FileDownload?file='+attachid;
                   
                    update applicationRecord;
                }
  	    	 } 
     
        }catch(Exception e){isErrorMsg = true;ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));}
    }    
    /**** Extension save & new method *****/
    public PageReference doSaveNew(){
        try{
            insert applicationRecord;
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }
        if(applicationRecord.Id != null || applicationRecord.Id != '')
        {
            PageReference page = new PageReference('/'+applicationRecord.Id);
            return page;
        }
        return null;                    
    }    
    /** Extension cancel method  **/ 
    public PageReference doCancel(){
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
                return new PageReference (cancelURL);
        }
        return null;
    }
    
    /** delete application category not applicatoion varoiants **/ 
    public void doDelete(){
        if(applicationRecord != null && applicationRecord.Id != null){
            try{
                isErrorMsg = false;
                delete applicationRecord;
            } catch(Exception e){
                isErrorMsg = true;
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));    
            }  
        }    
    }
    
        public pagereference redirectadmin(){
        pagereference pageref = new pagereference('/apex/mainpageadmin?tabfocus=DataTab');
        return pageref;
        
    }

}