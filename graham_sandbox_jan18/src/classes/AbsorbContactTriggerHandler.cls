// ===========================================================================
// Object: AbsorbContactTriggerHandler
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Handles contact trigger to create an LMS user when the LMS_Trigger_User_Create__c checkbox is checked
// ===========================================================================
// Changes: 2016-02-09 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class AbsorbContactTriggerHandler
{
}