public class MySFDCFullQtrYTDActualsDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {
	public Boolean rendered {get;set;}

	public MySFDCFullQtrYTDActualsDashboard() {
		super();
		//rendered = MySFDCUtil.getCurrentQuarter().Number > 1;
		//change: Jan 13/15
		rendered = true;
		report = 'FullYTDActuals';
	}

	public String getReportTitle() {
		return 'YTD FQ Actuals';
	}

	protected override String criteria() {
		//by default get all opps that are not closed lost
		//String crit = 'OwnerId in :userIds and (IsClosed = false or IsWon = false)';
		String crit = 'OwnerId in :userIds ' + 
			'and IsWon = true ' +
			'and ((Shippable_Date__c >= :startDate and Shippable_Date__c <= :endDate) ' +
			'or (Shippable_Date__c = NULL and CloseDate >= :startDate and CloseDate <= :endDate))';
		if(MySFDCUtil.isServiceUser(filter.runAs)) {
			crit += ' and RecordType.Name = \'Service\'';
		}
		if(!String.isBlank(filter.marketVertical)) {
			crit += ' and Market_Vertical__c = :filter.marketVertical';
		}
		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry = :country';
			}
		}
		return crit;
	}

	public override void query() {
		Period currentQuarter = MySFDCUtil.getCurrentQuarter();
		if(currentQuarter.Number > 0) {
			List<AggregateResult> fullQtrQuotas = null;
			String fyName = String.valueOf(Date.today().year());
			Date d1YTD = Date.newInstance(Date.today().year(), 1, 1);
			//Date d2YTD = currentQuarter.StartDate.addDays(-1);
			Date d2YTD = Date.today();
			Id runAsId = runAs.Id;

			//change: Jan 13/15
			/*
			if(currentQuarter.Number == 4) {
				fullQtrQuotas = [
					select SUM(Quota__c) quota from Quota__c 
					where ((Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName) OR
					(Fiscal_Quarter__c = '2' and Fiscal_Year__c = :fyName) OR 
					(Fiscal_Quarter__c = '3' and Fiscal_Year__c = :fyName))
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}else if(currentQuarter.Number == 3) {
				fullQtrQuotas = [select SUM(Quota__c) quota from Quota__c where 
					((Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName) OR
					(Fiscal_Quarter__c = '2' and Fiscal_Year__c = :fyName))
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}else if(currentQuarter.Number == 2) {
				fullQtrQuotas = [select SUM(Quota__c) quota from Quota__c where 
					(Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName)
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}
			*/
			if(currentQuarter.Number == 4) {
				fullQtrQuotas = [
					select SUM(Quota__c) quota from Quota__c 
					where ((Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName) OR
					(Fiscal_Quarter__c = '2' and Fiscal_Year__c = :fyName) OR 
					(Fiscal_Quarter__c = '3' and Fiscal_Year__c = :fyName) OR 
					(Fiscal_Quarter__c = '4' and Fiscal_Year__c = :fyName))
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}else if(currentQuarter.Number == 3) {
				fullQtrQuotas = [select SUM(Quota__c) quota from Quota__c where 
					((Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName) OR
					(Fiscal_Quarter__c = '2' and Fiscal_Year__c = :fyName) OR
					(Fiscal_Quarter__c = '3' and Fiscal_Year__c = :fyName))
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}else if(currentQuarter.Number == 2) {
				fullQtrQuotas = [select SUM(Quota__c) quota from Quota__c where 
					((Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName) OR
					(Fiscal_Quarter__c = '2' and Fiscal_Year__c = :fyName))
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}else if(currentQuarter.Number == 1) {
				fullQtrQuotas = [select SUM(Quota__c) quota from Quota__c where 
					(Fiscal_Quarter__c = '1' and Fiscal_Year__c = :fyName)
					and OwnerId = :runAsId];
				this.quota = (Decimal)fullQtrQuotas[0].get('quota');
			}

			String queryFullQtrYTDActualsSOQL = 'select SUM(Amount) amt from Opportunity ' +
				'where OwnerId in :userIds ' +
	 			'and IsWon = true ' +
	 			'and ((Shippable_Date__c >= :d1YTD and Shippable_Date__c <= :d2YTD) ' +
				'or (Shippable_Date__c = NULL and CloseDate >= :d1YTD and CloseDate <= :d2YTD))';
			if(MySFDCUtil.isServiceUser(filter.runAs)) {
				queryFullQtrYTDActualsSOQL += ' and RecordType.Name = \'Service\'';
			}
			String marketVertical = filter.marketVertical;
			List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
			List<String> country = RegionUtil.getCountryAliases(filter.country);
			if(!String.isBlank(marketVertical)) {
				if(marketVertical == 'Clinical & Forensic') {
					queryFullQtrYTDActualsSOQL += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
				}else if(marketVertical == 'Food & Environmental') {
					queryFullQtrYTDActualsSOQL += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
				}else {
					queryFullQtrYTDActualsSOQL += ' and Market_Vertical__c = :marketVertical';
				}
			}
			if(!String.isBlank(filter.region)) {
				if(String.isBlank(filter.country)) {
					//all countries in the region
					queryFullQtrYTDActualsSOQL += ' and Account.BillingCountry in :countries';
				}else{
					//specific country
					queryFullQtrYTDActualsSOQL += ' and Account.BillingCountry in :country';
				}
			}
			List<AggregateResult> queryFullQtrYTDActuals = Database.query(queryFullQtrYTDActualsSOQL);
			this.actual = (Decimal)queryFullQtrYTDActuals[0].get('amt');
		}
	}

	/* the default sorting criteria for the query */
	protected override String defaultSorting() {
		return ' order by Shippable_Date__c desc, CloseDate desc';
	}

	/* the list of fields for the report */
	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	/* Generate the google chart data */
	public override GoogleGaugeChartData getGoogleGaugeChartData() {
		GoogleGaugeChartData c = new GoogleGaugeChartData(ABSciexDashboardFactory.FULL_YTD_ACTUALS);
		c.gaugeLabel = '($M)';
		c.title = getReportTitle();
		c.actual = this.actual;
		c.quota = this.quota;
		return c;
	}

}