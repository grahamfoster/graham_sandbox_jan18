public without sharing class SVMX_WorkDetail_Helper
{	

	public static void checkProductStock(List<SVMXC__Service_Order_Line__c> newList, Map<Id, SVMXC__Service_Order_Line__c> oldMap) 
	{
		Set<Id> locationSet = new Set<Id>();
		Set<Id> partsSet = new Set<Id>();
		Set<Id> woSet = new Set<Id>();

        Schema.DescribeSObjectResult svmxwd = Schema.SObjectType.SVMXC__Service_Order_Line__c ;
        Map<String,Schema.RecordTypeInfo> rtMapByNamewd = svmxwd.getRecordTypeInfosByName();
        String wdIdUsage = rtMapByNamewd.get('Usage/Consumption').getRecordTypeId();

        Map<String, SVMXC__Product_Stock__c> productStockMap = new Map<String, SVMXC__Product_Stock__c>();
        Map<String, Decimal> qtyConsumedByStockMap = new Map<String, Decimal>();

		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			if (Trigger.isInsert)
			{
				if (wd.recordTypeId == wdIdUsage && wd.SVMXC__Consumed_From_Location__c != null && wd.SVMXC__Product__c != null && wd.SVMXC__Actual_Quantity2__c != null)
				{
					locationSet.add(wd.SVMXC__Consumed_From_Location__c);
					partsSet.add(wd.SVMXC__Product__c);
					woSet.add(wd.SVMXC__Service_Order__c);
					
					Decimal totConsumedQty = qtyConsumedByStockMap.get('' + wd.SVMXC__Consumed_From_Location__c + '|' + wd.SVMXC__Product__c);
					
					if (totConsumedQty == null)
						totConsumedQty = 0.00;

					qtyConsumedByStockMap.put('' + wd.SVMXC__Consumed_From_Location__c + '|' + wd.SVMXC__Product__c, totConsumedQty + wd.SVMXC__Actual_Quantity2__c);
				}			
			}
		}			

		if (!locationSet.isEmpty() && !partsSet.isEmpty())
		{
			List<SVMXC__Product_Stock__c> productStockList = [Select Id, SVMXC__Product__c, SVMXC__Product__r.Name, SVMXC__Available_Qty__c, SVMXC__Location__r.Name, SVMXC__Location__c from SVMXC__Product_Stock__c where SVMXC__Status__c = 'Available' and SVMXC__Location__c in :locationSet and SVMXC__Product__c in :partsSet];
			Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([Select Id, Name from SVMXC__Service_Order__c where Id in :woSet]);

			if (productStockList != null)
			{
				for (SVMXC__Product_Stock__c prodStock : productStockList)
					productStockMap.put('' + prodStock.SVMXC__Location__c + '|' + prodStock.SVMXC__Product__c , prodStock);
			}

			for (SVMXC__Service_Order_Line__c wd : newList)
			{
				if (Trigger.isInsert)
				{
					if (wd.recordTypeId == wdIdUsage && wd.SVMXC__Consumed_From_Location__c != null && wd.SVMXC__Product__c != null && wd.SVMXC__Actual_Quantity2__c != null)
					{
						SVMXC__Product_Stock__c prodStock = productStockMap.get('' + wd.SVMXC__Consumed_From_Location__c + '|' + wd.SVMXC__Product__c);
						Decimal totConsumedQty = qtyConsumedByStockMap.get('' + wd.SVMXC__Consumed_From_Location__c + '|' + wd.SVMXC__Product__c);

						if (prodStock == null || totConsumedQty == null || (totConsumedQty > prodStock.SVMXC__Available_Qty__c))
						{
							String partName = wd.SVMXC__Product__c;
							String locName = wd.SVMXC__Consumed_From_Location__c;

							if (prodStock != null)
							{
								partName = prodStock.SVMXC__Product__r.Name;
								locName = prodStock.SVMXC__Location__r.Name;
							}

							SVMXC__Service_Order__c wo = woMap.get(wd.SVMXC__Service_Order__c);
							String woNumber = wo.Name;

							wd.addError('Error: The product stock quantity for part ' + partName + ' at location ' + locName + ' is not sufficient to meet the total consumed quantity of ' + totConsumedQty + ' including ' + wd.SVMXC__Actual_Quantity2__c + ' consumed on a materials line on work order ' + woNumber + '.');
						} 
					}
				}
			}
		}
	}
}