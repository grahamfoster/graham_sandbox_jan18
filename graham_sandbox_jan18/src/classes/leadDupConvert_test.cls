@isTest
//(SeeAllData=true)
public class leadDupConvert_test {
   
    @testSetup static void setup() {    

	    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
	    User u1 = new User(Alias = 'MSuser', Email='msu@testorg.com', EmailEncodingKey='UTF-8', LastName='MS USER', LanguageLocaleKey='en_US',
	           		LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
		insert u1;
	    User u2 = new User(Alias = 'CEuser', Email='CEu@testorg.com', EmailEncodingKey='UTF-8', LastName='CE USER', LanguageLocaleKey='en_US',
	           		LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn2@testorg.test');    
		insert u2;
    
	    Lead lea = new Lead(LastName='LeadTest',
	        Company = 'TestLead',
		    Lead_Type__c = 'Existing Contact',                 
	        Status = 'Open',
		    Business_Segment__c = 'MS',
		    CE_Intent_to_Purchase__c = 'B - Warm (7-12 months purchase)', //Secondary
		    Intent_to_Purchase__c = 'A - Hot (0-6 months purchase)', //Primary
		    Marketing_Lead_Score__c='1',
			Email = 'neededfortesting@test.com',    
		    Country = 'Canada', 
		    OwnerID = u1.id,
		    Lead_Secondary_Owner_ID__c = u2.id,
		    Lead_Secondary_Email__c = u2.email,
		    Lead_Secondary_Owner__c = u2.Name,
			primary_routing__c = 'MS',
	        secondary_routing__c = 'CE',
	        dual_intent__c = True,
	        Lead_Assigned__c = False );
	   	insert lea;
    }
    
    @isTest public static  void Convert_Dual_Intent() {

		Lead lea = [SELECT LastName, Company, Lead_Type__c, Status, Business_Segment__c, CE_Intent_to_Purchase__c,
		    Intent_to_Purchase__c, Marketing_Lead_Score__c, Email, Country, OwnerID, Lead_Secondary_Owner_ID__c, 
            Lead_Secondary_Email__c, Lead_Secondary_Owner__c, primary_routing__c, secondary_routing__c, dual_intent__c,
	        Lead_Assigned__c, IsConverted FROM Lead LIMIT 1];

		test.StartTest();	        
			Database.LeadConvert le = new database.LeadConvert();     
		        le.setLeadId(lea.id);
				le.setDoNotCreateOpportunity(false);
				le.setConvertedStatus('Existing Opportunity');
	
		    Database.LeadConvertResult lcr = Database.convertLead(le);        
        test.StopTest();
    }        
     
	@isTest public static  void  Confirm_Dup_Lead() {    
 
		Lead lea = [SELECT LastName, Company, Lead_Type__c, Status, Business_Segment__c, CE_Intent_to_Purchase__c,
		    Intent_to_Purchase__c, Marketing_Lead_Score__c, Email, Country, OwnerID, Lead_Secondary_Owner_ID__c, 
            Lead_Secondary_Email__c, Lead_Secondary_Owner__c, primary_routing__c, secondary_routing__c, dual_intent__c,
	        Lead_Assigned__c, IsConverted FROM Lead LIMIT 1];
	        
			Database.LeadConvert le = new database.LeadConvert();     
		        le.setLeadId(lea.id);
				le.setDoNotCreateOpportunity(false);
				le.setConvertedStatus('Existing Opportunity');
	
		    Database.LeadConvertResult lcr = Database.convertLead(le);  	    
    
    
   		test.StartTest();      
    
			Lead ConvertedLead = [SELECT Id, OwnerID, Lead_Secondary_Owner_ID__c FROM Lead WHERE
                        isConverted = TRUE
//                        and ID = :lea.Id];
                        ORDER BY ConvertedDate DESC LIMIT 1];

System.debug('!!! - ConvertedLead: ' + ConvertedLead);
    
    		Lead dupLead = [SELECT Id, PrimeLeadID__c, OwnerID, Dual_Intent__c, Secondary_Routing__c, Lead_Type__c, ConvertedDate, ConvertedOpportunityId, 
                    ConvertedAccountId, IsConverted, ConvertedContactId, 
                    Lead_Secondary_Email__c, Lead_Secondary_Owner__c, Lead_Secondary_Owner_ID__c FROM Lead     WHERE
                    	isConverted = FALSE and PrimeLeadID__c =: ConvertedLead.ID
                    	ORDER BY Id DESC LIMIT 1]; 
     
System.debug('!!! - dupLead: ' + dupLead);
    
    		System.assertNotEquals(ConvertedLead.id, dupLead.Id);
			System.assertNotEquals(NULL, dupLead.Id);    
			System.assertEquals(ConvertedLead.Lead_Secondary_Owner_ID__c, dupLead.OwnerID);
			System.assertEquals(ConvertedLead.Id, dupLead.PrimeLeadID__c);    
			System.assertEquals(False, dupLead.Dual_Intent__c);
    		System.assertEquals(duplead.Secondary_Routing__c, NULL);
    		System.assertEquals(duplead.Lead_Type__c, 'Existing Contact');
    		System.assertEquals(duplead.ConvertedDate, NULL);
    		System.assertEquals(duplead.ConvertedOpportunityId, NULL);
    		System.assertEquals(duplead.ConvertedAccountId, NULL);
    		System.assertEquals(duplead.IsConverted, FALSE);
    		System.assertEquals(duplead.ConvertedContactId, NULL);
    		System.assertEquals(duplead.Lead_Secondary_Email__c, NULL);
    		System.assertEquals(duplead.Lead_Secondary_Owner__c, NULL);
    		System.assertEquals(duplead.Lead_Secondary_Owner_ID__c, NULL);
		test.stopTest();  
    }
}