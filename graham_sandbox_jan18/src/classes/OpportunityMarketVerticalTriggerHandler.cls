/**
 ** @author Reid Beckett, Cloudware Connections
**/
public class OpportunityMarketVerticalTriggerHandler {
	public static Boolean PrimaryContactUpdate = false;
	
	public static void onAccountUpdate(Account[] accts, Map<Id, Account> oldMap) {
		system.debug('OpportunityMarketVerticalTriggerHandler.onAccountUpdate::start');
		Set<Id> accountIds = new Set<Id>();
		for(Account a : accts) {
			Account oldAccount = oldMap.get(a.Id);
			if(oldAccount != null && oldAccount.Institution_Type__c != a.Institution_Type__c) {
				accountIds.add(a.Id);
			}			
		}
		
		Opportunity[] opps = [select Id, Institution_Type__c from Opportunity where AccountId in :accountIds];
		update opps;
	}
	
	public static void onInsertUpdate(Opportunity[] opps) {
		system.debug('OpportunityMarketVerticalTriggerHandler.onInsertUpdate::start');
		if(OpportunityMarketVerticalTriggerHandler.PrimaryContactUpdate) return;
		Map<Id,Account> accountMap = new Map<Id,Account>();
		for(Opportunity opp : opps) {
			accountMap.put(opp.AccountId, null);						
		}
		
		Account[] accounts = [select Id, Institution_Type__c from Account where Id in :accountMap.keySet()];
		for(Account a : accounts) {
			accountMap.put(a.Id, a);
		}
		
		Set<String> mvcKeys = new Set<String>();
		for(Opportunity opp : opps){
			Account acct = accountMap.get(opp.AccountId);
			if(acct != null) {
				String key = acct.Institution_Type__c + ';' + opp.Primary_Application__c + ';' + opp.Market_Segment__c + ';' + opp.Routine_Testing__c;
				key = key.toLowerCase();
				mvcKeys.add(key); 
			}
		}
		
		Map<String,String> mvMap = MarketVerticalSelectionHelper.getMarketVertical(mvcKeys);
		system.debug('mvMap='+mvMap);
		for(Opportunity opp : opps){
			Account acct = accountMap.get(opp.AccountId);
			if(acct != null) {
				String key = acct.Institution_Type__c + ';' + opp.Primary_Application__c + ';' + opp.Market_Segment__c + ';' + opp.Routine_Testing__c;
				key = key.toLowerCase();
				String marketVertical = mvMap.get(key);
				//Mar 19/2013 - change so only if a marketVertical match is found then update the opp
				if(!String.isBlank(marketVertical)) {
					opp.Market_Vertical__c = marketVertical;
				}
			} 
		}
	}

	/*
	* Apr 29/2014 - copy Marketing information from Opportunity to the Contact's associated to the opp
	*/
	public static void onAfterUpdate(){
		//TODO
		Map<Id,Opportunity> oppsMap = new Map<Id,Opportunity>();
		for(sObject sobj : Trigger.new) {
			Opportunity opp = (Opportunity)sobj;
			Opportunity old_opp = (Opportunity)Trigger.oldMap.get(opp.Id);
			system.debug(opp);
			system.debug(old_opp);
			if((old_opp.Market_Vertical__c != opp.Market_Vertical__c) || (old_opp.Market_Segment__c != opp.Market_Segment__c) || (old_opp.Primary_Application__c != opp.Primary_Application__c)) {
				system.debug('changed something');
				oppsMap.put(opp.Id, opp);
			}
		}

		system.debug('oppsMap:'+oppsMap);
		if(oppsMap.size() > 0) {
			Map<Id,Contact> contactsMap = new Map<Id,Contact>();
			for(OpportunityContactRole ocr : [select OpportunityId, ContactId, Contact.Market_Segment_St__c, Contact.Market_Segment__c, Contact.Primary_Application__c from OpportunityContactRole where OpportunityId in :oppsMap.keySet()]){
				Opportunity opp = oppsMap.get(ocr.OpportunityId);
				if((opp.Market_Vertical__c != ocr.Contact.Market_Segment_St__c) || (opp.Market_Segment__c != ocr.Contact.Market_Segment__c) || (opp.Primary_Application__c != ocr.Contact.Primary_Application__c)) {
					contactsMap.put(ocr.ContactId, new Contact(Id = ocr.ContactId, Market_Segment_St__c = opp.Market_Vertical__c, Market_Segment__c = opp.Market_Segment__c, Primary_Application__c = opp.Primary_Application__c));
				}
			}
			system.debug('contactsMap:'+contactsMap);
			if(contactsMap.size() > 0) {
				update contactsMap.values();
			}
		}
	}

	public static testMethod void test1() {
		Market_Vertical_Config__c mvc = new Market_Vertical_Config__c(Institution_Type__c = 'Academic', Application__c = 'Allergen testing',
			Market_Segment__c = 'Agriculture', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='Food/Beverage');
		insert mvc;
		
		Account a = new Account(Name = 'Test', BillingCountry='United States', Institution_Type__c = 'Academic');
		insert a;
		
		Contact c = new Contact(AccountId = a.Id, FirstName = 'Test', LastName = 'Contact');
		insert c;
		
		Opportunity opp = new Opportunity(AccountId = a.Id, Name = 'Test opp',
			Market_Segment__c = 'Agriculture', Primary_Application__c = 'Allergen testing', Routine_Testing__c = 'Yes',
			CloseDate = Date.today(), StageName = 'Prospect/Budget Requested', ForecastCategoryName = 'Pipeline', Primary_Contact__c = c.Id, 
			Explicit_Needs__c = '?', Implications__c  = '?', Implied_Needs__c = '?');
		insert opp;
		
		opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		system.assertequals('Food/Beverage', opp.Market_Vertical__c);
	}

	public static testMethod void test2() {
		Market_Vertical_Config__c mvc = new Market_Vertical_Config__c(Institution_Type__c = 'Academic', Application__c = 'Allergen testing',
			Market_Segment__c = 'Agriculture', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='Food/Beverage');
		insert mvc;
		
		Account a = new Account(Name = 'Test', BillingCountry='United States', Institution_Type__c = 'Academic');
		insert a;
		
		Contact c = new Contact(AccountId = a.Id, FirstName = 'Test', LastName = 'Contact');
		insert c;
		
		Opportunity opp = new Opportunity(AccountId = a.Id, Name = 'Test opp',
			CloseDate = Date.today(), StageName = 'Prospect/Budget Requested', ForecastCategoryName = 'Pipeline', Primary_Contact__c = c.Id);
		try {
			insert opp;
			//system.assertEquals(false,true);
		}catch(DmlException e){
		}
		
		//opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		//system.assertequals(null, opp.Market_Vertical__c);
	}

	public static testMethod void test3() {
		Market_Vertical_Config__c mvc = new Market_Vertical_Config__c(Institution_Type__c = 'academic', Application__c = 'Allergen testing',
			Market_Segment__c = 'Agriculture', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='Food/Beverage');
		insert mvc;
		Market_Vertical_Config__c mvc2 = new Market_Vertical_Config__c(Institution_Type__c = 'Government', Application__c = 'Allergen testing',
			Market_Segment__c = 'Agriculture', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='old');
		insert mvc2;
		
		Account a = new Account(Name = 'Test', BillingCountry='United States', Institution_Type__c = 'Government');
		insert a;
		
		Contact c = new Contact(AccountId = a.Id, FirstName = 'Test', LastName = 'Contact');
		insert c;
		
		Opportunity opp = new Opportunity(AccountId = a.Id, Name = 'Test opp',
			Market_Segment__c = 'Agriculture', Primary_Application__c = 'Allergen testing', Routine_Testing__c = 'Yes',
			CloseDate = Date.today(), StageName = 'Prospect/Budget Requested', ForecastCategoryName = 'Pipeline', Primary_Contact__c = c.Id,
			Explicit_Needs__c = '?', Implications__c  = '?', Implied_Needs__c = '?');
		insert opp;
		opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		system.assertequals('old', opp.Market_Vertical__c);
		
		System.Test.startTest();
		a.Institution_Type__c = 'Academic';
		system.debug('----------- start Account update ---------------');
		update a;
		system.debug('----------- end Account update ---------------');
		
		opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		system.assertequals('Food/Beverage', opp.Market_Vertical__c);
		System.Test.stopTest();
	}

	public static testMethod void test4() {
		Market_Vertical_Config__c mvc = new Market_Vertical_Config__c(Institution_Type__c = 'Academic', Application__c = 'Allergen testing',
			Market_Segment__c = 'Agriculture', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='Food/Beverage');
		insert mvc;
		
		Account a = new Account(Name = 'Test', BillingCountry='United States', Institution_Type__c = 'Academic');
		insert a;
		
		Contact c = new Contact(AccountId = a.Id, FirstName = 'Test', LastName = 'Contact');
		insert c;
		
		Opportunity opp = new Opportunity(AccountId = a.Id, Name = 'Test opp',
			Market_Segment__c = 'Agriculture', Primary_Application__c = 'Allergen testing', Routine_Testing__c = 'Yes',
			CloseDate = Date.today(), StageName = 'Prospect/Budget Requested', ForecastCategoryName = 'Pipeline', Primary_Contact__c = c.Id,
			Explicit_Needs__c = '?', Implications__c  = '?', Implied_Needs__c = '?');
		insert opp;
		
		opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		system.assertequals('Food/Beverage', opp.Market_Vertical__c);
		
		//set to something that doesn't have config, update should not change the opp
		opp.Market_Segment__c = 'XXXXXX';
		update opp;
		opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		system.assertequals('Food/Beverage', opp.Market_Vertical__c);
	}

	public static testMethod void test5_updateContact() {
		
		Market_Vertical_Config__c mvc = new Market_Vertical_Config__c(Institution_Type__c = 'Academic', Application__c = 'Allergen testing',
			Market_Segment__c = 'Agriculture', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='Food/Beverage');
		insert mvc;

		Market_Vertical_Config__c mvc2 = new Market_Vertical_Config__c(Institution_Type__c = 'Academic', Application__c = 'Allergen testing',
			Market_Segment__c = 'Basic Research', 
			Routine_Testing__c = 'Yes', Market_Vertical__c='Pharma/CRO');
		insert mvc2;

		Account a = new Account(Name = 'Test', BillingCountry='United States', Institution_Type__c = 'Academic');
		insert a;
		
		Contact c = new Contact(AccountId = a.Id, FirstName = 'Test', LastName = 'Contact');
		insert c;
		
		Opportunity opp = new Opportunity(AccountId = a.Id, Name = 'Test opp',
			Market_Segment__c = 'Agriculture', Primary_Application__c = 'Allergen testing', Routine_Testing__c = 'Yes',
			CloseDate = Date.today(), StageName = 'Prospect/Budget Requested', ForecastCategoryName = 'Pipeline', Primary_Contact__c = c.Id,
			Explicit_Needs__c = '?', Implications__c  = '?', Implied_Needs__c = '?');
		insert opp;

		OpportunityContactRole ocr = new OpportunityContactRole(ContactId = c.Id, OpportunityId = opp.Id, Role = 'Other');
		insert ocr;
		
		opp = [select Market_Vertical__c from Opportunity where Id = :opp.Id];
		opp.Market_Segment__c = 'Basic Research';
		update opp;

		c = [select Market_Segment_St__c, Market_Segment__c, Primary_Application__c from Contact where Id = :c.Id];
		system.assertEquals('Pharma/CRO', c.Market_Segment_St__c);
		system.assertEquals('Basic Research', c.Market_Segment__c);
		system.assertEquals('Allergen testing', c.Primary_Application__c);
	}
}