/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Oct 24/2013
 **
 ** Handle the Timesheet trigger requirements
 ** 1) Validate that the Week Start is a Monday
 ** 2) Set the timesheet name
**/
public class TimesheetTriggerHandler {
    /**
     ** Handle before insert, before update event
     ** Validate that the week start is a Monday
    **/
    public static void onBefore(Map<Id, Timesheet__c> oldMap, Timesheet__c[] timesheets){
        Map<Id,User> ownerMap = new Map<Id,User>();
        for(Timesheet__c ts : timesheets){
            ownerMap.put(ts.OwnerId, null);
        }
        
        User[] owners = [select Id, Name from User where Id in :ownerMap.keySet()];
        for(User owner : owners){
        	ownerMap.put(owner.Id, owner);
        }
        
        for(Timesheet__c ts : timesheets){
            if(ts.Week_Start__c != null){
                String dow = DateTime.newInstance(ts.Week_Start__c.year(), ts.Week_Start__c.month(), ts.Week_Start__c.day()).format('E');
                if(dow.toLowerCase() != 'mon') {
                    ts.Week_Start__c.addError(Label.Err_WeekMustStartOnMonday);
                }else{
                	User owner = ownerMap.get(ts.OwnerId);
                    ts.Name = formatTimesheetName(owner, ts.Week_Start__c);
                }
                ts.UniqueKey__c = ts.OwnerId + '-' + ts.Week_Start__c.year() + '-' + ts.Week_Start__c.month() + '-' + ts.Week_Start__c.day();
            }
        }
    }
    
    public static String formatTimesheetName(User owner, Date weekStart){
		return owner.Name + ' - ' + DateTime.newInstance(weekStart.year(), weekStart.month(), weekStart.day()).format('M/d/yyyy');
    }
}