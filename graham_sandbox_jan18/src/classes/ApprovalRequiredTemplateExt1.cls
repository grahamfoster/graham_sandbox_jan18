/*********************************************************************************************
 * Name             : ApprovalRequiredTemplateExt1
 * Created By       : Buan Consulting
 * Created Date     : 08 March 2016
 * Purpose          : used as extension class in custom component for VF email template
 * Notes            : deactivated as the business chose to go back to to the unsorted email template
**********************************************************************************************/

public class ApprovalRequiredTemplateExt1 {

/*    public id quoteId {get;set;}

    public list<SBQQ__QuoteLine__c> getQuoteLineItems()
    {

          list<SBQQ__QuoteLine__c> qli = [SELECT Id, Name, Sales_Rep_Discount__c, District_Sales_Mgr_Discount__c, Regional_Sales_Mgr_Discount__c, Regional_General_Mgr_Discount__c, Regional_VP_Sales_Discount__c, General_Mgr_Discount__c, Discount_for_Approvals__c, Internal_External_Difference__c, Net_Total_Internal__c, SBQQ__NetPrice__c, SBQQ__NetTotal__c, Discount_Value__c, SBQQ__ProductName__c, SBQQ__ProductCode__c, SBQQ__Number__c, SBQQ__Quantity__c, SBQQ__ListPrice__c, Internal_Discount__c, Net_Unit_Price_Internal__c, Trade_In_Discount_Grouping__c, SBQQ__Quote__r.CurrencyIsoCode, Comm__c, Tier__c FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :quoteId ORDER by Net_Unit_Price_Internal__c DESC];

          return qli;
    }
*/
}