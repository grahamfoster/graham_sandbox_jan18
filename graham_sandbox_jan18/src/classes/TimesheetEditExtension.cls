/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Oct 29/2013
 **
 ** Timesheet extension
**/
public with sharing class TimesheetEditExtension {
	public Timesheet__c timesheet {get;set;}
	//public Map<Integer,TimesheetEntryWrapper[]> entries {get;set;}
	public List<TimesheetDay> allEntries {get;set;}
	public CategoryWrapper[] categories {get;set;}
	public Integer currentDay {get;set;}
	public Integer removeDay {get;set;}
	public Integer removeIdx {get;set;}
	public String retURL {get;set;}
	public Boolean clone {get;set;}
	
	public TimesheetEditExtension(ApexPages.StandardController stdController){
		clone = ApexPages.currentPage().getParameters().containsKey('clone') ? (ApexPages.currentPage().getParameters().get('clone') == '1') : false;
		currentDay = 0; //default to monday
		Id timesheetId = ((Timesheet__c)stdController.getRecord()).Id;
		loadTimesheet(timesheetId);
		loadCategories();
		retURL = ApexPages.currentPage().getParameters().containsKey('retURL') ? EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('retURL'), 'UTF-8') : null;
	}
	
	public PageReference save(){
		system.debug('saving entries:');
		System.SavePoint savePoint = null;
		try{
			savePoint = Database.setSavePoint();
			//get the categories first
			Map<Id, Timesheet_Category__c> allCats = new Map<Id, Timesheet_Category__c>();
			Timesheet_Category__c[] timesheetCategories = [select Id, Name from Timesheet_Category__c];
			for(Timesheet_Category__c tc : timesheetCategories) {
				allCats.put(tc.Id, tc);
			}
			
			Timesheet_Entry__c[] entriesUpserts = new Timesheet_Entry__c[]{};
			Timesheet_Entry__c[] entriesDeletes = new Timesheet_Entry__c[]{};
			/**
			if(clone) {
				timesheet = timesheet.clone(false,true);
				insert timesheet;
				for(TimesheetDay tday : allEntries) { 
					for(TimesheetEntryWrapper e : tday.entries) {
						if(!e.removed) {
							if(e.entry.End_Time__c > e.entry.Start_Time__c) {
								e.entry.Timesheet__c = timesheet.Id;
								e.entry.Date__c = timesheet.Week_Start__c.addDays(tday.day);
								entriesUpserts.add(e.entry);
							}
						}
					}
				}
			}else{ **/
	            upsert timesheet;
				for(TimesheetDay tday : allEntries) { 
					for(TimesheetEntryWrapper e : tday.entries) {
						if(!e.removed) {
							if(e.entry.End_Time__c > e.entry.Start_Time__c) {
								e.entry.Date__c = timesheet.Week_Start__c.addDays(tday.day);
								system.debug('saving: e.entry.Date__c='+e.entry.Date__c);
								if(e.entry.Timesheet__c == null) e.entry.Timesheet__c = timesheet.Id;
								entriesUpserts.add(e.entry);
							}
						} else if(e.entry.Id != null) {
							entriesDeletes.add(e.entry);
						}
					}
				}
			/**}**/
			delete entriesDeletes;
			upsert entriesUpserts;
			//SfdcUtil.addConfirmMessageToPage('Entries saved');
			if(retURL != null && !clone) {
				return new PageReference(retURL);
			}else{
				return new PageReference('/' + timesheet.Id);
			}
		}catch(DmlException e){
			SfdcUtil.addDMLExceptionMessagesToPage(e);
			if(savePoint != null) Database.rollback(savePoint);
			return null;
		}catch(Exception e){
			SfdcUtil.addExceptionMessageToPage(e);
			if(savePoint != null) Database.rollback(savePoint);
			return null;
		}
	}
	
	public PageReference addCategory(){
		Timesheet_Entry__c entry = new Timesheet_Entry__c(Timesheet__c = timesheet.Id, Date__c = timesheet.Week_Start__c.addDays(currentDay));
		integer idx = allEntries.get(currentDay).entries.size() == 0 ? 0 : (allEntries.get(currentDay).entries[allEntries.get(currentDay).entries.size()-1].idx + 1);
		TimesheetEntryWrapper prevEntry = null;
		for(TimesheetEntryWrapper tew : allEntries.get(currentDay).entries) {
			if(prevEntry == null || tew.idx > prevEntry.idx) prevEntry = tew;
		}
		
		//Timesheet_Entry__c prevEntry = idx > 0 ? allEntries.get(currentDay).entries[idx-1].entry : null;
		if(prevEntry != null) {
			entry.Start_Time__c = prevEntry.entry.End_Time__c;
			entry.End_Time__c = entry.Start_Time__c + 60;
		}else{
			entry.Start_Time__c = 7 * 60; //7am
			entry.End_Time__c = 7 * 60; //8am
		}
		allEntries.get(currentDay).entries.add(new TimesheetEntryWrapper(idx, entry));
		for(TimesheetDay tday : allEntries) {
			if(tday.day != currentDay) tday.display = false;
			else tday.display = true;
		}
		return null;
	}
	
	public PageReference setCurrentDayAction(){
		return null;
	}
	
	public PageReference removeEntryAction() {
		system.debug('removeEntryAction: removeDay='+removeDay+'; removeIdx='+removeIdx);
		if(allEntries.get(removeDay).entries.size() > removeIdx) {
			system.debug('found for removal');
			allEntries.get(removeDay).entries.get(removeIdx).removed = true;
			if(allEntries.get(removeDay).displayedEntries.size() == 0) {
				Timesheet_Entry__c tsentry = new Timesheet_Entry__c(Timesheet__c = timesheet.Id, Date__c = timesheet.Week_Start__c.addDays(currentDay)); 
				allEntries.get(removeDay).entries.add(new TimesheetEntryWrapper(allEntries.get(removeDay).entries.size(), tsentry));
			}
		}
		for(TimesheetDay tday : allEntries) {
			if(tday.day != removeDay) tday.display = false;
			else tday.display = true;
		}
		return null;
	}
	
	private void loadTimesheet(Id timesheetId){
		//entries = new Map<Integer,TimesheetEntryWrapper[]>();
		allEntries = new List<TimesheetDay> {
			new TimesheetDay(0,new TimesheetEntryWrapper[]{},true),
			new TimesheetDay(1,new TimesheetEntryWrapper[]{},true),
			new TimesheetDay(2,new TimesheetEntryWrapper[]{},true),
			new TimesheetDay(3,new TimesheetEntryWrapper[]{},true),
			new TimesheetDay(4,new TimesheetEntryWrapper[]{},true)
		};
		if(timesheetId != null) {
			timesheet = [select Id, OwnerId, Owner.Name, Name, Week_Start__c from Timesheet__c where Id = :timesheetId];
			if(clone) {
				Timesheet_Entry__c[] tsentries = [select Id, Name, Timesheet__c, Timesheet_Category__c, Timesheet_Category__r.Name, Date__c, Start_Time__c, End_Time__c
					from Timesheet_Entry__c where Timesheet__c = :timesheetId order by Date__c asc, End_Time__c asc];
				timesheet = timesheet.clone(false,true);
				
				timesheet.Week_Start__c = timesheet.Week_Start__c.addDays(7);

				for(Timesheet_Entry__c entry : tsentries) {
					entry = entry.clone(false, true);
					entry.Timesheet__c = null;
					entry.Date__c = entry.Date__c.addDays(7);
					Integer dayDiff = timesheet.Week_Start__c.daysBetween(entry.Date__c);
					integer idx = allEntries.get(dayDiff).entries.size();
					allEntries.get(dayDiff).entries.add(new TimesheetEntryWrapper(idx, entry));
				}
			}else {
				Date compareDate = timesheet.Week_Start__c.addDays(currentDay);
				Timesheet_Entry__c[] tsentries = [select Id, Name, Timesheet__c, Timesheet_Category__c, Timesheet_Category__r.Name, Date__c, Start_Time__c, End_Time__c
					from Timesheet_Entry__c where Timesheet__c = :timesheetId order by Date__c asc, End_Time__c asc];
				for(Timesheet_Entry__c entry : tsentries){
					Integer dayDiff = timesheet.Week_Start__c.daysBetween(entry.Date__c);
					integer idx = allEntries.get(dayDiff).entries.size();
					allEntries.get(dayDiff).entries.add(new TimesheetEntryWrapper(idx, entry));
				}
			}			
		}else {
			timesheet = new Timesheet__c();
			//entries.put(currentDay, new TimesheetEntryWrapper[]{});			
		}
		
		//TODO: add one extra empty entry for each day
		for(TimesheetDay tday : allEntries) {
			if(tday.displayedEntries.size() == 0) {
				tday.entries.add(new TimesheetEntryWrapper(0, new Timesheet_Entry__c(Timesheet__c=timesheetId, Date__c = timesheet.Week_Start__c.addDays(currentDay))));
			}
		}
	}
	
	@RemoteAction
	public static String loadEntries(String idx) {
		return idx;
	}
	
	private void loadCategories(){
		categories = new CategoryWrapper[]{};
		Timesheet_Category__c[] tscategories = [select Id, Name, Name_JP__c, Name_CN__c, Description__c, Description_JP__c, Description_CN__c, Code__c from Timesheet_Category__c order by Name asc];
		for(Timesheet_Category__c c : tscategories) {
			categories.add(new CategoryWrapper(c));
		} 
	}

    public CategoryWrapper firstCategory {
        get {
            return categories[0];
        }
    }
	
	public SelectOption[] categoryOptions {
		get {
			SelectOption[] options = new SelectOption[]{}; 
			for(CategoryWrapper cw : categories) {
				options.add(new SelectOption(cw.tscategory.Id, cw.name));
			} 
			return options;
		}
	}
	
	public class CategoryWrapper {
		public Timesheet_Category__c tscategory {get;set;}
		
		public CategoryWrapper(Timesheet_Category__c tscategory){
			this.tscategory = tscategory;
		}
		
		public String name {
			get {
				if(UserInfo.getLanguage() == 'ja')
					return tscategory.Name_JP__c;
				else if(UserInfo.getLanguage() == 'zh_TW' || UserInfo.getLanguage() == 'zh_CN')
					return tscategory.Name_CN__c;
				else
					return tscategory.Name;
			}
		}
		
		public String description {
			get {
				if(UserInfo.getLanguage() == 'ja')
					return tscategory.Description_JP__c;
				else if(UserInfo.getLanguage() == 'zh_TW' || UserInfo.getLanguage() == 'zh_CN')
					return tscategory.Description_CN__c;
				else
					return tscategory.Description__c;
			}
		}

	} 
	
	public class TimesheetEntryWrapper {
		public Timesheet_Entry__c entry {get;set;}
		public String categorySelected {get;set;}
		public Integer idx {get;set;}
		public Boolean removed {get;set;}
		
		public TimesheetEntryWrapper(Integer idx, Timesheet_Entry__c entry){
			this.idx = idx;
			this.entry = entry;
			this.removed = false;
		}
		
		//TODO: categorySelected on set change the timesheet category lookup
		
		public String categoryName {
			get {
				//TODO: multilingual name
				return entry.Timesheet_Category__r.Name;
			}
		}
		
		public String startStr {
			get {
				//integer startHour = 7*60;
				if(entry == null || entry.Start_Time__c == null) return '';
				integer startHour = 0;
				integer startVal = Integer.valueOf(starthour + entry.Start_Time__c);
				String s = SfdcUtil.leftPad(String.valueOf((startVal - Math.mod(startVal,60))/60), '0', 2) + ':' + SfdcUtil.leftPad(String.valueOf(Math.mod(startVal,60)), '0', 2);
				return s;
			}
			set {
				//in: 08:00 formatted time, convert to integer # of minutes from midnight
				Integer hrs = 0;
				Integer mins = 0;
				if(value != null) {
					hrs = value.trim().length() > 1 ? Integer.valueOf(value.trim().substring(0,2)) : 0;
					mins = value.length() > 4 ? Integer.valueOf(value.substring(3,5)) : 0;
				}
				entry.Start_Time__c = hrs*60 + mins;
			}
		}

		public String endStr {
			get {
				//integer startHour = 7*60;
				if(entry == null || entry.End_Time__c == null) return '';
				integer startHour = 0;
				integer endVal = Integer.valueOf(starthour + entry.End_Time__c);
				String s = SfdcUtil.leftPad(String.valueOf((endVal - Math.mod(endVal,60))/60), '0', 2) + ':' + SfdcUtil.leftPad(String.valueOf(Math.mod(endVal,60)), '0', 2);
				return s;
			}
			set {
				//in: 08:00 formatted time, convert to integer # of minutes from midnight
				Integer hrs = 0;
				Integer mins = 0;
				if(value != null) {
					hrs = value.length() > 1 ? Integer.valueOf(value.substring(0,2)) : 0;
					mins = value.length() > 4 ? Integer.valueOf(value.substring(3,5)) : 0;
				}
				entry.End_Time__c = hrs*60 + mins;
			}
		}
	}
	
	class TimesheetDay {
		public Integer day {get;set;}
		public List<TimesheetEntryWrapper> entries {get;set;}
		private Boolean display {get;set;}
		
		public TimesheetDay(Integer day, List<TimesheetEntryWrapper> entries, Boolean display){
			this.day = day;
			this.entries = entries;
			this.display = display;
		}
		
		public String css {
			get {
				if(!display) return 'display:none';
				else return '';
			}
		}
		
		public List<TimesheetEntryWrapper>  displayedEntries {
			get {
				List<TimesheetEntryWrapper> lst = new List<TimesheetEntryWrapper>();
				for(TimesheetEntryWrapper tew : entries) {
					if(!tew.removed) lst.add(tew);
				}
				return lst;
			}
		}
	}

	public List<TimesheetEntryWrapper> mondayEntries {
		get {
			return allEntries.get(0).displayedEntries;
		}
	}

	public List<TimesheetEntryWrapper> tuesdayEntries {
		get {
			return allEntries.get(1).displayedEntries;
		}
	}

	public List<TimesheetEntryWrapper> wednesdayEntries {
		get {
			return allEntries.get(2).displayedEntries;
		}
	}

	public List<TimesheetEntryWrapper> thursdayEntries {
		get {
			return allEntries.get(3).displayedEntries;
		}
	}

	public List<TimesheetEntryWrapper> fridayEntries {
		get {
			return allEntries.get(4).displayedEntries;
		}
	}
	/**
	public List<TimesheetDay> allEntries {
		get {
			return new List<TimesheetDay> { 
				new TimesheetDay(0, mondayEntries, currentDay==0), 
				new TimesheetDay(1, tuesdayEntries, currentDay==1), 
				new TimesheetDay(2, wednesdayEntries, currentDay==2),
				new TimesheetDay(3, thursdayEntries, currentDay==3),
				new TimesheetDay(4, fridayEntries, currentDay==4)
			};
		}
	}
	**/
}