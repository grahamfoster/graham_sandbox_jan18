@isTest
/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Aug 5/2013
 **
 ** This trigger modifies the owner of the Task as it is created by Eloqua
 ** The task should be updated to the owner of the Lead or Contact
**/
public class TaskTriggerTests {
	public static testMethod void test_contact(){
		setUp();
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'United States');
		insert acct;
		
		Contact cont = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe');
		insert cont;
		
		User elqUser = TaskTriggerHandler.findEloquaUser();
		system.runAs(elqUser) {
			Task t = new Task(WhatId = acct.Id, WhoId = cont.Id, Subject = 'Test subject - Academic Pro Opp AMER');
			insert t;
		}
	}

	public static testMethod void test_lead(){
		setUp();
		
		User elqUser = TaskTriggerHandler.findEloquaUser();
		system.runAs(elqUser) {
            Lead ld = new Lead(Company = 'Test Company', FirstName = 'John', LastName = 'Doe');
            insert ld;

            Task t = new Task(WhoId = ld.Id, Subject = 'Test subject - Academic Pro Opp AMER');
			insert t;
		}
	}

	public static testMethod void test_lead_multiple(){
		setUp();
		
		User elqUser = TaskTriggerHandler.findEloquaUser();
		system.runAs(elqUser) {
            Lead ld = new Lead(Company = 'Test Company', FirstName = 'John', LastName = 'Doe');
            insert ld;
            
			Task[] t = new Task[]{
				new Task(WhoId = ld.Id, Subject = 'Test subject 1 - Academic Pro Opp AMER'),
				new Task(WhoId = ld.Id, Subject = 'Test subject 2 - Academic Pro Opp AMER')
			};
			insert t;
		}
	}
	
	private static void setUp() {
		EloquaCampaign__c[] elqCampaigns = new EloquaCampaign__c[]{ 
			new EloquaCampaign__c(Name = 'Academic Pro Opp - Sep 4/2013', Task_Subject_Key__c = 'Academic Pro Opp AMER')		
		};
		insert elqCampaigns;
	}
}