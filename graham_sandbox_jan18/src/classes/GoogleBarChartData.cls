/**
** @author Reid Beckett, Cloudware Connections
** @created Mar 4/2015
**
** Encapsulates data for a Goolge Visualization bar chart
** Assumes stacked bar chart
** 
**/
public class GoogleBarChartData 
{
	public String id {get;set;}
	public String title {get;set;}
	public String hName {get;set;}
	public String vName {get;set;}
	public String hLabel {get;set;}
	public String numberFormat {get;set;}
	public List<Tick> ticks {get;set;}
	public List<String> colors {get;set;}
	public List<List<Double>> data {get;set;}
	
	public GoogleBarChartData(String id) 
	{
		this.id = id;
		ticks = new List<Tick>();
		data = new List<List<Double>>();
		colors = new List<String>();
	}

	public String colorsArray {
		get {
			List<String> cstrings = new List<String>();
			for(String c : colors) {
				cstrings.add('\'' + c + '\'');
			}
			return String.join(cstrings,',');
		}
	}

	public void addTick(String label, Double value)
	{
		this.ticks.add(new Tick(label, value));
	}

	public class Tick 
	{
		public String label {get;set;}
		public Double value {get;set;}

		public Tick(String label, Double value)
		{
			this.label = label;
			this.value = value;
		}

		public String asJSON
		{
			get {
				return label == null ? String.valueOf(value) : '{v:'+value+',f:"' + label + '"}';
			}
		}
	}
}