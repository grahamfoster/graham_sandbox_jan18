public class AddNewProductMeta_CTRL{
   
    public Product_Meta__c prodmeta{get;set;}
    public Specifications_Meta__c specmeta{get;set;}
    public List<Product_Meta__c> prodmetalist{get;set;}
    public String SpecificID{get;set;}
    public boolean errflag{get;set;}
    public Boolean isComplete{get; private set;}
    public String Metaid{get;set;}
    Public Product_Meta__c prodmeta1{get;set;}
    public Boolean isErrorMsg{get;set;}
    
    

    public AddNewProductMeta_CTRL(){
        Metaid=ApexPages.currentPage().getParameters().get('Metaid');
        SpecificID=ApexPages.currentPage().getParameters().get('specid');
        if((SpecificID != null || SpecificID !='')  && (Metaid == null || Metaid =='')){
            specmeta =[select id,isactive__c,Specifications_Meta_Name__c from Specifications_Meta__c where id=:SpecificID];
            prodmetalist= new List<Product_Meta__c>();
            prodmeta= new Product_Meta__c();
            prodmetalist.add(prodmeta); 
            
         }
         if((SpecificID == null || SpecificID =='')  && (Metaid != null || Metaid !='')){
           
            prodmeta1=[select id,isactive__c,Product_Meta_Name__c from Product_Meta__c where id=:Metaid ];
           
         }
        isComplete = false;
        
        isErrorMsg= false;
    }
    
    public void manageOpenCategory()
    {
    	EditCategory editObj = new EditCategory();
    	editObj.selProdFeatId = SpecificID;
    	editObj.refreshCatList();
    }
   
    public PageReference InsertProduct(){
    try{
    isComplete = false;
    isErrorMsg = false;
    if(Metaid == null && SpecificID != null){
            Integer count1=prodmetalist.size();  
            List<Product_Meta__c> prodmetalist1= new List<Product_Meta__c>();
              
                    if(prodmetalist != null){
                      
                        for(Product_Meta__c ab:prodmetalist){
                            if( ab.Product_Meta_Name__c!=null){
                                ab.Specifications_Meta__c=SpecificID;
                                prodmetalist1.add(ab);
                                if(count1 ==prodmetalist1.size())
                                 errflag=false;
                            }
                            else{
                               errflag=true;
                               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please Insert Product Meta.');
                               ApexPages.Addmessage(myMsg);   
                               isComplete = true; 
                               return null;
                            }
                        }
                        if(errflag==false){
                            insert prodmetalist1;
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You information has been saved successfully.');
                            ApexPages.Addmessage(myMsg);
                            isComplete = true; 
                            return null;    
                        }
                           // manageOpenCategory();          
                            System.debug('inside call....')  ;
                    }
                }
                if(Metaid != null && SpecificID == null){
                    update prodmeta1;
                    isComplete = true; 
                    return null;
             }
            isComplete = true; 
            isErrorMsg= true;
        }
         catch(Exception e){
            isComplete = false;
            isErrorMsg= false; 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); 
      
        }
        return null;
    }   
    public PageReference DeleteProduct(){
      try{
          isComplete = false; 
          if(Metaid != null){
              delete [select Id from Product_Meta__c where id=:Metaid];
          }
        
          isComplete = true; 
       }
      catch(Exception e){ isComplete = false;  ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); 
     }
     return null;
      
    }
     // Four methods for add and remove row
    public void InsertMoreProduct(){
        prodmetalist.add(new Product_Meta__c());
    
    }
    public void RemoveProduct(){
         integer a=prodmetalist.size();
         if(a>1){
             prodmetalist.remove(prodmetalist.size()-1);
         }
     
    }
  

}