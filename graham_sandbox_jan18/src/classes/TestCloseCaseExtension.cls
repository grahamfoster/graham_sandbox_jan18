@IsTest
public class TestCloseCaseExtension {
    static testMethod void TestCloseCaseExtension()
    {
        //create a new case - should go through fine
        List<Case> testCases = new List<Case>();
        Case testCase = new Case(QTRICS_Send_Survey__c=false, Master_Control_ID__c='Complaint-1234', 
                      Sales_Order_Number__c='001234', Training_Part_Number__c='ABC1234', Complaint__c=true);
        testCases.add(testCase);
        //expect this case to throw a not completed exception
        Case testCase1 = new Case(QTRICS_Send_Survey__c=false, Master_Control_ID__c='Complaint-1234', Status='Closed', 
                      Sales_Order_Number__c='001234', Training_Part_Number__c='ABC1234', Complaint__c=true);
        testCases.add(testCase1);
        insert testCases;
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        CloseCaseExtension cce = new CloseCaseExtension(sc);
        try{
            //try it without any comments
            cce.closeCase();
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Please enter internal comments') ? true : false;
            system.assertEquals(expectedExceptionThrown,true);      
            }
        cce.comment.CommentBody = 'Closing..';
            //added a comment - try again
            cce.closeCase();
        system.assertEquals(true, [SELECT IsClosed FROM Case WHERE Id=:testCase.Id].IsClosed);
        
        sc = new ApexPages.StandardController(testCase1);
        cce = new CloseCaseExtension(sc);
		try{
            //try it without any comments
            cce.closeCase();
        }catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('You cannot update a closed case') ? true : false;
            system.assertEquals(expectedExceptionThrown,true);      
            }
        Test.stopTest();
        }
        
        
    }