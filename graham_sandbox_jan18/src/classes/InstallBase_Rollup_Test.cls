/**
 * @author Brett Moore
 * @created - May 2017
 * @Revision  
 * @Last Revision 
 * 
 * Testing for the following classes:
 * 		IB_Rollup_Schedule 
 *      IB_Rollup_Batch 
 *      InstallBase_Rollup
 **/
@isTest
public class InstallBase_Rollup_Test {

    @testSetup static void setup() {
        Date nowDate = date.today();
        Date startDate = nowDate.addMonths(-3);
        Date endDate = nowDate.addMonths(6);
		Date startDate2 = nowDate.addMonths(-2);
        Date endDate2 = nowDate.addMonths(5);
        
    //  Account
        Account acct = new Account(Name = 'Test Co #1',CurrencyIsoCode = 'USD', BillingCountry = 'USA', Site='a');
        insert acct;
	// Contact
        Contact c = new Contact(AccountId= acct.id, firstname='Bob', lastname='Smith', Contact_Status__C='Active');
        insert c;
        
    //  Product2
		List<Product2> prod = new List<Product2>();
        prod.add(new Product2(Name='Instrument',ProductCode='Instrument',SVC_OrclSerInstruModelName__c='Mass Spec')); 
		prod.add(new Product2(Name='Assurance 0PM',ProductCode='ABSX Assurance 0PM',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Response 24',ProductCode='Response 24',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Response 48',ProductCode='Response 48',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Recertification/PM',ProductCode='Recertification/PM',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Qualification',ProductCode='Qualification',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Additional PQ',ProductCode='Additional PQ',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Additional PQ',ProductCode='Additional PM',SVC_OrclSerInstruModelName__c=''));
		prod.add(new Product2(Name='Additional PQ',ProductCode='Additional OQ',SVC_OrclSerInstruModelName__c=''));
        insert prod;        
        
	//  SVMXC__Installed_Product__c

        List<SVMXC__Installed_Product__c> ip = new List<SVMXC__Installed_Product__c>();
        ip.add(new SVMXC__Installed_Product__c(NAME='API3000 - AF28211410',CURRENCYISOCODE=acct.CURRENCYISOCODE,SVMXC__COMPANY__C=acct.id, 
        	SVMXC__PRODUCT__C=prod.get(0).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id,Current_Coverage_Start_Date__c = nowDate, Current_Coverage_End_Date__c =  nowDate, Current_Item_Master__c = '', Current_Coverage_Description__c = '', Document_Number__c = '', Seconds_Line_Contract__c = ''));              
        insert ip;          

        //  Service Maintenance Contract
        List<SVMXC__Service_Contract__c> SMC = new List<SVMXC__Service_Contract__c>();
        SMC.add(new SVMXC__Service_Contract__c(Name='contract MS1', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=acct.CURRENCYISOCODE, 
                SVMXC__COMPANY__C=acct.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=endDate, PARENT_CONTRACT_NUMBER__c='12345'+acct.site, 
                SVC_CATEGORY__C='Service Agreement', Renewal_Opportunity_Created__c=FALSE,SVMXC__Start_Date__c=startDate, SVMXC__Active__c = True));
        insert SMC;
        
        //  Covered Product 
        List<SVMXC__Service_Contract_Products__c> SMCprod = new List<SVMXC__Service_Contract_Products__c>();
        SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate,SVMXC__PRODUCT__C=prod.get(1).id, SVMXC__START_DATE__C=startDate, EXTERNAL_ID__C='33750'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(1).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 1'));
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(2).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='33751'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(2).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 2'));
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(3).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='33752'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(3).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 3'));  
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(4).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='33753'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(4).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 4'));  
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(5).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='33754'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(5).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 5'));  
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(6).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='33755'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(6).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 6'));  
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(7).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='33756'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(7).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 7'));  
		SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=SMC.get(0).CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id, 
                    SVMXC__END_DATE__C=endDate2,SVMXC__PRODUCT__C=prod.get(8).id, SVMXC__START_DATE__C=startDate2, EXTERNAL_ID__C='337597'+SMC.get(0).id,
                    SVC_ITEM_MASTER_NAME__C=prod.get(8).id, SVMXC__Installed_Product__c=ip.get(0).id, SVC_Contract_Subline_Status__c='ACTIVE',
                    SVC_Coverage_Description__c='Desc 8'));  
				
        insert SMCprod; 
        
    }
    static testmethod void testSchedule(){

		system.debug('***@@@ ----------------- Start Schedule' );      
		Test.startTest();
        	IB_Rollup_Schedule ibRollUp = new IB_Rollup_Schedule();
        	String sch = '20 30 8 10 2 ?';
           	String jobID = system.schedule('schedule batch Job1',sch ,ibRollUp);
        Test.stopTest();        
		system.debug('***@@@ ----------------- Stop Schedule' );         
    } 
    static testmethod void testBatch(){

		system.debug('***@@@ ----------------- Start Batch' );      
		Test.startTest();
        	IB_Rollup_Batch b = new IB_Rollup_Batch();
			id batchInstanceId = database.executebatch(b,100);
        Test.stopTest();        
		system.debug('***@@@ ----------------- Stop Batch' );         
    }     
    
}