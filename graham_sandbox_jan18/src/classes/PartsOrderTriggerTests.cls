/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 27/2015
**
** Test coverage for PartsOrderTrigger and PartsOrderTriggerHandler
** 
**/
@isTest(SeeAllData=true)
public class PartsOrderTriggerTests 
{
	public static testMethod void test1()
    {
        RecordType rmaRT = SfdcUtil.getRecordType('SVMXC__RMA_Shipment_Order__c', 'RMA');
		SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c(SVMXC__Order_Status__c='Open', RecordTypeId = rmaRT.Id);
        insert partsOrder;
        
        SVMXC__RMA_Shipment_Line__c poLine = new SVMXC__RMA_Shipment_Line__c(SVMXC__RMA_Shipment_Order__c = partsOrder.Id, SVMXC__Line_Status__c = 'Completed');
        insert poLine;
        
        partsOrder.SVMXC__Order_Status__c = 'Submitted';
        update partsOrder;
        
        poLine = [select SVMXC__Line_Status__c from SVMXC__RMA_Shipment_Line__c where Id = :poLine.Id];
        system.assertEquals('Submitted', poLine.SVMXC__Line_Status__c);
    }
}