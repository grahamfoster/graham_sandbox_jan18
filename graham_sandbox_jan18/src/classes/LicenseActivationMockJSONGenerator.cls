@isTest
global class LicenseActivationMockJSONGenerator implements HttpCalloutMock {

    protected String JSONResponse = null;
    protected Integer ResponseCode = 0;

    public LicenseActivationMockJSONGenerator(String jsonResponse, Integer responseCode) {
        System.debug('LicenseActivationMockJSONGenerator(String jsonResponse) was just called...');
        this.JSONResponse = jsonResponse;
        this.ResponseCode = responseCode;
        System.debug('LicenseActivationMockJSONGenerator(String jsonResponse) is going to return...');
    }

    global HTTPResponse respond(HTTPRequest req) {
        System.debug('respond(HTTPRequest req) was just called...');
        HttpResponse response = new HttpResponse();
        System.debug('Constructed the response...');
        response.setHeader('Content-Type', 'application/json;charset=UTF-8');
        System.debug('Set the response header...');
        if (this.JSONResponse == null)
            System.debug('JSONResponse is null...');
        else {
            response.setBody(this.JSONResponse);
            System.debug('Set the response body to "' + this.JSONResponse + '"...');
        }
        response.setStatusCode(this.ResponseCode);
        System.debug('Set the response status code...');
        System.debug('respond(HTTPRequest req) is going to return...');
        return response;
    }
}