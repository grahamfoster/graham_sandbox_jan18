/*
 *	ContactTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description 
 *	Daniel	11/23/2016 Added Country and State Mapping functionality in SetCountryMap method
 */
public class ContactTriggerHandler extends TriggerHandler
{

	public ContactTriggerHandler() {
		//this.setMaxLoopCount(1);
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void afterInsert() {
    	KeyOpinionLeader();
            createOrUpdateLMSUser();
	}
	override protected void afterUpdate() {
    	KeyOpinionLeader();
            createOrUpdateLMSUser();
	}

    override protected void beforeInsert(){        
		updateContactEntitlement();
    	SetCountryMap();
		pendingAccountAssignmentProcessing();
    }    
    override protected void beforeUpdate(){        
		updateContactEntitlement();
        SetCountryMap();
		pendingAccountAssignmentProcessing();
    }
    
	// **************************************************************************
	// 		private methods
	// **************************************************************************
    


/*******
 * Created by = Daniel 
 * Purpose of 'ContactSetCountryMap' = to assign Contacts to State and Country mapping records
 * See https://sciexbase.atlassian.net/wiki/display/SFDC/Country+and+State+Mapping
 * *****/    
private void SetCountryMap()
{
        List<Contact> ContactNeedMapping = new List<Contact>();

	for( Contact newContact : (List<Contact>)Trigger.new){
   Contact oldContact = NULL;

   //If update, grab old record for comparison
   if(Trigger.IsUpdate) 
   { 
      oldContact = ((Map<Id, Contact>)Trigger.oldMap).get(newContact.ID);  
   }
   
   //Add to list only if NEW or Country/State field changes
   if(Trigger.IsInsert || oldContact.MailingCountry != newContact.MailingCountry ||  oldContact.MailingState != newContact.MailingState)       
   {    
     ContactNeedMapping.add(newContact);
   }
	}  
    
if(ContactNeedMapping.size()>0)
    {
        List<Country_Mapping__c> allCountries = [Select Name, id,Permutations__c, Country_Code__c, Country_Phone_Code__c, Sales_Region__c 
                                                 from Country_Mapping__c];        
        List<State_Mapping__c> allStates = [Select Name, id, State_Province_Permutations__c, Country_Mapping__c 
                                            from State_Mapping__c];         
        Map<String,Country_Mapping__c> countryAlias = new Map<String,Country_Mapping__c>();  
        Map<String,State_Mapping__c> stateAlias = new Map<String,State_Mapping__c>(); 
        
        for(Country_Mapping__c country :allCountries){
            // add country code, we will be treating it the same way as permutations
          countryAlias.put(country.Country_Code__c.toUpperCase(),country);

            // split all permutations
          if(!String.isBlank(country.Permutations__c)){
              if(country.Permutations__c.contains(';')){
                for(String alias : country.Permutations__c.split(';')) {
            if(!String.isBlank(alias.trim())) 
                            countryAlias.put(alias.trim().toUpperCase(),country);
                    }
              } else {
                      countryAlias.put(country.Permutations__c.trim().toUpperCase(),country);
                }
        }
            
      }
        for(State_Mapping__c state :allStates){
                    
                    if(!String.isBlank(state.State_Province_Permutations__c)){
                  
                        if(state.State_Province_Permutations__c.contains(';') ){
                    
                            for(String alias : state.State_Province_Permutations__c.split(';')) {
                                if(!String.isBlank(alias.trim())  )
                                    stateAlias.put(alias.trim().toUpperCase(),state);
                            }
                                } else {
                                    stateAlias.put(state.State_Province_Permutations__c.trim().toUpperCase(),state);
                                }
                            }  
                        }        
    
for(Contact Contactrecord : ContactNeedMapping){
        
    ID countryID;
    String countryName;
    ID stateID;
    String stateName; 

    if(Contactrecord.MailingCountry != null){  
                
        IF(countryAlias.containsKey(Contactrecord.MailingCountry.trim().toUpperCase()))
        {
            countryID = countryAlias.get(Contactrecord.MailingCountry.trim().toUpperCase()).id;
        }       
                //Country MAP FOUND --> Populate the Country and Country Mapping fields
                if(countryID != NULL)
                {   
                    Contactrecord.Country_Mapping__c = countryID;
                    countryName = countryAlias.get(Contactrecord.MailingCountry.trim().toUpperCase()).name;
                
                    if(String.isNotBlank(countryName))
                    {
                        Contactrecord.MailingCountry = countryName;
                        Contactrecord.Country_Phone_Code__c = countryAlias.get(Contactrecord.MailingCountry.trim().toUpperCase()).Country_Phone_Code__c;
                        Contactrecord.Sales_Region_GlobPick__c = countryAlias.get(Contactrecord.MailingCountry.trim().toUpperCase()).Sales_Region__c;
                    }
                } 
                
                // Country MAP NOT FOUND --> NULL out the Country and Country Mapping fields
                if(countryID == NULL)
                { 
                    Contactrecord.Country_Mapping__c = NULL;
                    Contactrecord.Country_Phone_Code__c = NULL;
                    Contactrecord.Sales_Region_GlobPick__c = NULL;
                    
                    //Contactrecord.Country = NULL;
                }
                
            }

    if(countryID != null && Contactrecord.MailingState != null)   {  
    
        IF(stateAlias.containsKey(Contactrecord.MailingState.trim().toUpperCase()))
        {
            stateID = stateAlias.get(Contactrecord.MailingState.trim().toUpperCase()).id;
        }         
        
        if(stateID != NULL && countryID == stateAlias.get(Contactrecord.MailingState.trim().toUpperCase()).Country_Mapping__c)
            {   
                Contactrecord.State_Mapping__c = stateID;
                stateName = stateAlias.get(Contactrecord.MailingState.trim().toUpperCase()).name;
                  
                if(String.isNotBlank(stateName))
                {
                    Contactrecord.MailingState = stateName;
                }
            }
    }     

    // Country BLANK --> NULL out the Country Mapping fields
    if(Contactrecord.MailingCountry == null){  
        Contactrecord.Country_Mapping__c = NULL;
    }
            
    // State BLANK --> NULL out the State Mapping fields
    if(Contactrecord.MailingState == null  || String.isBlank(stateName)){  
        Contactrecord.State_Mapping__c = NULL;
    }
    } 
  }
 }//END OF ContactSetCountryMap
    
    private Map<Id,Account> accountsMap;
    
	private void KeyOpinionLeader()
    {
        Set<Id> accountIds = new Set<Id>();
        Set<Id> deleteSet = new Set<Id>();
        Map<Id, Strategic_Customer_Profile__c> contactsMap = new Map<Id, Strategic_Customer_Profile__c >();
		for(Contact c : (List<Contact>)Trigger.new)
        {
            if(isChanged(c, Contact.Key_Opinion_Leader_KOL__c.getDescribe().getName()))
            {
                if(c.Key_Opinion_Leader_KOL__c)
                {
                    //Set the related
                    contactsMap.put(c.Id, null);
                    accountIds.add(c.AccountId);
                }else{
                    deleteSet.add(c.Id);
                }
            }
        }
        
        //load parent accounts
        accountsMap = new Map<Id,Account>([select Id, ParentId from Account where Id in :accountIds]);
        
        //delete the ones that were nulled out
        if(deleteSet.size() > 0) delete [select Id from Strategic_Customer_Profile__c where Contact__c in :deleteSet];
        
        //gather the profiles that exist
        for(Strategic_Customer_Profile__c scProfile : [select Id, Contact__c from Strategic_Customer_Profile__c where Contact__c in :contactsMap.keySet()]) { contactsMap.put(scProfile.Contact__c, scProfile); }
        
        Strategic_Customer_Profile__c[] scpUpserts = new Strategic_Customer_Profile__c[]{};
        for(Id contactId : contactsMap.keySet())
        {
            Contact cont = (Contact)Trigger.newMap.get(contactId);
            Strategic_Customer_Profile__c scProfile = contactsMap.get(contactId);
            scProfile = copyContactToProfile(scProfile, cont);
            scpUpserts.add(scProfile);
        }
        
        upsert scpUpserts;
    }
    
    private Strategic_Customer_Profile__c copyContactToProfile(Strategic_Customer_Profile__c scProfile, Contact cont)
    {
        if(scProfile == null) {
            scProfile = new Strategic_Customer_Profile__c(Contact__c = cont.Id);
        }
        Account locationAccount = accountsMap.get(cont.AccountId);
        if(locationAccount != null) scProfile.Location__c = locationAccount.Id;
        if(locationAccount != null && locationAccount.ParentId != null) scProfile.Key_Account__c = locationAccount.ParentId;
        return scProfile;
    }

	/* 
	 *	Check if the specified field has been changed.
	 *		- Copied from BaseTriggerHandler.isChanged()
	 */
    private Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }
    

    //Absorb LMS code, refactored from AbsorbContactTriggerHandler
    public void createOrUpdateLMSUser() 
    {
        for(Contact c : (List<Contact>)Trigger.new)         
        {
            if(isChanged(c, 'LMS_Trigger_User_Create__c') && c.LMS_Trigger_User_Create__c && c.LMS_User_ID__c == null && (!String.isBlank(c.LMS_Username__c) || !String.isBlank(c.Email)))
            {
            	if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()) {
     				System.enqueueJob(new CreateLMSUserQueueable(c));
				} 
            }

            //if some fields changed, sync them over to the Absorb LMS
            if(c.LMS_User_ID__c != null && (isChanged(c,'Email') || isChanged(c,'Phone') || isChanged(c, 'MailingStreet') || isChanged(c,'MailingCity') || isChanged(c,'MailingState') || isChanged(c,'MailingCountry') || isChanged(c,'Market_Segment_St__c') || isChanged(c,'LMS_Primary_Workflow__c') || isChanged(c,'LMS_Learner_Level__c') || isChanged(c, 'Sales_Region_GlobPick__c') || isChanged(c, 'LMS_Content_Entitled__c')))
            {
                //sync to LMS with update user call
				if(Limits.getQueueableJobs() < Limits.getLimitQueueableJobs()){
     				System.enqueueJob(new UpdateLMSUserQueueable(c));
				} 
            }
        }
    }

    public class UpdateLMSUserQueueable implements Queueable, Database.AllowsCallouts
    {
        private Contact c;
        
        public UpdateLMSUserQueueable(Contact c)
        {
            this.c = c;
        }

        public void execute(QueueableContext context) 
        {
            this.c = AbsorbUtil.findContact(this.c.Id);
            AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
            AbsorbModel.User u = AbsorbUtil.createAbsorbUser(c);
            //call update user
            try {
                String updateUserResponse = absorbClient.updateUser(u);
            }catch(Exception e){
                AbsorbLogger.error(e.getMessage() + '\n' + e.getStackTraceString());
            }finally{
                absorbClient.close();
            }
        }
    }

    public class CreateLMSUserQueueable implements Queueable, Database.AllowsCallouts
    {
        private Contact c;
        
        public CreateLMSUserQueueable(Contact c)
        {
            this.c = c;
        }

        public void execute(QueueableContext context) 
        {
            this.c = AbsorbUtil.findContact(this.c.Id);
            AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
            AbsorbModel.User u = AbsorbUtil.createAbsorbUser(c);
            try {
                AbsorbModel.CreateUserResponse creatUserResponse = absorbClient.createUser(u);
                Contact contactUpdate = new Contact(Id = c.Id);
                contactUpdate.LMS_User_ID__c = creatUserResponse.Id;
                contactUpdate.LMS_User_Created_Date__c = DateTime.now();
                if(String.isBlank(this.c.LMS_Username__c)) contactUpdate.LMS_Username__c = this.c.Email;
                update contactUpdate;
            }catch(Exception e){
                AbsorbLogger.error(e.getMessage() + '\n' + e.getStackTraceString());
            }finally{
                absorbClient.close();
            }
            
        }
    }
	
	public static Boolean runPendingCode = true;
	
	/*****************************************************************************************************
	* @description Assigns a regional account to any contacts placed into the pending account assignment
	* master account.  This is to enable regional direction of cases as well as assignment of survey
	* results from these contacts to a region
	* Author - Graham Foster 2017-07-04
	*/
	private void pendingAccountAssignmentProcessing()
	{
		//make sure it only runs once in any insert
		if(runPendingCode)
		{
			//query the pending account assignment accounts
			List<Account> pendingAssignmentAccounts = [SELECT Id, Name, Country_Mapping__c, 
			Country_Mapping__r.SCIEXNow_Region__c FROM Account WHERE Name LIKE 'Pending Account Assignment%'];
			Map<String, Id> pendingAccountMap = new Map<String, Id>();
			for(Account a : pendingAssignmentAccounts)
			{
				//add the Ids to a map with their SCIEXNow Region as the Key
				if(a.Name == 'Pending Account Assignment')
				{
					pendingAccountMap.put('DEFAULT', a.Id);
				} else 
				{
					pendingAccountMap.put(a.Country_Mapping__r.SCIEXNow_Region__c, a.Id);
				}
			}
			//loop through the trigger and if the new contacts are in the
			//pending account assignment account then add their country mapping
			//Id to a set so that we can query the sciexnow region
			Id pendingId = pendingAccountMap.get('DEFAULT');
			Set<Id> pendingMappingId = new Set<Id>();
			for(Contact c : (List<Contact>)Trigger.new)
			{
				if(c.AccountId == pendingId)
				{
					pendingMappingId.add(c.Country_Mapping__c);
				}
			}

			if (pendingMappingId.size() > 0)
			{
				//if we have any country mappings to query then get the SCIEXNow Region
				Map<Id, Country_Mapping__c> pendingMappings = new Map<Id, Country_Mapping__c>(
				[SELECT Id, SCIEXNow_Region__c FROM Country_Mapping__c WHERE Id IN :pendingMappingId]);
				System.debug('**GF** CountryMaps:' + pendingMappings.values());
				//loop the trigger and if the contact is in the master pending account assignment 
				//then check if we can get the sciexnow region and if so then get the 
				//pending account for that region
				for(Contact c : (List<Contact>)Trigger.new)
				{
					if(c.AccountId == pendingId && pendingMappings.containsKey(c.Country_Mapping__c))
					{
						//get the correct account to assign the contact to
						System.debug('**GF** CountryMapping Id:' + c.Country_Mapping__c);
						Country_Mapping__c contMap = pendingMappings.get(c.Country_Mapping__c);
						String pRegion = contMap.SCIEXNow_Region__c;
						if(!String.isEmpty(pRegion) && pendingAccountMap.containsKey(pRegion))
						{
							Id pAccId = pendingAccountMap.get(pRegion);
							if(!String.isEmpty(pAccId))
							{
								//assign the pending assignment account to the contact
								c.AccountId = pAccId;
							}
						}	
					}
				}
			}
			//ensure the code does not execute again
			runPendingCode = false;
		}
	}        
    
    private void updateContactEntitlement(){
        Set<Id> contactIdsToCheck = new Set<Id>();
        for(Contact c : (List<Contact>)Trigger.new) {
            if(c.Content_Eligible_Approved__c != null && c.Content_Eligible_Approved__c) {
                c.LMS_Content_Entitled__c = true;
            }else if(Trigger.isUpdate) {
                Contact oldContact = (Contact)Trigger.oldMap.get(c.Id);
                if(oldContact.Content_Eligible_Approved__c != null && oldContact.Content_Eligible_Approved__c) {
                    //changed to content eligible approved off, must check related assets
                    contactIdsToCheck.add(c.Id);
                }
            }
        }
        
        if(!contactIdsToCheck.isEmpty()) {
            Map<Id, Boolean> contactEntitlementsById = new Map<Id, Boolean>();
            Map<Id, List<Asset_MultipleContacts__c>> assetLinksByContactId = new Map<Id, List<Asset_MultipleContacts__c>>();
            for(Asset_MultipleContacts__c contactLink : [
                select Id, Contact__c, Contact__r.LMS_Content_Entitled__c, Asset__c,
                Asset__r.SCIEXNow_Hardware_Entitled__c, Asset__r.SCIEXNow_Software_Entitled__c,
                Asset__r.SCIEXNOWSoftware_Upgrade_Entitled__c
				from Asset_MultipleContacts__c where Asset__c != null and Contact__c in :contactIdsToCheck
            ]){
                if(assetLinksByContactId.containsKey(contactLink.Contact__c)) {
                    assetLinksByContactId.get(contactLink.Contact__c).add(contactLink);
                }else{
                    assetLinksByContactId.put(contactLink.Contact__c, new Asset_MultipleContacts__c[]{ contactLink });
                }
            }
            
            for(Id contactId : contactIdsToCheck) {
                Boolean entitled = false;
                if(assetLinksByContactId.containsKey(contactId)){
                    for(Asset_MultipleContacts__c contactLink : assetLinksByContactId.get(contactId)) {
                        if(!entitled && isContentEntitled(contactLink.Asset__r)) {
                            entitled = true;
                        }
                    }
                }
                contactEntitlementsById.put(contactId, entitled);
            }

            for(Contact c : (List<Contact>)Trigger.new) {
                if(!(c.Content_Eligible_Approved__c != null && c.Content_Eligible_Approved__c) && Trigger.isUpdate) {
                    Contact oldContact = (Contact)Trigger.oldMap.get(c.Id);
                    if(oldContact.Content_Eligible_Approved__c != null && oldContact.Content_Eligible_Approved__c) {
                        if(contactEntitlementsById.containsKey(c.Id)) {
                            c.LMS_Content_Entitled__c = contactEntitlementsById.get(c.Id);
                        }
                    }
                }
            }
        }
    }

    private Boolean isContentEntitled(Asset ast) {
        return ast.SCIEXNow_Hardware_Entitled__c || ast.SCIEXNow_Software_Entitled__c || ast.SCIEXNOWSoftware_Upgrade_Entitled__c;
    }
}