public with sharing class BattleCardApp_Ctrl {

   List<Application__c> appCat {get; set;}
   List<Application_Variants__c> app {get; set;}
   Set<Id> appId = new Set<Id>();
   public string appcId {get; set;}
   public map<String,list<AppWrapper>> appMap{get;set;}
   public boolean clickedCell {get; set;}
   public string ApplicationName {get; set;}
   public string ApplicationId {get; set;}
    
   public BattleCardApp_Ctrl(){
        clickedCell = false;
        appcat = [select Id, Name, Application_Name__c from Application__c where isActive__c = true Order By Application_Name__c];
        for(Application__c ap: appcat){
            appId.add(ap.Id);
        }
        
        app = [Select Id, Name, Application_Variant_Name__c, Application__c From Application_Variants__c Where isActive__c =: true AND Application__c In: appId];
        
        appMap = new map<String,list<appWrapper>>();
        for(Application__c ap: appcat){
            list<appWrapper> appList = new list<appWrapper>();
            for(Application_Variants__c  a : app){ 
               if(ap.Id == a.Application__c ){
                   appWrapper appWrap = new appWrapper();
                   appWrap.ApplicationCategoryName = ap.Application_Name__c; 
                   appWrap.ApplicationName = a.Application_Variant_Name__c;
                   appWrap.applicationId = a.Id;
                   appList.add(appwrap);
               }
            }
            appMap.put(ap.Application_Name__c,appList); 
        } 
   }
   
      
   public void parentpage(){
      clickedcell = true;
   }
   
   
    
   
    public class AppWrapper {
        public String ApplicationCategoryName{get;set;}
        public String ApplicationName{get;set;}
        public Id applicationId{get;set;}
    }
   

}