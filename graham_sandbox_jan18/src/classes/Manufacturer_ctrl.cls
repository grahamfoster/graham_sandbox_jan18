public with sharing class Manufacturer_ctrl {

    public Manufacturer__c ManufacturerRecord{get;set;}
    public String pageDirection{get;set;}
    public Boolean isErrorMsg{get;set;}
    public Manufacturer_ctrl(ApexPages.StandardController Controller){
        isErrorMsg = false; 
        string para = ApexPages.CurrentPage().getParameters().get('selec');
        System.debug('para....'+para);
        ManufacturerRecord= (Manufacturer__c)controller.getRecord();
        pageDirection = 'false';
    }
    
    public PageReference doSaveAndNew(){
        try{
           
            insert ManufacturerRecord;
            return new pagereference('/apex/Manufacturer');
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        return Null;
    }
    public PageReference doSave(){
        try{ 
           
             //if(ManufacturerRecord != null && String.isNotEmpty(ManufacturerRecord.Manufacturer_Name__c)){
             if(ManufacturerRecord != null && String.isNotEmpty(ManufacturerRecord.Name)){
                 system.debug('fiest block');
                 isErrorMsg = false; 
                 ManufacturerRecord.isActive__c = true; 
                 upsert ManufacturerRecord;
                 pageDirection = 'true';
             
             } else {
                 system.debug('second block');
                 isErrorMsg = true; 
                 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ' Manufacturer Name: You must enter a value'));
             }

        }
        catch(Exception e){
            isErrorMsg = true; 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            pageDirection = 'false';             
        }
        return null;
    }
    
    public pageReference doCancel(){
       pageDirection = 'true';
        string CancelURL=ApexPages.currentPage().getParameters().get('retURL');
        if(CancelURL !=null && CancelURL.trim()!=''){
            return new PageReference(CancelURL);
        }
        return null;
    }
    
    public void doDelete(){
        if(ManufacturerRecord != null && ManufacturerRecord.Id != null){
            try{
                isErrorMsg = false;
                delete ManufacturerRecord;
            } catch(Exception e){ isErrorMsg = true;  ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));  
                
                
            }  
        }    
    }
}