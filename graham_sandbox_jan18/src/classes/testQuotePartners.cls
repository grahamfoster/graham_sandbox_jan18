/*********************************************************************
Name  : testQuotePartners
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuotePartners  

*********************************************************************/

public class testQuotePartners{

  public static testMethod void checkQuotePartners() {

    QuotePartners.ArrayOfpartnerType  cls1 = new QuotePartners.ArrayOfpartnerType();
    QuotePartners.ABI_SalesQuotation_process_loadSAPpartnersInSFDCWsd_Port  cls2 = new QuotePartners.ABI_SalesQuotation_process_loadSAPpartnersInSFDCWsd_Port();
    QuotePartners.loadSAPpartnersInSFDC request_x = new QuotePartners.loadSAPpartnersInSFDC();
    String str = cls2.loadSAPpartnersInSFDC(request_x.partnerInfo);
    QuotePartners.loadSAPpartnersInSFDCResponse  cls3 = new QuotePartners.loadSAPpartnersInSFDCResponse();
    QuotePartners.partnerInfo  cls4 = new QuotePartners.partnerInfo();
    QuotePartners.partnerType  cls5 = new QuotePartners.partnerType();
    QuotePartners.loadSAPpartnersInSFDC  cls6 = new QuotePartners.loadSAPpartnersInSFDC();
  }  
}