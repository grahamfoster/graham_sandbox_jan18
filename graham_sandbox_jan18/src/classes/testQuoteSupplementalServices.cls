/*********************************************************************
Name  : testQuoteSupplementalServices 
Author: Appirio, Inc.
Date  : July 30, 2008 
Usage : Used to test the QuoteSupplementalServices 

*********************************************************************/
public class testQuoteSupplementalServices {

  public static testMethod void checkQuoteSupplementalServices() {

    QuoteSupplementalServices  cls = new QuoteSupplementalServices();
    QuoteSupplementalServices.itemListForTemplates  clsListTemp = new QuoteSupplementalServices.itemListForTemplates();
    QuoteSupplementalServices.getPartners   clsPartner = new QuoteSupplementalServices.getPartners();
    QuoteSupplementalServices.detailOfLineItems   clsLineItem = new QuoteSupplementalServices.detailOfLineItems();
    QuoteSupplementalServices.ArrayOfT_ITEMS   clsArray = new QuoteSupplementalServices.ArrayOfT_ITEMS();
    QuoteSupplementalServices.priceInputDoc   clsPrice = new QuoteSupplementalServices.priceInputDoc();
    QuoteSupplementalServices.partners    clspartners  = new QuoteSupplementalServices.partners ();
    QuoteSupplementalServices.getTemplateDetailsResponse   clsTemp  = new QuoteSupplementalServices.getTemplateDetailsResponse();
    QuoteSupplementalServices.getQuoteApprovalStatusResponse    cls2  = new QuoteSupplementalServices.getQuoteApprovalStatusResponse();
    QuoteSupplementalServices.partnerData    cls3  = new QuoteSupplementalServices.partnerData ();
    QuoteSupplementalServices.getPriceDetailsResponse    cls4  = new QuoteSupplementalServices.getPriceDetailsResponse();
    QuoteSupplementalServices.T_ITEMS    cls5  = new QuoteSupplementalServices.T_ITEMS();
    QuoteSupplementalServices.ArrayOfitemListForTemplates     cls6  = new QuoteSupplementalServices.ArrayOfitemListForTemplates();
    QuoteSupplementalServices.quoteNumber     cls7  = new QuoteSupplementalServices.quoteNumber();
    QuoteSupplementalServices.templateDetails     cls8  = new QuoteSupplementalServices.templateDetails();
    QuoteSupplementalServices.getPartnersResponse     cls9  = new QuoteSupplementalServices.getPartnersResponse();
    QuoteSupplementalServices.ArrayOfdetailOfLineItems      cls10  = new QuoteSupplementalServices.ArrayOfdetailOfLineItems();
    QuoteSupplementalServices.quoteStatus      cls11  = new QuoteSupplementalServices.quoteStatus();
    QuoteSupplementalServices.partnerType      cls12  = new QuoteSupplementalServices.partnerType();
    QuoteSupplementalServices.getQuoteApprovalStatus      cls13  = new QuoteSupplementalServices.getQuoteApprovalStatus();
    QuoteSupplementalServices.ABI_SalesQuotation_process_sq3Wsd_Port      cls14  = new QuoteSupplementalServices.ABI_SalesQuotation_process_sq3Wsd_Port();
    QuoteSupplementalServices.getPriceDetails       cls15  = new QuoteSupplementalServices.getPriceDetails();
    QuoteSupplementalServices.ArrayOfpartnerType       cls16  = new QuoteSupplementalServices.ArrayOfpartnerType();
    QuoteSupplementalServices.getTemplateDetails       cls17  = new QuoteSupplementalServices.getTemplateDetails();
    QuoteSupplementalServices.statusLog       cls18  = new QuoteSupplementalServices.statusLog();
    QuoteSupplementalServices.lineItemDetails        cls19  = new QuoteSupplementalServices.lineItemDetails();
    QuoteSupplementalServices.partnerInfo        cls20  = new QuoteSupplementalServices.partnerInfo();
    QuoteSupplementalServices.templateInfo        cls21  = new QuoteSupplementalServices.templateInfo();
    QuoteSupplementalServices.ArrayOfstatusLog         cls22  = new QuoteSupplementalServices.ArrayOfstatusLog();
    QuoteSupplementalServices.ArrayOfpartners         cls23  = new QuoteSupplementalServices.ArrayOfpartners();   
  }  
}