@isTest(SeeAllData=true)
public class QuoteLineEmailController_Test {

    public static testMethod void testQuoteLineEmailController() {
     
        Account testAccount = new Account(name='QuoteLineEmailControllerTEST');
        insert testAccount;
        
        Account acc = [Select ID, name from Account where name = 'QuoteLineEmailControllerTEST'];
        system.debug('Account TestClass= ' + acc.id + '-' + acc.Name); 
        
        Opportunity testOpp = new Opportunity(name='TestOppQuoteLineEmailController', AccountID = acc.id, CurrencyISOCode='CAD', StageName = 'Recognition of Needs', CloseDate = Date.today() );
        insert testOpp;
        
        Opportunity opp = [Select ID, name from Opportunity where name = 'TestOppQuoteLineEmailController'];
        system.debug('Opportunity TestClass = ' + opp.id + '-' + opp.Name); 
        
        PriceBook2 pb = new PriceBook2(name='TestPB', CurrencyISOCode = 'CAD', isActive = True);
        insert pb;
        system.debug('PriceBook TestClass = ' + pb.id + '-' + pb.Name);    
        
        SBQQ__Quote__c testQuote = new SBQQ__Quote__c (CurrencyIsoCode  = 'CAD',
            SBQQ__Account__c = acc.id,
            SBQQ__Opportunity2__c = opp.id,
            SBQQ__PricebookId__c = pb.id,
            SBQQ__PriceBook__c = pb.id
            //SBQQ__PrimaryContact__c   003F000002J3Ec0IAF
            //SBQQ__SalesRep__c 005F0000003JwpdIAC
        );
        insert testQuote;
        system.debug('Quote TestClass= ' + testQuote.id + '-' + testQuote.Name); 
        
        Product2 testprodnorm = new Product2 (name='testProd');         insert testprodnorm;
        Product2 testprodtrade = [Select ID, name from Product2 where productcode = 'S202-DHR'];
        Product2 testprodopt = new Product2 (name='testOpt');         insert testprodopt;
        system.debug('Products TestClass = ' + testprodnorm.id + ', ' + testprodtrade.Id + ', ' + testprodopt);
                     
        PriceBook2 pbstnd = [Select ID from pricebook2 where isStandard = true];
        system.debug('Stnd Pricebook TestClass= ' + pbstnd.id);
                     
        PriceBookEntry pbe1 = new PriceBookEntry (Pricebook2ID = pbstnd.Id, Product2ID = testprodnorm.id,CurrencyISOCode = 'CAD', UnitPrice = 5); insert pbe1;
        //PriceBookEntry pbe2 = new PriceBookEntry (Pricebook2ID = pbstnd.Id, Product2ID = testprodtrade.id,CurrencyISOCode = 'CAD', UnitPrice = 5); insert pbe2;       
        PriceBookEntry pbe3 = new PriceBookEntry (Pricebook2ID = pbstnd.Id, Product2ID = testprodopt.Id,CurrencyISOCode = 'CAD', UnitPrice = 5); insert pbe3;      
        PriceBookEntry pbe4 = new PriceBookEntry (Pricebook2ID = pb.Id, Product2ID = testprodnorm.id,CurrencyISOCode = 'CAD', UnitPrice = 5); insert pbe4;
        PriceBookEntry pbe5 = new PriceBookEntry (Pricebook2ID = pb.Id, Product2ID = testprodtrade.id,CurrencyISOCode = 'CAD', UnitPrice = 5); insert pbe5;       
        PriceBookEntry pbe6 = new PriceBookEntry (Pricebook2ID = pb.Id, Product2ID = testprodopt.Id,CurrencyISOCode = 'CAD', UnitPrice = 5); insert pbe6;      
        system.debug('PriceEntries Created TestClass ' + pbe6.id);
        
        SBQQ__QuoteLine__c qli1 = new SBQQ__QuoteLine__c (CurrencyIsoCode='CAD', SBQQ__Product__c = testprodnorm.Id, SBQQ__Quote__c = testQuote.id, SBQQ__Optional__c=FALSE); insert qli1;
        SBQQ__QuoteLine__c qli2 = new SBQQ__QuoteLine__c (CurrencyIsoCode='CAD', SBQQ__Product__c = testprodtrade.Id, SBQQ__Quote__c = testQuote.id, SBQQ__Optional__c=FALSE); insert qli2;
        SBQQ__QuoteLine__c qli3 = new SBQQ__QuoteLine__c (CurrencyIsoCode='CAD', SBQQ__Product__c = testprodopt.Id, SBQQ__Quote__c = testQuote.id, SBQQ__Optional__c=TRUE); insert qli3;
        
        SBQQ__QuoteLine__c[] quoteLines = [Select ID from SBQQ__QuoteLine__c where SBQQ__Quote__c =: testQuote.id];
        system.debug('Quote Lines TestClass= ' + quoteLines.size());         
        
        // Verify that the success page displays
        System.assertEquals(3, quoteLines.size());
        
        SBQQ__QuoteLine__c[] buntest = [SELECT ID FROM SBQQ__QuoteLine__c WHERE (NOT SBQQ__ProductCode__c LIKE '%Bundle') AND
                                     SBQQ__Quote__c = :testQuote.Id];

        QuoteLineEmailController con = new QuoteLineEmailController();
        	con.QuoteID = testQuote.id;
        	System.assertEquals(testQuote.Id, con.getQuotee().id);      
        	System.assertEquals(quoteLines.size(), con.getQuoteLines().size());        	        
        	system.assert(true, con.showOptionalSection);
        	system.assert(true, con.showTradeInSection);
        	system.assertEquals(buntest.size(), con.getCSVQuoteLines().size());
    }
}