public class LeadCallAssignmentRules {
    @InvocableMethod
    public static void LeadAssign(List<Id> LeadIds)
    {
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;          
            Lead Leads=[select id, ILS_Assignment_Needed__c, Status from lead where lead.id in :LeadIds];
        	Leads.ILS_Assignment_Needed__c = FALSE;
        	Leads.Status = 'Open';
            Leads.setOptions(dmo);
            update Leads;
   }
}