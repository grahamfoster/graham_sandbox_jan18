public with sharing class Battlecard_Ctrl 
{
	Public Battlecard__c BattlecardRecord{get;set;}   
    public Battlecard_Ctrl(ApexPages.StandardController controller)
    {        
        BattlecardRecord = (Battlecard__c)Controller.getRecord();            
    }
    public PageReference doSaveAndNew(){        
        try{
            insert BattlecardRecord;
            return new pagereference('/apex/Battlecard');
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }   
        return null;
    }
    public PageReference doSave(){
        try{
            insert BattlecardRecord;
             if(BattlecardRecord.Id != null || (BattlecardRecord.Id) != '')
            return new PageReference('/'+BattlecardRecord.Id);            
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }
        return null;
    }
    public PageReference doCancel(){
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
            return new PageReference (cancelURL);
        }
        return null;    
    }
}