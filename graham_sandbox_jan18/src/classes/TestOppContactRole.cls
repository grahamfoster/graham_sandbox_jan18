/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : December, 2012
Description    : This test class for triggers: NewOppCrContactRole, UpdateOppContactRole, leadUpdateOppWithPrimaryContactRole and class: OppPrimaryContactHelper 
History        :

*/
@isTest
private class TestOppContactRole {

    static testMethod void myUnitTest() 
    {
    	Test.startTest();
        Contact Con = new Contact();
		Account Acc = new Account();
		CampaignMember CoMem = new CampaignMember();
		Campaign Com = new Campaign();
		Opportunity Opp = new Opportunity();
		OpportunityContactRole ConRole = new OpportunityContactRole();
		Lead lead = new Lead();
		
		
		Acc.Name = 'Javante';
		Acc.BillingCountry='United States';
		Acc.Country__c = 'United States';
        Acc.Description = 'Testing delete department on account change.';  
        Acc.Customer_Classification__c='Good';		
        insert Acc;

        lead.LastName = 'Test';
        lead.Company = 'Javante';
        lead.Intent_To_Purchase__c = 'AA - Quote Requested';
        lead.Status = 'Open';
        lead.Marketing_Lead_Score__c = '4';
        //lead.Status = 'Converted-Opportunity Created';
        insert lead;
        
        Con.LastName = 'Test';
        Con.AccountId = Acc.Id;
		insert Con;
		
		Com.Name = 'Test';
		Com.StartDate = date.today();	
		Insert Com;
		
		CoMem.ContactId = Con.Id;
		CoMem.CampaignId = Com.id;
		Insert CoMem;
		
		Opp.AccountId = Acc.Id;
		Opp.Primary_Contact__c = Con.Id;
		Opp.Name = 'Test';
		Opp.CloseDate = date.today();
		Opp.StageName = 'Prospect/Budget Requested';
		Insert Opp;
		
		ConRole.ContactId = Con.Id;
		ConRole.OpportunityId = Opp.id;
		ConRole.IsPrimary = true;
		Insert ConRole;
    	Test.stopTest();		
       
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(lead.id);
		
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setAccountId(Acc.Id);
		lc.setConvertedStatus('Opportunity Created');
		
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
		
		List<Opportunity> Opps = new List<Opportunity>();		
		Opps = [Select Primary_Contact__c From Opportunity Where id=:Opp.id];
		
		System.assert(Opps[0].Primary_Contact__c == Con.Id);
		
		ApexPages.currentPage().getParameters().put('Opp',Opp.Id);  
		ApexPages.StandardController sc = new ApexPages.StandardController(Opp);
		OppPrimaryContactHelper prom = new OppPrimaryContactHelper(sc);
		prom.rollupOppContacts();

    }
}