/*
 *	AccountTrigger_Test
 *	
 *	Test class for AccountTrigger and AccountTriggerHandler.
 *
 *	If there are other test classes related to AccountTrigger, please document it here (as comments).
 * 
 * 	Created by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
private class AccountTrigger_Test {

	// IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] AccountTrigger_Test.test() [Begin]');
        //RecordType aRT = [Select Id, Name from RecordType where Name = 'NA PSM ABS' and sObjectType='Account' and isActive=true limit 1];
        //Account b = new Account(Name='Test Division',RecordTypeId=aRT.Id, BillingCountry='Canada');
    	Account iudu = new Account(Name='YongTest', BillingCountry = 'Canada');
		insert iudu;
		update iudu;
		delete iudu;
		undelete iudu;
        system.debug('[TEST] AccountTrigger_Test.test() [End]');
     }

	// **************************************************************************
	// 	AccountTriggerHandler.accountRollUp() testing
	// **************************************************************************
	//	Related test classes:
	//		- OpportunityRollupTriggerTests
	//	Related classes:
	//		- OpportunityRollupTriggerHandler
	//		- AccountRevenueBatchable
	// **************************************************************************


	// **************************************************************************
	// 	OpportunityMarketVerticalTriggerHandler.onAccountUpdate() testing
	// **************************************************************************
	//	Related test classes:
	//		- OpportunityMarketVerticalTriggerHandler
	// **************************************************************************

}