public virtual class BaseMySFDCController {
	public User runAs {get;set;}
	public String runAsId {get;set;}
	public User currentUser {get;set;}
	public List<Conversion_Rate__c> conversionRates {get;set;}
	public List<List<GoogleGaugeChartData>> rowsOfCharts {get;set;} //current Quarter Forecast guage

	protected MySFDCQuota quotas;
	protected Map<String,Decimal> conversionRatesMap;

	public String reportTitle {get;set;}
	public List<Opportunity> reportOpps {get;set;}
	public Pager reportPager {get;set;}

	public BaseMySFDCController() {
		
	}

	public PageReference init(){

		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			runAsId = ApexPages.currentPage().getParameters().get('u');
		}else{
			runAsId = UserInfo.getUserId();
		}
		runAs = MySFDCUtil.findUser(runAsId);
		currentUser = MySFDCUtil.findUser(UserInfo.getUserId());

		//load the conversion rates
		conversionRatesMap = MySFDCUtil.getConversionRates(runAs);
		conversionRates = [select Region__c, Conversion_Rate__c, Opportunity_Type__c from Conversion_Rate__c where Region__c = :runAs.License_Region__c order by Opportunity_Type__c asc ];

		//load quotas for the quarter
		Set<Id> userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		quotas = MySFDCUtil.getQuotas(runAsId);
		Decimal currentQuarterQuota = quotas.currentQuarterQuota;
		Decimal nextQuarterQuota = quotas.nextQuarterQuota;

		//TODO
		loadDashboards();

		prepareTables();
		
		return null;
	}

	protected virtual void loadDashboards(){
		//TODO
	}

	protected virtual void prepareTables(){
		//TODO
	}

	protected virtual IMySFDCDashboard findReportDashboard(String dbKey) {
		//TODO
		return null;
	}

	public PageReference initReport(){
		String report = ApexPages.currentPage().getParameters().get('r');
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			runAsId = ApexPages.currentPage().getParameters().get('u');
		}else{
			runAsId = UserInfo.getUserId();
		}
		runAs = MySFDCUtil.findUser(runAsId);

		//load quotas for the quarter
		Set<Id> userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		quotas = MySFDCUtil.getQuotas(runAs.Id);
		conversionRatesMap = MySFDCUtil.getConversionRates(runAs);
		//IMySFDCDashboard db = ABSciexDashboardFactory.getDashboard (
		//	report, runAs, quotas, conversionRatesMap
		//);
		IMySFDCDashboard db = findReportDashboard(report);

		reportTitle = db.getReportTitle();
		reportPager = (Pager)db;

		if(ApexPages.currentPage().getParameters().containsKey('s1')) {
			reportPager.setSortColumn1(ApexPages.currentPage().getParameters().get('s1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd1')) {
			reportPager.setSortDirection1(ApexPages.currentPage().getParameters().get('sd1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('s2')) {
			reportPager.setSortColumn2(ApexPages.currentPage().getParameters().get('s2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd2')) {
			reportPager.setSortDirection2(ApexPages.currentPage().getParameters().get('sd2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('p')) {
			reportPager.setPageNumber(Integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
		}
		reportPager.fetchRecords();
		return null;
	}

	public PageReference initReportExport(){
		String report = ApexPages.currentPage().getParameters().get('r');
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			runAsId = ApexPages.currentPage().getParameters().get('u');
		}else{
			runAsId = UserInfo.getUserId();
		}
		runAs = MySFDCUtil.findUser(runAsId);

		//load quotas for the quarter
		Set<Id> userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		MySFDCQuota quotas = MySFDCUtil.getQuotas(runAs.Id);
		Map<String,Decimal> conversionRatesMap = MySFDCUtil.getConversionRates(runAs);
		IMySFDCDashboard db = findReportDashboard(report);

		reportTitle = db.getReportTitle();
		reportPager = (Pager)db;

		reportPager.setPageSize(10000);

		if(ApexPages.currentPage().getParameters().containsKey('s1')) {
			reportPager.setSortColumn1(ApexPages.currentPage().getParameters().get('s1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd1')) {
			reportPager.setSortDirection1(ApexPages.currentPage().getParameters().get('sd1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('s2')) {
			reportPager.setSortColumn2(ApexPages.currentPage().getParameters().get('s2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd2')) {
			reportPager.setSortDirection2(ApexPages.currentPage().getParameters().get('sd2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('p')) {
			reportPager.setPageNumber(Integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
		}
		reportPager.fetchRecords();
		return null;
	}

	public PageReference saveInlineEdits(){
		try {
			update this.reportPager.getReportOpps();
			String url = '/apex/MySFDCReport';
			List<String> params = new List<String>();
			for(String p : ApexPages.currentPage().getParameters().keySet()){
				params.add(p + '=' + EncodingUtil.urlEncode(ApexPages.currentPage().getParameters().get(p), 'UTF-8'));
			}
			if(params.size() > 0) url += '?' + String.join(params,'&');
			return new PageReference(url).setRedirect(true);
		}catch(DmlException e) {
			MySFDCUtil.addDMLExceptionMessagesToPage(e);
			return null;
		}
	}

	public PageReference cancelInlineEdits() {
		String url = '/apex/MySFDC';
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			url += '?u=' + ApexPages.currentPage().getParameters().get('u');
		}
		return new PageReference(url).setRedirect(true);
	}

	public List<SelectOption> runAsList {
		get {
			List<SelectOption> options = new List<SelectOption>();
			List<User> users = MySFDCUtil.findUsersInRoleHierarchy(currentUser.UserRoleId);
			for(User u : users) {
				options.add(new SelectOption(u.Id, u.Name));
			}
			return options;
		}
	}
}