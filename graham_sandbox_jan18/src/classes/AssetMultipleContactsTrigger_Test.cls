/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 15/2017
**
** Test coverage for AssetMultipleContacts
**/
@isTest
public class AssetMultipleContactsTrigger_Test {
	@isTest
	public static void test() 
	{
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;
        
        Asset ast = new Asset(Name = 'Test Asset', AccountId = acct.Id);
        insert ast;

		Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
		insert new Contact[]{c1};

        Asset_MultipleContacts__c amc = new Asset_MultipleContacts__c(Contact__c = c1.Id, Asset__c = ast.Id);
        insert amc;
		update amc;
		delete amc;
		undelete amc;			
	}

	@isTest
	public static void test_AddLink() 
	{
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;
        
		Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1', LMS_Content_Entitled__c = false);
		insert new Contact[]{c1};

		system.debug('***** TEST: before installed product DML insert');
		SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(
            SVMXC__Company__c = acct.Id,
            Current_Coverage_Start_Date__c = Date.today().addDays(-1),
            Current_Coverage_End_Date__c = Date.today().addYears(1)
        );
        insert ip;  //triggers create asset
		system.debug('***** TEST: after installed product DML insert');
        
        Asset ast = [select Id from Asset where Installed_Product__c = :ip.Id];
		//lines added by graham to mimic hardward entitlement as these
		//are no longer based on Coverage Start/End Date Jan 2018
		ast.NEWSCIEXNow_Hardware_Entitled__c = true;
		update ast;
        //end modifications
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(false, c1.LMS_Content_Entitled__c);
        
        c1.Content_Eligible_Approved__c = true;
        update c1;
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);
        c1.Content_Eligible_Approved__c = false;
        update c1;
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(false, c1.LMS_Content_Entitled__c);

        Asset_MultipleContacts__c amc = new Asset_MultipleContacts__c(Contact__c = c1.Id, Asset__c = ast.Id);
        insert amc;

        //contact will have content entitled = true via process builder process "Contact Entitled"
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);
	}

	@isTest
	public static void test_UnsetContactFlag() 
	{
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;
        
		Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1', LMS_Content_Entitled__c = false, Content_Eligible_Approved__c = true);
		insert new Contact[]{c1};

        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);

        system.debug('***** TEST: before installed product DML insert');
		SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(
            SVMXC__Company__c = acct.Id,
            Current_Coverage_Start_Date__c = Date.today().addDays(-1),
            Current_Coverage_End_Date__c = Date.today().addYears(1)
        );
        insert ip;  //triggers create asset
		system.debug('***** TEST: after installed product DML insert');
        
        Asset ast = [select Id from Asset where Installed_Product__c = :ip.Id];
		//lines added by graham to mimic hardward entitlement as these
		//are no longer based on Coverage Start/End Date Jan 2018
		ast.NEWSCIEXNow_Hardware_Entitled__c = true;
		update ast;
        //end modifications

        Asset_MultipleContacts__c amc = new Asset_MultipleContacts__c(Contact__c = c1.Id, Asset__c = ast.Id);
        insert amc;

        //contact will have content entitled = true via process builder process "Contact Entitled"
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);
        
        c1.Content_Eligible_Approved__c = false;
        update c1;

        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);
    }
}