/*
 *  LeadTriggerHandler
 *  
 *      Trigger Handler class that implements Trigger Framework.
 *          https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *          
 *      According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *      The Trigger Handler class should extend TriggerHandler class.
 * 
 *  Created by Yong Chen on 2016-03-08
 *
 *  [Modification history]
 *  [Name] [Date] Description
 *
 */
public with sharing class LeadTriggerHandler extends TriggerHandler {

    static Boolean firstRun = TRUE;    

    public LeadTriggerHandler() {
        //this.setMaxLoopCount(1);
    }
    
    // **************************************************************************
    //      context overrides 
    // **************************************************************************
    override protected void afterUpdate() {
        // Outside of the Trigger itself, statics like Trigger.new and Trigger.newMap always contain raw sObjects or Maps of sObjects respectively.
        leadDupConvert();   
//        LeadStrategyTriggerHandler.onAfterUpdate(Trigger.new,(Map<Id, Lead>) Trigger.oldMap);        
//        LeadRoutingHelper.onAfterLead();
        leadAssignment();
    }
    
    override protected void afterInsert(){
//        LeadRoutingHelper.onAfterLead();
    }
    
    override protected void afterDelete(){        
//        LeadRoutingHelper.onAfterLead();
    }
        
    override protected void beforeInsert(){        
//        LeadRoutingHelper.onBeforeLead();
        LeadSetCountryMap();
    }    
    override protected void beforeUpdate(){    
     
//        LeadStrategyTriggerHandler.onBeforeUpdate(Trigger.new,(Map<Id, Lead>) Trigger.oldMap);
//        LeadRoutingHelper.onBeforeLead();
        LeadSetCountryMap();
    }

   
private void LeadSetCountryMap()
    {
        List<Lead> leadNeedMapping = new List<Lead>();

for( Lead newLead : (List<Lead>)Trigger.new){
   Lead oldLead = NULL;

   //If update, grab old record for comparison
   if(Trigger.IsUpdate) 
   { 
      oldLead = ((Map<Id, Lead>)Trigger.oldMap).get(newLead.ID);  
   }
   
   //Add to list only if NEW or Country/State field changes
   if(Trigger.IsInsert || oldLead.Country != newLead.Country ||  oldLead.State != newLead.State || newLead.Country_Mapping__c == NULL)       
   {    
     leadNeedMapping.add(newLead);
   }
}  
    
if(leadNeedMapping.size()>0)
    {
        List<Country_Mapping__c> allCountries = [Select Name, id,Permutations__c, Country_Code__c, Forecast_Territory__c, 
                                                    Forecast_Region__c 
                                                 from Country_Mapping__c];        
        List<State_Mapping__c> allStates = [Select Name, id, State_Province_Permutations__c, Country_Mapping__c 
                                            from State_Mapping__c];         
        Map<String,Country_Mapping__c> countryAlias = new Map<String,Country_Mapping__c>();  
        Map<String,State_Mapping__c> stateAlias = new Map<String,State_Mapping__c>(); 
        
        for(Country_Mapping__c country :allCountries){
            // add country code, we will be treating it the same way as permutations
          countryAlias.put(country.Country_Code__c.toUpperCase(),country);

            // split all permutations
          if(!String.isBlank(country.Permutations__c)){
              if(country.Permutations__c.contains(';')){
                for(String alias : country.Permutations__c.split(';')) {
            if(!String.isBlank(alias.trim())) 
                            countryAlias.put(alias.trim().toUpperCase(),country);
                    }
              } else {
                      countryAlias.put(country.Permutations__c.trim().toUpperCase(),country);
                }
        }
            
      }
        for(State_Mapping__c state :allStates){
                    
                    if(!String.isBlank(state.State_Province_Permutations__c)){
                  
                        if(state.State_Province_Permutations__c.contains(';') ){
                    
                            for(String alias : state.State_Province_Permutations__c.split(';')) {
                                if(!String.isBlank(alias.trim())  )
                                    stateAlias.put(alias.trim().toUpperCase(),state);
                            }
                                } else {
                                    stateAlias.put(state.State_Province_Permutations__c.trim().toUpperCase(),state);
                                }
                            }  
                        }        
    
for(Lead leadrecord : leadNeedMapping){
        
    ID countryID;
    String countryName;
    ID stateID;
    String stateName; 

    if(leadrecord.Country != null){  
                
        IF(countryAlias.containsKey(leadrecord.Country.trim().toUpperCase()))
        {
            countryID = countryAlias.get(leadrecord.Country.trim().toUpperCase()).id;
        }       
                //Country MAP FOUND --> Populate the Country and Country Mapping fields
                if(countryID != NULL)
                {   
                    leadrecord.Country_Mapping__c = countryID;
                    countryName = countryAlias.get(leadrecord.Country.trim().toUpperCase()).name;
                
                    if(String.isNotBlank(countryName))
                    {
                        leadrecord.Country = countryName;
                        leadrecord.Region__c = countryAlias.get(leadrecord.Country.trim().toUpperCase()).Forecast_Territory__c;
                        leadrecord.Forecast_Region__c = countryAlias.get(leadrecord.Country.trim().toUpperCase()).Forecast_Region__c;
                    }
                } 
                
                // Country MAP NOT FOUND --> NULL out the Country and Country Mapping fields
                if(countryID == NULL)
                { 
                    leadrecord.Country_Mapping__c = NULL;
                    //leadrecord.Country = NULL;
                    leadrecord.Region__c = NULL;
                    leadrecord.Forecast_Region__c = NULL;
                }
                
            }

    if(countryID != null && leadrecord.State != null)   {  
    
        IF(stateAlias.containsKey(leadrecord.State.trim().toUpperCase()))
        {
            stateID = stateAlias.get(leadrecord.State.trim().toUpperCase()).id;
        }         
        
        if(stateID != NULL && countryID == stateAlias.get(leadrecord.State.trim().toUpperCase()).Country_Mapping__c)
            {   
                leadrecord.State_Mapping__c = stateID;
                stateName = stateAlias.get(leadrecord.State.trim().toUpperCase()).name;
                  
                if(String.isNotBlank(stateName))
                {
                    leadrecord.State = stateName;
                }
            }
    }     

    // Country BLANK --> NULL out the Country Mapping fields
    if(leadrecord.Country == null){  
        leadrecord.Country_Mapping__c = NULL;
        leadrecord.Region__c = NULL;
        leadrecord.Forecast_Region__c = NULL;        
    }
            
    // State BLANK --> NULL out the State Mapping fields
    if(leadrecord.State == null  || String.isBlank(stateName)){  
        leadrecord.State_Mapping__c = NULL;
    }
    } 
  }
    }//END OF LeadSetCountryMap
    
private void leadAssignment()
    {
/****
 * Created by = Daniel (with help from others as sanity was lost)
 * Purpose of 'leadAssignment' = to assign leads to users/queues based on the State/Country combination + Intent scores
 * 
 * High Level 
 *  1. A lead comes into Salesforce, before update/insert Country and State (if set) are matched to Country/State Mapping record
 *  2. Lead is saved and filters through the Lead Assignment Rules, does not match any Campaign details, so falls to the catch all
 *      lead assignment rule that assigns the lead to AssignTriggerQueue.  
 *  3. Lead is saved, Intent to Purchase values (x4) are calculated, and the primary and secondary scores are set (all via WORKFLOW)
 *  4. Workflow sets winning and secondary score, and sets an Assigned Flag (field) as FALSE
 *  5. This method (leadAssignment) recognizes the criteria (Assigned Flag = False AND Owner = AssignTriggerQueue) --> fire trigger method
 *  6. leadAssignment 
 *      --> Calculation is done on results of which business unit should be contacted for both primary and secondary scores
 *      --> Based on results for each, lookup is done to the State (first), then Country (if state is blank), to determine the user (Owner) 
 *      or Queue (Queue) to assign the Lead too.
 *      --> Logic based on User vs Queue is fired, winner becomes the PrimaryOwner or SecondaryOwner
 * 
 * NOTE: Secondary Owner Name and ID are set rather than a lookup as you cannot currently have a lookup to Queue+User at the same time.
 * This ID setting via text allows for re-assignment later on when dual intent pass off is implemented.
 ****/
        
    List<Lead> LeadsToAssign = new list<Lead>();
    List<Lead> LeadsToUpdate = new list<Lead>();
    
    Group CatchAllLeadOwner = [Select ID from Group where name = 'AssignCatchAllQueue' Limit 1];

    Group DefaultLeadOwner = [Select ID from Group where name = 'AssignTriggerQueue' Limit 1];
    //AssignTriggerQueue is the CATCH ALL queue in the Lead Assignment Rules; should be the LAST rule

    LeadsToUpdate = [SELECT Id, State_Mapping__c, OwnerID, Country_Mapping__c, Primary_Score__c, Secondary_Score__c, 
                                        Lead_Assigned__c FROM Lead 
                                        WHERE Lead_Assigned__c = FALSE
                                        AND OwnerID = : DefaultLeadOwner.Id
                                        AND ID in : Trigger.New];  

if(LeadsToUpdate.size() > 0)  
{
        
    // Map of all Lead Management Results, considered Scores
    List<Lead_Management__c> LeadMgmt = [SELECT Name, Result__c, Dual_Intent__c FROM Lead_Management__c];
    Map<String, String> Scores= new Map<String,String>(); 
    For(Lead_Management__c lm :LeadMgmt){
         Scores.put(lm.name,lm.result__c);
    }
    
    //Map of all Lead Management Dual Intent Scores
    List<Lead_Management__c> LeadDualMgmt = [SELECT Name, Dual_Intent__c FROM Lead_Management__c];
    Map<String, Boolean> DualScores= new Map<String,Boolean>(); 
    For(Lead_Management__c lmd :LeadDualMgmt){
         DualScores.put(lmd.name,lmd.Dual_Intent__c);
    }    

    //Map of all Queues
    List<Group> Groups = [Select Name, ID from Group where Type = 'Queue'];
    Map<String, ID> Queues = new Map<String,ID>();
    For(Group GRP :Groups){
        Queues.put(GRP.Name, GRP.ID);
    }

    //
    
    // Map of all Countries and their Assignee values
    // It should be assumed that ALL QUEUE fields are NOT NULL
    Map<ID, Country_Mapping__c> Countries= new Map<ID,Country_Mapping__c>([SELECT CE_Owner__c,CE_Queue__c,Id,
            Marketing_Queue__c,Marketing_Owner__c,MS_Owner__c,MS_Queue__c,Nano_Owner__c,Nano_Queue__c,
            Service_Owner__c,Service_Queue__c,Telequalifications_Owner__c,Telequalifications_Queue__c 
            FROM Country_Mapping__c ]);  
    
    // Map of all States and their Assignee values
    // It should be assumed that ALL QUEUE fields are NOT NULL
    Map<ID, State_Mapping__c> States= new Map<ID,State_Mapping__c>([SELECT CE_Owner__c,CE_Queue__c,Id,
            Marketing_Queue__c,Marketing_Owner__c,MS_Owner__c,MS_Queue__c,Nano_Owner__c,Nano_Queue__c,
            Service_Owner__c,Service_Queue__c,Telequalifications_Owner__c,Telequalifications_Queue__c 
            FROM State_Mapping__c ]);    
    //Map of all Queues
    Map<ID, User> activeUsers = new Map<ID, User>([Select ID, isActive, Name, Email from User where isActive = True]);    

    for(Lead lea: LeadsToUpdate)
    {  
    String PrimaryOwner = CatchAllLeadOwner.Id; //added a catch all queue so leads can still be created        
    String SecondaryOwner;
    String SecondaryName;
    String SecondaryEmail;
      
            if (lea.State_Mapping__c != NULL) { // STATE MAPPING ASSIGNMENT
                State_Mapping__c StateMap =States.get(lea.State_Mapping__c);
                
                String PrimaryOwnerID; //PRIMARY ASSIGNMENT

/**********
 * --> PrimaryOwnerID = ((String)Statemap.get(Scores.get(lea.Primary_Score__c) + '_Owner__c'));
 * This line is replicated multiple times throughout.  Here is a brief description of what it is doing ('cuz its a b!tch)
 * lea.Primary_Score__c is a combination set by workflow on the Lead record; it's the top Intent Score (EX: AMMS16)
 * Scores.get(lea.Primary_Score__c) gets the RESULT of this top score based on the Lead Management object records (EX: MS)
 * (String)Statemap.get(Scores.get(lea.Primary_Score__c) + '_Owner__c') does 2 things
 *      1. concatenates the field on the StateMap record that it will search in (EX: MS_Owner__c)
 *      2. grabs the value in that concatenated field (EX: SFID of User in this field)
 * 
 * This means IF new intents are created, create those fields as (NewIntentName)_Owner__c and (NewIntentName)_Queue__c
 *      Then populate Lead Management object to have results based on Intent values = (NewIntentName)
 *      Code will take care of the rest
 * 
 * Pretty cool huh? F*CK ya
 **********/                 
                
                PrimaryOwnerID = ((String)Statemap.get(Scores.get(lea.Primary_Score__c) + '_Owner__c'));
                
                IF(PrimaryOwnerID != NULL)
                {       
                   IF(activeUsers.get(PrimaryOwnerID) != NULL)
                       //Lets make sure the user selected is still active, can't assign a lead to an inactive user
                            {    
                                PrimaryOwner = activeUsers.get(PrimaryOwnerID).id;
                            }                   
                    }
                
                IF(PrimaryOwnerID == NULL || primaryOwner == CatchAllLeadOwner.Id )
                {        
                   //User inactive? User field blank? No problem.  Assign lead to the queue.
                   String PrimaryQueueName;
                   PrimaryQueueName = ((String)Statemap.get(Scores.get(lea.Primary_Score__c) + '_Queue__c')); 
                   IF(Queues.get(PrimaryQueueName) != NULL)
                            {
                                primaryOwner = (Queues.get(PrimaryQueueName));
                            }
                    }        

                            
                
                String SecondaryOwnerID; //SECONDARY ASSIGNMENT
                SecondaryOwnerID = ((String)Statemap.get(Scores.get(lea.Secondary_Score__c) + '_Owner__c'));
                IF(SecondaryOwnerID != NULL)
                {       
                   IF(activeUsers.get(SecondaryOwnerID) != NULL && activeUsers.get(SecondaryOwnerID).id != primaryOwner)
                            {  
                                secondaryOwner = activeUsers.get(SecondaryOwnerID).id; //ID
                                secondaryName = activeUsers.get(SecondaryOwnerID).name;  //Name 
                                SecondaryEmail = activeUsers.get(SecondaryOwnerID).email; //Email
                            }                   
                    }
                If(secondaryOwner == NULL)
                {
                   String SecondaryQueueName;
                   SecondaryQueueName = ((String)Statemap.get(Scores.get(lea.Secondary_Score__c) + '_Queue__c')); 
                    
                   IF(Queues.get(SecondaryQueueName) != NULL && Queues.get(SecondaryQueueName) != primaryOwner)
                            {
                                secondaryOwner = (Queues.get(SecondaryQueueName)); //ID
                                secondaryName = (SecondaryQueueName); //Name
                                secondaryEmail = NULL;
                            }
                    }    
                } // END STATE LEAD ASSIGNMENT 
            
            if(lea.State_Mapping__c == NULL && lea.Country_Mapping__c != NULL) { // COUNTRY MAPPING ASSIGNMENT
                
                Country_Mapping__c CountryMap =Countries.get(lea.Country_Mapping__c);
                
                String PrimaryOwnerID; //PRIMARY ASSIGNMENT
                PrimaryOwnerID = ((String)CountryMap.get(Scores.get(lea.Primary_Score__c) + '_Owner__c'));
                    
                IF(PrimaryOwnerID != NULL)
                {       
               IF(activeUsers.get(PrimaryOwnerID) != NULL)
                        {    
                            PrimaryOwner = activeUsers.get(PrimaryOwnerID).id;
                        }                   
                }
                
                IF(PrimaryOwnerID == NULL || primaryOwner == NULL )
                {             
               String PrimaryQueueName;
               PrimaryQueueName = ((String)CountryMap.get(Scores.get(lea.Primary_Score__c) + '_Queue__c')); 
               IF(Queues.get(PrimaryQueueName) != NULL)
                        {
                            primaryOwner = (Queues.get(PrimaryQueueName));
                        }
                }       
                       
         
                String SecondaryOwnerID; //SECONDARY ASSIGNMENT
                SecondaryOwnerID = ((String)CountryMap.get(Scores.get(lea.Secondary_Score__c) + '_Owner__c'));
                
                IF(SecondaryOwnerID != NULL)
                {     
system.debug('SecondaryOwnerID = ' + SecondaryOwnerID);                                    
//                IF(activeUsers.get(SecondaryOwnerID) != NULL)
                IF(activeUsers.get(SecondaryOwnerID) != NULL  && activeUsers.get(SecondaryOwnerID).id != primaryOwnerID)                    
                        {  
                            secondaryOwner = activeUsers.get(SecondaryOwnerID).id; //ID
                            secondaryName = activeUsers.get(SecondaryOwnerID).name;  //Name 
                            SecondaryEmail = activeUsers.get(SecondaryOwnerID).email; //Email
system.debug('SecondaryOwnerID User= ' + SecondaryOwnerID);                            
system.debug('SecondaryOwner Name User= ' + activeUsers.get(SecondaryOwnerID).name);                              
                        }                   
                }

                If(secondaryOwnerID == NULL)
                {
               String SecondaryQueueName;
               SecondaryQueueName = ((String)CountryMap.get(Scores.get(lea.Secondary_Score__c) + '_Queue__c')); 
                    
               IF(Queues.get(SecondaryQueueName) != NULL && Queues.get(SecondaryQueueName) != primaryOwner)
                        {
                            secondaryOwner = (Queues.get(SecondaryQueueName)); //ID
                            secondaryName = (SecondaryQueueName); //Name
                            secondaryEmail = NULL;
                        }
                }    
            
                } // END COUTNRY Lead ASSIGNMENT             
        
            lea.id = lea.id;
            lea.ownerID = primaryOwner;
            lea.Lead_Secondary_Owner_ID__c = SecondaryOwner;
            lea.Lead_Secondary_Owner__c = SecondaryName;
            lea.Lead_Secondary_Email__c = secondaryEmail;
            lea.Status = 'Open';
            lea.primary_routing__c = Scores.get(lea.Primary_Score__c);
            lea.secondary_routing__c = Scores.get(lea.Secondary_Score__c);
            lea.dual_intent__c = DualScores.get(lea.Secondary_Score__c);
            lea.Lead_Assigned__c = TRUE;
            
            LeadsToAssign.add(lea);
        }            
    }
 

system.debug('Size of LeadsToAssign before UPDATE = ' + leadstoassign.size());
    if(LeadsToAssign.size()>0)
    {
         update LeadsToAssign;  
    }        
}//END OF leadAssignment


private void leadDupConvert()
    {
        List<Lead> LeadsToDup = new list<Lead>(); 
        List<Lead> duplicatedLead = new list <Lead>(); 
            
        for( Lead myLead : (List<Lead>)Trigger.new)
        {
        Lead oldLeadDetails = ((Map<Id, Lead>)Trigger.oldMap).get(myLead.ID);  
           
        if(myLead.Dual_Intent__c == TRUE && myLead.IsConverted && myLead.Lead_Secondary_Owner_ID__c != NULL 
           && !oldLeadDetails.isConverted && (firstRun || Test.isRunningTest()) ) // removed (&& firstRun) from criteria and test class works
            {                 
                Lead newLead = myLead.clone(false, false, false, false);
                newLead.PrimeLeadID__c = myLead.id;
                newLead.Dual_Intent__c = FALSE; 
                newLead.Lead_Assigned__c = TRUE;
                newLead.Status = 'Open';
                newLead.OwnerId = myLead.Lead_Secondary_Owner_ID__c;
                newLead.Primary_Routing__c = myLead.Secondary_Routing__c;
                newLead.Secondary_Routing__c = NULL;
                newLead.Lead_Type__c = 'Existing Contact';
                newLead.ConvertedDate = NULL;
                newLead.ConvertedOpportunityId = NULL;
                newLead.ConvertedAccountId = NULL;
                newLead.IsConverted = FALSE;
                newLead.ConvertedContactId = NULL;
                newLead.Last_Fired_Workflow__c = 'leadDupConvert - Duplicated Dual Intent Lead'; 
                newLead.Lead_Secondary_Email__c = NULL;
                newLead.Lead_Secondary_Owner__c = NULL;
                newLead.Lead_Secondary_Owner_ID__c = NULL;
            duplicatedLead.add(newLead);
            }
        
        }
        
        if(duplicatedLead.size()>0)
        {
            insert duplicatedLead;         
        }        
        firstRun = FALSE;
    }
}