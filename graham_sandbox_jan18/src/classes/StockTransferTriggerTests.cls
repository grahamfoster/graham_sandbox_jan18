/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 27/2015
**
** Test coverage for StockTransferTrigger and StockTransferTriggerHandler
** 
**/
@isTest
public class StockTransferTriggerTests 
{
	public static testMethod void test1()
    {
        SVMXC__Site__c loc1 = new SVMXC__Site__c (SVMXC__Stocking_Location__c = true);
        insert loc1;
        
		SVMXC__Stock_Transfer__c stockTransfer = new SVMXC__Stock_Transfer__c(SVC_Stock_Transfer__c='Open', SVMXC__Source_Location__c = loc1.Id);
        insert stockTransfer;
        
        SVMXC__Stock_Transfer_Line__c stockTransferLine = new SVMXC__Stock_Transfer_Line__c(SVMXC__Stock_Transfer__c = stockTransfer.Id, SVC_Status__c = 'Completed');
        insert stockTransferLine;
        
        stockTransfer.SVC_Stock_Transfer__c = 'Submitted';
        update stockTransfer;
        
        stockTransferLine = [select SVC_Status__c from SVMXC__Stock_Transfer_Line__c where Id = :stockTransferLine.Id];
        system.assertEquals('Submitted', stockTransferLine.SVC_Status__c);
    }

}