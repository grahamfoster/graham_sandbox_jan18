/**
 * @author Brett Moore
 * @created - Sept 2016
 * @Revision  
 * @Last Revision 
 * 
 * Schedulable APEX Class that launches the Create Module. Used to set an auto-fire schedule for the C2O application. 
 * 
**/
Global class C2O_Schedule_Create implements Schedulable{

    global void execute(SchedulableContext sc) {
        String preQuery = 'SELECT id, name, c2o_First_Product_Expiration__c, SVC_Oracle_Status__c, CurrencyISOCode,SVMXC__COMPANY__C,SVMXC__COMPANY__r.name,SVMXC__Company__r.CurrencyIsoCode, SVMXC__COMPANY__r.Default_Service_Pricebook__c, SVMXC__CONTACT__C, SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__c,  SVC_CATEGORY__C,    Renewal_Opportunity_Created__c FROM SVMXC__Service_Contract__c WHERE  ';
        Contract2Opportunity_Configuration__c config = ([SELECT id, Create_Query_Filter__c, scope__c FROM Contract2Opportunity_Configuration__c Limit 1]);
        String Filter = config.Create_Query_Filter__c;
		Integer scope = config.scope__c.intValue();
        String Query = preQuery + Filter;
        String process = 'Create';
        
        Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
		id batchInstanceId = database.executebatch(b,scope);
	}

}