/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Apr 28/2014
 **
 ** batch job to copy marketing information from Opportunity to Contact, from newly associated Contacts (OpportunityContactRole records created recently)
 ** 
**/
global class OpportunityContactRoleBatchable implements Database.Batchable<sObject>, Database.Stateful {
	global DateTime lastRun = null;
	global Map<Id,Contact> contactUpdates = new Map<Id,Contact>();
	
	global OpportunityContactRoleBatchable() {
		//determine when the job was last run
		lastRun = GlobalSettings__c.getOrgDefaults().OpportunityContactRoleBatchableLastRun__c;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String soql = 'select Id, ContactId, Opportunity.Market_Vertical__c, Opportunity.Market_Segment__c, Opportunity.Primary_Application__c '+
			'from OpportunityContactRole';
		if(lastRun != null) soql += ' where CreatedDate >= :lastRun';
		soql += ' limit 10000';
		return Database.getQueryLocator(soql);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		//TODO
		system.debug('scope:'+scope.size());
		for(sObject sobj : scope) {
			OpportunityContactRole ocr = (OpportunityContactRole)sobj;
			Contact contactUpdate = new Contact(Id = ocr.ContactId, Market_Segment_St__c = ocr.Opportunity.Market_Vertical__c, 
				Market_Segment__c = ocr.Opportunity.Market_Segment__c, Primary_Application__c = ocr.Opportunity.Primary_Application__c);
			contactUpdates.put(ocr.ContactId, contactUpdate);
		}
	}

	global void finish(Database.BatchableContext BC) {
		if(contactUpdates.size() > 0) update contactUpdates.values();
		GlobalSettings__c gs = GlobalSettings__c.getOrgDefaults();
		gs.OpportunityContactRoleBatchableLastRun__c = DateTime.now();
		update gs;
		system.debug('set lastRun='+lastRun);
	}
}