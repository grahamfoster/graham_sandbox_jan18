// ===========================================================================
// Object: AbsorbUtil
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Utility methods for interacting with the Absorb LMS API
// ===========================================================================
// Changes: 2016-01-30 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class AbsorbUtil 
{
	//id: Filters the list by the course id.
	//categoryId: Filters the list by the course's category id.
	//fromDt: Filters the list by the course's start date.
	//toDt: Filters the list by the course's expiry date.
	//see https://myabsorb.com/api/rest/v1/Help/Api/GET-api-Rest-v1-courses-forsale_id_categoryId_from_to
	public static String getCoursesURL(String id, String categoryId, Date fromDt, Date toDt)
	{
		//String url = getAPIBaseURL() + '/courses/forsale';
		String url = getAPIBaseURL() + '/Courses';
		/*List<String> urlParams = new List<String>();
		if(!String.isBlank(id)) urlParams.add('id='+id);
		if(!String.isBlank(categoryId)) urlParams.add('categoryId='+categoryId);
		if(fromDt != null) urlParams.add('from='+fromDt);
		if(toDt != null) urlParams.add('to='+toDt);
		if(!urlParams.isEmpty())
		{
			url += getAPIBaseURL().contains('requestb.in') ? '&' : '?';
			url += String.join(urlParams, '&');
		}
		*/
		return url;
	}

	//see https://myabsorb.com/api/rest/v1/Help/Api/GET-api-Rest-v1-users-userId-courses
	public static String getCoursesAvailableURL(String userId)
	{
		//return getAPIBaseURL() + '/users/'+userId+'/catalog';
		return getAPIBaseURL() + '/users/'+userId+'/courses';
	}

	public static String getAPIAuthenticateURL()
	{
		return getAPIBaseURL() + '/Authenticate?username=' + getAPIUsername() + '&password=' + getAPIPassword() + '&privateKey=' + getAPIPrivateKey();
		//return getAPIBaseURL() + '/Authenticate';
	}

	public static String getAuthenticateBody()
	{
		/*Map<String,String> authenticateMap = new Map<String, String> {
			'Username' => getAPIUsername(),
			'Password' => getAPIPassword(),
			'PrivateKey' => getAPIPrivateKey()
		};
		return JSON.serializePretty(authenticateMap);*/
		return '';
	}

	public static String getAPICreateUserURL()
	{
		return getAPIBaseURL() + '/users';
	}

	public static String getAPIUpdateUserURL(String id)
	{
		return getAPIBaseURL() + '/Users/'+id;
	}

	public static String getAPIEnrollmentsByUserURL(String userId, Integer status, Date modifiedSince)
	{
		if(String.isBlank(userId)) return null;
		String url = getAPIBaseURL() + '/users/'+userId+'/enrollments';
		String[] urlParams = new String[]{};
		if(status != null) urlParams.add('status='+status);
		if(modifiedSince != null) urlParams.add('modifiedSince='+modifiedSince);
		if(urlParams.size() > 0) url += '?' + String.join(urlParams, '&');
		return url;
	}

	public static String getAPIEnrollmentsByCourseURL(String courseId, Integer status, Date modifiedSince)
	{
		if(String.isBlank(courseId)) return null;
		String url = getAPIBaseURL() + '/courses/'+courseId+'/enrollments';
		String[] urlParams = new String[]{};
		if(status != null) urlParams.add('status='+status);
		if(modifiedSince != null) urlParams.add('modifiedSince='+formatDate(modifiedSince));
		if(urlParams.size() > 0) url += '?' + String.join(urlParams, '&');
		return url;
	}

	public static String getAPIEnrollmentsByCourseAndSessionURL(String courseId, String sessionId, Integer status, Date modifiedSince)
	{
		if(String.isBlank(courseId)) return null;
		String url = getAPIBaseURL() + '/courses/'+courseId+'/sessions/' + sessionId + '/enrollments';
		String[] urlParams = new String[]{};
		if(status != null) urlParams.add('status='+status);
		if(modifiedSince != null) urlParams.add('modifiedSince='+formatDate(modifiedSince));
		if(urlParams.size() > 0) url += '?' + String.join(urlParams, '&');
		return url;
	}

	public static String getAPIEnrollURL(String userId, String courseId)
	{
		if(String.isBlank(userId)) return null;
		if(String.isBlank(courseId)) return null;
		String url = getAPIBaseURL() + '/users/' + userId + '/enrollments/'+courseId;
		return url;
	}

	public static String getAPIDepartmentsURL(String departmentName)
	{
		String url = getAPIBaseURL() + '/departments';
		if(!String.isBlank(departmentName)) url += '?departmentName='+EncodingUtil.urlEncode(departmentName, 'UTF-8');
		return url;
	}

	public static String getAPICountriesURL()
	{
		return getAPIBaseURL() + '/Countries';
	}

	public static String getAPIProvincesURL(String countryId)
	{
		return getAPIBaseURL() + '/Provinces?countryId='+countryId;
	}

	public static String getAPISessionsURL(String courseId)
	{
		return getAPIBaseURL() + '/courses/' + courseId + '/sessions';
	}

	public static String getAPISessionSchedulesURL()
	{
		return getAPIBaseURL() + '/SessionSchedules';
	}

	public static String getAPISessionScheduleURL(String sessionScheduleId)
	{
		return getAPIBaseURL() + '/SessionSchedules/' + sessionScheduleId;
	}

	public static String getAPIUpdateEnrollmentURL(String userId)
	{
		return getAPIBaseURL() + '/users/'+userId+'/enrollments';
	}

	private static String getAPIBaseURL()
	{
		String apiEndpointURL = getAPIEndpointURL();
		String urlParamsToken = apiEndpointURL.contains('requestb.in') ? '?' : '/'; //for testing with requestb.in, we want to append our RESTful URL to the URL
		return apiEndpointURL + urlParamsToken + 'api/Rest/v1';
	}

	//return the existing valid api token (hasn't expired yet)
	public static String getApiToken()
	{
		AbsorbLMSSettings__c settings = getAbsorbLMSSettings();
		if(String.isBlank(settings.API_Token__c) || settings.API_Token_Generated_At__c == null || settings.API_Token_Timeout__c == null) return null;
		return (settings.API_Token_Generated_At__c.addMinutes(Integer.valueOf(settings.API_Token_Timeout__c)) > DateTime.now()) ? settings.API_Token__c : null;
	}

	public static void updateToken(String apiToken, DateTime tokenGeneratedAt)
	{
		AbsorbLMSSettings__c settings = getAbsorbLMSSettings();
		settings.API_Token_Generated_At__c = tokenGeneratedAt;
		settings.API_Token__c = apiToken;
		upsert settings;
	}

	public static AbsorbLMSSettings__c getAbsorbLMSSettings() 
	{
		return AbsorbLMSSettings__c.getOrgDefaults();
	}

	private static String getAPIPrivateKey() 
	{
		return getAbsorbLMSSettings().API_Private_Key__c;
	}	

	private static String getAPIUsername() 
	{
		return getAbsorbLMSSettings().API_Username__c;
	}	

	private static String getAPIPassword() 
	{
		return getAbsorbLMSSettings().API_Password__c;
	}	

	private static String getAPIEndpointURL() 
	{
		return getAbsorbLMSSettings().Endpoint_URL__c;
	}	

	//generate a 32-digit hex ID
	public static String generateGUID()
	{
		Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
		String hyphen = '-';
		return h.SubString(0,8) + hyphen + h.SubString(8,12) + hyphen + h.SubString(12,16) + hyphen + h.SubString(16,20) + hyphen + h.substring(20);
	}

	public static String getDepartmentName(Contact c) {
		if(!String.isBlank(c.Sales_Region_GlobPick__c) && AbsorbRegionToDepartmentMapping__c.getAll().containsKey(c.Sales_Region_GlobPick__c) ) 
			return AbsorbRegionToDepartmentMapping__c.getAll().get(c.Sales_Region_GlobPick__c).Department_Name__c;
		else return 'Americas';
	}

	private static AbsorbModel.Country findCountry(String sfdcCountryValue)
	{
		List<AbsorbModel.Country> countries = AbsorbAPIClient.getInstance().getCountries();
		for(AbsorbModel.Country c : countries)
		{
			if(c.Name == sfdcCountryValue || c.CountryCode == sfdcCountryValue) return c;
		}
		return null;
	}

	private static AbsorbModel.Province findProvince(AbsorbModel.Country countryRecord, String sfdcProvinceValue)
	{
		List<AbsorbModel.Province> provs = AbsorbAPIClient.getInstance().getProvinces(countryRecord.Id);
		for(AbsorbModel.Province p : provs)
		{
			if(p.Name == sfdcProvinceValue) return p;
		}
		return null;
	}

	public static AbsorbModel.User createAbsorbUser(Contact c)
	{

		AbsorbModel.User u = c.LMS_User_ID__c == null ? new AbsorbModel.UserCreate() : new AbsorbModel.User();
		u.Id = c.LMS_User_ID__c;
		if(u.Id == null) ((AbsorbModel.UserCreate)u).Password = 'Welcome1';
		u.Username = c.LMS_Username__c;
		if(String.isBlank(u.Username)) u.Username = c.Email;
		u.Phone = c.Phone;
		u.Address = c.MailingStreet;
		u.PostalCode = c.MailingPostalCode;
		u.City = c.MailingCity;
		if(!String.isBlank(c.MailingCountry))
		{
			AbsorbModel.Country theCountry = findCountry(c.MailingCountry);
			if(theCountry != null) {
				u.CountryId = theCountry.Id;
				if(!String.isBlank(c.MailingState))
				{
					AbsorbModel.Province theProvince = findProvince(theCountry, c.MailingState);
					if(theProvince != null) u.ProvinceId = theProvince.Id;
				}
			}
		}
		u.CustomFields.MarketVertical = c.Market_Segment_St__c;
		u.CustomFields.LearnerLevel = c.LMS_Learner_Level__c;
		u.CustomFields.PrimaryWorkflow = c.LMS_Primary_Workflow__c;
		u.CustomFields.Entitlement = c.LMS_Content_Entitled__c;
		if(c.OnBoarding_Contacts__r != null && c.OnBoarding_Contacts__r.size() > 0) {
			OnBoarding_Contacts__c cob = c.OnBoarding_Contacts__r[0];
			//u.CustomFields.SalesOrderNumber = cob.Customer_Onboard__r.Opp_Order_No__c;
			//u.CustomFields.TrainingPartNumber = cob.Customer_Onboard__r.Ordered_Product__r.OracleID__c;
			//u.CustomFields.Product = cob.Customer_Onboard__r.Product__r.Name;
			//changes July 19/2016
			u.CustomFields.TrainingPartNumber = cob.Customer_Onboard__r.Product__r.ProductCode;
			//u.CustomFields.Product = cob.Customer_Onboard__r.Ordered_Product__r.ProductCode;
			//changed Product mapping Oct 14/2016
			//Apr 19/2017 change for Fast record type
            if(cob.Customer_Onboard__r.RecordTypeId != COBOUtil.getFastCOBORecordTypeId()) {
            	u.CustomFields.Product = cob.Customer_Onboard__r.Ordered_Product__r.Family;
            } else {
                //get from related asset's property
                u.CustomFields.Product = cob.Customer_Onboard__r.Asset__r.Product2.Family;
            }
			u.CustomFields.SalesOrderNumber = cob.Customer_Onboard__r.Oracle_Order__r.Oracle_Order_Number__c;
		}
		List<AbsorbModel.Department> departments = AbsorbAPIClient.getInstance().getDepartments(getDepartmentName(c));
		if(departments.size() > 0) u.DepartmentId = departments[0].Id;
		else throw new AbsorbAPIClient.CreateUserException('Could not determine & find the department from "'+getDepartmentName(c)+'"');
		u.FirstName = c.FirstName; //required
		//u.MiddleName = c.MiddleName;
		u.LastName = c.LastName; //required
		//u.Password = ;
		u.EmailAddress = c.Email; //required
		return u;
	}

	public static Contact findContact(Id contactId)
	{
		return [select Id, FirstName, LastName, Email, LMS_User_ID__c, LMS_Username__c, Region__c, Account.Name, LMS_Learner_Level__c, Phone,
			MailingStreet, MailingPostalCode, MailingState, MailingCity, MailingCountry,
			LMS_Primary_Workflow__c, Market_Segment_St__c, Sales_Region_GlobPick__c, LMS_Content_Entitled__c,
			(select Id, Customer_Onboard__r.RecordTypeId, Customer_Onboard__r.Asset__r.Product2.Family, Customer_Onboard__r.Product__r.Name, Customer_Onboard__r.Opp_Order_No__c, Customer_Onboard__r.Ordered_Product__r.OracleID__c, Customer_Onboard__r.Ordered_Product__r.ProductCode, Customer_Onboard__r.Ordered_Product__r.Family, Customer_Onboard__r.Product__c, Customer_Onboard__r.Product__r.ProductCode, Customer_Onboard__r.Oracle_Order__r.Oracle_Order_Number__c from OnBoarding_Contacts__r where Role__c = 'Additional Learner' or Role__c = 'Primary Learner' order by LastModifiedDate desc) 
			from Contact where Id = :contactId
		];
	}

	public static List<Contact> findContacts(Set<Id> contactIds)
	{
		return [select Id, FirstName, LastName, Email, LMS_User_ID__c, LMS_Username__c, Region__c, Account.Name, LMS_Learner_Level__c, Phone,
			MailingStreet, MailingPostalCode, MailingState, MailingCity, MailingCountry,
			LMS_Primary_Workflow__c, Market_Segment_St__c, Sales_Region_GlobPick__c, LMS_Content_Entitled__c,
			(select Id, Customer_Onboard__r.RecordTypeId, Customer_Onboard__r.Asset__r.Product2.Family, Customer_Onboard__r.Product__r.Name, Customer_Onboard__r.Opp_Order_No__c, Customer_Onboard__r.Ordered_Product__r.OracleID__c, Customer_Onboard__r.Ordered_Product__r.ProductCode, Customer_Onboard__r.Ordered_Product__r.Family, Customer_Onboard__r.Product__c, Customer_Onboard__r.Product__r.ProductCode, Customer_Onboard__r.Oracle_Order__r.Oracle_Order_Number__c from OnBoarding_Contacts__r where Role__c = 'Additional Learner' or Role__c = 'Primary Learner' order by LastModifiedDate desc) 
			from Contact where Id in :contactIds
		];
	}

	//format as YYYY-MM-DD
	public static String formatDate(Date dt) {
		if(dt == null) return null;
		return dt.year() +'-'+ String.valueOf(dt.month()).leftPad(2,'0') +'-'+ String.valueOf(dt.day()).leftPad(2,'0');
	}
}