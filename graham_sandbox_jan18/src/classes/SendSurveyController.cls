/*
* Class to send out surveys on click of Send Survey button.
* Send Survey field on Case contact is updated which causes the Workflow rule to run.q1	`
	w2
*/
public without sharing class SendSurveyController {
	
	// Picklist values of Survey Send through
	private static final String CASE_CONTACT = 'Case Contact';
	private static final String ALL_CASE_CONTACTS = 'All Case Contacts';
	private static final String CASE_OWNER = 'Case Owner';
	
	// Class variables
	
	public Id caseId;
	public String buttonName;
	public Id caseContactId;
	public List<Case_Contact__c> updateCaseContact = new List<Case_Contact__c>();
	
	public SendSurveyController(){	
		caseId 		= Apexpages.currentPage().getParameters().get('caseId');
		buttonName	= Apexpages.currentPage().getParameters().get('buttonName');
		caseContactId 	= Apexpages.currentPage().getParameters().get('ccid');
	}

	public PageReference initateSendingSurveys(){
		
		// buttonName ='contactOnly' passed as parameter only if we want to send survey to just the contact
		if (  buttonName.equalsIgnoreCase('contactOnly') ){
			sendSurveyToContactRecord();
		} 
		// The caseId passed as parameter && (buttonName ='caseOwner' || butttonName ='contactAll'
		else if ( buttonName.equalsIgnoreCase('caseOwner') ){
			sendSurveysToOwner();
		}
		else if ( buttonName.equalsIgnoreCase('contactAll') ){
			sendSurveyToAll();
		}
	
		try{
			// Updating the Send Survey to true will  trigger the outbound message.
			if(!updateCaseContact.isEmpty())
				update updateCaseContact;
		}catch(Exception e){
			system.debug('Exception caught while trying to intitate send surveys'+ e.getMessage() );
		}

		return new PageReference('/'+caseId);
	}
	
	void sendSurveyToContactRecord(){
		Case_Contact__c caseContactRecord = [SELECT Id, Send_Survey__c,Survey_Send_through__c FROM Case_Contact__c WHERE Id =: caseContactId];
		if (caseContactRecord != null ){
			caseContactRecord.Send_Survey__c = true;
			caseContactRecord.Survey_Send_through__c = CASE_CONTACT;
			updateCaseContact.add(caseContactRecord);
		}
	}
	
	void sendSurveyToAll(){
		for(Case_Contact__c cc: [SELECT Id, Send_Survey__c,Survey_Send_through__c FROM Case_Contact__c WHERE Case__c =: caseId]){
			cc.Send_Survey__c = true;
			cc.Survey_Send_through__c = ALL_CASE_CONTACTS;
			updateCaseContact.add(cc);
		}
	}
	
	void sendSurveysToOwner(){
		for(Case_Contact__c cc: [SELECT Id, Send_Survey__c,Survey_Send_through__c FROM Case_Contact__c WHERE Case__c =: caseId]){
			cc.Send_Survey__c = true;
			cc.Survey_Send_through__c = CASE_OWNER;
			updateCaseContact.add(cc);
		}
	}
}