@isTest
public with sharing class EditProductFeature_CTRL_UnitTest {
    static TestMethod void EditProductFeature_CTRL_UnitTest1(){
        Product_Category__c prodCategRecObj = Util.CreateProductCategory();
        Specifications_Meta__c speciMetaRecObj = Util.CreateProductfeatureCategory(prodCategRecObj.id);
        Product_Meta__c prrodMetaRecObj = Util.CreateProductfeature(speciMetaRecObj.Id);
        ApexPages.currentPage().getParameters().put('catid',speciMetaRecObj.Id);
        EditProductFeature_CTRL EditProductFeatureObj = new EditProductFeature_CTRL();
        EditProductFeatureObj.ok();
        EditProductFeatureObj.dodelete();
        EditProductFeatureObj.docancel();
    }
}