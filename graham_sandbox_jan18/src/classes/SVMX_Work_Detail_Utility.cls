global class SVMX_Work_Detail_Utility implements comparable 
{
	public SVMXC__Service_Order_Line__c wd;
	public Date startDate;

	public SVMX_Work_Detail_Utility(SVMXC__Service_Order_Line__c wd) 
	{
		this.wd = wd;	
		if (wd.SVMXC__Start_Date_and_Time__c != null)
			this.startDate = wd.SVMXC__Start_Date_and_Time__c.date();
		else
			this.startDate = System.today();
	}

	global Integer compareTo(Object compareTo) {
	
		SVMXC__Service_Order_Line__c compareToWD = ((SVMX_Work_Detail_Utility) compareTo).wd;

		if (compareToWD.SVMXC__Start_Date_and_Time__c != null)
		{
			if (this.startDate == compareToWD.SVMXC__Start_Date_and_Time__c.date())
				return 0;
			
			if (this.startDate < compareToWD.SVMXC__Start_Date_and_Time__c.date())
				return -1;
		}
		
		return 1;
	    
	}

}