@isTest
global class CustomerOnboardTriggerMockHTTPGenerator implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req){
        System.assertEquals('GET', req.getMethod());
        
        //create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"foo":"bar"}');
	    res.setStatusCode(200);
	    return res;
    }

}