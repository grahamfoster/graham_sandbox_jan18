/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 14/2014
**
** Controller to manage admin of Conversion Rates (GMs) and Quotas (reps)
** 
**/
public with sharing class MySFDCAdminController {
	public User currentUser {get;set;}
	public List<ConversionRateWrapper> conversionRates {get;set;}
	public Id selectedConversionRateId {get;set;}
	public List<QuotaWrapper> quotas {get;set;}
	public Id selectedQuotaId {get;set;}
	
	public MySFDCAdminController() {
	}

	public PageReference init(){
		currentUser = [select Id, Name, UserRole.Name, Profile.Name, License_Region__c, License_Department__c from User where Id = :UserInfo.getUserId()];	
		conversionRates = new List<ConversionRateWrapper>();
		String region = getRegion();
		String sector = MySFDCUtil.getUserSector(currentUser);

		if(region == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Region is unknown'));
			return null;
		}
		
		for(Conversion_Rate__c sobj : [select Id, Name, Opportunity_Type__c, Conversion_Rate__c from Conversion_Rate__c where Region__c = :region and Sector__c = :sector order by Opportunity_Type__c asc]) {
			conversionRates.add(new ConversionRateWrapper(sobj));
		}

//		conversionRates.add(new ConversionRateWrapper());

		//load quotas
		quotas = new List<QuotaWrapper>();
		for(Quota__c sobj : [select Id, Fiscal_Year__c, Fiscal_Quarter__c, Quota__c, Type__c from Quota__c where OwnerId = :UserInfo.getUserId() order by Fiscal_Year__c asc, Fiscal_Quarter__c asc, Type__c asc]) {
			quotas.add(new QuotaWrapper(sobj));
		}
		quotas.add(new QuotaWrapper());


		return null;
	}

	public PageReference editConversionRate(){
		for(ConversionRateWrapper crw : this.conversionRates) {
			if(crw.conversionRate != null && crw.conversionRate.Id == selectedConversionRateId) {
				crw.editMode = true;
			}
		}
		return null;
	}

	public PageReference cancelEditConversionRate(){
		for(ConversionRateWrapper crw : this.conversionRates) {
			if(crw.conversionRate != null && crw.conversionRate.Id == selectedConversionRateId && selectedConversionRateId != null) {
				crw.editMode = false;
			}else if(crw.conversionRate != null && crw.conversionRate.Id == null){
				crw.conversionRate = null;
				crw.editMode = false;
			}
		}
		selectedConversionRateId = null;
		return null;
	}

	public PageReference saveConversionRate(){
		try{
			boolean savedNew = false;
			for(ConversionRateWrapper crw : this.conversionRates) {
				if(crw.conversionRate != null && crw.conversionRate.Id == selectedConversionRateId && selectedConversionRateId != null) {
					update crw.conversionRate;
		            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Conversion Rate saved succesfully'));
					crw.editMode = false;
				}else if(crw.conversionRate != null && crw.conversionRate.Id == null){
					insert crw.conversionRate;
					savedNew = true;
		            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Conversion Rate saved succesfully'));
					crw.editMode = false;
				}
			}
			if(savedNew) conversionRates.add(new ConversionRateWrapper());
			selectedConversionRateId = null;
			return null;
		}catch(DmlException e){
	        for(integer i = 0; i < e.getNumDml(); i++){
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getDmlMessage(i)));
	        }
			return null;			
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
			return null;			
		}
	}

	public PageReference addConversionRate(){
		this.conversionRates[this.conversionRates.size()-1].conversionRate = new Conversion_Rate__c(Region__c = getRegion());
		this.conversionRates[this.conversionRates.size()-1].editMode = true;
		return null;
	}


	public PageReference editQuota(){
		for(QuotaWrapper q : this.quotas) {
			if(q.quota != null && q.quota.Id == selectedQuotaId) {
				q.editMode = true;
			}
		}
		return null;
	}

	public PageReference deleteQuota(){
		integer deletedIndex = -1;
		integer i = 0;
		for(QuotaWrapper q : this.quotas) {
			if(q.quota != null && q.quota.Id == selectedQuotaId) {
				delete q.quota;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Quota deleted succesfully'));
				deletedIndex = i;
			}
			i++;
		}
		if(deletedIndex != -1) this.quotas.remove(deletedIndex);
		return null;
	}

	public PageReference cancelEditQuota(){
		for(QuotaWrapper q : this.quotas) {
			if(q.quota != null && q.quota.Id == selectedQuotaId && selectedQuotaId != null) {
				q.editMode = false;
				Quota__c[] theQuota = [select Id, Fiscal_Year__c, Fiscal_Quarter__c, Quota__c, Type__c from Quota__c where Id = :selectedQuotaId];
				if(theQuota.size() > 0) q.quota = theQuota[0];
			}else if(q.quota != null && q.quota.Id == null){
				q.quota = null;
				q.editMode = false;
			}
		}
		selectedQuotaId = null;
		return null;
	}

	public PageReference saveQuota(){
		try{
			boolean savedNew = false;
			for(QuotaWrapper q : this.quotas) {
				if(q.quota != null && q.quota.Id == selectedQuotaId && selectedQuotaId != null) {
					update q.quota;
		            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Quota saved succesfully'));
					q.editMode = false;
				}else if(q.quota != null && q.quota.Id == null){
					insert q.quota;
					savedNew = true;
		            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Quota saved succesfully'));
					q.editMode = false;
				}
			}
			if(savedNew) quotas.add(new QuotaWrapper());
			selectedQuotaId = null;
			this.quotas.sort();
			return null;
		}catch(DmlException e){
	        for(integer i = 0; i < e.getNumDml(); i++){
	            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getDmlMessage(i)));
	        }
			return null;			
		}catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
			return null;			
		}
	}

	public PageReference addQuota(){
		this.quotas[this.quotas.size()-1].quota = new Quota__c(CurrencyIsoCode = 'USD');
		this.quotas[this.quotas.size()-1].editMode = true;
		return null;
	}

	public String getRegion(){
		/*
		integer gmIdx = currentUser.UserRole.Name.indexOf('GM ');
		integer salesIdx = currentUser.UserRole.Name.indexOf(' Sales');
		//TODO: if not a GM, query up the role hierarchy to figure out the region
		if(gmIdx != 0 || salesIdx < gmIdx) return 'Americas';
		return currentUser.UserRole.Name.substring(3, salesIdx);
		*/
		return currentUser.License_Region__c;
	}

	public Boolean editConversionRates {
		get {
			//only GM's edit the conversion rates
			//return (currentUser.UserRole.Name.indexOf('GM ') == 0) || currentUser.Profile.Name == 'System Administrator';
			/*
			List<ObjectPermissions> objPermissions = [SELECT Id, Parent.Profile.Name, Parent.PermissionsTransferAnyLead, PermissionsRead, PermissionsCreate, PermissionsEdit, PermissionsDelete
				FROM ObjectPermissions
				WHERE SobjectType = 'Conversion_Rate__c' and Parent.ProfileId = :currentUser.ProfileId];
			*/
			/*return objPermissions.size() > 0 
				&& objPermissions[0].PermissionsRead 
				&& objPermissions[0].PermissionsCreate 
				&& objPermissions[0].PermissionsEdit 
				&& objPermissions[0].PermissionsDelete;*/
			return Schema.SobjectType.Conversion_Rate__c.isAccessible() && Schema.SobjectType.Conversion_Rate__c.isUpdateable();
		}
	}

	public Boolean editQuotas {
		get {
			//only sales people edit the conversion rates
			/*
			List<ObjectPermissions> objPermissions = [SELECT Id, Parent.Profile.Name, Parent.PermissionsTransferAnyLead, PermissionsRead, PermissionsCreate, PermissionsEdit, PermissionsDelete
				FROM ObjectPermissions
				WHERE SobjectType = 'Quota__c' and Parent.ProfileId = :currentUser.ProfileId];

			return objPermissions.size() > 0 
				&& objPermissions[0].PermissionsRead 
				&& objPermissions[0].PermissionsCreate 
				&& objPermissions[0].PermissionsEdit 
				&& objPermissions[0].PermissionsDelete;
			*/
			return Schema.SobjectType.Quota__c.isAccessible() && Schema.SobjectType.Quota__c.isUpdateable() && Schema.SobjectType.Quota__c.isCreateable();
		}
	}

	public class ConversionRateWrapper {
		public Conversion_Rate__c conversionRate {get;set;}
		public Boolean editMode {get;set;}

		public ConversionRateWrapper(Conversion_Rate__c conversionRate) {
			this.conversionRate = conversionRate;
			this.editMode = false;
		}

		public ConversionRateWrapper() {
			this.editMode = false;
		}

		public Boolean renderEditButton {
			get {
				return !editMode && conversionRate != null;
			}
		}

		public Boolean renderSaveButton {
			get {
				return editMode && conversionRate != null;
			}
		}

		public Boolean renderCancelButton {
			get {
				return editMode && conversionRate != null;
			}
		}

		public Boolean renderNewButton {
			get {
				return !editMode && conversionRate == null;
			}
		}
	}

	public Boolean renderTypeColumn {
		get {
			return MySFDCUtil.isServiceUser(currentUser);
		}
	}

	public class QuotaWrapper implements Comparable {
		public Quota__c quota {get;set;}
		public Boolean editMode {get;set;}

		public QuotaWrapper(Quota__c quota){
			this.quota = quota;
			this.editMode = false;
		}

		public QuotaWrapper(){
			this.editMode = false;
		}

		public Integer compareTo(Object thatObj)
		{
			if(thatObj == null) return 1;
			QuotaWrapper that = (QuotaWrapper)thatObj;
			if(this.quota == null) return 1;
			else if (that.quota == null) return -1;
			else if(this.quota.Fiscal_Year__c != that.quota.Fiscal_Year__c && this.quota.Fiscal_Year__c != null) return this.quota.Fiscal_Year__c.compareTo(that.quota.Fiscal_Year__c);
			else if(this.quota.Fiscal_Quarter__c != that.quota.Fiscal_Quarter__c && this.quota.Fiscal_Quarter__c != null) return this.quota.Fiscal_Quarter__c.compareTo(that.quota.Fiscal_Quarter__c);
			else if(this.quota.Type__c != that.quota.Type__c && this.quota.Type__c != null) return this.quota.Type__c.compareTo(that.quota.Type__c);
			else return 0;
		}

		public Boolean renderEditButton {
			get {
				return !editMode && quota != null;
			}
		}

		public Boolean renderDeleteButton {
			get {
				return !editMode && quota != null;
			}
		}

		public Boolean renderSaveButton {
			get {
				return editMode && quota != null;
			}
		}

		public Boolean renderCancelButton {
			get {
				return editMode && quota != null;
			}
		}

		public Boolean renderNewButton {
			get {
				return !editMode && quota == null;
			}
		}
	}
}