@isTest(seealldata=true)
private class SVMX_WorkDetails_Test {
	
	@isTest static void test_method_one() {
		// Implement test code
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}


    static testMethod void setupData(){
    

         //Insert account
        Account acc = new Account(
                                Name = 'Test Account',
                                BillingStreet = '714 test street',
                                BillingCity = 'Chesterfield',
                                BillingState = 'MO',
                                BillingPostalCode = '12345',
                                BillingCountry = 'Canada',
                                ShippingStreet = '714 test street',
                                ShippingCity = 'Chesterfield',
                                ShippingState = 'MO',
                                ShippingPostalCode = '12345',
                                ShippingCountry = 'Canada');
        insert acc; 
        
        //RecordType siteRecordType = [Select Id from RecordType where Name = 'Site' And SObjectType = 'SVMXC__Site__c'];
        //Insert Inventpry location
        SVMXC__Site__c site = new SVMXC__Site__c(
                                Name = 'Test Location',
                                SVMXC__Account__c = acc.id,
                                SVMXC__Stocking_Location__c = true,
                                SVMXC__State__c = 'NY',
                                SVMXC__Service_Engineer__c = userInfo.getUserId(),
                                SVMXC__Country__c = 'United States',
                                SVMXC__Zip__c = '12345'
                                );

        insert site;

        SVMXC__Site__c site2 = new SVMXC__Site__c(Name='ToLoc', SVMXC__Country__c = 'United States', SVMXC__Account__c = acc.id);
        site2.SVMXC__Stocking_Location__c = true;
        insert site2;

        SVMXC__Site__c site3 = new SVMXC__Site__c(Name='ToLoc', SVMXC__Country__c = 'United States', SVMXC__Account__c = acc.id);
        site3.SVMXC__Stocking_Location__c = true;
        insert site3;
        
        //Insert sertialsed product
        Product2 serializedProd = new Product2(
                        Name = 'Test Product',
                        IsActive = true,
                        SVMXC__Tracking__c = 'Serialized',
                        SVMXC__Stockable__c = true,
                        SVMXC__Enable_Serialized_Tracking__c = true);
        //insert serializedProd;
        //Insert service team anfd a technician for it
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c(
                                                    Name = 'Test Service Team',
                                                    SVMXC__Active__c = true,
                                                    SVMXC__State__c = 'NY',
                                                    SVMXC__Country__c = 'United States',
                                                    SVMXC__Zip__c = '12345');
        insert serviceTeam;
        
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(
                                                        SVMXC__Active__c = true,
                                                        Name = 'Test Technician',
                                                        SVMXC__Service_Group__c = serviceTeam.Id,
                                                        SVMXC__Salesforce_User__c = userInfo.getUserId(),
                                                        SVMXC__Inventory_Location__c = site.Id);
        insert technician;
        
        Product2[] prods = new Product2[]{ serializedProd };
            
        Product2 prod0 = new Product2();
        prod0.isActive = true;
        prod0.Name = 'Thingamajig';
        prod0.Top200__c = 'MS';
        //insert prod0;
        prods.add(prod0);
        

        Product2 prod = new Product2();
        prod.isActive = true;
        prod.Name = 'ZONE Travel';
        prod.Top200__c = 'MS';
        //insert prod;
        prods.add(prod);

        Product2 prod1 = new Product2();
        prod1.isActive = true;
        prod1.Name = 'Non Billable Travel';
        prod1.ProductCode = 'SV0000293';
        prod1.Top200__c = 'MS';
        //insert prod1;
        prods.add(prod1);

        Product2 prod2 = new Product2();
        prod2.isActive = true;
        prod2.Name = 'SV Daily Travel';
        prod2.ProductCode = 'SV0000292';
        prod2.Top200__c = 'MS';
        //insert prod2;
        prods.add(prod2);

        Product2 prod3 = new Product2();
        prod3.isActive = true;
        prod3.Name = 'Test Product ZZZ';
        prod3.Top200__c = 'MS';
        //insert prod3;
        prods.add(prod3);

        Product2 workScopeProd = new Product2();
        workScopeProd.isActive = true;
        workScopeProd.Name = 'Bundle Test Product ZZZ';
        prod3.Top200__c = 'MS';
        //insert workScopeProd;
        prods.add(workScopeProd);
        
        insert prods;

        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(
        										Name = 'Hello Component',
        										SVMXC__Product__c = prod3.Id,
        										SVMXC__Country__c= 'United States',
        										SVC_Service_Zone__c = 'Zone 1',
        										SVMXC__Site__c = site2.Id,
                                                SVMXC__Company__c = acc.Id
        									);
        insert ip1;

        ip1.SVMXC__Site__c = site.Id;
        update ip1;


        //for (SVMXC__Product_Stock__c prodStock : [Select Id, SVMXC__Product__c, SVMXC__Location__c, SVMXC__Product_Cost__c, SVMXC__Location__r.Site__c from SVMXC__Product_Stock__c where SVMXC__Status__c = 'Available' and SVMXC__Location__r.Site__c = true and SVMXC__Location__c in :consumedLocations and SVMXC__Product__c in :consumedParts limit 10000])
        SVMXC__Product_Stock__c prodStock0 = new SVMXC__Product_Stock__c();
        prodStock0.SVMXC__Product__c = prod0.Id;
        prodStock0.SVMXC__Quantity2__c = 30;
        prodStock0.SVMXC__Product_Cost__c = 100.00;
        prodStock0.SVMXC__Status__c = 'Available';
        prodStock0.SVMXC__Location__c = site.Id;
        insert prodStock0;


        SVMXC__Product_Stock__c prodStock = new SVMXC__Product_Stock__c();
        prodStock.SVMXC__Product__c = prod.Id;
        prodStock.SVMXC__Quantity2__c = 30;
        prodStock.SVMXC__Product_Cost__c = 100.00;
        prodStock.SVMXC__Status__c = 'Available';
        prodStock.SVMXC__Location__c = site.Id;
        insert prodStock;

        SVMXC__Product_Stock__c prodStock2 = new SVMXC__Product_Stock__c();
        prodStock.SVMXC__Product__c = prod2.Id;
        prodStock.SVMXC__Quantity2__c = 30;
        prodStock.SVMXC__Product_Cost__c = 150.00;
        prodStock.SVMXC__Status__c = 'Available';
        prodStock.SVMXC__Location__c = site.Id;
        insert prodStock2;

        SVMXC__Product_Stock__c prodStock3 = new SVMXC__Product_Stock__c();
        prodStock3.SVMXC__Product__c = prod3.Id;
        prodStock3.SVMXC__Quantity2__c = 30;
        prodStock3.SVMXC__Product_Cost__c = 150.00;
        prodStock3.SVMXC__Status__c = 'Available';
        prodStock3.SVMXC__Location__c = site.Id;
        insert prodStock3;

        //Insert Work Orders
        List<SVMXC__Service_Order__c> workOrderList = new List<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c workOrder1 = new SVMXC__Service_Order__c();
        workOrder1.SVMXC__Order_Status__c = 'Open';
        workOrder1.SVMXC__Group_Member__c = technician.Id;
        workOrder1.SVMXC__Component__c = ip1.Id;
        workOrderList.add(workOrder1);
        
		SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c(Name='SCSCXXX');
		insert sc;        
        
        Test.startTest();
        Case c = new Case(Description = 'Hello Case');
        c.SVC_Service_Request_Type__c = 'ABSX Entitled';
        c.SVMXC__Service_Contract__c = sc.ID;
        insert c;

		site.SVC_Default_Service_Contract__c = sc.Id;

        SVMXC__Service_Order__c workOrder2 = new SVMXC__Service_Order__c();
        workOrder2.SVMXC__Order_Status__c = 'Open';
        workOrder2.SVMXC__Group_Member__c = technician.Id;
        workOrder2.SVMXC__Component__c = ip1.Id;
		workOrder2.SVMXC__Is_Entitlement_Performed__c = true;
		workOrder2.SVMXC__Auto_Entitlement_Status__c = 'Failed';  
		workOrder2.SVMXC__Case__c = c.Id;   

        workOrderList.add(workOrder2);
         
        insert workOrderList;


         //Insert work details
        //RecordType usageRecordType = [Select Id from RecordType where Name = 'Usage/Consumption' And SObjectType = 'SVMXC__Service_Order_Line__c'];
        Id usageRecordTypeId = SVMXC__Service_Order_Line__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();

        List<SVMXC__Service_Order_Line__c> workDetailList = new List<SVMXC__Service_Order_Line__c>();
 /*       
        SVMXC__Service_Order_Line__c workDetail1 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Product__c = serializedProd.Id,
                                                    SVMXC__Consumed_From_Location__c = site.Id,
													SVMXC__Actual_Quantity2__c = 1                                              
                                                    );
        workDetailList.add(workDetail1);
        
        SVMXC__Service_Order_Line__c workDetail2 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Consumed_From_Location__c = site.Id,
                                                    SVMXC__Product__c = workScopeProd.Id,
                                                    SVMXC__Actual_Quantity2__c = 1                                                  
                                                    );
        workDetailList.add(workDetail2);

        SVMXC__Service_Order_Line__c workDetail3 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Product__c = prod.Id,
                                                    SVMXC__Consumed_From_Location__c = site.Id,
													SVMXC__Actual_Quantity2__c = 1                                                  
                                                    );
        workDetailList.add(workDetail3);
*/
        SVMXC__Service_Order_Line__c workDetail4 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordTypeId,
                                                    SVMXC__Product__c = prod.Id,
													SVMXC__Actual_Quantity2__c = 1,                                                
                                                    Line_Sub_Type__c = 'Labor',
                                                    SVMXC__Line_Type__c= 'Parts'
                                                    );
        workDetailList.add(workDetail4);

        SVMXC__Service_Order_Line__c workDetail5 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordTypeId,
                                                    SVMXC__Product__c = prod.Id,
													SVMXC__Actual_Quantity2__c = 1,                                                 
                                                    Line_Sub_Type__c = 'Travel',
                                                    SVMXC__Line_Type__c= 'Parts',
                                                    SVMXC__Start_Date_and_Time__c = System.now()
                                                    );
        workDetailList.add(workDetail5);

        SVMXC__Service_Order_Line__c workDetail6 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordTypeId,
                                                    SVMXC__Product__c = prod0.Id,
                                                    SVMXC__Consumed_From_Location__c = site.Id,
													SVMXC__Actual_Quantity2__c = 1,     
													SVMXC__Line_Type__c= 'Parts',                                            
                                                    Line_Sub_Type__c = 'Material'
                                                    );
        workDetailList.add(workDetail6);

        SVMXC__Service_Order_Line__c workDetail7 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder1.id,
                                                    RecordTypeId = usageRecordTypeId,
                                                    SVMXC__Product__c = prod1.Id,
													SVMXC__Actual_Quantity2__c = 1,                                                 
                                                    Line_Sub_Type__c = 'Travel',
                                                    SVMXC__Line_Type__c= 'Parts',
                                                    SVMXC__Start_Date_and_Time__c = System.now().addHours(2)
                                                    );
        workDetailList.add(workDetail7);


        insert workDetailList;
                
        workDetailList[1].SVMXC__Start_Date_and_Time__c = System.now().addHours(1);
        update workDetailList;

/*        SVMXC__Service_Order_Line__c workDetail8 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder2.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Product__c = prod1.Id,
													SVMXC__Actual_Quantity2__c = 1,                                                 
                                                    Line_Sub_Type__c = 'Travel',
                                                    SVMXC__Line_Type__c= 'Parts',
                                                    SVMXC__Start_Date_and_Time__c = System.now()
                                                    );
        insert workDetail8;

        SVMXC__Service_Order_Line__c workDetail9 = new SVMXC__Service_Order_Line__c(
                                                    SVMXC__Service_Order__c = workOrder2.id,
                                                    RecordTypeId = usageRecordType.Id,
                                                    SVMXC__Product__c = prod1.Id,
													SVMXC__Actual_Quantity2__c = 1,                                                 
                                                    Line_Sub_Type__c = 'Travel',
                                                    SVMXC__Line_Type__c= 'Parts',
                                                    SVMXC__Start_Date_and_Time__c = System.now()
                                                    );
        insert workDetail9;
*/

        try
        {
	        SVMXC__Service_Order_Line__c workDetail20 = new SVMXC__Service_Order_Line__c(
	                                                    SVMXC__Service_Order__c = workOrder1.id,
	                                                    RecordTypeId = usageRecordTypeId,
	                                                    SVMXC__Product__c = prod.Id,
	                                                    SVMXC__Consumed_From_Location__c = site.Id,
														SVMXC__Actual_Quantity2__c = 190,                                                 
	                                                    Line_Sub_Type__c = 'Material'
	                                                    );
	        insert workDetail20;
	    }
	    catch (Exception e)
	    {
	    	//If we get here it is expected and we're good.
	    }


		Test.stopTest();

    }

	
}