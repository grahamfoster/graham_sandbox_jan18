/**
** @author Reid Beckett, Cloudware Connections
** @created Jun 4/2014
**
** Helper implements shadowing of OpportunityContactRole to/from Opportunity_Contact_Role_Shadow__c
** 
**/
public class OpportunityContactRoleShadowHelper {
	
	public OpportunityContactRoleShadowHelper() {
		
	}

	/**
	 ** pick up changes made on OpportunityContactRole records and upsert them to the shadow records
	 ** Called when SDS page is clicked for view and by nightly batch process
	 ** Returns a list of upserts to make
	**/
	public void syncToShadow(Set<Id> opportunityIds) {
		Map<String, Opportunity_Contact_Role_Shadow__c> shadowsMap = new Map<String, Opportunity_Contact_Role_Shadow__c>();
		Map<String, OpportunityContactRole> contactRolesMap = new Map<String, OpportunityContactRole>();

		for(OpportunityContactRole ocr : [select Id, OpportunityId, ContactId, Role, IsPrimary from OpportunityContactRole where OpportunityId in :opportunityIds]) {
			shadowsMap.put(uniqueKey(ocr.OpportunityId, ocr.ContactId), new Opportunity_Contact_Role_Shadow__c(Opportunity__c = ocr.OpportunityId, Contact__c = ocr.ContactId,
				Role__c = ocr.Role, IsPrimary__c = ocr.IsPrimary));
			contactRolesMap.put(uniqueKey(ocr.OpportunityId, ocr.ContactId), ocr);
		}

		List<Opportunity_Contact_Role_Shadow__c> existingShadows = [select Id, Opportunity__c, Contact__c, Role__c, IsPrimary__c, Unique_Key__c from Opportunity_Contact_Role_Shadow__c 
			where Unique_Key__c in :shadowsMap.keySet()];
		for(Opportunity_Contact_Role_Shadow__c shadow : existingShadows) {
			//sync fields from OpportunityContactRole
			OpportunityContactRole contactRole = contactRolesMap.get(shadow.Unique_Key__c);
			shadow.Role__c = contactRole.Role;
			shadow.IsPrimary__c = contactRole.IsPrimary;
			//replace in the map
			shadowsMap.put(shadow.Unique_Key__c, shadow);
		}
		
		//DML upsert to create/update shadow records
		upsert shadowsMap.values();
	}

	public void onBeforeShadow(){
		for(sObject sobj : Trigger.new) {
			Opportunity_Contact_Role_Shadow__c shadowObj = (Opportunity_Contact_Role_Shadow__c) sobj;
			shadowObj.Unique_Key__c = uniqueKey(shadowObj.Opportunity__c, shadowObj.Contact__c);
		}
	}

	private String uniqueKey(Id opportunityId, Id contactId) {
		return opportunityId + '-' + contactId;
	}
}