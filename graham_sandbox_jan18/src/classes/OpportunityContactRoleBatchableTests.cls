/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Apr 29/2014
 **
 ** Test coverage for OpportunityContactRoleBatchable
 ** 
**/
@isTest
public class OpportunityContactRoleBatchableTests {
	private static Id contactId = null;

	public static testMethod void test1() {
		setUp();

		Test.startTest();
		OpportunityContactRoleBatchable ocrb = new OpportunityContactRoleBatchable();
		Database.executeBatch(ocrb, 1);
		Test.stopTest();

		Contact c = [select Market_Segment_St__c, Market_Segment__c, Primary_Application__c from Contact where Id = :contactId];
		system.assertEquals('Academia/Omics', c.Market_Segment_St__c);
		system.assertEquals('Agriculture', c.Market_Segment__c);
		system.assertEquals('Amino Acid Analysis', c.Primary_Application__c);
	}

	private static void setUp(){
		GlobalSettings__c gs = GlobalSettings__c.getOrgDefaults();
		if(gs == null || gs.Id == null) {
			gs = new GlobalSettings__c();
			insert gs;
		}

		Account testAcct = new Account(Name = 'Test Account', BillingCountry = 'United States');
		insert testAcct;

		Contact testContact = new Contact(AccountId = testAcct.Id, FirstName = 'John', LastName = 'Doe', Email = 'johndoe@example.com');
		insert testContact;
		contactId = testContact.Id;

		OpportunityStage openStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = false and DefaultProbability >= 50 order by DefaultProbability desc limit 1];

		List<Opportunity> opps = new List<Opportunity>{
			new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), StageName = openStage.MasterLabel, 
				Customer_Type__c = 'Existing AB SCIEX account', Shippable_Date__c = Date.today(),
				Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000,
				Market_Vertical__c = 'Academia/Omics', Market_Segment__c = 'Agriculture', Primary_Application__c = 'Amino Acid Analysis')
		};
		insert opps;

		OpportunityContactRole ocr = new OpportunityContactRole(ContactId = testContact.Id, OpportunityId = opps[0].Id, Role = 'Other', IsPrimary = true);
		insert ocr;
	}
}