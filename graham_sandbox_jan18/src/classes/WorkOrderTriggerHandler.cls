/*
 *	WorkOrderTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2016-05-20 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class WorkOrderTriggerHandler extends TriggerHandler {
    
    static Boolean firstRun = TRUE;

    static Boolean firstafter = TRUE;
    
	public WorkOrderTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void beforeInsert() {
		populateDataBefore();
        populateFSRemail();
        findZonePrice();
	}
	override protected void beforeUpdate() {
    	populateDataBefore();
        populateFSRemail();
        findZonePrice();
	}
    override protected void afterInsert() {
		populateDataAfter();
        updateCaseDates();
	}
	override protected void afterUpdate() {
    	populateDataAfter();
        updateCaseDates();
	}
	override protected void afterUndelete() {
    
	}
	override protected void afterDelete() {
    
	}

	// **************************************************************************
	// 		private methods
	// **************************************************************************
	/* 
	 * Copied from original WorkOrderTriggerHandler written by Reid Beckette
	*/
    public void populateDataBefore()
	//It is a trigger to update 3 case fields Problem Code, Resolution Code, & Summary Field.
	//When these 3 same fields are updated on a related Work Order (they have same field names).
	//If a new related Work Order is created from a Case then 3 fields need to be brought over to the new Work Order.
	//If these 3 fields are updated on the new Work Order then this updates the 3 fields on the related Case.
    //Aug 31 - add populate of 2 more Case fields: Install_Start_DateTime__c, Install_End_DateTime__c      
    //Nov 30 - Add populate of 1 more field: Zone  
    {
        Set<Id> caseIds = collectParentIds(Trigger.new, SVMXC__Service_Order__c.SVMXC__Case__c.getDescribe().getName());
        Set<Id> techIds = collectParentIds(Trigger.new, SVMXC__Service_Order__c.SVMXC__Group_Member__c.getDescribe().getName());
        //  Added mapping of the IB Country to Work Order Country Field 
        Map<Id,Case> caseMap = new Map<Id,Case>([select Id, CurrencyIsoCode, AccountId, SVC_SR_Problem_Code__c, SVC_Resolution_Code__c, SVMX_Resolution_Summary__c, SVMXC__Component__r.SVMXC__Country__c, SVMXC__Component__r.SVC_Service_Zone__c, Account.Country_Mapping__r.Default_Service_Price_Book__c, SVMXC__Component__r.SVC_Ship_to_Address__c, SVC_Primary_FSE__r.SVMXC__Inventory_Location__c   from Case where Id in :caseIds]);
        //Added a map for technicians
        Map<Id,SVMXC__Service_Group_Members__c> techMap = new Map<Id,SVMXC__Service_Group_Members__c>([select Id, SVMXC__Salesforce_User__r.Id, SVMXC__Inventory_Location__c  from SVMXC__Service_Group_Members__c where Id in : techIds]);
        
        for(SVMXC__Service_Order__c wo : (List<SVMXC__Service_Order__c>)Trigger.new)
        {
            if((wo.CurrencyIsoCode != null || Trigger.isInsert) && wo.SVMXC__Case__c != null && caseMap.containsKey(wo.SVMXC__Case__c))
            {
				Case theCase = caseMap.get(wo.SVMXC__Case__c);
                wo.CurrencyIsoCode = theCase.CurrencyIsoCode;
            }

            if(wo.SVMXC__Case__c != null && caseMap.containsKey(wo.SVMXC__Case__c))
            {
				Case theCase = caseMap.get(wo.SVMXC__Case__c);
                wo.SVMXC__Company__c = theCase.AccountId;
                if(Trigger.isInsert)
                {
					if(wo.SVC_Problem_Code__c == null) wo.SVC_Problem_Code__c = theCase.SVC_SR_Problem_Code__c;
                    if(wo.SVC_Resolution_Code__c == null) wo.SVC_Resolution_Code__c = theCase.SVC_Resolution_Code__c;
                    if(wo.SVC_Resolution_Summary__c == null) wo.SVC_Resolution_Summary__c = theCase.SVMX_Resolution_Summary__c;
                }
            }
            //  Added mapping of the IB Country to Work Order Country Field 
            if(wo.SVMXC__Country__c == null && caseMap.containsKey(wo.SVMXC__Case__c)){
                Case theCase = caseMap.get(wo.SVMXC__Case__c);
                wo.SVMXC__Country__c = theCase.SVMXC__Component__r.SVMXC__Country__c;
            }
			//  Added mapping of the Zone Field 
            if(wo.FSH_Zone__c == null && caseMap.containsKey(wo.SVMXC__Case__c)){
                Case theCase = caseMap.get(wo.SVMXC__Case__c);
                if(string.isNotBlank(theCase.SVMXC__Component__r.SVC_Service_Zone__c)){
                    wo.FSH_Zone__c = theCase.SVMXC__Component__r.SVC_Service_Zone__c;
                }
            }
  			//  Added mapping of the Parts Pricebook Field 
            if(wo.FSH_Part_Pricebook__c == null && caseMap.containsKey(wo.SVMXC__Case__c)){
                Case theCase = caseMap.get(wo.SVMXC__Case__c);
                if(string.isNotBlank(theCase.Account.Country_Mapping__r.Default_Service_Price_Book__c)){
                  wo.FSH_Part_Pricebook__c = theCase.Account.Country_Mapping__r.Default_Service_Price_Book__c;
                }
            }
            //  Added mapping of the Install Base. Shipping location Field 
            if(wo.IB_Ship_to__c == null && caseMap.containsKey(wo.SVMXC__Case__c)){
                Case theCase = caseMap.get(wo.SVMXC__Case__c);
                if(string.isNotBlank(theCase.SVMXC__Component__r.SVC_Ship_to_Address__c)){
                  wo.IB_Ship_to__c = theCase.SVMXC__Component__r.SVC_Ship_to_Address__c;
                }
            }
            
            //Oct 6 - ER# 1239 - Rule to ensure Assigned Technician = workorder Owner
            if(wo.SVMXC__Group_Member__c != null && techMap.containsKey(wo.SVMXC__Group_Member__c)){
            	SVMXC__Service_Group_Members__c tech = techMap.get(wo.SVMXC__Group_Member__c);
                    if(tech.SVMXC__Salesforce_User__r.Id != null){
                          if(wo.OwnerId != tech.SVMXC__Salesforce_User__r.Id){
                            wo.OwnerId = tech.SVMXC__Salesforce_User__r.Id;
                        }  
                }
                //  Set WO Consumed from Location
                if(string.isNotBlank(tech.SVMXC__Inventory_Location__c )){
                  	wo.SVC_Consumed_From_Location__c = tech.SVMXC__Inventory_Location__c;
                  	wo.Dispatch_Notes__c = 'updated';	
                }  else if(!wo.LocationIndicator__c && caseMap.containsKey(wo.SVMXC__Case__c)) {
                	wo.SVC_Consumed_From_Location__c = NULL;
            	}
            }
           
        }
    }
    
    public void populateDataAfter()
    {
        // Only run once per transaction
        if(firstafter){
            firstafter = FALSE;
        	//It is a trigger to update 3 case fields Problem Code, Resolution Code, & Summary Field.
			//When these 3 same fields are updated on a related Work Order (they have same field names).
			//If a new related Work Order is created from a Case then 3 fields need to be brought over to the new Work Order.
			//If these 3 fields are updated on the new Work Order then this updates the 3 fields on the related Case.
        	//Aug 31 - add populate of 2 more Case fields: Install_Start_DateTime__c, Install_End_DateTime__c
        	Set<Id> caseIds = collectParentIds(Trigger.new, SVMXC__Service_Order__c.SVMXC__Case__c.getDescribe().getName());
        	Map<Id,Case> caseMap = new Map<Id,Case>([select Id, CurrencyIsoCode, AccountId, Survey_Task_Type__c from Case where Id in :caseIds]);
        	Map<Id,Case> caseUpdates = new Map<Id,Case>();
        	for(SVMXC__Service_Order__c wo : (List<SVMXC__Service_Order__c>)Trigger.new)
	        {
	            if(wo.SVMXC__Case__c != null && caseMap.containsKey(wo.SVMXC__Case__c))
	            {
	                Case theCase = caseMap.get(wo.SVMXC__Case__c);
	                if(Trigger.isUpdate)
	                {
	                    SVMXC__Service_Order__c old_wo = (SVMXC__Service_Order__c)Trigger.oldMap.get(wo.Id);
	                    
	                    	//update the case Problem Code
	                        theCase.SVC_SR_Problem_Code__c = wo.SVC_Problem_Code__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                        //update the case Resolution Code
	                        theCase.SVC_Resolution_Code__c = wo.SVC_Resolution_Code__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                        //update the case Resolution Summary
	                        if(String.isNotBlank(wo.SVC_Resolution_Summary__c)) theCase.SVMX_Resolution_Summary__c = wo.SVC_Resolution_Summary__c.left(83) ;
	                        caseUpdates.put(theCase.Id, theCase);
	                        
	                    // RK - Sep 22, 2017 - Reverted to previous code because of blank Resolution/Problem code at case level issue
                        /*
	                    if(old_wo.SVC_Problem_Code__c != wo.SVC_Problem_Code__c)
	                    {
	                        //update the case Problem Code
	                        theCase.SVC_SR_Problem_Code__c = wo.SVC_Problem_Code__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                    }
	                    if(old_wo.SVC_Resolution_Code__c != wo.SVC_Resolution_Code__c)
	                    {
	                        //update the case Resolution Code
	                        theCase.SVC_Resolution_Code__c = wo.SVC_Resolution_Code__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                    }
	                    if(old_wo.SVC_Resolution_Summary__c != wo.SVC_Resolution_Summary__c)
	                    {
	                        //update the case Resolution Summary
	                        if(String.isNotBlank(wo.SVC_Resolution_Summary__c)) theCase.SVMX_Resolution_Summary__c = wo.SVC_Resolution_Summary__c.left(83) ;
	                        caseUpdates.put(theCase.Id, theCase);
	                    }
	                    */
	                    
	                    if(old_wo.SVMXC_Scheduled_Date_c__c != wo.SVMXC_Scheduled_Date_c__c)
	                    {
	                        //update the case Install Start Date
	                        theCase.Install_Start_DateTime__c = wo.SVMXC_Scheduled_Date_c__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                    }
	                    if(old_wo.SVC_Scheduled_End__c != wo.SVC_Scheduled_End__c)
	                    {
	                        //update the case Install End Date
	                        theCase.Install_End_DateTime__c = wo.SVC_Scheduled_End__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                    }
	                    if(old_wo.SVMXC__Order_Type__c != wo.SVMXC__Order_Type__c || String.isBlank(theCase.Survey_Task_Type__c) )
	                    {
	                        //update the Survey Task Type on case 
	                        theCase.Survey_Task_Type__c = wo.SVMXC__Order_Type__c;
	                        caseUpdates.put(theCase.Id, theCase);
	                    }
	                }else if (Trigger.isInsert)
	                {
	                    //update the case Problem Code
	                    theCase.SVC_SR_Problem_Code__c = wo.SVC_Problem_Code__c;
	                    //update the case Resolution Code
	                    theCase.SVC_Resolution_Code__c = wo.SVC_Resolution_Code__c;
	                    //update the case Resolution Summary
	                    theCase.SVMX_Resolution_Summary__c = wo.SVC_Resolution_Summary__c;
	                    theCase.Install_Start_DateTime__c = wo.SVMXC_Scheduled_Date_c__c;
	                    theCase.Install_End_DateTime__c = wo.SVC_Scheduled_End__c;
	                    //update the Survey Task Type on case 
	                    theCase.Survey_Task_Type__c = wo.SVMXC__Order_Type__c;

	                    caseUpdates.put(theCase.Id, theCase);
	                }
	            }
	        }
        if(caseUpdates.size() > 0) update caseUpdates.values();
        }
    }

	/* 
	 *  Helper Method moved from BaseTriggerHandler 
	*/    
    public Set<Id> collectParentIds(List<sObject> sobjs, String parentIdField)
    {
        Set<Id> ids = new Set<Id>();
        for(sObject sobj : sobjs)
        {
            if(sobj.get(parentIdField) != null) ids.add((Id)sobj.get(parentIdField));
        }
        return ids;
    }
    /* 
	 *  Set the FSR Notification email fields based on values set in the 
	 *  workOrderFSRemailNotifications__c Custom Setting 
	*/ 
    public  void populateFSRemail(){
		// Query workOrderFSRemailNotifications__c and populate Map of "Service Team Name", Object
        List<workOrderFSRemailNotifications__c> allsettings = new List<workOrderFSRemailNotifications__c>([SELECT Country__c, Operating_Unit__c, SEP_Email__c, email_1__c, email_2__c FROM workOrderFSRemailNotifications__c WHERE isDeleted = FALSE ]);  
        Map<String, workOrderFSRemailNotifications__c> mapByCountry = new Map<String, workOrderFSRemailNotifications__c>();
        Map<String, workOrderFSRemailNotifications__c> mapByOU = new Map<String, workOrderFSRemailNotifications__c>();
        for (workOrderFSRemailNotifications__c i :allsettings){
            mapByCountry.put(i.Country__c,i);
            if(String.isBlank(i.Country__c))mapByOU.put(i.Operating_Unit__c,i);
        }     
        if(allsettings.size() > 0){
        	// Loop through all records
        	For( SVMXC__Service_Order__c wo : (List<SVMXC__Service_Order__c>)Trigger.new){
	        	// Check if update is required
	            if(String.isNotBlank(wo.SVC_Operating_Unit__c) && String.isNotBlank(wo.SVC_Service_Team_Name__c)){
                    if(mapByCountry.containsKey(wo.SVMXC__Country__c) && String.isNotBlank(mapByCountry.get(wo.SVMXC__Country__c).email_1__c)){                  
                        if(wo.SVC_Service_Team_Name__c.contains('SEP') && String.isNotBlank(mapByCountry.get(wo.SVMXC__Country__c).SEP_Email__c )){
                            wo.FSR_Notification_address1__c = mapByCountry.get(wo.SVMXC__Country__c).SEP_Email__c;                    
                        } else {
                            wo.FSR_Notification_address1__c = mapByCountry.get(wo.SVMXC__Country__c).email_1__c;                    
                            if(mapByCountry.containsKey(wo.SVMXC__Country__c) && String.isNotBlank(mapByCountry.get(wo.SVMXC__Country__c).email_2__c)){
                        	    wo.FSR_Notification_address2__c = mapByCountry.get(wo.SVMXC__Country__c).email_2__c;                        
                            }
                        }
                    } else if (mapByOU.containsKey(wo.SVC_Operating_Unit__c) && String.isNotBlank(mapByOU.get(wo.SVC_Operating_Unit__c).email_1__c)){
                        if(wo.SVC_Service_Team_Name__c.contains('SEP') && String.isNotBlank(mapByOU.get(wo.SVC_Operating_Unit__c).SEP_Email__c )){
                            wo.FSR_Notification_address1__c = mapByOU.get(wo.SVC_Operating_Unit__c).SEP_Email__c;                    
                        } else {
                            wo.FSR_Notification_address1__c = mapByOU.get(wo.SVC_Operating_Unit__c).email_1__c;                    
                            if(mapByOU.containsKey(wo.SVC_Operating_Unit__c) && String.isNotBlank(mapByOU.get(wo.SVC_Operating_Unit__c).email_2__c)){
                        	    wo.FSR_Notification_address2__c = mapByOU.get(wo.SVC_Operating_Unit__c).email_2__c;                        
                            }
                        }

                    }  
	            }
 	       }
        }
    }
    
    public  void updateCaseDates(){
   
        // Only run once per transaction
        if(firstRun){
            firstRun = FALSE;
			// When Work Order changes, Roll up the MIN Labor Start Date and MAX Labor End Date to the parent Case
			// (Used for consolidating all Work Orders into 1 FSR document)    
			List <ID> caseIds = new List <ID>();    
			For( SVMXC__Service_Order__c wo : (List<SVMXC__Service_Order__c>)Trigger.new){
	            //  Make a list of all Work Order parent Cases
	            caseIDs.add(wo.SVMXC__Case__c);
	        }
	        //Query for all Parent Cases
	        List <Case> caseList = new List <Case>([SELECT id, SVC_Labor_End_Date__c, SVC_Labor_Start_Date__c,Last_Work_Order_Close__c FROM Case WHERE id IN :caseIds]);
	        //Query for all Work Orders with the same parent Case(s)
	        List <SVMXC__Service_Order__c> woList = new List <SVMXC__Service_Order__c>([SELECT id, SVC_Labor_End_Date__c, SVC_Labor_Start_Date__c, SVMXC__Closed_On__c, SVMXC__Case__c FROM SVMXC__Service_Order__c WHERE SVMXC__Order_Status__c = 'Completed' AND SVMXC__Case__c IN :caseIds]);
			List <Case> updatedCases = new List <Case>();
	        // Iterate through each Case
	        For(Case c :caseList){
	            DateTime lStart;
	            DateTime lEnd;
	            DateTime lClose;                
	            // Find the MIN Labor Start Date and MAX Labor End Date for this Case
	            For( SVMXC__Service_Order__c w :  woList){
	                if(w.SVMXC__Case__c == c.id){
System.debug('@@@@@@ w.SVC_Labor_Start_Date__c: ' + w.SVC_Labor_Start_Date__c) ;
System.debug('@@@@@@ SVMXC__Closed_On__c: ' + w.SVMXC__Closed_On__c) ;                         
	                    if(lStart == null || lStart > w.SVC_Labor_Start_Date__c){
	                        lStart =w.SVC_Labor_Start_Date__c;
	                    }
	                    if(lEnd == null || lEnd < w.SVC_Labor_End_Date__c){
	                        lEnd = w.SVC_Labor_End_Date__c;
	                    }
						if(lClose == null || lClose < w.SVMXC__Closed_On__c  ){
	                        lClose = w.SVMXC__Closed_On__c ;
	                    }
	                }
	            } 
	            // Assign dates to Case
System.debug('@@@@@@ lStart: ' + lStart) ;
System.debug('@@@@@@ lClose: ' + lClose) ;                
	            if( lStart != null) c.SVC_Labor_Start_Date__c = lStart;
	            if( lEnd != null) c.SVC_Labor_End_Date__c = lEnd;
                if( lClose != null) c.Last_Work_Order_Close__c = lClose;
	            updatedCases.add(c);
	            
	        }        
	        if(updatedCases.size() > 0){
          		update updatedCases;
        	}
        }
    }

    public static void findZonePrice(){
        // Loop through all records and collect Part and Pricebook IDs
        List <ID> pbIds = new List <ID>();
        For( SVMXC__Service_Order__c wo : (List<SVMXC__Service_Order__c>)Trigger.new){
            if(String.isNotEmpty(wo.FSH_Part_Pricebook__c)){
                pbIds.add(wo.FSH_Part_Pricebook__c);
            }
        }
        if(pbIds.size() > 0 ){
            // Query all needed PBEs
        	List <PriceBookEntry> PBEs = new List <PriceBookEntry>([SELECT id, PRICEBOOK2ID,CURRENCYISOCODE,PRODUCT2ID, NAME,UNITPRICE, ProductCode FROM PriceBookEntry WHERE PRICEBOOK2ID IN :pbIds AND (Name LIKE 'Zone%' OR ProductCode = 'SV000009' OR ProductCode LIKE 'SV000091%' OR ProductCode = 'SV000029')]);
            // Loop for each trigger record
            For( SVMXC__Service_Order__c wo : (List<SVMXC__Service_Order__c>)Trigger.new){
                // find correct PBE
                if(String.isNotEmpty(wo.FSH_Zone__c)){
                	for(PricebookEntry Entry :PBEs){ 
                       	if(Entry.Pricebook2Id == wo.FSH_Part_Pricebook__c && Entry.CurrencyIsoCode == wo.CurrencyIsoCode){
                            if(Entry.Name == wo.FSH_Zone__c.toUpperCase() + ' STANDARD TRAVEL CHARGE'){
                            	wo.Zone_Fee__c = Entry.UnitPrice;                            
                            } else if(Entry.ProductCode == 'SV000009'){
                                wo.FSH_Labor_Price__c=wo.FSH_Labor_Hours__c * Entry.UnitPrice ;
                            } else if(Entry.ProductCode == 'SV000091 - '+ wo.FSH_Model__c.trim() && wo.SVMXC__Order_Type__c.contains('Planned Maintenance')){
                                wo.FSH_FPPM_Price__c=Entry.UnitPrice ;
                            } else if(Entry.ProductCode == 'SV000029'){
                                wo.FSH_Overnight_Fee__c = Entry.UnitPrice ;   
                            }
	                    }
                    }
                }
                
            }
        }
    }

}