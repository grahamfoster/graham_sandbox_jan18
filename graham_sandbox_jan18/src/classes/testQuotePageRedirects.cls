/*********************************************************************
Name  : testQuotePageRedirects
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuotePageRedirects  

*********************************************************************/

public class testQuotePageRedirects{

  public static testMethod void checkQuotePageRedirects() {
    
    PageReference  pref;
    pref = QuotePageRedirects.LandingPage();
    pref = QuotePageRedirects.NewHeader();
    pref = QuotePageRedirects.NewPartner();
    pref = QuotePageRedirects.NewOptions();
    pref = QuotePageRedirects.ProductSelect();
    pref = QuotePageRedirects.productDiscount();
    pref = QuotePageRedirects.goToProductDiscountZAUABD();
    pref = QuotePageRedirects.goToProductDiscountZDF1();
    pref = QuotePageRedirects.ProductSort();
    pref = QuotePageRedirects.ProductSearch();
    pref = QuotePageRedirects.goToProductHierchy();
    pref = QuotePageRedirects.TemplateSearch();
    pref = QuotePageRedirects.Materials();
    pref = QuotePageRedirects.Summary();
    pref = QuotePageRedirects.ApprovalLog();
    pref = QuotePageRedirects.SessionLog();
    pref = QuotePageRedirects.ProductDetail();
  }  
}