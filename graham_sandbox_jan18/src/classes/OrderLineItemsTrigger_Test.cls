// ===========================================================================
// Object: OrderLineItemsTrigger_Test
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Test coverage for the OrderLineItemsTrigger
// ===========================================================================
// Changes: 2017-02-18 Reid Beckett
//           Class created
// ===========================================================================
@isTest
public class OrderLineItemsTrigger_Test 
{
    
    //this standard test executes all the events
    @isTest 
    static void test() {
        ProductFamilySettings__c pfs1 = new ProductFamilySettings__c(Name = '2000', Instrument__c = true, Priority__c = 2);
        insert pfs1;

        Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert acct;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Account__c = acct.Id);
        insert oracleOrder;

        Product2 p2000 = new Product2(Family = '2000', ProductCode = 'p2000', Name = 'Test Product 2000');
        insert p2000;

        Order_Line_Items__c lineItem1 = new Order_Line_Items__c(Oracle_Order__c = oracleOrder.Id, Product__c = p2000.Id);
        insert lineItem1;
        update lineItem1;
        delete lineItem1;
        undelete lineItem1;
    }

    //This test asserts the prioritization of the rollup of order line items to the order when setting the MS Product, MS Product 2, and MS Product 3 fields on the order
    //The order of priority is determined by the productfamilysettings custom settings Priority__c value
    @isTest 
    static void test_prioritization() 
    {
        ProductFamilySettings__c pfs1 = new ProductFamilySettings__c(Name = '2000', Instrument__c = true, Priority__c = 2);
        ProductFamilySettings__c pfs2 = new ProductFamilySettings__c(Name = '3000', Instrument__c = true, Priority__c = 1);
        ProductFamilySettings__c pfs3 = new ProductFamilySettings__c(Name = '3200', Instrument__c = true, Priority__c = 3);
        insert new List<ProductFamilySettings__c>{ pfs1, pfs2, pfs3 };

        Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert acct;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Account__c = acct.Id);
        insert oracleOrder;

        Product2 p2000 = new Product2(Family = '2000', ProductCode = 'p2000', Name = 'Test Product 2000');
        Product2 p3000 = new Product2(Family = '3000', ProductCode = 'p3000', Name = 'Test Product 3000');
        Product2 p3200 = new Product2(Family = '3200', ProductCode = 'p3200', Name = 'Test Product 3200');

        Product2 tp1 = new Product2(Family = 'Service', ProductCode = 'TRN00016', Name = 'Test Product TRN00016', Igor_Class__c='H', Part_Priority__c='2');
        Product2 tp2 = new Product2(Family = 'Service', ProductCode = 'TRN00013', Name = 'Test Product TRN00013', Igor_Class__c='H', Part_Priority__c='1');
        insert new List<Product2>{ p2000, p3000, p3200, tp1, tp2 };

        Order_Line_Items__c lineItem1 = new Order_Line_Items__c(Oracle_Order__c = oracleOrder.Id, Product__c = p2000.Id);
        Order_Line_Items__c lineItem2 = new Order_Line_Items__c(Oracle_Order__c = oracleOrder.Id, Product__c = p3000.Id);
        Order_Line_Items__c lineItem3 = new Order_Line_Items__c(Oracle_Order__c = oracleOrder.Id, Product__c = p3200.Id);
        Order_Line_Items__c lineItem4 = new Order_Line_Items__c(Oracle_Order__c = oracleOrder.Id, Product__c = tp1.Id);
        Order_Line_Items__c lineItem5 = new Order_Line_Items__c(Oracle_Order__c = oracleOrder.Id, Product__c = tp2.Id);

        insert new List<Order_Line_Items__c>{ lineItem1, lineItem2, lineItem3, lineItem4, lineItem5 };

        /**
        List<OrderLineItemTriggerHandler.OrderLineWrapper> orderLines = new List<OrderLineItemTriggerHandler.OrderLineWrapper>();
        for(Order_Line_Items__c lineItem : [select Id, Oracle_Order__c, Total_Number_of_FAS_Days_Purchased__c, Actual_Shipment_Date__c, Requested_Shipment_Date__c, Scheduled_Shipment_Date__c, Product__r.Igor_Class__c, Product__r.ProductCode, Status__c, Product__r.Part_Priority__c, Product__r.Family from Order_Line_Items__c where Oracle_Order__c = :oracleOrder.Id])
        {
            orderLines.add(new OrderLineItemTriggerHandler.OrderLineWrapper(lineItem));
        }
        orderLines.sort();
        system.assertEquals(p3000.Id, orderLines.get(0).lineItem.Product__c);
        **/

        oracleOrder = [select MS_Product__c, MS_Product_2__c, MS_Product_3__c, Training_Product__c, Additional_Training_Product__c from Oracle_Order__c where Id = :oracleOrder.Id];
        system.assertEquals('p3000', oracleOrder.MS_Product__c);
        system.assertEquals('p2000', oracleOrder.MS_Product_2__c);
        system.assertEquals('p3200', oracleOrder.MS_Product_3__c);
        system.assertEquals('TRN00013', oracleOrder.Training_Product__c);
        system.assertEquals('TRN00016', oracleOrder.Additional_Training_Product__c);
    }
    
}