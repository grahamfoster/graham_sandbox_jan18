/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 25/2015
**
** For new Parts Request Lines (managed custom object), if the status of the Parts Request parent = “Submitted” then update the line status picklist to “Submitted”
** 
**/
public class PartsRequestLineTriggerHandler extends BaseTriggerHandler
{
    public void onBefore()
    {
        Set<Id> parentIds = collectParentIds(Trigger.new, 'SVMXC__Parts_Request__c');
        Map<Id,SVMXC__Parts_Request__c> partsReqMap = new Map<Id,SVMXC__Parts_Request__c>(
            [select Id, SVMXC__Status__c from SVMXC__Parts_Request__c where Id in :parentIds]
        );

        for(SVMXC__Parts_Request_Line__c reqLine : (List<SVMXC__Parts_Request_Line__c>)Trigger.new)
        {
            SVMXC__Parts_Request__c parentPartsReq = partsReqMap.get(reqLine.SVMXC__Parts_Request__c);
            if(parentPartsReq != null && parentPartsReq.SVMXC__Status__c == 'Submitted')
            {
                reqLine.SVMXC__Line_Status__c = 'Submitted';
            }
        }
    }
}