/*
 *	TaskTrigger_Test
 *	
 *	Test class for ContactTrigger and ContactTriggerHandler.
 *
 *	If there are other test classes related to ContactTrigger, please document it here (as comments).
 * 
 * 	Created by Graham Foster on 2016-08-30
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
public class TaskTrigger_Test {
    
    // IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] TaskTrigger_Test() [Begin]');
    	Task iudu = new Task(Subject = 'Test Task');
		insert iudu;
		update iudu;
		delete iudu;
		undelete iudu;
        system.debug('[TEST] TaskTrigger_Test() [Begin]');
     }

	 @IsTest static void test_TrainingTasks()
	 {
		//Training Case Testing Setup
		Training_Part_Processes__c tppFullLp = new Training_Part_Processes__c(Name = 'FullLp', Process_Path__c = 1);
		Training_Part_Processes__c tppPostOnly = new Training_Part_Processes__c(Name = 'PostOnly', Process_Path__c = 2);
		Training_Part_Processes__c tppOnsiteOnly = new Training_Part_Processes__c(Name = 'OnsiteOnly', Process_Path__c = 3);
		Product2 prodFullLp = new Product2(Name='Test FullLp', ProductCode = 'FullLp');
		Product2 prodPostOnly = new Product2(Name='Test PostOnly', ProductCode = 'PostOnly');
		Product2 prodOnsiteOnly = new Product2(Name='Test OnsiteOnly', ProductCode = 'OnsiteOnly');
		Customer_Onboard__c cobo = new Customer_Onboard__c();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert new List<SObject>{prodFullLp, prodPostOnly, prodOnsiteOnly,tppFullLp,tppPostOnly,tppOnsiteOnly,cobo,a};


		Contact c1 = new Contact(AccountId = a.Id, FirstName = 'John', LastName = 'Doe');
		Contact c2 = new Contact(AccountId = a.Id, FirstName = 'New', LastName = 'Learner');
		
        insert new List<SObject>{c1, c2};


		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(OnBoard_Contact__c = c1.Id, Customer_Onboard__c = cobo.Id, 
		Role__c = 'Primary Learner');

		Id trainingRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId();
		Id srRecType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId();

		Case caseOBFullLp = new Case(ContactId = c1.Id, Subject = 'Test FullLp', 
		Description = 'Test FullLp', Training_Part__c = prodFullLp.Id, Type = 'On-Boarding', 
		Customer_Onboard__c = cobo.Id, Status = 'New', RecordTypeId = trainingRecType);
		Case caseOBPostOnly = new Case(ContactId = c1.Id, Subject = 'Test PostOnly', 
		Description = 'Test PostOnly', Training_Part__c = prodPostOnly.Id, Type = 'On-Boarding', 
		Customer_Onboard__c = cobo.Id, Status = 'New', RecordTypeId = trainingRecType);
		Case caseOBOnsiteOnly = new Case(ContactId = c1.Id, Subject = 'Test OnsiteOnly', 
		Description = 'Test OnsiteOnly', Training_Part__c = prodOnsiteOnly.Id, Type = 'On-Boarding', 
		Customer_Onboard__c = cobo.Id, Status = 'New', RecordTypeId = trainingRecType);
		Case caseSAFullLp = new Case(ContactId = c1.Id, Subject = 'Test FullLp', 
		Description = 'Test FullLp', Training_Part__c = prodFullLp.Id, Type = 'Standalone', 
		Status = 'New', RecordTypeId = trainingRecType);
		Case caseSAPostOnly = new Case(ContactId = c1.Id, Subject = 'Test PostOnly', 
		Description = 'Test PostOnly', Training_Part__c = prodPostOnly.Id, Type = 'Standalone', 
		Status = 'New', RecordTypeId = trainingRecType);
		Case caseSAOnsiteOnly = new Case(ContactId = c1.Id, Subject = 'Test OnsiteOnly', 
		Description = 'Test OnsiteOnly', Training_Part__c = prodOnsiteOnly.Id, Type = 'Standalone', 
		Status = 'New', RecordTypeId = trainingRecType);
		Case serviceReqCase = new Case(ContactId = c1.Id, Subject = 'Test Service Case', 
		Description = 'Test Service Case', RecordTypeId = srRecType, Install_Start_DateTime__c = System.now(), Status = 'Closed');
		insert new List<SObject>{caseOBFullLp, caseOBPostOnly, caseOBOnsiteOnly, 
		caseSAFullLp, caseSAPostOnly, caseSAOnsiteOnly, serviceReqCase, obc};

		Task t1 = new Task(WhatId = caseOBFullLp.Id, Training_Task_Type__c = 'Schedule Pre Install', Type = 'Training');
		Task t2 = new Task(WhatId = caseOBFullLp.Id, Training_Task_Type__c = 'Complete Pre Install', Type = 'Training');
		Task t3 = new Task(WhatId = caseOBFullLp.Id, Training_Task_Type__c = 'Schedule onsite', Type = 'Training');
		Task t4 = new Task(WhatId = caseOBFullLp.Id, Training_Task_Type__c = 'Perform Instructor Led', Type = 'Training');
		Task t5 = new Task(WhatId = caseOBFullLp.Id, Training_Task_Type__c = 'Schedule Post-Training', Type = 'Training');
		Task t6 = new Task(WhatId = caseOBFullLp.Id, Training_Task_Type__c = 'Complete Post-Training', Type = 'Training');
		Task t7 = new Task(WhatId = caseSAFullLp.Id, Training_Task_Type__c = 'Schedule Pre Install', Type = 'Training');
		Task t13 = new Task(WhatId = caseSAOnsiteOnly.Id, Training_Task_Type__c = 'Perform Instructor Led', Type = 'Training');

		insert new List<SObject>{t1,t2,t3,t4,t5,t6,t7,t13};

		Task tc1 = new Task(Id = t1.Id, Scheduled_Start_DateTime__c = System.now(), Scheduled_End_DateTime__c = System.now().addHours(1), Status = 'Completed');
		Task tc7 = new Task(Id = t7.Id, Scheduled_Start_DateTime__c = System.now(), Scheduled_End_DateTime__c = System.now().addHours(1), Status = 'Completed');

		update new List<SObject>{tc1,tc7};

		Test.startTest();
		List<Case> cases1 = [SELECT Pre_Install_Webex_Scheduled_Date__c, (SELECT Id FROM Tasks WHERE Training_Task_Type__c = 'Complete Pre Install') FROM Case WHERE Id = :t1.WhatId OR Id = :t7.WhatId];
		Task newSATask;
		for(Case c : cases1)
		{
			if(c.Id == t1.WhatId)
			{
				System.assertEquals(tc1.Scheduled_Start_DateTime__c, c.Pre_Install_Webex_Scheduled_Date__c);
			} else 
			{
				System.assertEquals(tc7.Scheduled_Start_DateTime__c, c.Pre_Install_Webex_Scheduled_Date__c);
				System.assertEquals(c.Tasks.size(), 1);
				newSATask = c.Tasks[0];
			}
		}

		Task tc2 = new Task(Id = t2.Id, Status = 'Completed');
		newSATask.Status = 'Completed';

		update new List<Task>{tc2, newSATask};

		List<Case> cases2 = [SELECT Pre_Install_Webex_Completion_Date__c, Status, (SELECT Id FROM Tasks WHERE Training_Task_Type__c = 'Schedule onsite') 
		FROM Case WHERE Id = :t1.WhatId OR Id = :t7.WhatId];

		for(Case c : cases2)
		{
			if(c.Id == t1.WhatId)
			{
				System.assertEquals(Date.today(), c.Pre_Install_Webex_Completion_Date__c);
			} else 
			{
				System.assertEquals(Date.today(), c.Pre_Install_Webex_Completion_Date__c);
				System.assertEquals(c.Tasks.size(), 1);
				newSATask = c.Tasks[0];
			}
		}

		Task tc3 = new Task(Id = t3.Id, Scheduled_Start_DateTime__c = System.now(), Scheduled_End_DateTime__c = System.now().addHours(1), Status = 'Completed');
		newSATask.Status = 'Completed';
		newSATask.Scheduled_Start_DateTime__c = System.now();
		newSATask.Scheduled_End_DateTime__c = System.now().addHours(1);
		update new List<Task>{tc3, newSATask};

		List<Case> cases3 = [SELECT Scheduled_Training_Date__c, (SELECT Id FROM Tasks WHERE Training_Task_Type__c = 'Perform Instructor Led') 
		FROM Case WHERE Id = :t1.WhatId OR Id = :t7.WhatId];

		for(Case c : cases3)
		{
			if(c.Id == t1.WhatId)
			{
				System.assertEquals(Date.today(), c.Scheduled_Training_Date__c);
			} else 
			{
				System.assertEquals(Date.today(), c.Scheduled_Training_Date__c);
				System.assertEquals(c.Tasks.size(), 1);
				newSATask = c.Tasks[0];
			}
		}

		Task tc4 = new Task(Id = t4.Id, Status = 'Completed');
		Task tc13 = new Task(Id = t13.Id, Status = 'Completed');
		newSATask.Status = 'Completed';
		update new List<Task>{tc4, tc13, newSATask};

		List<Case> cases4 = [SELECT Actual_Onsite_Training_Date__c, Status, (SELECT Id FROM Tasks WHERE Training_Task_Type__c = 'Schedule Post-Training') 
		FROM Case WHERE Id = :t1.WhatId OR Id = :t7.WhatId OR Id = :t13.WhatId];

		for(Case c : cases4)
		{
			if(c.Id == t1.WhatId)
			{
				System.assertEquals(Date.today(), c.Actual_Onsite_Training_Date__c);
			} else if (c.Id == t13.WhatId)
			{
				System.assertEquals(Date.today(), c.Actual_Onsite_Training_Date__c);
				System.assertEquals('Closed', c.Status);
			} else
			{
				System.assertEquals(Date.today(), c.Actual_Onsite_Training_Date__c);
				System.assertEquals(c.Tasks.size(), 1);
				newSATask = c.Tasks[0];
			}
		}

		Task tc5 = new Task(Id = t5.Id, Scheduled_Start_DateTime__c = System.now(), Scheduled_End_DateTime__c = System.now().addHours(1), Status = 'Completed');
		newSATask.Status = 'Completed';
		newSATask.Scheduled_Start_DateTime__c = System.now();
		newSATask.Scheduled_End_DateTime__c = System.now().addHours(1);
		update new List<Task>{tc5, newSATask};

		List<Case> cases5 = [SELECT Post_OnSite_Webex_Scheduled_Date__c, (SELECT Id FROM Tasks WHERE Training_Task_Type__c = 'Complete Post-Training') 
		FROM Case WHERE Id = :t1.WhatId OR Id = :t7.WhatId];

		for(Case c : cases5)
		{
			if(c.Id == t1.WhatId)
			{
				System.assertEquals(tc5.Scheduled_Start_DateTime__c, c.Post_OnSite_Webex_Scheduled_Date__c);
			} else 
			{
				System.assertEquals(newSATask.Scheduled_Start_DateTime__c, c.Post_OnSite_Webex_Scheduled_Date__c);
				System.assertEquals(c.Tasks.size(), 1);
				newSATask = c.Tasks[0];
			}
		}

		List<Task> nonCompleteTasks = [SELECT Id, Status FROM Task WHERE Type = 'Training' AND Status != 'Completed'];

		for(Task t : nonCompleteTasks)
		{
			t.Status = 'Completed';
			t.Scheduled_Start_DateTime__c = System.now(); 
			t.Scheduled_End_DateTime__c = System.now().addHours(1);
		}
		update nonCompleteTasks;

		Case cases6 = [SELECT Post_OnSite_Webex_Actual_Date__c, Status
		FROM Case WHERE Id = :t1.WhatId LIMIT 1];

		System.assertEquals(Date.today(), cases6.Post_OnSite_Webex_Actual_Date__c);
		System.assertEquals('Closed', cases6.Status);
		Test.stopTest();
	 }

	@IsTest static void test_CTICaseChanged()
	{
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;
		Contact c = new Contact(AccountId = a.Id, FirstName = 'John', LastName = 'Doe');
		insert c;
		Case cs = new Case(Subject = 'TestCase', Description = 'TestDescription', ContactId = c.Id);
		Case cs2 = new Case(Subject = 'TestCase2', Description = 'TestDescription', ContactId = c.Id);
		insert new List<Case>{cs,cs2};
		Task t = new Task(Subject = 'TestCall1', WhatId = cs.Id, cnx__CTIInfo__c = 'Test');
		insert t;
		t.WhatId = cs2.Id;
		update t;
		Task tRes = [SELECT CTI_What_Id_Changed__c FROM Task WHERE Id = :t.Id];
		System.Assert(tRes.CTI_What_Id_Changed__c);
	}

	@IsTest static void test_setCallStartTime()
	{
		Task t = new Task(Subject = 'TestCall1', cnx__CTIInfo__c = 'Test');
		insert t;
		System.assertNotEquals(null, [SELECT Call_Start_Time__c FROM Task WHERE Id = :t.Id][0].Call_Start_Time__c);
	}

	@IsTest static void test_setCallEndTime()
	{
		Task t = new Task(Subject = 'TestCall1', cnx__CTIInfo__c = 'Test');
		insert t;
		t.CallDurationInSeconds = 10;
		update t;
		System.assertNotEquals(null, [SELECT Call_End_Time__c FROM Task WHERE Id = :t.Id][0].Call_End_Time__c);
	}
    
	@IsTest static void test_warmTransferred()
	{
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;
		Contact c = new Contact(AccountId = a.Id, FirstName = 'John', LastName = 'Doe');
		insert c;
		Case cs = new Case(Subject = 'TestCase', Description = 'TestDescription', ContactId = c.Id);
		Case cs2 = new Case(Subject = 'TestCase2', Description = 'TestDescription', ContactId = c.Id);
		Case cs3 = new Case(Subject = 'TestCase3', Description = 'TestDescription', ContactId = c.Id);
		insert new List<Case>{cs,cs2,cs3};
		Task t = new Task(Subject = 'TestCall1', WhatId = cs.Id, cnx__CTIInfo__c = 'Test', Call_Start_Time__c = System.now(), Call_End_Time__c = System.now().addMinutes(2));
		Task t4 = new Task(Subject = 'TestCall4', WhatId = cs2.Id, cnx__CTIInfo__c = 'Test', Call_Start_Time__c = System.now(), Call_End_Time__c = System.now().addMinutes(2));
		Task t5 = new Task(Subject = 'TestCall5', WhatId = cs3.Id, cnx__CTIInfo__c = 'Test', Call_Start_Time__c = System.now(), Call_End_Time__c = System.now().addMinutes(2));
		
		insert new List<Task>{t,t4,t5};
		//now we need to insert a task as a technical user
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test', SCIEXNow_Role__c = 'TAC');
        insert u;
		
		System.runAs(u) {
			Task t2 = new Task(Subject = 'TestCall2', WhatId = cs.Id, cnx__CTIInfo__c = 'Test', Call_Start_Time__c = System.now().addMinutes(1), Call_End_Time__c = System.now().addMinutes(3));
			Task t3 = new Task(Subject = 'TestCall3', CallDurationInSeconds = 1, cnx__CTIInfo__c = 'Test', Call_Start_Time__c = System.now().addMinutes(1), Call_End_Time__c = System.now().addMinutes(3));
			Task t6 = new Task(Subject = 'TestCall6', WhatId = cs3.Id, CallDurationInSeconds = 1, cnx__CTIInfo__c = 'Test', Call_Start_Time__c = System.now().addMinutes(1), 
			Call_End_Time__c = System.now().addMinutes(3));
			insert new List<Task>{t2, t3,t6};
		}

		//change the call duration to hit the update trigger
		Task t2 = [SELECT Id, Call_Start_Time__c FROM Task WHERE Subject = 'TestCall2'];
		t2.CallDurationInSeconds = 1;
		update t2;
		cs = [SELECT Id, First_Contact_Date__c, Warm_Transferred__c FROM Case WHERE Id = :cs.Id];
		System.assertNotEquals(null, cs.First_Contact_Date__c);
		System.assert(cs.Warm_Transferred__c);

		//now test re-evaluation if the WhatId is changed
		Task t3 = [SELECT Id, Call_Start_Time__c FROM Task WHERE Subject = 'TestCall3'];
		t3.WhatId = cs2.Id;
		update t3;
		cs2 = [SELECT Id, First_Contact_Date__c, Warm_Transferred__c FROM Case WHERE Id = :cs2.Id];
		System.assertNotEquals(null, cs2.First_Contact_Date__c);
		System.assert(cs2.Warm_Transferred__c);

		//and now test the insert end of the trigger
		Task t6 = [SELECT Id, Call_Start_Time__c FROM Task WHERE Subject = 'TestCall6'];
		cs3 = [SELECT Id, First_Contact_Date__c, Warm_Transferred__c FROM Case WHERE Id = :cs3.Id];
		System.assertNotEquals(null, cs3.First_Contact_Date__c);
		System.assert(cs3.Warm_Transferred__c);

	}
   
   @isTest static void test_setFirstActivityDate()
   {
		Case c = new Case(Subject = 'TestCaseSubject', Description = 'TestCaseDescription',
		RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId());
		insert c;

		Task t = new Task(Subject = 'TestTaskSubject', WhatId = c.Id, Training_Task_Type__c = 'Schedule Pre Install');
		insert t;

		Test.startTest();
		t.Status = 'Completed';
		update t;
		Test.stopTest();
		
		c = [SELECT First_Training_Activity_Completed__c FROM Case WHERE Id = :c.Id];

		System.assertNotEquals(null, c.First_Training_Activity_Completed__c);
   }

}