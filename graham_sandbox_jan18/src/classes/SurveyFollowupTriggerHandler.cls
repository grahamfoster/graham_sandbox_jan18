/*
 *	SurveyFollowupTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2017-03-20 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class SurveyFollowupTriggerHandler extends TriggerHandler {
    

	public SurveyFollowupTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void beforeInsert() {
		assignFollowUp();
	}
	override protected void beforeUpdate() {
	}
    override protected void afterInsert() {
		collectQuestions(); // link to all related Survey Results
        rollupResults();
	}
	override protected void afterUpdate() {

	}
	override protected void afterUndelete() {
    
	}
	override protected void afterDelete() {
    
	}

	// **************************************************************************
	// 		private methods
	// **************************************************************************
	/* 
	 * Copied from original WorkOrderTriggerHandler written by Reid Beckette
	*/       
    public void collectQuestions(){
        // Loop through all records and collect originating Case IDs
        List <String> responseIds = new List <String>();
        For( Survey_Follow_Up__c sfu : (List<Survey_Follow_Up__c>)Trigger.new){
            responseIds.add(sfu.ResponseID__c);            
        }
        // Query all needed Survey Responses
        Map <Id, Qualtrics_Survey_Result__c> results = new Map <Id,Qualtrics_Survey_Result__c>([Select id, Response_Id__c, Survey_Follow_Up__c, Response_Recode_Number__c FROM Qualtrics_Survey_Result__c WHERE Response_Id__c IN :responseIds]);
		        
		// Loop through all records and Link Responses to FollowUp record
        For( Survey_Follow_Up__c sfu : (List<Survey_Follow_Up__c>)Trigger.new){
            for( Qualtrics_Survey_Result__c response :results.values() ) {
                if(response.Response_Id__c == sfu.ResponseID__c){
                    response.Survey_Follow_Up__c = sfu.id;
                }
            }
        }  
        if(results.size() > 0){
            update results.values();
        }
    }   

    public void rollupResults(){    
    	
        List<Survey_Follow_Up__c> unrolled = [SELECT id,Highest_Score__c,Lowest_Score__c,ResponseID__c FROM Survey_Follow_Up__c WHERE LastModifiedDate = THIS_WEEK ];
        List<Qualtrics_Survey_Result__c> results = [Select id, Response_Id__c, Survey_Follow_Up__c, Response_Recode_Number__c FROM Qualtrics_Survey_Result__c WHERE Survey_Follow_Up__c IN :unrolled];
        // Loop through all records and Calculat high/low score
        For( Survey_Follow_Up__c sfu : unrolled){
            Double high = 0;
            Double low = 99;
            for( Qualtrics_Survey_Result__c response :results) {
                if(response.Response_Id__c == sfu.ResponseID__c){
                    response.Survey_Follow_Up__c = sfu.id;
system.debug('!!!   High / Low : ' + high + ' / ' + low);
                    if(response.Response_Recode_Number__c != null && response.Response_Recode_Number__c < low) low = response.Response_Recode_Number__c;
                    if(response.Response_Recode_Number__c != null && response.Response_Recode_Number__c > high) high = response.Response_Recode_Number__c;
                }
            }
system.debug('!!! Final High / Low : ' + high + ' / ' + low);            
            if(high > 0) sfu.Highest_Score__c = high;
            if(low < 10) sfu.Lowest_Score__c = low;
                   }  
        if(unrolled.size() > 0){
            update unrolled;
        }
    
    }


	/*****************************************************************************************************
	* @description looks through the new objects and finds High or Medium Priority ones which need to 
	* be assigned to people for follow up
	*/
	public void assignFollowUp()
	{
		Set<Id> followUpCases = new Set<Id>();
		Set<Id> followUpContacts = new Set<Id>();

		Map<Id, Case> caseDetails;
		Map<Id, Contact> contactDetails;

		for(Survey_Follow_Up__c sfu : (List<Survey_Follow_Up__c>)Trigger.new)
		{
			if(!String.isEmpty(sfu.Case__c))
			{
				followUpCases.add(sfu.Case__c);
			} else if(!String.isEmpty(sfu.Contact__c))
			{
				followUpContacts.add(sfu.Contact__c);
			}
		}

		if(followUpCases.size() > 0)
		{
			caseDetails = new Map<Id, Case>([SELECT Account.Country_Mapping__r.SAS_Follow_Up_User__c, 
			Account.Country_Mapping__r.TAC_Follow_Up_User__c, Account.Country_Mapping__r.Training_Follow_Up_User__c, 
			Account.Country_Mapping__r.Portal_Follow_Up_User__c, Account.Country_Mapping__r.Software_Activation_Follow_Up_User__c,
			Account.Country_Mapping__r.Compliance_Survey_Follow_Up_User__c, Account.Country_Mapping__r.CE_Training_Follow_Up_User__c,
			Type FROM Case WHERE Id IN :followUpCases]);
		}

		if(followUpContacts.size() > 0)
		{
			contactDetails = new Map<Id, Contact>([SELECT Account.Country_Mapping__r.SAS_Follow_Up_User__c, 
			Account.Country_Mapping__r.TAC_Follow_Up_User__c, Account.Country_Mapping__r.Training_Follow_Up_User__c, 
			Account.Country_Mapping__r.Portal_Follow_Up_User__c, Account.Country_Mapping__r.Software_Activation_Follow_Up_User__c,
			Account.Country_Mapping__r.Compliance_Survey_Follow_Up_User__c, Account.Country_Mapping__r.CE_Training_Follow_Up_User__c
			FROM Contact WHERE Id IN :followUpContacts]);
		}



		for(Survey_Follow_Up__c sfu : (List<Survey_Follow_Up__c>)Trigger.new)
		{
			if(!String.isEmpty(sfu.Case__c))
			{
				//get the case from the map
				Case assignDetails = caseDetails.get(sfu.Case__c);
				if(sfu.Survey_Title__c == 'SCIEXNow: Post Case')
					{
						if(assignDetails.Type == 'TAC')
						{
							if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.TAC_Follow_Up_User__c))
							{
								sfu.OwnerId = assignDetails.Account.Country_Mapping__r.TAC_Follow_Up_User__c;
							}
							
						}
						if(assignDetails.Type == 'SAS')
						{
							if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.SAS_Follow_Up_User__c))
							{
								sfu.OwnerId = assignDetails.Account.Country_Mapping__r.SAS_Follow_Up_User__c;
							}
						}
					} else if (sfu.Survey_Title__c.contains('SCIEXNow: Portal'))
					{
						if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.Portal_Follow_Up_User__c))
						{
							sfu.OwnerId = assignDetails.Account.Country_Mapping__r.Portal_Follow_Up_User__c;
						}
					} else if (sfu.Survey_Title__c == 'Global Post FAS Training Survey')
					{
						if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.Training_Follow_Up_User__c))
						{
							sfu.OwnerId = assignDetails.Account.Country_Mapping__r.Training_Follow_Up_User__c;
						}
					} else if (sfu.Survey_Title__c == 'Customer Training Separations')
					{
						if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.CE_Training_Follow_Up_User__c))
						{
							sfu.OwnerId = assignDetails.Account.Country_Mapping__r.CE_Training_Follow_Up_User__c;
						}
					}
			} else if(!String.isEmpty(sfu.Contact__c))
			{
				//must be a software activation or compliance survey as they dont have 
				//cases so we use the contact data

				//get the contact details
				Contact assignDetails = contactDetails.get(sfu.Contact__c);
				if (sfu.Survey_Title__c == 'Software Product Activation 90days')
				{
					if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.Software_Activation_Follow_Up_User__c))
					{
						sfu.OwnerId = assignDetails.Account.Country_Mapping__r.Software_Activation_Follow_Up_User__c;
					}
				} else if (sfu.Survey_Title__c == 'Compliance Services')
				{
					if(!String.isEmpty(assignDetails.Account.Country_Mapping__r.Compliance_Survey_Follow_Up_User__c))
					{
						sfu.OwnerId = assignDetails.Account.Country_Mapping__r.Compliance_Survey_Follow_Up_User__c;
					}
				}
			}
			
		}

	}
    
}