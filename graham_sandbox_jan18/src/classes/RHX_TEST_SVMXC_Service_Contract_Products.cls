@isTest(SeeAllData=true)
public class RHX_TEST_SVMXC_Service_Contract_Products {
    static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
            FROM SVMXC__Service_Contract_Products__c LIMIT 1];
        if(sourceList.size() == 0) {
            
         SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c();
         insert contract;
            
         sourceList.add(new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c=contract.ID));
        }
        rh2.ParentUtil.UpsertRollupTestRecords(sourceList);
    }
}