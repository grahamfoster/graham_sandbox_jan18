@isTest
private class BattlecardManageCtrl_UnitTest {
    static testMethod void runTestMethod(){
        Application__c application = Util.createApplication();
        Application_Variants__c applicationvar = Util.createAppVariants(application.Id);
        Persona__c persona =  Util.createPersona();
        Manufacturer__c manufacturer =  Util.createManufacturer();
        Manufacturer__c manufacturerABX =  Util.createManufacturerABX();
        Product_Category__c prodCatObj =  Util.CreateProductCategory();
        Product2 abxPorduct = util.createProduct2(manufacturerABX.Id, prodCatObj.Id);
        Product2 comPorduct = util.createProduct2(manufacturer.Id, prodCatObj.Id);
        Package__c pkgObj =  Util.CreatePackage(application.Id, applicationvar.Id, persona.Id, abxPorduct.Id, comPorduct.Id);
        Battlecard__c cardObj = Util.createBattlecard(pkgObj.Id);
        Positioning__c posObj = Util.createPositioning(cardObj.Id);
        Demo_Strategy__c demoObj =  Util.createDemoStrategy(cardObj.Id);
        Resource_Category__c resCat = Util.createResCategory(cardObj.Id);
        Additional_Resource__c resObj = Util.createAddresource(cardObj.Id, resCat.Id);
        Summary__c sumobj = Util.createSummary(cardObj.Id);
        Rating_Management__c ratingObj = Util.createRatingManagement(pkgObj.Id, UserInfo.getUserId());
        
        BattlecardManage_Ctrl battleObj = new BattlecardManage_Ctrl();
         
        BattlecardManage_Ctrl.getCompetitorProductCount(prodCatObj.Id);
        BattlecardManage_Ctrl.getAppVariantCount();
        ApexPages.currentPage().getParameters().put('id', pkgObj.Id);
        BattlecardManage_Ctrl battleCardObj = new BattlecardManage_Ctrl();
        battleCardObj.getABSciexProductCount();
        battleCardObj.getListOfCat();
        battleCardObj.getListOfPersona();
        battleCardObj.getListOfApplication();
        battleCardObj.getListOfApplicationvarient();
        battleCardObj.redirecToMainAdmin();
        battleCardObj.selectedCat = prodCatObj.Id;
        battleCardObj.ChangevaluesOnCat();
        battleCardObj.comManufacselect = manufacturer.Id;
        battleCardObj.ChangevaluesComManuFac();
        battleCardObj.validatePosition();
        battleCardObj.AddressingNeed = 'test';
        battleCardObj.makeListOfAll();
        battleCardObj.selPositionId = 1;
        battleCardObj.makeListOfAll(); 
        battleCardObj.ResourceCat = 'test';
        battleCardObj.addUpdateResourceCategory();
        battleCardObj.resname =  'test';
        battleCardObj.addUpdateResourceCategory();
        battleCardObj.selResouceId = 1;
        battleCardObj.updateAddResource();
        battleCardObj.makeSummaryList();
        battleCardObj.Summary = 'test';
        battleCardObj.makeSummaryList();
        battleCardObj.selSummaryId = 1;
        battleCardObj.makeSummaryList();
        
        battleCardObj.SavePackageBattlecard();
        battleCardObj.selectedProdName = abxPorduct.name;
        battleCardObj.selectedProdId = abxPorduct.Id;
        battleCardObj.selectedComProdName = comPorduct.Name;
        battleCardObj.selectedComProdId = comPorduct.Id; 
        battleCardObj.selectedAppName = applicationvar.Id;
        battleCardObj.selectedAppId = applicationvar.Id;
        battleCardObj.selectedPersonaId = persona.Id;
        battleCardObj.packageName = pkgObj.Id;
        battleCardObj.SavePackageBattlecard();
        
        battleCardObj.GOToBattle();
        battleCardObj.CancelPackageBattlecard();
        battleCardObj.getPersonaItems();
        battleCardObj.delSelPackage();
        
        ApexPages.currentPage().getParameters().put('positionIndex', '1'); 
        battleCardObj.removePosition(); 
        ApexPages.currentPage().getParameters().put('positionId', posObj.Id);
        battleCardObj.removePosition(); 
     
        ApexPages.currentPage().getParameters().put('resIndex', '1'); 
        battleCardObj.removeResource();
        ApexPages.currentPage().getParameters().put('resId', resObj.Id);
        battleCardObj.removeResource();
       
        String rowIndex = ApexPages.currentPage().getParameters().put('summaryIndex', '1');  
        battleCardObj.removeSummary();
        String recordId = ApexPages.currentPage().getParameters().put('summaryId', sumobj.Id);
        battleCardObj.removeSummary();
       
        battleCardObj.removeResCategory();
        battleCardObj.hiddenpackageId = pkgObj.Id;
        battleCardObj.delChildObjectsOfPackage(pkgObj.Id);
        battleCardObj.delSelPackage();
        
    }
}