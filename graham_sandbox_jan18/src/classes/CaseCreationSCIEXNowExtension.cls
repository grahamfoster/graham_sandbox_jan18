/*
 *Extension for Visualforce Page CaseCreationSCIEXNow
 *
 *Creates a new case for SCIEXNow and adds a new contact linked to asset if the
 *case contact doesn not already have the asset registered
 */
public with sharing class CaseCreationSCIEXNowExtension {
    
    public Case newCase {get; set;}
    ApexPages.standardController m_sc = null;
    private String conId = null; 
    public Contact cont {get;set;}
    
    public Boolean newAsset
    {
        get 
        {
        	return selectedAsset == 'Other'; 	    
        }
    }
    
    public String selectedAsset {get; set;}
    
    public String phoneOverwrite {get;set;}
    
    private Set<Id> assetIdSet {get; set;}
    
    public CaseCreationSCIEXNowExtension(ApexPages.StandardController controller)
    {
        m_sc = controller;
        conId = ApexPages.currentPage().getParameters().get('cid');
        cont = [SELECT FirstName, LastName, Email, Account.Name, Phone, MobilePhone 
                            FROM Contact WHERE Id = :conId LIMIT 1];
        assetIdSet = new Set<Id>();
        RecordType rt = SfdcUtil.getRecordType('Case', 'CICCase');
        selectedAsset = 'Other';
        newCase = new Case(RecordTypeId = rt.Id, ContactId = conId);    
    }
    /*****************************************************************************************************
	* @description get the details for the ContactId passed to the vf page
	* @return name variable to be used on the new case form
	* Author - Graham Foster 2017-06-22
	*/
    public String getContactName()
    {
        String n = cont.FirstName + ' ' + cont.LastName;
        return n;
    }
    /*****************************************************************************************************
	* @description get a list of the contacts linked to asset that are registered to this contact
	* @return List of select options to be used in the picklist
	* Author - Graham Foster 2017-06-22
	*/
    public List<SelectOption> getItems() {
        	List<Asset_MultipleContacts__c> assets = [SELECT Asset__c, Asset__r.Name 
                                                      FROM Asset_MultipleContacts__c WHERE Contact__r.Email = :cont.Email];
            List<SelectOption> options = new List<SelectOption>();
        	options.add(new SelectOption('Other','Other'));
                for(Asset_MultipleContacts__c a : assets)
            	{
                    system.debug('**GF Asset Added' + a.Asset__c);
                	options.add(new SelectOption(a.Asset__c, a.Asset__r.Name));
                    assetIdSet.add(a.Asset__c);
                }
            return options;
        }
    
    /*****************************************************************************************************
	* @description select list for the update phone number picklist
	* Author - Graham Foster 2017-06-22
	*/
    public List<SelectOption> getPhoneFields()
    {
        List<SelectOption> overOptions = new List<SelectOption>();
        overOptions.add(new SelectOption('No','No'));
        overOptions.add(new SelectOption('MobilePhone','Mobile Phone'));
        overOptions.add(new SelectOption('Phone','Phone'));
        return overOptions;
    }
    
    /*****************************************************************************************************
	* @description method to change the WhatId on the currently active phone call to the newly
	* created case
	* Author - Graham Foster 2017-06-22
	*/
    private void currentCall(Id caseId)
    {
        Task updateTask = CTIUtil.CurrentCallTask();

        if(updateTask != null)
        {
            updateTask.WhatId = caseId;
			update updateTask;
            if(phoneOverwrite != 'No' && !String.isBlank(updateTask.cnx__CTIInfo__c))
            {
                system.debug('**GF** PhoneOption:' + phoneOverwrite);
                system.debug('**GF** PhoneANI:' + updateTask.cnx__CTIInfo__c);
                updateContactPhone(phoneOverwrite, updateTask);
            }
        }
    }
    
    /*****************************************************************************************************
	* @description method to update the phone number of the contact with the dialed number from the
	* cisco integration
	* Author - Graham Foster 2017-06-22
	*/
    private void updateContactPhone(String fieldName, Task ctiTask)
    {
        Contact c = new Contact(Id = cont.Id);
        String phoneNumber = CTIUtil.callNumber(ctiTask);
		if(!String.isEmpty(phoneNumber))
		{
			c.put(fieldName, phoneNumber);
			update c;
		}   
    }
    
    
    /*****************************************************************************************************
	* @description method executed when save button is pressed on the visualforce page
	* Author - Graham Foster 2017-06-22
	*/
    public PageReference save()
    {
        //check if its a new asset (one not already linked to the contact)
        if(!newAsset)
        {
            //if its not (so its been selected in the picklist) then set the asset Id 
            //on the case and insert it
            newCase.AssetId = selectedAsset;
            insert newCase;
        } else 
        {
        	//if the contact is not already linked to the asset
            //create a new contact linked to asset and insert this as the case
            system.debug('**GF Asset' + newCase.AssetId);
            system.debug('**GF AssetIdSet' + assetIdSet);
        	if(!String.isEmpty(newCase.AssetId) && !assetIdSet.contains(newCase.AssetId))
            {
               	Asset_MultipleContacts__c am = new Asset_MultipleContacts__c(Asset__c = newCase.AssetId, Contact__c = conId, ContactRole__c = 'Owner');
            	insert new List<Sobject>{newCase, am};
            }else {
                insert newCase;
            }
            
        }
        currentCall(newCase.Id);
        PageReference reRend = new PageReference('/' + newCase.Id);
        reRend.setRedirect(true);
        return reRend;
    }
    
    /*****************************************************************************************************
	* @description method executed when cancel button is pressed on the visualforce page
	* Author - Graham Foster 2017-06-22
	*/
    public PageReference cancel()
    {
     	return m_sc.cancel();
    }
    
    

}