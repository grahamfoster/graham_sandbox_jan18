public with sharing class QuoteReportUtils {
   // public static final String STANDARD_QUOTING_REPORT = '/00O40000001vcbk';
    public static final String STANDARD_QUOTING_REPORT = '/00O20000002Ah7O';
    
    /*
    public static String[] getSoldToList(){
    	System.debug('QuoteReportUtils.getSoldToList() is fired...');
        Map<Integer,String> soldtos = new Map<Integer,String>();

        //List<User> directReports = getDirectReports(UserInfo.getUserId());
        //for(Account acct : [Select a.Account_Sold_To__c From Account a Where a.Account_Sold_To__c != NULL  
        //	and (a.OwnerId = :UserInfo.getUserId() or a.OwnerId in :directReports) Limit 1000]){
        Integer i = 0;
        for(Account acct : [Select a.Account_Sold_To__c From Account a Where a.Account_Sold_To__c != NULL Limit 400]){
        	soldtos.put(i++, acct.Account_Sold_To__c);
        }
        System.debug('==============> Sold-To Count: ' + soldtos.size());
        		
        return soldtos.values();
    }
    */
    
    public static void deleteReportData(){
    	System.debug('QuoteReportUtils.deleteReportData() is fired...');
    	try{
    		Boolean isCompleted = false;
    		while(isCompleted == false){
    			for(List<QuoteReportData__c> data : [Select q.Id From QuoteReportData__c q Where q.UserId__c = :UserInfo.getUserId() Limit 800]){
    				if(data != null && data.size() > 0){
						delete data;        		
        			}else{
        				isCompleted = true;
    				}
        		}
    		}
    	}catch(DmlException dmle){
    		System.debug(dmle);
    	}
    }
    
    public static void deleteAll(){
    	try{
    		Boolean isCompleted = false;
    		while(isCompleted == false){
    			for(List<QuoteReportData__c> data : [Select q.Id From QuoteReportData__c q Limit 800]){
    				if(data != null && data.size() > 0){
						delete data;        		
        			}else{
        				isCompleted = true;
    				}
        		}
    		}
    	}catch(DmlException dmle){
    		System.debug(dmle);
    	}
    }
    
    /**
    *	Return: 
    *	0 	- Completed
    *	-1 	- Failure
    *	1	- In Progress 
    */
    public static Integer checkProgress(String userId){
    	Integer counter = [Select count() From QuoteReportData__c q Where q.UserId__c = :userId];
    	if(counter > 0){
    		counter = [Select count() From QuoteReportData__c q Where q.UserId__c = :userId and q.Report_Status__c = 'COMPLETE'];
    		if(counter > 0)
    			return 0;
    		else
    			return 1;
    	}else{
    		return -1;
    	}	
    }
    
    public static List<User> getDirectReports(String userid){
    	return [Select u.Id From User u Where u.ManagerId = :userid Limit 800];
    }
}