/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 21/2014
**
** Test coverage of MySFDCController
** 
**/
@isTest
public class MySFDCControllerTests {
    static User serviceUser;

    public static testMethod void test1(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        List<SelectOption> opts = c.runAsList;
        ApexPages.currentPage().getParameters().put('r','CurrentForecast');
        c.initReport();
        c.cancelInlineEdits();
        c.initReport();
        c.saveInlineEdits();
    }

    public static testMethod void test1_service1(){
        setUp();
        system.runAs(serviceUser)
        {
            MySFDCController c = new MySFDCController();
            c.init();
            List<SelectOption> opts = c.runAsList;
            ApexPages.currentPage().getParameters().put('r', ABSciexDashboardFactory.PIPELINE_CHART);
            c.initReport();
        }
    }

    public static testMethod void test1_service2(){
        setUp();
        system.runAs(serviceUser)
        {
            MySFDCController c = new MySFDCController();
            c.init();
            List<SelectOption> opts = c.runAsList;
            ApexPages.currentPage().getParameters().put('r', ABSciexDashboardFactory.CUMULATIVE_QUOTA);
            c.initReport();
        }
    }

    public static testMethod void test1_service3(){
        setUp();
        system.runAs(serviceUser)
        {
            MySFDCController c = new MySFDCController();
            c.init();
            system.debug(c.pipelineChartData);
            system.debug(c.cumulativeChartData);
            system.debug(c.quotaVarianceChartData);

            system.debug(c.monthSummary);
            system.debug(c.quarterSummary);
            system.debug(c.yearSummary);
        }
    }

    public static testMethod void test1_dashboards(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();

        GoogleGaugeChartData chart = c.rowsOfCharts.get(0).get(1);
        chart.getScaledQuota();
        chart.getScaledActual();
        chart.getTicks();
        chart.getRedFrom();
        chart.getRedTo();
        chart.getYellowFrom();
        chart.getYellowTo();
        chart.getGreenFrom();
        chart.getGreenTo();
        chart.getTotalNumberFormat();
        chart.getNumberFormatSuffix();

        GoogleGaugePercentageChartData chart2 = new GoogleGaugePercentageChartData('test');
        chart2.getScaledQuota();
        chart2.getScaledActual();
        chart2.getTicks();
        chart2.getRedFrom();
        chart2.getRedTo();
        chart2.getYellowFrom();
        chart2.getYellowTo();
        chart2.getGreenFrom();
        chart2.getGreenTo();
        chart2.getTotalNumberFormat();
        chart2.getNumberFormatSuffix();
    }

    public static testMethod void test2_report_pager(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','CurrentForecast');
        c.initReport();
        Integer pn = c.reportPager.getPageNumber();
        Integer ps = c.reportPager.getPageSize();
        Integer tn = c.reportPager.getTotalNumberOfRecords();
        Integer np = c.reportPager.getNumberOfPages();
        Boolean hn = c.reportPager.getHasNextPage();
        Boolean hp = c.reportPager.getHasPreviousPage();
        String s1 = c.reportPager.getSortColumn1();
        String s2 = c.reportPager.getSortColumn2();
        String sd1 = c.reportPager.getSortDirection1();
        String sd2 = c.reportPager.getSortDirection2();
    }

    public static testMethod void test2_report1(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','CurrentForecast');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test2_report2(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','NextForecast');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test2_report3(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','CurrentForecastAndUp');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test2_report4(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','NextForecastAndUp');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test2_report5(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','CurrentForecastAdjusted');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test2_report6(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','NextForecastAdjusted');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test2_report7(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r', ABSciexDashboardFactory.FULL_YTD_ACTUALS);
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test3_sorting1(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','CurrentForecast');
        c.initReport();
        c.reportPager.setSortColumn1('Account.Name');
        c.reportPager.setSortDirection1('asc');
        c.reportPager.setSortColumn2('Amount');
        c.reportPager.setSortDirection2('asc');
        c.reportPager.sortAction();
        ApexPages.currentPage().getParameters().put('s1','Account.Name');
        ApexPages.currentPage().getParameters().put('sd1','asc');
        ApexPages.currentPage().getParameters().put('s2','Amount');
        ApexPages.currentPage().getParameters().put('sd2','asc');
        c.initReport();
        c.initReportExport();
    }

    public static testMethod void test4_paging(){
        Test.startTest();
        setUp();
        Test.stopTest();
        MySFDCController c = new MySFDCController();
        c.init();
        ApexPages.currentPage().getParameters().put('r','CurrentForecast');
        c.initReport();
        c.reportPager.setPageSize(1);
        c.initReport();
        c.reportPager.nextAction();
        ApexPages.currentPage().getParameters().put('p','2');
        c.initReport();
        c.reportPager.previousAction();
        ApexPages.currentPage().getParameters().put('p','1');
        c.initReport();
    }

    private static void setUp(){
        User currentUser = MySFDCUtil.findUser(UserInfo.getUserId());
        serviceUser = createUser('testsvc', 'AMERICAS Service Sales Manager');
        serviceUser.License_Department__c = 'Service';
        update serviceUser;
        Period qtr = MySFDCUtil.getCurrentQuarter();
        Period nextQtr = MySFDCUtil.getNextQuarter();

        List<Conversion_Rate__c> crates = new List<Conversion_Rate__c>{
            new Conversion_Rate__c(Region__c = currentUser.License_Region__c, Conversion_Rate__c = 95, Opportunity_Type__c = 'Forecast'),
            new Conversion_Rate__c(Region__c = currentUser.License_Region__c, Conversion_Rate__c = 60, Opportunity_Type__c = 'Upside Existing'),
            new Conversion_Rate__c(Region__c = currentUser.License_Region__c, Conversion_Rate__c = 25, Opportunity_Type__c = 'Upside New')
        };
        insert crates;

        List<Quota__c> quotas = new List<Quota__c>{
            new Quota__c(Fiscal_Year__c = qtr.FiscalYearSettings.Name, Fiscal_Quarter__c = String.valueOf(qtr.Number), Quota__c = 1000000),
            new Quota__c(Fiscal_Year__c = nextQtr.FiscalYearSettings.Name, Fiscal_Quarter__c = String.valueOf(nextQtr.Number), Quota__c = 1000000),
            new Quota__c(OwnerId = serviceUser.Id, Fiscal_Year__c = qtr.FiscalYearSettings.Name, Fiscal_Quarter__c = '1', Quota__c = 1000000),
            new Quota__c(OwnerId = serviceUser.Id, Fiscal_Year__c = nextQtr.FiscalYearSettings.Name, Fiscal_Quarter__c = '2', Quota__c = 1000000),
            new Quota__c(OwnerId = serviceUser.Id, Fiscal_Year__c = qtr.FiscalYearSettings.Name, Fiscal_Quarter__c = '3', Quota__c = 1000000),
            new Quota__c(OwnerId = serviceUser.Id, Fiscal_Year__c = nextQtr.FiscalYearSettings.Name, Fiscal_Quarter__c = '4', Quota__c = 1000000)
        };
        insert quotas;

        Account testAcct = new Account(Name = 'Test Account', BillingCountry = 'United States');
        insert testAcct;

        RecordType serviceRecType = [select Id from RecordType where Name = 'Service' and SobjectType = 'Opportunity'];

        OpportunityStage openStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = false and DefaultProbability >= 50 order by DefaultProbability desc limit 1];

        List<Opportunity> opps = new List<Opportunity>{
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), Shippable_Date__c = Date.today(), StageName = openStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account', Mgr_Upside__c = false, Rep_Upside__c = true,
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, In_Forecast_Mgr__c = true,
                Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), Shippable_Date__c = Date.today(), StageName = openStage.MasterLabel, 
                Customer_Type__c = 'New to Mass Spec', Mgr_Upside__c = false, Rep_Upside__c = true,
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, In_Forecast_Mgr__c = true,
                Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), Shippable_Date__c = Date.today(), StageName = openStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account',
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, In_Forecast_Mgr__c = true,
                Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = nextQtr.StartDate, StageName = openStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account', Mgr_Upside__c = true, Rep_Upside__c = true, Shippable_Date__c = nextQtr.StartDate,
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = nextQtr.StartDate, StageName = openStage.MasterLabel, 
                Customer_Type__c = 'New to Mass Spec', Mgr_Upside__c = true, Rep_Upside__c = true, Shippable_Date__c = nextQtr.StartDate,
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = nextQtr.StartDate, StageName = openStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account',
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, Sales_Manager_Top5__c = true, Regional_Manager_Top5__c = true, Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(RecordTypeId = serviceRecType.Id, OwnerId = serviceUser.Id, AccountId = testAcct.Id, Name = 'Test Svc Opp 1', CloseDate = Date.today(), Shippable_Date__c = Date.today(), StageName = openStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account', Mgr_Upside__c = false, Rep_Upside__c = true,
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, In_Forecast_Mgr__c = true, Market_Vertical__c = 'Academia/Omics')
        };
        insert opps;

        OpportunityStage wonStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = true and IsWon = true order by DefaultProbability desc limit 1];
        List<Opportunity> wonOpps = new List<Opportunity>{
            new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), StageName = wonStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account', Mgr_Upside__c = true, Rep_Upside__c = true, Shippable_Date__c = Date.today(),
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, Market_Vertical__c = 'Academia/Omics'),
            new Opportunity(RecordTypeId = serviceRecType.Id, OwnerId = serviceUser.Id, AccountId = testAcct.Id, Name = 'Test Svc Won Opp 1', CloseDate = Date.today(), StageName = wonStage.MasterLabel, 
                Customer_Type__c = 'Existing AB SCIEX account', Mgr_Upside__c = true, Rep_Upside__c = true, Shippable_Date__c = Date.today(),
                Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', Amount = 100000, Market_Vertical__c = 'Academia/Omics')
        };
        insert wonOpps;

    }

    private static User createUser(String alias, String profileName)
    {
        Profile p = [select Id from Profile where Name = :profileName];
        User u = new User(alias = alias,email=alias+'@example.com',emailencodingkey='UTF-8',lastname=alias, languagelocalekey='en_US',
        localesidkey='en_US',
        profileId = p.Id,
        timezonesidkey='America/New_York',
        username=alias+'@example.com');
        insert u;     
        return [select Id, Name from User where Id = :u.Id];
    }
}