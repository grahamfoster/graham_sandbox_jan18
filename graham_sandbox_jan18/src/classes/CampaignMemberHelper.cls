public without sharing class CampaignMemberHelper {



	public static void updateRollupCountsOnCampaign(List<CampaignMember> newList, List<CampaignMember> oldList, Map<Id, CampaignMember> oldMap, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete)
	{
		
		Set<Id> campaignSet = new Set<Id>();
		Map<Id, Campaign> campaignMap = new Map<Id, Campaign>();
		List<Campaign> updateList = new List<Campaign>();
		Map<Id, Campaign> campaignSaveMap = new Map<Id, Campaign>();
		
		if (/*isInsert ||*/ isUpdate)
		{
			
			for (CampaignMember cm : newList)
			{
				if (cm.CampaignId != null)
				{
					campaignSet.add(cm.CampaignId);
				}
			}
		}
		if (isDelete)
		{
			for (CampaignMember cm : oldList)
			{
				if (cm.CampaignId != null)
				{
					campaignSet.add(cm.CampaignId);
				}
			}
		}
		
			
			List<Campaign> campaignList = 
				[Select Id, Actual_Services_A_E__c, Actual_Other_A_E__c, Actual_Services_ABC__c, Actual_Other_ABC__c, Actual_Triple_TOF_A_E__c, Actual_QTRAP_A_E__c, Actual_EKSIGENT_A_E__c, Actual_TOF_TOF_A_E__c, Actual_Triple_Quad_A_E__c, Actual_SELEXION_A_E__c, 
			 	Actual_Clinical_Forensic_A_E__c, Actual_Food_Env_A_E__c, Actual_Academics_OMICS_A_E__c, Actual_Pharma_CRO_A_E__c,
				Actual_Triple_TOF_A_C__c, Actual_QTRAP_A_C__c, Actual_EKSIGENT_A_C__c, Actual_TOF_TOF_A_C__c, Actual_Triple_Quad_A_C__c, Actual_SELEXION_A_C__c,
				Actual_Clinical_Forensic_A_C__c, Actual_Food_Env_A_C__c, Actual_Academics_OMICS_A_C__c, Actual_Pharma_CRO_A_C__c 
				from Campaign where Id in :campaignSet limit 10000];
			
			if (campaignList != null)
			{
				for (Campaign c : campaignList)
				{
					campaignMap.put(c.Id, c);
				}
			}
			
			if (isDelete)
			{
				for (CampaignMember cm : oldList)
				{
					Campaign c = campaignMap.get(cm.CampaignId);
					updateCampaignCounts(c, cm, cm, -1, false, true);
					
					//System.debug('######@@@  CALLING DELETE!!!');
					if (campaignSaveMap.get(c.Id) == null)
					{
						campaignSaveMap.put(c.Id, c);
						updateList.add(c);
					}
							
				}
			}
			else if (isUpdate)
			{
				for (CampaignMember cm : newList)
				{
					//System.debug('####$$$$WE ARE IN HERE UPDATE:recordcounter: ' + cm.Record_Update_Counter__c);
					
					//System.debug('######## before recordupdate value:' + oldMap.get(cm.Id).Record_Update_Counter__c + ' Now:' + cm.Record_Update_Counter__c);
					if (oldMap.get(cm.Id).Record_Update_Counter__c != cm.Record_Update_Counter__c)
					{
						Campaign c = campaignMap.get(cm.CampaignId);
						updateCampaignCounts(c, cm, oldMap.get(cm.Id), 1, true, false);
						if (campaignSaveMap.get(c.Id) == null)
						{
							campaignSaveMap.put(c.Id, c);
							updateList.add(c);
						}
					}
				}
			}
			
			if (updateList != null && updateList.size() > 0)
				Database.update(updateList, false);
			
	}

	public static Decimal determineValue(Decimal newVal, Decimal oldVal, Integer multiplier, Decimal campaignCurrentTotal, Boolean isUpdate)
	{
		Decimal currentCount = campaignCurrentTotal;
		if (oldVal == null)
			oldVal = 0;
		
		if (newVal == null)	
			newVal = 0;
				
		if (currentCount == null)
			currentCount = 0;
		
			
		if (isUpdate && newval != oldVal)
		{
			if (newVal == 1)
			{
				currentCount += 1;
			}
			else
			{
				currentCount -= 1;
			}
		}
		else if (!isUpdate)
		{
			if (newVal == null)
				newVal = 0;
				
			currentCount += multiplier * newVal;
		}
		
		return currentCount;
	}
	
	
	public static void updateCampaignCounts(Campaign c, CampaignMember cm, CampaignMember oldCM, Integer i, Boolean isUpdate, Boolean isDelete)
	{
		
		if (c != null)
		{
				
			c.Actual_Triple_TOF_A_E__c = determineValue(cm.Triple_TOF_A_E_A__c, oldCM.Triple_TOF_A_E_A__c, i, c.Actual_Triple_TOF_A_E__c, isUpdate);

			c.Actual_QTRAP_A_E__c = determineValue(cm.QTRAP_A_E_A__c, oldCM.QTRAP_A_E_A__c, i, c.Actual_QTRAP_A_E__c, isUpdate);

			c.Actual_EKSIGENT_A_E__c = determineValue(cm.EKSIGENT_A_E_A__c, oldCM.EKSIGENT_A_E_A__c, i, c.Actual_EKSIGENT_A_E__c, isUpdate);

			c.Actual_TOF_TOF_A_E__c = determineValue(cm.TOF_TOF_A_E_A__c, oldCM.TOF_TOF_A_E_A__c, i, c.Actual_TOF_TOF_A_E__c, isUpdate);
		
			c.Actual_Triple_Quad_A_E__c = determineValue(cm.Triple_Quad_A_E_A__c, oldCM.Triple_Quad_A_E_A__c, i, c.Actual_Triple_Quad_A_E__c, isUpdate);
	
			c.Actual_SELEXION_A_E__c = determineValue(cm.SELEXION_A_E_A__c, oldCM.SELEXION_A_E_A__c, i, c.Actual_SELEXION_A_E__c, isUpdate);
	
			c.Actual_Clinical_Forensic_A_E__c = determineValue(cm.Clinical_Forensic_A_E_A__c, oldCM.Clinical_Forensic_A_E_A__c, i, c.Actual_Clinical_Forensic_A_E__c, isUpdate);

			c.Actual_Food_Env_A_E__c = determineValue(cm.Food_Env_A_E_A__c, oldCM.Food_Env_A_E_A__c, i, c.Actual_Food_Env_A_E__c, isUpdate);

			c.Actual_Academics_OMICS_A_E__c = determineValue(cm.Academics_OMICS_A_E_A__c, oldCM.Academics_OMICS_A_E_A__c, i, c.Actual_Academics_OMICS_A_E__c, isUpdate);

			c.Actual_Pharma_CRO_A_E__c = determineValue(cm.Pharma_CRO_A_E_A__c, oldCM.Pharma_CRO_A_E_A__c, i, c.Actual_Pharma_CRO_A_E__c, isUpdate);

			c.Actual_Services_A_E__c = 	determineValue(cm.Services_A_E_A__c, oldCM.Services_A_E_A__c, i, c.Actual_Services_A_E__c, isUpdate);
	
			c.Actual_Other_A_E__c = determineValue(cm.Other_A_E_A__c, oldCM.Other_A_E_A__c, i, c.Actual_Other_A_E__c, isUpdate);
//-----------
				
			c.Actual_Triple_TOF_A_C__c = determineValue(cm.Triple_TOF_ABC_A__c, oldCM.Triple_TOF_ABC_A__c, i, c.Actual_Triple_TOF_A_C__c, isUpdate);
				
			c.Actual_QTRAP_A_C__c = determineValue(cm.QTRAP_ABC_A__c, oldCM.QTRAP_ABC_A__c, i, c.Actual_QTRAP_A_C__c, isUpdate);

			c.Actual_EKSIGENT_A_C__c = determineValue(cm.EKSIGENT_ABC_A__c, oldCM.EKSIGENT_ABC_A__c, i, c.Actual_EKSIGENT_A_C__c, isUpdate);

			c.Actual_TOF_TOF_A_C__c = determineValue(cm.TOF_TOF_ABC_A__c, oldCM.TOF_TOF_ABC_A__c, i, c.Actual_TOF_TOF_A_C__c, isUpdate);
			
			c.Actual_Triple_Quad_A_C__c = determineValue(cm.Triple_Quad_ABC_A__c, oldCM.Triple_Quad_ABC_A__c, i, c.Actual_Triple_Quad_A_C__c, isUpdate);
		
			c.Actual_SELEXION_A_C__c = determineValue(cm.SELEXION_ABC_A__c, oldCM.SELEXION_ABC_A__c, i, c.Actual_SELEXION_A_C__c, isUpdate);
	
			c.Actual_Clinical_Forensic_A_C__c = determineValue(cm.Clinical_Forensic_ABC_A__c, oldCM.Clinical_Forensic_ABC_A__c, i, c.Actual_Clinical_Forensic_A_C__c, isUpdate);

			c.Actual_Food_Env_A_C__c = determineValue(cm.Food_Env_ABC_A__c, oldCM.Food_Env_ABC_A__c, i, c.Actual_Food_Env_A_C__c, isUpdate);

			c.Actual_Academics_OMICS_A_C__c = determineValue(cm.Academics_OMICS_ABC_A__c, oldCM.Academics_OMICS_ABC_A__c, i, c.Actual_Academics_OMICS_A_C__c, isUpdate);

			c.Actual_Pharma_CRO_A_C__c = determineValue(cm.Pharma_CRO_ABC_A__c, oldCM.Pharma_CRO_ABC_A__c, i, c.Actual_Pharma_CRO_A_C__c, isUpdate);
			
			c.Actual_Services_ABC__c = determineValue(cm.Services_ABC_A__c, oldCM.Services_ABC_A__c, i, c.Actual_Services_ABC__c, isUpdate);
			
			c.Actual_Other_ABC__c = determineValue(cm.Other_ABC_A__c, oldCM.Other_ABC_A__c, i, c.Actual_Other_ABC__c, isUpdate);
			
						
		}
		
	}
}