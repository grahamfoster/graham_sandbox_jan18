/*********************************************************************
Name  : testAbTestClient
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the AbTestClient  

*********************************************************************/

public class testAbTestClient{

  public static testMethod void checkAbTestClient() {

    AbTestClient.testCreateQuoteCallNew();
    AbTestClient.testGetQuoteDetails('43456123','002046076','viewsap');
    AbTestClient.testGetQuoteDetails('','','');
    AbTestClient.testSearchQuoteInCDI('2008100601');
    AbTestClient.testSearchQuoteInCDI('');
    AbTestClient.testSubmitQuote();
    AbTestClient.testSubmitQuoteTEST();
    AbTestClient.testGetQuoteApprovalStatus('43456123');
    AbTestClient.testGetQuoteApprovalStatus('');    
    AbTestClient.testGetPartnerData('80012693','NA');
    AbTestClient.testGetPartnerData('','');    
    AbTestClient.testListTemplates();
    AbTestClient.testGetTemplateDetails();
    AbTestClient.testListProducts();
    AbTestClient.testPriceDetails();
    List<QuoteServices.lineItemDetails> fullQuoteLineItems = new List<QuoteServices.lineItemDetails>();
    List<QuoteServices.lineItemDetails> qliTextItems = new List<QuoteServices.lineItemDetails>();
    AbTestClient.consolidateLineItemText(fullQuoteLineItems , qliTextItems );
    QuoteServices.SalesQuote SQ = new QuoteServices.SalesQuote();
    AbTestClient.printSalesQuoteDebug(SQ);
  }  
  public static testMethod void checkAbTestClient2() {
    AbTestClient.testCreateQuoteCallNewNOAUTH(true);
    AbTestClient.testCreateQuoteCallNewNOAUTH(false);

  }
}