/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 25/2015
**
** When fields modified, copy back to related cases
** Nov 11/2016 - Activated the trigger, restructured to use superclass TriggerHandler, commented out the copyValuesToChildCase logic
** 
**/
public class InstalledProductTriggerHandler extends TriggerHandler
{
    public override void afterUpdate() {
        //copyValuesToChildCase();
        //updateCOBORelationship();
        updateAssets();
    }

    public override void afterDelete() {
        //copyValuesToChildCase();
    }

    public override void afterUndelete() {
        //copyValuesToChildCase();
    }

    public override void afterInsert() {
		createAssets();
    }
    
    /*
    private void copyValuesToChildCase()
    {
        List<sObject> sobjs = Trigger.isDelete ? Trigger.old : Trigger.new;
        Set<Id> ipIds = collectIds(sobjs);
        List<Case> cases = findCasesByComponentIds(ipIds);
        for(Case cs : findCasesByComponentIds(ipIds))
        {
            If( !cs.IsClosed){
            	CaseTriggerHandler.copyComponentToCase(cs, (SVMXC__Installed_Product__c)Trigger.newMap.get(cs.SVMXC__Component__c));    
            }
        }
        if(!cases.isEmpty()) update cases;
    }
    */

    //Commented out Apr 12/2017
    /*
    private void updateCOBORelationship() 
    {
        Set<Id> noLongerTopLevel = new Set<Id>();
        Set<Id> becameTopLevel = new Set<Id>();

        for(SVMXC__Installed_Product__c ip : (List<SVMXC__Installed_Product__c>)Trigger.new) 
        {
            SVMXC__Installed_Product__c oldIP = (SVMXC__Installed_Product__c)Trigger.oldMap.get(ip.Id);
            if(oldIP.SVMXC__Top_Level__c == null && ip.SVMXC__Top_Level__c != null) 
            {
                //no longer the top level IP, so remove related COBO relationship to InstallationCase__c
                noLongerTopLevel.add(ip.Id);
            }                
            
            if(oldIP.SVMXC__Top_Level__c != null && ip.SVMXC__Top_Level__c == null) 
            {
                //Became the top level IP, so add related COBO relationship to InstallationCase__c
                becameTopLevel.add(ip.Id);
            }                
        }

        Map<Id, Customer_Onboard__c> coboUpdates = new Map<Id, Customer_Onboard__c>();
        for(Case caseObj : [select Id, (select Id, InstallationCase__c from Customer_Onboards2__r) from Case where SVMXC__Component__c in :noLongerTopLevel]) {
            //remove relationship
            if(!caseObj.Customer_Onboards2__r.isEmpty()) {
                for(Customer_Onboard__c cobo : caseObj.Customer_Onboards2__r) {
                    cobo.InstallationCase__c = null;
                    coboUpdates.put(cobo.Id, cobo);
                }
    }
        }

        Map<String, Case> casesByOrderNumber = new Map<String,Case>();
        for(Case caseObj : [select Id, Sales_Order_Number__c,
                    (select Id, SVMXC__Order_Type__c from SVMXC__Service_Order__r where SVMXC__Order_Type__c = 'Installation'), 
                    (select Id, InstallationCase__c from Customer_Onboards2__r) from Case where 
                    SVMXC__Component__c in :becameTopLevel]) {
            
            //add relationship if qualifies and we find matching cobo by oracle order
            if(!caseObj.SVMXC__Service_Order__r.isEmpty()) 
            {
                if(!String.isBlank(caseObj.Sales_Order_Number__c)) casesByOrderNumber.put(caseObj.Sales_Order_Number__c, caseObj);
            }
        }

        for(Customer_Onboard__c cobo : [select Id, InstallationCase__c, Oracle_Order__r.Oracle_Order_Number__c from Customer_Onboard__c where Oracle_Order__r.Oracle_Order_Number__c in :casesByOrderNumber.keySet()]) 
        {
            Case relatedInstallCase = casesByOrderNumber.get(cobo.Oracle_Order__r.Oracle_Order_Number__c);
            //TODO: Apr 12/2017: Must be an instrument, check against ProductFamilySettings custom setting
            if(relatedInstallCase != null) {
                cobo.InstallationCase__c = relatedInstallCase.Id;
                coboUpdates.put(cobo.Id, cobo);
            }
        }

        if(!coboUpdates.isEmpty()) update coboUpdates.values();
    }
    */

    private Set<String> collectSerialNumbers(List<SVMXC__Installed_Product__c> installedProducts) {
        Set<String> serialNumbers = new Set<String>();
        for(SVMXC__Installed_Product__c ip : installedProducts) {
            if(!String.isBlank(ip.SVMXC__Serial_Lot_Number__c)) serialNumbers.add(ip.SVMXC__Serial_Lot_Number__c);
        }
        return serialNumbers;
    }
    
    private void createAssets() 
    {
        Map<Id, Asset> assetsByIP = new Map<Id, Asset>();
        for(Asset assetObj : [select Installed_Product__c from Asset where Installed_Product__c in :Trigger.new]) 
        {
            assetsByIP.put(assetObj.Installed_Product__c, assetObj);
        }
        
        Set<String> ipSerialNumbers = collectSerialNumbers((List<SVMXC__Installed_Product__c>)Trigger.new);
        Map<String, Asset> assetsBySerialNumber = new Map<String, Asset>();
        for(Asset assetObj : [select Id, SerialNumber, Installed_Product__c from Asset where SerialNumber in :ipSerialNumbers]) {
            assetsBySerialNumber.put(assetObj.SerialNumber, assetObj);
        }

        Set<Id> topLevelIPIds = new Set<Id>();
        for(SVMXC__Installed_Product__c ip : (List<SVMXC__Installed_Product__c>)Trigger.new) {
            if(ip.SVMXC__Top_Level__c != null) {
                topLevelIPIds.add(ip.SVMXC__Top_Level__c);
            }
        }
        Map<Id, SVMXC__Installed_Product__c> topLevelIPsById = new Map<Id, SVMXC__Installed_Product__c>([select Id, (select Id from Assets__r limit 1) from SVMXC__Installed_Product__c where Id in :topLevelIPIds]);
        
        Map<Id, Asset> assetInactiveUpdatesById = new Map<Id, Asset>();
        Map<Id, Asset> newAssetsByIP = new Map<Id, Asset>();
        for(SVMXC__Installed_Product__c ip : (List<SVMXC__Installed_Product__c>)Trigger.new) 
        {
            if(!assetsByIP.containsKey(ip.Id))
            {
                if(!String.isBlank(ip.SVMXC__Serial_Lot_Number__c) && assetsBySerialNumber.containsKey(ip.SVMXC__Serial_Lot_Number__c)) {
                	Asset matchingAsset = assetsBySerialNumber.get(ip.SVMXC__Serial_Lot_Number__c);
                    matchingAsset.Status = 'Inactive';
                    assetInactiveUpdatesById.put(matchingAsset.Id, matchingAsset);
                }
                
                Asset assetObj = new Asset(
                    Installed_Product__c = ip.Id, 
                    AccountId = ip.SVMXC__Company__c,
                    Name = ip.Name,
                    Product2Id = ip.SVMXC__Product__c,
                    SerialNumber = ip.SVMXC__Serial_Lot_Number__c,
                    Primary_Contact__c = ip.SVC_Primary_Contact__c,
                    Status = 'Active'
                );
                if(ip.SVMXC__Top_Level__c != null && topLevelIPsById.containsKey(ip.SVMXC__Top_Level__c)) {
                    SVMXC__Installed_Product__c topLevelIP = topLevelIPsById.get(ip.SVMXC__Top_Level__c);
                    if(topLevelIP != null && !topLevelIP.Assets__r.isEmpty()) {
                        assetObj.ParentId = topLevelIP.Assets__r[0].Id;
                    }
                }
                newAssetsByIP.put(ip.Id, assetObj);
            }
        }
        if(!assetInactiveUpdatesById.isEmpty()) update assetInactiveUpdatesById.values();
        insert newAssetsByIP.values();
    }
    
    private void updateAssets() 
    {
        
        Set<Id> topLevelIPIds = new Set<Id>();
        for(SVMXC__Installed_Product__c ip : (List<SVMXC__Installed_Product__c>)Trigger.new) {
            if(ip.SVMXC__Top_Level__c != null) {
                topLevelIPIds.add(ip.SVMXC__Top_Level__c);
            }
        }
        Map<Id, SVMXC__Installed_Product__c> topLevelIPsById = new Map<Id, SVMXC__Installed_Product__c>([select Id, (select Id from Assets__r limit 1) from SVMXC__Installed_Product__c where Id in :topLevelIPIds]);
        
        //loop through assets on the old value and link to the new value
        Map<Id, Asset> assetUpdates = new Map<Id, Asset>();
        for(Asset assetObj : [select Id, AccountId, Installed_Product__c from Asset where Installed_Product__c in :Trigger.new]) {
        	SVMXC__Installed_Product__c ip = (SVMXC__Installed_Product__c)Trigger.newMap.get(assetObj.Installed_Product__c);
            if(ip.SVMXC__Company__c != assetObj.AccountId) {
                assetObj.AccountId = ip.SVMXC__Company__c;
                assetUpdates.put(assetObj.Id, assetObj);
            }
            Id parentId = null;
            if(ip.SVMXC__Top_Level__c != null && topLevelIPsById.containsKey(ip.SVMXC__Top_Level__c)) {
                SVMXC__Installed_Product__c topLevelIP = topLevelIPsById.get(ip.SVMXC__Top_Level__c);
                if(topLevelIP != null && !topLevelIP.Assets__r.isEmpty()) {
                    parentId = topLevelIP.Assets__r[0].Id;
                }
            }
            assetObj.ParentId = parentId;
            //Oct 5/2017 - update the Asset status if changed
            assetObj.Status = ip.SVMXC__Status__c;
            assetUpdates.put(assetObj.Id, assetObj);
        }
        if(!assetUpdates.isEmpty()) update assetUpdates.values();
    }

    /*
    private List<Case> findCasesByComponentIds(Set<Id> componentIds)
    {
        return [select Id, AccountId, Oracle_Reference__c, SVC_Primary_FSE__c, SVC_Secondary_FSE__c,
                SVC_Severity__c, SVMXC__Is_Entitlement_Performed__c, SVMXC__Site__c,
                SVMX_IB_City__c, SVMX_IB_Country__c, SVMX_IB_State__c, SVMX_IB_Street__c,
                SVMX_IB_Zip__c, SVMXC__Component__c from Case where SVMXC__Component__c in :componentIds];
    }

    public Set<Id> collectIds(List<sObject> sobjs)
    {
        Set<Id> ids = new Set<Id>();
        for(sObject sobj : sobjs)
        {
            ids.add(sobj.Id);
        }
        return ids;
    }
    */
}