@isTest
public class CustomLogger_test {

	static testmethod void testSendLog(){    
   
        //Create Account (to attach Log to)
       	Account a = new Account(Name = 'Test Co #1',CurrencyIsoCode = 'USD', BillingCountry = 'United States');
       	insert a;
        
        system.debug('***- Start testSendLog()' );      
		Test.startTest();
            //Initialize Logger
        	CustomLogger.Logger customLog = new CustomLogger.Logger();
	    	customLog.loglevel = 'Info';
			customLog.subject = 'Generating Opportunities Log';
			customLog.sendTo = 'Not.Bob@Sciex.test';
			customLog.logfile = 'report.csv';
			customLog.msgBody = 'Here is the log report for ABC';
			customLog.attachToId = a.Id;
		
        	customLog.addLine('Info','Object','ObjectID','Container','Action', 'Message 1');
			customLog.addLine('Warning','Object','ObjectID','Container','Action', 'Message 2');
        	customLog.addLine('Success','Object','ObjectID','Container','Action', 'Message 3');
        	customLog.addLine('Error','Object','ObjectID','Container','Action', 'Message 4');
        	customLog.addLine('Info','Object','ObjectID','Container','Action', 'Message 5');
			
        	customLog.SendLog('Debug');	
        	customLog.SendLog('email');	
        	customLog.SendLog('File');	
			customLog.SaveLog();
        
        Test.stopTest();        
		system.debug('***- Stop testSendLog()' );   
    }
    
}