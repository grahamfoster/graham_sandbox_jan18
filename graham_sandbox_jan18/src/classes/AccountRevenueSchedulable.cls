/**
 ** @author Daniel Lachcik
 ** @created Oct 6/2015
 **
 ** Scheduled job to recalculate Account revenue rollups
 ** 
**/
global class AccountRevenueSchedulable implements Schedulable 
{
    private static final String EVERY_JAN_1 = '0 0 5 1 1 ?'; //every Jan 1 at 5:00am

    global void execute(SchedulableContext sc) {
		Database.executeBatch(new AccountRevBatchable());        
	}

	global static void scheduleJob(String title, String cronExpression){
		System.schedule(title, cronExpression, new AccountRevenueSchedulable());
        
	}

    global static void scheduleJob(){
		scheduleJob('Account Revenue Rollups (Jan 1)', EVERY_JAN_1);
	}

}