// ===========================================================================
// Object: OracleOrderTriggerHandler
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Handles Oracle Order trigger logic
// ===========================================================================
// Changes: 2016-08-02 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class OracleOrderTriggerHandler extends TriggerHandler 
{
	override protected void afterUpdate() 
	{
		//if its the informatica integration there is likely to be lots of concurant updates and no requirement 
		//to do updates real time so added to a queueable method.
		setCOBOToOracleOrder();
		if(System.Limits.getQueueableJobs() < System.Limits.getLimitQueueableJobs())
			System.enqueueJob(new OracleOrderUpdatesQueueable((List<Oracle_Order__c>)Trigger.new, (Map<Id,Oracle_Order__c>)Trigger.oldMap, 
			Trigger.isInsert, Trigger.isUpdate, Trigger.isBefore, Trigger.isAfter));
	}

	override protected void afterInsert() 
	{
		setCOBOToOracleOrder();
	}

	override protected void beforeUpdate() 
	{
		autoPopulateTrainingProduct();
	}

	override protected void beforeInsert() 
	{
		autoPopulateTrainingProduct();
	}

	//populate the Training Product value to TRNLP004 if the MS Product is not blank and the incoming Training Product is blank
	//this will trigger population of the COBO in after trigger
	private void autoPopulateTrainingProduct()
	{
		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
			if(String.isBlank(oo.Training_Product__c) && !String.isBlank(oo.MS_Product__c)) {
				oo.Training_Product__c = 'TRNLP004';
			}
		}
	}

	private void setCOBOToOracleOrder() 
	{
		Map<Id,Oracle_Order__c> ordersByOpportunity = new Map<Id,Oracle_Order__c>();
		Set<Id> oracleOrderIds = new Set<Id>();
		Map<String, Product2> productsByCode = new Map<String,Product2>();
		Set<String> trainingProductCodes = new Set<String>();
		Set<String> msProductCodes = new Set<String>();
		Set<String> additionalTrainingProductCodes = new Set<String>();

        //Oct 24 - load the products by code to get the priority
        /*
        Map<String,Product2> productsByCode = new Map<String,Product2>();
        Set<String> productCodes = new Set<String>();
		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
            if(!String.isBlank(oo.Training_Product__c))
            {
                String[] productCodeToks = oo.Training_Product__c.split(' ');
                for(String productCode : productCodeToks) {
                    productCodes.add(productCode);
                }
            }
        }
        
        if(!productCodes.isEmpty()) {
            for(Product2 p : [select Id, ProductCode, Part_Priority__c from Product2 where IsActive = true and ProductCode in :productCodes]) {
				  productsByCode.put(p.ProductCode, p);              
            }
        }
		*/

		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
			if(Trigger.isUpdate) {
				Oracle_Order__c oldOO = (Oracle_Order__c)Trigger.oldMap.get(oo.Id);
                /*
				if(oldOO.Opportunity__c == null && oo.Opportunity__c != null)
				{
					//link the COBO to the Opportunity
					if(!String.isBlank(oo.MS_Product__c)) ordersByOpportunity.put(oo.Opportunity__c, oo);
				}
				*/
                if(oo.Opportunity__c != null) ordersByOpportunity.put(oo.Opportunity__c, oo);

				if(!String.isBlank(oo.Training_Product__c))
				{
					oracleOrderIds.add(oo.Id);
                    String[] productCodeToks = oo.Training_Product__c.split(' ');
                    for(String productCode : productCodeToks) {
                        trainingProductCodes.add(productCode);
                    }
				}

				if(oldOO.MS_Product__c != oo.MS_Product__c && !String.isBlank(oo.MS_Product__c))
				{
					oracleOrderIds.add(oo.Id);
					msProductCodes.add(oo.MS_Product__c);	
				}

				if(oldOO.Additional_Training_Product__c != oo.Additional_Training_Product__c && !String.isBlank(oo.Additional_Training_Product__c)) 
				{
					oracleOrderIds.add(oo.Id);
					additionalTrainingProductCodes.add(oo.Additional_Training_Product__c);
				}
			}else {
				if(oo.Opportunity__c != null)
				{
					//link the COBO to the Opportunity
					ordersByOpportunity.put(oo.Opportunity__c, oo);
				}

				if(!String.isBlank(oo.Training_Product__c))
				{
					oracleOrderIds.add(oo.Id);
                    String[] productCodeToks = oo.Training_Product__c.split(' ');
                    for(String productCode : productCodeToks) {
                        trainingProductCodes.add(productCode);
                    }
				}

				if(!String.isBlank(oo.MS_Product__c))
				{
					oracleOrderIds.add(oo.Id);
					msProductCodes.add(oo.MS_Product__c);	
				}
				
				if(!String.isBlank(oo.Additional_Training_Product__c)) 
				{
					oracleOrderIds.add(oo.Id);
					additionalTrainingProductCodes.add(oo.Additional_Training_Product__c);
				}
			}
		}

		for(Product2 p : [select Id, ProductCode, Family, Part_Priority__c from Product2 where IsActive = true and (ProductCode in :trainingProductCodes or ProductCode in :msProductCodes or ProductCode in :additionalTrainingProductCodes)])
		{
			productsByCode.put(p.ProductCode, p);
		}

		Set<Id> syncCOBOIds = new Set<Id>();

		Map<Id, Customer_Onboard__c> cobos = new Map<Id, Customer_Onboard__c>([select Id, Opportunity__c, Opportunity__r.RecordTypeId, Opportunity__r.RecordType.Name, Oracle_Order__r.Training_Product__c, Oracle_Order__r.MS_Product__c, Oracle_Order__r.Additional_Training_Product__c  from Customer_Onboard__c where Opportunity__c in :ordersByOpportunity.keySet()]);
		for(Customer_Onboard__c cobo : cobos.values()) 
		{
			Oracle_Order__c oracleOrder = null;
			if(ordersByOpportunity.containsKey(cobo.Opportunity__c)) {
				oracleOrder = ordersByOpportunity.get(cobo.Opportunity__c);
                if(!String.isBlank(oracleOrder.MS_Product__c) || cobo.Opportunity__r.RecordTypeId == COBOUtil.getFastOpportunityRecordTypeId()) {
                    cobo.Oracle_Order__c = oracleOrder.Id;
                }
			}else{
				oracleOrder = cobo.Oracle_Order__r;
			}

			if(oracleOrder != null) 
			{
                //TODO: prioritize Training_Product__c                
                /*
				if(!String.isBlank(oracleOrder.Training_Product__c) && productsByCode.containsKey(oracleOrder.Training_Product__c))
				{
					Product2 p = productsByCode.get(oracleOrder.Training_Product__c);
					cobo.Product__c = p.Id;
					syncCOBOIds.add(cobo.Id);
				}
				*/
				if(!String.isBlank(oracleOrder.Training_Product__c))
				{
                    String[] trainingProductCodeToks = oracleOrder.Training_Product__c.split(' ');
                    Product2 prod1 = productsByCode.get(trainingProductCodeToks[0]);
                    Product2 prod2 = null;

                    if(trainingProductCodeToks.size() == 2) {
                        prod2 = productsByCode.get(trainingProductCodeToks[1]);
                    }
                    
                    if(prod1 != null && prod2 != null) {
                        Id highestPriorityProduct = null;
                        Id lowestPriorityProduct = null;

                        Integer p1 = prod1.Part_Priority__c != null ? Integer.valueOf(prod1.Part_Priority__c) : null;
                        Integer p2 = prod2.Part_Priority__c != null ? Integer.valueOf(prod2.Part_Priority__c) : null;
                        
                        if(p1 != null && p2 != null) {
                            if(p1 >= p2) {
                                highestPriorityProduct = prod1.Id;
                                lowestPriorityProduct = prod2.Id;
                            }else{
                                highestPriorityProduct = prod2.Id;
                                lowestPriorityProduct = prod1.Id;
                            }
                        }else{
                            highestPriorityProduct = prod1.Id;
                            lowestPriorityProduct = prod2.Id;
                        }
                        cobo.Product__c = highestPriorityProduct;
                        if(String.isBlank(oracleOrder.Additional_Training_Product__c)) 
                            cobo.Additional_Training_Product__c = lowestPriorityProduct;
                    }else{
                        if(prod1 != null) cobo.Product__c = prod1.Id;
                    }
                    syncCOBOIds.add(cobo.Id);
				}

				if(!String.isBlank(oracleOrder.MS_Product__c) && productsByCode.containsKey(oracleOrder.MS_Product__c))
				{
					Product2 p = productsByCode.get(oracleOrder.MS_Product__c);
					cobo.Ordered_Product__c = p.Id;
					syncCOBOIds.add(cobo.Id);
				}
				if(!String.isBlank(oracleOrder.Additional_Training_Product__c) && productsByCode.containsKey(oracleOrder.Additional_Training_Product__c))
				{
					Product2 p = productsByCode.get(oracleOrder.Additional_Training_Product__c);
					cobo.Additional_Training_Product__c = p.Id;
					syncCOBOIds.add(cobo.Id);
				}
			}
		}

		Map<Id, Customer_Onboard__c> cobos2 = new Map<Id, Customer_Onboard__c>([select Id, Ordered_Product__c, Product__c, Oracle_Order__r.Training_Product__c, Oracle_Order__r.MS_Product__c, Oracle_Order__r.Additional_Training_Product__c from Customer_Onboard__c where Id not in :cobos.keySet() and Oracle_Order__c in :oracleOrderIds]);
		for(Customer_Onboard__c cobo : cobos2.values()) 
		{
            //TODO: prioritize Training_Product__c
			if(!String.isBlank(cobo.Oracle_Order__r.Training_Product__c))
			{
                /*
				Product2 p = productsByCode.get(cobo.Oracle_Order__r.Training_Product__c);
				cobo.Product__c = p.Id;
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
				*/
                String[] trainingProductCodeToks = cobo.Oracle_Order__r.Training_Product__c.split(' ');
                Product2 prod1 = productsByCode.get(trainingProductCodeToks[0]);
                Product2 prod2 = null;
                
                if(trainingProductCodeToks.size() == 2) {
                    prod2 = productsByCode.get(trainingProductCodeToks[1]);
                }
                
                if(prod1 != null && prod2 != null) {
                    Id highestPriorityProduct = null;
                    Id lowestPriorityProduct = null;
                    
                    Integer p1 = prod1.Part_Priority__c != null ? Integer.valueOf(prod1.Part_Priority__c) : null;
                    Integer p2 = prod2.Part_Priority__c != null ? Integer.valueOf(prod2.Part_Priority__c) : null;
                    
                    if(p1 != null && p2 != null) {
                        if(p1 <= p2) {
                            highestPriorityProduct = prod1.Id;
                            lowestPriorityProduct = prod2.Id;
                        }else{
                            highestPriorityProduct = prod2.Id;
                            lowestPriorityProduct = prod1.Id;
                        }
                    }else{
                        highestPriorityProduct = prod1.Id;
                        lowestPriorityProduct = prod2.Id;
                    }
                    cobo.Product__c = highestPriorityProduct;
                    if(String.isBlank(cobo.Oracle_Order__r.Additional_Training_Product__c)) 
                        cobo.Additional_Training_Product__c = lowestPriorityProduct;
                }else{
                    if(prod1 != null) cobo.Product__c = prod1.Id;
                }
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
			}
			if(!String.isBlank(cobo.Oracle_Order__r.MS_Product__c) && productsByCode.containsKey(cobo.Oracle_Order__r.MS_Product__c))
			{
				Product2 p = productsByCode.get(cobo.Oracle_Order__r.MS_Product__c);
				cobo.Ordered_Product__c = p.Id;
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
			}
			if(!String.isBlank(cobo.Oracle_Order__r.Additional_Training_Product__c) && productsByCode.containsKey(cobo.Oracle_Order__r.Additional_Training_Product__c))
			{
				Product2 p = productsByCode.get(cobo.Oracle_Order__r.Additional_Training_Product__c);
				cobo.Additional_Training_Product__c = p.Id;
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
			}
		}
		update cobos.values();

		if(!syncCOBOIds.isEmpty()) {
			//find related contacts
			Set<Id> contactIds = new Set<Id>();
			for(OnBoarding_Contacts__c onboardingContact : [select Id, OnBoard_Contact__c from OnBoarding_Contacts__c where 
				Customer_Onboard__c in :syncCOBOIds
				and (Role__c = 'Primary Learner' or Role__c = 'Additional Learner')]) 
			{
				contactIds.add(onboardingContact.OnBoard_Contact__c);
			}

			if(!contactIds.isEmpty())
			{
				//perform update to LMS with those contacts
				List<Contact> contacts = AbsorbUtil.findContacts(contactIds);
				for(Contact c : contacts) {
					System.enqueueJob(new ContactTriggerHandler.UpdateLMSUserQueueable(c));
				}
			}
		}
	}

	public Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }

	private void updateTrainingCaseFASDays()
	{
		Map<Id,Decimal> FASData = new Map<Id,Decimal>();
		Set<Id> ooIds = new Set<Id>();
		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
			if(isChanged(oo,'Total_Number_of_FAS_Days_Purchased__c'))
			{
				FASData.put(oo.Id, oo.Total_Number_of_FAS_Days_Purchased__c);
			}
		}
		System.debug('**GF** Changed Oracle Orders:' + FASData);
		if(FASData.size() > 0)
		{
			
			List<Case> caseData = [SELECT Id, Number_of_FAS_Days__c, Oracle_Order__c 
			FROM Case WHERE Oracle_Order__c IN :FASData.keySet()];
			System.debug('**GF** Cases For Changed Oracle Orders:' + caseData);
			if(caseData.size() > 0)
			{

				for(Case c : caseData)
				{
					c.Total_Number_of_FAS_Days__c = FASData.get(c.Oracle_Order__c);
				}

				update caseData;
			}

		}



	}

		/**
		* @description an after update trigger to move the cobo status to Shipment when the MS_Instrument_Status__c of the Oracle Order is changed from null
		* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/143949860/setCOBOStatusToShipment
		*/ 
		private void setCOBOStatusToShipment()
	{
		System.debug('**GF** cobo Status to shipment: start');
		Set<Id> coboIds = new Set<Id>();
		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
			if(isChanged(oo, 'MS_Instrument_Status__c') && oo.MS_Instrument_Status__c != null)
			{
				coboIds.add(oo.Id);
			}
		}
		System.debug('**GF** coboIds:' + coboIds);
		if(coboIds.size() > 0)
		{
			setCOBOStatusToShipmentAsync(coboIds);
		}
		


	}

	/**
	* @description a future method called async to update the cobo records when the MS_Instrument_Status__c is changed from null.  
	* Performed async as oracle order updates are done by informatica so there is no requirement to do synchronously and cobo has many automations 
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/144179227/setCOBOStatusToShipmentAsync
	* @param coboIds Set of Oracle Order Id's where the MS_Instrument_Status__c has been changed from null
	*/ 
	@Future static void setCOBOStatusToShipmentAsync(Set<Id> coboIds)
	{
		List<Customer_Onboard__c> updaters = new List<Customer_Onboard__c>();
		List<Customer_Onboard__c> cobos = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Oracle_Order__c IN :coboIds];
		for(Customer_Onboard__c c : cobos)
		{
			if(c.Status__c == 'Order Confirmed')
			{
				c.Status__c = 'Shipment';
				updaters.add(c);
			}
		}

		if(updaters.size() > 0)
		{
			update updaters;
		}
	}

	
	/**
	* @description an after update trigger to move the cobo status to installation when the shipment status of the Oracle Order is set to closed.
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/144179201/setCOBOStatusToInstallation
	*/ 
	private void setCOBOStatusToInstallation()
	{
		System.debug('**GF** cobo Status to Installation: start');
		Set<Id> coboIds = new Set<Id>();
		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
			if(isChanged(oo, 'Shipment_Status__c') && oo.Shipment_Status__c == 'Closed')
			{
				coboIds.add(oo.Id);
			}
		}
		System.debug('**GF** coboIds:' + coboIds);
		List<Customer_Onboard__c> updaters = new List<Customer_Onboard__c>();
		if(coboIds.size() > 0)
		{
			setCOBOStatusToInstallationAsync(coboIds);
		}
		
	}

	/**
	* @description a future method called async to update the cobo records when the Oracle Order Shipment Status is changed to closed.  
	* Performed async as oracle order updates are done by informatica so there is no requirement to do synchronously and cobo has many automations
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/143949837/setCOBOStatusToInstallationAsync
	* @param coboIds a Set of Oracle Order Id's where the shipment status has been changed to Closed
	*/ 
	@Future static void setCOBOStatusToInstallationAsync(Set<Id> coboIds)
	{
		List<Customer_Onboard__c> updaters = new List<Customer_Onboard__c>();
		List<Customer_Onboard__c> cobos = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Oracle_Order__c IN :coboIds];
		for(Customer_Onboard__c c : cobos)
		{
			if(c.Status__c == 'Shipment')
			{
				c.Status__c = 'Installation';
				updaters.add(c);
			}
		}

		if(updaters.size() > 0)
		{
			update updaters;
		}
	}

	public class OracleOrderUpdatesQueueable implements Queueable
	{
		public List<Oracle_Order__c> updatedOrders { get; set; }
		public Map<Id, Oracle_Order__c> oldMap {get; set;}
		public Boolean isInsertTrigger {get;set;}
		public Boolean isUpdateTrigger {get;set;}
		public Boolean isBeforeTrigger {get;set;}
		public Boolean isAfterTrigger {get;set;}

		public OracleOrderUpdatesQueueable(List<Oracle_Order__c> updatedOrders, Map<Id, Oracle_Order__c> oldMap, 
		Boolean isInsertTrigger, Boolean isUpdateTrigger, Boolean isBeforeTrigger, Boolean isAfterTrigger)
		{
			this.updatedOrders = updatedOrders;
			this.oldMap = oldMap;
			this.isInsertTrigger = isInsertTrigger;
			this.isUpdateTrigger = isUpdateTrigger;
			this.isBeforeTrigger = isBeforeTrigger;
			this.isAfterTrigger = isAfterTrigger;
		}

		public void execute(QueueableContext context)
		{
			if(isAfterTrigger && isUpdateTrigger)
			{
				afterUpdate();
			} else if(isAfterTrigger && isInsertTrigger)
			{
				afterInsert();
			}
		}

		private void afterUpdate()
		{
			//setCOBOToOracleOrder();
			setCOBOStatusToShipment();
			setCOBOStatusToInstallation();
			updateTrainingCaseFASDays();
		}

		private void afterInsert()
		{
			//setCOBOToOracleOrder();
		}

		private void setCOBOStatusToShipment()
		{
			System.debug('**GF** cobo Status to shipment: start');
			Set<Id> ooIds = new Set<Id>();
			for(Oracle_Order__c oo : updatedOrders)
			{
				if(SfdcUtil.isChanged(oo, oldMap, 'MS_Instrument_Status__c', isInsertTrigger) && oo.MS_Instrument_Status__c != null)
				{
					ooIds.add(oo.Id);
				}
			}
			System.debug('**GF** coboIds:' + ooIds);
			if(ooIds.size() > 0)
			{
				List<Customer_Onboard__c> updaters = new List<Customer_Onboard__c>();
				List<Customer_Onboard__c> cobos = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Oracle_Order__c IN :ooIds];
				for(Customer_Onboard__c c : cobos)
				{
					if(c.Status__c == 'Order Confirmed')
					{
						c.Status__c = 'Shipment';
						updaters.add(c);
					}
				}

				if(updaters.size() > 0)
				{
					update updaters;
				}
			}
		}

		private void setCOBOStatusToInstallation()
		{
			System.debug('**GF** cobo Status to Installation: start');
			Set<Id> ooIds = new Set<Id>();
			for(Oracle_Order__c oo : updatedOrders)
			{
				if(SfdcUtil.isChanged(oo,oldMap,'Shipment_Status__c',isInsertTrigger) && oo.Shipment_Status__c == 'Closed')
				{
					ooIds.add(oo.Id);
				}
			}
			System.debug('**GF** ooIds:' + ooIds);
			if(ooIds.size() > 0)
			{
				List<Customer_Onboard__c> updaters = new List<Customer_Onboard__c>();
				List<Customer_Onboard__c> cobos = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Oracle_Order__c IN :ooIds];
				for(Customer_Onboard__c c : cobos)
				{
					if(c.Status__c == 'Shipment')
					{
						c.Status__c = 'Installation';
						updaters.add(c);
					}
				}

				if(updaters.size() > 0)
				{
					update updaters;
				}
			}
		}

		private void updateTrainingCaseFASDays()
		{
			Map<Id,Decimal> FASData = new Map<Id,Decimal>();
			Set<Id> ooIds = new Set<Id>();
			for(Oracle_Order__c oo : updatedOrders)
			{
				if(SfdcUtil.isChanged(oo,oldMap,'Total_Number_of_FAS_Days_Purchased__c',isInsertTrigger))
				{
					FASData.put(oo.Id, oo.Total_Number_of_FAS_Days_Purchased__c);
				}
			}
			System.debug('**GF** Changed Oracle Orders:' + FASData);
			if(FASData.size() > 0)
			{
			
				List<Case> caseData = [SELECT Id, Number_of_FAS_Days__c, Oracle_Order__c 
				FROM Case WHERE Oracle_Order__c IN :FASData.keySet()];
				System.debug('**GF** Cases For Changed Oracle Orders:' + caseData);
				if(caseData.size() > 0)
				{

					for(Case c : caseData)
					{
						c.Total_Number_of_FAS_Days__c = FASData.get(c.Oracle_Order__c);
					}

					update caseData;
				}

			}
		}

	private void setCOBOToOracleOrder() 
	{
		Map<Id,Oracle_Order__c> ordersByOpportunity = new Map<Id,Oracle_Order__c>();
		Set<Id> oracleOrderIds = new Set<Id>();
		Map<String, Product2> productsByCode = new Map<String,Product2>();
		Set<String> trainingProductCodes = new Set<String>();
		Set<String> msProductCodes = new Set<String>();
		Set<String> additionalTrainingProductCodes = new Set<String>();

        //Oct 24 - load the products by code to get the priority
        /*
        Map<String,Product2> productsByCode = new Map<String,Product2>();
        Set<String> productCodes = new Set<String>();
		for(Oracle_Order__c oo : (List<Oracle_Order__c>)Trigger.new)
		{
            if(!String.isBlank(oo.Training_Product__c))
            {
                String[] productCodeToks = oo.Training_Product__c.split(' ');
                for(String productCode : productCodeToks) {
                    productCodes.add(productCode);
                }
            }
        }
        
        if(!productCodes.isEmpty()) {
            for(Product2 p : [select Id, ProductCode, Part_Priority__c from Product2 where IsActive = true and ProductCode in :productCodes]) {
				  productsByCode.put(p.ProductCode, p);              
            }
        }
		*/

		for(Oracle_Order__c oo : updatedOrders)
		{
			if(isUpdateTrigger && isAfterTrigger) {
				Oracle_Order__c oldOO = oldMap.get(oo.Id);
                /*
				if(oldOO.Opportunity__c == null && oo.Opportunity__c != null)
				{
					//link the COBO to the Opportunity
					if(!String.isBlank(oo.MS_Product__c)) ordersByOpportunity.put(oo.Opportunity__c, oo);
				}
				*/
                if(oo.Opportunity__c != null) ordersByOpportunity.put(oo.Opportunity__c, oo);

				if(!String.isBlank(oo.Training_Product__c))
				{
					oracleOrderIds.add(oo.Id);
                    String[] productCodeToks = oo.Training_Product__c.split(' ');
                    for(String productCode : productCodeToks) {
                        trainingProductCodes.add(productCode);
                    }
				}

				if(oldOO.MS_Product__c != oo.MS_Product__c && !String.isBlank(oo.MS_Product__c))
				{
					oracleOrderIds.add(oo.Id);
					msProductCodes.add(oo.MS_Product__c);	
				}

				if(oldOO.Additional_Training_Product__c != oo.Additional_Training_Product__c && !String.isBlank(oo.Additional_Training_Product__c)) 
				{
					oracleOrderIds.add(oo.Id);
					additionalTrainingProductCodes.add(oo.Additional_Training_Product__c);
				}
			}else {
				if(oo.Opportunity__c != null)
				{
					//link the COBO to the Opportunity
					ordersByOpportunity.put(oo.Opportunity__c, oo);
				}

				if(!String.isBlank(oo.Training_Product__c))
				{
					oracleOrderIds.add(oo.Id);
                    String[] productCodeToks = oo.Training_Product__c.split(' ');
                    for(String productCode : productCodeToks) {
                        trainingProductCodes.add(productCode);
                    }
				}

				if(!String.isBlank(oo.MS_Product__c))
				{
					oracleOrderIds.add(oo.Id);
					msProductCodes.add(oo.MS_Product__c);	
				}
				
				if(!String.isBlank(oo.Additional_Training_Product__c)) 
				{
					oracleOrderIds.add(oo.Id);
					additionalTrainingProductCodes.add(oo.Additional_Training_Product__c);
				}
			}
		}

		for(Product2 p : [select Id, ProductCode, Family, Part_Priority__c from Product2 where IsActive = true and (ProductCode in :trainingProductCodes or ProductCode in :msProductCodes or ProductCode in :additionalTrainingProductCodes)])
		{
			productsByCode.put(p.ProductCode, p);
		}

		Set<Id> syncCOBOIds = new Set<Id>();

		Map<Id, Customer_Onboard__c> cobos = new Map<Id, Customer_Onboard__c>([select Id, Opportunity__c, Opportunity__r.RecordTypeId, Opportunity__r.RecordType.Name, Oracle_Order__r.Training_Product__c, Oracle_Order__r.MS_Product__c, Oracle_Order__r.Additional_Training_Product__c  from Customer_Onboard__c where Opportunity__c in :ordersByOpportunity.keySet()]);
		for(Customer_Onboard__c cobo : cobos.values()) 
		{
			Oracle_Order__c oracleOrder = null;
			if(ordersByOpportunity.containsKey(cobo.Opportunity__c)) {
				oracleOrder = ordersByOpportunity.get(cobo.Opportunity__c);
                if(!String.isBlank(oracleOrder.MS_Product__c) || cobo.Opportunity__r.RecordTypeId == COBOUtil.getFastOpportunityRecordTypeId()) {
                    cobo.Oracle_Order__c = oracleOrder.Id;
                }
			}else{
				oracleOrder = cobo.Oracle_Order__r;
			}

			if(oracleOrder != null) 
			{
                //TODO: prioritize Training_Product__c                
                /*
				if(!String.isBlank(oracleOrder.Training_Product__c) && productsByCode.containsKey(oracleOrder.Training_Product__c))
				{
					Product2 p = productsByCode.get(oracleOrder.Training_Product__c);
					cobo.Product__c = p.Id;
					syncCOBOIds.add(cobo.Id);
				}
				*/
				if(!String.isBlank(oracleOrder.Training_Product__c))
				{
                    String[] trainingProductCodeToks = oracleOrder.Training_Product__c.split(' ');
                    Product2 prod1 = productsByCode.get(trainingProductCodeToks[0]);
                    Product2 prod2 = null;

                    if(trainingProductCodeToks.size() == 2) {
                        prod2 = productsByCode.get(trainingProductCodeToks[1]);
                    }
                    
                    if(prod1 != null && prod2 != null) {
                        Id highestPriorityProduct = null;
                        Id lowestPriorityProduct = null;

                        Integer p1 = prod1.Part_Priority__c != null ? Integer.valueOf(prod1.Part_Priority__c) : null;
                        Integer p2 = prod2.Part_Priority__c != null ? Integer.valueOf(prod2.Part_Priority__c) : null;
                        
                        if(p1 != null && p2 != null) {
                            if(p1 >= p2) {
                                highestPriorityProduct = prod1.Id;
                                lowestPriorityProduct = prod2.Id;
                            }else{
                                highestPriorityProduct = prod2.Id;
                                lowestPriorityProduct = prod1.Id;
                            }
                        }else{
                            highestPriorityProduct = prod1.Id;
                            lowestPriorityProduct = prod2.Id;
                        }
                        cobo.Product__c = highestPriorityProduct;
                        if(String.isBlank(oracleOrder.Additional_Training_Product__c)) 
                            cobo.Additional_Training_Product__c = lowestPriorityProduct;
                    }else{
                        if(prod1 != null) cobo.Product__c = prod1.Id;
                    }
                    syncCOBOIds.add(cobo.Id);
				}

				if(!String.isBlank(oracleOrder.MS_Product__c) && productsByCode.containsKey(oracleOrder.MS_Product__c))
				{
					Product2 p = productsByCode.get(oracleOrder.MS_Product__c);
					cobo.Ordered_Product__c = p.Id;
					syncCOBOIds.add(cobo.Id);
				}
				if(!String.isBlank(oracleOrder.Additional_Training_Product__c) && productsByCode.containsKey(oracleOrder.Additional_Training_Product__c))
				{
					Product2 p = productsByCode.get(oracleOrder.Additional_Training_Product__c);
					cobo.Additional_Training_Product__c = p.Id;
					syncCOBOIds.add(cobo.Id);
				}
			}
		}

		Map<Id, Customer_Onboard__c> cobos2 = new Map<Id, Customer_Onboard__c>([select Id, Ordered_Product__c, Product__c, Oracle_Order__r.Training_Product__c, Oracle_Order__r.MS_Product__c, Oracle_Order__r.Additional_Training_Product__c from Customer_Onboard__c where Id not in :cobos.keySet() and Oracle_Order__c in :oracleOrderIds]);
		for(Customer_Onboard__c cobo : cobos2.values()) 
		{
            //TODO: prioritize Training_Product__c
			if(!String.isBlank(cobo.Oracle_Order__r.Training_Product__c))
			{
                /*
				Product2 p = productsByCode.get(cobo.Oracle_Order__r.Training_Product__c);
				cobo.Product__c = p.Id;
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
				*/
                String[] trainingProductCodeToks = cobo.Oracle_Order__r.Training_Product__c.split(' ');
                Product2 prod1 = productsByCode.get(trainingProductCodeToks[0]);
                Product2 prod2 = null;
                
                if(trainingProductCodeToks.size() == 2) {
                    prod2 = productsByCode.get(trainingProductCodeToks[1]);
                }
                
                if(prod1 != null && prod2 != null) {
                    Id highestPriorityProduct = null;
                    Id lowestPriorityProduct = null;
                    
                    Integer p1 = prod1.Part_Priority__c != null ? Integer.valueOf(prod1.Part_Priority__c) : null;
                    Integer p2 = prod2.Part_Priority__c != null ? Integer.valueOf(prod2.Part_Priority__c) : null;
                    
                    if(p1 != null && p2 != null) {
                        if(p1 <= p2) {
                            highestPriorityProduct = prod1.Id;
                            lowestPriorityProduct = prod2.Id;
                        }else{
                            highestPriorityProduct = prod2.Id;
                            lowestPriorityProduct = prod1.Id;
                        }
                    }else{
                        highestPriorityProduct = prod1.Id;
                        lowestPriorityProduct = prod2.Id;
                    }
                    cobo.Product__c = highestPriorityProduct;
                    if(String.isBlank(cobo.Oracle_Order__r.Additional_Training_Product__c)) 
                        cobo.Additional_Training_Product__c = lowestPriorityProduct;
                }else{
                    if(prod1 != null) cobo.Product__c = prod1.Id;
                }
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
			}
			if(!String.isBlank(cobo.Oracle_Order__r.MS_Product__c) && productsByCode.containsKey(cobo.Oracle_Order__r.MS_Product__c))
			{
				Product2 p = productsByCode.get(cobo.Oracle_Order__r.MS_Product__c);
				cobo.Ordered_Product__c = p.Id;
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
			}
			if(!String.isBlank(cobo.Oracle_Order__r.Additional_Training_Product__c) && productsByCode.containsKey(cobo.Oracle_Order__r.Additional_Training_Product__c))
			{
				Product2 p = productsByCode.get(cobo.Oracle_Order__r.Additional_Training_Product__c);
				cobo.Additional_Training_Product__c = p.Id;
				cobos.put(cobo.Id, cobo);
				syncCOBOIds.add(cobo.Id);
			}
		}
		update cobos.values();

		if(!syncCOBOIds.isEmpty()) {
			//find related contacts
			Set<Id> contactIds = new Set<Id>();
			for(OnBoarding_Contacts__c onboardingContact : [select Id, OnBoard_Contact__c from OnBoarding_Contacts__c where 
				Customer_Onboard__c in :syncCOBOIds
				and (Role__c = 'Primary Learner' or Role__c = 'Additional Learner')]) 
			{
				contactIds.add(onboardingContact.OnBoard_Contact__c);
			}

			if(!contactIds.isEmpty())
			{
				//perform update to LMS with those contacts
				List<Contact> contacts = AbsorbUtil.findContacts(contactIds);
				for(Contact c : contacts) {
					System.enqueueJob(new ContactTriggerHandler.UpdateLMSUserQueueable(c));
				}
			}
		}
	}

	}
}