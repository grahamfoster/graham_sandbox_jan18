/* Test Class for CloseCaseExtension
*
* Graham Foster   January 2017
*/

@isTest
public class CloseCaseExtension_Test 
{
    
    @testSetup
    static void setup()
    {
        Account a = new Account(Name = 'Tester Account', BillingCountry = 'Canada');
		insert a;
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', 
		AccountId = a.Id);
		insert c;
		Case ca = new Case(ContactId = c.Id, Subject = '0 TestCase', Description = 'Tester Case', QTRICS_Send_Survey__c = false);
    	Case ca1 = new Case(ContactId = c.Id, Subject = '1 TestClosedCase', Description = 'Tester Case', QTRICS_Send_Survey__c = false);
    	Case ca2 = new Case(ContactId = c.Id, Subject = '2 TestNoComment', Description = 'Tester Case', QTRICS_Send_Survey__c = false);
    	Case ca3 = new Case(ContactId = c.Id, Subject = '3 TestNoResCode', Description = 'Tester Case', QTRICS_Send_Survey__c = false);
    	Case ca4 = new Case(ContactId = c.Id, Subject = '4 TestGeneralInquiry', Description = 'Tester Case', QTRICS_Send_Survey__c = false);
    	Case ca5 = new Case(ContactId = c.Id, Subject = '5 TestNotGeneralInquiry', Description = 'Tester Case', QTRICS_Send_Survey__c = false);
    	List<Case> newCases =  new List<Case>{ca,ca1,ca2,ca3,ca4,ca5};
    	insert newCases;
        
    }
    
    @isTest
    static void allFields_Test()
    {
        List<Case> originalCase = [SELECT IsClosed, QTRICS_Send_Survey__c, 
								Master_Control_ID__c, Sales_Order_Number__c, 
								Training_Part_Number__c, Complaint__c, Resolution_Code__c, 
								Training_Issue__c 
								FROM Case WHERE Subject = '0 TestCase'];
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(originalCase[0]);
		CloseCaseExtension cce = new CloseCaseExtension(sc);

		cce.comment.CommentBody = 'Testing a case comment is added';

		cce.sendSurvey.QTRICS_Send_Survey__c = true;
		cce.sendSurvey.Master_Control_ID__c = 'TestMC';
		cce.sendSurvey.Sales_Order_Number__c = 'TestOrderNumber';
		cce.sendSurvey.Training_Part_Number__c = 'TestPartNumber';
		cce.sendSurvey.Complaint__c = true;
		cce.sendSurvey.Resolution_Code__c = 'General inquiry';
		cce.sendSurvey.Training_Issue__c = 'Yes';
		test.startTest();
		cce.closeCase();
        test.stopTest();

		List<Case> updatedCase = [SELECT IsClosed, QTRICS_Send_Survey__c, 
								Master_Control_ID__c, Sales_Order_Number__c, 
								Training_Part_Number__c, Complaint__c, Resolution_Code__c, 
								Training_Issue__c 
								FROM Case WHERE Id = :originalCase[0].Id LIMIT 1];

		Case orCase = originalCase[0];
		Case upCase = updatedCase[0];

		//make sure all of the fields have been updated
		System.assertNotEquals(upCase.IsClosed, orCase.IsClosed);
		//System.assertNotEquals(upCase.QTRICS_Send_Survey__c, orCase.QTRICS_Send_Survey__c);
		System.assertEquals(upCase.Master_Control_ID__c, 'TestMC');
		System.assertEquals(upCase.Sales_Order_Number__c, 'TestOrderNumber');
		System.assertEquals(upCase.Training_Part_Number__c, 'TestPartNumber');
		System.assertNotEquals(upCase.Complaint__c, orCase.Complaint__c);
		System.assertEquals(upCase.Resolution_Code__c, 'General inquiry');
		System.assertEquals(upCase.Training_Issue__c, 'Yes');

		//test that the comment was added
		List<CaseComment> addedComment = [SELECT CommentBody FROM CaseComment WHERE ParentId = :originalCase[0].Id LIMIT 1];

		System.assertEquals(addedComment[0].CommentBody, 'Testing a case comment is added');
    }
    
    @isTest
    static void noResolutionCode_Test()
    {
        List<Case> originalCase = [SELECT IsClosed, QTRICS_Send_Survey__c, 
								Master_Control_ID__c, Sales_Order_Number__c, 
								Training_Part_Number__c, Complaint__c, Resolution_Code__c, 
								Training_Issue__c 
								FROM Case WHERE Subject = '3 TestNoResCode'];
        
        ApexPages.StandardController sc3 = new ApexPages.StandardController(originalCase[0]);
		CloseCaseExtension cce3 = new CloseCaseExtension(sc3);
    	cce3.comment.CommentBody = 'Testing a case comment is added';
		cce3.sendSurvey.QTRICS_Send_Survey__c = true;
		cce3.sendSurvey.Master_Control_ID__c = 'TestMC';
		cce3.sendSurvey.Sales_Order_Number__c = 'TestOrderNumber';
		cce3.sendSurvey.Training_Part_Number__c = 'TestPartNumber';
		cce3.sendSurvey.Complaint__c = true;
		cce3.sendSurvey.Resolution_Code__c = 'NOT SET';
		cce3.sendSurvey.Training_Issue__c = 'Yes';
		test.startTest();
        cce3.closeCase();
        test.stopTest();
    
		System.assert(ApexPages.hasMessages(),'You must set the Resolution Code');     
        
    	List<Case> updatedCase3 = [SELECT IsClosed
								FROM Case WHERE Id = :originalCase[0].Id LIMIT 1];
    
    	System.assertEquals(updatedCase3[0].IsClosed, false);
    }
    
    @isTest
    static void generalEnquiry_Test()
    {
        List<Case> originalCase = [SELECT IsClosed, QTRICS_Send_Survey__c, 
								Master_Control_ID__c, Sales_Order_Number__c, 
								Training_Part_Number__c, Complaint__c, Resolution_Code__c, 
								Training_Issue__c 
								FROM Case WHERE Subject = '4 TestGeneralInquiry'];
        
        ApexPages.StandardController sc4 = new ApexPages.StandardController(originalCase[0]);
		CloseCaseExtension cce4 = new CloseCaseExtension(sc4);

		cce4.comment.CommentBody = 'Testing a case comment is added';

		cce4.sendSurvey.QTRICS_Send_Survey__c = true;
		cce4.sendSurvey.Master_Control_ID__c = 'TestMC';
		cce4.sendSurvey.Sales_Order_Number__c = 'TestOrderNumber';
		cce4.sendSurvey.Training_Part_Number__c = 'TestPartNumber';
		cce4.sendSurvey.Complaint__c = true;
		cce4.sendSurvey.Resolution_Code__c = 'General inquiry';
		cce4.sendSurvey.Training_Issue__c = 'NOT SET';
		test.startTest();
		cce4.closeCase();
        test.stopTest();

		List<Case> updatedCase4 = [SELECT IsClosed, QTRICS_Send_Survey__c, 
								Master_Control_ID__c, Sales_Order_Number__c, 
								Training_Part_Number__c, Complaint__c, Resolution_Code__c, 
								Training_Issue__c 
								FROM Case WHERE Id = :originalCase[0].Id LIMIT 1];

		Case orCase4 = originalCase[0];
		Case upCase4 = updatedCase4[0];

		//make sure all of the fields have been updated
		System.assertNotEquals(upCase4.IsClosed, orCase4.IsClosed);
		//System.assertNotEquals(upCase4.QTRICS_Send_Survey__c, orCase4.QTRICS_Send_Survey__c);
		System.assertEquals(upCase4.Master_Control_ID__c, 'TestMC');
		System.assertEquals(upCase4.Sales_Order_Number__c, 'TestOrderNumber');
		System.assertEquals(upCase4.Training_Part_Number__c, 'TestPartNumber');
		System.assertNotEquals(upCase4.Complaint__c, orCase4.Complaint__c);
		System.assertEquals(upCase4.Resolution_Code__c, 'General inquiry');
		System.assertEquals(upCase4.Training_Issue__c, 'NOT SET');

		//test that the comment was added
		List<CaseComment> addedComment4 = [SELECT CommentBody FROM CaseComment WHERE ParentId = :originalCase[0].Id LIMIT 1];

		System.assertEquals(addedComment4[0].CommentBody, 'Testing a case comment is added');
    }
    
    @isTest
    static void notGeneralEnquiry_Test()
    {
        List<Case> originalCase = [SELECT IsClosed, QTRICS_Send_Survey__c, 
								Master_Control_ID__c, Sales_Order_Number__c, 
								Training_Part_Number__c, Complaint__c, Resolution_Code__c, 
								Training_Issue__c 
								FROM Case WHERE Subject = '5 TestNotGeneralInquiry'];
        
        ApexPages.StandardController sc5 = new ApexPages.StandardController(originalCase[0]);
		CloseCaseExtension cce5 = new CloseCaseExtension(sc5);
    	cce5.comment.CommentBody = 'Testing a case comment is added';
		cce5.sendSurvey.QTRICS_Send_Survey__c = true;
		cce5.sendSurvey.Master_Control_ID__c = 'TestMC';
		cce5.sendSurvey.Sales_Order_Number__c = 'TestOrderNumber';
		cce5.sendSurvey.Training_Part_Number__c = 'TestPartNumber';
		cce5.sendSurvey.Complaint__c = true;
		cce5.sendSurvey.Resolution_Code__c = 'Licensing';
		cce5.sendSurvey.Training_Issue__c = 'NOT SET';
		test.startTest();
        cce5.closeCase();
        test.stopTest();
    
		System.assert(ApexPages.hasMessages(),'Declare if this case was related to a training issue');     
        
    	List<Case> updatedCase5 = [SELECT IsClosed
								FROM Case WHERE Id = :originalCase[0].Id LIMIT 1];
    
    	System.assertEquals(updatedCase5[0].IsClosed, false);
        
        //finally test the picklists
		List<Schema.PicklistEntry> resolutionCodesTest = Case.Resolution_Code__c.getDescribe().getPicklistValues();
		List<Schema.PicklistEntry> trainingIssueTest = Case.Training_Issue__c.getDescribe().getPicklistValues();
		//adding 1 to the count of options because we add the --None-- option when building the list
		System.assertEquals(resolutionCodesTest.size() + 1, cce5.getResCodes().size());
		System.assertEquals(trainingIssueTest.size() + 1, cce5.getTrainingIssue().size());
    }
    
}