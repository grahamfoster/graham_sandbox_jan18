/*
 *	OpportunityLineItemTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2016-08-11 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class OpportunityLineItemTriggerHandler extends TriggerHandler {

	public OpportunityLineItemTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void beforeInsert() {

	}
	override protected void beforeUpdate() {
		
	}
    override protected void afterInsert() {
		setOnOpportunity();     
	}
	override protected void afterUpdate() {
       	setOnOpportunity();
	}
	override protected void afterDelete() {
    	setOnOpportunity();
	}    
    
	// **************************************************************************
	// 		private methods
	// **************************************************************************
	
	// When Opportunity Line Items change, find any related Opportunity IB records (used for tracking contract renewals) and update these records.
	private void setOnOpportunity(){
        List <Id> liIds = new List <Id>();
        
        if(Trigger.isDelete){
            for(OpportunityLineItem li : (List<OpportunityLineItem>)Trigger.old){
            	if(String.isNotBlank(li.Opportunity_IB__c) )  {
                	liIds.add(li.Opportunity_IB__c);
                }
            }
            if(liIds.size() > 0){
                List <Opportunity_IB__c> updateIBs = new List <Opportunity_IB__c>([SELECT id, On_Opportunity__c FROM Opportunity_IB__c WHERE id IN :liIds]);
                for(Opportunity_IB__c oib :updateIBs){
                    oib.On_Opportunity__c = FALSE;
                }
                if(updateIBs.size() > 0){
                	update updateIBs;
                }
            } 
		} else {
            for(OpportunityLineItem li : (List<OpportunityLineItem>)Trigger.new){
            	if(String.isNotBlank(li.Opportunity_IB__c) )  {
                	liIds.add(li.Opportunity_IB__c);
                }
            }
            if(liIds.size() > 0){
                List <Opportunity_IB__c> updateIBs = new List <Opportunity_IB__c>([SELECT id, On_Opportunity__c FROM Opportunity_IB__c WHERE id IN :liIds]);
                for(Opportunity_IB__c oib :updateIBs){
                    oib.On_Opportunity__c = TRUE;
                }
                if(updateIBs.size() > 0){
                	update updateIBs;
                }
            }            
        }            
            
        
            

        
        
    }

}