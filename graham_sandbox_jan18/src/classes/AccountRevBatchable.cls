/**
** @author Daniel Lachcik
** @created Apr 25/2016
** batch job to set the PY Revenue Genereated from the YTD Revenue Generated field on Key Accounts every Jan 1
**
**/
global class AccountRevBatchable implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        string inVar='Key Account';
        string tempInput = '%' + inVar + '%';    
        String query = 'select Id, PY_Revenue_Generated__c, YTD_Revenue_Generated__c, Name from Account where name like : tempInput ';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {   
        List<Account> AccList = (List<Account>)scope;
       
        for(Account Acc : scope) {
                Acc.PY_Revenue_Generated__c = Acc.YTD_Revenue_Generated__c;
                Acc.YTD_Revenue_Generated__c = 0;
              } 
              update AccList;
    }
           
    global void finish(Database.BatchableContext BC)
    {
    system.debug('YTD copied to PY for Key Account!');
    }
}