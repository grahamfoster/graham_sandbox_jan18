/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 5/2016
**
** Handler for OnboardingContact trigger logic
**/
public with sharing class OnboardingContactTriggerHandler extends TriggerHandler
{
	override protected void afterInsert() {
	    updateLMSUser();
	}

	private void updateLMSUser() 
	{
		Set<Id> contactIds = new Set<Id>();
		for(OnBoarding_Contacts__c obc : (List<OnBoarding_Contacts__c>)Trigger.new)
		{
			if(obc.OnBoard_Contact__c != null && obc.Type__c == 'Primary Learner') contactIds.add(obc.OnBoard_Contact__c);
		}

		for(Id contactId : contactIds)
		{
			ContactTriggerHandler.UpdateLMSUserQueueable qb = new ContactTriggerHandler.UpdateLMSUserQueueable(new Contact(Id=contactId));
			System.enqueueJob(qb);
		}
	}
}