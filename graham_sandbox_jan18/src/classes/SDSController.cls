/**
** @author Reid Beckett, Cloudware Connections
** @created Jun 4/2014
**
** Helper implements shadowing of OpportunityContactRole to/from Opportunity_Contact_Role_Shadow__c
** 
**/
public class SDSController {
    public Id opportunityId {get;set;}
    public Opportunity opp {get;set;}
    public Boolean renderDetails {get;set;}
   
    
    public SDSController(ApexPages.StandardController stdController) {
        opportunityId = stdController.getId();
        try {
            loadOpp();
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'ERROR'));
        }
    }

    public PageReference init(){
        try {
            new OpportunityContactRoleShadowHelper().syncToShadow(new Set<Id>{ opportunityId });

            loadOpp();
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'ERROR'));
        }
        return null;
    }

    private void loadOpp(){
        renderDetails = false;

        opp = [select Id, Name, CreatedDate, LastModifiedDate, Owner.Name, AccountId, Account.Name, Customer_Type__c, Primary_Application__c, Market_Vertical__c,
            Tender__c, Competition__c, Competitor_Instrument_1_del__c, Competitor_2__c, Competitor_Instrument_2__c, Competitor_3__c, Competitor_Instrument_3__c,
            Product__c, Amount, Funding__c, CloseDate, Timing__c, Sales_Rep_Top5__c, Funding_Amount__c, StageName, Competitive_Position__c, Market_Segment__c,
            Competitor_1_Rank__c, Competitor_2_Rank__c, Competitor_3_Rank__c, Opportunity_Strategy__c, Customer_Situation__c, Implied_Needs__c,
            Implications__c, Explicit_Needs__c, Need_Payoff_Value_of_Solution__c, Problems__c, Strengths_Benefits__c, How_to_Maximize__c,
            Red_Flags_Risks__c, How_to_Minimize__c, Possible_Actions_from_Brainstorming__c, Missing_Information__c, Review_Team__c, On_Track_with_Action_Plan__c,
            Probability, On_Track_with_Action_Plan_Details__c,
            (select Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.Title, Contact__r.Account.Site, Degree_of_Influence__c, Role__c, Social_Style__c,
            Pro_AB_SCIEX__c, Personal_Explicit_Needs__c, Rating__c, Evidence_Supporting_Rating__c from Opportunity_Contact_Role_Shadows__r),
            (select CompetitorName, Strengths, Weaknesses from OpportunityCompetitors)
            from Opportunity where Id = :opportunityId];
        if(opp.Sales_Rep_Top5__c) renderDetails = true;
        else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Opportunity.Sales_Rep_Top5__c.getDescribe().getLabel() + ' must be checked'));
        }
    }

    public PageReference save(){ 
        try {
            update opp.Opportunity_Contact_Role_Shadows__r;
            update opp;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Changes saved successfully'));
            return new PageReference('/apex/SDS?id=' + opp.Id).setRedirect(true);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'ERROR'));
            return null;
        }
    }

    public PageReference saveAndReturn(){ 
        try {
            update opp.Opportunity_Contact_Role_Shadows__r;
            update opp;
            return new PageReference('/' + opp.Id).setRedirect(true);
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+'ERROR'));
            return null;
        }
    }

    public PageReference saveForPdf(){ 
        //try {
            update opp.Opportunity_Contact_Role_Shadows__r;
            update opp;

            //save as PDF
            //PageReference pdfPageRef = Page.SDSPdf;
            //pdfPageRef.getParameters().put('id',opp.Id);
            Page.SDSPdf.getParameters().put('id',opp.Id);
            //pdfPageRef.setRedirect(true);

            Attachment att = new Attachment(ParentId = opp.Id);
            att.Body = Test.isRunningTest() ? Blob.valueOf('') : Page.SDSPdf.getContent();
            //att.Name = 'SDS Sheet ' + DateTime.now().format('yyyy-MM-dd_HH_mm_ss') + '.pdf';
            att.Name = 'SDS Sheet ' + DateTime.now().format('yyyy-MM-dd_HH_mm_ss') + '.pdf';
            att.Description = 'Auto-generated PDF document of the Strategic Deal Sheet';
            att.IsPrivate = false;
            insert att;

            String pdfUrl = '/servlet/servlet.FileDownload?file=' + att.Id;
            return new PageReference(pdfUrl);

//          system.debug('pdf body:');
//          system.debug(att.Body.toString());

//          return null;
        //}catch(Exception e){
        //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        //  return null;
        //}
    }

    public PageReference cancel(){ 
        return new PageReference('/' + opp.Id).setRedirect(true);
    }

    public PageReference newContactRole() {
        PageReference savedPageRef = save();
        if(savedPageRef != null) {
            String url = '/p/opp/ContactRoleEditUi/e?oppid=' + opp.Id + '&retURL=' + EncodingUtil.urlEncode('/apex/SDS?id='+ opp.Id + '&retURL=' + EncodingUtil.urlEncode('/'+opp.Id, 'UTF-8'), 'UTF-8');
            return new PageReference(url).setRedirect(true);
        }else{
            return null;            
        }
    }
}