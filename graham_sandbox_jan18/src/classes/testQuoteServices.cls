/*********************************************************************
Name  : testQuoteServices  
Author: Appirio, Inc.
Date  : July 30, 2008 
Usage : Used to test the QuoteServices  

*********************************************************************/

public class testQuoteServices  {

  public static testMethod void checkQuoteServices() {

    QuoteServices.quoteHeader2  cls1 = new QuoteServices.quoteHeader2();
    QuoteServices.attachmentDetails  cls2 = new QuoteServices.attachmentDetails();
    QuoteServices.defaultShippingAddress  cls3 = new QuoteServices.defaultShippingAddress();
    QuoteServices.createSalesQuoteResponse  cls4 = new QuoteServices.createSalesQuoteResponse();
    QuoteServices.ABI_SalesQuotation_process_sq1Wsd_Port  cls5 = new QuoteServices.ABI_SalesQuotation_process_sq1Wsd_Port();
    QuoteServices.quoteHeader  cls6 = new QuoteServices.quoteHeader();
    QuoteServices.shippingAddress  cls7 = new QuoteServices.shippingAddress();
    QuoteServices.termsAndConditions  cls8 = new QuoteServices.termsAndConditions();
    QuoteServices.ArrayOflineItemDetails  cls9 = new QuoteServices.ArrayOflineItemDetails();
    QuoteServices.submitStatus  cls10 = new QuoteServices.submitStatus();
    QuoteServices.submitQuote  cls11 = new QuoteServices.submitQuote();
    QuoteServices.getSalesQuoteDtlsResponse  cls12 = new QuoteServices.getSalesQuoteDtlsResponse();
    QuoteServices.createSalesQuote  cls13 = new QuoteServices.createSalesQuote();
    QuoteServices.lineItemDetails  cls14 = new QuoteServices.lineItemDetails();
    QuoteServices.partnerData  cls15 = new QuoteServices.partnerData();
    QuoteServices.getSalesQuoteDtls  cls16 = new QuoteServices.getSalesQuoteDtls();
    QuoteServices.quoteHeader2  cls17 = new QuoteServices.quoteHeader2();
    QuoteServices.partnerInfo  cls18 = new QuoteServices.partnerInfo();
    QuoteServices.quoteDetails_SFDC  cls19 = new QuoteServices.quoteDetails_SFDC();
    QuoteServices.createQuote  cls20 = new QuoteServices.createQuote();
    QuoteServices.getSalesQuoteDtls  cls21 = new QuoteServices.getSalesQuoteDtls();
    QuoteServices.quoteInfo  cls22 = new QuoteServices.quoteInfo();
    QuoteServices.ArrayOfpartnerInfo  cls23 = new QuoteServices.ArrayOfpartnerInfo();
    QuoteServices.ArrayOfattachmentDetails  cls24 = new QuoteServices.ArrayOfattachmentDetails();
    QuoteServices.submitQuoteResponse  cls25 = new QuoteServices.submitQuoteResponse();
    QuoteServices.SalesQuote  cls26 = new QuoteServices.SalesQuote();

  }  
}