/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 29/2014
**
** Controller for custom VF page to search for service opportunities by serial number
** 
**/
public with sharing class ServiceOpportunitySearchController implements Pager {
	public static final String SEARCH_SOQL = 'select Id, Name, CloseDate, Shippable_Date__c, Amount, Contract_Number__c from Opportunity where RecordType.DeveloperName = \'Service\' ' +
		'and Id in (select OpportunityId from OpportunityLineItem where Serial_Number__c like :searchTerm)';
	public String serialNumber {get;set;}
	public List<OpportunityWrapper> results {get;set;}
	public Boolean displayNoResultsError {get;set;}

	Integer pageNumber = 1;
	Integer pageSize = 25;
	Integer totalNumberOfRecords;
	String sortColumn;
	String sortDirection;

	public ServiceOpportunitySearchController() {
		displayNoResultsError = false;
		pageNumber = 1;
	}

	public PageReference doSearch(){
		fetchRecords();
		return null;
	}

	public Boolean displayResultsPageBlock {
		get {
			return results != null && results.size() > 0;
		}
	}

	public class OpportunityWrapper {
		public Opportunity opp {get;set;}

		public OpportunityWrapper(Opportunity opp){
			this.opp = opp;
		}
	}

	/* pager methods */
	public Pager pager {
		get {
			return this;
		}
	}
	public void fetchRecords() {
		results = new List<OpportunityWrapper>();
		displayNoResultsError = false;
		if(String.isBlank(serialNumber)) {
			displayNoResultsError = true;
			SfdcUtil.addErrorMessageToPage('Please enter a serial number or part of the serial number');
			return;
		}
		String searchTerm = '%' + serialNumber + '%';
		String soql = SEARCH_SOQL;
		if(!String.isBlank(sortColumn) && !String.isBlank(sortDirection)) {
			soql += ' order by ' + sortColumn + ' ' + sortDirection;
		}

		for(Opportunity opp : Database.query(soql)) {
			results.add(new OpportunityWrapper(opp));
		}
		setTotalNumberOfRecords(results.size());
		if(results.size() == 0) {
			displayNoResultsError = results.size() == 0;
			SfdcUtil.addErrorMessageToPage('No results found');
		}
	}

	public List<Opportunity> getReportOpps() { return null; }
	public String getReport() { return null; }

	public PageReference nextAction() {
		fetchRecords();
		return null;
	}

	public PageReference previousAction() {
		fetchRecords();
		return null;
	}

	public PageReference sortAction() {
		fetchRecords();
		return null;
	}

	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Integer getPageNumber() {
		return this.pageNumber;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageSize() {
		return this.pageSize;
	}

	public void setTotalNumberOfRecords(Integer totalNumberOfRecords) {
		this.totalNumberOfRecords = totalNumberOfRecords;
	}
	
	public Integer getTotalNumberOfRecords() {
		return totalNumberOfRecords;
	}

	public Integer getNumberOfPages() {
		return Integer.valueOf(Math.floor((totalNumberOfRecords - 1)/pageSize) + 1);
	}
	
	public Boolean getHasNextPage() {
		return getNumberOfPages() > pageNumber;
	}

	public Boolean getHasPreviousPage() {
		return pageNumber > 1;
	}

	public void setSortColumn1(String sortColumn) {
		this.sortColumn = sortColumn;
	}

	public  String getSortColumn1() { 
		return sortColumn; 
	}

	public void setSortDirection1(String sortDirection) {
		this.sortDirection = sortDirection;
	}

	public String getSortDirection1() { 
		return sortDirection; 
	}

	public void setSortColumn2(String sortColumn) {
		//this.sortColumn = sortColumn;
	}

	public String getSortColumn2() { 
		//return sortColumn; 
		return null;
	}

	public void setSortDirection2(String sortDirection) {
		//this.sortDirection = sortDirection;
	}

	public String getSortDirection2() { 
		//return sortDirection; 
		return null;
	}

}