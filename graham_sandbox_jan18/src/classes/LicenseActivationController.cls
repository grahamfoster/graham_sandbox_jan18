public class LicenseActivationController {

    private ApexPages.StandardController internalController;
    private final Contact contact;
    
    public Boolean sendActivationEmail { get; set; }
    public String activationID { get; set; }
    public String computerID { get; set; }
    public Asset_Lookup__c assetLookup { get; set; }
    public Boolean detailsResponseSuccess { get; set; }
    public String detailsResponseMessage { get; set; }
    public String detailsResponseProductName { get; set; }
    public String detailsResponseProductVersion { get; set; }
    public Boolean hasActivationResponse { get; set; }
    public String activationProductNameLabel { get; set; }
    public String activationProductVersionLabel { get; set; }
    public String activationLicenseText { get; set; }
    public String activationLicenseFilename { get; set; }
    public String activationResponseMessage { get; set; }
    
    public LicenseActivationController(ApexPages.StandardController controller) {
        contact = (Contact)controller.getRecord();
        internalController = controller;
        assetLookup = new Asset_Lookup__c();
        sendActivationEmail = true;
    }
    
    public void ActivationIDChanged() {
        /*  public class DetailsResponse {
                public boolean success;
                public String message = new String();
                public String diagnostics;
                public String exception;
                public String productName;
                public String productVersion;
            
                public DetailsResponse() { }
            } */
        detailsResponseSuccess = false;
        detailsResponseMessage = 'An unknown error occurred. Please contact support.';
        detailsResponseProductName = null;
        detailsResponseProductVersion = null;
        
        try {
            HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
            List<LicenseActivationSettings__c> settingsList = Database.query('SELECT ActivationDetailsServletURI__c FROM LicenseActivationSettings__c WHERE Name = \'' + UserInfo.getOrganizationId() + '\'');
            LicenseActivationSettings__c settings = settingsList[0];
            String url = settings.ActivationDetailsServletURI__c + '?activationId=' + EncodingUtil.urlEncode(activationId, 'UTF-8'); 
            request.setEndpoint(url);
            Http http = new Http();
            HTTPResponse response = http.send(request);
            String jsonString = response.getBody();
            JSONParser parser = JSON.createParser(jsonString);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() != JSONToken.FIELD_NAME)
                    continue;
                String fieldName = parser.getText();
                if (fieldName == 'success') {
                    parser.nextToken();
                    detailsResponseSuccess = parser.getBooleanValue();
                }
                if (fieldName == 'message') {
                    parser.nextToken();
                    detailsResponseMessage = parser.getText();
                }
                if (fieldName == 'productName') {
                    parser.nextToken();
                    detailsResponseProductName = parser.getText();
                }
                if (fieldName == 'productVersion') {
                    parser.nextToken();
                    detailsResponseProductVersion = parser.getText();
                }
            }
        }
        catch (Exception e) {
            System.debug(e);
        }
    }
    
    public void activateLicense() {
        /*  public class ActivationResponse {
                public boolean success;
                public String message = new String();
                public String diagnostics;
                public String exception;
                public String licenseText;
                public String licenseFilename;
                public String productName;
                public String productVersion;
                public Map<String,String> customer = new HashMap<String,String>();
          
                public ActivationResponse() { }
            } */
        hasActivationResponse = true;
        activationProductNameLabel = null;
        activationProductVersionLabel = null;
        activationLicenseText = 'License.lic';
        activationLicenseFilename = null;
        activationResponseMessage = 'An unknown error occurred. Please contact support.';
        User activeUser = [SELECT Email FROM User WHERE Username = : UserInfo.getUserName() LIMIT 1];
        if (activeUser == null) {
            activationResponseMessage = 'Your active user account was deleted. Please contact support.';
            return;
        }
        if (activeUser.Email == null || activeUser.Email.length() <= 0) {
            activationResponseMessage = 'Your user account needs to have an email address set on it.';
            return;
        }
        if (assetLookup == null || assetLookup.Asset__c == null) {
            activationResponseMessage = 'You must select an asset.';
            return;
        }
        Asset selectedAsset = [SELECT SerialNumber FROM Asset WHERE Id = :assetLookup.Asset__c];
        if (selectedAsset == null) {
            activationResponseMessage = 'The asset you have selected has been deleted.';
            return;
        }
        if (selectedAsset.SerialNumber == null || selectedAsset.SerialNumber.length() <= 0) {
            activationResponseMessage = 'The asset you have selected must have a serial number applied to it.';
            return;
        }
        try {
            HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
            List<LicenseActivationSettings__c> settingsList = Database.query('SELECT ActivateLicenseServletURI__c FROM LicenseActivationSettings__c WHERE Name = \'' + UserInfo.getOrganizationId() + '\'');
            LicenseActivationSettings__c settings = settingsList[0];
            String url = settings.ActivateLicenseServletURI__c + '?activationId=' + EncodingUtil.urlEncode(activationID, 'UTF-8') 
            + '&computerId=' + EncodingUtil.urlEncode(computerID, 'UTF-8')
            + '&instrumentSN=' + EncodingUtil.urlEncode(selectedAsset.SerialNumber, 'UTF-8')
            + '&userEmail=' + EncodingUtil.urlEncode(activeUser.Email, 'UTF-8')
            + '&emailLicense=' + EncodingUtil.urlEncode((sendActivationEmail ? 'true' : 'false'), 'UTF-8')
            + '&lastName=' + EncodingUtil.urlEncode((contact.LastName != null ? contact.LastName : ''), 'UTF-8') 
            + '&state=' + EncodingUtil.urlEncode((contact.MailingState != null ? contact.MailingState : ''), 'UTF-8') 
            + '&addressLineOne=' + EncodingUtil.urlEncode((contact.MailingStreet != null ? contact.MailingStreet : ''), 'UTF-8') 
            + '&country=' + EncodingUtil.urlEncode((contact.MailingCountry != null ? contact.MailingCountry : ''), 'UTF-8') 
            + '&city=' + EncodingUtil.urlEncode((contact.MailingCity != null ? contact.MailingCity : ''), 'UTF-8') 
            + '&phoneNumber=' + EncodingUtil.urlEncode((contact.Phone != null ? contact.Phone : ''), 'UTF-8') 
            + '&email=' + EncodingUtil.urlEncode((contact.Email != null ? contact.Email : ''), 'UTF-8') 
            + '&zipCode=' + EncodingUtil.urlEncode((contact.MailingPostalCode != null ? contact.MailingPostalCode : ''), 'UTF-8') 
            + '&company=' + EncodingUtil.urlEncode(((contact.Account != null && contact.Account.Name != null) ? contact.Account.Name : ''), 'UTF-8') 
            + '&firstName=' + EncodingUtil.urlEncode((contact.FirstName != null ? contact.FirstName : ''), 'UTF-8');
            request.setEndpoint(url);
            Http http = new Http();
            HTTPResponse response = http.send(request);
            String jsonString = response.getBody();
            JSONParser parser = JSON.createParser(jsonString);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() != JSONToken.FIELD_NAME)
                    continue;
                String fieldName = parser.getText();
                if (fieldName == 'message') {
                    parser.nextToken();
                    activationResponseMessage = parser.getText();
                }
                if (fieldName == 'productName') {
                    parser.nextToken();
                    activationProductNameLabel = 'Product Name: ' + parser.getText();
                }
                if (fieldName == 'productVersion') {
                    parser.nextToken();
                    activationProductVersionLabel = 'Product Version: ' + parser.getText();
                }
                if (fieldName == 'licenseText') {
                    parser.nextToken();
                    activationLicenseText = parser.getText();
                }
                if (fieldName == 'licenseFilename') {
                    parser.nextToken();
                    activationLicenseFilename = parser.getText();
                }            
            }
        }
        catch (Exception e) { 
            System.debug(e);
        }
    }
}