/*
 *	AttachmentTrigger_Test
 *	
 *	Test class for AttachmentTrigger and AttachmentTriggerHandler.
 *
 *	If there are other test classes related to AttachmentTrigger, please document it here (as comments).
 * 
 * 	Created by Brett Moore 2016-05-20 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
private class AttachmentTrigger_Test {

	// IUDU (Insert Update Delete Undelete) test
        @isTest static void test() {

    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'United States');
        insert testAccount;            
    	
        workOrderFSRemailNotifications__c testWON = new workOrderFSRemailNotifications__c(email_1__c = 'Test@test.com',email_2__c = 'Test@test.com',SEP_Email__c= 'Test@test.com', Name = 'US',Operating_Unit__c='SX US OU');
        insert testWON;
        
        SVMXC__Service_Order__c testWO = new SVMXC__Service_Order__c(CurrencyIsoCode='USD',FSR_Added__c = FALSE);
        insert testWO;
        
        String fileData = 'Some test File Data';
		blob myBlob = Blob.valueOf(fileData);            
		Attachment testAtt1 = new Attachment(parentId=testAccount.id,name='myfile.doc', body=myBlob);            
		Attachment testAtt2 = new Attachment(parentId=testAccount.id,name='Sciex_FSR.pdf', body=myBlob);         
		Attachment testAtt3 = new Attachment(parentId=testWO.id,name='myfile.doc', body=myBlob);            
		Attachment testAtt4 = new Attachment(parentId=testWO.id,name='Sciex_FSR.pdf', body=myBlob);                        
// Insert Non-WorkOrder Atachment with random filename
	    insert testAtt1;
// Insert Non-WorkOrder Atachment with FSR filename
		insert testAtt2;
// Insert WorkOrder Atachment with random filename
		insert testAtt3;
// Insert WorkOrder Atachment with FSR filename 
		insert testAtt4;
         
        testAtt4.name = 'newFilename.txt';
		update testAtt4;
		delete testAtt4;
		undelete testAtt4;            
     }

	 @IsTest static void test_notifyCaseOwnerOfPortalAttachment()
	 {
		Account ac = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert ac;
		Contact c = new Contact(FirstName = 'J', LastName = 'Doe', Email = 'jdoe@example.com', AccountId = ac.Id);
		insert c;
		Case c1 = new Case(Subject = 'Test', Description = 'Case', ContactId = c.Id);
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', FirstName = 'SCIEXNow', LastName='Portal', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
        insert new List<SObject>{u, c1};
		//test adding an attachment to the case
		System.runAs(u)
		{
			String fileData = 'Some test File Data';
			blob myBlob = Blob.valueOf(fileData);
			Attachment a = new Attachment(ParentId = c1.Id, name='myfile.doc', body=myBlob);
			Test.startTest();
			insert a;
			Integer invocations = Limits.getEmailInvocations();
			Test.stopTest();
			system.assertEquals(1, invocations);
		}
		
	 }



}