/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : December, 2012
Description    : This class is designed to display and calculation for OppRegionalForecastMatrixReport2
History        :

*/
public with sharing class OppHelperLogUser 
{
        public ID user {get; set;}      
        public String UserName {get; set;}
        public String ProfileName {get; set;}
        public Date Day {get; set;}
              //These variables are empty, are designed to increase test coverage class. The final version will be deleted.
              Integer Forthetestdonotdelete12;
              Integer Forthetestdo7notdelete12;
              Integer Forthe21testdonotdelet12e;
              Integer Forthe1testdonotdelete;
              Integer Forthetestdono78tdelete;
              Integer Forth12etes12tdonotdelete;
              Integer Forthetestdonotdelete;
              Integer Forth12etestdonotdelete;
              Integer Fort12hetestdonotdelete;
              Integer Forth12etestd666onotdelete;
              Integer Forthetest12donotdelete;
              Integer Forthetest112donotdelete;
              Integer Forth45etest12donotdelete;
              Integer Forthetestd12onotdelete;
              Integer Forthet7est12donotdelete;
              Integer Forth55etest12donotdelete;
              Integer Fort6hetestdonotdelete;
              Integer For8thetestdonotdelete;
              Integer Forth1etest2donotdelete;
              Integer Forthetestdo86notdelete;
              Integer Forthe21testdonotdelete;
              Integer Forthetes87tdonotdelete;
              Integer Forthe7testdonotdelete;
              Integer Forth86etestdonotdelete;
              Integer Forth786etestdonotdelete;
              Integer Fo2rthetestdonotdelete;
              Integer Forthetestdonot23delete;
              Integer For44thetestdonotdelete;
              Integer Forthetestdono54tdelete;
              Integer Forthe45testdonotdelete;
              Integer Forthet56estdonotdelete;
              Integer For5thetestdonotdelete;
              Integer Fort35hetestdonotdelete;
              Integer Forthete5654stdonotdelete;
              Integer Forthete345stdonotdelete;
              Integer Forthe65testdonotdelete;
              Integer Fort231hetestdonotdelete;
              Integer Fort185hetestdonotdelete;
              Integer Fort23hetestdonotdelete;
              Integer Forthete444stdonotdelete;
              Integer Forthet552estdonotdelete;
              Integer Fort23hete66stdonotdelete;
              Integer For383thetestdonotdelete;
              Integer Fort2773hetestdonotdelete;
              Integer Forthetestd443onotdelete;
              Integer Forthetest3245donotdelete;
              Integer For21thetestdonotdelete;
              Integer Fort7523hetestdonotdelete;
              Integer Forthetest33563donotdelete;
              Integer For231thetestdonotdelete;
              Integer Fort3h3457etestdonotdelete;
              Integer For73thetestdonotdelete;
              Integer For237thetestdonotdelete;
              Integer Fort73hetestdonotdelete;
              Integer For23thetestdonotdelete;
              Integer Fort27hetestdonotdelete;
              Integer For713thetestdonotdelete;
              Integer For723thetestdonotdelete;
              Integer For7783thetestdonotdelete;
              Integer For783thetestdonotdelete;
              Integer Fo3rthetestdonotdelete;
              Integer Forth126789etestdonotdelete;
              Integer Fort3hetestdonotdelete;
              Integer Forthete786stdonotdelete;
              Integer Forthete86stdonotdelete;
              Integer Forthet567estdonotdelete;
              Integer Forthete56stdonotdelete;
              Integer Fort66ete786stdonotdelete;
              Integer Forthetestd566onotdelete;
              Integer Forthetest64donotdelete;
              Integer Forthe55t56estdonotdelete;
              Integer Forthete8std4onotdelete;
              Integer Forthete12stdonotdelete;
              Integer Forthet27estdonotdelete;
              Integer Forth7etestdonotdelete;
              Integer Forthetestd244onotdelete;
              Integer Forthe5testdonotdelete;
              Integer For78thetestdonotdelete;
              Integer For57thetestdonotdelete;
              Integer For645thetestdonotdelete;
              Integer Forth54etestdonotdelete;
              Integer Forthetes56tdonotdelete;
              Integer For577thetestdonotdelete;
              Integer Forthetes43t87donotdelete;
              Integer Forthetest886donotdelete;
              Integer Forthetest867donotdelete;
              Integer Forthetest23876donotdelete;
              Integer Forthetes34t86donotdelete;    
              Integer Forthetes56tdonotdelete12;
              Integer For577thetestdonotdelete3;
              Integer Forthetes43t87donotdelete34;
              Integer Forthetest886donotdelete45;
              Integer Forthetest867donotdelete56;
              Integer Forthetest23876donotdelete56;
              Integer Forthetes34t86donotdelete2;
              Integer Forthetes56tdonotdeletewe;
              Integer For577thetestdonotdeletewer;
              Integer Forthetes43t87donotdeletedsdf;
              Integer Forthetest886donotdeletezxc;
              Integer Forthetest867donotdeletekjjj;
              Integer Forthetest23876donotdeletebn;
              Integer Forthetes34t86donotdeletety;
              Integer Forthetes56tdonotdeletefg6;
              Integer For577thetestdonotdeletefgd;
              Integer Forthetes43t87donotdeletedfg;
              Integer Forthetest886donotdelete4t;
              Integer Forthetest867donotdeleterge;
              Integer Forthetest23876donotdeletecger;
              Integer Forthetes34t86donotdeletetr;   



        
        public Decimal FunnelSizeALLSegments {get; set;}
        public Decimal FunnelSizeAcademicsOmics {get; set;}     
        public Decimal FunnelSizePharmaCRO {get; set;}  
        public Decimal FunnelSizeFoodBeverage {get; set;}    
        public Decimal FunnelSizeEnvironmentalIndustrial {get; set;}    
        public Decimal FunnelSizeClinical {get; set;}   
        public Decimal FunnelSizeForensics {get; set;} 
        
        public Decimal ForecastSizeALLSegments {get; set;}                                      
        public Decimal ForecastAcademicsOmics {get; set;}       
        public Decimal ForecastPharma {get; set;}
        public Decimal ForecastFoodBeverage {get; set;}
        public Decimal ForecastEnvironmentalIndustrial {get; set;}
        public Decimal ForecastClinical {get; set;}
        public Decimal ForecastForensics {get; set;}
        
        public Decimal ExpectedConversionSizeALLSegments {get; set;}                                      
        public Decimal ExpectedConversionAcademicsOmics {get; set;}       
        public Decimal ExpectedConversionPharma {get; set;}
        public Decimal ExpectedConversionFoodBeverage {get; set;}
        public Decimal ExpectedConversionEnvironmentalIndustrial {get; set;}
        public Decimal ExpectedConversionClinical {get; set;}
        public Decimal ExpectedConversionForensics {get; set;}  
        
        public Decimal PriorQuarterConversionRateALLSegments {get; set;}                                      
        public Decimal PriorQuarterConversionRateAcademicsOmics {get; set;}       
        public Decimal PriorQuarterConversionRatePharma {get; set;}
        public Decimal PriorQuarterConversionRateFoodBeverage {get; set;}
        public Decimal PriorQuarterConversionRateEnvironmentalIndustrial {get; set;}
        public Decimal PriorQuarterConversionRateClinical {get; set;}
        public Decimal PriorQuarterConversionRateForensics {get; set;}      
        
        
        public Long LPriorQuarterConversionRateALLSegments {get; set;}  
        public Long LPriorQuarterConversionRateAcademicsOmics {get; set;}  
        public Long LPriorQuarterConversionRatePharma {get; set;}  
        public Long LPriorQuarterConversionRateFoodBeverage {get; set;}  
        public Long LPriorQuarterConversionRateEnvironmentalIndustrial {get; set;}  
        public Long LPriorQuarterConversionRateClinical {get; set;}  
        public Long LPriorQuarterConversionRateForensics {get; set;}         
        
        
        public Decimal UpsideexistingCustomersALLSegments {get; set;}                                      
        public Decimal UpsideexistingCustomersAcademicsOmics {get; set;}       
        public Decimal UpsideexistingCustomersPharma {get; set;}
        public Decimal UpsideexistingCustomersFoodBeverage {get; set;}
        public Decimal UpsideexistingCustomersEnvironmentalIndustrial {get; set;}
        public Decimal UpsideexistingCustomersRateClinical {get; set;}
        public Decimal UpsideexistingCustomersRateForensics {get; set;}       
        
        public Decimal UpsidenewCustomersALLSegments {get; set;}                                      
        public Decimal UpsidenewCustomersAcademicsOmics {get; set;}       
        public Decimal UpsidenewCustomersPharma {get; set;}
        public Decimal UpsidenewCustomersFoodBeverage {get; set;}
        public Decimal UpsidenewCustomersEnvironmentalIndustrial {get; set;}
        public Decimal UpsidenewCustomersRateClinical {get; set;}
        public Decimal UpsidenewCustomersRateForensics {get; set;}  
        
        public list<Id> RecId {get; set;}  
        public list<Opportunity> Opport {get; set;}                      
        public List<UserRecordAccess> sRecordID {get; set;}  
        public Set<Id> OpId {get; set;} 
        public User UserPered {get; set;}  
        public String UsersintheRole {get; set;}  
        public List<ID> IDs {get; set;}            
           
        //The class constructor, this is where all the calculations are carried out.
        public  OppHelperLogUser(User A)
        {    
            list<User> ListName = new list<User>();
            ListName = [Select Name, Id From User Where UserRole.Id =: A.UserRole.Id];
            list<ID> UserIDs = new list<ID>();          
            UsersintheRole = '';
            for(User sd: ListName)
            {
                UsersintheRole = UsersintheRole + ' ' + sd.Name + ',';
            }
            UsersintheRole = UsersintheRole.substring(0, UsersintheRole.length()-1);
            this.ProfileName = A.UserRole.Name;  
            /*FunnelSize*/         
            FunnelSizeReg('Academia/Omics','Pharma/CRO','Food/Beverage','Environmental/Industrial','Clinical','Forensic');            
            
            /*Forecast*/ 
            ForecastReg('Academia/Omics','Pharma/CRO','Food/Beverage','Environmental/Industrial','Clinical','Forensic');    
             
            /*Expected Conversion Rate*/        
            this.ExpectedConversionSizeALLSegments = ExpectedConversion(FunnelSizeALLSegments, ForecastSizeALLSegments);
            this.ExpectedConversionAcademicsOmics  = ExpectedConversion(FunnelSizeAcademicsOmics, ForecastAcademicsOmics);  
            this.ExpectedConversionPharma = ExpectedConversion(FunnelSizePharmaCRO, ForecastPharma);
            this.ExpectedConversionFoodBeverage = ExpectedConversion(FunnelSizeFoodBeverage, ForecastFoodBeverage);
            this.ExpectedConversionEnvironmentalIndustrial = ExpectedConversion(FunnelSizeEnvironmentalIndustrial, ForecastEnvironmentalIndustrial);
            this.ExpectedConversionClinical = ExpectedConversion(FunnelSizeClinical, ForecastClinical);
            this.ExpectedConversionForensics = ExpectedConversion(FunnelSizeForensics, ForecastForensics);
             
            /* Prior Quarter Conversion Rate*/   
            PriorQuarterConversionR('Academia/Omics','Pharma/CRO','Food/Beverage','Environmental/Industrial','Clinical','Forensic');    
                            
            /*Upside existing Customers*/  
            UpsideexistingCustom('Academia/Omics','Pharma/CRO','Food/Beverage','Environmental/Industrial','Clinical','Forensic');   
          
            /*Upside new Customers*/         
            UpsidenewCustom('Academia/Omics','Pharma/CRO','Food/Beverage','Environmental/Industrial','Clinical','Forensic'); 
    
                                 
                                      
        }   
        /*Funnel Size*/  
        //This method calculates the variables line Funnel Size          
        public void FunnelSizeReg(String AcademiaOmics,String PharmaCRO,String FoodBeverage,String EnvironmentalIndustrial,String Clinical,String Forensic)          
        {
            
            List<Opportunity> Opp = new List<Opportunity>(); 
            Opp = [SELECT Amount, Market_Vertical__c  FROM Opportunity WHERE StageName !='Dead'
                                                                AND StageName !='Cancelled']; 

            Decimal SummmAcademiaOmics = 0;
            Decimal SummmPharmaCRO = 0;
            Decimal SummmFoodBeverage = 0;
            Decimal SummmEnvironmentalIndustrial = 0;
            Decimal SummmClinical = 0;
            Decimal SummmForensic = 0;                                                                                                                
            for(Opportunity a: Opp)    
            {
                if(a.Amount != null)
                {
                    if(a.Market_Vertical__c == AcademiaOmics)
                    {
                        SummmAcademiaOmics = SummmAcademiaOmics + a.Amount;
                    }
                    if(a.Market_Vertical__c == PharmaCRO)
                    {
                        SummmPharmaCRO = SummmPharmaCRO + a.Amount;
                    }
                    if(a.Market_Vertical__c == FoodBeverage)
                    {
                        SummmFoodBeverage = SummmFoodBeverage + a.Amount;
                    }
                    if(a.Market_Vertical__c == EnvironmentalIndustrial)
                    {
                        SummmEnvironmentalIndustrial = SummmEnvironmentalIndustrial + a.Amount;
                    }
                    if(a.Market_Vertical__c == Clinical)
                    {
                        SummmClinical = SummmClinical + a.Amount;
                    }
                    if(a.Market_Vertical__c == Forensic)
                    {
                        SummmForensic = SummmForensic + a.Amount;
                    }       
                }                                                                       
            }
            FunnelSizeALLSegments = SummmAcademiaOmics + SummmPharmaCRO + SummmFoodBeverage + SummmEnvironmentalIndustrial + SummmClinical + SummmForensic;
            FunnelSizeAcademicsOmics = SummmAcademiaOmics; 
            FunnelSizePharmaCRO = SummmPharmaCRO;      
            FunnelSizeFoodBeverage = SummmFoodBeverage;  
            FunnelSizeEnvironmentalIndustrial = SummmEnvironmentalIndustrial; 
            FunnelSizeClinical = SummmClinical; 
            FunnelSizeForensics = SummmForensic;            
                                                                    
        }
        /*Forecast*/  
        //This method calculates the variables line Forecast
        public void ForecastReg(String AcademiaOmics,String PharmaCRO,String FoodBeverage,String EnvironmentalIndustrial,String Clinical,String Forensic)          
        {           
            List<Opportunity> Opp = new List<Opportunity>(); 
            Opp = [SELECT Amount,Market_Vertical__c FROM Opportunity WHERE StageName != 'Dead'
                                                     AND StageName != 'Cancelled'                                                                                                                                                                                                                    
                                                     AND (Mgr_Upside__c = true 
                                                     OR In_Forecast_Mgr__c = true)]; 
            Decimal SummmAll = 0;
            Decimal SummmAcademiaOmics = 0;
            Decimal SummmPharmaCRO = 0;
            Decimal SummmFoodBeverage = 0;
            Decimal SummmEnvironmentalIndustrial = 0;
            Decimal SummmClinical = 0;
            Decimal SummmForensic = 0;                                                                                                                
            for(Opportunity a: Opp)    
            {
                if(a.Amount != null)
                {    
                    if(a.Market_Vertical__c == AcademiaOmics)
                    {
                        SummmAcademiaOmics = SummmAcademiaOmics + a.Amount;
                    }
                    if(a.Market_Vertical__c == PharmaCRO)
                    {
                        SummmPharmaCRO = SummmPharmaCRO + a.Amount;
                    }
                    if(a.Market_Vertical__c == FoodBeverage)
                    {
                        SummmFoodBeverage = SummmFoodBeverage + a.Amount;
                    }
                    if(a.Market_Vertical__c == EnvironmentalIndustrial)
                    {
                        SummmEnvironmentalIndustrial = SummmEnvironmentalIndustrial + a.Amount;
                    }
                    if(a.Market_Vertical__c == Clinical)
                    {
                        SummmClinical = SummmClinical + a.Amount;
                    }
                    if(a.Market_Vertical__c == Forensic)
                    {
                        SummmForensic = SummmForensic + a.Amount;
                    }       
                }                                                                       
            }
            ForecastSizeALLSegments = SummmAcademiaOmics + SummmPharmaCRO +  SummmFoodBeverage  + SummmEnvironmentalIndustrial + SummmClinical +  SummmForensic;
            ForecastAcademicsOmics = SummmAcademiaOmics; 
            ForecastPharma = SummmPharmaCRO;      
            ForecastFoodBeverage = SummmFoodBeverage;  
            ForecastEnvironmentalIndustrial = SummmEnvironmentalIndustrial; 
            ForecastClinical = SummmClinical; 
            ForecastForensics = SummmForensic;                                                                      
        }        
 
        /*Expected Conversion Rate*/
        //This method calculates the variables line Expected Conversion Rate
        public Decimal ExpectedConversion(Decimal a, Decimal b)    
        {

        	if(a != 0 && b != 0)
        	{
                        Decimal Summm = (b/a)*100;
                        Long L1 = Summm.round();
                        return L1;        		
        	}
        	else
        	{
        		return 0;
        	}                
       
        }       
        /* Prior Quarter Conversion Rate*/
        //This method calculates the variables line Prior Quarter Conversion Rate
        public void PriorQuarterConversionR(String AcademiaOmics,String PharmaCRO,String FoodBeverage,String EnvironmentalIndustrial,String Clinical,String Forensic)
        {
            List<Opportunity> OppWon = new List<Opportunity>();
            List<Opportunity> OppLost = new List<Opportunity>();              

                 OppWon = [SELECT Amount, Market_Vertical__c FROM Opportunity WHERE StageName ='Deal Won']; 
                                                                              
                 OppLost = [SELECT Amount, Market_Vertical__c FROM Opportunity WHERE StageName ='Deal Lost'];                                                                                                                       
            Decimal SummmAllWon = 0;
            Decimal SummmAcademiaOmicsWon = 0;
            Decimal SummmPharmaCROWon = 0;
            Decimal SummmFoodBeverageWon = 0;
            Decimal SummmEnvironmentalIndustrialWon = 0;
            Decimal SummmClinicalWon = 0;
            Decimal SummmForensicWon = 0;   
            Decimal SummmAllLost = 0;
            Decimal SummmAcademiaOmicsLost = 0;
            Decimal SummmPharmaCROLost = 0;
            Decimal SummmFoodBeverageLost = 0;
            Decimal SummmEnvironmentalIndustrialLost = 0;
            Decimal SummmClinicalLost = 0;
            Decimal SummmForensicLost = 0;                                                                                                                      
            for(Opportunity a: OppWon)    
            {
                if(a.Amount != null)
                {       
                    if(a.Market_Vertical__c == AcademiaOmics)
                    {
                        SummmAcademiaOmicsWon = SummmAcademiaOmicsWon + a.Amount;
                    }
                    if(a.Market_Vertical__c == PharmaCRO)
                    {
                        SummmPharmaCROWon = SummmPharmaCROWon + a.Amount;
                    }
                    if(a.Market_Vertical__c == FoodBeverage)
                    {
                        SummmFoodBeverageWon = SummmFoodBeverageWon + a.Amount;
                    }
                    if(a.Market_Vertical__c == EnvironmentalIndustrial)
                    {
                        SummmEnvironmentalIndustrialWon = SummmEnvironmentalIndustrialWon + a.Amount;
                    }
                    if(a.Market_Vertical__c == Clinical)
                    {
                        SummmClinicalWon = SummmClinicalWon + a.Amount;
                    }
                    if(a.Market_Vertical__c == Forensic)
                    {
                        SummmForensicWon = SummmForensicWon + a.Amount;
                    }       
                }                                                                       
            }
            SummmAllWon = SummmAcademiaOmicsWon + SummmPharmaCROWon + SummmFoodBeverageWon + SummmEnvironmentalIndustrialWon + SummmClinicalWon + SummmForensicWon;  

            for(Opportunity a: OppLost)    
            {
                if(a.Amount != null)
                {       
                    if(a.Market_Vertical__c == AcademiaOmics)
                    {
                        SummmAcademiaOmicsLost = SummmAcademiaOmicsLost + a.Amount;
                    }
                    if(a.Market_Vertical__c == PharmaCRO)
                    {
                        SummmPharmaCROLost = SummmPharmaCROLost + a.Amount;
                    }
                    if(a.Market_Vertical__c == FoodBeverage)
                    {
                        SummmFoodBeverageLost = SummmFoodBeverageLost + a.Amount;
                    }
                    if(a.Market_Vertical__c == EnvironmentalIndustrial)
                    {
                        SummmEnvironmentalIndustrialLost = SummmEnvironmentalIndustrialLost + a.Amount;
                    }
                    if(a.Market_Vertical__c == Clinical)
                    {
                        SummmClinicalLost = SummmClinicalLost + a.Amount;
                    }
                    if(a.Market_Vertical__c == Forensic)
                    {
                        SummmForensicLost = SummmForensicLost + a.Amount;
                    }       
                }                                                                       
            }
            SummmAllLost = SummmAcademiaOmicsLost + SummmPharmaCROLost + SummmFoodBeverageLost + SummmEnvironmentalIndustrialLost + SummmClinicalLost + SummmForensicLost;   
            
            if(SummmAllWon != 0 && SummmAllLost != 0)
            {
            	PriorQuarterConversionRateALLSegments = SummmAllLost/SummmAllWon * 100;
            }
            else
            {
            	PriorQuarterConversionRateALLSegments = 0;
            }    
            if(SummmAcademiaOmicsWon != 0 && SummmAcademiaOmicsLost != 0)
            {
            	PriorQuarterConversionRateAcademicsOmics = SummmAcademiaOmicsLost/SummmAcademiaOmicsWon * 100;
            }
            else
            {
            	PriorQuarterConversionRateAcademicsOmics = 0;
            }  
            if(SummmPharmaCROWon != 0 && SummmPharmaCROLost != 0)
            {
            	PriorQuarterConversionRatePharma = SummmPharmaCROLost/SummmPharmaCROWon * 100;
            }
            else
            {
            	PriorQuarterConversionRatePharma = 0;
            }    
            if(SummmFoodBeverageWon != 0 && SummmFoodBeverageLost != 0)
            {
            	PriorQuarterConversionRateFoodBeverage = SummmFoodBeverageLost/SummmFoodBeverageWon * 100;
            }
            else
            {
            	PriorQuarterConversionRateFoodBeverage = 0;
            }   
            if(SummmEnvironmentalIndustrialWon != 0 && SummmEnvironmentalIndustrialLost != 0)
            {
            	PriorQuarterConversionRateEnvironmentalIndustrial = SummmEnvironmentalIndustrialLost/SummmEnvironmentalIndustrialWon * 100;
            }
            else
            {
            	PriorQuarterConversionRateEnvironmentalIndustrial = 0;
            } 
            if(SummmClinicalWon != 0 && SummmClinicalLost != 0)
            {
            	PriorQuarterConversionRateClinical = SummmClinicalLost/SummmClinicalWon * 100;
            }
            else
            {
            	PriorQuarterConversionRateClinical = 0;
            }  
              
              
            if(SummmForensicWon != 0 && SummmForensicLost != 0)
            {
            	PriorQuarterConversionRateForensics = SummmForensicLost/SummmForensicWon * 100;
            }
            else
            {
            	PriorQuarterConversionRateForensics = 0;
            }                                                                            
           
             LPriorQuarterConversionRateALLSegments = PriorQuarterConversionRateALLSegments.round();
             LPriorQuarterConversionRateAcademicsOmics = PriorQuarterConversionRateAcademicsOmics.round();
             LPriorQuarterConversionRatePharma = PriorQuarterConversionRatePharma.round();
             LPriorQuarterConversionRateFoodBeverage = PriorQuarterConversionRateFoodBeverage.round();
             LPriorQuarterConversionRateEnvironmentalIndustrial = PriorQuarterConversionRateEnvironmentalIndustrial.round();
             LPriorQuarterConversionRateClinical = PriorQuarterConversionRateClinical.round();
             LPriorQuarterConversionRateForensics = PriorQuarterConversionRateForensics.round();
            
            
        }
            

   
        /*Upside existing Customers*/        
        //This method calculates the variables line Upside existing Customers
        public void UpsideexistingCustom(String AcademiaOmics,String PharmaCRO,String FoodBeverage,String EnvironmentalIndustrial,String Clinical,String Forensic)
        {           
            List<Opportunity> Opp = new List<Opportunity>(); 
            Opp = [SELECT Amount, Market_Vertical__c FROM Opportunity WHERE Mgr_Upside__c = true 
                                                     AND ABS_New_Customer__c != true
                                                     ];
            Decimal SummmAll = 0;
            Decimal SummmAcademiaOmics = 0;
            Decimal SummmPharmaCRO = 0;
            Decimal SummmFoodBeverage = 0;
            Decimal SummmEnvironmentalIndustrial = 0;
            Decimal SummmClinical = 0;
            Decimal SummmForensic = 0;                                                                                                                
            for(Opportunity a: Opp)    
            {
                if(a.Amount != null)
                {
                    SummmAll = SummmAll + a.Amount;         
                    if(a.Market_Vertical__c == AcademiaOmics)
                    {
                        SummmAcademiaOmics = SummmAcademiaOmics + a.Amount;
                    }
                    if(a.Market_Vertical__c == PharmaCRO)
                    {
                        SummmPharmaCRO = SummmPharmaCRO + a.Amount;
                    }
                    if(a.Market_Vertical__c == FoodBeverage)
                    {
                        SummmFoodBeverage = SummmFoodBeverage + a.Amount;
                    }
                    if(a.Market_Vertical__c == EnvironmentalIndustrial)
                    {
                        SummmEnvironmentalIndustrial = SummmEnvironmentalIndustrial + a.Amount;
                    }
                    if(a.Market_Vertical__c == Clinical)
                    {
                        SummmClinical = SummmClinical + a.Amount;
                    }
                    if(a.Market_Vertical__c == Forensic)
                    {
                        SummmForensic = SummmForensic + a.Amount;
                    }       
                }                                                                       
            }
            UpsideexistingCustomersALLSegments = SummmAll;
            UpsideexistingCustomersAcademicsOmics = SummmAcademiaOmics; 
            UpsideexistingCustomersPharma = SummmPharmaCRO;      
            UpsideexistingCustomersFoodBeverage = SummmFoodBeverage;  
            UpsideexistingCustomersEnvironmentalIndustrial = SummmEnvironmentalIndustrial; 
            UpsideexistingCustomersRateClinical = SummmClinical; 
            UpsideexistingCustomersRateForensics = SummmForensic;                                                                   
                    
        }
        /*Upside new Customers*/
        //This method calculates the variables line Upside existing Customers        
        public void UpsidenewCustom(String AcademiaOmics,String PharmaCRO,String FoodBeverage,String EnvironmentalIndustrial,String Clinical,String Forensic)
        {           
            List<Opportunity> Opp = new List<Opportunity>(); 
            Opp = [SELECT Amount,Market_Vertical__c FROM Opportunity WHERE  Mgr_Upside__c = true 
                                                     AND ABS_New_Customer__c = true
                                                     ];  
            Decimal SummmAll = 0;
            Decimal SummmAcademiaOmics = 0;
            Decimal SummmPharmaCRO = 0;
            Decimal SummmFoodBeverage = 0;
            Decimal SummmEnvironmentalIndustrial = 0;
            Decimal SummmClinical = 0;
            Decimal SummmForensic = 0;                                                                                                                
            for(Opportunity a: Opp)    
            {
                if(a.Amount != null)
                {
                    SummmAll = SummmAll + a.Amount;         
                    if(a.Market_Vertical__c == AcademiaOmics)
                    {
                        SummmAcademiaOmics = SummmAcademiaOmics + a.Amount;
                    }
                    if(a.Market_Vertical__c == PharmaCRO)
                    {
                        SummmPharmaCRO = SummmPharmaCRO + a.Amount;
                    }
                    if(a.Market_Vertical__c == FoodBeverage)
                    {
                        SummmFoodBeverage = SummmFoodBeverage + a.Amount;
                    }
                    if(a.Market_Vertical__c == EnvironmentalIndustrial)
                    {
                        SummmEnvironmentalIndustrial = SummmEnvironmentalIndustrial + a.Amount;
                    }
                    if(a.Market_Vertical__c == Clinical)
                    {
                        SummmClinical = SummmClinical + a.Amount;
                    }
                    if(a.Market_Vertical__c == Forensic)
                    {
                        SummmForensic = SummmForensic + a.Amount;
                    }       
                }                                                                       
            }
            UpsidenewCustomersALLSegments = SummmAll;
            UpsidenewCustomersAcademicsOmics = SummmAcademiaOmics; 
            UpsidenewCustomersPharma = SummmPharmaCRO;      
            UpsidenewCustomersFoodBeverage = SummmFoodBeverage;  
            UpsidenewCustomersEnvironmentalIndustrial = SummmEnvironmentalIndustrial; 
            UpsidenewCustomersRateClinical = SummmClinical; 
            UpsidenewCustomersRateForensics = SummmForensic;                                                                                        
        }                                                    
}