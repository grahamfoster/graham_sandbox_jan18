@isTest
public class communityCase_Test {

@isTest
public static void test_createTheCase()
{
	//create test objects
	Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada');
	insert a;
	Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email = 'test@example.com');
	insert c;
	Community_Thread__c ct = new Community_Thread__c(Name = 'TestThread', Contact__c = c.Id, Community_URL__c = 'http://example.com',
														Description__c = 'TestDescription', Thread_Name__c = 'TestName');
	insert ct;
	Community_Post__c cp = new Community_Post__c(Community_Thread__c = ct.Id, Contact__c = c.Id, Post_Body__c = 'TestBody');
	insert cp;

	//test the class
	String caseId = communityCase.createTheCase(cp.Id);
	//test we have a case
	system.assertNotEquals(null, caseId);
	//check the community post to show the case has been added
	List<Community_Post__c> cpNew = [SELECT SCIEXNow_Case__c FROM Community_Post__c WHERE Id = :cp.Id LIMIT 1];
	system.assertEquals(caseId, cpNew[0].SCIEXNow_Case__c);
	//check the case content
	List<Case> cases = [SELECT ContactId, Subject, Description FROM Case WHERE Id = :caseId LIMIT 1];
	system.assertEquals(c.Id, cases[0].ContactId);
	system.assertEquals('SCIEX Community - ' + ct.Thread_Name__c, cases[0].Subject);
	system.assertEquals(ct.Community_URL__c + '\n\n' + cp.Post_Body__c, cases[0].Description);
	//check the case comments
	List<CaseComment> comments = [SELECT IsPublished, CommentBody FROM CaseComment WHERE ParentId = :caseId];
	//ensure there are 3
	system.assertEquals(3, comments.size());
	
}

}