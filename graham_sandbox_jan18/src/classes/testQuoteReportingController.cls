/*********************************************************************
Name  : testQuoteReportingController
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuoteReportingController     

*********************************************************************/

public class testQuoteReportingController{

  public static testMethod void checkQuoteReportingController() {

    QuoteReportingController myController = new QuoteReportingController();  
    myController.setIsIgnored(true);
    string str = '';
    myController.setIGORCodeDescValue('');
    str = myController.getIGORCodeDescValue();
    myController.setPlanCodeDescValue('');
    str = myController.getPlanCodeDescValue();
    myController.setIGORItemClassValue('');
    str = myController.getIGORItemClassValue();
    myController.setProductLineDescValue('');
    str = myController.getProductLineDescValue();
    myController.setIGORItemClassDescValue('');
    str = myController.getIGORItemClassDescValue();
    myController.setIGORCodeValue('');
    str = myController.getIGORCodeValue();
    myController.setProductLineValue('');
    str = myController.getProductLineValue();
    str = myController.getCdiUsername();
    myController.setCdiUsername('');
    str = myController.getCdiPassword();
    myController.setCdiPassword('');
    str = myController.getFilter_cdiQuote();
    myController.setFilter_cdiQuote('');
    List<SelectOption> lstSelectOption = myController.getFilter_QuoteStatus();
    List<Quote__c> lst = myController.getResult();
    
    // ************* Following commented methods thrown System.Exception: Too many query rows: 501 ****************
    //lstSelectOption = myController.getproductLine();
    //lstSelectOption = myController.getproductLineDesc();
    //lstSelectOption = myController.getplanCode();
    //lstSelectOption = myController.getplanCodeDesc();
    //lstSelectOption = myController.getiGORItemClass();
    //lstSelectOption = myController.getiGORItemClassDesc();
    //lstSelectOption = myController.getiGORCode();
    //lstSelectOption = myController.getiGORCodeDesc();
    //*************************************************************************************************************
    myController.setFilter_QuoteStatusQLP('');
    str = myController.getFilter_QuoteStatusQLP();
    myController.setPlanCodeValue('');
    str = myController.getPlanCodeValue();
    myController.setFilter_ProductCategory('');
    str = myController.getFilter_ProductCategory();
    myController.setFilter_ProductLine('');
    str = myController.getFilter_ProductLine();
    myController.setFilter_PlanCode ('');
    str = myController.getFilter_PlanCode();
    myController.setFilter_IGORItemClass('');
    str = myController.getFilter_IGORItemClass();
    myController.setFilter_IGORCode('');
    str = myController.getFilter_IGORCode();
    myController.setFilter_Template('');
    str = myController.getFilter_Template();
    myController.setFilter_SAPQuote('test');
    str = myController.getFilter_SAPQuote();
    myController.setFilter_QuoteStatusQLP('test');
    str = myController.getFilter_QuoteStatusQLP();
    myController.setPlanCodeValue('test');
    str = myController.getPlanCodeValue();
    myController.setFilter_ProductCategory('test');
    str = myController.getFilter_ProductCategory();
    myController.setFilter_Product('test');
    str = myController.getFilter_Product();
    myController.setFilter_ProductLine('test');
    str = myController.getFilter_ProductLine();
    myController.setFilter_PlanCode('test');
    str = myController.getFilter_PlanCode();
    myController.setFilter_IGORItemClass('test');
    str = myController.getFilter_IGORItemClass();
    myController.setFilter_IGORCode('test');
    str = myController.getFilter_IGORCode();
    myController.setFilter_Template('test');
    str = myController.getFilter_Template();
    str = myController.getWmResponse();
    myController.deleteData();
    //str = myController.invokeWM();
    
    
    PageReference pgRef = null;
    pgRef = myController.loginToCDI();
    List<Product_Hierarchy__c> lst12 = myController.getSearchResults();
    pgRef = myController.searchProductHierarchy();
    pgRef = myController.quoteSearchCDI();
    pgRef = myController.QuoteSearchSAP();
    pgRef = myController.QuoteNewHeader();
    pgRef = myController.QuoteNewPartner();
    pgRef = myController.QuoteNewOptions();
    pgRef = myController.QuoteProductSelect();
    pgRef = myController.QuoteProductDiscount();
    pgRef = myController.QuoteProductSort();
    pgRef = myController.QuoteProductSearch();
    pgRef = myController.QuoteTemplateSearch();
    pgRef = myController.QuoteMaterials();
    pgRef = myController.redirectToLandingPage();
    pgRef = myController.redirectToNewQuote1();
    pgRef = myController.redirectToNewQuote2();
    pgRef = myController.redirectToNewQuote3();
    pgRef = myController.redirectToProductSelect();
    pgRef = myController.redirectToProductDiscount();
    pgRef = myController.redirectToProductSort();
    pgRef = myController.redirectToProductSearch();
    pgRef = myController.redirectToTemplateSearch();
    pgRef = myController.redirectToMaterials();
    pgRef = myController.createReport();
    Quote__c  q = myController.getQuoteProxy();
    pgRef = myController.loadData();
    
  }  

  public static testMethod void checkQuoteReportingController1() {

    QuoteReportingController myController = new QuoteReportingController(); 
    myController.setIsIgnored(true); 
    string str = '';

    myController.setFilter_QuoteStatusQLP('');
    str = myController.getFilter_QuoteStatusQLP();
    myController.setPlanCodeValue('');
    str = myController.getPlanCodeValue();
    myController.setFilter_ProductCategory('');
    str = myController.getFilter_ProductCategory();
    myController.setFilter_ProductLine('');
    str = myController.getFilter_ProductLine();
    myController.setFilter_PlanCode ('');
    str = myController.getFilter_PlanCode();
    myController.setFilter_IGORItemClass('');
    str = myController.getFilter_IGORItemClass();
    myController.setFilter_IGORCode('');
    str = myController.getFilter_IGORCode();
    myController.setFilter_Template('');
    str = myController.getFilter_Template();
    myController.setFilter_SAPQuote('test');
    str = myController.getFilter_SAPQuote();
    myController.setFilter_QuoteStatusQLP('test');
    str = myController.getFilter_QuoteStatusQLP();
    myController.setPlanCodeValue('test');
    str = myController.getPlanCodeValue();
    myController.setFilter_ProductCategory('test');
    str = myController.getFilter_ProductCategory();
    myController.setFilter_Product('test');
    str = myController.getFilter_Product();
    myController.setFilter_ProductLine('test');
    str = myController.getFilter_ProductLine();
    myController.setFilter_PlanCode('test');
    str = myController.getFilter_PlanCode();
    myController.setFilter_IGORItemClass('test');
    str = myController.getFilter_IGORItemClass();
    myController.setFilter_IGORCode('test');
    str = myController.getFilter_IGORCode();
    myController.setFilter_Template('test');
    str = myController.getFilter_Template();
    str = myController.getWmResponse();
    myController.deleteData();
    //str = myController.invokeWM();
    
    
    PageReference pgRef = null;
    myController.setCdiUsername('squser');
    myController.setCdiPassword('sq@abi');
    pgRef = myController.loginToCDI();
    List<Product_Hierarchy__c> lst12 = myController.getSearchResults();
    pgRef = myController.searchProductHierarchy();
    pgRef = myController.quoteSearchCDI();
    pgRef = myController.QuoteSearchSAP();
    pgRef = myController.QuoteNewHeader();
    pgRef = myController.QuoteNewPartner();
    pgRef = myController.QuoteNewOptions();
    pgRef = myController.QuoteProductSelect();
    pgRef = myController.QuoteProductDiscount();
    pgRef = myController.QuoteProductSort();
    pgRef = myController.QuoteProductSearch();
    pgRef = myController.QuoteTemplateSearch();
    pgRef = myController.QuoteMaterials();
    pgRef = myController.redirectToLandingPage();
    pgRef = myController.redirectToNewQuote1();
    pgRef = myController.redirectToNewQuote2();
    pgRef = myController.redirectToNewQuote3();
    pgRef = myController.redirectToProductSelect();
    pgRef = myController.redirectToProductDiscount();
    pgRef = myController.redirectToProductSort();
    pgRef = myController.redirectToProductSearch();
    pgRef = myController.redirectToTemplateSearch();
    pgRef = myController.redirectToMaterials();
    pgRef = myController.createReport();
    Quote__c  q = myController.getQuoteProxy();
    //pgRef = myController.loadData();
    
  }    
  
  public static testMethod void checkQuoteReportingController2() {

    QuoteReportingController myController = new QuoteReportingController();  
    myController.setIsIgnored(true);
    string str = '';

    PageReference pgRef = null;
    pgRef = myController.loginToCDI();
    List<Product_Hierarchy__c> lst12 = myController.getSearchResults();
    pgRef = myController.searchProductHierarchy();
    pgRef = myController.quoteSearchCDI();
    pgRef = myController.QuoteSearchSAP();
    pgRef = myController.QuoteNewHeader();
    pgRef = myController.QuoteNewPartner();
    pgRef = myController.QuoteNewOptions();
    pgRef = myController.QuoteProductSelect();
    pgRef = myController.QuoteProductDiscount();
    pgRef = myController.QuoteProductSort();
    pgRef = myController.QuoteProductSearch();
    pgRef = myController.QuoteTemplateSearch();
    pgRef = myController.QuoteMaterials();
    pgRef = myController.redirectToLandingPage();
    pgRef = myController.redirectToNewQuote1();
    pgRef = myController.redirectToNewQuote2();
    pgRef = myController.redirectToNewQuote3();
    pgRef = myController.redirectToProductSelect();
    pgRef = myController.redirectToProductDiscount();
    pgRef = myController.redirectToProductSort();
    pgRef = myController.redirectToProductSearch();
    pgRef = myController.redirectToTemplateSearch();
    pgRef = myController.redirectToMaterials();
    pgRef = myController.createReport();
    Quote__c  q = myController.getQuoteProxy();
    //pgRef = myController.loadData();
    
    //Additional Test Methohds
    pgRef = myController.checkProgress();
    myController.setLoadProgress(null);
    myController.getLoadProgress();
    myController.setLoadProgress('Loading...');
    myController.getLoadProgress();
    pgRef = myController.cancelAndRetry();
    pgRef = myController.verifyAuthentication();
    
    myController.setIGORCodeRangeSelected('');
    myController.getIGORCodeRangeSelected();
    myController.getFilter_District();
    myController.setfilter_District('');
    myController.getFilter_salesRep();
    myController.getCdiUsername();
    myController.setErrorMessage('');
    myController.getErrorMessage();
    myController.setfilter_salesRep('');
    //myController.deleteAll();
    
    
    new QuoteReportServices.loadCDIQuotesReportInSFDC();
    new QuoteReportServices.ABI_SalesQuotation_process_loadCDIQuotesReportInSFDCWsd_Port();
    new QuoteReportServices.ArrayOfstring();
    new QuoteReportServices.cdiQuotesReport();
    new QuoteReportServices.loadCDIQuotesReportInSFDCResponse();
  }      
  

}