/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Apr 29/2014
 **
 ** Scheduled job to kick off the OpportunityContactRoleBatchable job
**/
global class OpportunityContactRoleSchedulable implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new OpportunityContactRoleBatchable(), 2000);
	}

	global static void scheduleJob(){
		System.schedule('Opportunity Marketing to Contact', '0 0 * * * ?', new OpportunityContactRoleSchedulable());
	}
}