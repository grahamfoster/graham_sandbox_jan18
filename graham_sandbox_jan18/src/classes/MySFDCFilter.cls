/**
** @author Reid Beckett, Cloudware Connections
** @created Dec 1/2014
**
** Filters for specialized view of My SFDC using filters in the top right
** 
**/
public class MySFDCFilter {
	public User runAs {get;set;}
	public String marketVertical {get;set;}
	public String productType {get;set;}
	public String region {get;set;}
	public String country {get;set;}
	
	public MySFDCFilter() {
		
	}
}