/*
 *	EmailMessageTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2016-08-11 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class EmailMessageTriggerHandler extends TriggerHandler {

	public EmailMessageTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void beforeInsert() {
		cloneClosedSR();
		createPortalBody();
	}
	override protected void beforeUpdate() {
		cloneClosedSR();
		createPortalBody();
	}
    override protected void afterInsert() { 
	createEmail();    
	}
	override protected void afterUpdate() {
	createEmail();    
	}
	override protected void afterUndelete() {
    
	}
	override protected void afterDelete() {
    
	}    
    
	// **************************************************************************
	// 		private methods
	// **************************************************************************
	/* 
	 * Copied from original EmailMessageTriggerHandler written by Reid Beckette
	*/
	
	public void createEmail()
    {
        // Line Added by Graham Foster to generate portal email objects
        new PortalEmailCreator().CreateEmails((List<EmailMessage>)Trigger.new);
        // end of Graham Line Added
       
    }    

    public void cloneClosedSR(){
        Set<Id> caseIds = new Set<Id>();
        // Iterate through all Email Messages in Trigger
        for(EmailMessage em : (List<EmailMessage>)Trigger.new)
        {
            if(em.ParentId != null) caseIds.add(em.ParentId);
        }
        // Prepare list of new (cloned) Cases
        List<Case> cloneCases = new List<Case>();
        // Prepare Map of ID (Original Case), new Case
		Map<Id,Case> caseMap = new Map<Id,Case>();
        RecordType CaseRecType = SfdcUtil.getRecordType('Case', 'CICCase'); 
        RecordType SRRecType = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
		//Fetching the assignment rules on case
		AssignmentRule AR = new AssignmentRule();
		AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];

		//Creating the DMLOptions for "Assign using active assignment rules" checkbox
		Database.DMLOptions dmlOpts = new Database.DMLOptions();
		dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
		// Query all Closed Cases apearing in caseIds Map
        for(Case closedCase : [select Id,BusinessHoursId,ContactId,AccountId,Type,Case_Type__c,Subject,Description,RecordTypeID from Case where IsClosed=true and Id in :caseIds]){        
            If ( closedCase.RecordTypeId == SRRecType.id ){
            	// Create new 'cloned case'
            	Case newCase = new Case(	RecordType = CaseRecType  ,
                    						BusinessHoursId = closedCase.BusinessHoursId  ,
											ContactId =  closedCase.ContactId ,
											AccountId = closedCase.AccountId  ,
											Type = closedCase.Type  ,
											Case_Type__c = closedCase.Case_Type__c  ,
											Subject = 'Cloned Case due to Customer sending in email to reopen – See Parent Case for full details'   ,
											Description = closedCase.Description  ,
											Original_Case__c = closedCase.id ,
	                						ParentId = closedCase.id ,
											Status = 'New'  ,
											Priority = 'Medium'  ,
											Origin  =  'Email' );
                newCase.RecordTypeId = CaseRecType.id;
				newCase.setOptions(dmlOpts);
        	    cloneCases.add(newCase);
            	// Populate Map with (orignal Case ID), New Case
            	caseMap.put(closedCase.id, newCase);
            }
        }
        if(cloneCases.size() > 0) {
			  insert  cloneCases;         
			
        }
        // Iterate through Trigger again (to reparent Messages with new clones cases)
        for(EmailMessage em : (List<EmailMessage>)Trigger.new)
        {
            // if Case has been cloned, re-parent incoming Email Message to the newly created case
            if(caseMap.containsKey(em.ParentId)){
                em.ParentId = caseMap.get(em.ParentId).id;
                If(em.Subject.contains('[ ref:')){
                   em.Subject = em.Subject.Left(em.Subject.indexOf('[ ref:'));         
                }
            }
        }
    }

	private void createPortalBody()
	{
		List<Portal_Email_Settings__c> emailSettings = Portal_Email_Settings__c.getAll().values();
		Set<String> SubjectsToExclude = new Set<String>();
		Set<String> fromSplitters = new Set<String>();
		for(Portal_Email_Settings__c pe : emailSettings)
		{
			if(pe.Setting_Type__c == 'From Splitter')
			{
				fromSplitters.add(pe.Value__c);
			} else if(pe.Setting_Type__c == 'Exclude Subject Line')
			{
				SubjectsToExclude.add(pe.Value__c);
			}
		}
		String casePrefix = Schema.SObjectType.Case.getKeyPrefix();
		Set<Id> caseIds = new Set<Id>();
		for(EmailMessage em : (List<EmailMessage>)Trigger.new)
		{
			String emId = (String)em.ParentId;
			if(emId.startsWith(casePrefix))
			{
				caseIds.add(em.ParentId);
			}
		}

		Map<Id, Case> parentCases = new Map<Id, Case>([SELECT Id, Contact.Email FROM Case WHERE Id IN :caseIds]); 

		for(EmailMessage em : (List<EmailMessage>)Trigger.new)
		{
			String emId = (String)em.ParentId;
			if(emId.startsWith(casePrefix))
			{
				//the message is related to a case so we should do the 
				//action to split the email up
				Integer splitIndex = -1;
				if(!String.isBlank(em.TextBody)){
					splitIndex = FindFirstFromIndex(em.TextBody, fromSplitters);
						}
				system.debug('The split index is calculated as:' + splitIndex);
				if(splitIndex == -1){
					em.Portal_Email_Body__c = em.TextBody;
				}else{
					em.Portal_Email_Body__c = PortalEmailBody(em.TextBody,splitIndex);
				}
				Boolean showCase = false;
				Case pCase = parentCases.get(em.ParentId);
				try{
					if(parentCases.containsKey(em.ParentId) && !String.isEmpty(pCase.Contact.Email) && (
					(!String.isEmpty(em.ToAddress) && em.ToAddress.contains(pCase.Contact.Email)) 
					|| (!String.isEmpty(em.FromAddress) && em.FromAddress.contains(pCase.Contact.Email)) 
					|| (!String.isEmpty(em.CcAddress) && em.CcAddress.contains(pCase.Contact.Email))))
					{
						showCase = true;
						for(String s : SubjectsToExclude)
						{
							if(em.Subject.startsWith(s))
							{
								showCase = false;
							}
						}
					}
				} catch (Exception e)
				{
					System.debug('**GF** Exception Email Mesage:' + em);
					System.debug('**GF** Exception Case:' + pCase);
					System.debug('**GF** Exception Message:' + e.getMessage());
				} 
				em.Show_On_Portal__c = showCase;
			}


		}
	}

	private integer FindFirstFromIndex(string messageBody, Set<String> seperators){
        //use this function to find the first instance of
        //the From: entry so that we can split off the most
        //recent message
        
        //an array of from strings so that we can 
        //seperate from in different languages
        
        String UpperMBody = messageBody.toUpperCase();     
        Integer Marker = -1;
        for(String s : seperators){
            if(UpperMBody.contains(s)){
                Integer sIndex = UpperMBody.indexOf(s);
                system.debug(s + ' found at index:' + sIndex);
                if(Marker == -1){
                    Marker = sIndex;
                }else{
                    if(sIndex < Marker){
                        Marker = sIndex;
                    }
                }
            }
        }
        return Marker;
    }
    
    private String PortalEmailBody(String messageBody, Integer splitPoint){
        //split the email string according to the position of
        //the seperator
        String body = messageBody.substring(0, splitPoint);
        //now just remove the last line as it always a blank line
        //or -----------Orignal Message------------ in local language
        String rtrnStr = '';
        List<String> bodyLines = body.split('\n');
		if(bodyLines.size() > 2)
		{
			for(integer i = 0;i < bodyLines.size() - 1; i++){
				rtrnStr = rtrnStr + bodyLines[i] + '\n';
			}
		} else {rtrnStr = body;}
		System.debug('**GF** Original Email Body:' + messageBody);
		System.debug('**GF** Portal Email Body:' + rtrnStr);
        return rtrnStr;
    }

}