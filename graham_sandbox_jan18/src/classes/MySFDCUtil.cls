/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 15/2014
**
** General purpose methods for My SFDC dashboard
** 
**/
public class MySFDCUtil {
	static Period currentQuarter = null;
	static Period nextQuarter = null;
	static Period thirdQuarter = null;
	static Period fourthQuarter = null;
	static Map<String,Map<String,Decimal>> conversionRatesMap = null;
	//static Map<String,Decimal> conversionRatesMap = null;
	//static Map<String,Decimal> conversionRatesMapService = null;
	static Map<Id,List<User>> roleHierarchyCache = null;
	static final String SERVICE_SECTOR = 'Service';
	static final String NONSERVICE_SECTOR = 'Non-Service';
	/**
	 ** Load all users in roles below this role (don't include this role)
	**/
	public static List<User> findUsersInRoleHierarchy(Id roleId) {
		if(roleId == null) return new List<User>{};
		if(roleHierarchyCache == null) roleHierarchyCache = new Map<Id,List<User>>();
		if(!roleHierarchyCache.containsKey(roleId)) {
			List<UserRole> roles = findRolesInHierarchy(new List<UserRole>(), new Set<Id>{ roleId });
			Set<Id> roleIds = new Set<Id>();
			for(UserRole role : roles) {
				roleIds.add(role.Id);
			}
			roleHierarchyCache.put(roleId, [select Id, Name, License_Region__c, License_Department__c from User where UserRoleId in :roleIds and IsActive = true order by FirstName asc]);
		}
		return roleHierarchyCache.get(roleId);
	}
	
	/**
	 ** Load all users in roles below this role (don't include this role)
	**/
	public static List<UserRole> findRolesInHierarchy(List<UserRole> currentUserRoles, Set<Id> roleIds) {
		UserRole[] childRoles = [select Id, Name from UserRole where ParentRoleId in :roleIds];
		Set<Id> childRoleIds = new Set<Id>();
		for(UserRole childRole : childRoles) {
			childRoleIds.add(childRole.Id);
			currentUserRoles.add(childRole);
		}
		if(childRoleIds.size() > 0) currentUserRoles = findRolesInHierarchy(currentUserRoles, childRoleIds);
		return currentUserRoles;
	}

	public static User findUser(Id userId){
		return [select Id, Name, UserRoleId, UserRole.Name, Profile.Name, License_Region__c, Management_Level__c, License_Department__c, DefaultCurrencyIsoCode from User where Id = :userId];	
	}

	public static Period getCurrentQuarter(){
		if(currentQuarter == null) {
			String year = String.valueOf(Date.today().year());
			Date today = Date.today();
			currentQuarter = [select Id, StartDate, EndDate, Number, Type, FiscalYearSettings.Name from Period where Type = 'Quarter' and FiscalYearSettings.Name = :year and StartDate <= :today and EndDate >= :today limit 1];
		}
		return currentQuarter;
	}

	public static Period getNextQuarter(){
		if(nextQuarter == null) 
		{
			initQuarters();
		}
		return nextQuarter;
	}

	public static Period getThirdQuarter()
	{
		if(thirdQuarter == null)
		{
			initQuarters();
		}
		return thirdQuarter;
	}

	public static Period getFourthQuarter()
	{
		if(fourthQuarter == null)
		{
			initQuarters();
		}
		return fourthQuarter;
	}

	private static void initQuarters()
	{
		Period currentPeriod = getCurrentQuarter();
		Integer currentYear = Date.today().year();
		Integer nextYear = currentYear + 1;
		String currentYearStr = String.valueOf(currentYear);
		String nextYearStr = String.valueOf(nextYear); 
		Period[] periods = [select Id, StartDate, EndDate, Number, Type, FiscalYearSettings.Name from Period 
			where Type = 'Quarter' and (FiscalYearSettings.Name = :currentYearStr or FiscalYearSettings.Name = :nextYearStr) 
			order by Number asc];

		Integer currentQtrValue = 4 * Integer.valueOf(currentPeriod.FiscalYearSettings.Name) + currentPeriod.Number;
		for(Period p : periods)
		{
			Integer pValue = 4 * Integer.valueOf(p.FiscalYearSettings.Name) + p.Number;
			if(pValue == currentQtrValue + 1)
			{
				nextQuarter = p;
			}
			if(pValue == currentQtrValue + 2)
			{
				thirdQuarter = p;
			}
			if(pValue == currentQtrValue + 3)
			{
				fourthQuarter = p;
			}
		}
	}

	public static Map<String,Decimal> getConversionRates(User usr){
		String region = usr.License_Region__c;
		String sector = getUserSector(usr);

		if(conversionRatesMap == null) {
			conversionRatesMap = new Map<String,Map<String,Decimal>>();
		}

		if(!conversionRatesMap.containsKey(sector)) {
			Map<String,Decimal> innerConversionRatesMap = new Map<String,Decimal>();
			for(Conversion_Rate__c cr : [select Region__c, Conversion_Rate__c, Opportunity_Type__c from Conversion_Rate__c 
				where Region__c = :region and Sector__c = :sector order by Opportunity_Type__c asc ]) {
				innerConversionRatesMap.put(cr.Opportunity_Type__c, cr.Conversion_Rate__c/100.0);
			}
			conversionRatesMap.put(sector, innerConversionRatesMap);
		}
		return conversionRatesMap.get(sector);
	}

	public static List<Conversion_Rate__c> getConversionRatesList(User usr){
		String region = usr.License_Region__c;
		String userSector = getUserSector(usr);
		return [select Region__c, Conversion_Rate__c, Opportunity_Type__c from Conversion_Rate__c 
				where Region__c = :region and Sector__c = :userSector order by Opportunity_Type__c asc ];
	}

	public static MySFDCQuota getQuotas(Id userId) {
		MySFDCQuota q = new MySFDCQuota();
		q.ytdQuota = 0;
		q.currentQuarterQuota = 0;
		q.nextQuarterQuota = 0;

		Period currentQuarterP = getCurrentQuarter();
		Period nextQuarterP = getNextQuarter();
		Period thirdQuarterP = getThirdQuarter();
		Period fourthQuarterP = getFourthQuarter();

		String year = String.valueOf(Date.today().year());

		for(Quota__c qta : [select Fiscal_Year__c, Fiscal_Quarter__c, Quota__c, Type__c from Quota__c where 
			OwnerId = :userId
			order by Fiscal_Quarter__c asc]) 
		{
			if(qta.Fiscal_Year__c == currentQuarterP.FiscalYearSettings.Name && qta.Fiscal_Quarter__c == String.valueOf(currentQuarterP.Number))
			{
				q.addCurrentQuarterQuota(qta);
			}

			if(qta.Fiscal_Year__c == nextQuarterP.FiscalYearSettings.Name && qta.Fiscal_Quarter__c == String.valueOf(nextQuarterP.Number))
			{
				q.addNextQuarterQuota(qta);
			}

			if(qta.Fiscal_Year__c == thirdQuarterP.FiscalYearSettings.Name && qta.Fiscal_Quarter__c == String.valueOf(thirdQuarterP.Number))
			{
				q.addThirdQuarterQuota(qta);
			}

			if(qta.Fiscal_Year__c == fourthQuarterP.FiscalYearSettings.Name && qta.Fiscal_Quarter__c == String.valueOf(fourthQuarterP.Number))
			{
				q.addFourthQuarterQuota(qta);
			}

			if(qta.Fiscal_Quarter__c != null && qta.Fiscal_Year__c == year && Integer.valueOf(qta.Fiscal_Quarter__c) < currentQuarterP.Number) 
			{
				q.addYTDQuarterQuota(qta);
			}

			if(qta.Fiscal_Year__c == year) 
			{
				q.addFullYearQuota(qta);
			}

		}

		/*
		List<AggregateResult> quotas = [select Fiscal_Quarter__c qtr, SUM(Quota__c) quota from Quota__c where 
			Fiscal_Year__c = :year and OwnerId = :userId
			group by Fiscal_Quarter__c
			order by Fiscal_Quarter__c asc];
		for(AggregateResult aggr : quotas) {
			String qtr = (String)aggr.get('qtr');
			if(qtr == String.valueOf(currentQuarterP.Number)) {
				if(aggr.get('quota') != null) q.currentQuarterQuota = (Decimal)aggr.get('quota');
			}else if(qtr == String.valueOf(nextQuarterP.Number) && year == nextQuarterP.FiscalYearSettings.Name) {
				if(aggr.get('quota') != null) q.nextQuarterQuota = (Decimal)aggr.get('quota');
			}else if(qtr == String.valueOf(thirdQuarterP.Number) && year == thirdQuarterP.FiscalYearSettings.Name) {
				if(aggr.get('quota') != null) q.thirdQuarterQuota = (Decimal)aggr.get('quota');
			}else if(qtr == String.valueOf(fourthQuarterP.Number) && year == fourthQuarterP.FiscalYearSettings.Name) {
				if(aggr.get('quota') != null) q.fourthQuarterQuota = (Decimal)aggr.get('quota');
			}
			if(qtr != null && Integer.valueOf(qtr) < currentQuarterP.Number) {
				if(aggr.get('quota') != null) q.ytdQuota += (Decimal)aggr.get('quota');
			}
		}
		if(nextQuarterP.Number == 1) {
			//get quarter from next year
			String nextYear = String.valueOf(Date.today().year() + 1);
			List<AggregateResult> quotas2 = [select Fiscal_Quarter__c qtr, SUM(Quota__c) quota from Quota__c where 
				Fiscal_Year__c = :nextYear and Fiscal_Quarter__c ='1' and OwnerId = :userId
				group by Fiscal_Quarter__c
				order by Fiscal_Quarter__c asc];
			for(AggregateResult aggr : quotas2) {
				String qtr = (String)aggr.get('qtr');
				if(aggr.get('quota') != null) q.nextQuarterQuota = (Decimal)aggr.get('quota');
			}
		}
		*/

		//if(q.ytdQuota == null) q.ytdQuota = 0;
		//if(q.nextQuarterQuota == null) q.ytdQuota = 0;
		q.finalizeCalculations();
		return q;
	}

	/**
	 ** When catching a DML exception, add the messages to the pageMessages
	**/
    public static void addDMLExceptionMessagesToPage(DMLException e) {
        for(integer i = 0; i < e.getNumDml(); i++){
    		String errorMessage = 'Row ' + (e.getDmlIndex(i) + 1);
    		if(e.getDmlFields(i) != null) errorMessage += '[' + e.getDmlFields(i)[0].getDescribe().getLabel()  + ']';
    		errorMessage += + ': ' + e.getDmlMessage(i);
        	if(e.getDmlType(i) == StatusCode.DUPLICATE_VALUE) {
            	//ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, '<a href="/' + e.getDmlId(i) + '">'+e.getDmlId(i)+'</a>'));
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, errorMessage));
        	}else{
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, errorMessage));
        	}
        }
    }

    /** new methods for service enhancements **/
    public static String getUserSector(User usr) {
    	//return (usr.Profile.Name != null && usr.Profile.Name.toLowerCase().contains('service')) ? SERVICE_SECTOR : NONSERVICE_SECTOR;
    	return isServiceUser(usr) ? SERVICE_SECTOR : NONSERVICE_SECTOR;
    }
    
    public static Boolean isServiceUser(User usr) {
    	//return getUserSector(usr) == SERVICE_SECTOR;
    	return usr.License_Department__c == 'Service';
    }

    /** currency conversion **/
	static Map<String, CurrencyType> currencyTypeCache = new Map<String,CurrencyType>();

	public static Decimal convertCurrency(Decimal amount, String srcCurrencyIso, String destCurrencyIso) 
	{
		if(amount == null) return null;
		if(srcCurrencyIso == destCurrencyIso) {
			return amount;
		}else{
			CurrencyType src_ctype = findCurrencyType(srcCurrencyIso);
			if(src_ctype == null) return amount;
			if(src_ctype.IsCorporate) {
				Decimal exchangeRate = getExchangeRate(destCurrencyIso);
				return amount * exchangeRate;
			}else {
				Decimal exchangeRate = getExchangeRate(destCurrencyIso);
				CurrencyType dest_ctype = findCurrencyType(destCurrencyIso);
				Decimal srcExchangeRate = getExchangeRate(srcCurrencyIso);
				if(dest_ctype.IsCorporate) {
					return amount * (1/srcExchangeRate);
				}else {
					return amount * (exchangeRate/srcExchangeRate);
				}
			}
		}
	}

	private static Decimal getExchangeRate(String isoCode)
	{
		CurrencyType dest_ctype = findCurrencyType(isoCode);
		return dest_ctype == null ? 1 : dest_ctype.ConversionRate;
	}

	private static CurrencyType findCurrencyType(String isoCode) {
		if(currencyTypeCache.containsKey(isoCode)) return currencyTypeCache.get(isoCode);
		else{
			List<CurrencyType> ctypes = [select IsoCode, ConversionRate, IsCorporate from CurrencyType where IsActive = true];
			for(CurrencyType ctype : ctypes) {
				currencyTypeCache.put(ctype.IsoCode, ctype);
			}
			return currencyTypeCache.get(isoCode);
		}
	}
}