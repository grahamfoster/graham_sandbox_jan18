@isTest(SeeAllData=true)
public class ContactSetCountryMap_Test {
	
    
    static testmethod void Contact_Proper_Country() {
      Account acc = [Select ID from Account where Name = 'Pending Account Assignment' limit 1];
        // Create a test Contact
      Contact con = new Contact(LastName='ContactTest',
                          MailingCountry='ca',
                          MailingState='on',
                          AccountID = acc.Id);
      insert con;
	  
   	  Country_Mapping__c Country = [Select ID, Name from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];
        
      con = [select Id, MailingCountry, MailingState, State_Mapping__c, Country_Mapping__c, Name from Contact where Id =: con.Id];
      System.assertEquals('Canada', con.MailingCountry);
      System.assertEquals(Country.ID, con.Country_Mapping__c);
      System.assertEquals('Ontario', con.MailingState);
      System.assertEquals(State.ID, con.State_Mapping__c);        
    }     
    
      static testmethod void Contact_Improper_Country() {
      Account acc = [Select ID from Account where Name = 'Pending Account Assignment' limit 1];
          // Create a test Contact
      Contact con = new Contact(LastName='ContactTest',
                          MailingCountry='something',
                          MailingState='Ontario',
                          AccountID = acc.Id);
      insert con;
	  
      con = [select Id, MailingCountry, Country_Mapping__c, State_Mapping__c, Name from Contact where Id =: con.Id];
      //System.assertEquals(NULL, con.Country);
      //Contact Country Values should not be nulled out - can be utilized for scrubbing later on/adjustment of permutations
      System.assertEquals(NULL, con.Country_Mapping__c);
      System.assertEquals(NULL, con.State_Mapping__c);          
    }    
    
      static testmethod void Contact_NULL_Country() {
      Account acc = [Select ID from Account where Name = 'Pending Account Assignment' limit 1];
          // Create a test Contact
      Contact con = new Contact(LastName='ContactTest',
                          MailingCountry=NULL,
                          MailingState='Ontario',                                
                          AccountID = acc.Id);
      insert con;
	  
      con = [select Id, MailingCountry, Country_Mapping__c, State_Mapping__c,Name from Contact where Id =: con.Id];
      System.assertEquals(NULL, con.MailingCountry);
      System.assertEquals(NULL, con.Country_Mapping__c);
      System.assertEquals(NULL, con.State_Mapping__c);            
    }    
}