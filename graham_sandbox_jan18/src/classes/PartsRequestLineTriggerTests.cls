/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 25/2015
**
** For new Parts Request Lines (managed custom object), if the status of the Parts Request parent = “Submitted” then update the line status picklist to “Submitted”
** 
**/
@isTest
public class PartsRequestLineTriggerTests 
{
	public static testMethod void test1()
    {
        SVMXC__Site__c loc1 = new SVMXC__Site__c (SVMXC__Stocking_Location__c = true);
        insert loc1;
    	
        SVMXC__Parts_Request__c pr = new SVMXC__Parts_Request__c(SVMXC__Status__c = 'Submitted', SVC_NeedbyDate__c = Date.today().addDays(2), SVMXC__Requested_From__c = loc1.Id);
        insert pr;
        
        SVMXC__Parts_Request_Line__c pline = new SVMXC__Parts_Request_Line__c(SVMXC__Parts_Request__c = pr.Id);
        insert pline;
        
        pline = [select SVMXC__Line_Status__c from SVMXC__Parts_Request_Line__c where Id = :pline.Id];
        system.assertEquals('Submitted', pline.SVMXC__Line_Status__c);
    }
}