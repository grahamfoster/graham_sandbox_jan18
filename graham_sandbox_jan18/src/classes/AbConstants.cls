public class AbConstants {
  public static final String DRAFT_STATUS = 'DRFT';
  public static final String ENDPOINT = 'https://wm1.appliedbiosystems.com:7662';
  
  public static final String RETURN_URL_FROM_LOGIN = 'RETURN_URL_FROM_LOGIN';
 
  //PARTNER
  public static final String SOLD_TO_PARTNER_TYPE = 'SP';
  public static final String SHIP_TO_PARTNER_TYPE = 'SH';
  public static final String SALES_REP_PARTNER_TYPE = 'Z1';
  public static final String DEST_COUNTRY_PARTNER_TYPE = 'Z6';
  public static final String PY_PARTNER_TYPE = 'PY';
  public static final String BILL_TO_PARTNER_TYPE = 'BP';
  public static final String SAP_CONTACT_PARTNER_TYPE = 'CP';
  public static final String SALES_OFFICE_PARTNER_TYPE = 'XO';
  public static final String SALES_GROUP_PARTNER_TYPE = 'XG';
  public static final String SFDC_CONTACT_PARTNER_TYPE = 'XC';
  public static final String SFDC_ACCT_PARTNER_TYPE = 'XA';

  //GET QUOTE DETAILS PARAMS
  public static final String SAP_QUOTE = 'viewsap';
  public static final String CDI_QUOTE = 'view';
  public static final String REVISE_QUOTE = 'revise';
  public static final String CANCEL_QUOTE = 'cancel';
  public static final String GETQUOTE_HEADER = 'HEADERINFO';
  public static final String GETQUOTE_LINEITEM_INFO = 'LINEITEMINFO';
  public static final String GETQUOTE_LINEITEM_TEXT = 'LINEITEMTEXT';
    
  //Revision source
  public static final String SAP_REVISION_TYPE = 'SAP';
  public static final String CDI_REVISION_TYPE = 'CDI';
  public static final String SFDC_REVISION_TYPE = 'SFDC';

  //PRICING DETAILS
  public static final String US_REGION = 'US';
  public static final String NA_REGION = 'NA';
  public static final String EU_REGION = 'EU';
  public static final String AP_REGION = 'AP';  //Asia Pacific
  public static final String FINAL_PRICE_UPDATE = 'C';
  public static final String DRAFT_PRICE_UPDATE = 'D';
  public static final String HEADER_LINE_ITEM_NUM = '000000';

  //COUNTRY MATRIX
  public static final String DISCOUNT_SCREEN = 'DSCR'; 
  public static final String PAYMENT_TERMS = 'PAYT'; 
  public static final String INCO = 'INCO'; 
  public static final String OUTPUT_MEDIUM = 'OMED'; 
  public static final String OUTPUT_TYPE = 'OTYP'; 
  public static final String CURRENCY_CODE = 'CURR'; 
  public static final String LANGUAGE_CODE = 'LANG'; 
  public static final String PRICE_OUTPUT = 'PDIS'; 
  public static final String STATUS_VALUES = 'QSTA';  
  public static final String WARRANTY = 'WARR';  
  public static final String EST_DELIVERY = 'ESTD';  
  public static final String INSTALLATION = 'INST';  
 
  public static final String ALL_PAYMENT_TERMS = 'PAYX'; 
  public static final String ALL_INCO_TERMS = 'INCX';
 
 
  //DISCOUNT SCREEN VALUES
  public static final String DISCOUNT_ZCAAB1 = 'ZCAAB1'; 
  public static final String DISCOUNT_ZUSABD = 'ZUSABD'; 
  public static final String DISCOUNT_ZDF1   = 'ZDF1'; 
  public static final String DISCOUNT_ZSGABD = 'ZSGABD'; 
  public static final String DISCOUNT_ZAUABD = 'ZAUABD'; 
  public static testmethod void testcase(){
      AbConstants abc=new AbConstants();
  }
  
  //PRODUCT SELECT DEFAULT 
  public static final Integer OPTIONS_STARTING_SEQ = 500; 
  
  public static final String TEMPLATE_TYPE_ZPPR = 'ZPPR';
}