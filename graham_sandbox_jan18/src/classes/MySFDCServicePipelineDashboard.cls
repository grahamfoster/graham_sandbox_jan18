/**
** @author Reid Beckett, Cloudware Connections
** @created Mar 5/2015
**
** Service-only dashboard showing a combo chart of the pipeline grouped by type, against the quota
** 
**/
public class MySFDCServicePipelineDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {
	public MySFDCQuota quotas {get;set;}

	public override GoogleGaugeChartData getGoogleGaugeChartData()
	{
		return null;
	}
	
	public String getReportTitle()
	{
		return 'Pipeline Chart';
	}

	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	protected override String criteria() {
		//by default get all opps that are not closed lost
		String crit = 'OwnerId in :userIds and (IsWon = true or In_Forecast_Mgr__c = true or Mgr_Upside__c = true)';
		crit += ' and RecordType.Name = \'Service\'';
		String marketVertical = filter.marketVertical;
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				crit += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				crit += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				crit += ' and Market_Vertical__c = :marketVertical';
			}
		}

		String productType = filter.productType;
		if(!String.isBlank(productType)) {
			crit += ' and Product_Type__c = :productType';
		}

		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry in :country';
			}
		}
		
		return crit;		
	}

	public GoogleComboChartData getGoogleComboChartData()
	{
		GoogleComboChartData gcd = new GoogleComboChartData('pipeline_chart');
		gcd.title = 'Pipeline ' + String.valueOf(Date.today().year());
		gcd.hName = 'Fiscal Quarter';
		gcd.vName = 'Amount ($K)';
		//gcd.hLabels = new List<String>{ 'Q1', 'Q2', 'Q3', 'Q4' };
		gcd.vLabels = new List<String>{ 'Deal Won', 'Forecast', 'Upside', 'Quota' };
		gcd.data = new List<List<Double>> {
			new List<Double>(),
			new List<Double>(),
			new List<Double>(),
			new List<Double>()
		};
		gcd.colors = new List<String>{
			'black','blue','orange','#ff0000'
		};
		gcd.numberFormat = '$#0.000K';

		Period currentQuarter = MySFDCUtil.getCurrentQuarter();
		Period nextQuarter = MySFDCUtil.getNextQuarter();
		Period thirdQuarter = MySFDCUtil.getThirdQuarter();
		Period fourthQuarter = MySFDCUtil.getFourthQuarter();
		Date startDate = currentQuarter.StartDate;
		Date endDate = fourthQuarter.EndDate;
		Set<Id> userIds = new Set<Id> { filter.runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(filter.runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		gcd.hLabels = new List<String>{
			'Q' + String.valueOf(currentQuarter.Number) + '/' + currentQuarter.FiscalYearSettings.Name,
			'Q' + String.valueOf(nextQuarter.Number) + '/' + nextQuarter.FiscalYearSettings.Name,
			'Q' + String.valueOf(thirdQuarter.Number) + '/' + thirdQuarter.FiscalYearSettings.Name,
			'Q' + String.valueOf(fourthQuarter.Number) + '/' + fourthQuarter.FiscalYearSettings.Name
		};
		
		Map<Integer, Double> totalsMap = new Map<Integer, Double>();
		//key: 11 = Q1, upside; 12 = Q1, forecast, 13 = Q1 deal won, etc.
		//key: 21 = Q1, upside; 22 = Q1, forecast, 23 = Q1 deal won, etc.

		String marketVertical = filter.marketVertical;
		String productType = filter.productType;
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);

		String soql = 'select IsWon, In_Forecast_Mgr__c, Mgr_Upside__c, CloseDate, convertCurrency(ExpectedRevenue) from Opportunity '+
			'where CloseDate >= :startDate and CloseDate <= :endDate and ' + criteria();	
		for(Opportunity opp : Database.query(soql))
		{
			Double total = 0;
			Integer mapKey = -1;
			if(opp.CloseDate >= currentQuarter.StartDate && opp.CloseDate <= currentQuarter.EndDate) {
				mapKey = 10;
			}else if(opp.CloseDate >= nextQuarter.StartDate && opp.CloseDate <= nextQuarter.EndDate) {
				mapKey = 20;
			}else if(opp.CloseDate >= thirdQuarter.StartDate && opp.CloseDate <= thirdQuarter.EndDate) {
				mapKey = 30;
			}else if(opp.CloseDate >= fourthQuarter.StartDate && opp.CloseDate <= fourthQuarter.EndDate) {
				mapKey = 40;
			}
			if(opp.IsWon) mapKey += 3;
			else if(opp.In_Forecast_Mgr__c) mapKey += 2;
			else if(opp.Mgr_Upside__c) mapKey += 1;
			if(totalsMap.containsKey(mapKey)) total = totalsMap.get(mapKey);
			//convert currency if user is not USD
			if(opp.ExpectedRevenue != null) {
				Double expectedRev = opp.ExpectedRevenue;
				if(UserInfo.getDefaultCurrency() != 'USD') {
					expectedRev = MySFDCUtil.convertCurrency(expectedRev, UserInfo.getDefaultCurrency(), 'USD');
				}
				total += expectedRev;
			}
			totalsMap.put(mapKey, total);
		}

		Decimal currentQuarterQuota = quotas.currentQuarterQuota;
		Decimal nextQuarterQuota = quotas.nextQuarterQuota;
		Decimal thirdQuarterQuota = quotas.thirdQuarterQuota;
		Decimal fourthQuarterQuota = quotas.fourthQuarterQuota;

		if(!String.isBlank(filter.productType)) 
		{
			currentQuarterQuota = quotas.currentQuarterQuotaByType.get(filter.productType);
			if(currentQuarterQuota == null) currentQuarterQuota = 0;
			nextQuarterQuota = quotas.nextQuarterQuotaByType.get(filter.productType);
			if(nextQuarterQuota == null) nextQuarterQuota = 0;
			thirdQuarterQuota = quotas.thirdQuarterQuotaByType.get(filter.productType);
			if(thirdQuarterQuota == null) thirdQuarterQuota = 0;
			fourthQuarterQuota = quotas.fourthQuarterQuotaByType.get(filter.productType);
			if(fourthQuarterQuota == null) fourthQuarterQuota = 0;
		}

		gcd.data[0] = new List<Double>();
		gcd.data[0].add( totalsMap.containsKey(13) ? totalsMap.get(13)/1000.0 : 0 );
		gcd.data[0].add( totalsMap.containsKey(12) ? totalsMap.get(12)/1000.0 : 0 );
		gcd.data[0].add( totalsMap.containsKey(11) ? totalsMap.get(11)/1000.0 : 0 );
		gcd.data[0].add( currentQuarterQuota/1000.0 );

		gcd.data[1] = new List<Double>();
		gcd.data[1].add( totalsMap.containsKey(23) ? totalsMap.get(23)/1000.0 : 0 );
		gcd.data[1].add( totalsMap.containsKey(22) ? totalsMap.get(22)/1000.0 : 0 );
		gcd.data[1].add( totalsMap.containsKey(21) ? totalsMap.get(21)/1000.0 : 0 );
		gcd.data[1].add( nextQuarterQuota/1000.0 );

		gcd.data[2] = new List<Double>();
		gcd.data[2].add( totalsMap.containsKey(33) ? totalsMap.get(33)/1000.0 : 0 );
		gcd.data[2].add( totalsMap.containsKey(32) ? totalsMap.get(32)/1000.0 : 0 );
		gcd.data[2].add( totalsMap.containsKey(31) ? totalsMap.get(31)/1000.0 : 0 );
		gcd.data[2].add( (thirdQuarterQuota != null ? thirdQuarterQuota/1000.0 : 0) );

		gcd.data[3] = new List<Double>();
		gcd.data[3].add( totalsMap.containsKey(43) ? totalsMap.get(43)/1000.0 : 0 );
		gcd.data[3].add( totalsMap.containsKey(42) ? totalsMap.get(42)/1000.0 : 0 );
		gcd.data[3].add( totalsMap.containsKey(41) ? totalsMap.get(41)/1000.0 : 0 );
		gcd.data[3].add( (fourthQuarterQuota != null ? fourthQuarterQuota/1000.0 : 0) );

		return gcd;
	}
}