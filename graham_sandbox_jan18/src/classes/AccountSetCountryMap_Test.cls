@isTest(SeeAllData=true)

public class AccountSetCountryMap_Test {
    
    static testmethod void Account_Proper_Country() {
      // Create a test account
      Account Acct = new Account(Name='AccountTest Key Account',
                                   BillingCountry='ca',
                                   BillingState='on',
                                   Site = '123');
      insert Acct;
	  
   	  Country_Mapping__c Country = [Select ID, Name, Sales_Region__c, Country_Grouping__c, Forecasting_Country__c, Forecast_Region__c, 
                                    Forecast_Territory__c, Regional_Grouping__c from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];

        
      Acct = [select Id, BillingCountry, BillingState, State_Mapping__c, Country_Mapping__c, ENGLISH_Country_Grouping__c, sales_region__c,
              ENGLISH_Forecast_Country__c, ENGLISH_Forecast_Region__c,ENGLISH_Forecast_Territory__c,ENGLISH_Regional_Grouping__c,
              Name from Account where Id =: Acct.Id];
      Account Acct2 = new Account(ID=Acct.ID,
                                  Name='AccountTest Key Account',
                                   BillingCountry='ca',
                                   BillingState='ONT',
                                   Site = '123');
      update Acct2;
      Acct = [select Id, BillingCountry, BillingState, State_Mapping__c, Country_Mapping__c, ENGLISH_Country_Grouping__c, sales_region__c,
              ENGLISH_Forecast_Country__c, ENGLISH_Forecast_Region__c,ENGLISH_Forecast_Territory__c,ENGLISH_Regional_Grouping__c,
              Name from Account where Id =: Acct.Id];
      System.assertEquals('Canada', Acct.BillingCountry);
      System.assertEquals(Country.ID, Acct.Country_Mapping__c);
      System.assertEquals(Country.Sales_Region__c, Acct.Sales_Region__c); 
      System.assertEquals('Ontario', Acct.BillingState);
      System.assertEquals(State.ID, Acct.State_Mapping__c); 
	  System.assertEquals(Country.Country_Grouping__c, Acct.ENGLISH_Country_Grouping__c);         
	  System.assertEquals(Country.Forecasting_Country__c, Acct.ENGLISH_Forecast_Country__c);
	  System.assertEquals(Country.Forecast_Region__c, Acct.ENGLISH_Forecast_Region__c);
	  System.assertEquals(Country.Regional_Grouping__c, Acct.ENGLISH_Regional_Grouping__c);
	  System.assertEquals(Country.Forecast_Territory__c, Acct.ENGLISH_Forecast_Territory__c);          
    }     
    
      static testmethod void Account_Improper_Country() {
      // Create a test account
      Account Acct = new Account(Name='AccountTest Key Account',
                                   BillingCountry='something',
                                   BillingState='on',                                 
                                   Site = '123');
      insert Acct;
   	  Country_Mapping__c Country = [Select ID, Name, Sales_Region__c, Country_Grouping__c, Forecasting_Country__c, Forecast_Region__c, 
                                    Forecast_Territory__c, Regional_Grouping__c from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];

      Acct = [select Id, BillingCountry, BillingState, State_Mapping__c, Country_Mapping__c, ENGLISH_Country_Grouping__c, sales_region__c,
              ENGLISH_Forecast_Country__c, ENGLISH_Forecast_Region__c,ENGLISH_Forecast_Territory__c,ENGLISH_Regional_Grouping__c,Name 
              from Account where Id =: Acct.Id];          
      System.assertEquals('something', Acct.BillingCountry);
      System.assertEquals(NULL, Acct.Country_Mapping__c);
      System.assertEquals('on', Acct.BillingState);
      System.assertEquals(NULL, Acct.State_Mapping__c);       
    }    
    
      static testmethod void Account_NULL_Country() {
      // Create a test account
      Account Acct = new Account(Name='AccountTest Key Account',
                                   BillingCountry=NULL,
                                   BillingState='on',
                                   Site = '123');
      insert Acct;
   	  Country_Mapping__c Country = [Select ID, Sales_Region__c, Country_Grouping__c, Forecasting_Country__c, Forecast_Region__c, 
                                    Forecast_Territory__c, Regional_Grouping__c, Name from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];
	  
      Acct = [select Id, BillingCountry, BillingState, State_Mapping__c, Country_Mapping__c, Name, ENGLISH_Country_Grouping__c, sales_region__c,
              ENGLISH_Forecast_Country__c, ENGLISH_Forecast_Region__c,ENGLISH_Forecast_Territory__c,ENGLISH_Regional_Grouping__c from Account where Id =: Acct.Id];       
      System.assertEquals(NULL, Acct.BillingCountry);
      System.assertEquals(NULL, Acct.Country_Mapping__c);
      System.assertEquals(NULL, Acct.Sales_Region__c);
      System.assertEquals('on', Acct.BillingState);
      System.assertEquals(NULL, Acct.State_Mapping__c);  
	  System.assertEquals(NULL, Acct.ENGLISH_Country_Grouping__c);         
	  System.assertEquals(NULL, Acct.ENGLISH_Forecast_Country__c);
	  System.assertEquals(NULL, Acct.ENGLISH_Forecast_Region__c);
	  System.assertEquals(NULL, Acct.ENGLISH_Regional_Grouping__c);
	  System.assertEquals(NULL, Acct.ENGLISH_Forecast_Territory__c);             
    }    
    
      static testmethod void Account_ImProper_State() {
      Account Acct = new Account(Name='AccountTest Key Account',
                                   BillingCountry='Can',
                                   BillingState='ot',
                                   Site = '123');
      insert Acct;
   	  Country_Mapping__c Country = [Select ID, Name from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];

      Acct = [select Id, BillingCountry, BillingState, State_Mapping__c, Country_Mapping__c, Name from Account where Id =: Acct.Id];
      System.assertEquals('Canada', Acct.BillingCountry);
      System.assertNotEquals('Ontario', Acct.BillingState);
      System.assertEquals(Country.ID, Acct.Country_Mapping__c);
      System.assertNotEquals(State.Id, Acct.State_Mapping__c);
    }
}