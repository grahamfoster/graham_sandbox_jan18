/**
 * An apex page controller that exposes Opportunity IB details
 */
@IsTest global with sharing class  OIBDetailsController_test {
    
        // Prepare initial sample data for running tests:
    @testSetup static void setup() {
		Date useDate = date.valueOf('2019-08-08');
        Date useDate2 = date.valueOf('2019-10-12');

		//  Pricebooks
       	List<Pricebook2> pb = new List<Pricebook2>{
            new Pricebook2(Name='United States Price Book',CURRENCYISOCODE='USD',isActive=TRUE),   // 0 - US
			new Pricebook2(Name='China Price Book',CURRENCYISOCODE='CNY',isActive=TRUE),		   // 1 - China
			new Pricebook2(Name='France Price Book',CURRENCYISOCODE='EUR',isActive=TRUE)           // 2 - France     
                }; 
       	insert pb;

        //  Product2
        List<Product2> prod = new List<Product2>{
            new Product2(Name='Assurance 0PM',ProductCode='ABSX ASSURANCE 0PM',SVC_OrclSerInstruModelName__c=''),  						// 0 - Item Master 0PM
			new Product2(Name='Assurance 1PM',ProductCode='ABSX ASSURANCE 1PM',SVC_OrclSerInstruModelName__c=''),  						// 1 - Item Master 1PM              
			new Product2(Name='Total Service Coverage',ProductCode='ST 8X5',SVC_OrclSerInstruModelName__c=''),							// 2 - Item Master CE              
			new Product2(Name='Assurance 0PM',ProductCode='ABSX ASSURANCE 0PM|API3000',SVC_OrclSerInstruModelName__c='API3000',Model__c='API3000'),		// 3 - 0PM API3000
			new Product2(Name='Assurance 0PM',ProductCode='ABSX ASSURANCE 0PM|5800',SVC_OrclSerInstruModelName__c='5800',Model__c='5800'),				// 4 - 0PM 5800
			new Product2(Name='Assurance 1PM',ProductCode='ABSX ASSURANCE 1PM|SHIMXR',SVC_OrclSerInstruModelName__c='SHIMXR',Model__c='SHIMXR'),			// 5 - 1PM SHIMXR
			new Product2(Name='Assurance 1Z',ProductCode='AX ASSURNCE 1ZM|SHIMXR',SVC_OrclSerInstruModelName__c='SHIMXR',Model__c='SHIMXR'),				// 6 - Bad data SHIMXR                
			new Product2(Name='Total Service Coverage',ProductCode='ST 8X5|LIF 488 SS',SVC_OrclSerInstruModelName__c='LIF 488 SS',Model__c='LIF 488 SS'),    	// 7 - CE - LIF 488 SS
			new Product2(Name='Total Service Coverage',ProductCode='ST 8X5|PA 800',SVC_OrclSerInstruModelName__c='PA 800',Model__c='PA 800'),             // 8 - CE - PA 800    
            new Product2(Name='Total Servie Coverge',ProductCode='S 8X5|P 800',SVC_OrclSerInstruModelName__c='PA 80',Model__c='PA 80')    };				// 9 - CE - Bad Data PA 800
        insert prod;

		// Country Mapping
		List<Country_Mapping__c> cm = new List<Country_Mapping__c>{
           new Country_Mapping__c(Name='United States',Permutations__c='UNITED STATES; USA; US; UNITED STATES OF AMERICA',
				Country_Code__c='US',Country__c='United States',Support_Region__c='AMERICAS',Default_Service_Price_Book__c=pb.get(0).id),	// -0 USA
           new Country_Mapping__c(Name='China',Permutations__c='China',
				Country_Code__c='CN',Country__c='China',Support_Region__c='APAC',Default_Service_Price_Book__c=pb.get(1).id),				// -1 China
           new Country_Mapping__c(Name='France',Permutations__c='France',
				Country_Code__c='FR',Country__c='France',Support_Region__c='EMEA',Default_Service_Price_Book__c=pb.get(2).id)};				// -2 France               
		insert cm;
       
       // Pricebook Entries
       	List<PricebookEntry> pbe = new List<PricebookEntry>();
        for(Pricebook2 cpb :pb){ 
            for(Product2 cp :prod){
                pbe.add(new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), Product2Id = cp.Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=cpb.CurrencyIsoCode));
				pbe.add(new PricebookEntry(Pricebook2ID=cpb.Id,Product2id=cp.id,CurrencyIsoCode=cpb.CurrencyIsoCode,isActive=TRUE,UnitPrice=4325.85));                        
            }			
        }
     	insert pbe;
              
        //  Account
        List<Account> acct = new List<Account>{
            new Account(Name = 'Test Co #1',CurrencyIsoCode = 'USD', BillingCountry = 'USA', Site='a'),				// -0 USA
			new Account(Name = 'Test Co #2',CurrencyIsoCode = 'CNY', BillingCountry = 'China', Site='b'),			// -1 China
            new Account(Name = 'Test Co #3',CurrencyIsoCode = 'EUR', BillingCountry = 'France', Site='c'),			// -2 France
            new Account(Name = 'Test Co #4',CurrencyIsoCode = 'CAD', BillingCountry = 'Canada', Site='d')};			// -3 Canada (No region Data)
        insert acct;
        
       // Contact
        Contact c = new Contact(AccountId= acct.get(0).id, firstname='Bob', lastname='Smith', Contact_Status__C='Active');
       	insert c;

		// User       
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
	    List<User> u = new List<User>{
           	new User(Alias = 'MSuser', Email='msu@testorg.com', EmailEncodingKey='UTF-8', LastName='MS USER', LanguageLocaleKey='en_US',
           		LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test'),
			new User(Alias = 'CEuser', Email='ceu@testorg.com', EmailEncodingKey='UTF-8', LastName='CE USER', LanguageLocaleKey='en_US',
                LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn1@testorg.test')};
       	insert u;
    	                
		// Territory2  -  Territory2 can not be populated in Tests, select entries from DB
        List<Territory2> t = [SELECT Name, Id, Default_Service_Pricebook__c,Default_Service_Sales_Rep__C,Default_CE_Service_Sales_Rep__C  FROM Territory2 WHERE Default_Service_Sales_Rep__C != '' AND Default_CE_Service_Sales_Rep__C != '' LIMIT 1];
 
		// ObjectTerritory2Association
       	List<ObjectTerritory2Association> OT2AList = new List<ObjectTerritory2Association>();    
       	for(Account a :acct){
        	ObjectTerritory2Association OT2A = new ObjectTerritory2Association(ObjectId= a.Id, Territory2Id=t.get(0).id,AssociationCause='Territory2Manual');
            OT2AList.add(OT2A);   
       	}
        insert OT2AList;       
       
        //  SVMXC__Installed_Product__c
        List<SVMXC__Installed_Product__c> ip = new List<SVMXC__Installed_Product__c>();
        for(Account a :acct){
        	ip.add(new SVMXC__Installed_Product__c(NAME='API3000 - AF28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(3).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
           	ip.add(new SVMXC__Installed_Product__c(NAME='5800 - AF28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(4).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
        	ip.add(new SVMXC__Installed_Product__c(NAME='SHIMXR - AF28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(5).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
			ip.add(new SVMXC__Installed_Product__c(NAME='SHIMR - AB28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(6).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
        	ip.add(new SVMXC__Installed_Product__c(NAME='LIF 488 SS - AF28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(7).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
           	ip.add(new SVMXC__Installed_Product__c(NAME='PA 800 - AF28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(8).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
        	ip.add(new SVMXC__Installed_Product__c(NAME='PDA 800 - AF28211410',CURRENCYISOCODE=a.CURRENCYISOCODE,SVMXC__COMPANY__C=a.id, 
				SVMXC__PRODUCT__C=prod.get(9).id, SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.id));
        }                   
		insert ip;          

        //  Service Maintenance Contract
        List<SVMXC__Service_Contract__c> SMC = new List<SVMXC__Service_Contract__c>();
		for(Account a :acct){
            //  0 Contract - Service Agreement - MS
            SMC.add(new SVMXC__Service_Contract__c(Name='contract MS1', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=a.CURRENCYISOCODE, 
				SVMXC__COMPANY__C=a.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12345'+a.site, 
				SVC_CATEGORY__C='Service Agreement', Renewal_Opportunity_Created__c=FALSE));
            //  1 Contract - Service Agreement - MS - duplicate Contract number
            SMC.add(new SVMXC__Service_Contract__c(Name='contract MS2', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=a.CURRENCYISOCODE, 
				SVMXC__COMPANY__C=a.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12345'+a.site, 
				SVC_CATEGORY__C='Service Agreement', Renewal_Opportunity_Created__c=FALSE));
            //  2 Contract - Warranty - MS 
            SMC.add(new SVMXC__Service_Contract__c(Name='contract MS3', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=a.CURRENCYISOCODE, 
				SVMXC__COMPANY__C=a.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12346'+a.site, 
				SVC_CATEGORY__C='Warranty and Extended Warranty', Renewal_Opportunity_Created__c=FALSE));
            //  3 Contract - Service Agreement - CE
            SMC.add(new SVMXC__Service_Contract__c(Name='contract CE1', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=a.CURRENCYISOCODE, 
				SVMXC__COMPANY__C=a.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12347'+a.site, 
				SVC_CATEGORY__C='Service Agreement', Renewal_Opportunity_Created__c=FALSE));
            //  4 Contract - Service Agreement - CE - duplicate Contract number
            SMC.add(new SVMXC__Service_Contract__c(Name='contract CE2', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=a.CURRENCYISOCODE, 
				SVMXC__COMPANY__C=a.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12347'+a.site, 
				SVC_CATEGORY__C='Service Agreement', Renewal_Opportunity_Created__c=FALSE));
            //  5 Contract - Warranty - CE 
            SMC.add(new SVMXC__Service_Contract__c(Name='contract CE3', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE=a.CURRENCYISOCODE, 
				SVMXC__COMPANY__C=a.id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12348'+a.site, 
				SVC_CATEGORY__C='Warranty and Extended Warranty', Renewal_Opportunity_Created__c=FALSE));            
        }    
		insert SMC;

	    //  SVMXC__Service_Contract_Products__c   
		List<SVMXC__Service_Contract_Products__c> SMCprod =  new List<SVMXC__Service_Contract_Products__c>();
       	for(SVMXC__Service_Contract__c contract :SMC){
            if(contract.name.contains('MS')){
            	SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate2,SVMXC__PRODUCT__C=prod.get(3).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='33753'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(0).id, SVMXC__Installed_Product__c=ip.get(1).id, SVC_Contract_Subline_Status__c='ACTIVE'));
            	SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(4).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='33754'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(0).id, SVMXC__Installed_Product__c=ip.get(2).id, SVC_Contract_Subline_Status__c='ACTIVE'));
            	SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate2,SVMXC__PRODUCT__C=prod.get(5).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='33755'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(1).id, SVMXC__Installed_Product__c=ip.get(3).id, SVC_Contract_Subline_Status__c='ACTIVE'));
            	SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(6).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='33756'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(0).id, SVMXC__Installed_Product__c=ip.get(4).id, SVC_Contract_Subline_Status__c='ACTIVE'));                            
			} else {
                SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate2,SVMXC__PRODUCT__C=prod.get(7).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='43753'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(2).id, SVMXC__Installed_Product__c=ip.get(5).id, SVC_Contract_Subline_Status__c='ACTIVE'));              
				SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate2,SVMXC__PRODUCT__C=prod.get(8).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='53753'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(2).id, SVMXC__Installed_Product__c=ip.get(6).id, SVC_Contract_Subline_Status__c='ACTIVE'));
            	SMCprod.add(new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=contract.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=contract.id, 
					SVMXC__END_DATE__C=useDate2,SVMXC__PRODUCT__C=prod.get(9).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='63753'+contract.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(2).id, SVMXC__Installed_Product__c=ip.get(7).id, SVC_Contract_Subline_Status__c='ACTIVE'));
            }  
		}
        insert SMCprod;

        // c2oSettings__c
        List<c2oSettings__c> allsettings = new List<c2oSettings__c>{
            new c2oSettings__c(Name='Contract Renewals', Contract_Type__c='Service Agreement', Naming_Convention__C='CRNWL', Product_Type__c='Contract',
				RecordType__c='012F00000011o28', Stage_Name__c = 'Recognition of Needs'),
            new c2oSettings__c(Name='Warranty Conversion', Contract_Type__c='Warranty and Extended Warranty', Naming_Convention__C='WCONV', Product_Type__c='Contract',
				RecordType__c='012F00000011o28', Stage_Name__c = 'Recognition of Needs')}; 
		insert allsettings;
        
        // Contract2Opportunity_Configuration__c
        Contract2Opportunity_Configuration__c config = new Contract2Opportunity_Configuration__c(
            name= 'ABC',
            Assign_Query_Filter__c = 'c2o_AutoGeneration_Status__c = \'Populated\'',
			Create_Query_Filter__c = 'Renewal_Opportunity_Created__c = FALSE AND SVMXC__Active__c = TRUE AND c2o_First_Product_Expiration__c < 2017-01-01  AND  c2o_First_Product_Expiration__c > TODAY ORDER BY PARENT_CONTRACT_NUMBER__c ASC',
            Link_Query_Filter__c = 'c2o_AutoGeneration_Status__c = \'Created\'',
            Log_Level__c = 'Warning',
			Populate_Query_Filter__c ='c2o_AutoGeneration_Status__c = \'Sanitized\'',
            Sanitize_Query_Filter__c ='c2o_AutoGeneration_Status__c = \'Linked\'',
   	        Scope__c = 200);  
        insert config;
        
        // CE Exceptions
        List<c2oExceptions__c> allExceptions = new List<c2oExceptions__c>{
            new c2oExceptions__c(Name='PA 800', Exception_Type__c='CE'),
			new c2oExceptions__c(Name='LIF 488 SS', Exception_Type__c='CE')};  
		insert allExceptions;                
        
        // Warranty Lookups
   		List<c2oWarranty__c> warranties = new List<c2oWarranty__c>{
           new c2oWarranty__c(name='1',WARRANTY__C='',				REGION__C='',			DEFAULT_PLAN__C='ABSX ASSURANCE 0PM', DEFAULT_CE_PLAN__C='ST 8X5'),
           new c2oWarranty__c(name='2',WARRANTY__C='ABSX WARRANTY',	REGION__C='',			DEFAULT_PLAN__C='ABSX ASSURANCE 1PM', DEFAULT_CE_PLAN__C='ST 8X5'),
           new c2oWarranty__c(name='3',WARRANTY__C='',				REGION__C='AMERICAS',	DEFAULT_PLAN__C='ABSX ASSURANCE 0PM', DEFAULT_CE_PLAN__C='ST 8X5'),               
           new c2oWarranty__c(name='4',WARRANTY__C='ABSX WARRANTY',	REGION__C='AMERICAS',	DEFAULT_PLAN__C='ABSX ASSURANCE 1PM', DEFAULT_CE_PLAN__C='ST 8X5'),
           new c2oWarranty__c(name='5',WARRANTY__C='',				REGION__C='EMEA',		DEFAULT_PLAN__C='ABSX ASSURANCE 0PM', DEFAULT_CE_PLAN__C='ST 8X5'),               
           new c2oWarranty__c(name='6',WARRANTY__C='ABSX WARRANTY',	REGION__C='EMEA',		DEFAULT_PLAN__C='ABSX ASSURANCE 1PM', DEFAULT_CE_PLAN__C='ST 8X5'),               
           new c2oWarranty__c(name='7',WARRANTY__C='',				REGION__C='APAC',		DEFAULT_PLAN__C='ABSX ASSURANCE 0PM', DEFAULT_CE_PLAN__C='ST 8X5'),               
           new c2oWarranty__c(name='8',WARRANTY__C='ABSX WARRANTY',	REGION__C='APAC',		DEFAULT_PLAN__C='ABSX ASSURANCE 1PM', DEFAULT_CE_PLAN__C='ST 8X5')};
		insert  warranties;
       
       // Opportunities
       List<Opportunity> opps = new List<Opportunity>();
       // 0 -- Account 1 (USA) - MS Contract Renewal (SMC 0)
       opps.add(new Opportunity(name='Opp 1',Previous_Contract__c=SMC.get(0).id,Pricebook2Id = pb.get(0).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
					RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(0).Parent_Contract_Number__c,AccountId = SMC.get(0).SVMXC__COMPANY__C,
					Contract_Expiry_Date__c = SMC.get(0).SVMXC__END_DATE__C,CloseDate = SMC.get(0).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
					CurrencyIsoCode = SMC.get(0).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created'));
	   // 1 -- Account 1 (USA) - MS Warranty Convert (SMC 2)
       opps.add(new Opportunity(name='Opp 2',Previous_Contract__c=SMC.get(2).id,Pricebook2Id = pb.get(0).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(2).Parent_Contract_Number__c,AccountId = SMC.get(2).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(2).SVMXC__END_DATE__C,CloseDate = SMC.get(2).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(2).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created'));
       // 2 -- Account 1 (USA) - CE Contract Renewal (SMC 3)
       opps.add(new Opportunity(name='Opp 3',Previous_Contract__c=SMC.get(3).id,Pricebook2Id = pb.get(0).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(3).Parent_Contract_Number__c,AccountId = SMC.get(3).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(3).SVMXC__END_DATE__C,CloseDate = SMC.get(3).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(3).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created',c2o_CE_Opportunity__c=TRUE));
       // 3 -- Account 1 (USA) - CE Warranty Convert (SMC 5)
       opps.add(new Opportunity(name='Opp 4',Previous_Contract__c=SMC.get(2).id,Pricebook2Id = pb.get(0).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(2).Parent_Contract_Number__c,AccountId = SMC.get(2).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(2).SVMXC__END_DATE__C,CloseDate = SMC.get(2).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(2).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created',c2o_CE_Opportunity__c=TRUE));              
       // 4 -- Account 2 (China) - MS Contract Renewal (SMC 6)
       opps.add(new Opportunity(name='Opp 5',Previous_Contract__c=SMC.get(6).id,Pricebook2Id = pb.get(1).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(6).Parent_Contract_Number__c,AccountId = SMC.get(6).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(6).SVMXC__END_DATE__C,CloseDate = SMC.get(6).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(6).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created'));
	   // 5 -- Account 2 (China) - MS Warranty Convert (SMC 7)
       opps.add(new Opportunity(name='Opp 6',Previous_Contract__c=SMC.get(7).id,Pricebook2Id = pb.get(1).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(7).Parent_Contract_Number__c,AccountId = SMC.get(7).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(7).SVMXC__END_DATE__C,CloseDate = SMC.get(7).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(7).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created'));
	   // 6 -- Account 2 (China) - CE Contract Renewal (SMC 9)
       opps.add(new Opportunity(name='Opp 7',Previous_Contract__c=SMC.get(9).id,Pricebook2Id = pb.get(1).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(9).Parent_Contract_Number__c,AccountId = SMC.get(9).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(9).SVMXC__END_DATE__C,CloseDate = SMC.get(9).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(9).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created',c2o_CE_Opportunity__c=TRUE));
	   // 7 -- Account 3 (China) - CE Warranty Convert (SMC 10)
       opps.add(new Opportunity(name='Opp 8',Previous_Contract__c=SMC.get(10).id,Pricebook2Id = pb.get(1).id,Product_Type__c ='Contract',StageName ='Recognition of Needs',
				RecordTypeId ='012F00000011o28',Contract_Number__c = SMC.get(10).Parent_Contract_Number__c,AccountId = SMC.get(10).SVMXC__COMPANY__C,
				Contract_Expiry_Date__c = SMC.get(10).SVMXC__END_DATE__C,CloseDate = SMC.get(10).SVMXC__END_DATE__C,Primary_Contact__c = c.id,
				CurrencyIsoCode = SMC.get(10).CurrencyIsoCode,c2o_AutoGeneration_Status__c = 'Created',c2o_CE_Opportunity__c=TRUE));  
		Test.startTest();
        insert opps;   
        
        //update related Contracts
        SMC.get(0).c2o_Renewal_Opportunity__c = opps.get(0).id;
	    SMC.get(2).c2o_Renewal_Opportunity__c = opps.get(1).id;
       	SMC.get(3).c2o_Renewal_Opportunity__c = opps.get(2).id;
        SMC.get(5).c2o_Renewal_Opportunity__c = opps.get(3).id;
        SMC.get(6).c2o_Renewal_Opportunity__c = opps.get(4).id;
        SMC.get(7).c2o_Renewal_Opportunity__c = opps.get(5).id;
        SMC.get(9).c2o_Renewal_Opportunity__c = opps.get(6).id;
        SMC.get(10).c2o_Renewal_Opportunity__c = opps.get(7).id;
        update SMC;
       
       //Add SMC with Bad Data
       SVMXC__Service_Contract__c bSMC = new SVMXC__Service_Contract__c(Name='contract bad1', SVC_Oracle_Status__c='ACTIVE', CURRENCYISOCODE= acct.get(0).CurrencyIsoCode, 
				SVMXC__COMPANY__C=acct.get(0).id,SVMXC__CONTACT__C=c.id, SVMXC__END_DATE__C=useDate, PARENT_CONTRACT_NUMBER__c='12399'+acct.get(0).site, 
				SVC_CATEGORY__C='XYZ', Renewal_Opportunity_Created__c=FALSE);
       insert bSMC;
       SVMXC__Service_Contract_Products__c bSMCprod =  new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE=bSMC.CURRENCYISOCODE,SVMXC__SERVICE_CONTRACT__C=bSMC.id, 
					SVMXC__END_DATE__C=useDate2,SVMXC__PRODUCT__C=prod.get(3).id, SVMXC__START_DATE__C=useDate, EXTERNAL_ID__C='33799'+bSMC.id,
					SVC_ITEM_MASTER_NAME__C=prod.get(0).id, SVMXC__Installed_Product__c=ip.get(1).id);
       insert bSMCprod;
       
		// OpportunityIBs
       	List<Opportunity_IB__C> IBs = new List<Opportunity_IB__C>();
		Map<ID, SVMXC__Service_Contract__c> SMCmap= new Map<ID, SVMXC__Service_Contract__c>(SMC);     
       	for(SVMXC__Service_Contract_Products__c line :SMCprod){          
           	if( String.isNotEmpty(SMCmap.get(line.SVMXC__Service_Contract__c).c2o_Renewal_Opportunity__c)){          
               	IBs.add(new Opportunity_IB__C(Covered_Product__c=line.id, Opportunity__c=SMCmap.get(line.SVMXC__Service_Contract__c).c2o_Renewal_Opportunity__c));
           	}
       	}  
       	insert IBs;
        
        Test.stopTest();
        

	}
    
    static testmethod void test_OIBDetailsController () {
        // grab test data
   		Map<Id, Opportunity> initialQuery = new Map<Id, Opportunity>([	SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,
																			c2o_CE_Opportunity__c, accountid
  																		FROM 
																			Opportunity WHERE name='Opp 1']);
        // Grab linked IB's
        List<Opportunity_IB__c> oib = new List<Opportunity_IB__c>([SELECT id, Covered_Product__r.SVMXC__Service_Contract__c ,Opportunity__c, Serial_Number__c, Model__c, Current_Plan__c, Expiration__c, Opportunity__r.Accountid  FROM Opportunity_IB__c WHERE Opportunity__r.name = 'Opp 1']);
        // Duplicate Opportunity (for sanitization to find)
        Opportunity dupOpp = new Opportunity(Name = 'Dupe 1', CurrencyIsoCode='USD', Pricebook2Id=initialQuery.values().get(0).Pricebook2Id , c2o_AutoGeneration_Status__c='Duplicate', c2o_CE_Opportunity__c=false, AccountId=initialQuery.values().get(0).AccountId, StageName='Recognition of Needs', CloseDate=date.valueOf('2019-10-12'),CONTRACT_EXPIRY_DATE__C=date.valueOf('2019-10-12'));
        insert dupOpp;
        
		// Duplicate Opportunity IB for each Duplicate Opportunity (for sanitization to find)
		List <Opportunity_IB__c> dupOIB= new List<Opportunity_IB__c>();
        for(Opportunity_IB__c ib :oib){
        	dupOIB.add(new Opportunity_IB__c(Covered_Product__c=ib.Covered_Product__c, Opportunity__c=initialQuery.values().get(0).id));
        }
        insert dupOIB;
        
        dupOIB =([SELECT id, Covered_Product__c, Opportunity__c, Model__c,Serial_Number__c FROM Opportunity_IB__c]);
        
        // Add Opportunity Line Items to each Duplicate Opportunity
        List <OpportunityLineItem> dupOLI= new List<OpportunityLineItem>();
        Map<Id,Product2> prods = new  Map<Id,Product2>([SELECT id, Name,ProductCode,SVC_OrclSerInstruModelName__c FROM Product2]); 
           
		List<PricebookEntry> pbes = new List<PricebookEntry>([SELECT Name, id, Pricebook2Id, Product2Id, UnitPrice, IsActive, CurrencyIsoCode FROM PricebookEntry]);             
        for(Opportunity_IB__c opIB :dupOIB ){
            String thisProduct = '';
            String thisEntry = '';
            for(Product2 myprod :prods.values()){
                if(myprod.SVC_OrclSerInstruModelName__c == opIB.Model__c){
                    thisProduct = myprod.id;
                }
            }
			for(PricebookEntry mypbe :pbes){
                if(mypbe.Pricebook2Id == dupOpp.Pricebook2Id && thisProduct == mypbe.Product2Id){
                    thisEntry = mypbe.Id;
                }
            }
            dupOLI.add(new OpportunityLineItem(OPPORTUNITYID=dupOpp.id, PricebookEntryId=thisEntry, QUANTITY=1,UNITPRICE=0,MODEL__C= opIB.Model__c,SERIAL_NUMBER__C=opIB.Serial_Number__c ));
        }
        insert dupOLI;

        system.debug('***- Start  test_OIBDetailsController' );      
		Test.startTest();   
          	// Instantiate a new controller with all parameters in the page
        	OIBDetailsController controller = new OIBDetailsController ();
        	Test.setCurrentPageReference(new PageReference('Page.OIBDetails')); 
			System.currentPageReference().getParameters().put('id', oib.get(0).id);
	        controller.getIB();
system.debug('***- OIB id: ' + oib.get(0).id );     
        
		Test.stopTest();        
		system.debug('***- Stop  test_OIBDetailsController' );        
    
    
   
	} 
    
    
}