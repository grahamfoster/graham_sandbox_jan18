@isTest
public with sharing class userfavorites_Unittest {
  Static TestMethod void userfavorites_Unittest()
  {
    User u = Util.createUser();
    String uid = userInfo.getuserId();
    Persona__c persona =  Util.createPersona();
    Manufacturer__c manufacturer =  Util.createManufacturerABX();
    Manufacturer__c manufacturer1 =  Util.createManufacturer();
    Application__c application =  Util.createApplication();  
    Application_Variants__c applicationvar = Util.createAppVariants(application.Id);
    Product_Category__c prodcat = Util.CreateProductCategory();     
    Specifications_Meta__c specMeta = Util.CreateProductfeatureCategory(prodcat.Id);
    Product_Meta__c prodMeta = Util.CreateProductfeature(specMeta.Id);
    Product2 abprod = Util.createProduct2(manufacturer.Id, prodcat.Id);
    Product2 compprod = Util.createProduct2(manufacturer1.Id, prodcat.Id);
    Package__c pckg = Util.CreatePackage(application.Id , applicationvar.Id , persona.Id , abprod.Id , compprod.Id);
    Battlecard__c btlcrd = Util.createBattlecard(pckg.Id);
    Add_to_Favorites_Junction__c addfav = Util.createAddtoFavjuntion(pckg.Id , uiD);
    
    userfavorites userfav1 = new userfavorites();
    userfav1.getfav();   
    
    Rating_Management__c rmc = Util.createRatingManagement(pckg.Id , uid);
    
    userfavorites userfav = new userfavorites();
    userfav.getfav();    
    
  }
  
}