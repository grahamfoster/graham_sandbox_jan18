@isTest
public with sharing class UpdatePackageRating_test {

public static testMethod void updatePackageMethod(){
	
	Package__c pack = new Package__c();
	insert pack;
	
	Rating_Management__c rmcObj = new Rating_Management__c();
	rmcObj.Package__c = pack.Id;
	insert rmcObj;
	
	Rating_Management__c rmcObj1 = new Rating_Management__c();
	rmcObj1.Package__c = pack.Id;
	insert rmcObj1;


}
public static testMethod void updatePackageMethod1(){
	try{
	Package__c pack = new Package__c();
	insert pack;
	
	Product_Category__c pc = new Product_Category__c();
	pc.Product_Category_Name__c = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
	insert pc;

	Rating_Management__c rmcObj = new Rating_Management__c();
	rmcObj.Package__c = pack.Id;
	insert rmcObj;}
	
	catch(Exception e) {
	System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
	System.Assert(e.getMessage().contains('Product_Category__c.Id'));
	System.Assert(e.getMessage().contains('My Error Message'));
} 
	
}

}