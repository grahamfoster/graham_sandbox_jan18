public class ABSciexDashboardFactory {
	public static final String CURRENT_FORECAST = 'CurrentForecast';
	public static final String NEXT_FORECAST = 'NextForecast';
	public static final String CURRENT_FORECAST_AND_UP = 'CurrentForecastAndUp';
	public static final String NEXT_FORECAST_AND_UP = 'NextForecastAndUp';
	public static final String CURRENT_ADJUSTED = 'CurrentForecastAdjusted';
	public static final String NEXT_ADJUSTED = 'NextForecastAdjusted';
	public static final String FULL_YTD_ACTUALS = 'FullYTDActuals';
	public static final String YTD_WIN_RATE = 'YTDWinRate';
	public static final String PIPELINE_CHART = 'pipeline_chart';
	public static final String CUMULATIVE_QUOTA = 'cumulative_quota';
	public static final String QUOTA_VARIANCE = 'quota_variance';

	public static IMySFDCDashboard getDashboard(String key, MySFDCFilter filter, MySFDCQuota quotas, Map<String,Decimal> conversionRatesMap) {
		if(key == CURRENT_FORECAST) {
			Period p = MySFDCUtil.getCurrentQuarter();
			MySFDCForecastDashboard db = new MySFDCForecastDashboard();
			db.setQuota(quotas.currentQuarterQuota);
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setStartDate(p.StartDate);
			db.setEndDate(p.EndDate);
			db.quarterNumber = p.Number;
			db.isCurrentQuarter = true;
			db.conversionRateMap = conversionRatesMap;
			return db;
		}
		if(key == NEXT_FORECAST) {
			Period p = MySFDCUtil.getNextQuarter();
			MySFDCForecastDashboard db = new MySFDCForecastDashboard();
			db.setQuota(quotas.nextQuarterQuota);
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setStartDate(p.StartDate);
			db.setEndDate(p.EndDate);
			db.quarterNumber = p.Number;
			db.isCurrentQuarter = false;
			db.conversionRateMap = conversionRatesMap;
			return db;
		}
		if(key == CURRENT_FORECAST_AND_UP) {
			Period p = MySFDCUtil.getCurrentQuarter();
			MySFDCForecastAndUpDashboard db = new MySFDCForecastAndUpDashboard();
			db.setQuota(quotas.currentQuarterQuota);
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setStartDate(p.StartDate);
			db.setEndDate(p.EndDate);
			db.quarterNumber = p.Number;
			db.isCurrentQuarter = true;
			db.conversionRateMap = conversionRatesMap;
			return db;
		}
		if(key == NEXT_FORECAST_AND_UP) {
			Period p = MySFDCUtil.getNextQuarter();
			MySFDCForecastAndUpDashboard db = new MySFDCForecastAndUpDashboard();
			db.setQuota(quotas.nextQuarterQuota);
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setStartDate(p.StartDate);
			db.setEndDate(p.EndDate);
			db.quarterNumber = p.Number;
			db.isCurrentQuarter = false;
			db.conversionRateMap = conversionRatesMap;
			return db;
		}
		if(key == CURRENT_ADJUSTED) {
			Period p = MySFDCUtil.getCurrentQuarter();
			MySFDCAdjustedDashboard db = new MySFDCAdjustedDashboard();
			db.setQuota(quotas.currentQuarterQuota);
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setStartDate(p.StartDate);
			db.setEndDate(p.EndDate);
			db.quarterNumber = p.Number;
			db.isCurrentQuarter = true;
			db.conversionRateMap = conversionRatesMap;
			return db;
		}
		if(key == NEXT_ADJUSTED) {
			Period p = MySFDCUtil.getNextQuarter();
			MySFDCAdjustedDashboard db = new MySFDCAdjustedDashboard();
			db.setQuota(quotas.nextQuarterQuota);
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setStartDate(p.StartDate);
			db.setEndDate(p.EndDate);
			db.quarterNumber = p.Number;
			db.isCurrentQuarter = false;
			db.conversionRateMap = conversionRatesMap;
			return db;
		}
		if(key == YTD_WIN_RATE) {
			MySFDCYTDWinRateDashboard db = new MySFDCYTDWinRateDashboard();
			db.filter = filter;
			db.setRunAs(filter.runAs);
			Date startDate = Date.newInstance(Date.today().year(), 1, 1);
			Date endDate = Date.today();
			db.setStartDate(startDate);
			db.setEndDate(endDate);
			return db;
		}
		if(key == FULL_YTD_ACTUALS) {
			MySFDCFullQtrYTDActualsDashboard db = new MySFDCFullQtrYTDActualsDashboard();
			db.filter = filter;
			db.setRunAs(filter.runAs);
			Date startDate = Date.newInstance(Date.today().year(), 1, 1);
			Date endDate = Date.today();
			db.setStartDate(startDate);
			db.setEndDate(endDate);
			return db;
		}

		if(key == PIPELINE_CHART)
		{
			MySFDCServicePipelineDashboard db = new MySFDCServicePipelineDashboard();
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setReport(key);
			db.quotas = quotas;
			return db;
		}

		if(key == CUMULATIVE_QUOTA)
		{
			MySFDCServiceCumulativeYTDQuotaDashboard db = new MySFDCServiceCumulativeYTDQuotaDashboard();
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setReport(key);
			db.quotas = quotas;
			return db;
		}

		if(key == QUOTA_VARIANCE)
		{
			MySFDCServiceVarianceDashboard db = new MySFDCServiceVarianceDashboard();
			db.filter = filter;
			db.setRunAs(filter.runAs);
			db.setReport(key);
			db.quotas = quotas;
			return db;
		}
		return null;
	}

	public static List<List<GoogleGaugeChartData>> getDashboardsTable(MySFDCFilter filter){
		MySFDCQuota quotas = MySFDCUtil.getQuotas(filter.runAs.Id);
		Map<String,Decimal> conversionRatesMap = MySFDCUtil.getConversionRates(filter.runAs);
		Decimal forecastConversionRate = conversionRatesMap.containsKey('Forecast') ? conversionRatesMap.get('Forecast') : 1.0;
		Decimal upsideExistingConversionRate = conversionRatesMap.containsKey('Upside Existing') ? conversionRatesMap.get('Upside Existing') : 1.0;
		Decimal upsideNewConversionRate = conversionRatesMap.containsKey('Upside New') ? conversionRatesMap.get('Upside New') : 1.0;

		List<List<GoogleGaugeChartData>> rowsOfCharts = new List<List<GoogleGaugeChartData>>();

		if(!MySFDCUtil.isServiceUser(filter.runAs)) 
		{
			//row 1
			rowsOfCharts.add(new List<GoogleGaugeChartData>());
			IMySFDCDashboard db10 = ABSciexDashboardFactory.getDashboard (FULL_YTD_ACTUALS, filter, quotas, conversionRatesMap);
			if(((MySFDCFullQtrYTDActualsDashboard)db10).rendered) {
				db10.query();
				rowsOfCharts.get(0).add(db10.getGoogleGaugeChartData());
			}

			IMySFDCDashboard db11 = getDashboard(CURRENT_FORECAST, filter, quotas, conversionRatesMap);
			db11.query();
			rowsOfCharts.get(0).add(db11.getGoogleGaugeChartData());

			IMySFDCDashboard db12 = getDashboard(NEXT_FORECAST, filter, quotas, conversionRatesMap);
			db12.query();
			rowsOfCharts.get(0).add(db12.getGoogleGaugeChartData());

			//row 2
			rowsOfCharts.add(new List<GoogleGaugeChartData>());
			IMySFDCDashboard db20 = ABSciexDashboardFactory.getDashboard (YTD_WIN_RATE, filter, quotas, conversionRatesMap);
			db20.query();
			rowsOfCharts.get(1).add(db20.getGoogleGaugeChartData());

			IMySFDCDashboard db21 = getDashboard(CURRENT_FORECAST_AND_UP, filter, quotas, conversionRatesMap);
			db21.query();
			rowsOfCharts.get(1).add(db21.getGoogleGaugeChartData());

			IMySFDCDashboard db22 = getDashboard(NEXT_FORECAST_AND_UP, filter, quotas, conversionRatesMap);
			db22.query();
			rowsOfCharts.get(1).add(db22.getGoogleGaugeChartData());

			//row 3
			rowsOfCharts.add(new List<GoogleGaugeChartData>());

			GoogleGaugeChartData db30 = new GoogleGaugeChartData('blank');
			rowsOfCharts.get(2).add(db30);

			IMySFDCDashboard db31 = ABSciexDashboardFactory.getDashboard (CURRENT_ADJUSTED, filter, quotas, conversionRatesMap);
			db31.query();
			rowsOfCharts.get(2).add(db31.getGoogleGaugeChartData());

			IMySFDCDashboard db32 = ABSciexDashboardFactory.getDashboard (NEXT_ADJUSTED, filter, quotas, conversionRatesMap);
			db32.query();
			rowsOfCharts.get(2).add(db32.getGoogleGaugeChartData());
		}else
		{
			//row 1
			/*
			rowsOfCharts.add(new List<GoogleGaugeChartData>());
			IMySFDCDashboard db20 = ABSciexDashboardFactory.getDashboard (YTD_WIN_RATE, filter, quotas, conversionRatesMap);
			db20.query();
			rowsOfCharts.get(0).add(db20.getGoogleGaugeChartData());
			*/
		}

		return rowsOfCharts;
	}
}