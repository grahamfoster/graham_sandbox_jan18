/**
** @author Reid Beckett, Cloudware Connections
** @created July 14/2014
**
** Refactored out of MySFDCController
** 
**/
public class GoogleGaugePercentageChartData extends GoogleGaugeChartData {
	public Decimal redFrom {get;set;}
	public Decimal redTo {get;set;}
	public Decimal yellowFrom {get;set;}
	public Decimal yellowTo {get;set;}
	public Decimal greenFrom {get;set;}
	public Decimal greenTo {get;set;}

	public GoogleGaugePercentageChartData(String id){
		super(id);
		this.redFrom = 10;
		this.redTo = 30;
		this.yellowFrom = 30;
		this.yellowTo = 50;
		this.greenFrom = 50;
		this.greenTo = 70;
	}

	public override Decimal[] getTicks() {
		return new Decimal[] {
			10,
			30,
			50,
			70
		};
	}

	public override Decimal getRedFrom() {
		return this.redFrom;
	}

	public override Decimal getRedTo() {
		return this.redTo;
	}

	public override Decimal getYellowFrom(){
		return this.yellowFrom;
	}

	public override Decimal getYellowTo(){
		return this.yellowTo;
	}

	public override Decimal getGreenFrom() {
		return this.greenFrom;
	}

	public override Decimal getGreenTo() {
		return this.greenTo;
	}

	public override Decimal getScaledQuota() {
		return quota != null ? quota : 0;
	}

	public override Decimal getScaledActual() {
		return actual != null ? actual : 0;
	}

	public override String getTotalNumberFormat(){
		return '0';
	}

	public override String getNumberFormatSuffix() {
		return '%';
	}
}