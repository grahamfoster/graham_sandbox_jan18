@isTest
private class SendSurveyTest {
	
	public static Contact testContact;
	public static Case testCase;
	public static Case_Contact__c testCC;
    public static Account testAccount;

	static testMethod void init(){
	RecordType AccRecType = SfdcUtil.getRecordType('Account', 'NAPSM');    
    RecordType ConRecType = SfdcUtil.getRecordType('Contact', 'NAPSM');    
    RecordType CaseRecType = SfdcUtil.getRecordType('Case', 'SVC_Escalation');    
        
        testAccount = new Account (
            Name = 'Test Account',
            Site = 'Test',
            Institution_Type__c = 'Academic',
            RecordTypeId = AccRecType.Id, 
        	BillingCountry = 'CA');
        insert testAccount;
        
        
        testContact = new Contact(
            LastName    = 'Test',
            FirstName   = 'Contact',
            Email       = 'test@egmail.com',
            AccountID = testAccount.ID,
            RecordType = ConRecType);
        insert testContact;
        
        testCase = new Case(
            Subject = 'Test Case',
            RecordTypeId = CaseRecType.Id
        );
        insert testCase;

        
        testCC = new Case_Contact__c(
        	Case__c 	= testCase.id,
        	Contact__c	= testContact.id
        );
        insert testCC;
	}

    static testMethod void testSendSurveyAll() {

		init();
        test.startTest();
        
        PageReference pageRef = Page.SendSurvey;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('caseId', testCase.Id);
        ApexPages.currentPage().getParameters().put('buttonName', 'contactAll');
        
        SendSurveyController ss = new SendSurveyController();
        ss.initateSendingSurveys();
        
        system.assertEquals(String.valueOf(new PageReference('/'+testCase.Id)), String.valueOf((ss.initateSendingSurveys())) );
        system.assertEquals(false, testCC.Send_Survey__c);
        test.stopTest();
        
    }
    static testMethod void testSendSurveyOwner() {

		init();
        test.startTest();
        
        PageReference pageRef = Page.SendSurvey;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('caseId', testCase.Id);
        ApexPages.currentPage().getParameters().put('buttonName', 'caseOwner');
        
        SendSurveyController ss = new SendSurveyController();
        ss.initateSendingSurveys();
        
        system.assertEquals(String.valueOf(new PageReference('/'+testCase.Id)), String.valueOf((ss.initateSendingSurveys())) );
        system.assertEquals(false, testCC.Send_Survey__c);
        test.stopTest();
        
    }
    static testMethod void testSendSurveyContactOnly() {

		init();
        test.startTest();
        
        PageReference pageRef = Page.SendSurvey;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('caseId', testCase.Id);
        ApexPages.currentPage().getParameters().put('ccid', testCC.Id);
        ApexPages.currentPage().getParameters().put('buttonName', 'contactOnly');
        
        SendSurveyController ss = new SendSurveyController();
        ss.initateSendingSurveys();
        
        system.assertEquals(String.valueOf(new PageReference('/'+testCase.Id)), String.valueOf((ss.initateSendingSurveys())) );
        system.assertEquals(false, testCC.Send_Survey__c);
        test.stopTest();
        
    }

}