/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 24/2015
**
** Test coverage for WorkOrderTrigger and WorkOrderTriggerHandler
** 
**/
@isTest
public class WorkOrderTriggerTests 
{
    public static testMethod void test1()
    {
        Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'United States');
        insert testAccount;
        
        RecordType prodrt = SfdcUtil.getRecordType('Product2', 'AB_Sciex');
        Product2 prod = new Product2(Name='Test Product', RecordTypeID = prodrt.ID);
        insert prod;
        
        SVMXC__Installed_Product__c comp = new SVMXC__Installed_Product__c(SVMXC__Company__c = testAccount.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id );
        insert comp;

        RecordType rt = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
        Case ccase = new Case(RecordTypeId = rt.Id, AccountId = testAccount.Id, CurrencyIsoCode = 'USD', SVMXC__Component__c = comp.Id);
        insert ccase;
        
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(CurrencyIsoCode = 'CAD', SVMXC__Case__c = ccase.Id, SVC_Problem_Code__c = 'Electrical', SVC_Resolution_Code__c = 'Replaced Part(s)', SVC_Resolution_Summary__c = 'abc');
        insert wo;
        
        Test.startTest();
        ccase = [select SVC_SR_Problem_Code__c, SVC_Resolution_Code__c, SVMX_Resolution_Summary__c from Case where Id = :ccase.Id];
        system.assertEquals('Electrical', ccase.SVC_SR_Problem_Code__c);

        SVMXC__Service_Order__c wo_for_insert = new SVMXC__Service_Order__c(CurrencyIsoCode = 'CAD', SVMXC__Case__c = ccase.Id);
        insert wo_for_insert;
        wo_for_insert = [select Id, SVC_Problem_Code__c, SVC_Resolution_Code__c, SVC_Resolution_Summary__c from SVMXC__Service_Order__c where Id = :wo_for_insert.Id];
        system.assertEquals(ccase.SVC_SR_Problem_Code__c, wo_for_insert.SVC_Problem_Code__c);
        system.assertEquals(ccase.SVC_Resolution_Code__c, wo_for_insert.SVC_Resolution_Code__c);
        system.assertEquals(ccase.SVMX_Resolution_Summary__c, wo_for_insert.SVC_Resolution_Summary__c);
        
        wo = [select Id, SVMXC__Company__c, CurrencyIsoCode from SVMXC__Service_Order__c where Id = :wo.Id];
        system.assertEquals('USD', wo.CurrencyIsoCode);
        system.assertEquals(testAccount.Id, wo.SVMXC__Company__c);

        SVMXC__Service_Order__c wo2 = new SVMXC__Service_Order__c(SVMXC__Case__c = ccase.Id);
        insert wo2;
        wo2 = [select Id, SVMXC__Company__c, CurrencyIsoCode from SVMXC__Service_Order__c where Id = :wo.Id];
        system.assertEquals('USD', wo2.CurrencyIsoCode);
        
        wo.SVC_Problem_Code__c = 'Vacuum';
        wo.SVC_Resolution_Code__c = 'Replaced Consumable(s)';
        wo.SVC_Resolution_Summary__c = 'def';
        update wo;
        ccase = [select SVC_SR_Problem_Code__c from Case where Id = :ccase.Id];
  //      system.assertEquals('Vacuum', ccase.SVC_SR_Problem_Code__c);
        Test.stopTest();
    }

}