@isTest
public with sharing class packageSelectPage_Unittest {
  Static TestMethod void packageSelectPage_Unittest()
  {
    Test.startTest();
  	packageSelectPage p = new packageSelectPage();
  	
    User u = Util.createUser();
    String uid = userInfo.getuserId();
    Persona__c persona =  Util.createPersona();
    Persona__c persona1 =  Util.createPersona();
    Manufacturer__c manufacturer =  Util.createManufacturerABX();
    Manufacturer__c manufacturer1 =  Util.createManufacturer();
    Application__c application =  Util.createApplication();  
    Application_Variants__c applicationvar = Util.createAppVariants(application.Id);
    Product_Category__c prodcat = Util.CreateProductCategory();     
    Specifications_Meta__c specMeta = Util.CreateProductfeatureCategory(prodcat.Id);
    Product_Meta__c prodMeta = Util.CreateProductfeature(specMeta.Id);
    Product2 abprod = Util.createProduct2(manufacturer.Id, prodcat.Id);
    Product2 compprod = Util.createProduct2(manufacturer1.Id, prodcat.Id);
    Package__c pckg = Util.CreatePackage(application.Id , applicationvar.Id , persona.Id , abprod.Id , compprod.Id);
    Battlecard__c btlcrd = Util.createBattlecard(pckg.Id);
    
    Product_Value__c prdval = Util.CreateProductValue(prodMeta.Id);
    prdval.Product__c = abprod.Id;
    update prdval;
    Product_Value__c prdval1 = Util.CreateProductValue(prodMeta.Id);
    prdval1.Product__c = compprod.Id;
    update prdval1;
    
    Test.stopTest();

    packageSelectPage pckselpg1 = new packageSelectPage();
    pckselpg1.AddToFavorites();    
    pckselpg1.RemoveFromFavorites();
    
    ApexPages.currentPage().getParameters().put('pckId', pckg.Id);  
    ApexPages.currentPage().getParameters().put('showspec', 'true');  
    ApexPages.currentPage().getParameters().put('showBtlcrd', 'true');    
    packageSelectPage pckselpg = new packageSelectPage();
    pckselpg.selectedPersona = persona.Id;
    
    List<selectOption> abc = pckselpg.personaItems;
    pckselpg.AddToFavorites();
    Add_to_Favorites_Junction__c addfav = Util.createAddtoFavjuntion(pckg.Id , uiD);
    pckselpg.AddToFavorites();    
    pckselpg.RemoveFromFavorites();
    pckselpg.refreshRating();
    pckselpg.getOpportunities();
    pckselpg.hidemessage();
    pckselpg.gotoPackage();
    
    Opportunity opp = new Opportunity();
    opp.Name = 'testopp';
    opp.closedate = system.Today();
    //opp.AB_Sciex_Model__c = abprod.Id;
    //opp.Competitor_Model__c = compprod.Id;
    //opp.Application__c = applicationvar.Id;
    opp.Persona__c = persona.Id;
    //opp.Won_By__c = compprod.Id;
    //opp.Reason_Won_Lost_Dead__c = 'Performance';
    opp.StageName = 'Closed Lost';
    insert opp;
    
    ApexPages.currentPage().getParameters().put('fromOpp', 'true');  
    ApexPages.currentPage().getParameters().put('productId', abprod.Id);  
    ApexPages.currentPage().getParameters().put('relproductId', compProd.Id);
    ApexPages.currentPage().getParameters().put('applicationId', applicationvar.Id);
    ApexPages.currentPage().getParameters().put('personaId', persona.Id);
    packageSelectPage pckselpg2 = new packageSelectPage();
    /*pckselpg2.absciexProductId = abprod.Id;
    pckselpg2.relatedCatProductId = compprod.Id;
    pckselpg2.selectedPersona = persona.Id;
    pckselpg2.selectedApplicationId = applicationvar.Id;*/
    pckselpg2.fromOpportunity();
    pckselpg2.gotoPackage();
    pckselpg2.getLostReport(); 
    pckselpg2.getWinLostReport();
    
  }
  
}