/**
** This class handles insert/update of Campaign and manages instances of Strategy_ROI__c records
** For each of the 6 verticals, if a strategy is entered then a Strategy_ROI__c record is created/updated
** for each
**/
public class CampaignStrategyTriggerHandler {
    public static final String PHARMA_CRO = 'Pharma/CRO';
    public static final String ACADEMIA_OMICS = 'Academia/Omics';
    public static final String FOOD_BEVERAGE = 'Food/Beverage';
    public static final String ENVIRONMENTAL_INDUSTRIAL = 'Environmental/Industrial';
    public static final String CLINICAL = 'Clinical';
    public static final String FORENSIC = 'Forensic';

    public static void onUpdate(Campaign[] campaigns, Map<Id,Campaign> oldMap) {
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign c : campaigns){
            campaignIds.add(c.Id);
        }
        
        Map<String,Strategy_ROI__c> sroiMap = new Map<String,Strategy_ROI__c>();
        Strategy_ROI__c[] existingStrategyROIs = [select Id, Vertical__c, Campaign__c from Strategy_ROI__c
            where Campaign__c in :campaignIds];
        for(Strategy_ROI__c sroi : existingStrategyROIs){
            String k = sroi.Campaign__c + ':' + sroi.Vertical__c;
            sroiMap.put(k, sroi);
        }
        
        Strategy_ROI__c[] updates = new Strategy_ROI__c[]{};
        Strategy_ROI__c[] inserts = new Strategy_ROI__c[]{};
        Strategy_ROI__c[] deletes = new Strategy_ROI__c[]{};
        
        for(Campaign c : campaigns){
            Campaign oldCampaign = oldMap.get(c.Id);
            if(c.Academia_Omics_Strategy__c != oldCampaign.Academia_Omics_Strategy__c) {
                //academia strategy has changed
                String k = c.Id + ':' + ACADEMIA_OMICS;
                if(sroiMap.containsKey(k)){
                    //update it
                    Strategy_ROI__c sroi = sroiMap.get(k);
                    if(String.isBlank(c.Academia_Omics_Strategy__c)){
                        deletes.add(sroi);
                    }else{
                        sroi.Strategy__c = c.Academia_Omics_Strategy__c;
                        updates.add(sroi);
                    }
                }else{
                    //create it
                    Strategy_ROI__c sroi = new Strategy_ROI__c(
                        Name = ACADEMIA_OMICS,
                        Campaign__c = c.Id, 
                        Vertical__c = ACADEMIA_OMICS, 
                        Strategy__c = c.Academia_Omics_Strategy__c
                    );
                    inserts.add(sroi);
                }
            }
            if(c.Clinical_Strategy__c != oldCampaign.Clinical_Strategy__c) {
                //clinical strategy has changed
                String k = c.Id + ':' + CLINICAL;
                if(sroiMap.containsKey(k)){
                    //update it
                    Strategy_ROI__c sroi = sroiMap.get(k);
                    if(String.isBlank(c.Clinical_Strategy__c)){
                        deletes.add(sroi);
                    }else{
                        sroi.Strategy__c = c.Clinical_Strategy__c;
                        updates.add(sroi);
                    }
                }else{
                    //create it
                    Strategy_ROI__c sroi = new Strategy_ROI__c(
                        Name = CLINICAL,
                        Campaign__c = c.Id, 
                        Vertical__c = CLINICAL, 
                        Strategy__c = c.Clinical_Strategy__c
                    );
                    inserts.add(sroi);
                }
            }
            if(c.Environmental_Industrial_Strategy__c != oldCampaign.Environmental_Industrial_Strategy__c) {
                //environmental/industrial strategy has changed
                String k = c.Id + ':' + ENVIRONMENTAL_INDUSTRIAL;
                if(sroiMap.containsKey(k)){
                    //update it
                    Strategy_ROI__c sroi = sroiMap.get(k);
                    if(String.isBlank(c.Environmental_Industrial_Strategy__c)){
                        deletes.add(sroi);
                    }else{
                        sroi.Strategy__c = c.Environmental_Industrial_Strategy__c;
                        updates.add(sroi);
                    }
                }else{
                    //create it
                    Strategy_ROI__c sroi = new Strategy_ROI__c(
                        Name = ENVIRONMENTAL_INDUSTRIAL,
                        Campaign__c = c.Id, 
                        Vertical__c = ENVIRONMENTAL_INDUSTRIAL, 
                        Strategy__c = c.Environmental_Industrial_Strategy__c
                    );
                    inserts.add(sroi);
                }
            }
            if(c.Food_Beverage_Strategy__c != oldCampaign.Food_Beverage_Strategy__c) {
                //food/beverage strategy has changed
                String k = c.Id + ':' + FOOD_BEVERAGE;
                if(sroiMap.containsKey(k)){
                    //update it
                    Strategy_ROI__c sroi = sroiMap.get(k);
                    if(String.isBlank(c.Food_Beverage_Strategy__c)){
                        deletes.add(sroi);
                    }else{
                        sroi.Strategy__c = c.Food_Beverage_Strategy__c;
                        updates.add(sroi);
                    }
                }else{
                    //create it
                    Strategy_ROI__c sroi = new Strategy_ROI__c(
                        Name = FOOD_BEVERAGE,
                        Campaign__c = c.Id, 
                        Vertical__c = FOOD_BEVERAGE, 
                        Strategy__c = c.Food_Beverage_Strategy__c
                    );
                    inserts.add(sroi);
                }
            }
            if(c.Forensic_Strategy__c != oldCampaign.Forensic_Strategy__c) {
                //forensic strategy has changed
                String k = c.Id + ':' + FORENSIC;
                if(sroiMap.containsKey(k)){
                    //update it
                    Strategy_ROI__c sroi = sroiMap.get(k);
                    if(String.isBlank(c.Forensic_Strategy__c)){
                        deletes.add(sroi);
                    }else{
                        sroi.Strategy__c = c.Forensic_Strategy__c;
                        updates.add(sroi);
                    }
                }else{
                    //create it
                    Strategy_ROI__c sroi = new Strategy_ROI__c(
                        Name = FORENSIC,
                        Campaign__c = c.Id, 
                        Vertical__c = FORENSIC, 
                        Strategy__c = c.Forensic_Strategy__c
                    );
                    inserts.add(sroi);
                }
            }
            if(c.Pharma_CRO_Strategy__c != oldCampaign.Pharma_CRO_Strategy__c) {
                //pharma/cro strategy has changed
                String k = c.Id + ':' + PHARMA_CRO;
                if(sroiMap.containsKey(k)){
                    //update it
                    Strategy_ROI__c sroi = sroiMap.get(k);
                    if(String.isBlank(c.Pharma_CRO_Strategy__c)){
                        deletes.add(sroi);                    
                    }else{
                        sroi.Strategy__c = c.Pharma_CRO_Strategy__c;
                        updates.add(sroi);
                    }
                }else{
                    //create it
                    Strategy_ROI__c sroi = new Strategy_ROI__c(
                        Name = PHARMA_CRO,
                        Campaign__c = c.Id, 
                        Vertical__c = PHARMA_CRO, 
                        Strategy__c = c.Pharma_CRO_Strategy__c
                    );
                    inserts.add(sroi);
                }
            }
        }
        
        if(deletes.size() > 0) {
            delete deletes;
        }
        
        if(updates.size() > 0){
        	update updates;
        }
        
        if(inserts.size() > 0){
            insert inserts;
            associateOppsAndLeads(inserts);
        }
    }
    
    private static void associateOppsAndLeads(Strategy_ROI__c[] inserts) {
        //associate opps to the new Strategy_ROI__c(s) created
        //the Opportunity trigger will force a recalc on the Strategy_ROI__c records
        Map<String, Strategy_ROI__c> newStrategyROIs = new Map<String, Strategy_ROI__c>(); 
        Set<Id> insertCampaignIds = new Set<Id>();
        for(Strategy_ROI__c sroi : inserts) {
        	insertCampaignIds.add(sroi.Campaign__c);
        	String k = sroi.Campaign__c + ':' + sroi.Vertical__c; 
        	newStrategyROIs.put(k, sroi);
        }
        
        Opportunity[] oppUpdates = new Opportunity[]{}; 
        Opportunity[] opps = [select Id, CampaignId, Market_Vertical__c from Opportunity where CampaignId in :insertCampaignIds];
        for(Opportunity opp : opps) {
        	String k = opp.CampaignId + ':' + opp.Market_Vertical__c;
        	if(newStrategyROIs.containsKey(k)) {
        		opp.Strategy_ROI__c = newStrategyROIs.get(k).Id;
        		oppUpdates.add(opp);
        	}
        }
        if(oppUpdates.size() > 0) {
        	update oppUpdates;
        }
        
        //associate leads to the new Strategy_ROI__c(s) created
        //the Lead trigger will force a recalc on the Strategy_ROI__c records
        Lead[] leadUpdates = new Lead[]{};
        CampaignMember[] campaignMembers = [select Id, CampaignId, LeadId, Lead.Market_Segment__c from CampaignMember where CampaignId in :insertCampaignIds and Lead.IsConverted != TRUE];
        for(CampaignMember campaignMember : campaignMembers) {
        	String k = campaignMember.CampaignId + ':' + campaignMember.Lead.Market_Segment__c;
        	if(newStrategyROIs.containsKey(k)) {
        		Lead leadUpdate = new Lead(Id = campaignMember.LeadId, Strategy_ROI__c = newStrategyROIs.get(k).Id);
        		leadUpdates.add(leadUpdate);
        	}
        }
        if(leadUpdates.size() > 0) {
        	update leadUpdates;
        }
    }
    
    public static void onInsert(Campaign[] campaigns){
        Strategy_ROI__c[] strategyROIs = new Strategy_ROI__c[]{};
        
        for(Campaign c : campaigns){
            if(!String.isBlank(c.Academia_Omics_Strategy__c)) {
                Strategy_ROI__c sroi = new Strategy_ROI__c(
                    Name = ACADEMIA_OMICS,
                    Campaign__c = c.Id, 
                    Vertical__c = ACADEMIA_OMICS, 
                    Strategy__c = c.Academia_Omics_Strategy__c
                );
                strategyROIs.add(sroi);
            }
            if(!String.isBlank(c.Clinical_Strategy__c)) {
                Strategy_ROI__c sroi = new Strategy_ROI__c(
                    Name = CLINICAL,
                    Campaign__c = c.Id, 
                    Vertical__c = CLINICAL, 
                    Strategy__c = c.Clinical_Strategy__c
                );
                strategyROIs.add(sroi);
            }
            if(!String.isBlank(c.Environmental_Industrial_Strategy__c)) {
                Strategy_ROI__c sroi = new Strategy_ROI__c(
                    Name = ENVIRONMENTAL_INDUSTRIAL,
                    Campaign__c = c.Id, 
                    Vertical__c = ENVIRONMENTAL_INDUSTRIAL, 
                    Strategy__c = c.Environmental_Industrial_Strategy__c
                );
                strategyROIs.add(sroi);
            }
            if(!String.isBlank(c.Food_Beverage_Strategy__c)) {
                Strategy_ROI__c sroi = new Strategy_ROI__c(
                    Name = FOOD_BEVERAGE,
                    Campaign__c = c.Id, 
                    Vertical__c = FOOD_BEVERAGE, 
                    Strategy__c = c.Food_Beverage_Strategy__c
                );
                strategyROIs.add(sroi);
            }
            if(!String.isBlank(c.Forensic_Strategy__c)) {
                Strategy_ROI__c sroi = new Strategy_ROI__c(
                    Name = FORENSIC,
                    Campaign__c = c.Id, 
                    Vertical__c = FORENSIC, 
                    Strategy__c = c.Forensic_Strategy__c
                );
                strategyROIs.add(sroi);
            }
            if(!String.isBlank(c.Pharma_CRO_Strategy__c)) {
                Strategy_ROI__c sroi = new Strategy_ROI__c(
                    Name = PHARMA_CRO,
                    Campaign__c = c.Id, 
                    Vertical__c = PHARMA_CRO, 
                    Strategy__c = c.Pharma_CRO_Strategy__c
                );
                strategyROIs.add(sroi);
            }
        }
        
        if(strategyROIs.size() > 0){
            insert strategyROIs;
            associateOppsAndLeads(strategyROIs);
        }
    }
}