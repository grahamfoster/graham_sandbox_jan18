/*
 *	C2O_Merge
 * 
 *  In order to respect SFDC governor limits, modules process records in 'chunks'. 
 * While the Create module automatically merges Contracts where needed, it is possible for two Opportunities that should be merged to 
 * appear in separate 'chunks'.  This class looks for these instances and merges the records where needed.
 * 
 * 	Created by Brett Moore 2016-08-11 based 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class C2O_Merge {
    
    public List<OpportunityLineItem> mergeLines;
    public List<Opportunity> delOpps; 
   
	public C2O_Merge() {  

system.debug('******************** - START');        
        Map<Id,Opportunity> OppMap = new Map<Id,Opportunity>([SELECT id, LastModifiedBy.id, CreatedBy.id, Contract_Number__c, Previous_Contract__c FROM Opportunity WHERE c2o_AutoGeneration_Status__c = 'Created' AND CreatedDate = LAST_N_DAYS:10 ORDER BY Contract_Number__c]);
        Map<String,ID> existingOpps = new Map<String,ID>();			// Parent Contract#, Existing Opportunity ID		
        Map<ID,ID> dupOpps = new Map<ID,ID>();						// ID of Oportunity entries removed, ID of the Opportunity to be used instead
		this.mergeLines = new List<OpportunityLineItem>();
        this.delOpps = new List<Opportunity>();

        
		Map<Id,SVMXC__Service_Contract__c> contractMap = new Map<Id,SVMXC__Service_Contract__c>([SELECT id, c2o_Renewal_Opportunity__c FROM SVMXC__Service_Contract__c WHERE c2o_Renewal_Opportunity__c IN :OppMap.keySet()]);                
        
        for(Opportunity o :OppMap.values()){
            if(o.LastModifiedById == o.CreatedById){
            	if(!existingOpps.containsKey(o.Contract_Number__c) ){
               		existingOpps.put(o.Contract_Number__c, o.Id);
            	} else {
system.debug('******************** - dupe');            
               		dupOpps.put(o.id,existingOpps.get(o.Contract_Number__c));

                    delOpps.add(o);
            	}
            }
        }
        
        for(SVMXC__Service_Contract__c c :contractMap.values()){
        	if(dupOpps.containsKey(c.c2o_Renewal_Opportunity__c) ){
				c.c2o_Renewal_Opportunity__c = dupOpps.get(c.c2o_Renewal_Opportunity__c);
            }
        }
        
        
        Map<Id,OpportunityLineItem> OppLineMap = new Map<Id,OpportunityLineItem>([SELECT id, OpportunityId, ProductCode, Quantity, Type__c, Serial_Number__c, Model__c, PricebookEntryId, UnitPrice   From OpportunityLineItem WHERE OpportunityId In :dupOpps.keySet() ]);
        
        
        for(OpportunityLineItem ol :OppLineMap.values()){
            if(dupOpps.containsKey(ol.OpportunityId)){
                OpportunityLineItem tempOpp = new OpportunityLineItem(OpportunityId = dupOpps.get(ol.OpportunityId),Quantity = ol.Quantity, Type__c = ol.Type__c, Serial_Number__c = ol.Serial_Number__c, Model__c = ol.Model__c, PricebookEntryId = ol.PricebookEntryId, UnitPrice = ol.UnitPrice);    
                mergeLines.add(tempOpp);
            }
        }
system.debug('******************** - START DML');		
 		        insert mergeLines;
        		update contractMap.values();
                delete delOpps;

system.debug('******************** - END');
    }
}

/*
 * Code for invoking via Annonymus execution
 * 
 * C2O_Merge mo = new C2O_Merge();
 *
 */