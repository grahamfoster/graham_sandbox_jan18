/*********************************************************************
Name  : testQuoteAttachments
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuoteAttachments  

*********************************************************************/

public class testQuoteAttachments{

  public static testMethod void checkQuoteAttachments() {

    QuoteAttachments.ArrayOffileAttachments  cls1 = new QuoteAttachments.ArrayOffileAttachments();
    QuoteAttachments.individualFileOutputs  cls2 = new QuoteAttachments.individualFileOutputs();
    QuoteAttachments.receiveAttachments  cls3 = new QuoteAttachments.receiveAttachments();
    QuoteAttachments.receiveAttachmentsResponse  cls4 = new QuoteAttachments.receiveAttachmentsResponse();
    QuoteAttachments.attachmentOutputs  cls5 = new QuoteAttachments.attachmentOutputs();
    QuoteAttachments.fileAttachments  cls6 = new QuoteAttachments.fileAttachments();
    QuoteAttachments.ABI_SalesQuotation_process_receiveAttachmentsWsd_Port  cls7 = new QuoteAttachments.ABI_SalesQuotation_process_receiveAttachmentsWsd_Port();
    QuoteAttachments.attachmentInputs attachmentInputs = new QuoteAttachments.attachmentInputs();
    QuoteAttachments.attachmentOutputs qa = cls7.receiveAttachments(attachmentInputs );
    QuoteAttachments.ArrayOfindividualFileOutputs  cls8 = new QuoteAttachments.ArrayOfindividualFileOutputs();
    QuoteAttachments.attachmentInputs  cls9 = new QuoteAttachments.attachmentInputs();
  }  
}