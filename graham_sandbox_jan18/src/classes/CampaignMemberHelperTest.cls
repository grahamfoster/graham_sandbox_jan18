/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CampaignMemberHelperTest {

	public static testMethod void testThis()
	{
		Campaign c = new Campaign();
		c.Name = 'TEST Campaign Astadia 1';
		//c.Actual_Clinical_Forensic_A_C__c = 0;
		//c.Actual_Triple_TOF_A_C__c = 0;
	 	insert c;
	 	
		List<CampaignMember> cmList = new List<CampaignMember>();
		List<Lead> leadList = new List<Lead>();
		
		for (Integer i = 0; i < 150; i++)
		{
			Lead l = new Lead();
			l.LastName = 'Hello Lead ' + i;
			l.Company = 'Astadia';
            l.Country = 'United States';
			
			leadList.add(l);
		}
		
		insert leadList;
		
		for (Integer i = 0; i < 150; i++)
		{
			CampaignMember cm = new CampaignMember();
			cm.Core_Platform__c = 'TRIPLE TOF';
			cm.Market_Segment__c = 'Clinical/Forensic';
			cm.Campaign_Intent_To_Purchase__c = 'A - Hot (0-6 months purchase)';
			cm.CampaignId = c.Id;
			cm.LeadId = leadList.get(i).Id;
			cmList.add(cm);
		}
		
		insert cmList;
		
		c = [Select Id, Actual_Clinical_Forensic_A_C__c, Actual_Triple_TOF_A_C__c from Campaign where id = :c.Id limit 1];
		System.debug(Logginglevel.INFO, '####################### c.Actual_Clinical_Forensic_A_C__c = ' + c.Actual_Clinical_Forensic_A_C__c);
		System.debug(Logginglevel.INFO, '####################### c.Actual_Triple_TOF_A_C__c = ' + c.Actual_Triple_TOF_A_C__c);
		
		System.assert(c.Actual_Clinical_Forensic_A_C__c == 150);
		System.assert(c.Actual_Triple_TOF_A_C__c == 150);
		
		List<CampaignMember> cmDeleteList = new List<CampaignMember>();
		
		for (Integer i = 0; i < 100; i++)
		{
			cmDeleteList.add(cmList.get(i));
		}
		
		delete cmDeleteList;
		
		c = [Select Id, Actual_Clinical_Forensic_A_C__c, Actual_Triple_TOF_A_C__c from Campaign where id = :c.Id limit 1];
		System.debug(Logginglevel.INFO, '#######DELETE################ c.Actual_Clinical_Forensic_A_C__c = ' + c.Actual_Clinical_Forensic_A_C__c);
		System.debug(Logginglevel.INFO, '#######DELETE################ c.Actual_Triple_TOF_A_C__c = ' + c.Actual_Triple_TOF_A_C__c);
		
		System.assert(c.Actual_Clinical_Forensic_A_C__c == 50);
		System.assert(c.Actual_Triple_TOF_A_C__c == 50);
		
		
	}
}