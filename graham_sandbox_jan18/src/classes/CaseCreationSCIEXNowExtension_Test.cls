/* Test Class for CaseCreationSCIEXNowExtension
*
* Graham Foster   January 2017
*/

@isTest
public class CaseCreationSCIEXNowExtension_Test {

    @testSetup
    static void setup()
    {
        Account a = new Account(Name = 'Tester Account', BillingCountry = 'Canada');
		insert a;
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', 
		AccountId = a.Id, Email = 'tc@example.com');
		insert c;
		Asset nAsset = new Asset(Name = 'TestAsset', SerialNumber = '1234', 
                                 AccountId = a.Id, Primary_Contact__c = c.Id, 
                                 ContactId = c.Id);
        Asset nAsset2 = new Asset(Name = 'TestAsset2', SerialNumber = '12342', 
                                 AccountId = a.Id, Primary_Contact__c = c.Id, 
                                 ContactId = c.Id);
        insert new List<Asset>{nAsset, nAsset2};
        
        Asset_MultipleContacts__c nLink = new Asset_MultipleContacts__c(Asset__c = nAsset.Id, Contact__c = c.Id);
        insert nLink;
        
        Date dateToday = Date.today();
		String sMonth = String.valueof(dateToday.month());
		String sDay = String.valueof(dateToday.day());
		if(sMonth.length()==1){
  			sMonth = '0' + sMonth;
		}
		if(sDay.length()==1){
  		sDay = '0' + sDay;
		}
		String sToday = 'Call ' + String.valueof(dateToday.year()) + '-' + sMonth + '-' + sDay + '11111111';
        Task t = new Task(Subject = sToday, cnx__CTIInfo__c = 'ANI: 447818507838, DNIS: 1925237611, DialedNumber: 1925237611');
        insert t;
    }
    
    @isTest
    static void TestWithExistingAsset()
    {   
        Case newCase = new Case();
        Contact c = [SELECT Id FROM Contact WHERE Email = 'tc@example.com' LIMIT 1];
        Asset nAsset = [SELECT Id FROM Asset WHERE SerialNumber = '1234' LIMIT 1];
            
        ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
        apexpages.currentpage().getparameters().put('cid' , c.Id);
        CaseCreationSCIEXNowExtension ccExt = new CaseCreationSCIEXNowExtension(sc);
        
        ccExt.selectedAsset = nAsset.Id;
        ccExt.newCase.Type = 'SAS';
        ccExt.newCase.Case_Type__c = 'LCMS';
        ccExt.newCase.Subject = 'TestSubject';
        ccExt.newCase.Description = 'TestDescription';
        
        String contactNameTest = ccExt.getContactName();
        system.assertEquals('Test Contact', contactNameTest);

        List<SelectOption> optionsTest = ccExt.getItems();
        system.assertEquals(2, optionsTest.size());
        List<SelectOption> optionsTest2 = ccExt.getPhoneFields();
        system.assertEquals(3, optionsTest2.size());
        ccExt.phoneOverwrite = 'Phone';
        system.assertEquals('tc@example.com', ccExt.cont.Email);
        system.assertEquals('Tester Account', ccExt.cont.Account.Name);
        ccExt.save();
        
        Case ca = [SELECT Id, AssetId, ContactId FROM Case WHERE Subject = 'TestSubject' LIMIT 1];
        system.assertEquals(nAsset.Id, ca.AssetId);
        system.assertEquals(c.Id, ca.ContactId);
        Task t = [SELECT Id, WhatId FROM Task WHERE cnx__CTIInfo__c = 'ANI: 447818507838, DNIS: 1925237611, DialedNumber: 1925237611' LIMIT 1];
        system.assertEquals(ca.Id, t.WhatId);
        Contact c1 = [SELECT Phone FROM Contact WHERE Id = :c.Id LIMIT 1];
        system.assertEquals('+447818507838', c1.Phone);
    }
    
    @isTest
    static void TestWithNewAsset()
    {
        Case newCase = new Case();
        Contact c = [SELECT Id FROM Contact WHERE Email = 'tc@example.com' LIMIT 1];
        Asset nAsset = [SELECT Id FROM Asset WHERE SerialNumber = '12342' LIMIT 1];
            
        ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
        apexpages.currentpage().getparameters().put('cid' , c.Id);
        CaseCreationSCIEXNowExtension ccExt = new CaseCreationSCIEXNowExtension(sc);
        
        ccExt.newCase.AssetId = nAsset.Id;
        ccExt.newCase.Type = 'SAS';
        ccExt.newCase.Case_Type__c = 'LCMS';
        ccExt.newCase.Subject = 'TestSubject';
        ccExt.newCase.Description = 'TestDescription';
        ccExt.phoneOverwrite = 'No';
		ccExt.save();        
        Case ca = [SELECT AssetId, ContactId FROM Case WHERE Subject = 'TestSubject' LIMIT 1];
        system.assertEquals(nAsset.Id, ca.AssetId);
        system.assertEquals(c.Id, ca.ContactId);
        
        List<Asset_MultipleContacts__c> nLink = [SELECT Id FROM Asset_MultipleContacts__c WHERE Asset__c = :nAsset.Id AND Contact__c = :c.Id];
        
        system.assertEquals(1, nLink.size());
    }
    
    @isTest
    static void TestWithNoAsset()
    {
        Case newCase = new Case();
        Contact c = [SELECT Id FROM Contact WHERE Email = 'tc@example.com' LIMIT 1];
        Asset nAsset = [SELECT Id FROM Asset WHERE SerialNumber = '12342' LIMIT 1];
            
        ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
        apexpages.currentpage().getparameters().put('cid' , c.Id);
        CaseCreationSCIEXNowExtension ccExt = new CaseCreationSCIEXNowExtension(sc);
        
        ccExt.newCase.Type = 'SAS';
        ccExt.newCase.Case_Type__c = 'LCMS';
        ccExt.newCase.Subject = 'TestSubject';
        ccExt.newCase.Description = 'TestDescription';
        ccExt.phoneOverwrite = 'No';
		ccExt.save();        
        Case ca = [SELECT AssetId, ContactId FROM Case WHERE Subject = 'TestSubject' LIMIT 1];
        system.assertEquals(null, ca.AssetId);
        system.assertEquals(c.Id, ca.ContactId);
        
    }
    
    @isTest
    static void testExistingAssetMultipleContact()
    {
        Case newCase = new Case();
        Contact c = [SELECT Id FROM Contact WHERE Email = 'tc@example.com' LIMIT 1];
        Asset nAsset = [SELECT Id FROM Asset WHERE SerialNumber = '1234' LIMIT 1];
            
        ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
        apexpages.currentpage().getparameters().put('cid' , c.Id);
        CaseCreationSCIEXNowExtension ccExt = new CaseCreationSCIEXNowExtension(sc);
        
        ccExt.newCase.AssetId = nAsset.Id;
        ccExt.newCase.Type = 'SAS';
        ccExt.newCase.Case_Type__c = 'LCMS';
        ccExt.newCase.Subject = 'TestSubject';
        ccExt.newCase.Description = 'TestDescription';
        String contactNameTest = ccExt.getContactName();
        List<SelectOption> optionsTest = ccExt.getItems();
        ccExt.phoneOverwrite = 'No';
		ccExt.save();        
        Case ca = [SELECT AssetId, ContactId FROM Case WHERE Subject = 'TestSubject' LIMIT 1];
        system.assertEquals(nAsset.Id, ca.AssetId);
        system.assertEquals(c.Id, ca.ContactId);
        
        List<Asset_MultipleContacts__c> nLink = [SELECT Id FROM Asset_MultipleContacts__c WHERE Asset__c = :nAsset.Id AND Contact__c = :c.Id];
        
        system.assertEquals(1, nLink.size());
    }
    
    @isTest
    static void CancelTest()
    {
        Case newCase = new Case();
        Contact c = [SELECT Id FROM Contact WHERE Email = 'tc@example.com' LIMIT 1];
            
        ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
        apexpages.currentpage().getparameters().put('cid' , c.Id);
        CaseCreationSCIEXNowExtension ccExt = new CaseCreationSCIEXNowExtension(sc);
        System.assertEquals('/home/home.jsp', ccExt.cancel().getUrl() );
    }
    
    
    
}