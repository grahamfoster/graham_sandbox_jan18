/**
** @author Reid Beckett, Cloudware Connections
** @created Oct 7/2014
**
** Test coverage for Product2 SetProductFieldGroupings trigger
** 
**/
@isTest
public class SetProductFieldGroupingsTests {
    public static testMethod void test1() {
        setUp();
        Product2 newProduct = new Product2(Name = 'Test Product', Top200__c = '06A');
        insert newProduct;

        newProduct = [select Id, Name, Brand__c, Family, Product_Family_Range_Grouping__c from Product2 where Id = :newProduct.Id];
        system.assertEquals('AB SCIEX', newProduct.Brand__c);
        system.assertEquals('Consumables', newProduct.Family);
        system.assertEquals('GroupA', newProduct.Product_Family_Range_Grouping__c);
    }

    private static void setUp() {
        Product_Field_Groupings__c pfg = new Product_Field_Groupings__c(Name = '06A', Product_Brand__c = 'AB SCIEX', Product_Family__c = 'Consumables', Product_Family_Range_Grouping__c = 'GroupA');
        insert pfg;     
    }
}