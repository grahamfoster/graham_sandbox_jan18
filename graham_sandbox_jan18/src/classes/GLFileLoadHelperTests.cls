/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Feb 28/2014
 **
 ** Class to process Account_Opportunity_Load_Record__c records
**/
@isTest(SeeAllData=true)
public class GLFileLoadHelperTests {
    private static Pricebook2 pricebook;

    public static testMethod void test1_insert1_success(){
        createTestPricebook();

        String acctSoldTo = '42822xxxxx';
        String contractNumber = '35306459xxx';
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = acctSoldTo, Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(),
                Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12345', Contract_Expiry_Date__c = Date.today().addYears(1))
        };
        
        insert aolRecords;
        
        aolRecords = [select Id, Processing_Status__c, Processing_Message__c from Account_Opportunity_Load_Record__c where id = :aolRecords[0].Id];
        system.assertEquals(1, aolRecords.size());
        system.assertEquals('Processed', aolRecords[0].Processing_Status__c); 
        system.debug(aolRecords[0].Processing_Status__c);
        system.debug(aolRecords[0].Processing_Message__c);
        
        Account account = [select Id, Name, (select Amount from Opportunities) from Account where Account_Sold_To__c = :acctSoldTo];
        system.assertEquals('GENENTECH INC (AB SCIEX)', account.Name);
        system.assertEquals(1, account.Opportunities.size());
        system.assertEquals(10000, account.Opportunities[0].Amount);
    }

    public static testMethod void test2_insert2_success(){
        createTestPricebook();
        
        String acctSoldTo = '42822xxxxx';
        String contractNumber = '35306459xxx';

        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = acctSoldTo, Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12345', Contract_Expiry_Date__c = Date.today().addYears(1),
                Type__c = 'WCONV'),
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = acctSoldTo, Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12346', Contract_Expiry_Date__c = Date.today().addYears(1),
                Type__c = 'CRNWL')
        };
        
        insert aolRecords;

        Set<Id> aolrecids = new Set<Id>();
        for(Account_Opportunity_Load_Record__c aolr : aolRecords) {
            aolrecids.add(aolr.Id);
        }
        
        aolRecords = [select Id, Processing_Status__c, Processing_Message__c from Account_Opportunity_Load_Record__c where id in :aolrecids];
        system.assertEquals(2, aolRecords.size());
        system.assertEquals('Processed', aolRecords[0].Processing_Status__c); 
        
        Account account = [select Id, Name, (select Amount from Opportunities) from Account where Account_Sold_To__c = :acctSoldTo];
        system.assertEquals('GENENTECH INC (AB SCIEX)', account.Name);
        system.assertEquals(1, account.Opportunities.size());
        system.assertEquals(20000, account.Opportunities[0].Amount);
    }

    public static testMethod void test2_insert2_diffExpiries_success(){
        createTestPricebook();
        
        String acctSoldTo = '42822xxxxx';
        String contractNumber = '35306459xxx';

        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = acctSoldTo, Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12345', Contract_Expiry_Date__c = Date.today().addYears(1)),
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = acctSoldTo, Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12346', Contract_Expiry_Date__c = Date.today().addYears(1).addDays(-1))
        };
        
        insert aolRecords;

        Set<Id> aolrecids = new Set<Id>();
        for(Account_Opportunity_Load_Record__c aolr : aolRecords) {
            aolrecids.add(aolr.Id);
        }
        
        aolRecords = [select Id, Processing_Status__c, Processing_Message__c from Account_Opportunity_Load_Record__c where id in :aolrecids];
        system.assertEquals(2, aolRecords.size());
        system.assertEquals('Processed', aolRecords[0].Processing_Status__c); 
        
        Account account = [select Id, Name, (select Amount from Opportunities) from Account where Account_Sold_To__c = :acctSoldTo];
        system.assertEquals('GENENTECH INC (AB SCIEX)', account.Name);
        system.assertEquals(2, account.Opportunities.size());
        system.assertEquals(10000, account.Opportunities[0].Amount);
        system.assertEquals(10000, account.Opportunities[1].Amount);
    }

    public static testMethod void test3_update_createline_success(){
        Test.startTest();
        createTestPricebook();
        
        String acctSoldTo = '42822xxxxx';
        String contractNumber = '35306459xxx';

        Date contractExpiryDate = Date.today().addYears(1);

        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = acctSoldTo, BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = contractNumber, Contract_Expiry_Date__c = contractExpiryDate, Explicit_Needs__c = '?', Implications__c = '?', CurrencyISOCode='USD');
        insert opp;
        Test.stopTest();
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Contract_Expiry_Date__c = contractExpiryDate, 
                Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = acctSoldTo, Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12345')
        };
        
        insert aolRecords;

        Set<Id> aolrecids = new Set<Id>();
        for(Account_Opportunity_Load_Record__c aolr : aolRecords) {
            aolrecids.add(aolr.Id);
        }
        
        aolRecords = [select Id, Processing_Status__c, Processing_Message__c from Account_Opportunity_Load_Record__c where id in :aolrecids];
        system.assertEquals(1, aolRecords.size());
        system.assertEquals('Processed', aolRecords[0].Processing_Status__c); 
        system.debug(aolRecords[0].Processing_Status__c);
        system.debug(aolRecords[0].Processing_Message__c);
        
        Account account = [select Id, Name, (select Amount from Opportunities) from Account where Account_Sold_To__c = :acctSoldTo];
        system.assertEquals('GENENTECH INC (AB SCIEX)', account.Name);
        system.assertEquals(1, account.Opportunities.size());
        system.assertEquals(10000, account.Opportunities[0].Amount);
    }

    public static testMethod void test4_update_skipline_success(){
        createTestPricebook();
        
        String acctSoldTo = '42822xxxxx';
        String contractNumber = '35306459xxx';

        Date contractExpiryDate = Date.today().addYears(1);

        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = acctSoldTo, BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = contractNumber, Contract_Expiry_Date__c = contractExpiryDate, Explicit_Needs__c = '?', Implications__c = '?', CurrencyISOCode='USD');
        insert opp;
        
        PricebookEntry pbe = [select Id from PricebookEntry where Product2.ProductCode = 'test-1' and Pricebook2.IsStandard = false limit 1];
        OpportunityLineItem lineItem = new OpportunityLineItem(OpportunityId = opp.Id, PricebookEntryId = pbe.Id, UnitPrice = 10000, Quantity = 1, Serial_Number__c = '12345');
        insert lineItem;
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = acctSoldTo, Contract_Number__c = contractNumber, Contract_Expiry_Date__c = contractExpiryDate, 
                Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE', Serial_Number__c = '12345')
        };
        
        insert aolRecords;

        Set<Id> aolrecids = new Set<Id>();
        for(Account_Opportunity_Load_Record__c aolr : aolRecords) {
            aolrecids.add(aolr.Id);
        }
        
        aolRecords = [select Id, Processing_Status__c, Processing_Message__c from Account_Opportunity_Load_Record__c where id in :aolrecids];
        system.assertEquals(1, aolRecords.size());
        system.assertEquals('Processed', aolRecords[0].Processing_Status__c); 
        system.debug(aolRecords[0].Processing_Status__c);
        system.debug(aolRecords[0].Processing_Message__c);
        
        Account account = [select Id, Name, (select Amount from Opportunities) from Account where Account_Sold_To__c = :acctSoldTo];
        system.assertEquals('GENENTECH INC (AB SCIEX)', account.Name);
        system.assertEquals(1, account.Opportunities.size());
        system.assertEquals(10000, account.Opportunities[0].Amount);
    }

    public static testMethod void test_error_1_missing_soldto(){
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Contract_Number__c = '35306459', Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', Contract_Expiry_Date__c = Date.today().addYears(1), CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;

        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_1b_missing_account_name(){
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;

        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_2_missing_contract_number(){
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_2_missing_contract_expiry(){
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }


    public static testMethod void test_error_3_product_not_found(){
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-6', CurrencyIsoCode = 'USD', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_4_pricebookentry_not_found(){
        createTestPricebook();
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'EUR', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today())
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_5_no_price(){
        createTestPricebook();
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_6_no_model(){
        createTestPricebook();
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Line_Item_End__c = Date.today(), Value_Local_Currency__c = 10000)
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_error_7_no_serialnumber(){
        createTestPricebook();
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Line_Item_End__c = Date.today(), Value_Local_Currency__c = 10000, Model_Number__c = 'VOYAGER-DE')
        };
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_update_error_1_no_product(){
        createTestPricebook();
        
        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = '42822', BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'USD', StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-6', CurrencyIsoCode = 'EUR', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }
    
    public static testMethod void test_update_error_2_no_pricebookentry(){
        createTestPricebook();
        
        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = '42822', BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'USD', StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'EUR', Value_Local_Currency__c = 10000, Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_update_error_3_no_price(){
        createTestPricebook();
        
        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = '42822', BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'USD', StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Line_Item_End__c = Date.today(), Model_Number__c = 'VOYAGER-DE')
        };
        
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_update_error_4_no_model(){
        createTestPricebook();
        
        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = '42822', BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'USD', StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Line_Item_End__c = Date.today(), Value_Local_Currency__c = 10000)
        };
        
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    public static testMethod void test_update_error_5_no_serialnumber(){
        createTestPricebook();
        
        Account acct = new Account(Name = 'GENENTECH INC (AB SCIEX)', Account_Sold_To__c = '42822', BillingCountry = 'United States');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'USD', StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;
        
        List<Account_Opportunity_Load_Record__c> aolRecords = new List<Account_Opportunity_Load_Record__c> {
            new Account_Opportunity_Load_Record__c(Instance_Sold_To_Number__c = '42822', Contract_Number__c = '35306459', Contract_Expiry_Date__c = Date.today().addYears(1), Created__c = Date.newInstance(2013, 12, 19),
                Sold_To_Account_Name__c = 'GENENTECH INC (AB SCIEX)', Z1_Customer_Name__c = 'Tafari, Yeshiwork', Ship_To_Number__c = '42822', Country_Name__c = 'UNITED STATES',
                Contract_Name__c = 'test-1', CurrencyIsoCode = 'USD', Line_Item_End__c = Date.today(), Value_Local_Currency__c = 10000, Model_Number__c = 'VOYAGER-DE')
        };
        
        insert aolRecords;
        
        system.assertEquals('Error', [select Processing_Status__c from Account_Opportunity_Load_Record__c where Id = :aolRecords[0].Id].Processing_Status__c);
    }

    private static void createTestPricebook(){
        Pricebook2 stdPricebook = [select Id from Pricebook2 where IsStandard = true limit 1];

        pricebook = new Pricebook2(Name = 'Service Price Book', CurrencyIsoCode = 'USD', IsActive = true);
        insert pricebook; 
        
        List<Product2> testProducts = new List<Product2> {
            new Product2(Name = 'Test product 1', ProductCode = 'test-1', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 2', ProductCode = 'test-2', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 3', ProductCode = 'test-3', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 4', ProductCode = 'test-4', IsActive = true, CurrencyIsoCode = 'USD'),
            new Product2(Name = 'Test product 5', ProductCode = 'test-5', IsActive = true, CurrencyIsoCode = 'USD')
        };
        insert testProducts;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(Product2 tp : testProducts) {
            pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
        }
        insert pricebookEntries;

        List<PricebookEntry> testPricebookEntries = new List<PricebookEntry>();
        for(Product2 tp : testProducts) {
            testPricebookEntries.add(new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
        }
        insert testPricebookEntries;
    }
}