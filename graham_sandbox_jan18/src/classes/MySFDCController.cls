/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 15/2014
**
** Controller to display My SFDC dashboard
** 
**/
public class MySFDCController {
	public User runAs {get;set;}
	public User currentUser {get;set;}
	public String runAsId {get;set;}
	public List<List<GoogleGaugeChartData>> rowsOfCharts {get;set;} //current Quarter Forecast guage
	public QuotaTable quotaTable {get;set;}
	public Top5Table top5Table {get;set;}
	public Top5Table plannedLostTable {get;set;}

	public String reportTitle {get;set;}
	public List<Opportunity> reportOpps {get;set;}
	public Pager reportPager {get;set;}
	public List<Conversion_Rate__c> conversionRates {get;set;}
	public String marketVertical {get;set;}
	public String productType {get;set;}
	public String region {get;set;}
	public String country {get;set;}

	/* for service */
	public GoogleComboChartData pipelineChartData {get;set;}
	public GoogleBarChartData cumulativeChartData {get;set;}
	public GoogleComboChartData quotaVarianceChartData {get;set;}

	public ServiceSummaryTable monthSummary {get;set;}
	public ServiceSummaryTable quarterSummary {get;set;}
	public ServiceSummaryTable yearSummary {get;set;}

	public MySFDCController() {
		
	}

	public MySFDCController(ApexPages.StandardController stdController) {
		
	}

	public string baseURL {
		get {
			return URL.getSalesforceBaseUrl().toExternalForm();
		}
	}

	public PageReference init(){
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			runAsId = ApexPages.currentPage().getParameters().get('u');
		}else{
			runAsId = UserInfo.getUserId();
		}
		if(ApexPages.currentPage().getParameters().containsKey('mv')) {
			marketVertical = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('mv'), 'UTF-8');
		}else{
			marketVertical = '';
		}
		if(ApexPages.currentPage().getParameters().containsKey('pt')) {
			productType = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('pt'), 'UTF-8');
		}else{
			productType = '';
		}
		if(ApexPages.currentPage().getParameters().containsKey('rg')) {
			region = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('rg'), 'UTF-8');
		}else{
			region = '';
		}
		if(ApexPages.currentPage().getParameters().containsKey('ctry')) {
			country = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('ctry'), 'UTF-8');
		}else{
			country = '';
		}
		//if region with only 1 country, then set it
		if(String.isBlank(country) && !String.isBlank(region)) {
			List<String> countries = RegionUtil.getAllCountries(region);
			if(countries.size()==1) country = countries[0];
		} 
		runAs = MySFDCUtil.findUser(runAsId);
		currentUser = MySFDCUtil.findUser(UserInfo.getUserId());

		//load the conversion rates
		Map<String,Decimal> conversionRatesMap = MySFDCUtil.getConversionRates(runAs);
		conversionRates = MySFDCUtil.getConversionRatesList(runAs);

		Period currentQuarter = MySFDCUtil.getCurrentQuarter();
		Period nextQuarter = MySFDCUtil.getNextQuarter();

		//load quotas for the quarter
		Set<Id> userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		MySFDCQuota quotas = MySFDCUtil.getQuotas(runAsId);
		Decimal currentQuarterQuota = quotas.currentQuarterQuota;
		Decimal nextQuarterQuota = quotas.nextQuarterQuota;

		//generate the dashboards
		MySFDCFilter filter = new MySFDCFilter();
		filter.runAs = runAs;
		filter.marketVertical = marketVertical;
		filter.productType = productType;
		filter.region = region;
		filter.country = country;

		//rowsOfCharts = ABSciexDashboardFactory.getDashboardsTable(runAs, marketVertical);
		rowsOfCharts = ABSciexDashboardFactory.getDashboardsTable(filter);
		//IMySFDCDashboard cqForecastDB = ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.CURRENT_FORECAST, runAs, marketVertical, quotas, conversionRatesMap);
		IMySFDCDashboard cqForecastDB = ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.CURRENT_FORECAST, filter, quotas, conversionRatesMap);
		cqForecastDB.query();
		//IMySFDCDashboard cqUpside = ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.CURRENT_FORECAST_AND_UP, runAs, marketVertical, quotas, conversionRatesMap);
		IMySFDCDashboard cqUpside = ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.CURRENT_FORECAST_AND_UP, filter, quotas, conversionRatesMap);
		cqUpside.query();

		//TODO: get these values
		Decimal currentQtrUpsideExisting = ((MySFDCForecastAndUpDashboard)cqUpside).upsideExisting;
		Decimal currentQtrUpsideNew = ((MySFDCForecastAndUpDashboard)cqUpside).upsideNew;

		quotaTable = new QuotaTable('Q'+currentQuarter.Number+' (Current Quarter)' + MySFDCUtil.getUserSector(runAs));
		quotaTable.tableData.add(new QuotaTableRow('Quota',currentQuarterQuota/1000000.0));
		if(!MySFDCUtil.isServiceUser(runAs)) {
			quotaTable.tableData.add(new QuotaTableRow('Forecast',cqForecastDB.getActual()/1000000.0));
			quotaTable.tableData.add(new QuotaTableRow('Upside Existing',currentQtrUpsideExisting/1000000.0));
			quotaTable.tableData.add(new QuotaTableRow('Upside New',currentQtrUpsideNew/1000000.0));
		}else{
			quotaTable.tableData.add(new QuotaTableRow('Forecast',cqForecastDB.getActual()/1000000.0));
			Decimal upside = currentQtrUpsideExisting + currentQtrUpsideNew;
			quotaTable.tableData.add(new QuotaTableRow('Upside',upside/1000000.0));
		}
		//quotaTable.tableData.add(new QuotaTableRow('Total',(cqForecastDB.getActual()+cqUpside.getActual())/1000000.0));
		quotaTable.tableData.add(new QuotaTableRow('Total',(cqForecastDB.getActual()+currentQtrUpsideExisting+currentQtrUpsideNew)/1000000.0));

		List<Opportunity> top5Opps = findTop5Opps(userIds);

		top5Table = new Top5Table();
		top5Table.isServiceUser = MySFDCUtil.isServiceUser(runAs);
		for(Opportunity opp : top5Opps) {
			top5Table.tableData.add(new Top5TableRow(opp));
		}

		//Are we really losing these deals opps...
		List<Opportunity> plannedLostOpps = MySFDCUtil.isServiceUser(runAs) ? findPlannedLostOppsService(userIds) : findPlannedLostOpps(userIds);

		plannedLostTable = new Top5Table();
		plannedLostTable.title = MySFDCUtil.isServiceUser(runAs) ? 'Deals at Risk' : 'Deals We are Planning to Lose';
		for(Opportunity opp : plannedLostOpps) {
			plannedLostTable.tableData.add(new Top5TableRow(opp));
		}

		if(isServiceUser)
		{
			MySFDCServicePipelineDashboard pipelineDB = (MySFDCServicePipelineDashboard)ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.PIPELINE_CHART, filter, quotas, conversionRatesMap);
			pipelineChartData = pipelineDB.getGoogleComboChartData();
			MySFDCServiceCumulativeYTDQuotaDashboard cumulativeDB = (MySFDCServiceCumulativeYTDQuotaDashboard)ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.CUMULATIVE_QUOTA, filter, quotas, conversionRatesMap);
			cumulativeChartData = cumulativeDB.getGoogleBarChartData();
			MySFDCServiceVarianceDashboard quotaVarianceDB = (MySFDCServiceVarianceDashboard)ABSciexDashboardFactory.getDashboard(ABSciexDashboardFactory.QUOTA_VARIANCE, filter, quotas, conversionRatesMap);
			quotaVarianceChartData = quotaVarianceDB.getGoogleComboChartData();

			calculateMonthSummary(quotas, filter);
			calculateQuarterSummary(quotas, filter);
			calculateYearSummary(quotas, filter);
		}

		return null;
	}

	private void calculateMonthSummary(MySFDCQuota quotas, MySFDCFilter filter)
	{
		monthSummary = new ServiceSummaryTable('Month');	
		MySFDCSettings__c settings = !Test.isRunningTest() ? MySFDCSettings__c.getInstance() : new MySFDCSettings__c(Month_1_Quota_Pct__c=0.2, Month_2_Quota_Pct__c=0.25, Month_3_Quota_Pct__c=0.55);
		Double m1Pct = nullToZero(settings.Month_1_Quota_Pct__c)/100.0;
		Double m2Pct = nullToZero(settings.Month_2_Quota_Pct__c)/100.0;
		Double m3Pct = nullToZero(settings.Month_3_Quota_Pct__c)/100.0;
		Period currentPeriod = MySFDCUtil.getCurrentQuarter();
		Integer currentMonth = Date.today().month();

		//added for split quota
		Decimal quotaAmt = quotas.currentQuarterQuota;
		if(!String.isBlank(filter.productType)) quotaAmt = quotas.currentQuarterQuotaByType.get(filter.productType);

		if(currentMonth == currentPeriod.StartDate.month()) {
			if(quotaAmt != null) monthSummary.quota = m1Pct * quotaAmt;
		}else if(currentMonth == currentPeriod.StartDate.month() + 1) {
			//monthSummary.quota = (m1Pct+m2Pct) * quotaAmt;
			if(quotaAmt != null) monthSummary.quota = m2Pct * quotaAmt;
		}else {
			//monthSummary.quota = (m1Pct+m2Pct+m3Pct) * quotaAmt;
			if(quotaAmt != null) monthSummary.quota = m3Pct * quotaAmt;
		}

		Date startDate = Date.newInstance(Date.today().year(), Date.today().month(), 1);
		Date endDate = Date.newInstance(Date.today().year(), Date.today().month()+1, 1).addDays(-1);
		monthSummary = querySummaryData(monthSummary, filter, startDate, endDate);
	}

	private void calculateQuarterSummary(MySFDCQuota quotas, MySFDCFilter filter)
	{
		quarterSummary = new ServiceSummaryTable('Quarter');
		quarterSummary.quota = quotas.currentQuarterQuota;
		if(!String.isBlank(filter.productType)) quarterSummary.quota = quotas.currentQuarterQuotaByType.get(filter.productType);
		Period currentPeriod = MySFDCUtil.getCurrentQuarter();
		quarterSummary = querySummaryData(quarterSummary, filter, currentPeriod.StartDate, currentPeriod.EndDate);
	}

	private void calculateYearSummary(MySFDCQuota quotas, MySFDCFilter filter)
	{
		yearSummary = new ServiceSummaryTable('Year');
		Id userId = filter.runAs.Id;
		String year = String.valueOf(Date.today().year());
		/*
		AggregateResult aggr = [select SUM(Quota__c) quota from Quota__c where 
			Fiscal_Year__c = :year and OwnerId = :userId];
		yearSummary.quota = (Double)aggr.get('quota');
		*/
		if(String.isBlank(filter.productType)) {
			yearSummary.quota = quotas.fullYearQuota;
		}else{
			yearSummary.quota = quotas.fullYearQuotaByType.get(filter.productType);
		}

		Date startDate = Date.newInstance(Date.today().year(), 1, 1);
		Date endDate = Date.newInstance(Date.today().year(), 12, 31);
		yearSummary = querySummaryData(yearSummary, filter, startDate, endDate);
	}

	private ServiceSummaryTable querySummaryData(ServiceSummaryTable summary, MySFDCFilter filter, Date startDate, Date endDate)
	{
		Set<Id> userIds = new Set<Id> { filter.runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		String soql = 'select SUM(ExpectedRevenue) a, IsWon, In_Forecast_Mgr__c, Mgr_Upside__c from Opportunity '+
			'where OwnerId in :userIds and (IsWon = true or IsClosed = false) '+
			'and RecordType.Name=\'Service\' ' +
			'and CloseDate >= :startDate and CloseDate <= :endDate ' +
			'and StageName != \'Deal Lost\' and StageName != \'Dead/Cancelled\'';
		String marketVertical = filter.marketVertical;
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				soql += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				soql += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				soql += ' and Market_Vertical__c = :marketVertical';
			}
		}
		String productType = filter.productType;
		if(!String.isBlank(productType)) {
			soql += ' and Product_Type__c = :productType';
		}

		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				soql += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				soql += ' and Account.BillingCountry in :country';
			}
		}
		soql += ' group by IsWon, In_Forecast_Mgr__c, Mgr_Upside__c';

		system.debug('userIds='+String.join(new List<Id>(userIds),','));
		system.debug(soql);

		Double wonAmount = 0, forecast = 0, upside = 0, rawFunnel = 0;
		AggregateResult[] aggrResults = Database.query(soql);
		for(AggregateResult ar : aggrResults)
		{
			if((Boolean)ar.get('IsWon')) 
			{
				wonAmount += nullToZero((Double)ar.get('a'));
			}else if((Boolean)ar.get('In_Forecast_Mgr__c'))
			{
				forecast += nullToZero((Double)ar.get('a'));
				rawFunnel += nullToZero((Double)ar.get('a'));
			}else if((Boolean)ar.get('Mgr_Upside__c'))
			{
				upside += nullToZero((Double)ar.get('a'));
				rawFunnel += nullToZero((Double)ar.get('a'));
			}else{
				rawFunnel += nullToZero((Double)ar.get('a'));
			}
		}
		summary.wonAmount = wonAmount;
		summary.upside = upside;
		summary.forecast = forecast;
		summary.rawFunnel = rawFunnel;
		return summary;
	}

	private List<Opportunity> findTop5Opps(Set<Id> userIds) {
		Date today = Date.today();
		String baseQuery = 'select Id, Name, AccountId, Account.Name, Amount, Market_Vertical__c, StageName from Opportunity';
		//String top5Criteria = '(Sales_Rep_Top5__c = true or Sales_Manager_Top5__c = true or Regional_Manager_Top5__c = true)'; 
		String top5Criteria; 
		if(isSalesRepForTop5()) {
			top5Criteria = '(Sales_Rep_Top5__c = true or Sales_Manager_Top5__c = true or Regional_Manager_Top5__c = true)'; 
		} else if(isSalesManagerForTop5()) {
			//top5Criteria = '(Sales_Rep_Top5__c = true or Sales_Manager_Top5__c = true or Regional_Manager_Top5__c = true)'; 
			top5Criteria = 'Sales_Manager_Top5__c = true'; 
		}else{
			top5Criteria = 'Regional_Manager_Top5__c = true';
		}
		/*
		if(runAs.UserRole != null && runAs.UserRole.Name.indexOf('Sales Rep') >= 0) {
			top5Criteria = 'Sales_Rep_Top5__c = true';
		}else if(runAs.UserRole != null && runAs.UserRole.Name.indexOf('Sales Manager') >= 0){
			top5Criteria = 'Sales_Manager_Top5__c = true';
		}else {
			top5Criteria = 'Regional_Manager_Top5__c = true';
		}*/
		String top5SOQL = baseQuery + ' where ' + top5Criteria + ' and OwnerId in :userIds and CloseDate >= :today';
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				top5SOQL += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				top5SOQL += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				top5SOQL += ' and Market_Vertical__c = :marketVertical';
			}
		}
		if(!String.isBlank(productType)) {
			top5SOQL += ' and Product_Type__c = :productType';
		}
		if(!String.isBlank(country)) {
			top5SOQL += ' and Account.BillingCountry = :country';
		}
		top5SOQL += ' order by Amount desc';
		return Database.query(top5SOQL);
	}

	private Boolean isSalesRepForTop5()
	{
		//return userRole != null && (userRole.Name.toLowerCase().contains('partner') || userRole.Name.toLowerCase().contains('sales rep'));
		return runAs.Management_Level__c == 'Sales Rep';
	}

	private Boolean isSalesManagerForTop5()
	{
		//return userRole != null && (userRole.Name.toLowerCase().contains('lead') || userRole.Name.toLowerCase().contains('sales manager'));
		return runAs.Management_Level__c == 'Sales Manager';
	}

	private List<Opportunity> findPlannedLostOpps(Set<Id> userIds) {
		Date startDate = Date.today();
		Date endDate = Date.today().addDays(60);
		String plannedLostSOQL = 'select Id, Name, AccountId, Account.Name, Amount, Market_Vertical__c from Opportunity '+
			'where (StageName != \'Deal Won\' and StageName != \'Deal Lost\' and StageName != \'Dead/Cancelled\') ' +
			'and (Funding__c = \'70\' or Funding__c = \'90\' or Funding__c = \'100\') ' +
			'and (Competitive_Position__c = \'0\' or Competitive_Position__c = \'10\' or Competitive_Position__c = \'30\') ' + 
			'and (Timing__c = \'70\' or Timing__c = \'90\' or Timing__c = \'100\') ' +
			'and (CloseDate >= :startDate and CloseDate <= :endDate) ' +
			'and OwnerId in :userIds';
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				plannedLostSOQL += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				plannedLostSOQL += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				plannedLostSOQL += ' and Market_Vertical__c = :marketVertical';
			}
		}

		if(!String.isBlank(productType)) {
			plannedLostSOQL += ' and Product_Type__c = :productType';
		}

		if(!String.isBlank(country)) {
			plannedLostSOQL += ' and Account.BillingCountry = :country';
		}

		plannedLostSOQL += ' order by Amount desc';
		return Database.query(plannedLostSOQL);	
	}

	private List<Opportunity> findPlannedLostOppsService(Set<Id> userIds) {
		Date startDate = Date.today();
		Date endDate = Date.today().addDays(60);
		String plannedLostSOQL = 'select Id, Name, AccountId, Account.Name, Amount, Market_Vertical__c from Opportunity '+
			'where (StageName != \'Deal Won\' and StageName != \'Deal Lost\' and StageName != \'Dead/Cancelled\') ' +
			'and Deal_at_Risk__c = true ' +
			'and (CloseDate >= :startDate and CloseDate <= :endDate) ' +
			'and OwnerId in :userIds';
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				plannedLostSOQL += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				plannedLostSOQL += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				plannedLostSOQL += ' and Market_Vertical__c = :marketVertical';
			}
		}

		if(!String.isBlank(productType)) {
			plannedLostSOQL += ' and Product_Type__c = :productType';
		}

		if(!String.isBlank(country)) {
			plannedLostSOQL += ' and Account.BillingCountry = :country';
		}

		plannedLostSOQL += ' order by Amount desc';
		return Database.query(plannedLostSOQL);	
	}
	/* see Opportunity.Forecast_Category__c formula field for logic */
	private String getForecastCategory(String customerType, Boolean mgrUpside, Boolean inForecast){
		if(mgrUpside && (customerType == 'New to Mass Spec' || customerType == 'Competitive conversion')) return 'Upside New Customer';
		else if(mgrUpside && (customerType == 'Existing AB SCIEX account')) return 'Upside Existing Customer';
		else if(inForecast) return 'Forecast';
		else return 'Not Forecast';
	}

	public PageReference initReport(){
		String report = ApexPages.currentPage().getParameters().get('r');
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			runAsId = ApexPages.currentPage().getParameters().get('u');
		}else{
			runAsId = UserInfo.getUserId();
		}
		runAs = MySFDCUtil.findUser(runAsId);
		if(ApexPages.currentPage().getParameters().containsKey('mv')) {
			marketVertical = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('mv'), 'UTF-8');
		}else{
			marketVertical = '';
		}
		if(ApexPages.currentPage().getParameters().containsKey('pt')) {
			productType = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('pt'), 'UTF-8');
		}else{
			productType = '';
		}

		//load quotas for the quarter
		Set<Id> userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		MySFDCQuota quotas = MySFDCUtil.getQuotas(runAs.Id);
		Map<String,Decimal> conversionRatesMap = MySFDCUtil.getConversionRates(runAs);
		MySFDCFilter filter = new MySFDCFilter();
		filter.runAs = runAs;
		filter.marketVertical = marketVertical;
		filter.productType = productType;
		filter.region = region;
		filter.country = country;

		/*
		IMySFDCDashboard db = ABSciexDashboardFactory.getDashboard (
			report, runAs, marketVertical, quotas, conversionRatesMap
		);
		*/
		IMySFDCDashboard db = ABSciexDashboardFactory.getDashboard (
			report, filter, quotas, conversionRatesMap
		);
		reportTitle = db.getReportTitle();
		reportPager = (Pager)db;

		if(ApexPages.currentPage().getParameters().containsKey('s1')) {
			reportPager.setSortColumn1(ApexPages.currentPage().getParameters().get('s1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd1')) {
			reportPager.setSortDirection1(ApexPages.currentPage().getParameters().get('sd1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('s2')) {
			reportPager.setSortColumn2(ApexPages.currentPage().getParameters().get('s2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd2')) {
			reportPager.setSortDirection2(ApexPages.currentPage().getParameters().get('sd2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('p')) {
			reportPager.setPageNumber(Integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
		}
		reportPager.fetchRecords();
		return null;
	}

	public PageReference initReportExport(){
		String report = ApexPages.currentPage().getParameters().get('r');
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			runAsId = ApexPages.currentPage().getParameters().get('u');
		}else{
			runAsId = UserInfo.getUserId();
		}
		runAs = MySFDCUtil.findUser(runAsId);
		if(ApexPages.currentPage().getParameters().containsKey('mv')) {
			marketVertical = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('mv'), 'UTF-8');
		}else{
			marketVertical = '';
		}

		//load quotas for the quarter
		Set<Id> userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		MySFDCQuota quotas = MySFDCUtil.getQuotas(runAs.Id);
		Map<String,Decimal> conversionRatesMap = MySFDCUtil.getConversionRates(runAs);
		MySFDCFilter filter = new MySFDCFilter();
		filter.runAs = runAs;
		filter.marketVertical = marketVertical;

		/*
		IMySFDCDashboard db = ABSciexDashboardFactory.getDashboard (
			report, runAs, marketVertical, quotas, conversionRatesMap
		);
		*/
		IMySFDCDashboard db = ABSciexDashboardFactory.getDashboard (
			report, filter, quotas, conversionRatesMap
		);
		reportTitle = db.getReportTitle();
		reportPager = (Pager)db;

		reportPager.setPageSize(1000);

		if(ApexPages.currentPage().getParameters().containsKey('s1')) {
			reportPager.setSortColumn1(ApexPages.currentPage().getParameters().get('s1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd1')) {
			reportPager.setSortDirection1(ApexPages.currentPage().getParameters().get('sd1'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('s2')) {
			reportPager.setSortColumn2(ApexPages.currentPage().getParameters().get('s2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('sd2')) {
			reportPager.setSortDirection2(ApexPages.currentPage().getParameters().get('sd2'));
		}
		if(ApexPages.currentPage().getParameters().containsKey('p')) {
			reportPager.setPageNumber(Integer.valueOf(ApexPages.currentPage().getParameters().get('p')));
		}
		reportPager.fetchRecords();
		return null;
	}

	public PageReference saveInlineEdits(){
		try {
			update this.reportPager.getReportOpps();
			String url = '/apex/MySFDCReport';
			List<String> params = new List<String>();
			for(String p : ApexPages.currentPage().getParameters().keySet()){
				params.add(p + '=' + EncodingUtil.urlEncode(ApexPages.currentPage().getParameters().get(p), 'UTF-8'));
			}
			if(params.size() > 0) url += '?' + String.join(params,'&');
			return new PageReference(url).setRedirect(true);
		}catch(DmlException e) {
			MySFDCUtil.addDMLExceptionMessagesToPage(e);
			return null;
		}
	}

	public PageReference cancelInlineEdits() {
		String url = '/apex/MySFDC';
		if(ApexPages.currentPage().getParameters().containsKey('u')) {
			url += '?u=' + ApexPages.currentPage().getParameters().get('u');
		}
		return new PageReference(url).setRedirect(true);
	}

	public List<SelectOption> runAsList {
		get {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption(currentUser.Id, currentUser.Name));
			List<User> users = MySFDCUtil.findUsersInRoleHierarchy(currentUser.UserRoleId);
			for(User u : users) {
				options.add(new SelectOption(u.Id, u.Name));
			}
			return options;
		}
	}

	public List<SelectOption> marketVerticalList {
		get {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', 'All'));
     		for(Schema.PicklistEntry ple : Opportunity.Market_Vertical__c.getDescribe().getPicklistValues()){
     			options.add(new SelectOption(ple.getValue(), ple.getValue()));
     		}
     		options.add(new SelectOption('Clinical & Forensic', 'Clinical & Forensic'));
     		options.add(new SelectOption('Food & Environmental', 'Food & Environmental'));
     		return options;
		}
	}

	public List<SelectOption> productTypeList {
		get {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', 'All'));
	     		for(Schema.PicklistEntry ple : Opportunity.Product_Type__c.getDescribe().getPicklistValues()){
	     			options.add(new SelectOption(ple.getValue(), ple.getValue()));
	     		}
	     		return options;
		}
	}

	public class QuotaTable {
		public String title {get;set;}
		public List<QuotaTableRow> tableData {get;set;}

		public QuotaTable(String title){
			this.title = title;
			this.tableData = new List<QuotaTableRow>();
		}
	}

	public class QuotaTableRow {
		public String label {get;set;}
		public Decimal value {get;set;}

		public QuotaTableRow(String label, Decimal value){
			this.label = label;
			this.value = value;
		}
	}

	public class Top5Table {
		public String title {get;set;}
		public List<Top5TableRow> tableData {get;set;}
		public Boolean isServiceUser {get;set;}

		public Top5Table(){
			this.title = 'Top 5 Deals';
			this.tableData = new List<Top5TableRow>();
			isServiceUser = false;
		}
	}

	public class Top5TableRow {
		public Opportunity opp {get;set;}

		public Top5TableRow(Opportunity opp){
			this.opp = opp;
		}
	}

	public List<SelectOption> regionsList {
		get {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', 'All'));
	     		for(String region : RegionUtil.getAllRegions()){
	     			options.add(new SelectOption(region, region));
	     		}
	     		return options;
		}
	}

	public Boolean isServiceUser {
		get {
			return MySFDCUtil.isServiceUser(currentUser);
		}
	}

	public List<SelectOption> countriesList {
		get {
			List<SelectOption> options = new List<SelectOption>();
			options.add(new SelectOption('', 'All'));
			for(String c : RegionUtil.getAllCountries(region)) {
     				options.add(new SelectOption(c, c));
			}
	     		return options;
		}
	}

	public class ServiceSummaryTable 
	{
		public String period {get;set;} //Month, Quarter, Year
		public Double quota {get;set;}
		public Double wonAmount {get;set;}
		public Double gap {
			get {
				Double quotaAmt = nullToZero(quota);
				Double wonAmt = nullToZero(wonAmount);
				if(quotaAmt >= wonAmt) return quotaAmt - wonAmt;
				else return wonAmt - quotaAmt;
			}
		}

		public String gapLabel {
			get {
				Double quotaAmt = nullToZero(quota);
				Double wonAmt = nullToZero(wonAmount);
				if(quotaAmt >= wonAmt) return 'Gap';
				else return 'Above Plan';
			}
		}

		public Double rawFunnel {get;set;}
		public Double upside {get;set;}
		public Double forecast {get;set;}
		public Double delta {
			get {
				return nullToZero(forecast) + nullToZero(upside) - gap;
			}
		}

		public Double percentToPlan {
			get {
				if(wonAmount == null) return 0;
				else if(quota == null || quota == 0) return 0;
				else return (wonAmount/quota)*100;
			}
		}

		public ServiceSummaryTable(String period)
		{
			this.period = period;
		}

	}

	private static Double nullToZero(Double num){
		return num == null ? 0 : num;
	}

	private static Decimal nullToZero(Decimal num){
		return num == null ? 0 : num;
	}
}