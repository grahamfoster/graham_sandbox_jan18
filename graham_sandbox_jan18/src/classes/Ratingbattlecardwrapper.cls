global class Ratingbattlecardwrapper implements Comparable {
    public string battlename{get;set;}
    public string dated{get;set;}
    public decimal cp{get;set;}
    public decimal pp{get;set;}
    public decimal demo{get;set;}
    public decimal sum{get;set;}
    public decimal res{get;set;}
    public Boolean selected {get; set;}
    public string ratingManageId {get; set;}
    public string comment{get;set;}
    public String packageid{get; set;}
    public String userName{get; set;}
    public String eMail{get; set;}
    public Static String SortField;
    public Static String SortDirection;

    public Ratingbattlecardwrapper()
    {
        this.selected = true;
    }

    global Integer compareTo(Object objToCompare)
    {
        if(SortDirection == 'ASC') {
            return sortAscending(objToCompare);
        } else {
            return sortDescending(objToCompare);
        }

        return 0;
    }

    public integer sortAscending(Object objToCompare)
    {
        if (SortField == 'Battlecard_name__c')
            return battlename.compareTo(((Ratingbattlecardwrapper)objToCompare).battlename);

        if (SortField == 'CreatedDate')
            return dated.compareTo(((Ratingbattlecardwrapper)objToCompare).dated);

        if (SortField == 'CP__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (cp == compareRating.cp) return 0;
            if (cp > compareRating.cp) return 1;
            return -1;
        }

        if (SortField == 'PP__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (pp == compareRating.pp) return 0;
            if (pp > compareRating.pp) return 1;
            return -1;
        }

        if (SortField == 'Demo__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (demo == compareRating.demo) return 0;
            if (demo > compareRating.demo) return 1;
            return -1;
        }

        if (SortField == 'Summary__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (sum == compareRating.sum) return 0;
            if (sum > compareRating.sum) return 1;
            return -1;
        }

        if (SortField == 'Additional_Resource_Rating__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (res == compareRating.res) return 0;
            if (res > compareRating.res) return 1;
            return -1;
        }

        if(SortField == 'Comments__c') {
            if (comment != null && ((Ratingbattlecardwrapper)objToCompare).comment != null)
            {
                return ((Ratingbattlecardwrapper)objToCompare).comment.compareTo(comment) * (-1);
            }
            if (comment == null && ((Ratingbattlecardwrapper)objToCompare).comment == null)
            {
                return 0;
            }
            if (comment == null)
            {
               return -1;
            }
            if (((Ratingbattlecardwrapper)objToCompare).comment == null)
            {
               return 1;
            }
        }
        return 0;
    }

    public integer sortDescending(Object objToCompare)
    {
        if (SortField == 'Battlecard_name__c')
            return ((Ratingbattlecardwrapper)objToCompare).battlename.compareTo(battlename);

        if (SortField == 'CreatedDate')
            return ((Ratingbattlecardwrapper)objToCompare).dated.compareTo(dated);

        if (SortField == 'CP__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (cp == compareRating.cp) return 0;
            if (cp > compareRating.cp) return -1;
            return 1;
        }

        if (SortField == 'PP__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (pp == compareRating.pp) return 0;
            if (pp > compareRating.pp) return -1;
            return 1;
        }

        if (SortField == 'Demo__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (demo == compareRating.demo) return 0;
            if (demo > compareRating.demo) return -1;
            return 1;
        }

        if (SortField == 'Summary__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (sum == compareRating.sum) return 0;
            if (sum > compareRating.sum) return -1;
            return 1;
        }

        if (SortField == 'Additional_Resource_Rating__c') {
            Ratingbattlecardwrapper compareRating = (Ratingbattlecardwrapper)objToCompare;
            if (res == compareRating.res) return 0;
            if (res > compareRating.res) return -1;
            return 1;
        }

        if (SortField == 'Comments__c') {
            if (comment != null && ((Ratingbattlecardwrapper)objToCompare).comment != null)
            {
                return ((Ratingbattlecardwrapper)objToCompare).comment.compareTo(comment);
            }
            if (comment == null && ((Ratingbattlecardwrapper)objToCompare).comment == null)
            {
                return 0;
            }
            if (comment == null)
            {
               return 1;
            }
            if (((Ratingbattlecardwrapper)objToCompare).comment == null)
            {
               return -1;
            }
        }

        return 0;
    }
}