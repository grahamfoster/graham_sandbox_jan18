/**
 * @author Brett Moore
 * @created - July 2016
 * @Revision  
 * @Last Revision 
 * 
 * Class to generate contract Opportunities based on expiring contacts
 * 
**/
public class Contract2Opportunity_Create extends Contract2Opportunity_Utils{

    public Contract2Opportunity_Configuration__c c2oConfig;     // Contract2Opportunity general Configuration
    private Map<String,c2oSettings__c> settingsMap;             // Contract2Opportunity Contract Settings
    private Map<String, SVMXC__Service_Contract__c> Contracts;  // Map of Parent level contracts
    public List<SVMXC__Service_Contract__c> completedSMCs;  // List of Service Contracts that have been successfully run
    public Map<Id, SVMXC__Service_Contract__c> failedSMCs;  // Set of Service Contracts that have failed     
    public Map<Id, Id> mergeWith; //  Map< (ID of Contract) , (ID of Contract to Merge with) >
    Public Map<ID,SVMXC__Service_Contract__c> updateSMCs; // Map of SMCs to be updated with Oportunity record and Opp Created = TRUE
    public Map<id, Opportunity> oppList; //Map of <SMC ID, new Opportunity>
    
    public Contract2Opportunity_Create(Map<Id, SVMXC__Service_Contract__c> initialQuery) {    

        // Initialization
        this.c2oConfig = c2oConfiguration();
        this.settingsMap = customSettings();
        this.completedSMCs = new List<SVMXC__Service_Contract__c>();
        this.failedSMCs = new Map<Id,SVMXC__Service_Contract__c>();
        // Initialize custom Logging for Contracts
        CustomLogger.Logger ContractLog = new CustomLogger.Logger();
        ContractLog.loglevel = c2oConfig.Log_Level__c;
        ContractLog.process = 'Create Opportunities';
        // Index unique Contracts by  Parent Contract Number
        this.Contracts = new Map<String, SVMXC__Service_Contract__c>();  // <Parent Contract Number, Contract>
        this.mergeWith = new Map<Id, Id>();
        if(initialQuery.size() >0){
        	for(SVMXC__Service_Contract__c i :initialQuery.values()){
	            if(!Contracts.containsKey(i.Parent_Contract_Number__c)){ 
	               Contracts.put(i.Parent_Contract_Number__c, i);
	               ContractLog.addLine('Info','Contract' , i.id , i.Parent_Contract_Number__c , 'Evaluate' , 'Marked for Opportuntiy Creation');
	            } else {
	               ContractLog.addLine('Info','Contract' , i.id , i.Parent_Contract_Number__c , 'Merge' , 'Merge with ' + Contracts.get(i.Parent_Contract_Number__c).id); 
	               mergeWith.put(i.id, Contracts.get(i.Parent_Contract_Number__c).id);                
	            }
	        }
        } 
        //Prepare Opportunities
        Map < ID , Opportunity> OpportunityMap = new Map < ID , Opportunity>();
        this.oppList = new Map<Id,Opportunity>();
        if(Contracts.size() >0){
	        for(SVMXC__Service_Contract__c i :Contracts.values()){
	            if(settingsMap.containsKey(i.SVC_CATEGORY__C)){
	                Opportunity newOpp = new Opportunity();
	                newOpp.Pricebook2Id = i.SVMXC__Company__r.Default_Service_Pricebook__c;                     
	                newOpp.Product_Type__c = settingsMap.get(i.SVC_CATEGORY__C).Product_Type__C;
	                newOpp.StageName =  settingsMap.get(i.SVC_CATEGORY__C).STAGE_NAME__C;
	                newOpp.RecordTypeId =  settingsMap.get(i.SVC_CATEGORY__C).RECORDTYPE__C;       
	                string nameCode = settingsMap.get(i.SVC_CATEGORY__C).Naming_Convention__C;  
	                newOpp.Contract_Number__c = i.Parent_Contract_Number__c;
	                newOpp.AccountId = i.SVMXC__Company__c;                  
	                String formatDate = i.SVMXC__END_DATE__C.year() +'';
	                if(i.SVMXC__END_DATE__C.month()>9){
	                    formatDate = formatDate + i.SVMXC__END_DATE__C.month();
	                } else{
	                    formatDate = formatDate + '0' + i.SVMXC__END_DATE__C.month();
	                }
	                if(i.SVMXC__END_DATE__C.day()>9){
	                    formatDate = formatDate + i.SVMXC__END_DATE__C.day();
	                } else{
	                    formatDate = formatDate + '0' + i.SVMXC__END_DATE__C.day();
	                }
	                newOpp.Name = formatDate + '_' +nameCode + '_' +  i.SVMXC__Company__r.Name.left(100) ;            
	                newOpp.Contract_Expiry_Date__c = i.SVMXC__END_DATE__C;
	                newOpp.CloseDate = i.SVMXC__END_DATE__C;
	                newOpp.Primary_Contact__c = i.SVMXC__Contact__c;  
	                newOpp.CurrencyIsoCode = i.SVMXC__Company__r.CurrencyIsoCode;
	                newOpp.Previous_Contract__c = i.id;
	                newOpp.c2o_AutoGeneration_Status__c = 'Created';
	                if(validOpp(newOpp)){
	                    ContractLog.addLine('Info','Contract' , i.id , i.Parent_Contract_Number__c , 'Prepare' , 'Opportunity Data is Valid');
	                    oppList.put( i.id,newOpp);
	                    completedSMCs.add(i);
	                } else {	       
						failedSMCs.put(i.id,i);                        
	                    ContractLog.addLine('Error','Contract' , i.id , i.Parent_Contract_Number__c , 'Prepare' , 'Opportunity Data is Invalid (Possible Missing Country Mapping)');         
	                }
	            } else {
	                ContractLog.addLine('Error','Contract' , i.id , i.Parent_Contract_Number__c , 'Prepare' , 'Unable to find custom settings for Contract type: ' + i.SVC_CATEGORY__C); 
	            }
        	}
        }
        // Insert Opportunities
        if(oppList.size() > 0){
            Database.SaveResult[] srList = Database.insert(oppList.values(), false);   
            // Iterate through each returned result
            for (Integer c = 0; c < srList.size(); c++) {
                if (srList.get(c).isSuccess() ) {
                    // Operation was successful, post to Log
                    ContractLog.addLine('Success','Contract' , completedSMCs.get(c).id , completedSMCs.get(c).Parent_Contract_Number__c , 'INSERT' , 'Opportunity Created ' + srList.get(c).getId()); 
                } else { 
                    // Operation failed, post errors to Log
                    Database.Error[] err = srList.get(c).getErrors();
                    String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
                    String escapeErr = errorMsg.replace(',','.');
                    ContractLog.addLine('Error','Contract' , completedSMCs.get(c).id , completedSMCs.get(c).Parent_Contract_Number__c , 'INSERT' , escapeErr);                 
                    failedSMCs.put(completedSMCs.get(c).id,completedSMCs.get(c));
                }
            }
        }
        // Iterate through all failed Opportunity inserts - remove contracts that did not generate
        this.updateSMCs = new Map<ID,SVMXC__Service_Contract__c>(completedSMCs);      
        for(id fid :failedSMCs.keySet()){           
          if(updateSMCs.containsKey(fid)) updateSMCs.remove(fid);
        }          
        // Iterate through all merged contracts - Add merged contracts to success list
        for(id mid :mergeWith.keySet()){    
            if(!failedSMCs.containsKey(mergeWith.get(mid))){      //  If the contract this is merged with is NOT found in the failed update list
                updateSMCs.put(mid, initialQuery.get(mid));             
            }
        }         
        
        // set Renewal Opportunity lookup and flag for successful Opportunities
        for(SVMXC__Service_Contract__c smc :updateSMCs.values()){
            if(oppList.containsKey(smc.id)){              
            	smc.c2o_Renewal_Opportunity__c = oppList.get(smc.id).id;
            } else {              
                smc.c2o_Renewal_Opportunity__c = oppList.get(mergeWith.get(smc.id)).id;             
            }    
            smc.Renewal_Opportunity_Created__c = TRUE;
        }
        // Update Contracts
        if(updateSMCs.size() > 0){
            List <SVMXC__Service_Contract__c> SMClist = new List <SVMXC__Service_Contract__c>(updateSMCs.values());
            Database.SaveResult[] srList = Database.update(SMClist, false);        
            // Iterate through each returned result
            for (Integer c = 0; c < srList.size(); c++) {
                if (srList.get(c).isSuccess() ) {
                    // Operation was successful, post to Log
                    ContractLog.addLine('Success','Contract' , SMClist.get(c).id , SMClist.get(c).Parent_Contract_Number__c , 'UPDATE' , 'Contract updated' ); 
                } else {
                    // Operation failed, post errors to Log
                    Database.Error[] err = srList.get(c).getErrors();
                    String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
                    String escapeErr = errorMsg.replace(',','.');
                    ContractLog.addLine('Error','Contract' , SMClist.get(c).id , SMClist.get(c).Parent_Contract_Number__c , 'UPDATE' , escapeErr);                 
                }
            }
        }
        ContractLog.saveLog();          
    }        
}

/*
 * Code for invoking vie Annonymus execution
 *  
 * Map<Id, SVMXC__Service_Contract__c> initialQuery = new Map<Id, SVMXC__Service_Contract__c>([SELECT id, name, c2o_First_Product_Expiration__c,
        SVC_Oracle_Status__c,CurrencyISOCode,SVMXC__COMPANY__C,SVMXC__COMPANY__r.name,SVMXC__Company__r.CurrencyIsoCode,SVMXC__COMPANY__r.Default_Service_Pricebook__c ,SVMXC__CONTACT__C,SVMXC__END_DATE__C,
        PARENT_CONTRACT_NUMBER__c,  SVC_CATEGORY__C,    Renewal_Opportunity_Created__c
    FROM 
        SVMXC__Service_Contract__c 
    WHERE 
        Renewal_Opportunity_Created__c = FALSE AND 
        SVMXC__Active__c = TRUE AND 
        SVMXC__END_DATE__C = NEXT_N_DAYS:180
    ORDER BY
        SVMXC__END_DATE__C ASC LIMIT 50]);

  new Contract2Opportunity_Create(initialQuery);
*/
/*  OR
 * 
        String Query = 'SELECT id, name, c2o_First_Product_Expiration__c,SVC_Oracle_Status__c,CurrencyISOCode,SVMXC__COMPANY__C,SVMXC__COMPANY__r.name,SVMXC__Company__r.CurrencyIsoCode,SVMXC__COMPANY__r.Default_Service_Pricebook__c ,SVMXC__CONTACT__C,SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__c,  SVC_CATEGORY__C,    Renewal_Opportunity_Created__c FROM SVMXC__Service_Contract__c WHERE Renewal_Opportunity_Created__c = FALSE AND SVMXC__Active__c = TRUE AND c2o_First_Product_Expiration__c = NEXT_N_DAYS:180 ORDER BY PARENT_CONTRACT_NUMBER__c ASC';
        Integer scope = 200;
        String process = 'Create';
        
        Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
		id batchInstanceId = database.executebatch(b,scope);
*/