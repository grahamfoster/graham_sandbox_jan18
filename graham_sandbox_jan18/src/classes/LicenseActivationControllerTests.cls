@isTest private class LicenseActivationControllerTests {

    static Contact CreateFakeContact() {
        Contact contact = new Contact();
        contact.FirstName = 'Test';
        contact.LastName = 'Test';
        contact.Email = 'test@sciex.com';
        contact.Phone = '9056609005';
        contact.MailingStreet = '71 Four Valley Drive';
        contact.MailingCity = 'Toronto';
        contact.MailingState = 'Ontario';
        contact.MailingCountry = 'Canada';
        contact.MailingPostalCode = 'L4K 4V8';
        insert contact;
        return contact;
    }
    
    static Account CreateFakeAccount() {
        Account account = new Account();
        account.Name = 'Test';
        account.BillingCountry = 'Canada';
        insert account;
        return account;
    }
    
    static Asset CreateFakeAsset() {
        Asset asset = new Asset();
        asset.AccountId = CreateFakeAccount().Id;
        asset.Name = 'Test';
        asset.SerialNumber = 'Test';
        insert asset;
        return asset;
    }
    
    static Asset_Lookup__c CreateFakeAssetLookup() {
        Asset_Lookup__c assetLookup = new Asset_Lookup__c();
        assetLookup.Asset__c = CreateFakeAsset().Id;
        insert assetLookup;
        return assetLookup;
    }
    
    static LicenseActivationSettings__c CreateFakeTestSettings() {
        LicenseActivationSettings__c settings = new LicenseActivationSettings__c();
        System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
        settings.Name = UserInfo.getOrganizationId();
        settings.ActivateLicenseServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/ActivateLicense';
        settings.ActivationDetailsServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/ActivationDetails';
        settings.GetTrialDetailsServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/GetTrialDetails';
        insert settings;
        return settings;
    }

    static testMethod void testLicenseActivationControllerConstructor() {
        Contact contact = CreateFakeContact();
        
        test.startTest();
        ApexPages.StandardController standardController = new ApexPages.StandardController(contact);
        LicenseActivationController controller = new LicenseActivationController(standardController);
        test.stopTest();
    }
    
    static testMethod void testActivationIDChanged() {
        Contact contact = CreateFakeContact();
        CreateFakeTestSettings();
        LicenseActivationMockJSONGenerator mockServlet = new LicenseActivationMockJSONGenerator(
            '{"success":true,"activated":false,"message":"The supplied license key is valid.","productName":"SCIEX OS Full","productVersion":"1.2"}',
            200
        );
        Test.setMock(HttpCalloutMock.class, mockServlet);
        
        test.startTest();     
        ApexPages.StandardController standardController = new ApexPages.StandardController(contact);
        LicenseActivationController controller = new LicenseActivationController(standardController);
        controller.activationId = '';
        controller.ActivationIDChanged();
        System.assertEquals(controller.detailsResponseSuccess, true);
        test.stopTest();
    }
    
    static testMethod void testActivateLicense() {
        Contact contact = CreateFakeContact();
        Asset_Lookup__c assetLookup = CreateFakeAssetLookup();
        CreateFakeTestSettings();
        LicenseActivationMockJSONGenerator mockServlet = new LicenseActivationMockJSONGenerator(
            '{"success":true,"message":"Successfully activated the supplied license key.","diagnostics":"Found the following existing users with this email address' +
            ' (alexandru.dima@sciex.com): 146959. Updating these records. Successfully updated records with this email address (alexandru.dima@sciex.com).",' +
            '"licenseText":"INCREMENT SCIEXOS.AQUISITION ScxMSSW0 1.2 11-jul-2017 uncounted \\\n\tHOSTID\u003d012345678901 ISSUER\u003dSCIEX ISSUED\u003d10-jul-2017' +
            ' \\\n\tSN\u003dAID-05c5-61b6-a98d-44b8-a7b1-38b3-4a9d-1fb4 \\\n\tSTART\u003d9-jul-2017 TS_OK SIGN\u003d\"00A4 2FBB F574 0EC9 6126 4ED2 \\\n\tE4ED CB00 B74B' +
            ' CEFE 406E 5F31 0613 BE7D A88E\"\nINCREMENT SCIEXOS.PROCESS ScxMSSW0 1.2 11-jul-2017 uncounted \\\n\tHOSTID\u003d012345678901 ISSUER\u003dSCIEX ISSUED\u003d10-jul-2017' +
            ' \\\n\tSN\u003dAID-05c5-61b6-a98d-44b8-a7b1-38b3-4a9d-1fb4 \\\n\tSTART\u003d9-jul-2017 TS_OK SIGN\u003d\"0014 FBE2 A74B 72C3 8E02 DFFE \\\n\t3B1B 5A00 8E99 47F0 DF7C 5DFE' +
            ' 48E2 95C8 2A32\"\nINCREMENT SCIEXOS.LAUNCH ScxMSSW0 1.2 11-jul-2017 uncounted \\\n\tHOSTID\u003d012345678901 ISSUER\u003dSCIEX ISSUED\u003d10-jul-2017' +
            ' \\\n\tSN\u003dAID-05c5-61b6-a98d-44b8-a7b1-38b3-4a9d-1fb4 \\\n\tSTART\u003d9-jul-2017 TS_OK SIGN\u003d\"00E8 0D3E 1962 07CF 3C2D B6C2 \\\n\tE2F3 3E00 3C1B 3791 9EA5 584E' +
            ' 2A01 8A67 6289\"\n","licenseFilename":"SCIEXOS1.2.lic","licenseFileLocation":"C:\\Program Files (x86)\\SCIEX\\SCIEXOS","productName":"SCIEX OS Full","productVersion":"1.2",' +
            '"serialNumber":"0123456789","customer":{"computerId":"012345678901","middleName":"Ovidiu","lastName":"Dima","state":"ON","instrumentSN":"0123456789","addressLineOne":"Test' +
            ' Address (Line One)","emailLicense":"true","country":"Canada","city":"Toronto","addressLineTwo":"Test Address (Line Two)","phoneNumber":"9056609005","email":' +
            '"alexandru.dima@sciex.com","zipCode":"L4K4V8","company":"SCIEX","firstName":"Alexandru","activationId":"AID-05c5-61b6-a98d-44b8-a7b1-38b3-4a9d-1fb4"}}',
            200
        );
        Test.setMock(HttpCalloutMock.class, mockServlet);
        
        test.startTest();
        ApexPages.StandardController standardController = new ApexPages.StandardController(contact);
        LicenseActivationController controller = new LicenseActivationController(standardController);
        controller.activationId = '';
        controller.computerId = '';
        controller.assetLookup = assetLookup;
        controller.ActivateLicense();
        test.stopTest();
    }
}