public class MySFDCForecastAndUpDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {
	public Decimal upsideExisting {get;set;}
	public Decimal upsideNew {get;set;}

	public MySFDCForecastAndUpDashboard() {
		super();
	}

	/* custom fields */
	public Boolean isCurrentQuarter {get;
		set {
			if(value == null || value) report = 'CurrentForecastAndUp';
			else report = 'NextForecastAndUp';
		}
	}
	public Integer quarterNumber {get;set;}

	public String getReportTitle() {
		return 'Q' + quarterNumber + ' Forecast and Upside';
	}

	/* the criteria for the query */
	protected override String criteria() {
		String crit = 'OwnerId in :userIds ' + 
			'and StageName != \'Dead/Cancelled\' and StageName != \'Deal Lost\' ' +
			'and ((Shippable_Date__c >= :startDate and Shippable_Date__c <= :endDate) ' +
			'or (Shippable_Date__c = NULL and CloseDate >= :startDate and CloseDate <= :endDate)) ' + 
			'and (In_Forecast_Mgr__c = true or Mgr_Upside__c = true) and Customer_Type__c in (\'New to Mass Spec\', \'Competitive conversion\', \'Existing AB SCIEX account\')';

		if(MySFDCUtil.isServiceUser(filter.runAs)) {
			crit += ' and RecordType.Name = \'Service\'';
		}

		if(!String.isBlank(filter.marketVertical)) {
			if(filter.marketVertical == 'Clinical & Forensic') {
				crit += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(filter.marketVertical == 'Food & Environmental') {
				crit += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				crit += ' and Market_Vertical__c = :marketVertical';
			}
		}

		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry in :country';
			}
		}
		return crit;
	}

	public override void query() {
		this.actual = 0;
		this.upsideExisting = 0;
		this.upsideNew = 0;

		String marketVertical = filter.marketVertical;
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
		List<String> country = RegionUtil.getCountryAliases(filter.country);

		String queryCQ = 'select IsWon, In_Forecast_Mgr__c inForecast, Customer_Type__c customerType, SUM(Amount) amt, SUM(ExpectedRevenue) erev from Opportunity '+
			'where ' + criteria() + ' group by In_Forecast_Mgr__c, Customer_Type__c, IsWon';

		Decimal upsideExistingConversionRate, upsideNewConversionRate;
		Decimal forecastConversionRate = conversionRateMap.containsKey('Forecast') ? conversionRateMap.get('Forecast') : 1.0;
		if(MySFDCUtil.isServiceUser(filter.runAs)) {
			upsideExistingConversionRate = conversionRateMap.containsKey('Upside') ? conversionRateMap.get('Upside') : 1.0;
			upsideNewConversionRate = conversionRateMap.containsKey('Upside') ? conversionRateMap.get('Upside') : 1.0;
		}else{
			upsideExistingConversionRate = conversionRateMap.containsKey('Upside Existing') ? conversionRateMap.get('Upside Existing') : 1.0;
			upsideNewConversionRate = conversionRateMap.containsKey('Upside New') ? conversionRateMap.get('Upside New') : 1.0;
		}


		for(AggregateResult aggr : Database.query(queryCQ)) {
			Boolean isExistingCustomer = false;
			String customerType = (String)aggr.get('customerType');
			if(customerType == 'Existing AB SCIEX account') {
				isExistingCustomer = true;
			}
			Boolean isForecast = (Boolean)aggr.get('inForecast');
			Boolean isWon = (Boolean)aggr.get('IsWon');
			Decimal amt = aggr.get('amt') != null ? (Decimal)aggr.get('amt') : 0;
			Decimal erev = aggr.get('erev') != null ? (Decimal)aggr.get('erev') : 0;

			if(!isWon) {
				if(isForecast) {
					this.actual += forecastConversionRate * amt;
				}else if(isExistingCustomer) {
					this.actual += upsideExistingConversionRate * amt;
					this.upsideExisting += upsideExistingConversionRate * amt;
				} else {
					this.actual += upsideNewConversionRate * amt;
					this.upsideNew += upsideNewConversionRate * amt;
				}
			}else{
				this.actual += amt;
				if(isExistingCustomer) {
					this.upsideExisting += amt;
				}else{
					this.upsideNew += amt;
				}
			}
		}
	}

	/* the default sorting criteria for the query */
	protected override String defaultSorting() {
		return ' order by Shippable_Date__c desc, CloseDate desc';
	}

	/* the list of fields for the report */
	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	/* Generate the google chart data */
	public override GoogleGaugeChartData getGoogleGaugeChartData() {
		GoogleGaugeChartData c11 = new GoogleGaugeChartData(report);
		c11.gaugeLabel = '($M)';
		c11.title = 'Q'+quarterNumber+' FC + Upside';
		c11.actual = this.actual;
		c11.quota = this.quota;
		return c11;
	}
}