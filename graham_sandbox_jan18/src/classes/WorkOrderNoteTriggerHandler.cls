/*
 *	WorkOrderNoteTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2016-05-20 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public class WorkOrderNoteTriggerHandler extends TriggerHandler {

    public WorkOrderNoteTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void beforeInsert() {
		linkToCase();
	}
	override protected void beforeUpdate() {
		linkToCase();
	}
    override protected void afterInsert() {

	}
	override protected void afterUpdate() {

	}
	override protected void afterUndelete() {
    
	}
	override protected void afterDelete() {
    
	}
    public void linktoCase(){
        // Loop through all records and collect Parent Work Order IDs
        List <ID> woIds = new List <ID>();
        For( SVC_WO_Note__c woNote : (List<SVC_WO_Note__c>)Trigger.new){
            woIds.add(woNote.SVC_Work_Order__c);            
        }
        // Query all needed WOs
        Map <Id, SVMXC__Service_Order__c> workOrders = new Map <Id,SVMXC__Service_Order__c>([Select id, SVMXC__Case__c FROM SVMXC__Service_Order__c WHERE id IN :woIds]);
		        
		// Loop through all records and update the SVMXC__Case__c field
        For( SVC_WO_Note__c woNote : (List<SVC_WO_Note__c>)Trigger.new){
            if(String.isBlank(woNote.SVC_Case__c)){
            	woNote.SVC_Case__c = workOrders.get(woNote.SVC_Work_Order__c).SVMXC__Case__c;           
            }
        }     
    }
}