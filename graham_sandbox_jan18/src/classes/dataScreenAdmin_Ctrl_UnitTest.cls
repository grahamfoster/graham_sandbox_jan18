@isTest
public class dataScreenAdmin_Ctrl_UnitTest {
// Covered  dataScreenAdmin_Ctrl
private static testMethod void dataScreenAdmin_Ctrl_UnitTest()
{
    test.startTest();
    Persona__c persona =  Util.createPersona();
    Manufacturer__c manufacturer =  Util.createManufacturer();
    Application__c application =  Util.createApplication();
    Application_Variants__c applicationvar = Util.createAppVariants(application.Id);

    dataScreenAdmin_Ctrl dataObj = new dataScreenAdmin_Ctrl();
    dataObj.returnPersonaList();
    dataObj.redirectadmin();
    dataObj.getAppmap();

    dataObj.delPersonaId = persona.Id;
    dataObj.delPersona();
    dataObj.selPersonaId = persona.Id;
    dataObj.delPersona();

    dataObj.selManufacturerId = manufacturer.Id;

    dataObj.selApp = application.Id;
    dataObj.delAppCategory();

    dataObj.selAppVariantId = applicationvar.Id;
    dataObj.delAppVariant();

    test.stopTest();
}

// Covered  Persona_Ctrl
private static testMethod void Persona_Ctrl_UnitTest()
{
    test.startTest();
    Persona__c persona =  Util.createPersona();
    ApexPages.StandardController standardPersona = new ApexPages.StandardController(persona);
    Persona_Ctrl personaObj =  new Persona_Ctrl(standardPersona);
    personaObj.doCancel();
    personaObj.doDelete();
    personaObj.doSave();
    personaObj.doSaveAndNew();
    test.stopTest();
}

// Covered  Manufacturer_ctrl
private static testMethod void Manufacturer_ctrl_UnitTest()
{
    test.startTest();
    Manufacturer__c manufacturer =  Util.createManufacturer();
    ApexPages.StandardController standardManu = new ApexPages.StandardController(manufacturer);
    Manufacturer_ctrl manuObj =  new Manufacturer_ctrl(standardManu);

    manuObj.doCancel();
    manuObj.doDelete();
    manuObj.doSave();
    manuObj.doSaveAndNew();

    test.stopTest();
}

// Covered  feedbackpage_ctrl
private static testMethod void Feedbackpage_ctrl_UnitTest()
{
    test.startTest();

    Application__c application =  Util.createApplication();
    User useObj = Util.createUser();
    Application_Variants__c applicationvar = Util.createAppVariants(application.Id);
    Persona__c persona =  Util.createPersona();
    Manufacturer__c manufacturer =  Util.createManufacturer();
    Manufacturer__c manufacturerABX =  Util.createManufacturerABX();
    Product_Category__c prodCatObj =   Util.CreateProductCategory();
    Product2 abxPorduct = util.createProduct2(manufacturerABX.Id, prodCatObj.Id);
    Product2 comPorduct = util.createProduct2(manufacturer.Id, prodCatObj.Id);
    Package__c packageObj = util.CreatePackage(application.Id, applicationvar.Id, persona.Id, abxPorduct.Id, comPorduct.Id);
    Battlecard__c battlecardObj = util.createBattlecard(packageObj.Id);
    Rating_Management__c ratingObj = util.createRatingManagement(packageObj.Id, useObj.Id);

    feedbackpage_ctrl feddBackObj = new feedbackpage_ctrl();
    feddBackObj.sortedField = '';
    feddBackObj.getSortDirection();
    feddBackObj.doArchive();
    feddBackObj.setSortDirection('ASC');
    feddBackObj.sortThisColumn();

    test.stopTest();
}

// Covered  Ratingbattlecardwrapper
private static testMethod void Ratingbattlecardwrapper_UnitTest()
{
    test.startTest();

    Application__c application =  Util.createApplication();
    User useObj = Util.createUser();
    Application_Variants__c applicationvar = Util.createAppVariants(application.Id);
    Persona__c persona =  Util.createPersona();
    Manufacturer__c manufacturer =  Util.createManufacturer();
    Manufacturer__c manufacturerABX =  Util.createManufacturerABX();
    Product_Category__c prodCatObj =   Util.CreateProductCategory();
    Product2 abxPorduct = util.createProduct2(manufacturerABX.Id, prodCatObj.Id);
    Product2 comPorduct = util.createProduct2(manufacturer.Id, prodCatObj.Id);
    Package__c packageObj = util.CreatePackage(application.Id, applicationvar.Id, persona.Id, abxPorduct.Id, comPorduct.Id);
    Battlecard__c battlecardObj = util.createBattlecard(packageObj.Id);
    Rating_Management__c ratingObj = util.createRatingManagement(packageObj.Id, useObj.Id);


    Ratingbattlecardwrapper wrapperOb =  new Ratingbattlecardwrapper();
    Ratingbattlecardwrapper wrapperOb_New =  new Ratingbattlecardwrapper();
    list<Ratingbattlecardwrapper> wrapperList = new  list<Ratingbattlecardwrapper>();
    wrapperOb.battlename = 'test1';
    wrapperOb.comment = 'test2';
    wrapperOb.cp = 2.3;
    wrapperOb.demo = 3.2;
    wrapperOb.res = 6.5;
    wrapperOb.pp = 3.3;
    wrapperOb.pp = 3.3;
    wrapperOb.comment = 'test Summary';
    wrapperOb.dated = String.valueOf(date.today());
    wrapperOb.packageid = packageObj.Id;


    wrapperOb_New.battlename = 'test1';
    wrapperOb_New.comment = 'test2';
    wrapperOb_New.cp = 1.3;
    wrapperOb_New.demo = 3.2;
    wrapperOb_New.res = 4.5;
    wrapperOb_New.sum = 6.3;
    wrapperOb_New.pp = 6.3;
    wrapperOb_New.comment = 'test Summary';
    wrapperOb_New.dated = String.valueOf(date.today());
    wrapperOb_New.packageid = packageObj.Id;

    wrapperList.add(wrapperOb);
    wrapperList.add(wrapperOb_New);

    wrapperOb.battlename = battlecardObj.Battlecard_Name__c;

    wrapperOb.compareTo(battlecardObj);
    Ratingbattlecardwrapper.SortField = 'Battlecard_name__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();

    Ratingbattlecardwrapper.SortField = 'CP__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();

    Ratingbattlecardwrapper.SortField = 'PP__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();

    Ratingbattlecardwrapper.SortField = 'Demo__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();

    Ratingbattlecardwrapper.SortField = 'Summary__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();

    Ratingbattlecardwrapper.SortField = 'Additional_Resource_Rating__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();

    Ratingbattlecardwrapper.SortField = 'Comments__c';
    Ratingbattlecardwrapper.SortDirection = 'ASC';
    wrapperList.sort();
    Ratingbattlecardwrapper.SortDirection = 'DESC';
    wrapperList.sort();
    //wrapperOb.sortAscending(application);

    test.stopTest();
}

// Covered  Productcategory_ctrl
private static testMethod void Productcategory_ctrl_UnitTest()
{
    test.startTest();

    Product_Category__c prodcategoryObj =   Util.CreateProductCategory();

    ApexPages.StandardController standardProdCat = new ApexPages.StandardController(prodcategoryObj);
    Productcategory_ctrl prodCatObj = new Productcategory_ctrl(standardProdCat);
    prodCatObj.donext();
    prodCatObj.saveProduct_Fea_Cat();
    prodCatObj.doSaveAndNew();
    prodCatObj.doSave();
    prodCatObj.doCancel();
    prodCatObj.file1 = blob.valueOf('It is a test');
    prodCatObj.contentype = 'image/jpg';
    prodCatObj.upload();

    Product_Category__c prodCatObj1 = new Product_Category__c();
    prodCatObj1.isActive__c = true;
    insert prodCatObj1;

    prodCatObj.donext();

    test.stopTest();
}

// Covered  AddNewProductMeta_CTRL
private static testMethod void AddNewProductMeta_CTRL_UnitTest()
{
    test.startTest();

    Product_Category__c prodcategoryObj =   Util.CreateProductCategory();
    Specifications_Meta__c spec =util.CreateProductfeatureCategory(prodcategoryObj.id);
    Product_Meta__c prodMeta = Util.CreateProductfeature(spec.Id);

    ApexPages.currentPage().getParameters().put('Metaid',prodMeta.Id);
    ApexPages.currentPage().getParameters().put('specid',spec.Id);

    AddNewProductMeta_CTRL newMeatObj =  new AddNewProductMeta_CTRL();
    newMeatObj.prodmetalist = new List<Product_Meta__c>();
    newMeatObj.prodmetalist.add(prodMeta);
    newMeatObj.InsertProduct();
    newMeatObj.DeleteProduct();
    newMeatObj.RemoveProduct();
    //newMeatObj.manageOpenCategory();


    ApexPages.currentPage().getParameters().put('Metaid',null);
    ApexPages.currentPage().getParameters().put('specid',spec.Id);
    AddNewProductMeta_CTRL newMeatObj_2D =  new AddNewProductMeta_CTRL();


    AddNewProductMeta_CTRL newMeatObj_New =  new AddNewProductMeta_CTRL();
    newMeatObj_New.prodmetalist = new List<Product_Meta__c>();
    newMeatObj_New.prodmetalist.add(prodMeta);

    newMeatObj_New.Metaid = null;
    newMeatObj_New.SpecificID = spec.Id;
    newMeatObj_New.InsertProduct();

    AddNewProductMeta_CTRL newMeatObjMeta2 =  new AddNewProductMeta_CTRL();
    newMeatObjMeta2.prodmetalist = new List<Product_Meta__c>();
    newMeatObjMeta2.prodmetalist.add(prodMeta);

    newMeatObjMeta2.Metaid = prodMeta.Id;
    newMeatObjMeta2.SpecificID = null;
    newMeatObjMeta2.errflag = false;
    newMeatObjMeta2.InsertProduct();

    AddNewProductMeta_CTRL newMeatObjMeta3 =  new AddNewProductMeta_CTRL();
    newMeatObjMeta3.InsertProduct();
    newMeatObjMeta3.InsertMoreProduct();

    //EditCategory ed = new EditCategory();
    //ed.refreshCatList();
    //newMeatObjMeta3.manageOpenCategory();

    test.stopTest();
}

}