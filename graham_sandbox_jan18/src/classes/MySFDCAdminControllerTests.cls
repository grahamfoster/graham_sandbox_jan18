/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 14/2014
**
** Test coverage for MySFDCAdminController
** 
**/
@isTest
public class MySFDCAdminControllerTests {
	static List<Conversion_Rate__c> crates;
	static List<Quota__c> quotas;
    static User adminUser;
    
	public static testMethod void test1_conversionRate_update() {
		Test.startTest();
		setUp();
		Test.stopTest();

        system.runAs(adminUser)
		{
			MySFDCAdminController c = new MySFDCAdminController();
			c.init();
			boolean b1 = c.editConversionRates;
			boolean b2 = c.editQuotas;
			boolean b3 = c.conversionRates[0].renderEditButton;
			boolean b4 = c.conversionRates[0].renderSaveButton;
			boolean b5 = c.conversionRates[0].renderCancelButton;
			boolean b6 = c.conversionRates[0].renderNewButton;
			c.selectedConversionRateId = crates[0].Id;
			c.editConversionRate();
			c.cancelEditConversionRate();
			c.selectedConversionRateId = crates[0].Id;
			c.editConversionRate();
			c.conversionRates[0].conversionRate.Conversion_Rate__c = 90;
			c.saveConversionRate();

			system.assertEquals(1, ApexPages.getMessages().size());
			system.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
			system.assertEquals(90, [select Conversion_Rate__c from Conversion_Rate__c where Id = :crates[0].Id].Conversion_Rate__c);
        }
     
	}

	public static testMethod void test2_conversionRate_insert() {
		Test.startTest();
		setUp();
		Test.stopTest();
        system.runAs(adminUser)
		{
			MySFDCAdminController c = new MySFDCAdminController();
			c.init();
			c.conversionRates.add(new MySFDCAdminController.ConversionRateWrapper());
			c.addConversionRate();
			c.cancelEditConversionRate();
			c.addConversionRate();
			c.conversionRates[1].conversionRate.Conversion_Rate__c = 65;
			c.conversionRates[1].conversionRate.Opportunity_Type__c = 'Upsell Existing';
			c.saveConversionRate();

			for(ApexPages.Message msg : ApexPages.getMessages()) {
				system.debug(msg.getSeverity() + ':' + msg.getDetail());
			}
			system.assertEquals(1, ApexPages.getMessages().size());
			system.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
			system.assertEquals(65, [select Conversion_Rate__c from Conversion_Rate__c where Opportunity_Type__c = 'Upsell Existing'].Conversion_Rate__c);
        }
	}

	public static testMethod void test3_dmlexception() {
		Test.startTest();
		setUp();
		Test.stopTest();
        
        system.runAs(adminUser)
		{
			MySFDCAdminController c = new MySFDCAdminController();
			c.init();
			c.conversionRates.add(new MySFDCAdminController.ConversionRateWrapper());
			c.addConversionRate();
			c.cancelEditConversionRate();
			c.addConversionRate();
			c.conversionRates[1].conversionRate.Conversion_Rate__c = 65;
			c.conversionRates[1].conversionRate.Opportunity_Type__c = 'Forecast';
			c.conversionRates[1].conversionRate.Sector__c = 'Non-Service';
			c.saveConversionRate();

			for(ApexPages.Message msg : ApexPages.getMessages()) {
				system.debug(msg.getSeverity() + ':' + msg.getDetail());
			}
			system.assertEquals(1, ApexPages.getMessages().size());
			system.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        }
	}

	public static testMethod void test1_quota_update() {
		Test.startTest();
		setUp();
		Test.stopTest();

		MySFDCAdminController c = new MySFDCAdminController();
		c.init();
		boolean b3 = c.quotas[0].renderEditButton;
		boolean b4 = c.quotas[0].renderSaveButton;
		boolean b5 = c.quotas[0].renderCancelButton;
		boolean b6 = c.quotas[0].renderNewButton;
		c.selectedQuotaId = quotas[0].Id;
		c.editQuota();
		c.cancelEditQuota();
		c.selectedQuotaId = quotas[0].Id;
		c.editQuota();
		c.quotas[0].quota.Quota__c = 1000000;
		c.saveQuota();

		system.assertEquals(1, ApexPages.getMessages().size());
		system.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
		system.assertEquals(1000000, [select Quota__c from Quota__c where Id = :quotas[0].Id].Quota__c);
	}

	public static testMethod void test2_quota_insert() {
		Test.startTest();
		setUp();
		Test.stopTest();

		MySFDCAdminController c = new MySFDCAdminController();
		c.init();
		c.addQuota();
		c.cancelEditQuota();
		c.addQuota();
		c.quotas[1].quota.Quota__c = 1200000;
		c.quotas[1].quota.Fiscal_Year__c = '2014';
		c.quotas[1].quota.Fiscal_Quarter__c = '3';
		c.saveQuota();

		for(ApexPages.Message msg : ApexPages.getMessages()) {
			system.debug(msg.getSeverity() + ':' + msg.getDetail());
		}
		system.assertEquals(1, ApexPages.getMessages().size());
		system.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
		system.assertEquals(1200000, [select Quota__c from Quota__c where Fiscal_Year__c = '2014' and Fiscal_Quarter__c = '3'].Quota__c);
	}

	public static testMethod void test3_quota_dmlexception() {
		Test.startTest();
		setUp();
		Test.stopTest();

		MySFDCAdminController c = new MySFDCAdminController();
		c.init();
		c.addQuota();
		c.cancelEditQuota();
		c.addQuota();
		c.quotas[1].quota.Quota__c = 1200000;
		c.quotas[1].quota.Fiscal_Year__c = '2014';
		c.quotas[1].quota.Fiscal_Quarter__c = '2';
		c.saveQuota();

		for(ApexPages.Message msg : ApexPages.getMessages()) {
			system.debug(msg.getSeverity() + ':' + msg.getDetail());
		}
		system.assertEquals(1, ApexPages.getMessages().size());
		system.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
	}

	private static void setUp(){
        adminUser = createUser('testsvc', 'System Administrator');
		adminUser.License_Department__c = 'IT';
        adminUser.License_Region__c = 'Americas';
		update adminUser;
        
		crates = new List<Conversion_Rate__c>{
			new Conversion_Rate__c(Region__c = 'Americas', Conversion_Rate__c = 95, Opportunity_Type__c = 'Forecast', Sector__c= 'Non-Service')
		};
		insert crates;

		quotas = new List<Quota__c>{
			new Quota__c(Fiscal_Year__c = '2014', Fiscal_Quarter__c = '2')
		};
		insert quotas;
	}
    
    private static User createUser(String alias, String profileName)
	{
		Profile p = [select Id from Profile where Name = :profileName];
		User u = new User(alias = alias,email=alias+'@example.com',emailencodingkey='UTF-8',lastname=alias, languagelocalekey='en_US',
		localesidkey='en_US',
		profileId = p.Id,
		timezonesidkey='America/New_York',
		username=alias+'@example.com');
		insert u;     
		return [select Id, Name from User where Id = :u.Id];
	}
}