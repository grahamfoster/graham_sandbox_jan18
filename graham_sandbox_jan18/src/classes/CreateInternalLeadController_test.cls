// ===========================================================================
// Object: CreateInternalLeadController_Test
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Test coverage for the standard controller extension CreateInternalLeadController
// ===========================================================================
// Changes: 2017-03-24 Reid Beckett
//           Class created
// ===========================================================================
@isTest
public class CreateInternalLeadController_test
{
    //test create lead from the Case record
	@isTest public static void test_ControllerCase()
    {
		Case testCase = [select Id from Case limit 1];
		
        ApexPages.currentPage().getParameters().put('id', testCase.Id);
        Test.setCurrentPage(Page.CreateInternalLeadCase);
		CreateInternalLeadController cont = new CreateInternalLeadController(new ApexPages.StandardController(testCase));
        cont.view();
        
        cont.businessUnit = 'Software Sales Contracts';
        cont.comments = 'test';
        cont.role = 'Sales';
        PageReference pageRef = cont.save();
        system.assertNotEquals(null, pageRef, ApexPages.getMessages());
    }
    
    //test create lead from the Contact record
	@isTest public static void test_ControllerContact()
    {
		Contact testContact = [select Id from Contact limit 1];
		
        ApexPages.currentPage().getParameters().put('id', testContact.Id);
        Test.setCurrentPage(Page.CreateInternalLeadContact);
		CreateInternalLeadController cont = new CreateInternalLeadController(new ApexPages.StandardController(testContact));
        cont.view();
        system.debug(cont.businessUnitOptions);
        system.debug(cont.sectionHeaderTitle);
        system.debug(cont.sectionHeaderSubtitle);
        system.debug(cont.pageBlockTitle);
        system.debug(cont.businessUnitLabel);
        system.debug(cont.commentsLabel);
        system.debug(cont.buttonLabel);
        
        cont.businessUnit = 'Software Sales Contracts';
        cont.comments = 'test';
        cont.role = 'Sales';        
        PageReference pageRef = cont.save();
        system.assertNotEquals(null, pageRef, ApexPages.getMessages());
    }

    //test errors - bad business unit name, and bad queue name
	@isTest public static void test_ControllerContact_error_no_biz_unit()
    {
		Contact testContact = [select Id from Contact limit 1];
		
        Test.setCurrentPage(Page.CreateInternalLeadContact);
        ApexPages.currentPage().getParameters().put('id', testContact.Id);
		CreateInternalLeadController cont = new CreateInternalLeadController(new ApexPages.StandardController(testContact));
        cont.view();
        cont.comments = 'test';
        cont.role = 'Sales';        
        PageReference pageRef = cont.save();
        system.assertEquals(null, pageRef);

        //cover bad business unit name
        cont.businessUnit = 'no custom setting for this';
        pageRef = cont.save();
        system.assertEquals(null, pageRef);

        //cover no queue found unit name
        cont.businessUnit = 'Software Sales Products';
        pageRef = cont.save();
        system.assertEquals(null, pageRef);
    }

    //test create lead from the Contact record
	@isTest public static void test_ControllerContact_existingLead1()
    {
        Lead matchingLead = new Lead(Email = 'johndoe@example.com', Company = 'Test Company', Status = 'Nurture', LastName = 'Doe');
        insert matchingLead;
        
		Contact testContact = [select Id from Contact limit 1];
		
        ApexPages.currentPage().getParameters().put('id', testContact.Id);
        Test.setCurrentPage(Page.CreateInternalLeadContact);
		CreateInternalLeadController cont = new CreateInternalLeadController(new ApexPages.StandardController(testContact));
        cont.view();
        system.debug(cont.businessUnitOptions);
        system.debug(cont.sectionHeaderTitle);
        system.debug(cont.sectionHeaderSubtitle);
        system.debug(cont.pageBlockTitle);
        system.debug(cont.businessUnitLabel);
        system.debug(cont.commentsLabel);
        system.debug(cont.buttonLabel);
        
        cont.businessUnit = 'Software Sales Contracts';
        cont.comments = 'test';
        cont.role = 'Sales';        
        PageReference pageRef = cont.save();
        system.assertNotEquals(null, pageRef, ApexPages.getMessages());
    }

    //test create lead from the Contact record
	@isTest public static void test_ControllerContact_existingLead2()
    {
        Lead matchingLead = new Lead(Email = 'johndoe@example.com', Company = 'Test Company', Status = 'Open', LastName = 'Doe');
        insert matchingLead;
        
		Contact testContact = [select Id from Contact limit 1];
		
        ApexPages.currentPage().getParameters().put('id', testContact.Id);
        Test.setCurrentPage(Page.CreateInternalLeadContact);
		CreateInternalLeadController cont = new CreateInternalLeadController(new ApexPages.StandardController(testContact));
        cont.view();
        system.debug(cont.businessUnitOptions);
        system.debug(cont.sectionHeaderTitle);
        system.debug(cont.sectionHeaderSubtitle);
        system.debug(cont.pageBlockTitle);
        system.debug(cont.businessUnitLabel);
        system.debug(cont.commentsLabel);
        system.debug(cont.buttonLabel);
        
        cont.businessUnit = 'Software Sales Contracts';
        cont.comments = 'test';
        cont.role = 'Sales';        
        PageReference pageRef = cont.save();
        system.assertNotEquals(null, pageRef, ApexPages.getMessages());
    }

    //set up the test data
    @testSetup static void setUp() {
        List<CreateInternalLeadBusinessUnit__c> bizUnits = new List<CreateInternalLeadBusinessUnit__c> {
            new CreateInternalLeadBusinessUnit__c(Name = 'Software Sales Contracts', Lead_Assignment_Queue_Name__c = 'Test Campaign'),
            new CreateInternalLeadBusinessUnit__c(Name = 'Software Sales Products', Lead_Assignment_Queue_Name__c = 'Non-existent Queue')
		};
        insert bizUnits;
        
        Campaign testCampaign1 = new Campaign(IsActive = true, Name = 'Test Campaign');
		insert testCampaign1;
        
        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;
        
        Schema.RecordTypeInfo rtInfo = Contact.SObjectType.getDescribe().getRecordTypeInfosByName().get('AMERICAS Contact Record');
        Contact c = new Contact(AccountId = a.Id, FirstName = 'John', LastName = 'Doe', Email = 'johndoe@example.com', RecordTypeId = rtInfo.getRecordTypeId());
        insert c;
        
		String caseType = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
		Case testCase = new Case(AccountId = a.Id, Type = caseType, Subject = 'Test Coverage', Origin = 'Email to Case', ContactId = c.Id);
        insert testCase;
    }
}