public without sharing class SVMX_WorkOrder_Helper {
	
	public static void insertDailyTravel(List<SVMXC__Service_Order_Line__c> newList) 
	{
		Map<String,SVC_Zone_Travel_Charge_Countries__c> countryMap = SVC_Zone_Travel_Charge_Countries__c.getall();


        Schema.DescribeSObjectResult svmxwd = Schema.SObjectType.SVMXC__Service_Order_Line__c ;
        Map<String,Schema.RecordTypeInfo> rtMapByNamewd = svmxwd.getRecordTypeInfosByName();
        String wdIdUsage = rtMapByNamewd.get('Usage/Consumption').getRecordTypeId();
        Map<Id, SVMXC__Service_Order_Line__c> woTravelZoneChargesMap = new Map<Id, SVMXC__Service_Order_Line__c>();
        Map<String, SVMXC__Service_Order_Line__c> woTravelDailyChargesMap = new Map<String, SVMXC__Service_Order_Line__c>();
		List<SVMXC__Service_Order_Line__c> updateWDList = new List<SVMXC__Service_Order_Line__c>();

		Map<String, String> prodCodeMap = new Map<String, String>();
		Set<String> prodCodeSet = new Set<String>();
		List<SVC_Zone_Travel_Charge_Product_Map__c> mcs = SVC_Zone_Travel_Charge_Product_Map__c.getall().values();
		Set<Id> productIdSet = new Set<Id>();

		prodCodeSet.add('SV000029');  //Daily Travel Charge
		for (SVC_Zone_Travel_Charge_Product_Map__c ztc : mcs)
		{
			prodCodeSet.add(ztc.SVC_Product_Code__c);
		}

		Set<Id> woSet = new Set<Id>();
		
		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			woSet.add(wd.SVMXC__Service_Order__c);
			productIdSet.add(wd.SVMXC__Product__c);
		}

		Map<Id, Product2> productMap = new Map<Id, Product2>([Select Id, Name from Product2 where Id in :productIdSet]);

		//List<SVMXC__Service_Order_Line__c> travelZoneChargesList = [Select Id, SVMXC__Service_Order__c, SVMXC__Start_Date_and_Time__c from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in :woSet and SVMXC__Product__r.Name like 'Zone%Travel Charge'];
		List<SVMXC__Service_Order_Line__c> travelZoneChargesList = [Select Id, SVMXC__Service_Order__c, SVMXC__Start_Date_and_Time__c, SVMXC__Product__r.Name from SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in :woSet and Line_Sub_Type__c = 'Travel'];		

		if (travelZoneChargesList != null)
		{
			for (SVMXC__Service_Order_Line__c wd : travelZoneChargesList)
			{
				if (wd.SVMXC__Product__r.Name != null && wd.SVMXC__Product__r.Name.startsWith('ZONE'))
					woTravelZoneChargesMap.put(wd.SVMXC__Service_Order__c, wd);
				else if (wd.SVMXC__Product__r.Name != null && wd.SVMXC__Product__r.Name.equalsIgnoreCase('SV Daily Travel') && wd.SVMXC__Start_Date_and_Time__c != null)
				{
					woTravelDailyChargesMap.put(wd.SVMXC__Service_Order__c + '|' + wd.SVMXC__Start_Date_and_Time__c.date(), wd);
				}
			}
		}

		Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([Select Id, SVMXC__Component__c, SVMXC__Component__r.SVMXC__Country__c, SVC_Serv_Request_Type__c,SVMXC__Component__r.SVC_Service_Zone__c, SVMXC__Component__r.SVMXC__Product__r.SVC_Division_Products__c from SVMXC__Service_Order__c where Id in :woSet]);

		List<Product2> travelZoneProducts = [Select Id, ProductCode, SVC_Division_Products__c from Product2 where ProductCode in :prodCodeSet];

		for (Product2 prod : travelZoneProducts)
		{
			prodCodeMap.put(prod.ProductCode + '|' + prod.SVC_Division_Products__c, prod.Id);
		}

		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			SVMXC__Service_Order__c wo = woMap.get(wd.SVMXC__Service_Order__c);

			if (wo != null && wo.SVMXC__Component__r.SVMXC__Country__c != null && countryMap.containsKey(wo.SVMXC__Component__r.SVMXC__Country__c))  //Sandip added 9/10/2015 per new requirement from Melynda
			{

				if (wo != null && wo.SVMXC__Component__c != null && productMap.get(wd.SVMXC__Product__c) != null && productMap.get(wd.SVMXC__Product__c).Name == 'Non Billable Travel')
				{
					//wd.SVMXC__Is_Billable__c = false;  //sandip removed 8/21
					String travelZone = wo.SVMXC__Component__r.SVC_Service_Zone__c;
					if (travelZone != null)
					{
						SVC_Zone_Travel_Charge_Product_Map__c cs = SVC_Zone_Travel_Charge_Product_Map__c.getInstance(travelZone);
						if (cs != null && woTravelZoneChargesMap.get(wd.SVMXC__Service_Order__c) == null)
						{

							SVMXC__Service_Order_Line__c wdNew = new SVMXC__Service_Order_Line__c();
							wdNew.SVMXC__Service_Order__c = wo.Id;
							wdNew.SVMXC__Product__c = prodCodeMap.get(cs.SVC_Product_Code__c + '|' + wo.SVMXC__Component__r.SVMXC__Product__r.SVC_Division_Products__c);
							wdNew.SVMXC__Actual_Quantity2__c = 1;
							wdNew.SVMXC__Line_Type__c = 'Parts';
							wdNew.Line_Sub_Type__c = 'Travel';
							wdNew.SVMXC__Start_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c;
							wdNew.RecordTypeId = wdIdUsage;
							wdNew.SVMXC__Actual_Quantity2__c = 1;
							wdNew.CurrencyIsoCode = wd.CurrencyIsoCode;
							wdNew.SVMXC__Group_Member__c = wd.SVMXC__Group_Member__c;
							wdNew.SVC_Auto_Generated_Travel_Line__c = true;
							wdNew.SVMXC__Work_Detail__c = wd.Id;

							if (wo.SVC_Serv_Request_Type__c != null && (wo.SVC_Serv_Request_Type__c == 'ABSX Other' || wo.SVC_Serv_Request_Type__c == 'ABSX Install' || wo.SVC_Serv_Request_Type__c == 'ABSX Entitled'))
								wdNew.SVMXC__Activity_Type__c = 'Non Billable Travel';
							else
								wdNew.SVMXC__Activity_Type__c = 'Billable Travel';


							if (wd.SVMXC__Start_Date_and_Time__c != null && wd.SVMXC__Actual_Quantity2__c != null)
							{
								Integer minutes = (wd.SVMXC__Actual_Quantity2__c * 60).intValue();
								wdNew.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addMinutes(minutes);
							}

							updateWDList.add(wdNew);

							woTravelZoneChargesMap.put(wd.SVMXC__Service_Order__c, wdNew);	
						}
						else if (cs != null && woTravelZoneChargesMap.get(wd.SVMXC__Service_Order__c) != null && 
							     	woTravelDailyChargesMap.get(wd.SVMXC__Service_Order__c + '|' + wd.SVMXC__Start_Date_and_Time__c.date()) == null)
						{
							
							SVMXC__Service_Order_Line__c tzwd = woTravelZoneChargesMap.get(wd.SVMXC__Service_Order__c);

							if (tzwd.SVMXC__Start_Date_and_Time__c != null && wd.SVMXC__Start_Date_and_Time__c != null && tzwd.SVMXC__Start_Date_and_Time__c.date() != wd.SVMXC__Start_Date_and_Time__c.date())
							{
								SVMXC__Service_Order_Line__c wdNew = new SVMXC__Service_Order_Line__c();
								wdNew.SVMXC__Service_Order__c = wo.Id;
								wdNew.SVMXC__Product__c = prodCodeMap.get('SV000029' + '|' + wo.SVMXC__Component__r.SVMXC__Product__r.SVC_Division_Products__c);
								wdNew.SVMXC__Actual_Quantity2__c = 1;
								wdNew.SVMXC__Line_Type__c = 'Parts';
								wdNew.Line_Sub_Type__c = 'Travel';
								wdNew.SVMXC__Start_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c;
								wdNew.SVMXC__Actual_Quantity2__c = 1;
								wdNew.CurrencyIsoCode = wd.CurrencyIsoCode;
								wdNew.SVMXC__Group_Member__c = wd.SVMXC__Group_Member__c;
								wdNew.SVC_Auto_Generated_Travel_Line__c = true;
								wdNew.SVMXC__Work_Detail__c = wd.Id;

								if (wo.SVC_Serv_Request_Type__c != null && (wo.SVC_Serv_Request_Type__c == 'ABSX Other' || wo.SVC_Serv_Request_Type__c == 'ABSX Install' || wo.SVC_Serv_Request_Type__c == 'ABSX Entitled'))
									wdNew.SVMXC__Activity_Type__c = 'Non Billable Travel';
								else
									wdNew.SVMXC__Activity_Type__c = 'Billable Travel';


								if (wd.SVMXC__Start_Date_and_Time__c != null && wd.SVMXC__Actual_Quantity2__c != null)
								{
									Integer minutes = (wd.SVMXC__Actual_Quantity2__c * 60).intValue();
									wdNew.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addMinutes(minutes);
								}

								wdNew.RecordTypeId = wdIdUsage;

								updateWDList.add(wdNew);

								woTravelDailyChargesMap.put(wdNew.SVMXC__Service_Order__c + '|' + wdNew.SVMXC__Start_Date_and_Time__c.date(), wdNew);
							}
						}

					}

				}
			}
		}

		if (!updateWDList.isEmpty())
			insert updateWDList;
	}


	public static void performLocationBasedEntitlement(List<SVMXC__Service_Order__c> woList)
	{
		Set<Id> woSet = new Set<Id>();



		for (SVMXC__Service_Order__c wo : woList)
		{
			if (wo.SVMXC__Is_Entitlement_Performed__c == true && (wo.SVMXC__Auto_Entitlement_Status__c == 'Failed')) // && (wo.SVC_Serv_Request_Type__c != 'ABSX Install' && wo.SVC_Serv_Request_Type__c != 'ABSX Other')) // || wo.SVC_Service_Request_Type__c == 'ABSX Install' || wo.SVC_Service_Request_Type__c == 'ABSX Other'))
			{
				woSet.add(wo.Id);
			}
		}

		List<SVMXC__Service_Order__c> woNonEntitledList = [Select Id, SVC_Serv_Request_Type__c, SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__c, SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__r.Name, SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__r.SVMXC__Start_Date__c, SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__r.SVMXC__End_Date__c from SVMXC__Service_Order__c where Id in :woSet];
		List<SVMXC__Entitlement_History__c> entitlementNotesList = new List<SVMXC__Entitlement_History__c>();
		for (SVMXC__Service_Order__c wo : woNonEntitledList)
		{
			if (wo.SVC_Serv_Request_Type__c != 'ABSX Install' && wo.SVC_Serv_Request_Type__c != 'ABSX Other')
			{
				wo.SVMXC__Auto_Entitlement_Status__c = 'Success';
				wo.SVMXC__Service_Contract__c = wo.SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__c;
				wo.SVMXC__Entitlement_Notes__c = 'Entitled by ServiceMax Location-Based Entitlement Engine.\nService Contract: ' + wo.SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__r.Name + '\n*** Default Time and Materials Billing (No Valid Customer Contract) ***';
				wo.SVMXC__Is_Entitlement_Performed__c = true;

				SVMXC__Entitlement_History__c sh = new SVMXC__Entitlement_History__c();
				sh.SVMXC__Service_Contract__c = wo.SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__c;
				sh.SVMXC__Start_Date__c = wo.SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__r.SVMXC__Start_Date__c;
				sh.SVMXC__End_Date__c = wo.SVMXC__Component__r.SVMXC__Site__r.SVC_Default_Service_Contract__r.SVMXC__End_Date__c;
				sh.SVMXC__Entitlement_notes__c = wo.SVMXC__Entitlement_Notes__c;
				sh.SVMXC__Date_of_entitlement__c = System.today();
				sh.SVMXC__Service_Order__c = wo.Id;
				entitlementNotesList.add(sh);
			}
			else
			{
				//wo.SVMXC__Auto_Entitlement_Status__c = 'Failed';
				//wo.SVMXC__Service_Contract__c = null;
				//wo.SVMXC__Entitlement_Notes__c = null;
				//wo.SVMXC__Is_Entitlement_Performed__c = true;
			}
		}

		if (!woNonEntitledList.isEmpty())
			update woNonEntitledList;
		if (!entitlementNotesList.isEmpty())
			insert entitlementNotesList;
	}

	public static void checkStopEntitlement(List<SVMXC__Service_Order__c> woList)
	{
		Set<Id> caseIDs = new Set<Id>();

		for (SVMXC__Service_Order__c wo : woList)
		{
			caseIds.add(wo.SVMXC__Case__c);
		}

		Map<Id, Case> caseMap = new Map<Id, Case>([Select Id, SVC_Service_Request_Type__c from Case where Id in :caseIds]);

		for (SVMXC__Service_Order__c wo : woList)
		{

			if (wo.SVMXC__Case__c != null && caseMap != null && caseMap.get(wo.SVMXC__Case__c) != null && (caseMap.get(wo.SVMXC__Case__c).SVC_Service_Request_Type__c == 'ABSX Install' || caseMap.get(wo.SVMXC__Case__c).SVC_Service_Request_Type__c == 'ABSX Other'))
			{
				wo.SVMXC__Perform_Auto_Entitlement__c = false;
				wo.SVMXC__Auto_Entitlement_Status__c = 'Failed';
				wo.SVMXC__Service_Contract__c = null;
				wo.SVMXC__Warranty__c = null;
				wo.SVMXC__Entitlement_Notes__c = null;
				wo.SVMXC__Is_Entitlement_Performed__c = true;
				//woIDs.add(wo.Id);
			}
		}

	}

	public static void checkUpdatedTravel(List<SVMXC__Service_Order_Line__c> newList, Map<Id, SVMXC__Service_Order_Line__c> oldMap)
	{
		List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c>();
		Set<Id> wdIds = new Set<Id>();
		Map<Id, SVMXC__Service_Order_Line__c> autoCreatedWDMap = new Map<Id, SVMXC__Service_Order_Line__c>();

		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			if (wd.Line_Sub_Type__c == 'Travel' && (wd.SVMXC__Start_Date_and_Time__c != oldMap.get(wd.Id).SVMXC__Start_Date_and_Time__c || wd.SVMXC__Actual_Quantity2__c != oldMap.get(wd.Id).SVMXC__Actual_Quantity2__c))
			{
				wdIds.add(wd.Id);
			}
		}

		if (!wdIds.isEmpty())
		{
			List<SVMXC__Service_Order_Line__c> tzList = [Select Id, SVMXC__Start_Date_and_Time__c, SVMXC__End_Date_and_Time__c, SVMXC__Actual_Quantity2__c, SVMXC__Work_Detail__c from SVMXC__Service_Order_Line__c where SVMXC__Work_Detail__c in :wdIds];
			if (tzList != null)
			{
				for (SVMXC__Service_Order_Line__c wdNew : tzList)
				{
					autoCreatedWDMap.put(wdNew.SVMXC__Work_Detail__c, wdNew);
				}
			}
		}


		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			if (wd.Line_Sub_Type__c == 'Travel' && (wd.SVMXC__Start_Date_and_Time__c != oldMap.get(wd.Id).SVMXC__Start_Date_and_Time__c || wd.SVMXC__Actual_Quantity2__c != oldMap.get(wd.Id).SVMXC__Actual_Quantity2__c))
			{
				if (wd.SVMXC__Start_Date_and_Time__c != null && wd.SVMXC__Actual_Quantity2__c != null)
				{
					Integer minutes = (wd.SVMXC__Actual_Quantity2__c * 60).intValue();
					SVMXC__Service_Order_Line__c wdAuto = autoCreatedWDMap.get(wd.Id);
					if (wdAuto != null)
					{
						SVMXC__Service_Order_Line__c wdNew = new SVMXC__Service_Order_Line__c(Id = wdAuto.Id);
						if (wdNew != null)
						{
							wdNew.SVMXC__Start_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c;
							wdNew.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addMinutes(minutes);

							wdList.add(wdNew);
						}
					}

					//wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addMinutes(minutes);


				}
			}
		}

		if (!wdList.isEmpty())
			upsert wdList;
	}

	public static void setupTravelLines(List<SVMXC__Service_Order_Line__c> newList, Map<Id, SVMXC__Service_Order_Line__c> oldMap)
	{

		Set<Id> productIdSet = new Set<Id>();
		Set<Id> woSet = new Set<Id>();

		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			woSet.add(wd.SVMXC__Service_Order__c);
			productIdSet.add(wd.SVMXC__Product__c);
		}

		Map<Id, SVMXC__Service_Order__c> woMap = new Map<Id, SVMXC__Service_Order__c>([Select Id, SVMXC__Component__c, SVC_Serv_Request_Type__c,SVMXC__Component__r.SVC_Service_Zone__c, SVMXC__Component__r.SVMXC__Product__r.SVC_Division_Products__c from SVMXC__Service_Order__c where Id in :woSet]);
		Map<Id, Product2> productMap = new Map<Id, Product2>([Select Id, Name from Product2 where Id in :productIdSet]);

		for (SVMXC__Service_Order_Line__c wd : newList)
		{
			SVMXC__Service_Order__c wo = woMap.get(wd.SVMXC__Service_Order__c);

			if (wo != null && wo.SVMXC__Component__c != null && productMap.get(wd.SVMXC__Product__c) != null && productMap.get(wd.SVMXC__Product__c).Name == 'Non Billable Travel')
			{
				wd.SVMXC__Is_Billable__c = false;  
			}

			if (wd.Line_Sub_Type__c  == 'Travel' && wo.SVC_Serv_Request_Type__c != null && (wo.SVC_Serv_Request_Type__c == 'ABSX Other' || wo.SVC_Serv_Request_Type__c == 'ABSX Install' || wo.SVC_Serv_Request_Type__c == 'ABSX Entitled'))
			{
				wd.SVMXC__Activity_Type__c = 'Non Billable Travel';
			}
			else if (wd.Line_Sub_Type__c  == 'Travel')
			{
				wd.SVMXC__Activity_Type__c = 'Billable Travel';
			}

			if (wd.Line_Sub_Type__c == 'Travel' && (Trigger.isInsert || (oldMap.get(wd.Id) != null && (wd.SVMXC__Start_Date_and_Time__c != oldMap.get(wd.Id).SVMXC__Start_Date_and_Time__c || wd.SVMXC__Actual_Quantity2__c != oldMap.get(wd.Id).SVMXC__Actual_Quantity2__c))))
			{
				if (wd.SVMXC__Start_Date_and_Time__c != null && wd.SVMXC__Actual_Quantity2__c != null)
				{
					Integer minutes = (wd.SVMXC__Actual_Quantity2__c * 60).intValue();
					wd.SVMXC__End_Date_and_Time__c = wd.SVMXC__Start_Date_and_Time__c.addMinutes(minutes);
				}
			}

		}



	}




}