/**
** @author Reid Beckett, Cloudware Connections
** @created July 14/2014
**
** Refactored out of MySFDCController
** 
**/
public virtual class GoogleGaugeChartData {
	public String id {get;set;}
	public String title {get;set;}
	public String gaugeLabel {get;set;}
	public Decimal quota {get;set;}
	public Decimal actual {get;set;}

	public GoogleGaugeChartData(String id){
		this.id = id;
	}

	public virtual Decimal getScaledQuota() {
		return quota != null ? quota/1000000.0 : 0;
	}

	public virtual Decimal getScaledActual() {
		return actual != null ? actual/1000000.0 : 0;
	}

	public virtual Decimal[] getTicks() {
		if(quota != null) {
			return new Decimal[] {
				(Math.round((quota/1000000.0)*0.6*1000)/1000.0),
				(Math.round((quota/1000000.0)*0.8*1000)/1000.0),
				(Math.round((quota/1000000.0)*1000)/1000.0),
				(Math.round((quota/1000000.0)*1.2*1000)/1000.0)
			};
		}else return new Decimal[]{};
	}

	public virtual Decimal getRedFrom(){
		Decimal[] ticks = getTicks();
		return ticks.size() > 0 ? ticks[0] : 0;
	}

	public virtual Decimal getRedTo(){
		Decimal[] ticks = getTicks();
		return ticks.size() > 1 ? ticks[1] : 0;
	}

	public virtual Decimal getYellowFrom(){
		Decimal[] ticks = getTicks();
		return ticks.size() > 1 ? ticks[1] : 0;
	}

	public virtual Decimal getYellowTo(){
		Decimal[] ticks = getTicks();
		return ticks.size() > 2 ? ticks[2] : 0;
	}

	public virtual Decimal getGreenFrom(){
		Decimal[] ticks = getTicks();
		return ticks.size() > 2 ? ticks[2] : 0;
	}

	public virtual Decimal getGreenTo(){
		Decimal[] ticks = getTicks();
		return ticks.size() > 3 ? ticks[3] : 0;
	}

	public virtual String getTotalNumberFormat(){
		return '$#0.000M';
	}

	public virtual String getNumberFormatSuffix() {
		return '';
	}
}