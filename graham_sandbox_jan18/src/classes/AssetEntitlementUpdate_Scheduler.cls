global class AssetEntitlementUpdate_Scheduler implements Schedulable {
	/**
	 * @description Executes the scheduled Apex job. 
	 * @param sc contains the job ID
	 */ 
	global void execute(SchedulableContext sc) {
		AssetEntitlementUpdate upd = new AssetEntitlementUpdate();
		Database.executeBatch(upd);
	}
}