public with sharing class Product_Ctrl{
    public Product2 productRecord{get;set;}
    public List<Product_Meta__c> prodFeaturesList{get;set;}
    public List<ProductMetaWrapper> metaWrapperList{get;set;}
    Product_Value__c prodValueObj;
    List<Product_Value__c> prodValueList = new List<Product_Value__c>(); 
    List<Product_Value__c> prodValueWrapperList = new List<Product_Value__c>(); 
    List<Product_Value__c> prodValueWrapperListup = new List<Product_Value__c>(); 
    Attachment AttchObj = new Attachment();
    public string attachid{get;set;}
    public  String prodCategoryId{get;set;}
    public String message{get;set;}
    public List<ProductMetaWrapperup> metaWrapperListup{get;set;}
    
    
    // Code for the picklist values starts here ---------------
    
    public string recordTypeName{get;set;}
    public string brandName{get;set;}
    public string familyName{get;set;}
    List<SelectOption> options{get;set;}
    public opportunity oppList{get;set;}
    public string checkup{get;set;}
    public string checkup1{get;set;}
    public string displaymessage{get;set;}
    /*
    public Dependentpicklist(){
        oppList = new Opportunity();
        oppList.name = 'Test';
        oppList.Type = 'New Business';
        oppList.StageName = 'Deal Lost';
        oppList.Amount = 10;
        oppList.CloseDate = Date.Today(); 
        checkup = 'false';
        checkup1 = 'false';
    }    */
    
    public void check(){
        system.debug('test1>>>'+recordTypename);
        if(recordTypename == 'AB Sciex' || recordTypename== 'NA PSM'){
            //brandName
            //familyName
            checkup = 'true';
            checkup1 = 'false';
            displaymessage = 'false';
            system.debug('test1>>>'+checkup);
            system.debug('test1>>>'+checkup1);             
        }
        else if (recordTypename == 'Competitor'){
            checkup1 = 'true';
            checkup = 'false';
             displaymessage = 'false';
        }       
        else if (recordTypename == 'None') {
            checkup1 = 'false';
            checkup = 'false';
            displaymessage = 'true';
        }
    }
    
    public string getcheckup(){
        return checkup;
    }
    
    public string getcheckup1(){
        return checkup1;
    }    
            
    Public List<SelectOption> getrecordTypes(){
        List<RecordType> rt = [Select name from recordtype where sobjecttype = 'Product2'];    
        options = new List<SelectOption>();
        options.add(new SelectOption('None','--- None ---')); 
        if(!rt.isEmpty())
        { 
            for(Recordtype r:rt){                 
                options.add(new SelectOption(r.name,r.name));
            }       
        }
        return options;
    }
    
    public List<SelectOption> getBrand(){
      options = new List<SelectOption>();
      Schema.sObjectType objType = Product2.getSObjectType();        
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();             
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
      list<Schema.PicklistEntry> values = fieldMap.get('Brand__c').getDescribe().getPickListValues();      
      system.debug('values>>>'+values);
      for (Schema.PicklistEntry a : values)
      {          
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }      
      return options;
    }
    
    public List<SelectOption> getfamily(){
      options = new List<SelectOption>();
      Schema.sObjectType objType = Product2.getSObjectType();        
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();             
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
      list<Schema.PicklistEntry> values = fieldMap.get('Family').getDescribe().getPickListValues();      
      system.debug('values>>>'+values);
      for (Schema.PicklistEntry a : values)
      {          
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }      
      return options;
    }
    
    public void ok(){     
        system.debug('test>>'+oppList.Select_Competitor_that_Won_Order__c);
        system.debug('test1>>'+oppList.Competitor_Instrument_Type__c);
    }
    
    // Code for the picklist values ends here ---------------
    
    
    
    //===============================================   
    public transient Attachment attach {get;set;}
    public string filetitle{get;set;}
    public transient blob file1{get;set;}
    public String fname{get;set;}   
    public string contentype{get;set;} 
    public Boolean isComplete{get; private set;}
       
    public Product_Ctrl(ApexPages.StandardController controller) 
    {   
        oppList = new Opportunity();
        oppList.name = 'Test';
        oppList.Type = 'New Business';
        oppList.StageName = 'Deal Lost';
        oppList.Amount = 10;
        oppList.CloseDate = Date.Today(); 
        checkup = 'false';
        checkup1 = 'false';
        displaymessage = 'false';
        
        metaWrapperListup = new List<ProductMetaWrapperup>();
        isComplete = false;
        prodCategoryId = ApexPages.currentPage().getParameters().get('catid');
        productRecord = (Product2)Controller.getRecord();
        System.debug('productRecord...' + productRecord);
        Set<ID> existingMetaIds = new Set<ID>();
        
        if(productRecord != null && productRecord.Id != null)
        {       
                metaWrapperListup = new List<ProductMetaWrapperup>();
                productRecord = [select RecordType.Name, Competitor_Model__c,Competitor_Manufacturer__c,family,Brand__c,Manufacturer__c,image__c,Model__c,position__c,Name,Last_Update__c,Description,LastModifiedDate from Product2 where id=:productRecord.Id];
                if(productRecord != null){
                    recordTypeName = productRecord.RecordType.Name;
                    if(recordTypename == 'AB Sciex' || recordTypename== 'NA PSM'){
                        brandName = productRecord.Brand__c;
                        familyName = productRecord.family;
                        checkup = 'true';
                        checkup1 = 'false';
                        displaymessage = 'false';                               
                    }
                    else if (recordTypename == 'Competitor'){
                        oppList.Select_Competitor_that_Won_Order__c = productRecord.Competitor_Manufacturer__c;
                        oppList.Competitor_Instrument_Type__c = productRecord.Competitor_Model__c;
                        checkup1 = 'true';
                        checkup = 'false';
                         displaymessage = 'false';
                    }                   
                }
                system.debug('oppList++'+oppList);
                List<Product_Value__c> prodValuelist=[select isActive__c,Product__c,Product_Meta__c,Product_Meta_Formula__c,Product_Value_Name__c,Value__c from Product_Value__c where Product__c=:productRecord.Id];
                System.debug('####valueList' + prodValuelist);
                
                
                List<Product_Meta__c> pmetalist = [Select isActive__c,id, Product_Meta_Name__c From Product_Meta__c where Specifications_Meta__c!='' order by CreatedDate ASC]; 
                 //List<Product_Meta__c> pmetalist = [Select isActive__c,id, Product_Meta_Name__c From Product_Meta__c where isActive__c = true AND Specifications_Meta__c!='' order by CreatedDate ASC];
              
                for(Product_Meta__c  pm: pmetalist) {
                    for(Product_Value__c p:prodValuelist ){
                        if(p.Product_Meta__c == pm.Id) {         //if(p.Product_Meta_Formula__c == pm.Product_Meta_Name__c)
                            metaWrapperListup.add(new ProductMetaWrapperup(pm,p));
                            existingMetaIds.add(pm.Id);
                        }
                    }
                }
            
                if(productRecord.id != null){
                    List<Attachment> att = new List<Attachment>();
                    att= [select id,Name , ContentType, body from Attachment where ParentId = :productRecord.id];  
                    if(att.size()>0){
                        system.debug('$$$'+att[0]);
                        system.debug('$$$'+att[0].Name);
                        system.debug('$$$'+fName);
                        filetitle = att[0].Name;                        
                        system.debug('$$$'+filetitle);
                        
                    }
                }                           
        }
        System.debug('metaWrapperListup --->' + metaWrapperListup);
        metaWrapperList = new List<ProductMetaWrapper>();
        System.debug('prodCategoryId --->' + prodCategoryId);
        
        
        
        if(prodCategoryId != null || prodCategoryId != '')
        {
           set<Specifications_Meta__c> productFeatureid = new set<Specifications_Meta__c>();
           /*List<Product_Category__c> prodCategoryList = [Select Product_Category_Name__c, 
                                                             (Select id From Specifications_Metas__r where isActive__c = true)
                                                                From Product_Category__c where id = : prodCategoryId and isActive__c = true];*/
            List<Product_Category__c> prodCategoryList = [Select Product_Category_Name__c, 
                                                             (Select id From Specifications_Metas__r)
                                                                From Product_Category__c where id = : prodCategoryId ];                                                    
            for(Product_Category__c pc :prodCategoryList)
            {
                if(!pc.Specifications_Metas__r.isEmpty())
                {
                    productFeatureid.addAll(pc.Specifications_Metas__r); 
                }
            } 
             System.debug('prodFeaturesList --->'+productFeatureid);
            prodFeaturesList = [Select isActive__c, Product_Meta_Name__c From Product_Meta__c where Specifications_Meta__c IN : productFeatureid order by CreatedDate ASC]; 
            //prodFeaturesList = [Select isActive__c, Product_Meta_Name__c From Product_Meta__c where Specifications_Meta__c IN : productFeatureid and isActive__c = true order by CreatedDate ASC];
            System.debug('prodFeaturesList --->'+prodFeaturesList);
            if(!prodFeaturesList.isEmpty())
            {
                // calling the Wrapper 
                for(Product_Meta__c pm :prodFeaturesList) {
                    if(!existingMetaIds.contains(pm.Id)){
                        metaWrapperList.add(new ProductMetaWrapper(pm));  
                    }                  
                }
                 System.debug('metaWrapperList --->'+metaWrapperList); 
                // for taking out only the list of ProductValues
                for(ProductMetaWrapper PMW: metaWrapperList){
                    prodValueWrapperList.add(PMW.ProdValue);
                } 
                System.debug('prodValueWrapperList --->'+prodValueWrapperList);                
            }                                                                                
        }
        // code for dependent picklist
       
    }
   
    public PageReference doSave(){
        try{
            isComplete = false;
            if(file1 == null){
                if(productRecord != null && productRecord.Id != null){
                    for(ProductMetaWrapperup PMW: metaWrapperListup){
                        prodValueWrapperListup.add(PMW.ProdValue);
                    } 
                    update prodValueWrapperListup;  
                }
                if(productRecord != null) {    
                   productRecord.Product_Category__c=prodCategoryId;
                   productRecord.IsActive = true;
                   if(recordTypename == 'None'){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Product Type')); displaymessage = 'true';
                        return null;
                   }
                   RecordType rt1 = [Select id from recordtype where sobjecttype = 'Product2' and name = :recordTypename]; 
                   if(rt1.Id != null){
                       productRecord.RecordTypeId = rt1.id; 
                   }                   
                    if(recordTypename == 'AB Sciex' || recordTypename== 'NA PSM'){
                        productRecord.Brand__c = brandName;
                        productRecord.Family = familyName;  
                        system.debug('test1>>>'+brandName);
                          system.debug('test1>>>'+familyName);                                  
                    }
                    else if (recordTypename == 'Competitor'){  productRecord.Competitor_Manufacturer__c = oppList.Select_Competitor_that_Won_Order__c;
                        productRecord.Competitor_Model__c = oppList.Competitor_Instrument_Type__c;
                        system.debug('test1>>>'+oppList.Select_Competitor_that_Won_Order__c);
                         system.debug('test1>>>'+oppList.Competitor_Instrument_Type__c);   
                    }
                   upsert productRecord;   
                }  
                for(Product_Value__c pvRec : prodValueWrapperList){
                    pvRec.Product__c = productRecord.Id;
                }   
                if(!prodValueWrapperList.isEmpty()){        
                    insert prodValueWrapperList;
                }     
            }
            if(file1 != null){    
                attach = new Attachment();
                attach.Name = fname ;
                attach.ContentType = contentype;
                attach.body=file1; 
                if (attach.Body.size() > 2000000 ) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Maximum Size Limit Exceeds! You can not upload more than 2MB.')); return null;
                }   
                if(attach != null && attach.Body.size() < 2000000 && (attach.ContentType == 'image/jpg' || attach.ContentType =='image/jpeg' || attach.ContentType == 'image/png' || attach.ContentType == 'image/gif' )){
                    try{
                        if(productRecord != null && productRecord.Id != null){
                                for(ProductMetaWrapperup PMW: metaWrapperListup){
                                    prodValueWrapperListup.add(PMW.ProdValue);
                                } 
                                System.debug('prodValueWrapperListup--->'+prodValueWrapperListup); 
                                update prodValueWrapperListup;  
                               // update productRecord;          
                           }
                            
                            
                            if(productRecord != null) {       //  && productRecord.id==null
                               productRecord.Product_Category__c=prodCategoryId;
                               productRecord.IsActive = true;
                               upsert productRecord;   
                            }  
                            attach.ParentId = productRecord.Id;     
                            upsert attach;
                            attachid = attach.id;
                         }
                         catch (DMLException e) {  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'+e.getMessage())); return null;
                         }finally {
                            attach = new Attachment(); 
                         }
                        system.debug('attachid....' +attachid);
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Product Saved successfully'));
                    }else{            
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Only .jpg/.jpeg/.png/.gif types of file for Image are supported.'));
                        file1 = null; 
                        attach=new Attachment();
                        return null;
                    }
                
                
                // to insert the image link in the image filed of the product =====
                if(attachid != null && attachid.trim() != '' && productRecord.id != null )
                {
                    productRecord.Image__c = '/servlet/servlet.FileDownload?file='+attachid;
                    system.debug('&&&&&&&&&&'+productRecord);
                    
                     // code added from here =================
	                    productRecord.Product_Category__c=prodCategoryId;
	                   productRecord.IsActive = true;
	                   if(recordTypename == 'None'){
	                   		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Product Type'));
	                   		displaymessage = 'true';
	                   		return null;
	                   }
	                   RecordType rt1 = [Select id from recordtype where sobjecttype = 'Product2' and name = :recordTypename]; 
	                   if(rt1.Id != null){
	                   	   productRecord.RecordTypeId = rt1.id;	
	                   }                   
	                    if(recordTypename == 'AB Sciex' || recordTypename== 'NA PSM'){
	                   		productRecord.Brand__c = brandName;
	                   		productRecord.Family = familyName;	
	                   		system.debug('test1>>>'+brandName);
	          				  system.debug('test1>>>'+familyName);   		        	            
				        }
				        else if (recordTypename == 'Competitor'){
				            productRecord.Competitor_Manufacturer__c = oppList.Select_Competitor_that_Won_Order__c;
	                   		productRecord.Competitor_Model__c = oppList.Competitor_Instrument_Type__c;
	                   		system.debug('test1>>>'+oppList.Select_Competitor_that_Won_Order__c);
	           				 system.debug('test1>>>'+oppList.Competitor_Instrument_Type__c);   
				        }
			        //==============================
                    
                    update productRecord;
                }
                
                
                for(Product_Value__c pvRec : prodValueWrapperList){
                    pvRec.Product__c = productRecord.Id;
                }   
                System.debug('prodValueWrapperList--->'+prodValueWrapperList); 
                
                if(!prodValueWrapperList.isEmpty()){        
                    insert prodValueWrapperList;
                }
                
                             
            }
             isComplete = true;
        }
        catch(Exception e){
            isComplete = false; 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); 
            message ='Whoops! An error occurred -- ' + e.getMessage();  
            System.debug('testing of message----->'+message) ;
            file1 = null; 
            attach=new Attachment(); 
        }
        return null;
    }
    
    
    public PageReference doCancel() {
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
            return new PageReference (cancelURL);
        }
        return null;    
    }
    
    public void dodelete() {
        isComplete = false;
        try {
             if(productRecord != null && productRecord.Id != null){
                 List<sObject> sobjList = new List<sObject>();
                 Product2 productObj = [SELECT Id,position__c FROM Product2 WHERE Id = :productRecord.Id LIMIT 1];
                 List<Product_Value__c> pdtValueList = [SELECT Id FROM Product_Value__c WHERE Product__c = :productRecord.Id];
                 sobjList.addAll((List<sObject>)pdtValueList);
                 sobjList.add(productObj);
                 Database.DeleteResult[] drList = Database.delete(sobjList, true);
                 isComplete = true;
             }
        } catch(Exception e){
             isComplete = false;
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));  
        }
    }
     
    
    public class ProductMetaWrapper 
    {
        public Product_Meta__c prodMeta{get;set;}
        public Product_Value__c ProdValue{get;set;}
        public ProductMetaWrapper(Product_Meta__c pm){
            this.prodMeta = pm;
            this.ProdValue = new Product_Value__c();
            this.ProdValue.Product_Meta__c = pm.id;
                      
        }                         
    }
    
    public class ProductMetaWrapperup 
    {
        public Product_Meta__c prodMeta{get;set;}
        public Product_Value__c ProdValue{get;set;}
        public ProductMetaWrapperup(Product_Meta__c pm,Product_Value__c pv){
            this.prodMeta = pm;
            this.ProdValue = pv;
        }                         
    }   
   
    
     
   /* public PageReference doSaveAndNew(){        
        try{
            productRecord.Product_Category__c=prodCategoryId;
            insert productRecord;
            for(Product_Value__c pvRec : prodValueWrapperList){
                pvRec.Product__c = productRecord.Id;
            } 
            insert prodValueWrapperList;
            return new pagereference('/apex/product?catid={!prodCategoryId}');
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }   
        return null;
    }
   
     public void upload()
    {   
        try{
            attach = new Attachment();
            attach.Name = fname ;
            attach.ContentType = contentype;
            attach.body=file1; 
           
            if (attach.Body.size() > 2000000 ) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'You can upload up to 2mb'));
            }    
         } 
         catch(Exception e){
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); 
            message ='Whoops! An error occurred -- ' + e.getMessage();  
            System.debug('testing of message----->'+message) ;
        }
         
    } */

}