@RestResource(urlMapping='/activations/trial')
global class TrialEmailController {

    private static void Finalize(Boolean success, String message, String diagnostics) {
        String result = '{ "success": ' + String.valueOf(success) + 
            (message != null ? ', "message": "' + message + '"' : '') + 
            (diagnostics != null ? ', "diagnostics": "' + diagnostics + '"' : '') + 
            ' }';
        RestContext.response.responseBody = Blob.valueOf(result);
    }
    
    private static void Finalize(Boolean success, String message) {
        Finalize(success, message, null);
    }
    
    private static void Finalize(Boolean success) {
        Finalize(success, null, null);
    }

    @HttpGet
    @HttpPost
    global static void trial() {
    
        try {
    
            RestContext.response.addHeader('Content-Type', 'application/json');
        
            // Parse parameters.
            String activationId = RestContext.request.params.get('activationId');
            
            if (activationId == null) {
                Finalize(false, 'An activation ID must be supplied.');
                return;
            }
                        
            Boolean sendEmail = false;
            String sendEmailParameter = RestContext.request.params.get('sendEmail');
            if (sendEmailParameter != null)
                sendEmail = Boolean.valueOf(sendEmailParameter);
            
            // Set up our variables.        
            Boolean trialDetailsResponseSuccess = false;
            String trialDetailsResponseMessage = null;
            String trialDetailsResponseShipToEmail = null;
            String trialDetailsProductName = null;
            String trialDetailsProductVersion = null;
            String trialDetailsProductDisplayName = null;
            
            // Call our web service.
            HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
            List<LicenseActivationSettings__c> settingsList = Database.query('SELECT GetTrialDetailsServletURI__c FROM LicenseActivationSettings__c WHERE Name = \'' + UserInfo.getOrganizationId() + '\'');
            LicenseActivationSettings__c settings = settingsList[0];
            String url = settings.GetTrialDetailsServletURI__c + '?activationId=' + EncodingUtil.urlEncode(activationId, 'UTF-8'); 
            request.setEndpoint(url);
            Http http = new Http();
            HTTPResponse response = http.send(request);
            String jsonString = response.getBody();
            JSONParser parser = JSON.createParser(jsonString);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() != JSONToken.FIELD_NAME)
                    continue;
                String fieldName = parser.getText();
                if (fieldName == 'success') {
                    parser.nextToken();
                    trialDetailsResponseSuccess = parser.getBooleanValue();
                }
                if (fieldName == 'message') {
                    parser.nextToken();
                    trialDetailsResponseMessage = parser.getText();
                }
                if (fieldName == 'shipToEmail') {
                    parser.nextToken();
                    trialDetailsResponseShipToEmail = parser.getText();
                }
                if (fieldName == 'productName') {
                    parser.nextToken();
                    trialDetailsProductName = parser.getText();
                }
                if (fieldName == 'productVersion') {
                    parser.nextToken();
                    trialDetailsProductVersion = parser.getText();
                }
                if (fieldName == 'productDisplayName') {
                    parser.nextToken();
                    trialDetailsProductDisplayName = parser.getText();
                }
            }
                    
            if (!trialDetailsResponseSuccess) {
                Finalize(false, trialDetailsResponseMessage);
                return;
            }
            
            List<Contact> contacts = Database.query('SELECT Id FROM Contact WHERE Email = \'' + trialDetailsResponseShipToEmail + '\'');
            if (contacts.size() == 0) {
                Finalize(false, 'No contact found with ' + trialDetailsResponseShipToEmail + ' as their email address.');
                return;
            }
            
            String emailTemplateName = 'Trial Email';
            List<EmailTemplate> templates = Database.query('SELECT Id FROM EmailTemplate WHERE Name = \'' + emailTemplateName + '\'');
            if (templates.size() == 0) {
                Finalize(false, 'The ' + emailTemplateName + ' template was not found.');
                return;
            }
            
            String oweaName = 'noreply@sciex.com';
            List<OrgWideEmailAddress> owea = Database.query('SELECT Id FROM OrgWideEmailAddress WHERE Address = \'' + oweaName + '\'');
            if (owea.size() == 0) {
                Finalize(false, 'The organization wide email address was not found for ' + oweaName + '.');
                return;
            }
            
            Trial_Key__c key = new Trial_Key__c();
            key.Activation_ID__c = activationId;
            key.Contact__c = contacts[0].Id;
            key.Creation_Date__c = date.today();
            key.Product_Name__c = trialDetailsProductName;
            key.Product_Version__c = trialDetailsProductVersion;
            key.Product_Display_Name__c = trialDetailsProductDisplayName;
            insert key;
            
            if (sendEmail) {
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTemplateId(templates[0].Id);
                mail.setTargetObjectId(contacts[0].Id);
                mail.setWhatId(key.Id);
                mail.setBccSender(false);
                mail.setUseSignature(false);
                mail.setSaveAsActivity(false);
                mail.setOrgWideEmailAddressId(owea[0].Id);
                Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }, true);
            }
            
        }
        catch (Exception e) {
            System.debug(e);
            Finalize(false, e.getMessage(), e.getStackTraceString());
            return;
        }
        
        Finalize(true);
    }
    
}