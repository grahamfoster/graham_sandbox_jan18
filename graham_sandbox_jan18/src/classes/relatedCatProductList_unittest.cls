@isTest
public with sharing class relatedCatProductList_unittest 
{
	public static testmethod void relatedCatProductList_unittest()
	{	
		Product_Category__c p_c= Util.CreateProductCategory();
		Manufacturer__c mrf= Util.createManufacturerABX();
		Manufacturer__c mrf1= Util.createManufacturer();
		
		Product2 prd= Util.createProduct2(mrf.id , p_c.id);		
		
		ApexPages.currentPage().getParameters().put('proCatId', p_c.Id);
		relatedCatProductList controller = new relatedCatProductList();
		controller.parentpage();
		controller.getlevel2Items();
		List<selectOption> abc = controller.level1Items;
		
		
		Product2 prd1= Util.createProduct2(mrf1.id , p_c.id);
		
		relatedCatProductList controller1 = new relatedCatProductList();
		controller1.parentpage();
		
				 
		relatedCatProductList controller5 = new relatedCatProductList();
		
		relatedCatProductList.ProdWrapper prod = new relatedCatProductList.ProdWrapper();
		
	
	}
	
	public static testmethod void relatedCatProductList_negative()
	{	
		Product_Category__c p_c= Util.CreateProductCategory();
		Manufacturer__c mrf= Util.createManufacturerABX();
		Manufacturer__c mrf1= Util.createManufacturer();
		
		Product2 prd= Util.createProduct2(mrf.id , p_c.id);		
		prd.Competitor_Manufacturer__c = 'Test';
		update prd;
		
		ApexPages.currentPage().getParameters().put('proCatId', p_c.Id);
		relatedCatProductList controller = new relatedCatProductList();
		controller.parentpage();
		controller.getlevel2Items();
		List<selectOption> abc = controller.level1Items;
		
	}
}