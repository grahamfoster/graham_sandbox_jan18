/*********************************************************************
Name  : testAbQuotingPartnerUtils
Author: Appirio, Inc.
Date  : August 26, 2008 
Usage : Used to test the AbQuotingPartnerUtils

*********************************************************************/

public class testAbQuotingPartnerUtils{

public static testMethod void checkAbQuotingPartnerUtils(){
   List<Quoting_SAP_Contact_Partners__c> lstqscp = AbQuotingPartnerUtils.get_SOLD_TO_PartnerList('test');
   lstqscp = AbQuotingPartnerUtils.get_SOLD_TO_PartnerListwithSalesOrg('test','test');
   lstqscp = AbQuotingPartnerUtils.get_SHIP_TO_PartnerListLOCAL('test','test');
   lstqscp = AbQuotingPartnerUtils.get_PAYER_PartnerListLOCAL('test','test');
   lstqscp = AbQuotingPartnerUtils.get_BILL_TO_PartnerListLOCAL('test');
   
   Quoting_SAP_Contact_Partners__c qscp = new Quoting_SAP_Contact_Partners__c();
   String str = AbQuotingPartnerUtils.generatePartnerDisplayValue(qscp);
   Map<String, String> mapStr = AbQuotingPartnerUtils.get_DEST_COUNTRY_PartnerListLOCAL('test');
   mapStr = AbQuotingPartnerUtils.get_SALES_REP_PartnerListLOCAL('test');
   lstqscp = AbQuotingPartnerUtils.get_SAP_CONTACT_ONE_K_PartnerListLOCAL('test','A-L');
   
   str = AbQuotingPartnerUtils.createDisplayValueForContacts('test','test','test');
   str = AbQuotingPartnerUtils.getPartnerCountryBasedOnSoldTo('test');
   
   Set<String> strSet = AbQuotingPartnerUtils.getSalesOrgBasedOnSoldTo('test');
   
}
}