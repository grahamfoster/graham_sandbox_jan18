/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 25/2015
**
** Base class for trigger handlers
** 
**/
public virtual class BaseTriggerHandler 
{
  public Set<Id> collectIds(List<sObject> sobjs)
    {
        Set<Id> ids = new Set<Id>();
        for(sObject sobj : sobjs)
        {
            ids.add(sobj.Id);
        }
        return ids;
    }
    
  public Set<Id> collectParentIds(List<sObject> sobjs, String parentIdField)
    {
        Set<Id> ids = new Set<Id>();
        for(sObject sobj : sobjs)
        {
            if(sobj.get(parentIdField) != null) ids.add((Id)sobj.get(parentIdField));
        }
        return ids;
    }
    
    public Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }

}