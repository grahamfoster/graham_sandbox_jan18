@isTest(SeeAllData=true)
public class ProductStndPricebook_Test {

    public static testMethod void TestInsertProduct() {
	 	
        User USERA = [SELECT Id, Alias FROM User WHERE Alias = 'IRobot'];
		        
        System.runAs(USERA) {

        
		// Find standard price book. There should be only one
		String standardPBId = [Select Id From Pricebook2 where IsStandard = true][0].Id;
		// get Currency Type count, and PricebookEntry count of the Product
		Integer countCT = [SELECT count() FROM CurrencyType WHERE IsActive=TRUE ];

		// ---------------------------------------------------------------------
		// test insert
		// ---------------------------------------------------------------------
	 	Product2 p = new Product2(Name='YongTest_InsertProduct', IsActive=true, CurrencyIsoCode='USD');
		insert p;
		
		// ---------------------------------------------------------------------
		// Verify the PricebookEntry count of this Product should be the same as 
		// Currency Type count   
		// ---------------------------------------------------------------------
		Integer countPBE = [SELECT count() FROM PricebookEntry WHERE Product2Id=:p.Id ];
		System.assertEquals(countCT,countPBE);
        }
	 	
	}
	
}