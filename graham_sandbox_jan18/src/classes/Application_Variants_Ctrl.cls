public with sharing class Application_Variants_Ctrl{

    Public Application_Variants__c AppVariantRecord{get;set;}   
    public Boolean isErrorMsg{get;set;}
    
    public Application_Variants_Ctrl(ApexPages.StandardController controller)
    {    
        isErrorMsg = true;    
        AppVariantRecord = (Application_Variants__c)Controller.getRecord();  
        try {
	        String appCatId = Apexpages.currentPage().getParameters().get('appCat');
	        if(String.isNotEmpty(appCatId)) {
	        	AppVariantRecord.Application__c = appCatId; 
	        }    
        }catch(Exception e){ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));}    
    }
    
    public PageReference doSaveAndNew(){        
        try{
            isErrorMsg = true;
            if(String.isNotEmpty(AppVariantRecord.Application_Variant_Name__c)) {
            insert AppVariantRecord;
            isErrorMsg = false;
            }
            return new pagereference('/apex/Application_Variants');
        }catch(Exception e){ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));isErrorMsg = true;}   
        return null;
    }
    public PageReference doSave(){
        try{
            if(AppVariantRecord != null && String.isNotEmpty(AppVariantRecord.Application_Variant_Name__c)){
                 system.debug('fiest block');
                 isErrorMsg = false; 
                 upsert AppVariantRecord;
             } else {
                 system.debug('second block');
                 isErrorMsg = true; 
                 ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ' Application Name: You must enter a value'));
             }        
         }catch(Exception e){ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));isErrorMsg = true;}
        return null;
    }
    public PageReference doCancel(){
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
            return new PageReference (cancelURL);
        }
        return null;    
    }
    
     public void doDelete(){
        if(AppVariantRecord != null && AppVariantRecord.Id != null){
            try{
                isErrorMsg = false;
                delete AppVariantRecord;
            } catch(Exception e){isErrorMsg = true;ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));    
            }  
        }    
    }
}