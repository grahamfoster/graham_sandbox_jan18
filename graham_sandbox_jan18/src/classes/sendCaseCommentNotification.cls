/*
 *	sendCaseCommentNotification
 *	
 *	Statuc method(s) involked by process builder to send case comment notifications to
 *	Case Contact according to templates on the contacts Account Country Mapping
 * 
 * 	Created by Graham Foster on 2017-05-09
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

public class sendCaseCommentNotification {

	/*****************************************************************************************************
	* @description Sends an Email to the case contact with the latest public case comment.  Triggered
	* from process builder.
	* @param CaseCommentIds - a list of Ids of case comments sent from process builder
	*/
	@InvocableMethod
	public static void sendCaseCommentEmail(List<Id> CaseCommentIds)
	{
		// get a list of the details from the case contacts and add the CaseIds to the 
		// set so that we can query the details from the case object
		List<CaseComment> cc = [SELECT CommentBody, ParentId FROM CaseComment 
		WHERE Id IN :CaseCommentIds];
		Set<Id> CaseIds = new Set<Id>();
		for(CaseComment c : cc)
		{
			CaseIds.add(c.ParentId);
		}

		//query the case details
		Map<Id, Case> caseData = new Map<Id, Case>([SELECT Id, CaseNumber, ContactId, Account.Country_Mapping__r.Case_Comment_Email_Template__c 
		FROM Case WHERE Id IN :CaseIds]);

		//we need the template Ids so build a set of the API Names of the
		//templates and query the Ids
		Map<String, Id> templateMap = new Map<String, Id>();
		Set<String> templateNames = new Set<String>();

		for(Case ca : caseData.Values())
		{
			if(!String.isEmpty(ca.Account.Country_Mapping__r.Case_Comment_Email_Template__c))
			{
				templateNames.Add(ca.Account.Country_Mapping__r.Case_Comment_Email_Template__c);
			}
		}

		if(templateNames.size() > 0)
		{
			List<EmailTemplate> templates = [SELECT Id, DeveloperName FROM EmailTemplate WHERE DeveloperName IN :templateNames];
		
			//add the template name and id to the map
			for(EmailTemplate e : templates)
			{
				templateMap.put(e.DeveloperName, e.Id);
			} 
		
			//build a list of email messages to be set.
			OrgWideEmailAddress owa = [select id from OrgWideEmailAddress WHERE Address = 'noreply@sciex.com' AND DisplayName = 'SCIEXNow' limit 1];
			List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
			for(CaseComment c : cc)
			{
				Case ca = caseData.get(c.ParentId);
				Id templateId = templateMap.get(ca.Account.Country_Mapping__r.Case_Comment_Email_Template__c);
				System.debug('**GF** Email Template:' + templateId);
				if(!String.isEmpty(owa.Id) && (!String.isEmpty(templateId)))
				{
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setTemplateID(templateId);
					mail.setOrgWideEmailAddressId(owa.id);
					mail.setTargetObjectId(ca.ContactId);
					mail.setWhatId(ca.Id);
					System.debug('**GF** Adding Email Message:' + mail);
					allmsg.add(mail);
				} 
			}
			if(allmsg.size() > 0)
			{
				Messaging.sendEmail(allmsg,false);
			}

		} 
		
	}

}