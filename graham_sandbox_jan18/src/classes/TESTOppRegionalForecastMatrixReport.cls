/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : December, 2012
Description    : This is a test class for: OppRegionalForecastMatrixReport, OppHelper classes
History        :

*/
@isTest
public with sharing class TESTOppRegionalForecastMatrixReport 
{
    @testSetup
    static void setUp() {
        Profile prof = [select Id from Profile where Name = 'System Administrator'];
        UserRole Role = [SELECT ID, Name From UserRole WHERE Name='North America Sales Manager Mid Atlantic'  limit 1];
        createUser('testusr1', prof.Id, Role.Id);
        UserRole DRole = [SELECT ID, Name From UserRole WHERE Name='North American Sales Rep Mid Atlantic'  limit 1];
        createUser('testusr2', prof.Id, DRole.Id);
    }
    
	@isTest
    static void testUpdateOpportunityTrigger() 
    {   
		User Us = [select Id, UserRole.Id from User where Username = 'testusr1@example.com'];
        system.assert(Us != null);
		User Us2 = [select Id, UserRole.Id from User where Username = 'testusr2@example.com'];
        system.assert(Us2 != null);

        Account Acc = new Account();
        Opportunity Opp1 = new Opportunity();
        Opportunity Opp2 = new Opportunity();
        Opportunity Opp3 = new Opportunity();
        Opportunity Opp4 = new Opportunity();
        Opportunity Opp5 = new Opportunity();
        Opportunity Opp6 = new Opportunity();        
        Opportunity Opp7 = new Opportunity();          
        Contact Con = new Contact();

        //User Us2 = [SELECT ID, Name, Title, UserRole.Name, UserRole.Id, Profile.Name, UserRoleId, 
		//                    License_Region__c From User WHERE UserRoleId=:DRole.id  AND IsActive = true limit 1];
        

        Market_Share__c MarketShare1 = new Market_Share__c();
        /*Market_Share__c MarketShare2 = new Market_Share__c();
        Market_Share__c MarketShare3 = new Market_Share__c();
        Market_Share__c MarketShare4 = new Market_Share__c();
        Market_Share__c MarketShare5 = new Market_Share__c();
        Market_Share__c MarketShare6 = new Market_Share__c();     
        Market_Share__c MarketShare7 = new Market_Share__c(); */
        
        MarketShare1.Region__c = 'Americas';  
        MarketShare1.Market_Share__c = 15;
        MarketShare1.Market_Vertical__c = 'ALL Segments';        
        
        /*MarketShare2.Region__c = 'Americas';  
        MarketShare2.Market_Share__c = 15;
        MarketShare2.Market_Vertical__c = 'Pharma/CRO';  
        
        MarketShare3.Region__c = 'Americas';  
        MarketShare3.Market_Share__c = 15;
        MarketShare3.Market_Vertical__c = 'Academia/Omics';  
        
        MarketShare4.Region__c = 'Americas';  
        MarketShare4.Market_Share__c = 15;
        MarketShare4.Market_Vertical__c = 'Food/Beverage';  
        
        MarketShare5.Region__c = 'Americas';  
        MarketShare5.Market_Share__c = 15;
        MarketShare5.Market_Vertical__c = 'Environmental/Industrial';  
        
        MarketShare6.Region__c = 'Americas';  
        MarketShare6.Market_Share__c = 15;
        MarketShare6.Market_Vertical__c = 'Clinical';  
        
        MarketShare7.Region__c = 'Americas';  
        MarketShare7.Market_Share__c = 15;
        MarketShare7.Market_Vertical__c = 'Forensic'; */
      	insert MarketShare1;
        /*insert MarketShare2;
        insert MarketShare3;
        insert MarketShare4;
        insert MarketShare5;
        insert MarketShare6;
        insert MarketShare7;*/
                                                         
                             
        Acc.Name = 'Javante';
        Acc.BillingCountry='United States';
        Acc.Country__c = 'United States';
        Acc.Description = 'Testing delete department on account change.';  
        Acc.Customer_Classification__c='Good';      
        insert Acc;
        
        Con.LastName = 'Test';
        Con.AccountId = Acc.Id;
        insert Con;        

        Opp1.AccountId = Acc.Id;
        Opp1.Primary_Contact__c = Con.Id;
        Opp1.Mgr_Upside__c = true;
        Opp1.OwnerId = Us.id;
        Opp1.Name = 'Test Opp_1';
        Opp1.CloseDate = date.today();
        Opp1.Shippable_Date__c = date.today();
        Opp1.Amount = 145668;
        Opp1.Market_Vertical__c = 'Pharma/CRO';
        Opp1.StageName = 'Prospect/Budget Requested';
        Insert Opp1;
        
        System.Test.startTest();
        
        /*Opp2.AccountId = Acc.Id;
        Opp2.Primary_Contact__c = Con.Id;
        Opp2.Name = 'Test Opp_2';
                Opp2.OwnerId = Us.id;
        Opp2.Mgr_Upside__c = true;
        Opp2.CloseDate = date.today();
        Opp2.Shippable_Date__c = date.today();
        Opp2.Market_Vertical__c = 'Academia/Omics';        
        Opp2.Amount = 145668;
        Opp2.StageName = 'Prospect/Budget Requested';
        Insert Opp2;
        
        Opp3.AccountId = Acc.Id;
        Opp3.Primary_Contact__c = Con.Id;
        Opp3.Name = 'Test Opp_3';
        Opp3.Mgr_Upside__c = true;
                Opp3.OwnerId = Us.id;
        Opp3.CloseDate = date.today();
        Opp3.Shippable_Date__c = date.today();
        Opp3.Market_Vertical__c = 'Food/Beverage';          
        Opp3.Amount = 145668;
        Opp3.StageName = 'Prospect/Budget Requested';
        Insert Opp3;
        
        //System.Test.startTest();
        
        Opp4.AccountId = Acc.Id;
        Opp4.Primary_Contact__c = Con.Id;
        Opp4.Name = 'Test Opp_4';
        Opp4.Mgr_Upside__c = true;
                Opp4.OwnerId = Us.id;
        Opp4.CloseDate = date.today();
        Opp4.Shippable_Date__c = date.today();
        Opp4.Market_Vertical__c = 'Environmental/Industrial';                
        Opp4.Amount = 145668;
        Opp4.StageName = 'Prospect/Budget Requested';
        Insert Opp4;
        
        Opp5.AccountId = Acc.Id;
        Opp5.Primary_Contact__c = Con.Id;
        Opp5.Name = 'Test Opp_5';
                Opp5.OwnerId = Us.id;
        Opp5.Mgr_Upside__c = true;
        Opp5.Shippable_Date__c = date.today();
        Opp5.Market_Vertical__c = 'Clinical';           
        Opp5.CloseDate = date.today();
        Opp5.Amount = 145668;
        Opp5.StageName = 'Prospect/Budget Requested';
        Insert Opp5;     
        
        Opp6.AccountId = Acc.Id;
        Opp6.Primary_Contact__c = Con.Id;
        Opp6.Mgr_Upside__c = true;
        Opp6.Name = 'Test Opp_6';
                Opp6.OwnerId = Us.id;
        Opp6.Shippable_Date__c = date.today();
        Opp6.Market_Vertical__c = 'Forensic';             
        Opp6.CloseDate = date.today();
        Opp6.Amount = 145668;
        Opp6.StageName = 'Prospect/Budget Requested';
        Insert Opp6;                                     
        
        Opp7.AccountId = Acc.Id;
        Opp7.Primary_Contact__c = Con.Id;
        Opp7.In_Forecast_Mgr__c = true;
        Opp7.Name = 'Test Opp_6';
        Opp7.OwnerId = Us.id;
        Opp7.Shippable_Date__c = date.today();
        Opp7.Market_Vertical__c = 'Forensic';             
        Opp7.CloseDate = date.today();
        Opp7.Amount = 145668;
        Opp7.StageName = 'Prospect/Budget Requested';
        Insert Opp7;
        */
        
        System.Test.stopTest();
  
       System.runAs(Us)
        {        
        OppRegionalForecastMatrixReport TeT = new OppRegionalForecastMatrixReport();  
        List<UserRole> PerUr = new List<UserRole>();
        List<UserRole> P2323erUr = new List<UserRole>();    
        List<OppHelper> P232wedsd3erUr = new List<OppHelper>();         
        UserRole PterUr = new UserRole();           
        User PerUr2Id = new User();         
        TeT.DochRole(Us.UserRole.id); 
        TeT.Ur2DochRole(TeT.DochRole(Us.UserRole.id));  
        PerUr2Id = TeT.LogUser;
        P232wedsd3erUr = Tet.AmericasSales;
        }
        
      /* System.runAs(Us2)
        {
            OppRegionalForecastMatrixReport TeT2 = new OppRegionalForecastMatrixReport();  
            List<UserRole> PerUr2 = new List<UserRole>();
            List<UserRole> P2323erUr2 = new List<UserRole>();    
            List<OppHelper> P232wedsd3erUr2 = new List<OppHelper>();            
            UserRole PterUr2 = new UserRole();          
            User PerUr2Id2 = new User();        
            String VisHomeBut;
            ApexPages.currentPage().getParameters().put('id',Us.UserRole.id);    
            VisHomeBut = TeT2.VisHomeBut;     
            TeT2.DochRole(Us2.UserRole.id);
            TeT2.Ur2DochRole(TeT2.DochRole(Us.UserRole.id));    
            PerUr2Id2 = TeT2.LogUser;
            P232wedsd3erUr2 = Tet2.AmericasSales;       
        }         */         
    }      

    private static void createUser(String alias, Id profileId, Id userRoleId){
        User u = new User(alias = alias,email=alias+'@example.com',emailencodingkey='UTF-8',lastname=alias, languagelocalekey='en_US',
            localesidkey='en_US',
            profileId = profileId,
			userroleid = userRoleId,
            timezonesidkey='America/New_York',
            username=alias+'@example.com');
        insert u;     
    }
}