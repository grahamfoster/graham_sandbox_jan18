public with sharing class OIBDetailsController {
    
    public OIBdetail myOIB {get; set;}
    public String oibId {get; set;}
        
    public OIBdetail getIB(){
        if(myOIB == null) {
            oibId = ApexPages.currentPage().getParameters().get('id');
        	myOIB = new OIBdetail();
            myOIB.contracts = new List<SVMXC__Service_Contract_Products__c>();
            myOIB.opportunities = new List<OpportunityLineItem>();
			List <Opportunity_IB__c> oib = new List<Opportunity_IB__c>([SELECT id, Covered_Product__r.SVMXC__Service_Contract__c ,Opportunity__c, Serial_Number__c, Model__c, Current_Plan__c, Expiration__c, Opportunity__r.Accountid  FROM Opportunity_IB__c WHERE id = :oibID LIMIT 1]);
            myOIB.originalIB = oib.get(0);        
	        for(SVMXC__Service_Contract_Products__c sc :[SELECT id, SVMXC__SERVICE_CONTRACT__C, SVMXC__SERVICE_CONTRACT__r.SVMXC__Company__c, SVMXC__END_DATE__C,SVMXC__INSTALLED_PRODUCT__C, SVC_CONTRACT_SUBLINE_STATUS__C,Model__c, Serial_Number__c, SVC_Item_Master_Name__c FROM SVMXC__Service_Contract_Products__c WHERE Serial_Number__c = :myOIB.originalIB.Serial_Number__c  AND SVMXC__Start_Date__c > TODAY AND SVC_CONTRACT_SUBLINE_STATUS__C IN('ACTIVE','SIGNED','MULTIPLE YEAR - GOT PO') AND SVC_Contract_Applicability__c IN('Future','Current') ] ){
    	        myOIB.contracts.add(sc);
        	}
        	for(OpportunityLineItem opp :[SELECT id, OpportunityId, Opportunity.StageName, Opportunity.Accountid, Opportunity.CONTRACT_EXPIRY_DATE__C, Product2Id, ProductCode, Covered_Product__c, Model__c, Opportunity_IB__c, Serial_Number__c, Type__c  FROM OpportunityLineItem WHERE Serial_Number__c = :myOIB.originalIB.Serial_Number__c  AND Opportunity.CONTRACT_EXPIRY_DATE__C > TODAY AND  Opportunity.StageName != 'Deal Lost' AND  Opportunity.StageName != 'Dead/Cancelled' AND OpportunityId != : myOIB.originalIB.Opportunity__c]  ){
	            myOIB.opportunities.add(opp);
    	    } 
        }
        return myOIB;
    }
        

    
   
    public class OIBdetail{
        public Opportunity_IB__c originalIB {get; set;} 
        public List<SVMXC__Service_Contract_Products__c> contracts {get; set;}
        public List<OpportunityLineItem> opportunities {get; set;}
    }
    
}