/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Aug 12/2014
 **
 ** Test coverage for EditAllProductsExtension
**/
@isTest(SeeAllData=true)
public class EditAllProductsExtensionTests {
	static Pricebook2 pricebook;
	static List<PricebookEntry> testPricebookEntries;

	public static testMethod void test1() {
		Opportunity testOpp = setUp();
		EditAllProductsExtension ext = new EditAllProductsExtension(new ApexPages.StandardController(testOpp));
		ext.init();

		EditAllProductsExtension.ProductWrapper[] productsFound = EditAllProductsExtension.doSearch('Test', pricebook.Id, 'CAD');
		system.assert(true, productsFound.size() > 0);

		ext.opportunityLineItemId = testOpp.OpportunityLineItems[0].Id;
		ext.pricebookEntryId = productsFound[1].pricebookEntry.Id;
		ext.setProduct();
        Test.startTest();
		ext.save();
        Test.stopTest();
	}

	public static testMethod void test2_dmlexception() {
		Opportunity testOpp = setUp();
		EditAllProductsExtension ext = new EditAllProductsExtension(new ApexPages.StandardController(testOpp));
		ext.init();

		ext.opp.OpportunityLineItems[0].Quantity = null;
		ext.opp.OpportunityLineItems[0].UnitPrice = null;

		ext.save();
	}

	private static Opportunity setUp(){
		createTestPricebook();
        
        Account acct = new Account(Name = 'Test Coverage Account', BillingCountry = 'Canada');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId = acct.Id, Name = 'Test Opp', Pricebook2Id = pricebook.Id, CurrencyIsoCode = 'CAD', 
        	StageName = 'Recognition of Needs', CloseDate = Date.today(),
            Contract_Number__c = '35306459', Explicit_Needs__c = '?', Implications__c = '?');
        insert opp;

        OpportunityLineItem lineItem = new OpportunityLineItem(OpportunityId = opp.Id, PricebookEntryId = testPricebookEntries[0].Id, 
        	Quantity = 1, UnitPrice = 1000);
        insert lineItem;

        opp = [select AccountId, Name, Pricebook2Id, CurrencyIsoCode, StageName, CloseDate, Contract_Number__c, 
        	(select Id from OpportunityLineItems) from Opportunity where Id = :opp.Id];
        return opp;
	}

    private static void createTestPricebook(){
        Pricebook2 stdPricebook = [select Id from Pricebook2 where IsStandard = true limit 1];

        pricebook = new Pricebook2(Name = 'Test Price Book', CurrencyIsoCode = 'CAD', IsActive = true);
        insert pricebook; 
        
        List<Product2> testProducts = new List<Product2> {
            new Product2(Name = 'Test product 1', ProductCode = 'test-1', IsActive = true, CurrencyIsoCode = 'CAD'),
            new Product2(Name = 'Test product 2', ProductCode = 'test-2', IsActive = true, CurrencyIsoCode = 'CAD'),
            new Product2(Name = 'Test product 3', ProductCode = 'test-3', IsActive = true, CurrencyIsoCode = 'CAD'),
            new Product2(Name = 'Test product 4', ProductCode = 'test-4', IsActive = true, CurrencyIsoCode = 'CAD'),
            new Product2(Name = 'Test product 5', ProductCode = 'test-5', IsActive = true, CurrencyIsoCode = 'CAD')
        };
        insert testProducts;
        
        List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
        for(Product2 tp : testProducts) {
            pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'CAD', UnitPrice = 1, IsActive = true));
        }
        insert pricebookEntries;

        testPricebookEntries = new List<PricebookEntry>();
        for(Product2 tp : testProducts) {
            testPricebookEntries.add(new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'CAD', UnitPrice = 1, IsActive = true));
        }
        insert testPricebookEntries;
    }
}