/**
 * @author Brett Moore
 * @created - July 2016
 * @Revision  
 * @Last Revision 
 * 
 * Batchable APEX Class that launches the C2O Modules. 
 * 
**/
global class Contract2Opportunity_Batch implements Database.Batchable<sObject> {
    
    global final String Query;
    global final String Process;

    global Contract2Opportunity_Batch(String q, String p){
        Query=q;
        Process=p; 
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
      	return Database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
		if(Process=='Populate'){
			Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>((List<Opportunity>)scope);
            new Contract2Opportunity_Populate(oppMap);
        } else if(Process=='Link'){
            C2O_Merge mergeO = new C2O_Merge();
			Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>((List<Opportunity>)scope);
            new Contract2Opportunity_Link(oppMap);
        } else if (Process=='Create'){
			Map<ID,SVMXC__Service_Contract__c> smcMap = new Map<ID,SVMXC__Service_Contract__c>((List<SVMXC__Service_Contract__c>)scope);
            new Contract2Opportunity_Create(smcMap);
        } else if (Process=='Sanitize'){
			Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>((List<Opportunity>)scope);
            new Contract2Opportunity_Sanitize(oppMap);
        } else if (Process=='Assign'){
			Map<ID,Opportunity> oppMap = new Map<ID,Opportunity>((List<Opportunity>)scope);
            new Contract2Opportunity_Assign(oppMap);
        }
    }
   
    global void finish(Database.BatchableContext BC){
   	}	

}