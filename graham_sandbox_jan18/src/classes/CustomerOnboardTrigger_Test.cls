/**
** @author Reid Beckett, Cloudware Connections
** @created July 13/2016
**
** Test coverage for Customer Onboard trigger logic
**/
@isTest
public class CustomerOnboardTrigger_Test 
{
    //test all 4 trigger operations
    @isTest
    static void test()
    {
        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;

        Case caseObj = new Case(
            AccountId = a.Id, 
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
            Sales_Order_Number__c = '123'
        );
        insert caseObj;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Oracle_Order_Number__c='123');
        insert oracleOrder;

        Customer_Onboard__c cob = new Customer_Onboard__c(Oracle_Order__c = oracleOrder.Id);
        insert cob;
        update cob;
        delete cob;
        undelete cob;
    }


    //test insert populates the Installation Case lookup
    @isTest
    static void testOnInsert()
    {
        RecordType prodrt = SfdcUtil.getRecordType('Product2', 'AB_Sciex');
        Product2 prod = new Product2(Name='Test Product', RecordTypeID = prodrt.ID, Family = '3000');
        insert prod;

        ProductFamilySettings__c pfs = new ProductFamilySettings__c(Name = '3000', Priority__c = 1, Instrument__c = true);
        insert pfs;

        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;

        SVMXC__Installed_Product__c comp = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id );
        insert comp;

        Asset ast = new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comp.Id);
        insert ast;

        Case caseObj = new Case(
            AccountId = a.Id, 
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
            Sales_Order_Number__c = '123',
            SVMXC__Component__c = comp.Id
        );
        insert caseObj;

        SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = caseObj.Id, SVMXC__Order_Type__c = 'Installation');
        insert workOrder;

        SVMXC__Installed_Product__c comp2 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id );
        insert comp2;

        Asset ast2 = new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comp2.Id);
        insert ast2;

        Test.startTest();
        Case caseObj2 = new Case(
            AccountId = a.Id, 
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
            Sales_Order_Number__c = '123',
            SVMXC__Component__c = comp2.Id
        );
        insert caseObj2;

        SVMXC__Service_Order__c workOrder2 = new SVMXC__Service_Order__c(SVMXC__Case__c = caseObj2.Id, SVMXC__Order_Type__c = 'Installation');
        insert workOrder2;

        SVMXC__Installed_Product__c comp3 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id );
        insert comp3;

        Asset ast3 = new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comp3.Id);
        insert ast3;

        Case caseObj3 = new Case(
            AccountId = a.Id, 
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
            Sales_Order_Number__c = '123',
            SVMXC__Component__c = comp3.Id
        );
        insert caseObj3;

        SVMXC__Service_Order__c workOrder3 = new SVMXC__Service_Order__c(SVMXC__Case__c = caseObj3.Id, SVMXC__Order_Type__c = 'Installation');
        insert workOrder3;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Oracle_Order_Number__c='123');
        insert oracleOrder;

        Customer_Onboard__c cob = new Customer_Onboard__c(Oracle_Order__c = oracleOrder.Id);
        insert cob;
        Test.stopTest();

        cob = [select InstallationCase__c, InstallationCase2__c, InstallationCase3__c, Asset__c, Asset2__c, Asset3__c from Customer_Onboard__c where Id = :cob.Id];
        system.assertEquals(caseObj.Id, cob.InstallationCase__c);
        system.assertEquals(caseObj2.Id, cob.InstallationCase2__c);
        system.assertEquals(caseObj3.Id, cob.InstallationCase3__c);
        system.assertEquals(ast.Id, cob.Asset__c);
        system.assertEquals(ast2.Id, cob.Asset2__c);
        system.assertEquals(ast3.Id, cob.Asset3__c);
    }

    //test update populates the Installation Case lookup
    @isTest
    static void testOnUpdate()
    {
        RecordType prodrt = SfdcUtil.getRecordType('Product2', 'AB_Sciex');
        Product2 prod = new Product2(Name='Test Product', RecordTypeID = prodrt.ID, Family='3000');
        insert prod;

        ProductFamilySettings__c pfs = new ProductFamilySettings__c(Name = '3000', Priority__c = 1, Instrument__c = true);
        insert pfs;

        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;

        SVMXC__Installed_Product__c[] comps = new SVMXC__Installed_Product__c[]{
            new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id ),
            new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id ),
            new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id )
		};
        insert comps;

        //SVMXC__Installed_Product__c comp = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Operating_Unit__c = 'ABC', SVMXC__Product__c = prod.Id );
        //insert comp;

        Asset[] asts = new Asset[]{
            new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comps[0].Id),
            new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comps[1].Id),
            new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comps[2].Id)
		};
        insert asts;

        //Asset ast = new Asset(Name = 'test asset', AccountId = a.Id, Installed_Product__c = comps[0].Id);
        //insert ast;

        Contact cont = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert cont;

        Test.startTest();
        Case[] caseObjs = new Case[]{
            new Case(
                AccountId = a.Id, 
                RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
                Sales_Order_Number__c = '123',
                SVMXC__Component__c = comps[0].Id           
            ),
            new Case(
                AccountId = a.Id, 
                RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
                Sales_Order_Number__c = '123',
                SVMXC__Component__c = comps[1].Id           
            ),
            new Case(
                AccountId = a.Id, 
                RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
                Sales_Order_Number__c = '123',
                SVMXC__Component__c = comps[2].Id           
            )
        };
        insert caseObjs;
        //GF Change for async Test.stopTest();

        /*
        Case caseObj = new Case(
            AccountId = a.Id, 
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
            Sales_Order_Number__c = '123',
            SVMXC__Component__c = comps[0].Id           
        );
        insert caseObj;
		*/

        SVMXC__Service_Order__c[] workOrders = new SVMXC__Service_Order__c[]{
            new SVMXC__Service_Order__c(SVMXC__Case__c = caseObjs[0].Id, SVMXC__Order_Type__c = 'Installation'),
            new SVMXC__Service_Order__c(SVMXC__Case__c = caseObjs[1].Id, SVMXC__Order_Type__c = 'Installation'),
            new SVMXC__Service_Order__c(SVMXC__Case__c = caseObjs[2].Id, SVMXC__Order_Type__c = 'Installation')
        };
		insert workOrders;
        
        //SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = caseObjs[0].Id, SVMXC__Order_Type__c = 'Installation');
        //insert workOrder;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Oracle_Order_Number__c='123', Training_Product__c = '456');
        insert oracleOrder;

        Customer_Onboard__c cob = new Customer_Onboard__c();
        insert cob;

        OnBoarding_Contacts__c obcont = new OnBoarding_Contacts__c(OnBoard_Contact__c = cont.Id, Customer_Onboard__c = cob.Id, Role__c = 'Primary Learner');
        insert obcont;

        cob.Oracle_Order__c = oracleOrder.Id;
        update cob;
		Test.stopTest();

        cob = [select InstallationCase__c, InstallationCase2__c, InstallationCase3__c, Asset__c, Asset2__c, Asset3__c from Customer_Onboard__c where Id = :cob.Id];
        system.assertEquals(caseObjs[0].Id, cob.InstallationCase__c);
        system.assertEquals(caseObjs[1].Id, cob.InstallationCase2__c);
        system.assertEquals(caseObjs[2].Id, cob.InstallationCase3__c);
        system.assertEquals(asts[0].Id, cob.Asset__c);
        system.assertEquals(asts[1].Id, cob.Asset2__c);
        system.assertEquals(asts[2].Id, cob.Asset3__c);

        oracleOrder.Training_Product__c = '456';
        update oracleOrder;
    }

    //Tests linking the Contact to the Asset via the CustomerOnboardTriggerHandler.linkOnboardingContactsToAsset method
    @isTest
    static void testContactAssetLinkOnUpdate()
    {
        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;

        Asset ast = new Asset(Name = 'test asset', AccountId = a.Id);
        insert ast;

        Asset ast2 = new Asset(Name = 'test asset', AccountId = a.Id);
        insert ast2;

        Contact cont = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = a.Id);
        insert cont;

        Contact cont2 = new Contact(FirstName = 'Jane', LastName = 'Doe', AccountId = a.Id);
        insert cont2;

        Asset_MultipleContacts__c contactAssetLink = new Asset_MultipleContacts__c(
            Asset__c = ast.Id, Contact__c = cont2.Id, ContactRole__c = 'User'
        );
        insert contactAssetLink;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Oracle_Order_Number__c='123', Training_Product__c = '456');
        insert oracleOrder;

        Customer_Onboard__c cobo = new Customer_Onboard__c();
        insert cobo;

        OnBoarding_Contacts__c obcont = new OnBoarding_Contacts__c(OnBoard_Contact__c = cont.Id, Customer_Onboard__c = cobo.Id, Role__c = 'Primary Learner');
        insert obcont;

        OnBoarding_Contacts__c obcont2 = new OnBoarding_Contacts__c(OnBoard_Contact__c = cont2.Id, Customer_Onboard__c = cobo.Id, Role__c = 'Primary Learner');
        insert obcont2;

        cobo.Asset__c = ast.Id;
        cobo.Asset2__c = ast2.Id;
        update cobo;

        List<Asset_MultipleContacts__c> contactAssetLinks = [select Id, Contact__c, Asset__c from Asset_MultipleContacts__c where Asset__c = :ast.Id or Asset__c = :ast2.Id];
        system.assertEquals(4, contactAssetLinks.size());
    }   

    //Tests case insert linking the InstallationCase to the COBO
    @isTest
    static void testOnCaseInsert()
    {
System.debug('!!! RBR  - Start testOnCaseInsert()');        
        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;

        Oracle_Order__c oracleOrder = new Oracle_Order__c(Oracle_Order_Number__c='123');
        insert oracleOrder;

        Customer_Onboard__c cobo = new Customer_Onboard__c(Oracle_Order__c = oracleOrder.Id);
        insert cobo;
        
		ProductFamilySettings__c pfs = new ProductFamilySettings__c(Name = '3000', Priority__c = 1, Instrument__c = true);
		insert pfs;

		RecordType prodrt = SfdcUtil.getRecordType('Product2', 'AB_Sciex');
		Product2 ip1Prod = new Product2(Name = 'Test Product 1', RecordTypeID = prodrt.ID, Family = '3000');
		insert ip1Prod;

        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(Name = 'test', SVMXC__Company__c = a.Id, SVMXC__Product__c = ip1Prod.Id);
        insert ip;

        Case caseObj = new Case(
            AccountId = a.Id, 
            RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
            Sales_Order_Number__c = '123',
            SVMXC__Component__c = ip.Id
        );
        insert caseObj;

        cobo = [select InstallationCase__c from Customer_Onboard__c where Id = :cobo.Id];
        system.assertEquals(caseObj.Id, cobo.InstallationCase__c);
    }
    
    //Test for the PDCL Processing
    @isTest
    static void testPostPDCLReturnProcessing(){
        
        PDCL_EMail__c pdem = new PDCL_Email__c(Name = 'AMERICAS', CC_adresses_CSV__c = 'graham.foster@sciex.com,test@testorg.com', Send_To_Sales_Rep__c = true, Send_To_Sales_Manager__c = true,
                                               Email_Template_Id__c = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'AMERICAS_PDCL_To_Sales_Admin' LIMIT 1][0].Id,
                                               Send_From_Id__c = [SELECT Id FROM OrgWideEmailAddress WHERE Address='noreply@sciex.com'][0].Id);
        insert pdem;
        
        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;
        
        Contact c = new Contact(FirstName = 'J', LastName = 'Doe', Email = 'jdoe@example.com', AccountId = a.Id);
        Contact c1 = new Contact(FirstName = 'J1', LastName = 'Doe', Email = 'j1doe@example.com', AccountId = a.Id);
        Contact c2 = new Contact(FirstName = 'J2', LastName = 'Doe', Email = 'j2doe@example.com', AccountId = a.Id);
        Contact c3 = new Contact(FirstName = 'J2', LastName = 'Doe', Email = 'sales.americas@sciex.com', AccountId = a.Id);
        Contact c4 = new Contact(FirstName = 'J2', LastName = 'Doe', Email = 'graham.foster@sciex.com', AccountId = a.Id);
        List<Contact> cons = new List<Contact> {c, c1, c2, c3, c4};
        insert cons;
        
        Opportunity o = new Opportunity(Name = 'Test Opportunity', CloseDate = Date.newInstance(2040, 12, 25), 
                                        Account = a, StageName = 'Recognition of Needs', Primary_Contact__c = c.Id);
        
        insert o;
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p1.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
		insert u;
        
        Product2 p = new Product2(Name = 'Test Product', Description = 'Test Product Description');
        insert p;
        
        test.startTest();
        
        Customer_Onboard__c cobo = new Customer_Onboard__c(
            PDCL_Version__c = 0,
			Sales_Manager__c = u.Id,
			Sales_Representative__c = u.Id,
            Region__c = 'AMERICAS',
            Contacts_Created__c = false);
        insert cobo;
        
            cobo.Primary_Learner_Email__c = 'jdoe@example.com'; 
            cobo.Opportunity_Account__c = a.Id; 
            cobo.Primary_Learner_First_Name__c = 'J'; 
            cobo.Primary_Learner_Last_Name__c = 'Doe';
            cobo.Learner_2_Email__c = 'janedoe@example.com'; 
            cobo.Learner_2_First_Name__c = 'Jane'; 
            cobo.Learner_2_Last_Name__c = 'Doe'; 
            cobo.Learner_3_Email__c = 'janedoe@example.com'; 
            cobo.Learner_3_First_Name__c = 'Jane'; 
            cobo.Learner_3_Last_Name__c = 'Doe'; 
            cobo.Install_Contact_Email__c = 'Test@example.com'; 
            cobo.Install_Contact_First_Name__c = 'T'; 
            cobo.Install_Contact_Last_Name__c = 'est'; 
            cobo.Install_Contact_Phone__c = '1234';
            cobo.Opportunity__c = o.Id;
            cobo.Instrument_Internet_Connected__c = true;
            cobo.Trade_In_SN__c = '123';
        
        //update the PDCL return date and everything should run
        cobo.Pre_Site_Completion_Date__c = system.now();
        update cobo;
        
        
        //test that the extra 2 contacts have been created
        List<Contact> contactCount = [SELECT Id FROM Contact WHERE Email = 'janedoe@example.com' OR Email = 'Test@example.com'];
        system.assertEquals(2, contactCount.size());
        
        //test that the cobo contacts have been created
        List<OnBoarding_Contacts__c> obc = [SELECT Id 
                                            FROM OnBoarding_Contacts__c WHERE Customer_Onboard__c = :cobo.Id];
        
        //check the correct number (4) have been made
        system.assertEquals(4, obc.size());
        
        
        test.stopTest();
    }

	@IsTest static void test_PDCLProcessingAddress()
	{
		PDCL_EMail__c pdem = new PDCL_Email__c(Name = 'AMERICAS', CC_adresses_CSV__c = 'graham.foster@sciex.com',
                                               Email_Template_Id__c = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'AMERICAS_PDCL_To_Sales_Admin' LIMIT 1][0].Id,
                                               Send_From_Id__c = [SELECT Id FROM OrgWideEmailAddress WHERE Address='noreply@sciex.com'][0].Id);
        insert pdem;
		
		Customer_Onboard__c cobo = new Customer_Onboard__c(
            PDCL_Version__c = 0,
            Region__c = 'AMERICAS',
			Chemicals_At_Same_Address__c = true,
			Trade_In_At_Same_Address__c = true,
            Contacts_Created__c = false);
		Customer_Onboard__c cobo2 = new Customer_Onboard__c(
            PDCL_Version__c = 0,
            Region__c = 'AMERICAS',
			Chemicals_At_Same_Address__c = false,
			Trade_In_At_Same_Address__c = false,
            Contacts_Created__c = false);
        insert new List<Customer_Onboard__c>{cobo, cobo2};


		cobo.Instrument_Ship_To_Address_2__c = 'Test1Add2';
        cobo.Instrument_Ship_To_Address__c = 'Test1Add';
        cobo.Instrument_Ship_To_City__c = 'Test1City';
        cobo.Instrument_Ship_To_Company__c = 'Test1Comp';
        cobo.Instrument_Ship_To_Country__c = 'Test1Cou';
        cobo.Instrument_Ship_To_PostalCode__c = 'Test1PC';
        cobo.Instrument_Ship_To_State__c = 'Test1State';
		cobo.Pre_Site_Completion_Date__c = system.now();

		cobo2.Instrument_Ship_To_Address_2__c = 'Test2Add2';
        cobo2.Instrument_Ship_To_Address__c = 'Test2Add';
        cobo2.Instrument_Ship_To_City__c = 'Test2City';
        cobo2.Instrument_Ship_To_Company__c = 'Test2Comp';
        cobo2.Instrument_Ship_To_Country__c = 'Test2Cou';
        cobo2.Instrument_Ship_To_PostalCode__c = 'Test2PC';
        cobo2.Instrument_Ship_To_State__c = 'Test2State';
		cobo2.Pre_Site_Completion_Date__c = system.now();

		update new List<Customer_Onboard__c>{cobo, cobo2};

		List<Customer_Onboard__c> cobos = [SELECT Chemical_Ship_To_Address_2__c, Chemical_Ship_To_Address__c, Chemical_Ship_To_City__c, 
		Chemical_Ship_To_Company__c, Chemical_Ship_To_Country__c, Chemical_Ship_To_PostalCode__c, Chemical_Ship_To_State__c, 
		Trade_In_Location_Address_2__c, Trade_In_Location_Address__c, Trade_In_Location_City__c, Trade_In_Location_Company__c, 
		Trade_In_Location_Country__c, Trade_In_Location_PostalCode__c, Trade_In_Location_State__c 
		FROM Customer_Onboard__c WHERE Id = :cobo.Id OR Id = :cobo2.Id];

		for(Customer_Onboard__c cob : cobos)
		{
			if(cob.Id == cobo.Id)
			{
				//adresses should be the same
				System.assertEquals(cobo.Instrument_Ship_To_Address_2__c, cob.Chemical_Ship_To_Address_2__c);
				System.assertEquals(cobo.Instrument_Ship_To_Address__c, cob.Chemical_Ship_To_Address__c);
				System.assertEquals(cobo.Instrument_Ship_To_City__c, cob.Chemical_Ship_To_City__c);
				System.assertEquals(cobo.Instrument_Ship_To_Company__c, cob.Chemical_Ship_To_Company__c);
				System.assertEquals(cobo.Instrument_Ship_To_Country__c, cob.Chemical_Ship_To_Country__c);
				System.assertEquals(cobo.Instrument_Ship_To_PostalCode__c, cob.Chemical_Ship_To_PostalCode__c);
				System.assertEquals(cobo.Instrument_Ship_To_State__c, cob.Chemical_Ship_To_State__c);
				System.assertEquals(cobo.Instrument_Ship_To_Address_2__c, cob.Trade_In_Location_Address_2__c);
				System.assertEquals(cobo.Instrument_Ship_To_Address__c, cob.Trade_In_Location_Address__c);
				System.assertEquals(cobo.Instrument_Ship_To_City__c, cob.Trade_In_Location_City__c);
				System.assertEquals(cobo.Instrument_Ship_To_Company__c, cob.Trade_In_Location_Company__c);
				System.assertEquals(cobo.Instrument_Ship_To_Country__c, cob.Trade_In_Location_Country__c);
				System.assertEquals(cobo.Instrument_Ship_To_PostalCode__c, cob.Trade_In_Location_PostalCode__c);
				System.assertEquals(cobo.Instrument_Ship_To_State__c, cob.Trade_In_Location_State__c);
			}else 
			{
				//addresses should be null (as we have not set them)
				System.assertEquals(null, cob.Chemical_Ship_To_Address_2__c);
				System.assertEquals(null, cob.Chemical_Ship_To_Address__c);
				System.assertEquals(null, cob.Chemical_Ship_To_City__c);
				System.assertEquals(null, cob.Chemical_Ship_To_Company__c);
				System.assertEquals(null, cob.Chemical_Ship_To_Country__c);
				System.assertEquals(null, cob.Chemical_Ship_To_PostalCode__c);
				System.assertEquals(null, cob.Chemical_Ship_To_State__c);
				System.assertEquals(null, cob.Trade_In_Location_Address_2__c);
				System.assertEquals(null, cob.Trade_In_Location_Address__c);
				System.assertEquals(null, cob.Trade_In_Location_City__c);
				System.assertEquals(null, cob.Trade_In_Location_Company__c);
				System.assertEquals(null, cob.Trade_In_Location_Country__c);
				System.assertEquals(null, cob.Trade_In_Location_PostalCode__c);
				System.assertEquals(null, cob.Trade_In_Location_State__c);
			}
		}
	}
    
    @isTest
    static void testCreateTrainingCaseCaseContacts(){
        
        PDCL_EMail__c pdem = new PDCL_Email__c(Name = 'AMERICAS', CC_adresses_CSV__c = 'graham.foster@sciex.com,nigel.blackwell@sciex.com,susanna.baque@sciex.com',
                                               Email_Template_Id__c = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'AMERICAS_PDCL_To_Sales_Admin' LIMIT 1][0].Id,
                                               Send_From_Id__c = [SELECT Id FROM OrgWideEmailAddress WHERE Address='noreply@sciex.com'][0].Id);
        insert pdem;
        
        Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert a;
        
        Contact c = new Contact(FirstName = 'J', LastName = 'Doe', Email = 'jdoe@example.com', AccountId = a.Id);

        List<Contact> cons = new List<Contact> {c};
        insert cons;
        
        Opportunity o = new Opportunity(Name = 'Test Opportunity', CloseDate = Date.newInstance(2040, 12, 25), 
                                        Account = a, StageName = 'Recognition of Needs', Primary_Contact__c = c.Id);
        
        insert o;
        
        
        Product2 p = new Product2(Name = 'Test Product', Description = 'Test Product Description');
        insert p;
        
            Customer_Onboard__c cobo = new Customer_Onboard__c(
            PDCL_Version__c = 0,
            Region__c = 'AMERICAS',
            Contacts_Created__c = false,
            Instrument_Internet_Connected__c = true);
        insert cobo;
        
            cobo.Primary_Learner_Email__c = 'j2doe@example.com'; 
            cobo.Opportunity_Account__c = a.Id; 
            cobo.Primary_Learner_First_Name__c = 'J'; 
            cobo.Primary_Learner_Last_Name__c = 'Doe';
            cobo.Learner_2_Email__c = 'j3anedoe@example.com'; 
            cobo.Learner_2_First_Name__c = 'Jane'; 
            cobo.Learner_2_Last_Name__c = 'Doe'; 
            cobo.Learner_3_Email__c = 'j4anedoe@example.com'; 
            cobo.Learner_3_First_Name__c = 'Jane'; 
            cobo.Learner_3_Last_Name__c = 'Doe'; 
            cobo.Install_Contact_Email__c = 'Test@example.com'; 
            cobo.Install_Contact_First_Name__c = 'T'; 
            cobo.Install_Contact_Last_Name__c = 'est'; 
            cobo.Install_Contact_Phone__c = '1234';
            cobo.Opportunity__c = o.Id;
            cobo.Trade_In_SN__c = '123';
        
        //update the PDCL return date and everything should run
        cobo.Pre_Site_Completion_Date__c = system.now();
        update cobo;
        
        test.startTest();
        
        cobo.Product__c = p.Id;
        update cobo;
        
        
        //check that there is a training case
        List<Customer_Onboard__c> trainingCheck = [SELECT Training_Case__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
        
        system.assertNotEquals(null, trainingCheck[0].Training_Case__c);
            
                    //make sure the training case has 3 case contacts
        List<Case_Contact__c> ccs = [SELECT Id FROM Case_Contact__c WHERE Case__c = :trainingCheck[0].Training_Case__c];
        system.assertEquals(3, ccs.size());
        
        cobo.Primary_Learner_Email__c = 'j3anedoe@example.com';
        update cobo;
        
        test.stopTest();
        
    }
    
    @isTest
    private static void test_updateAssetOnTrainingCase()
    {
        Account ac = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert ac;
        Asset a = new Asset(Name = 'TestAsset', AccountId = ac.Id);
        insert a;
        Case c = new Case(Subject = 'Test', Description = 'Test');
        insert c;
        Product2 p = new Product2(Name = 'Test Product', Description = 'Test Product Description');
        insert p;
        Oracle_Order__c oo = new Oracle_Order__c(Total_Number_of_FAS_Days_Purchased__c = 2);
        insert oo;
        Customer_Onboard__c cobo = new Customer_Onboard__c();
        insert cobo;
        	cobo.Opportunity_Account__c = ac.Id;
         	cobo.Primary_Learner_Email__c = 'j2doe@example.com'; 
            cobo.Primary_Learner_First_Name__c = 'J'; 
            cobo.Primary_Learner_Last_Name__c = 'Doe';
        	cobo.Product__c = p.Id;
        	cobo.Oracle_Order__c = oo.Id;
        update cobo;
        List<Customer_Onboard__c> cobos = [SELECT Id, Training_Case__c, Training_Case__r.AssetId, 
                                           Training_Case__r.Total_Number_of_FAS_Days__c
                                           FROM Customer_Onboard__c WHERE Id = :cobo.Id LIMIT 1];
        system.assertNotEquals(null, cobos[0].Training_Case__c);
        system.assertEquals(null, cobos[0].Training_Case__r.AssetId);
        system.assertEquals(2, cobos[0].Training_Case__r.Total_Number_of_FAS_Days__c);
        Oracle_Order__c oo1 = new Oracle_Order__c(Total_Number_of_FAS_Days_Purchased__c = 3);
        insert oo1;
        cobo.Oracle_Order__c = oo1.Id;
        cobo.Asset__c = a.Id;
        //Reid - inserted start/stop to avoid too many SOQL errors
        Test.startTest();
        update cobo;
        Test.stopTest();
        List<Customer_Onboard__c> cobos1 = [SELECT Id, Training_Case__c, Training_Case__r.AssetId, 
                                            Training_Case__r.Total_Number_of_FAS_Days__c 
                                           FROM Customer_Onboard__c WHERE Id = :cobo.Id LIMIT 1];
        system.assertNotEquals(null, cobos1[0].Training_Case__c);
        system.assertEquals(a.Id, cobos1[0].Training_Case__r.AssetId);
        system.assertEquals(3, cobos1[0].Training_Case__r.Total_Number_of_FAS_Days__c);
    }
    
    @isTest
    static void testFileCallout(){
        test.startTest();
        Test.setMock(HttpCalloutMock.class, new CustomerOnboardTriggerMockHTTPGenerator());
        Customer_Onboard__c cobo = new Customer_Onboard__c(Region__c = 'AMERICAS');
        insert cobo;
        cobo.PDCL_File_Name__c = 'test.json';
        cobo.PDCL_File_URL__c = 'https://example.com';
        cobo.Pre_Site_Completion_Date__c = system.now();
        update cobo;
        test.stopTest();
        //verify that a file attachement has been made
        List<Attachment> lstAtt = [SELECT Id FROM Attachment WHERE ParentId = :cobo.Id];
        system.assertEquals(1, lstAtt.size());
        
    }

	@isTest static void test_CoboContactRequested()
	{
		Account ac = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert ac;
		Contact c = new Contact(FirstName = 'J', LastName = 'Doe', Email = 'jdoe@example.com', AccountId = ac.Id, Portal_Status__c = 'Portal User');
		insert c;
		Customer_Onboard__c cobo = new Customer_Onboard__c(RecordTypeId = Schema.SObjectType.Customer_Onboard__c.getRecordTypeInfosByName().get('COBO-F').getRecordTypeId());
		insert cobo;
		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(OnBoard_Contact__c = c.Id, Customer_Onboard__c = cobo.Id);
		Oracle_Order__c oo = new Oracle_Order__c();
		insert new List<SObject>{obc,oo};
		Test.startTest();
		cobo.Oracle_Order__c = oo.Id;
		update cobo;
		Test.stopTest();
		Contact c1 = [SELECT Portal_Status__c FROM Contact WHERE Id=:c.Id];
		system.assertEquals('Requested', c1.Portal_Status__c);
		

	}

	@isTest static void test_statusUpdates()
	{
		Customer_Onboard__c cobo = new Customer_Onboard__c(Status__c = 'New');
        insert cobo;
		Oracle_Order__c oo = new Oracle_Order__c();
        insert oo;
		cobo.Oracle_Order__c = oo.Id;
		update cobo;
		Customer_Onboard__c cobo1 = [SELECT Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals('Order Confirmed', cobo1.Status__c);
		//update the NES to test that the cobo is closed
		cobo.Status__c = 'Success Confirmation';
		cobo.NES_Survey__c = '1';
		update cobo;
		Customer_Onboard__c cobo2 = [SELECT Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals('Closed', cobo2.Status__c);
	}

	@isTest static void test_closedTrainingCaseAdded()
	{
		Customer_Onboard__c cobo = new Customer_Onboard__c(Status__c = 'Application Training');
		insert cobo;
		Case trnCase = new Case(Subject = 'TestTrainingCase', Description = 'Test', 
		RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId(),
		Status = 'Closed');
		insert trnCase;
		cobo.Training_Case__c = trnCase.Id;
		update cobo;
		//test that the status has been updated
		Customer_Onboard__c cobo1 = [SELECT Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals('Success Confirmation', cobo1.Status__c);
	}

	@isTest static void test_closedInstallCaseAdded()
	{
		Customer_Onboard__c cobo = new Customer_Onboard__c(Status__c = 'Installation');
		insert cobo;
		Case insCase = new Case(Subject = 'TestInstallCase', Description = 'Test', 
		RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
		Status = 'Closed');
		Case trnCase = new Case(Subject = 'TestTrainingCase', Description = 'Test', 
		RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId());
		insert new List<Case> {insCase,trnCase};
		cobo.InstallationCase__c = insCase.Id;
		cobo.Training_Case__c = trnCase.Id;
		update cobo;
		//test that the status has been updated
		Customer_Onboard__c cobo1 = [SELECT Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals('Application Training', cobo1.Status__c);
		}

	@IsTest static void test_installCaseChanged()
	{
		Test.startTest();
		Account ac = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		SVMXC__Service_Group__c sg = new SVMXC__Service_Group__c();
		insert new List<SObject> {ac, sg};
		SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c(SVMXC__Service_Group__c = sg.Id);
		Asset a = new Asset(Name = 'TestAsset', AccountId = ac.Id);
		insert new List<sobject> {tech,a};
		Customer_Onboard__c cobo = new Customer_Onboard__c(Status__c = 'Installation', Asset__c = a.Id);
		insert cobo;
		Case insCase = new Case(Subject = 'TestInstallCase', Description = 'Test', 
		RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
		SVC_Primary_FSE__c = tech.Id, 
		Status = 'New');
		Case trnCase = new Case(Subject = 'TestTrainingCase', Description = 'Test', 
		RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId());
		insert new List<Case> {insCase,trnCase};
		cobo.Training_Case__c = trnCase.Id;
		update cobo;
		cobo.InstallationCase__c = insCase.Id;
		update cobo;
		Test.stopTest();
		trnCase = [SELECT AssetId, Installation_Case__c, SVC_Primary_FSE__c FROM Case WHERE Id = :trnCase.Id];
		System.assertEquals(a.Id, trnCase.AssetId);
		System.assertEquals(insCase.Id, trnCase.Installation_Case__c);
		System.assertEquals(tech.Id, trnCase.SVC_Primary_FSE__c);

	}
	
}