public class NewCaseCommentController
{
 /*  private Case parentCase;
   private CaseComment comment;
   private Time_Spent__c timeSpent;
   private Integer hours;
   private Integer minutes;
   private Boolean errorNoTimeSpent = false;

   public Case getParentCase()
   {
      return parentCase;
   }
   
   // initialize action defined in the page
   public void init()
   {
      System.debug('>>init()');
      
      // load the case
      String caseId = System.currentPageReference().getParameters().get('caseId');
      System.debug('Processing case: ' + caseId);
      
      // Check for inability to find the case - there is some use case that was causing a problem
      // with case retrieval
      try
      {      
        parentCase = [select Id, CaseNumber, Subject, Description
                    from Case where Id = :caseId];
      }
      catch (QueryException qe)
      {
          System.debug(LoggingLevel.ERROR, 'Unable to retrieve case with id ' + caseId);
      }
      
      // See if this is an edit of an existing comment
      String commentId = System.currentPageReference().getParameters().get('commentId');
      System.debug('Existing case comment: ' + commentId);
      if (commentId != null)
      {
         // load the existing comment
         comment = [select Id, ParentId, IsPublished, CommentBody
                    from CaseComment where Id = :commentId];
         
         // try to load an existing time spent object
         try 
         {
            //timeSpent = [select Id, Amount__c, Case_Comment_Id__c from Time_Spent__c
            timeSpent = [select Id, Time_in_Minutes__c, Case_Comment_Id__c, Case__c from Time_Spent__c
                         where Case_Comment_Id__c = :comment.Id];
            
            //System.debug('Found time spent with amount = ' + timeSpent.Amount__c);
         }
         catch (QueryException qe)
         {
            // no time spent object - ignore
            System.debug('No existing time spent for comment ' + commentId);
         }
      }
      else
      {
         // no existing comment - create a new one
         comment = new CaseComment();
         comment.ParentId = caseId;
         // default to private
         comment.IsPublished = false;
      }
      
      if (timeSpent == null)
         timeSpent = new Time_Spent__c();
      
      System.debug('<<init()');
   }
   
   public Boolean getErrorNoTimeSpent()
   {
      return errorNoTimeSpent; 
   }

   public Time_Spent__c getTimeSpent()
   {
      return timeSpent;
   }

   public CaseComment getComment()
   {
      return comment;
   }
     
   public Integer getNumHours()
   {
      Double min = timeSpent.Time_in_Minutes__c;
      return (min == null || min == 0) ? 0 : Math.floor(min / 60).intValue();
   }
   
   public Integer getNumMinutes()
   {
      Double min = timeSpent.Time_in_Minutes__c;
      return (min == null || min == 0) ? 0 : Math.mod(min.longValue(), 60).intValue();
   }
   
   public void setNumHours(Integer hrs)
   {
      hours = hrs;
   } 
   
   public void setNumMinutes(Integer min)
   {
      minutes = min;
   }

   public PageReference save() 
   {
      if ((hours == null || hours == 0) && (minutes == null || minutes == 0))
      {
         errorNoTimeSpent = true;
         return null;
      }
      
      // If there is an id, this is an update of an existing comment
      if (comment!=null && comment.Id != null)
         update comment;
      // otherwise, it's a new comment
      else if (comment!=null)
         insert comment;

      timeSpent.Time_In_Minutes__c = hours * 60 + minutes; 
      
      if (timeSpent.Id != null)
         update timeSpent;
      else
      {
         if (parentCase!=null && comment!=null)
         {     
             timeSpent.Case_Comment_Id__c = comment.Id;
             timeSpent.Case__c = parentCase.Id;
             insert timeSpent;
         }
      }

      return getReturnReference();
   }

   public PageReference cancel()
   {
      return getReturnReference();
   }
   
   private PageReference getReturnReference()
   {
      String retUrl = System.currentPageReference().getParameters().get('returnUrl');
      if ((retUrl == null || retUrl.length() == 0))
      {
      
          if (getParentCase()==null)              
              retUrl = '/';
          else
             // if none specified, return to the case
             retUrl = '/' + getParentCase().Id;
      }
         
      return new PageReference(retUrl);
   }

   // return existing comments for the related list
   public List<CaseComment> getExistingComments()
   {
      return [select Id, IsPublished, CommentBody, CreatedDate, CreatedById, CreatedBy.Name,
              LastModifiedBy.Name, LastModifiedDate, LastModifiedById
              from CaseComment
              where ParentId = :System.currentPageReference().getParameters().get('caseId')
              order by LastModifiedDate desc];
   }
   
    public static testMethod void testIt()
    {
        // create a test case
        Case c = new Case();
        c.Subject = 'Test case';
        c.Description = 'testing';
        c.Non_AB_Product__c = true;
        insert c;
    
        System.currentPageReference().getParameters().put('caseId', c.Id);
        
        NewCaseCommentController controller = new NewCaseCommentController();
        controller.init();
        controller.getExistingComments();
        controller.cancel();
        controller.getParentCase();
        controller.setNumHours(0);
        controller.setNumMinutes(0);
        controller.save();
        System.assert(controller.getErrorNoTimeSpent() == true);
        controller.setNumMinutes(1);
        controller.getComment().CommentBody = 'testing';
        controller.save();
        System.currentPageReference().getParameters().put('commentId', controller.getComment().Id);
        controller.init();
        controller.getNumMinutes();
        controller.getNumHours();
        System.assert(controller.getTimeSpent() != null);
        controller.setNumHours(2);
        controller.save();

        CaseComment cc = new CaseComment(parentId=c.Id);
        cc.IsPublished = false;
        cc.CommentBody = 'test comment';
        insert cc;
        System.currentPageReference().getParameters().put('commentId', cc.Id);
        controller.init();

 
        
    }
    
    public static testMethod void testNullCaseId()
    {
        // create a test case
        Case c = new Case();
        c.Subject = 'Test case';
        c.Description = 'testing';
        c.Non_AB_Product__c = true;
        insert c;
    
        // set a null case id
        System.currentPageReference().getParameters().put('caseId', null);        
        NewCaseCommentController controller = new NewCaseCommentController();
        controller.getExistingComments();
        controller.cancel();
        controller.getParentCase();
        controller.setNumHours(0);
        controller.setNumMinutes(0);
        controller.save();
 
        
    }    */
}