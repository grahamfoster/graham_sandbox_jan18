/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Apr 18/2014
 **
 ** Interface for paging/sorting
 ** 
**/
public interface Pager {

	void fetchRecords();
	List<Opportunity> getReportOpps();
	String getReport();

	PageReference nextAction();
	PageReference previousAction();
	PageReference sortAction();

	void setPageNumber(Integer pageNumber);
	Integer getPageNumber();
	void setPageSize(Integer pageSize);
	Integer getPageSize();
	void setTotalNumberOfRecords(Integer totalNumberOfRecords);
	Integer getTotalNumberOfRecords();
	Integer getNumberOfPages();
	Boolean getHasNextPage();
	Boolean getHasPreviousPage();

	void setSortColumn1(String sortColumn);
	String getSortColumn1();
	void setSortDirection1(String sortDirection);
	String getSortDirection1();
	void setSortColumn2(String sortColumn);
	String getSortColumn2();
	void setSortDirection2(String sortDirection);
	String getSortDirection2();
}