/*
 *	SurveyFollowupTrigger_Test
 *	
 *	Test class for SurveyFollowupTrigger and SurveyFollowupHandler.
 *
 *	If there are other test classes related to SurveyFollowupTrigger, please document it here (as comments).
 * 
 * 	Created by Brett Moore 2016-3-26 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
public class SurveyFollowupTrigger_Test {

	// Prepare initial sample data for running tests:
    @testSetup static void setup() {
   	    // Populate all referenced objects needed to create a WO Note record
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
        insert u;  
        Pricebook2 pb = new Pricebook2 (Name='United States Price Book 2',CURRENCYISOCODE='USD',isActive=TRUE);
		insert pb;
        Country_Mapping__c countryM = new Country_Mapping__c(Name='United States',Permutations__c='UNITED STATES; USA; US; UNITED STATES OF AMERICA',
                Country_Code__c='US',Country__c='United States',Support_Region__c='AMERICAS',Default_Service_Price_Book__c=pb.id);
        insert countryM;
    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'United States',CurrencyIsoCode = 'USD');
        insert testAccount;
        testAccount = [SELECT Name, Country_Mapping__c, BillingCountry ,CurrencyIsoCode, Country_Mapping__r.Default_Service_Price_Book__c FROM account LIMIT 1];
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = testAccount.Id);
        insert testContact;
 		Product2 prod = new Product2(Name='ABSX WARRANTY 2',ProductCode='ABSX WARRANTY 2');
        insert prod;       
		SVMXC__Installed_Product__c comp = New SVMXC__Installed_Product__c ( NAME='API3000 - AF28211410',
                                                                            CURRENCYISOCODE ='USD',	
                                                                            SVMXC__COMPANY__C=testAccount.Id,	
                                                                            SVMXC__PRODUCT__C=prod.id,	
                                                                            SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', 
                                                                            SVC_Operating_Unit__c = 'ABC');        
        insert comp;
        SVMXC__Site__c site = new SVMXC__Site__c(Name = 'Test Location',SVMXC__Account__c = testAccount.id,SVMXC__Stocking_Location__c = true,
                                SVMXC__State__c = 'NY',SVMXC__Service_Engineer__c = u.Id,SVMXC__Country__c = 'United States',
                                SVMXC__Zip__c = '12345');
        insert site;         
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c(
                                                    Name = 'ABC',
                                                    SVMXC__Active__c = true,
                                                    SVMXC__State__c = 'NY',
                                                    SVMXC__Country__c = 'United States',
                                                    SVMXC__Zip__c = '12345');
        insert serviceTeam;
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(
                                                        SVMXC__Active__c = true,
                                                        Name = 'Test Technician',
                                                        SVMXC__Service_Group__c = serviceTeam.Id,
                                                        SVMXC__Salesforce_User__c = u.Id,
                                                        SVMXC__Inventory_Location__c = site.Id);
        insert technician;
        RecordType rt = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
        Case ccase = new Case(RecordTypeId = rt.Id, AccountId = testAccount.Id, CurrencyIsoCode = 'USD', SVMXC__Component__c = comp.Id);
        insert ccase;
		Qualtrics_Survey_Result__c sfu = new Qualtrics_Survey_Result__c(Case__c = ccase.id,Contact__c=testContact.id,Question_Text__c='my question',Response_Id__c='1234',Response_Recode_Number__c=2,SurveyID__c='abcd',SurveyTitle__c='my surevy');
        insert sfu;
        
    }
    
    
    
     @isTest  static void SurveyFollowupIUDUTest() {
        List<Case> c = new List<Case>([SELECT id FROM Case]); 	
        // Create the Work detail object
 		Survey_Follow_Up__c fu = new Survey_Follow_Up__c(Case__c = c[0].id,ResponseId__c='1234',Survey_ID__c='abcd',Survey_Title__c='my surevy');
        //Start Test
        test.startTest();
			insert fu;       			
        	update fu;
        	delete fu;
        	undelete fu;
        test.stopTest();        
    }

	@isTest static void assignFollowUp_Test()
	{
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		List<User> followUsers = new List<User>();
		followUsers.Add(new User(Alias = 'TAC', Email='TAC@testorg.com', EmailEncodingKey='UTF-8', LastName='TAC', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='TAC@testorg.test'));
		followUsers.Add(new User(Alias = 'SAS', Email='SAS@testorg.com', EmailEncodingKey='UTF-8', LastName='SAS', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='SAS@testorg.test'));
		followUsers.Add(new User(Alias = 'POR', Email='POR@testorg.com', EmailEncodingKey='UTF-8', LastName='TRN', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='POR@testorg.test'));
		followUsers.Add(new User(Alias = 'TRN', Email='SAS@testorg.com', EmailEncodingKey='UTF-8', LastName='TRN', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='TRN@testorg.test'));
		followUsers.Add(new User(Alias = 'SAC', Email='SAC@testorg.com', EmailEncodingKey='UTF-8', LastName='TRN', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='SAC@testorg.test'));
		followUsers.Add(new User(Alias = 'COM', Email='COM@testorg.com', EmailEncodingKey='UTF-8', LastName='TRN', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='COM@testorg.test'));
		followUsers.Add(new User(Alias = 'SEP', Email='SEP@testorg.com', EmailEncodingKey='UTF-8', LastName='TRN', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='SEP@testorg.test'));
		
		INSERT followUsers;
		
		Country_Mapping__c countryM = [SELECT Id FROM Country_Mapping__c WHERE Name='United States' LIMIT 1];

		countryM.SAS_Follow_Up_User__c = followUsers[1].Id;
		countryM.TAC_Follow_Up_User__c = followUsers[0].Id; 
		countryM.Training_Follow_Up_User__c = followUsers[3].Id;
		countryM.Portal_Follow_Up_User__c = followUsers[2].Id;
		countryM.Software_Activation_Follow_Up_User__c = followUsers[4].Id;
		countryM.Compliance_Survey_Follow_Up_User__c = followUsers[5].Id;
		countryM.CE_Training_Follow_Up_User__c = followUsers[6].Id;

		UPDATE countryM;

		List<Contact> con = [SELECT Id, AccountId FROM Contact WHERE FirstName = 'Test' AND LastName = 'Contact' LIMIT 1];
		List<Case> cas = new List<Case>();

		RecordType rt = SfdcUtil.getRecordType('Case', 'CICCase');
		List<Case> newCases = new List<Case>();
        newCases.add(new Case(RecordTypeId = rt.Id, AccountId = con[0].AccountId, ContactId = con[0].Id, Type = 'TAC'));
		newCases.add(new Case(RecordTypeId = rt.Id, AccountId = con[0].AccountId, ContactId = con[0].Id, Type = 'SAS'));
		newCases.add(new Case(RecordTypeId = rt.Id, AccountId = con[0].AccountId, ContactId = con[0].Id, Type = 'PortalTest'));
		newCases.add(new Case(RecordTypeId = rt.Id, AccountId = con[0].AccountId, ContactId = con[0].Id, Type = 'TrainingTest'));
		newCases.add(new Case(RecordTypeId = rt.Id, AccountId = con[0].AccountId, ContactId = con[0].Id, Type = 'CETrainingTest'));

       INSERT newCases;

	   List<Survey_Follow_Up__c> followUps = new List<Survey_Follow_Up__c>();

	   followUps.add(new Survey_Follow_Up__c(Case__c = newCases[0].Id, Contact__c = con[0].Id, Survey_Title__c = 'SCIEXNow: Post Case'));
	   followUps.add(new Survey_Follow_Up__c(Case__c = newCases[1].Id, Contact__c = con[0].Id, Survey_Title__c = 'SCIEXNow: Post Case'));
	   followUps.add(new Survey_Follow_Up__c(Case__c = newCases[2].Id, Contact__c = con[0].Id, Survey_Title__c = 'SCIEXNow: Portal'));
	   followUps.add(new Survey_Follow_Up__c(Case__c = newCases[3].Id, Contact__c = con[0].Id, Survey_Title__c = 'Global Post FAS Training Survey'));
	   followUps.add(new Survey_Follow_Up__c(Case__c = newCases[4].Id, Contact__c = con[0].Id, Survey_Title__c = 'Customer Training Separations'));
	   followUps.add(new Survey_Follow_Up__c(Contact__c = con[0].Id, Survey_Title__c = 'Software Product Activation 90days'));
	   followUps.add(new Survey_Follow_Up__c(Contact__c = con[0].Id, Survey_Title__c = 'Compliance Services'));

	   INSERT followUps;

	   Set<Id> fUps = new Set<Id>{followUps[0].Id, followUps[1].Id, followUps[2].Id, followUps[3].Id, followUps[4].Id, followUps[5].Id, followUps[6].Id};

	   Map<Id, Survey_Follow_Up__c> insertedRecords = new Map<Id, Survey_Follow_Up__c>(
	   [SELECT Id, OwnerId FROM Survey_Follow_Up__c WHERE Id IN :fUps]);

	   Survey_Follow_Up__c TacTest = insertedRecords.get(followUps[0].Id);
	   system.assertEquals(TacTest.OwnerId, countryM.TAC_Follow_Up_User__c);

	   Survey_Follow_Up__c SASTest = insertedRecords.get(followUps[1].Id);
	   system.assertEquals(SASTest.OwnerId, countryM.SAS_Follow_Up_User__c);

	   Survey_Follow_Up__c PORTest = insertedRecords.get(followUps[2].Id);
	   system.assertEquals(PORTest.OwnerId, countryM.Portal_Follow_Up_User__c);

	   Survey_Follow_Up__c TRNTest = insertedRecords.get(followUps[3].Id);
	   system.assertEquals(TRNTest.OwnerId, countryM.Training_Follow_Up_User__c);

	   Survey_Follow_Up__c CETest = insertedRecords.get(followUps[4].Id);
	   system.assertEquals(CETest.OwnerId, countryM.CE_Training_Follow_Up_User__c);

	   Survey_Follow_Up__c SACTest = insertedRecords.get(followUps[5].Id);
	   system.assertEquals(SACTest.OwnerId, countryM.Software_Activation_Follow_Up_User__c);

	   Survey_Follow_Up__c COMTest = insertedRecords.get(followUps[6].Id);
	   system.assertEquals(COMTest.OwnerId, countryM.Compliance_Survey_Follow_Up_User__c);

	}
}