@isTest
private class case_splitTrainingCase_Test {

	@isTest
	private static void test_createTheCase(){

		Customer_Onboard__c co = new Customer_Onboard__c();
		Oracle_Order__c oo = new Oracle_Order__c();
		Product2 trainingProd = new Product2(Name='TestTrainingPart', Description = 'TestTrainingPart');
		Account a = new Account(Name = 'TestAccount', BillingCountry = 'Canada');
		insert new List<SObject>{co,oo,a};
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email = 'test@example.com');
		Contact con2 = new Contact(FirstName = 'Test2', LastName = 'Contact2', AccountId = a.Id, Email = 'test2@example.com');
		Contact con3 = new Contact(FirstName = 'Test3', LastName = 'Contact3', AccountId = a.Id, Email = 'test3@example.com');
		insert new List<Contact>{c,con2,con3};

		Asset asst = new Asset(Name = 'TestAsset', AccountId = a.Id, ContactId = c.Id);
		insert asst;

		Case cSer = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(),
		ContactId = c.Id, Subject = 'serTest', Description = 'TestService');
		
		Case c1 = new Case(RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId(),
		ContactId = c.Id, Subject = 'Test', Description = 'Case', Origin = 'Standalone', Customer_Onboard__c = co.Id,
		Oracle_Order__c = oo.Id, Additional_Training_Product__c = trainingProd.Id, AssetId = asst.Id);

		insert new List<Case> {cSer,c1};

		Case_Contact__c cc = new Case_Contact__c(Case__c = c1.Id, Contact__c = c.Id);
		Case_Contact__c cc2 = new Case_Contact__c(Case__c = c1.Id, Contact__c = con2.Id);
		Case_Contact__c cc3 = new Case_Contact__c(Case__c = c1.Id, Contact__c = con3.Id);
		
		insert new List<Case_Contact__c>{cc,cc2,cc3};

		Test.startTest();
		String caseId = case_splitTrainingCase.createTheCase(c1.Id);

		//test that we have created a case
		System.assert(String.isNotBlank(caseId));
		Case cNew = [SELECT Training_Part__c FROM Case WHERE Id = :caseId];
		System.assertEquals(trainingProd.Id, cNew.Training_Part__c);
		//check that the new case has the Case Contact Records
		List<Case_Contact__c> newCaseContacts = [SELECT Id FROM Case_Contact__c WHERE Case__c = :caseId];
		System.assertEquals(3, newCaseContacts.size());
		Test.stopTest();

	}
}