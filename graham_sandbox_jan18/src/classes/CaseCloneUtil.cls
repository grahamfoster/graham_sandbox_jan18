/**
** @author Reid Beckett, Cloudware Connections
** @created Sep 11/2015
** 
** This class takes care of cloning a case record.
** Cloning occurs when:
** 1) Case changes to closed (IsClosed=true) and has Origin like "Email%"
** 2) When the custom field "TriggerClone__c" is updated to true.
**/
public class CaseCloneUtil 
{
    /**
     * Fields in this set will be cloned.
     **/
    @TestVisible
    private static final Set<String> CLONE_FIELDS = new Set<String>
    {
        Case.AccountId.getDescribe().getName(),
        Case.ContactId.getDescribe().getName(),
        Case.AssetId.getDescribe().getName(),
        Case.ParentId.getDescribe().getName(),
        Case.SuppliedName.getDescribe().getName(),
        Case.SuppliedEmail.getDescribe().getName(),
        Case.SuppliedPhone.getDescribe().getName(),
        Case.SuppliedCompany.getDescribe().getName(),
        Case.Type.getDescribe().getName(),
        Case.RecordTypeId.getDescribe().getName(),
        Case.Reason.getDescribe().getName(),
        Case.Origin.getDescribe().getName(),
        Case.IsVisibleInSelfService.getDescribe().getName(),
        Case.Subject.getDescribe().getName(),
        Case.Description.getDescribe().getName(),
        Case.IsEscalated.getDescribe().getName(),
        Case.HasCommentsUnreadByOwner.getDescribe().getName(),
        Case.HasSelfServiceComments.getDescribe().getName(),
        Case.CurrencyIsoCode.getDescribe().getName(),
        Case.OwnerId.getDescribe().getName(),
        Case.Case_Asset_Account__c.getDescribe().getName(),
        Case.Case_Asset_Name__c.getDescribe().getName(),
        Case.Case_Asset_Status__c.getDescribe().getName(),
        Case.Case_Contract_Number__c.getDescribe().getName(),
        Case.Case_Equipment_Number__c.getDescribe().getName(),
        Case.Case_Type__c.getDescribe().getName(),
        Case.Complaint_Code__c.getDescribe().getName(),
        Case.Potential_Safety_Issue__c.getDescribe().getName(),
        Case.Issue_Category__c.getDescribe().getName(),
        Case.Software_Escalation_Start_Date__c.getDescribe().getName(),
        Case.Contract_Expiry__c.getDescribe().getName(),
        Case.Contract_Type__c.getDescribe().getName(),
        Case.Customer_Feedback__c.getDescribe().getName(),
        Case.Bug_Fix_Reference__c.getDescribe().getName(),
        Case.Data_Quality_Description__c.getDescribe().getName(),
        Case.Data_Quality_Score__c.getDescribe().getName(),
        Case.Software_Name__c.getDescribe().getName(),
        Case.Software_Version__c.getDescribe().getName(),
        Case.Escalation_Level__c.getDescribe().getName(),
        Case.FSE_Responsible__c.getDescribe().getName(),
        Case.Follow_Up_Actions__c.getDescribe().getName(),
        Case.Goodwill_Confirmation_Number__c.getDescribe().getName(),
        Case.Hardware__c.getDescribe().getName(),
        Case.Complaint__c.getDescribe().getName(),
        Case.KOL__c.getDescribe().getName(),
        Case.Key_Account__c.getDescribe().getName(),
        Case.Potential_Orders__c.getDescribe().getName(),
        Case.Account_Country__c.getDescribe().getName(),
        Case.Lot_Number__c.getDescribe().getName(),
        Case.Mode_of_Contact__c.getDescribe().getName(),
        Case.Model__c.getDescribe().getName(),
        Case.Non_AB_Product__c.getDescribe().getName(),
        Case.Operating_System__c.getDescribe().getName(),
        Case.PO_Number__c.getDescribe().getName(),
        Case.Operating_System__c.getDescribe().getName(),
        Case.Part_Number__c.getDescribe().getName(),
        Case.Problem_Type__c.getDescribe().getName(),
        Case.Product_Status_1__c.getDescribe().getName(),
        Case.Product_Status_2__c.getDescribe().getName(),
        Case.Product_Status_3__c.getDescribe().getName(),
        Case.Product_Status_4__c.getDescribe().getName(),
        Case.Product_Status_5__c.getDescribe().getName(),
        Case.Product__c.getDescribe().getName(),
        Case.Promo_Code__c.getDescribe().getName(),
        Case.QNote_Number__c.getDescribe().getName(),
        Case.Quantity__c.getDescribe().getName(),
        Case.SAP_SO_Number__c.getDescribe().getName(),
        Case.Contract_Warranty__c.getDescribe().getName(),
        Case.Billable__c.getDescribe().getName(),
        Case.Service_Reference_Number__c.getDescribe().getName(),
        Case.FAS_Days_Purchased_Granted__c.getDescribe().getName(),
        Case.FAS_Days_Remaining__c.getDescribe().getName(),
        Case.Market_Vertical__c.getDescribe().getName(),
        Case.Service_Notification_Number__c.getDescribe().getName(),
        Case.Service_Order_Number__c.getDescribe().getName(),
        Case.Software_Version_Other__c.getDescribe().getName(),
        Case.Solution__c.getDescribe().getName(),
        Case.Asset_New__c.getDescribe().getName(),
        Case.Product_New__c.getDescribe().getName(),
        Case.Sales_Order_Number__c.getDescribe().getName(),
        Case.Web_Address__c.getDescribe().getName(),
        Case.Web_City__c.getDescribe().getName(),
        Case.Web_Country__c.getDescribe().getName(),
        Case.Web_State_Province__c.getDescribe().getName(),
        Case.Web_Zip_Postal_Code__c.getDescribe().getName(),
        Case.Region__c.getDescribe().getName(),
        Case.Oracle_Reference__c.getDescribe().getName(),
        Case.CaseNumber.getDescribe().getName()
    };
        
    public class CloneException extends Exception {}
    public class CloneLimitsException extends Exception {}
    
    private static Set<Id> clonedCases = new Set<Id>();
    private static Set<Id> casesTriggeredToClone = new Set<Id>();
    private Map<Id, Case> clonesByOriginalCase = new Map<Id, Case>();
    
    public void onBeforeUpdate()
    {
        if(Trigger.isUpdate)
        {
            //collect the case IDs to query by original case
            Set<Id> caseIds = collectIds(Trigger.new);
            
            //collect clones by original case so that we don't clone a case twice
            for(Case c : [select Id, Original_Case__c from Case where Original_Case__c in :caseIds])
            {
                clonesByOriginalCase.put(c.Original_Case__c, c);
            }
        }
        
        for(Case c : (List<Case>)Trigger.new)
        {
            if(Trigger.isUpdate)
            {
                Case oldCase = (Case)Trigger.oldMap.get(c.Id);
                if(triggerClone(oldCase, c))
                {
                    //Oct22 - don't reset to false
                    //c.TriggerClone__c = false;
                    casesTriggeredToClone.add(c.Id);
                }
            }
        }
    }
    
    public void onAfterUpdate()
    {
        system.debug('***** CaseCloneUtil.onAfterUpdate()');
        system.debug('***** ' + Trigger.new);
        Set<Id> cloneIds = new Set<Id>();
        for(Case c : (List<Case>)Trigger.new)
        {
            if(Trigger.isUpdate)
            {
                if(casesTriggeredToClone.contains(c.Id))
                {
                    cloneIds.add(c.Id);
                }
            }
        }
        
        //batch up the clones by batch size
        boolean hitLimits = false;
        for(Id caseId : cloneIds)
        {
            try {
                if(!clonedCases.contains(caseId))
                {
                    if(!hitLimits) {
                        system.debug('***** cloning case '+caseId);
                        Case clonedCase = cloneCase(caseId);
                        clonedCases.add(caseId);
                    } else {
                        futureCloneCase(caseId);
                    }
                }
            }catch(CloneLimitsException e) {
                hitLimits = true;
                futureCloneCase(caseId);
            }
        }
    }
    
    private Set<Id> collectIds(List<sObject> sobjs)
    {
        Set<Id> ids = new Set<Id>();
        for(sObject sobj : sobjs)
        {
            ids.add(sobj.Id);
        }
        return ids;
    }

    private Boolean triggerClone(Case oldCase, Case newCase)
    {
        /*return (!oldCase.IsClosed && newCase.IsClosed && newCase.Origin != null && newCase.Origin.startsWith('Email')) ||
            (newCase.TriggerClone__c && (oldCase.TriggerClone__c == null || !oldCase.TriggerClone__c));*/
        return !clonesByOriginalCase.containsKey(oldCase.Id) && (newCase.TriggerClone__c && (oldCase.TriggerClone__c == null || !oldCase.TriggerClone__c));
    }
    
    @future
    public static void futureCloneCase(Id caseId)
    {
        system.debug('***** future cloning case '+caseId);
        Case clonedCase = new CaseCloneUtil().cloneCase(caseId);
        clonedCases.add(caseId);
    }
    
    public Case cloneCase(Id caseId)
    {
        if(Limits.getQueries() == Limits.getLimitQueries()) throw new CloneLimitsException();
        Case sourceCase = findCase(caseId);

        if(sourceCase == null) {
            throw new CloneException('Case with Id ' + caseId + ' not found');
        } else {
            Case clonedCase = sourceCase.clone(false,true);
            clonedCase.Original_Case__c = sourceCase.Id;
            insertSobject(clonedCase);
            system.debug('clonedCase.Id='+clonedCase.Id);
            //clone relationships
            //Email messages
            EmailMessage[] clonedEmailMessages = new EmailMessage[]{};
            for(EmailMessage em : sourceCase.EmailMessages) {
                EmailMessage clonedEmailMessage = em.clone(false,true);
                system.debug('setting ParentId ='+clonedCase.Id);
                clonedEmailMessage.ParentId = clonedCase.Id;
                clonedEmailMessages.add(clonedEmailMessage);
            }
            insertSobjects(clonedEmailMessages);

            //case comments
            CaseComment[] clonedCaseComments = new CaseComment[]{};
            for(CaseComment cc : sourceCase.CaseComments) {
                CaseComment clonedCaseComment = cc.clone(false,true);
                system.debug('setting ParentId ='+clonedCase.Id);
                clonedCaseComment.ParentId = clonedCase.Id;
                clonedCaseComments.add(clonedCaseComment);
            }
            
            clonedCaseComments.add(new CaseComment(ParentId = clonedCase.Id, CommentBody = 'Original Case #: '+ sourceCase.CaseNumber, IsPublished=true));

            insertSobjects(clonedCaseComments);
            
            //Attachments - must be queried one by one
            Attachment[] clonedAttachments = new Attachment[]{};
            for(Attachment att : sourceCase.Attachments)
            {
                Id attId = att.Id;
                String attachmentSOQL = generateChildSOQLForAttachments(true, 'Attachment') + ' where Id = :attId';
                if(Limits.getQueries() == Limits.getLimitQueries()) throw new CloneLimitsException();
                Attachment attWithBody = Database.query(attachmentSOQL);
                Attachment newAttachment = attWithBody.clone(false, true);
                system.debug('setting ParentId ='+clonedCase.Id);
                newAttachment.ParentId = clonedCase.Id;
                clonedAttachments.add(newAttachment);
            }
            system.debug('inserting cloned attachments...');
            system.debug(clonedAttachments);

            insertSobjects(clonedAttachments);
            
            //Case contacts
            Set<Id> contactIds = new Set<Id>();
            List<CaseContactRole> clonedCaseContactRoles = new List<CaseContactRole>();
            for(CaseContactRole ccr : sourceCase.CaseContactRoles)
            {
                CaseContactRole ccrClone = ccr.clone(false,true);
                ccrClone.CasesId = clonedCase.Id;
                clonedCaseContactRoles.add(ccrClone);
            }
            
            insertSobjects(clonedCaseContactRoles);
            
            //ContentDocumentLinks
            ContentDocumentLink[] cdlink_clones = new ContentDocumentLink[]{};
            for(ContentDocumentLink cdlink : [select ContentDocumentId, Id, IsDeleted, LinkedEntityId, ShareType, SystemModstamp, Visibility from ContentDocumentLink where LinkedEntityId = :sourceCase.Id])
            {
                ContentDocumentLink cdlink_clone = cdlink.clone(false,true);
                cdlink_clone.LinkedEntityId = clonedCase.Id;
                cdlink_clones.add(cdlink_clone);
            }
            insertSobjects(cdlink_clones);
            
            return clonedCase;
        }
    }
    
    private void insertSobject(sObject sobj)
    {
        if(Limits.getDMLStatements() == Limits.getLimitDMLStatements()) throw new CloneLimitsException();
        insert sobj;
    }

    private void insertSobjects(List<sObject> sobjs)
    {
        if(Limits.getDMLStatements() == Limits.getLimitDMLStatements()) throw new CloneLimitsException();
        insert sobjs;
    }
    
    private Case findCase(Id caseId)
    {
        String soql = generateSOQL();
        system.debug(soql);
        Case[] sourceCases = Database.query(soql);
        return sourceCases.size() > 0 ? sourceCases[0] : null;
    }
    
    private String generateSOQL()
    {
        String soql = 'select ' + String.join(new List<String>(CLONE_FIELDS), ',');
        //Email messages
        soql += ',(' + generateChildSOQL(EmailMessage.getSobjectType(), 'EmailMessages') + ' order by CreatedDate asc' + ')';
        //Case comments
        soql += ',(' + generateChildSOQL(CaseComment.getSobjectType(), 'CaseComments') + ')';
        //Attachments
        soql += ',(' + generateChildSOQLForAttachments(false, 'Attachments') + ')';
        //Case Contacts
        soql += ',(' + generateChildSOQL(CaseContactRole.getSobjectType(), 'CaseContactRoles') + ')';

        soql += ' from Case where Id = :caseId';
        return soql;
    }
    
    private String generateChildSOQLForAttachments(Boolean includeBody, String relationshipName)
    {
        String soql = 'select ';
        List<String> objFields = new List<String>();
        for(Schema.SobjectField dfr : Attachment.getSobjectType().getDescribe().fields.getMap().values())
        {
            if(includeBody || dfr.getDescribe().getName() != 'Body')
                objFields.add(dfr.getDescribe().getName());
        }
        soql += String.join(objFields,',') + ' from ' + relationshipName;
        return soql;
    }

    private String generateChildSOQL(SobjectType sobjType, String relationshipName)
    {
        String soql = 'select ';
        List<String> objFields = new List<String>();
        for(Schema.SobjectField dfr : sobjType.getDescribe().fields.getMap().values())
        {
            objFields.add(dfr.getDescribe().getName());
        }
        soql += String.join(objFields,',') + ' from ' + relationshipName;
        return soql;
    }
}