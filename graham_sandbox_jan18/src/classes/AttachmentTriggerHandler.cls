/*
 *	AttachmentTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Brett Moore 2016-05-20 based on Framework by Yong Chen 
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public with sharing class AttachmentTriggerHandler extends TriggerHandler {
    
	public AttachmentTriggerHandler() {
		
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void afterInsert() {
    	WorkOrderflagFSRAttachment();
		notifyCaseOwnerOfPortalAttachment();
	}
	override protected void afterUpdate() {
    	
	}
	override protected void afterUndelete() {
    
	}
	override protected void afterDelete() {
    
	}

	// **************************************************************************
	// 		private methods
	// **************************************************************************
	/* 
	 *  WorkOrderflagFSRAttachment - 
	 *     When Attachment is created, check to see if it is linked to the SVMXC__Service_Order__c object
	 *     If so, check filename to determine if it is a FSR document
	 * 	   For FSRs set the FSR_Added__c filed to TRUE  (used to manage email notifications)
	*/
	private void WorkOrderflagFSRAttachment(){
    	Set<Id> workOrders = new Set<Id>();
        string workOrderIdPrefix = Schema.SObjectType.SVMXC__Service_Order__c.getKeyPrefix();
        For(Attachment a :(List<Attachment>)Trigger.new){
            String parentId = a.parentId;
            if(parentId.startsWith(workOrderIdPrefix)){
                if(a.Name.startsWith('Sciex_FSR') &&  a.Name.endsWith('.pdf') ){
                    workOrders.add(a.ParentId);
                }
            }
        }
        // Query SVMXC__Service_Order__c for related records and update
        if(workOrders.size() > 0){
            List<SVMXC__Service_Order__c> woUpdate = ([SELECT id, FSR_Added__c FROM SVMXC__Service_Order__c WHERE id IN : workOrders]);
            For(SVMXC__Service_Order__c WO : woUpdate){
                WO.FSR_Added__c = TRUE;
            }
            update woUpdate;
        }
    }

	private void notifyCaseOwnerOfPortalAttachment()
	{
		String fName = UserInfo.getFirstName();
		String lName = UserInfo.getLastName();

		//if the trigger is not being run by the portal user then we dont need to do anything
		if(fName == 'SCIEXNow' && lName == 'Portal')
		{
			Set<Id> caseIds = new Set<Id>();
			string CasePrefix = Schema.SObjectType.Case.getKeyPrefix();
			//create a set of Id's so we can find the owner Ids for these 
			for(Attachment a : (List<Attachment>)Trigger.new)
			{
				String parentId = a.parentId;
				if(parentId.startsWith(CasePrefix))
				{
					caseIds.add(a.ParentId);
				}	
			}

			if(caseIds.size() > 0)
			{
				//get a map of the cases which we have added attachments so that we can get the owner ids and send an email to them
				Map<Id, Case> casesWithAttachments = new Map<Id, Case>([SELECT Id, OwnerId FROM Case WHERE Id IN :caseIds]);
				string UserPrefix = Schema.SObjectType.User.getKeyPrefix();
				OrgWideEmailAddress owa = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'noreply@sciex.com' LIMIT 1];
				EmailTemplate templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'New_Case_Attachment_From_Portal'];
				List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
				for(Attachment a : (List<Attachment>)Trigger.new)
				{
					String parentId = a.parentId;
					if(parentId.startsWith(CasePrefix) && casesWithAttachments.containsKey(a.ParentId))
					{
						Case c = casesWithAttachments.get(a.ParentId);
						string owner = c.OwnerId;
						//need to make sure its a person and not a queue
						if(owner.startsWith(UserPrefix))
						{
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							mail.setTemplateID(templateId.Id); 
							mail.setSaveAsActivity(false);
							mail.setOrgWideEmailAddressId(owa.id);
							mail.setWhatId(a.parentId);
							mail.setTargetObjectId(c.OwnerId);
							allmsg.add(mail);
						}
					}	
				}
				if(allmsg.size() > 0)
				{
					Messaging.sendEmail(allmsg,false);
				}
			}		
			
		}
	}
	
}