/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created July 27/2014
 **
 ** Overrides the standard products "Edit All" page
 ** May 21/2015 - changed to use a wrapper class to avoid security exception when user doesn't have access to edit Product2 object
 **/
public with sharing class EditAllProductsExtension {
    public Opportunity opp {get; set;}
    //variables used by the setProduct method
    public Id opportunityLineItemId {get; set;}
    public Id pricebookEntryId {get; set;}
    private Map<Id, OpportunityLineItem> updatedLineItems;
    private Map<Id, OpportunityLineItem> oldLineItems;
    public List<LineItemWrapper> lineItems {get; set;}

    public EditAllProductsExtension(ApexPages.StandardController controller) 
    {
        Id oppId = controller.getId();
        opp = [select Id, Name, Pricebook2Id, CurrencyIsoCode,
               (select Id, OpportunityId, UnitPrice, Quantity, Model__c, Type__c, Serial_Number__c, Description, PricebookEntry.Product2.Name, Igor_Class__c, SBQQ__QuoteLine__c, legacy_id__c from OpportunityLineItems)
               from Opportunity where Id = :oppId
        ];
        updatedLineItems = new Map<Id, OpportunityLineItem>();
        oldLineItems = new Map<Id, OpportunityLineItem>();

        this.lineItems = new List<LineItemWrapper>();
        for (OpportunityLineItem lineItem : opp.OpportunityLineItems) 
        {
            this.lineItems.add(new LineItemWrapper(lineItem));
        }
    }

    public PageReference init() 
    {
        return null;
    }

    public PageReference save() 
    {
        String retURL = ApexPages.currentPage().getParameters().containsKey('retURL') ? ApexPages.currentPage().getParameters().get('retURL') : ('/' + opp.Id);

        try 
        {
            //update opp.OpportunityLineItems;
            //May 21 fix to not save bound line items
            OpportunityLineItem[] updates = new OpportunityLineItem[] {};
            for (LineItemWrapper lineItem : lineItems) {
                OpportunityLineItem oli = lineItem.lineItem;
                if (!oldLineItems.containsKey(oli.Id)) {
                    updates.add(new OpportunityLineItem(
                                    Id = oli.Id,
                                    OpportunityId = oli.OpportunityId,
                                    UnitPrice = oli.UnitPrice,
                                    Quantity = oli.Quantity,
                                    Model__c = oli.Model__c,
                                    Type__c = oli.Type__c,
                                    Serial_Number__c = oli.Serial_Number__c,
                                    Description = oli.Description
                                )
                               );
                }
            }
            system.debug('start update');
            update updates;
            
            // do replace on updatedLineItems (delete + insert)
            if (updatedLineItems.size() > 0) 
            {
                OpportunityLineItem[] deletes = new OpportunityLineItem[] {};
                OpportunityLineItem[] inserts = new OpportunityLineItem[] {};
                for (Id oldId : updatedLineItems.keySet()) 
                {
                    OpportunityLineItem newLineItem = updatedLineItems.get(oldId);
                    OpportunityLineItem oldLineItem = oldLineItems.get(oldId);
                    if (oldLineItem != null) 
                    {
                        //copy newly updated values to clone
                        newLineItem.Serial_Number__c = oldLineItem.Serial_Number__c;
                        newLineItem.Description = oldLineItem.Description;
                        newLineItem.Model__c = oldLineItem.Model__c;
                        newLineItem.Type__c = oldLineItem.Type__c;
                        newLineItem.Quantity = oldLineItem.Quantity;
                        newLineItem.UnitPrice = oldLineItem.UnitPrice;
                    }
                    deletes.add(new OpportunityLineItem(Id = oldId));
                    inserts.add(newLineItem);
                }
                delete deletes;
                insert inserts;
            }

            return new PageReference(retURL).setRedirect(true);
        } 
        catch (DmlException e) 
        {
            system.debug(e.getStackTraceString());
            addDMLExceptionMessagesToPage(e);
            return null;
        } 
        catch (Exception e) 
        {
            system.debug(e.getStackTraceString());
            addExceptionMessageToPage(e);
            return null;
        }
    }

    public PageReference setProduct() 
    {
        system.debug('setProduct: opportunityLineItemId = ' + opportunityLineItemId);
        system.debug('setProduct: pricebookEntryId = ' + pricebookEntryId);
        for (OpportunityLineItem lineItem : opp.OpportunityLineItems) 
        {
            if (lineItem.Id == opportunityLineItemId) 
            {
                //create new Line Item in memory from the existing
                OpportunityLineItem newLineItem = lineItem.clone(false, true);
                newLineItem.PricebookEntryId = pricebookEntryId;
                updatedLineItems.put(lineItem.Id, newLineItem);
                oldLineItems.put(lineItem.Id, lineItem);
            }
        }
        return null;
    }

    public class LineItemWrapper 
    {
        public String productName {get; set;}
        public OpportunityLineItem lineItem {get; set;}

        public LineItemWrapper(OpportunityLineItem lineItem) 
        {
            this.lineItem = lineItem;
            this.productName = lineItem.PricebookEntry.Product2.Name;
        }
    }

    @RemoteAction
    public static ProductWrapper[] doSearch(String term, String pricebookId, String currencyIsoCode) 
    {
        String searchTerm = '%' + term + '%';
        String soql = 'select Id, Product2.Name, Product2.ProductCode from PricebookEntry where ' +
            'Pricebook2Id = :pricebookId and Product2.Name like :searchTerm and CurrencyIsoCode = :currencyIsoCode and Isactive = true order by Product2.Name asc limit 15';
        system.debug(soql);
        system.debug('searchTerm=' + searchTerm);
        system.debug('pricebookId=' + pricebookId);
        ProductWrapper[] results = new ProductWrapper[] {};
        for (PricebookEntry pbe : Database.query(soql)) 
        {
            results.add(new ProductWrapper(pbe));
        }
        return results;
    }

    public class ProductWrapper 
    {
        public PricebookEntry pricebookEntry { get; set; }
        public String productName { get; set; }

        public ProductWrapper(PricebookEntry pricebookEntry) 
        {
            this.pricebookEntry = pricebookEntry;
            this.productName = pricebookEntry.Product2.Name;
        }
    }

    /**
    ** When catching a general exception, add the message to the pageMessages
    **/
    public static void addExceptionMessageToPage(Exception e) 
    {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, e.getMessage()));
    }

    /**
    ** When catching a DML exception, add the messages to the pageMessages
    **/
    public static void addDMLExceptionMessagesToPage(DMLException e) 
    {
        for (integer i = 0; i < e.getNumDml(); i++) 
        {
            String errorMessage = 'Row ' + (e.getDmlIndex(i) + 1);
            if (e.getDmlFields(i) != null) errorMessage += '[' + e.getDmlFields(i)[0].getDescribe().getLabel()  + ']';
            errorMessage += + ': ' + e.getDmlMessage(i);
            
            if (e.getDmlType(i) == StatusCode.DUPLICATE_VALUE) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, errorMessage));
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, errorMessage));
            }
        }
    }


}