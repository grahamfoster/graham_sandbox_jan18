//Generated by wsdl2apex

public class QuotePartners {
    public class ArrayOfpartnerType {
        public QuotePartners.partnerType[] ArrayOfpartnerTypeItem;
        private String[] ArrayOfpartnerTypeItem_type_info = new String[]{'ArrayOfpartnerTypeItem','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','partnerType','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','false'};
        private String[] field_order_type_info = new String[]{'ArrayOfpartnerTypeItem'};
    } 
    public class ABI_SalesQuotation_process_loadSAPpartnersInSFDCWsd_Port {
        //public String endpoint_x = 'https://wmtest1.appliedbiosystems.com:443/ws/ABI_SalesQuotation.process:loadSAPpartnersInSFDCWsd';
        public String endpoint_x = AbConstants.ENDPOINT+'/ws/ABI_SalesQuotation.process:loadSAPpartnersInSFDCWsd';
        
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        private String[] ns_map_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd', 'QuotePartners'};
        public String loadSAPpartnersInSFDC(QuotePartners.partnerInfo partnerInfo) {
            QuotePartners.loadSAPpartnersInSFDC request_x = new QuotePartners.loadSAPpartnersInSFDC();
            QuotePartners.loadSAPpartnersInSFDCResponse response_x;
            request_x.partnerInfo = partnerInfo;
            Map<String, QuotePartners.loadSAPpartnersInSFDCResponse> response_map_x = new Map<String, QuotePartners.loadSAPpartnersInSFDCResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'ABI_SalesQuotation_process_loadSAPpartnersInSFDCWsd_Binder_loadSAPpartnersInSFDC',
              'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd',
              'loadSAPpartnersInSFDC',
              'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd',
              'loadSAPpartnersInSFDCResponse',
              'QuotePartners.loadSAPpartnersInSFDCResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.upsertPartnerConatctStatus;
        }
    }
    public class loadSAPpartnersInSFDCResponse {
        public String upsertPartnerConatctStatus;
        private String[] upsertPartnerConatctStatus_type_info = new String[]{'upsertPartnerConatctStatus','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','false'};
        private String[] field_order_type_info = new String[]{'upsertPartnerConatctStatus'};
    }
    public class partnerInfo {
        public String partnerID;
        public String region;
        public QuotePartners.ArrayOfpartnerType partnerType;
        private String[] partnerID_type_info = new String[]{'partnerID','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] region_type_info = new String[]{'region','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] partnerType_type_info = new String[]{'partnerType','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','ArrayOfpartnerType','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','false'};
        private String[] field_order_type_info = new String[]{'partnerID','region','partnerType'};
    }
    public class partnerType {
        public String partnerType;
        private String[] partnerType_type_info = new String[]{'partnerType','http://www.w3.org/2001/XMLSchema','string','0','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','false'};
        private String[] field_order_type_info = new String[]{'partnerType'};
    }
    public class loadSAPpartnersInSFDC {
        public QuotePartners.partnerInfo partnerInfo;
        private String[] partnerInfo_type_info = new String[]{'partnerInfo','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','partnerInfo','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/loadSAPpartnersInSFDCWsd','false'};
        private String[] field_order_type_info = new String[]{'partnerInfo'};
    }
}