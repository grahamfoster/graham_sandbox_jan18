public with sharing class recentupdatedPackage {

   public Id userId {get; set;}
   //public List<Add_to_Favorites_Junction__c> userfav {get; set;}
   public List<pckWrapper> pck_list;
   List<Package__c> pckgList {get; set;}
   public Set<Id> packageId = new Set<Id>();
   public List<Rating_Management__c> rm {get; set;}
   
   public recentupdatedPackage(){
       //userId = userInfo.getuserId();
       //userfav = [SELECT Id, Name, Package__c FROM Add_to_Favorites_Junction__c WHERE isActive__c =: true ORDER BY LastModifiedDate LIMIT 20];
   }
   
   public List<pckWrapper> getfav(){
         pck_list = new List<pckWrapper>();
         
         pckgList = [SELECT Id, Name, Product__r.Product_Category__c, Product__r.Product_Category__r.Name, Product__r.Product_Category__r.Category_Icon__c, Product__r.Name,
                                      Competitor_Product__r.Name, Package_Name__c, Package__c.Rating__c, Application_Variants__r.Application__r.Image__c, Application_Variants__r.Application_Variant_Name__c,
                                      Application_Variants__r.Color__c, Application_Variants__r.Application__r.Color__c FROM Package__c WHERE isActive__c =: true AND Product__c != null AND Product__r.Product_Category__c != null AND
                                      Competitor_Product__c != null ORDER BY LastModifiedDate Desc LIMIT 20];
         for(package__c pk :pckgList){
             //package__c pkc;
             integer fstr = 0;
             integer sstr = 0;
             integer nstr = 0;
             
             if(pk.Rating__c > 0){
             fstr = (integer)pk.Rating__c;
             if((pk.Rating__c - pk.Rating__c.round(roundingMode.DOWN))>0)
             sstr = 1;
             nstr = 5 - (integer)pk.Rating__c.round(roundingMode.DOWN) - sstr;
             pck_List.add(new pckWrapper(pk,fstr,sstr,nstr,(decimal)pk.Rating__c));
             }else
             pck_List.add(new pckWrapper(pk,0,0,5,0));
         }

         /*rm = [SELECT Id, Name, Package__c, Rating__c, User__c FROM Rating_Management__c WHERE Package__c IN: pckgList AND isActive__c =: true]; 
         for(package__c pk :pckgList){
             //Rating_Management__c rmc1;
             Decimal Rating = 0; 
             integer Count = 0;
             integer fstr = 0;
             integer sstr = 0;
             integer nstr = 0;
             for(Rating_Management__c rmc : rm){
                 if(pk.Id == rmc.Package__c){
                    //rmc1 = new Rating_Management__c();
                    //rmc1 = rmc;
                    Rating = Rating + rmc.Rating__c; 
                    Count = Count + 1;
                 }   
             }
             
             if(Count != 0){
             Rating = Rating/Count;
             fstr = (integer)Rating.round(roundingMode.DOWN);
             if((Rating - (integer)Rating.round(roundingMode.DOWN))>0)
             sstr = 1;
             nstr = 5 - (integer)Rating.round(roundingMode.DOWN) - sstr;
             pck_List.add(new pckWrapper(pk,Rating,fstr,sstr,nstr ));
             }else
             pck_List.add(new pckWrapper(pk,Rating,0,0,5));
         } */
         return pck_List;
   }
   
   public class pckwrapper{
       public Package__c pck {get; set;} //Package object
       //public Rating_Management__c rmc {get; set;} //Rating Management Object
       public decimal Rating {get; set;}
       public integer fullStar {get; set;} //Rating stars
       public integer semiStar {get; set;} //Rating stars
       public integer noStar {get; set;} //Rating stars
        
        public pckwrapper(Package__c p, integer f, integer s, integer n, Decimal Rating) {
            this.pck = p; //assign package
            this.Rating = Rating;
            //this.rmc = r; //assign Rating Management
            this.fullStar = f; //assign number of full stars
            this.semiStar = s; //assign number of semi stars
            this.noStar = n; //assign number of no stars
        }
   }
}