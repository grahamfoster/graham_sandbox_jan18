public with sharing class applicationListPage {

   List<Application__c> appCat {get; set;}
   List<Application_Variants__c> app {get; set;}
   Set<Id> appId = new Set<Id>();
   public string appcId {get; set;}
   public map<String,list<AppWrapper>> appMap{get;set;}
   public boolean clickedCell {get; set;}
   public string ApplicationName {get; set;}
   public string ApplicationId {get; set;}
   public boolean noBattlecardsDefined {get; set;}
    
   public applicationListPage(){  
   		boolean filterData = false;
   		noBattlecardsDefined = false;
   		//in select package in CIP Portal we have to filter applications 	
   		String absciexProdId = ApexPages.currentPage().getParameters().get('absciexProdId');
   		String competitorProdId = ApexPages.currentPage().getParameters().get('competProdId');
   		if(absciexProdId!=null && absciexProdId!='' && competitorProdId!=null && competitorProdId!='')
   		{
   			filterData = true;
   		}
   	
        clickedCell = false;
        
        appcat = [select Id, Name, Application_Name__c from Application__c Where isActive__c =: true Order By Application_Name__c];       
        for(Application__c ap: appcat){
            appId.add(ap.Id);
        }
        
        if (!filterData)
        {
            app = [Select Id, Name, Application_Variant_Name__c, Application__c From Application_Variants__c Where isActive__c =: true AND Application__c In: appId];       
        }
        else
        {
	        app = [Select Id, Name, Application_Variant_Name__c, Application__c From Application_Variants__c Where Id IN (SELECT Application_Variants__c FROM Package__c WHERE Competitor_Product__c =: competitorProdId AND Product__c =: absciexProdId AND isActive__c =: true) AND isActive__c =: true AND Application__c In: appId];       
        }
        
        appMap = new map<String,list<appWrapper>>();
        for(Application__c ap: appcat){
        	boolean isEmptyCategory = true;
            list<appWrapper> appList = new list<appWrapper>();
            for(Application_Variants__c  a : app){ 
               if(ap.Id == a.Application__c ){
               	   isEmptyCategory = false;
                   appWrapper appWrap = new appWrapper();
                   appWrap.ApplicationCategoryName = ap.Application_Name__c; 
                   appWrap.ApplicationName = a.Application_Variant_Name__c;
                   appWrap.applicationId = a.Id;
                   appList.add(appwrap);
               }
            }
            if (!isEmptyCategory) appMap.put(ap.Application_Name__c,appList); 
        } 
        if (appMap.size() == 0) noBattlecardsDefined = true;
   }
   
      
   public void parentpage(){
      clickedcell = true;
   }

    public class AppWrapper {
        public String ApplicationCategoryName{get;set;}
        public String ApplicationName{get;set;}
        public Id applicationId{get;set;}
    }
   

}