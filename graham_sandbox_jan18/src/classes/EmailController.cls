@RestResource(urlMapping='/activations/email')
global class EmailController {

    private static void Finalize(Boolean success, String message, String diagnostics) {
        String result = '{ "success": ' + String.valueOf(success) + 
            (message != null ? ', "message": "' + message + '"' : '') + 
            (diagnostics != null ? ', "diagnostics": "' + diagnostics + '"' : '') + 
            ' }';
        RestContext.response.responseBody = Blob.valueOf(result);
    }
    
    private static void Finalize(Boolean success, String message) {
        Finalize(success, message, null);
    }
    
    private static void Finalize(Boolean success) {
        Finalize(success, null, null);
    }

    @HttpGet
    @HttpPost
    global static void email() {
    
        try {
    
            RestContext.response.addHeader('Content-Type', 'application/json');
        
            // Parse parameters.
            String activationId = RestContext.request.params.get('activationId');
            
            if (activationId == null) {
                Finalize(false, 'An activation ID must be supplied.');
                return;
            }
            
            // Set up our variables.        
            Boolean detailsResponseSuccess = false;
            Boolean detailsResponseActivated = false;
            String detailsResponseMessage = null;
            String detailsResponseProductName = null;
            String detailsResponseProductVersion = null;
            String detailsResponseLicenseText = null;
            String detailsResponseLicenseFilename = 'License.lic';
            String detailsResponseLicenseFileLocation = null;
            String detailsResponseFulfillmentID = null;
            String detailsResponseExpirationDate = null;
            String detailsResponseHostID = null;
            String detailsResponseSerialNumber = null;
            String detailsResponseShipToEmail = null;
            
            // Call our web service.
            HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
            List<LicenseActivationSettings__c> settingsList = Database.query('SELECT ActivationDetailsServletURI__c FROM LicenseActivationSettings__c WHERE Name = \'' + UserInfo.getOrganizationId() + '\'');
            LicenseActivationSettings__c settings = settingsList[0];
            String url = settings.ActivationDetailsServletURI__c + '?activationId=' + EncodingUtil.urlEncode(activationId, 'UTF-8'); 
            request.setEndpoint(url);
            Http http = new Http();
            HTTPResponse response = http.send(request);
            String jsonString = response.getBody();
            JSONParser parser = JSON.createParser(jsonString);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() != JSONToken.FIELD_NAME)
                    continue;
                String fieldName = parser.getText();
                if (fieldName == 'success') {
                    parser.nextToken();
                    detailsResponseSuccess = parser.getBooleanValue();
                }
                if (fieldName == 'message') {
                    parser.nextToken();
                    detailsResponseMessage = parser.getText();
                }
                if (fieldName == 'productName') {
                    parser.nextToken();
                    detailsResponseProductName = parser.getText();
                }
                if (fieldName == 'productVersion') {
                    parser.nextToken();
                    detailsResponseProductVersion = parser.getText();
                }
                if (fieldName == 'licenseText') {
                    parser.nextToken();
                    detailsResponseLicenseText = parser.getText();
                }
                if (fieldName == 'licenseFilename') {
                    parser.nextToken();
                    detailsResponseLicenseFilename = parser.getText();
                }
                if (fieldName == 'licenseFileLocation') {
                    parser.nextToken();
                    detailsResponseLicenseFileLocation = parser.getText();
                }
                if (fieldName == 'shipToEmail') {
                    parser.nextToken();
                    detailsResponseShipToEmail = parser.getText();
                }
                if (fieldName == 'activated') {
                    parser.nextToken();
                    detailsResponseActivated  = parser.getBooleanValue();
                }
                if (fieldName == 'expirationDate') {
                    parser.nextToken();
                    detailsResponseExpirationDate = parser.getText();
                }
                if (fieldName == 'hostID') {
                    parser.nextToken();
                    detailsResponseHostID = parser.getText();
                }
                if (fieldName == 'fulfillmentID') {
                    parser.nextToken();
                    detailsResponseFulfillmentID = parser.getText();
                }
                if (fieldName == 'serialNumber') {
                    parser.nextToken();
                    detailsResponseSerialNumber = parser.getText();
                }
            }
                    
            if (!detailsResponseActivated) {
                Finalize(false, 'Activation record not found in FlexNet Operations.');
                return;
            }
                
            Software_Activation__c activationRecord = null;
                
            List<Software_Activation__c> activations = Database.query('SELECT Id, Contact__r.Id FROM Software_Activation__c WHERE Activation_ID__c = \'' + activationId + '\'');
            
            if (activations.size() == 0) {
                Finalize(false, 'No activation record for this activation ID was found in Salesforce.');
                return;
            }
            else
                activationRecord = activations[0];
                
            String recipientId = activationRecord.Contact__r.Id;
            
            if (recipientId == null) {
                Finalize(false, 'The record for this activation ID does not reference a valid contact in Salesforce.');
                return;
            }
            
            String emailTemplateName = 'Activation Email';
            List<EmailTemplate> templates = Database.query('SELECT Id FROM EmailTemplate WHERE Name = \'' + emailTemplateName + '\'');
            if (templates.size() == 0) {
                Finalize(false, 'The ' + emailTemplateName + ' template was not found.');
                return;
            }
            
            String oweaName = 'noreply@sciex.com';
            List<OrgWideEmailAddress> owea = Database.query('SELECT Id FROM OrgWideEmailAddress WHERE Address = \'' + oweaName + '\'');
            if (owea.size() == 0) {
                Finalize(false, 'The organization wide email address was not found for ' + oweaName + '.');
                return;
            }
              
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateId(templates[0].Id);
            mail.setTargetObjectId(recipientId);
            mail.setWhatId(activationRecord.Id);
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setSaveAsActivity(false);
            mail.setOrgWideEmailAddressId(owea[0].Id);
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(detailsResponseLicenseFilename != null ? detailsResponseLicenseFilename : 'License.lic');
            efa.setBody(Blob.valueOf(detailsResponseLicenseText));
            mail.setFileAttachments(new Messaging.EmailFileAttachment[] { efa });
            Messaging.SendEmailResult[] result = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }, true);
            
        }
        catch (Exception e) {
            System.debug(e);
            Finalize(false, e.getMessage(), e.getStackTraceString());
            return;
        }
        
        Finalize(true);
    }
    
}