/**
** @author Reid Beckett, Cloudware Connections
** @created Dec 28/2014
**
** Helper class to create/update Cost records for all currencies defined for the products
** 
**/
public class CostCurrencyHelper {
	private List<Product2> products;
//	private Map<Id,Product2> products;
	private Map<String, CurrencyType> currencyRates;

	public CostCurrencyHelper(List<Product2> products) {
		this.products = products;	
	}
/*
	private void loadProducts(){
		Set<Id> productIds = new Set<Id>();
		for(Product2 product : products) {
			if(!productIds.contains(product.Id)) productIds.add(Id);
		}

		products = new Map<Id,Product2>([
			select Id, ProductCode,
			(select Id, CurrencyIsoCode, Pricebook2Id, Product2Id from PricebookEntries where Pricebook2.IsStandard = true),
			(select Id, CurrencyIsoCode, SBQQ__UnitCost__c, Oracle_Item_Number__c from SBQQ__Costs__r)
			from Product2 where Id in :productIds
		]);
	}
*/
	private void loadCurrencyRates(){
		currencyRates = new Map<String,CurrencyType>();
		for(CurrencyType ctype : [select Id, ConversionRate, IsoCode from CurrencyType] ) {
			currencyRates.put(ctype.IsoCode, ctype);
		}
	}

	public void process(){
//		loadProducts();
		loadCurrencyRates();

		List<SBQQ__Cost__c> costsUpserts = new List<SBQQ__Cost__c>();
		for(Product2 p : products) {
//			if(products.containsKey(c.SBQQ__Product__c)) {
//				Product2 p = products.get(c.SBQQ__Product__c);
				Map<String, SBQQ__Cost__c> costsMap = new Map<String, SBQQ__Cost__c>();
				for(PricebookEntry pbe : p.PricebookEntries) {
					if(pbe.CurrencyIsoCode != 'USD') {
						String externalId = p.ProductCode + '-' + pbe.CurrencyIsoCode;
						SBQQ__Cost__c newCost = new SBQQ__Cost__c(
							SBQQ__UnitCost__c = 0,
							SBQQ__Product__c = pbe.Product2Id,
							SBQQ__Active__c = true,
							Oracle_Item_Number__c = externalId,
							CurrencyIsoCode = pbe.CurrencyIsoCode
						);
						costsMap.put(pbe.CurrencyIsoCode, newCost);
					}
				}

				SBQQ__Cost__c usdCost = null;
				for(SBQQ__Cost__c costRec : p.SBQQ__Costs__r) {
					if(costRec.CurrencyIsoCode == 'USD') usdCost = costRec;
					else if(costsMap.containsKey(costRec.CurrencyIsoCode)) {
						costsMap.put(costRec.CurrencyIsoCode, costRec);
					}
				}

				//update the costs for all non-usd records
				if(usdCost != null) {
					for(SBQQ__Cost__c costRec : costsMap.values()) {
						if(currencyRates.containsKey(costRec.CurrencyIsoCode)) {
							CurrencyType ctype = currencyRates.get(costRec.CurrencyIsoCode);
							costRec.SBQQ__UnitCost__c = ctype.ConversionRate * usdCost.SBQQ__UnitCost__c;
							costsMap.put(costRec.CurrencyIsoCode, costRec);
						}
					}
				}

				costsUpserts.addAll(costsMap.values());
//			}
			p.Costs_Last_Updated__c = DateTime.now();
		}

		upsert costsUpserts;
		update products;
	}
}