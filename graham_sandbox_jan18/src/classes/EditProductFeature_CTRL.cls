public class EditProductFeature_CTRL{
    public String ProductFeatureID{get;set;}
    public List<Specifications_Meta__c> prodfeaturelist{get;set;}
    public List<Product_Meta__c> prodmetalist{get;set;}
    public List<SpecificationWrapper> wrapperlist{get;set;}
    public Specifications_Meta__c specmeta{get;set;}
     public Boolean isComplete{get; private set;}
    
    
    public EditProductFeature_CTRL(){
       ProductFeatureID=ApexPages.currentPage().getParameters().get('catid');
       isComplete = false;
      
        //ProductFeatureID='a0Fb0000001KdVN';
        if(ProductFeatureID != null){
            wrapperlist= new List<SpecificationWrapper>();
            for(Specifications_Meta__c ac:[select id,isActive__c,(select id,isActive__c,Product_Meta_Name__c,Type__c,Specifications_Meta__c from Products_Meta__r),Product_Category__c,Title__c,Specifications_Meta_Name__c from Specifications_Meta__c where id=:ProductFeatureID])
              {            
                wrapperlist.add(new SpecificationWrapper(ac,ac.isActive__c,ac.Products_Meta__r));
                    
              }
              System.debug('wrapper list'+wrapperlist);
           
        }
        
    }
    public class SpecificationWrapper{
        public Specifications_Meta__c prodfeat{get;set;}
        public List<Product_Meta__c> prodmeta{get;set;}
        public Boolean Isselectedspec{get;set;}
        public SpecificationWrapper(Specifications_Meta__c prodfeatlist,Boolean Isselectedspec1,List<Product_Meta__c> prodmetalist){
            this.prodfeat=prodfeatlist;
            this.prodmeta=prodmetalist;
            this.Isselectedspec=Isselectedspec1;
         }
    }
   
    public Pagereference ok(){
    try{
        if(ProductFeatureID != null){
            isComplete = false;
            List<Specifications_Meta__c> selectedprodfeaturelist=new  List<Specifications_Meta__c>();
            List<Specifications_Meta__c> deselectedprodfeaturelist=new  List<Specifications_Meta__c>();
            List<Specifications_Meta__c> templist =new  List<Specifications_Meta__c>();
            List<Specifications_Meta__c> templist1 =new  List<Specifications_Meta__c>();
            List<Product_Meta__c> seletedprodmetalist= new  List<Product_Meta__c>();
            for(SpecificationWrapper c :wrapperlist){
              System.debug('product list '+c.prodfeat);
                if(c.Isselectedspec){
                    templist.add(c.prodfeat);
                    
                }
                if(!c.Isselectedspec){
                      templist1.add(c.prodfeat);
    
                }
                for(Product_Meta__c  pm: c.prodmeta){
                    seletedprodmetalist.add(pm);
                }
               
                update c.prodfeat;
                
              
           }
          
           if(seletedprodmetalist !=null){
                  update seletedprodmetalist;
           
           }
           for(Specifications_Meta__c ap:[select id,isactive__c,Specifications_Meta_Name__c from Specifications_Meta__c where id IN:templist]){
               ap.isactive__c=true;
               selectedprodfeaturelist.add(ap);
           }
                        
                    
           if(selectedprodfeaturelist !=null){
                  update selectedprodfeaturelist;
           
           }
           for(Specifications_Meta__c ap:[select id,Specifications_Meta_Name__c,isactive__c from Specifications_Meta__c where id IN: templist1]){
               ap.isactive__c=false;
               deselectedprodfeaturelist.add(ap);
           }
                         
                    
           if(deselectedprodfeaturelist !=null){
                  update deselectedprodfeaturelist;
           
           }
          
      }
          isComplete = true; 
          System.debug('edit feature...1');
      }
      catch(Exception e){
      	System.debug('edit feature...'+e.getMessage());
            isComplete = false; 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
       }
        return null;
      
     
     
    }
     public void dodelete(){
       
        
    
    }
     public void docancel(){
       
       
    
    }
    
    
    


}