@isTest 
private class AssetEntitlementUpdate_Test {

	@isTest
	private static void test_Entitlement_Update() {

		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;
		Contact c1 = new Contact(AccountId = a.Id, FirstName = 'John', LastName = 'Doe');
		insert c1;
		List<SVMXC__Installed_Product__c> installedProducts = new List<SVMXC__Installed_Product__c>();
		for (Integer i = 0; i < 100; i++){
			installedProducts.add(new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVC_Oracle_Bill_To_Number__c = String.valueOf(i)));
		}

		insert installedProducts;

		Product2 prod = new Product2(Name = 'TestProduct', SCIEXNow_Hardware_Entitled__c = true, SCIEXNOW_Software_Upgrade_Entitled__c = true, SCIEXNow_Software_Entitled__c = true);
		Product2 prod2 = new Product2(Name = 'TestProduct2', SCIEXNow_Hardware_Entitled__c = false, SCIEXNOW_Software_Upgrade_Entitled__c = false, SCIEXNow_Software_Entitled__c = false);
		Product2 prod3 = new Product2(Name = 'TestProduct3', SCIEXNow_Hardware_Entitled__c = true, SCIEXNOW_Software_Upgrade_Entitled__c = false, SCIEXNow_Software_Entitled__c = false);
		insert new List<Product2> {prod, prod2, prod3};
		
		SVMXC__Service_Contract__c serCon = new SVMXC__Service_Contract__c(SVMXC__Active__c = true);
		insert serCon;

		List<SVMXC__Service_Contract_Products__c> newContracts = new List<SVMXC__Service_Contract_Products__c>();
		for (Integer i = 0; i < 100; i++){
			Id productId = prod.Id;
			if(Math.mod(i,3) == 1) productId = prod2.Id;
			if(Math.mod(i,3) == 2) productId = prod3.Id;
			newContracts.add(new  SVMXC__Service_Contract_Products__c(SVMXC__Installed_Product__c = installedProducts[i].Id, 
			SVC_Item_Master_Name__c = productId, SVMXC__Service_Contract__c = serCon.Id, SVC_Contract_Subline_Status__c = 'ACTIVE', 
			SVMXC__Start_Date__c = System.today().addDays(-1), SVMXC__End_Date__c = System.today().addDays(1)));
		}
		insert newContracts;


		Test.startTest();
		AssetEntitlementUpdate upd = new AssetEntitlementUpdate();
		Database.executeBatch(upd);
		Test.stopTest();

		
		List<Case> newCases = new List<Case>{
			new Case(ContactId = c1.Id, AccountId = a.Id, Subject = 'A', Description = 'Test1'),
			new Case(ContactId = c1.Id, AccountId = a.Id, Subject = 'B', Description = 'Test1'),
			new Case(ContactId = c1.Id, AccountId = a.Id, Subject = 'C', Description = 'Test1')};

		List<Asset> updatedAssets = [SELECT Id, Name, Installed_Product__c, NEWSCIEXNow_Hardware_Entitled__c, NEWSCIEXNow_Software_Entitled__c, NEWSCIEXNOWSoftware_Upgrade_Entitled__c, 
		Installed_Product__r.SVC_Oracle_Bill_To_Number__c FROM Asset WHERE AccountId = :a.Id];
		for(Asset ups : updatedAssets)
		{
			Integer x = Integer.valueOf(ups.Installed_Product__r.SVC_Oracle_Bill_To_Number__c);
			if(Math.mod(x,3) == 1)
			{
				System.assert(!ups.NEWSCIEXNow_Hardware_Entitled__c && !ups.NEWSCIEXNow_Software_Entitled__c && !ups.NEWSCIEXNOWSoftware_Upgrade_Entitled__c);
				newCases[0].AssetId = ups.Id;
			} else if(Math.mod(x,3) == 2)
			{
				System.assert(ups.NEWSCIEXNow_Hardware_Entitled__c && !ups.NEWSCIEXNow_Software_Entitled__c && !ups.NEWSCIEXNOWSoftware_Upgrade_Entitled__c);
				newCases[1].SVMXC__Component__c = ups.Installed_Product__c;
			} else 
			{
				System.assert(ups.NEWSCIEXNow_Hardware_Entitled__c && ups.NEWSCIEXNow_Software_Entitled__c && ups.NEWSCIEXNOWSoftware_Upgrade_Entitled__c);
				newCases[2].AssetId = ups.Id;
			}
		}

		insert newCases;

		List<Case> testCases = [SELECT Subject, Software_Entitled__c, Software_Upgrade_Entitled__c, Hardware_Entitled__c 
		FROM Case WHERE Id = :newCases[0].Id OR Id = :newCases[1].Id OR Id = :newCases[2].Id ORDER BY Subject];

		System.assert(!testCases[0].Hardware_Entitled__c && !testCases[0].Software_Entitled__c && !testCases[0].Software_Upgrade_Entitled__c);
		System.assert(testCases[1].Hardware_Entitled__c && !testCases[1].Software_Entitled__c && !testCases[1].Software_Upgrade_Entitled__c);
		System.assert(testCases[2].Hardware_Entitled__c && testCases[2].Software_Entitled__c && testCases[2].Software_Upgrade_Entitled__c);

	}

	
}