/*
 *	TaskTriggerHandler_2
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Graham Foster on 2016-08-30
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

public class TaskTriggerHandler_2 extends TriggerHandler
{

    // **************************************************************************
	// 		context overrides 
	// **************************************************************************

	override protected void beforeUpdate(){
		setCallEndTime();
		CTICaseChanged();
		setFirstActivityDate();
    }
	
	override protected void afterUpdate() {
        TrainingTaskWorkFlow();
		setCallEndTime();
		//warmTranferredFirstContact();
		warmTransferred();
		//callCaseChanged must be the last call made after update
		callCaseChanged();
	}
    
    override protected void beforeInsert(){
        TaskTriggerHandler.onBeforeInsert(Trigger.new);
		setCallStartTime();
		//setCallEndTime();
    }
    
    override protected void afterInsert(){
       warmTransferred();
    }
    
    
    // **************************************************************************
	// 		private methods
	// **************************************************************************

	public static final String CasePrefix = Case.SObjectType.getDescribe().getKeyPrefix();

    private void TrainingTaskWorkFlow(){
        //updates case status's according to tasks being completed
        //thru the lifecyle of the case
        Map<Id, Case> CasesForUpdate = new Map<Id, Case>();
		Set<Id> CaseIds = new Set<Id>();
        //just loops each task in the trigger, checks
        //if its the correct type and updates case accordingly
         for (Task t : (List<Task>)trigger.new){
             String objId = (String)t.WhatId;
             if(!String.isEmpty(objId) && objId.indexOf(CasePrefix) == 0 && t.Type == 'Training' && t.Status == 'Completed' 
			 && isChanged(t,'Status'))
			 {
				CaseIds.add(objId);
			 }
		}

		//query the data for case type and training process
		Map<Id, Case> parentCases = new Map<Id, Case>([SELECT Id, Type, OwnerId, Training_Part__r.Product_Code_for_Search_Filter__c,
														Contact.FirstName, Contact.LastName, Contact.Phone, Account.Name
														FROM Case WHERE Id IN :CaseIds]);

		List<sObject> newTasksAndEvents = new List<sObject>();
		for (Task t : (List<Task>)trigger.new)
		{
			String objId = (String)t.WhatId;
			if(!String.isEmpty(objId) && objId.indexOf(CasePrefix) == 0 && t.Type == 'Training' && t.Status == 'Completed' 
			&& isChanged(t,'Status'))
			{
				Boolean existingUpdate = true;
				Case c = CasesForUpdate.get(t.WhatId); 
				if(c == null)
				{
					c = parentCases.get(t.WhatId);
					existingUpdate = false;
				} 
				if(t.Training_Task_Type__c == 'Schedule Pre Install')
				{
                     c.Pre_Install_Webex_Scheduled_Date__c = t.Scheduled_Start_DateTime__c;
					 //if its standalone generate the next task
					 if(c.Type == 'Standalone')
					 {
						Task newTask = createTrainingTask(c, 'Complete Pre Install');
						newTasksAndEvents.add(newTask);
						//newTasksAndEvents.add(calendarEvent(t, newTask));
					 }
                } else if (t.Training_Task_Type__c == 'Complete Pre Install')
				{
                     c.Pre_Install_Webex_Completion_Date__c = date.today();
                     c.Status = 'Training by FSE';
					 //if its standalone generate the next task
					 if(c.Type == 'Standalone')
					 {
						newTasksAndEvents.add(createTrainingTask(c, 'Schedule onsite'));
					 }
                } else if (t.Training_Task_Type__c == 'Schedule onsite')
				{
                     c.Scheduled_Training_Date__c = t.Scheduled_Start_DateTime__c.date();
					 if(c.Type == 'Standalone')
					 {
						Task newTask = createTrainingTask(c, 'Perform Instructor Led');
						newTasksAndEvents.add(newTask);
						//newTasksAndEvents.add(calendarEvent(t, newTask));
					 }
                } else if (t.Training_Task_Type__c == 'Perform Instructor Led')
				{
                     c.Actual_Onsite_Training_Date__c = Date.today();

					 if(c.Type == 'Standalone')
					 {
					 	String trnPartNumber =  c.Training_Part__r.Product_Code_for_Search_Filter__c;
						Integer trnProcess = trnPartNumber == null ? 0 :
					(Integer)Training_Part_Processes__c.getInstance(trnPartNumber).Process_Path__c;
						if(trnProcess == 3)
						{
							c.Status = 'Closed';
						}
						else
						{
							newTasksAndEvents.add(createTrainingTask(c, 'Schedule Post-Training'));
						}
					 }
                } else if (t.Training_Task_Type__c == 'Schedule Post-Training')
				{
                     c.Post_OnSite_Webex_Scheduled_Date__c = t.Scheduled_Start_DateTime__c;
					 if(c.Type == 'Standalone')
					 {
						Task newTask = createTrainingTask(c, 'Complete Post-Training');
						newTasksAndEvents.add(newTask);
						//newTasksAndEvents.add(calendarEvent(t, newTask));
					 }
                } else if (t.Training_Task_Type__c == 'Complete Post-Training')
				{
                     c.Post_OnSite_Webex_Actual_Date__c = date.today();
                     c.Status = 'Closed';
                }
				if(!existingUpdate)
				{
					CasesForUpdate.put(c.Id,c);
				}
			}
		}

		if(CasesForUpdate.size() > 0)
		{
			update CasesForUpdate.values();
		}
		if(newTasksAndEvents.size() > 0)
		{
			System.debug('**GF** new Tasks:' +  newTasksAndEvents);
			insert newTasksAndEvents;
		}
    }

	/*****************************************************************************************************
	* @description Creates a new task for the training case workflow
	* @param ca the Training Case for which we need to create a task
	* @param TrnTaskType the task type for the new task
	* @param Due the Date which will define the DueDate when the DueDatePlusDays parameter is added
	* @param existingTasks the tasks which exist for all of the cases in the trigger
	* @return new Task object
	*/
	private Task createTrainingTask(Case ca, String TrnTaskType)
	{
		String Descr = '';
		String Subj = '';
		//set different Descriptions and Subjects depending on the type of case
		if(TrnTaskType == 'Schedule Pre Install')
		{
			Descr = 'Please schedule preparative Webex & FAQ Session with ';
			Subj = 'Schedule preparative FAQ & Webex with Primary Learner - ';
		} else if(TrnTaskType == 'Complete Pre Install')
		{
			Descr = 'Complete preparative Webex & FAQ Session with ';
			Subj = 'Complete preparative FAQ & Webex with Primary Learner - ';
		} else if(TrnTaskType == 'Schedule onsite')
		{
			Descr = 'Schedule Application Support training with ';
			Subj = 'Schedule & prepare for Application Support Training - ';
		} else if(TrnTaskType == 'Perform Instructor Led')
		{
			Descr = 'Perform Application Support training with ';
			Subj = 'Perform Application Support Training - ';
		} else if(TrnTaskType == 'Schedule Post-Training')
		{
			Descr = 'Schedule Post Application Support FAQ & Webex with ';
			Subj = 'Schedule Post Application Support FAQ & Webex - ';
		} else if(TrnTaskType == 'Complete Post-Training')
		{
			Descr = 'Complete Post Application Support FAQ & Webex with ';
			Subj = 'Complete Post Application Support FAQ & Webex - ';
		} else { return null; }
		//create the new task
		Task t  = new Task(
		Training_Task_Type__c = TrnTaskType,
		Description = Descr + ca.Contact.FirstName + ' ' + ca.Contact.LastName + '(' + ca.Contact.Phone + ')',
		Subject = Subj + ca.Account.Name + ' - ' + ca.Contact.FirstName + ' ' + ca.Contact.LastName,
		WhatId = ca.Id,
		Type = 'Training',
		RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Training').getRecordTypeId(),
		OwnerId = ca.OwnerId		
		);
		return t;
	}
    
    
    /*****************************************************************************************************
	* @description Creates a new Event for the training case workflow (so that the training task can be 
	* displayed in users calendar)
	* @param c the Training Case for which we need to create a task
	* @param TrnTaskType the task type for the new task
	* @param existingTasks the tasks which exist for all of the cases in the trigger
	* @return new Event object
	*/
	private Event calendarEvent(Task existingTask, Task nTask)
	{

		Event e = new Event(
			EndDateTime = existingTask.Scheduled_End_DateTime__c,
			OwnerId = existingTask.OwnerId,
			StartDateTime = existingTask.Scheduled_Start_DateTime__c,
			Subject = 'EVENT ' + nTask.Subject,
			WhatId = existingTask.WhatId
		);
		return e;

		
	}

	public Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }
    

	/**
	* @description if the case that a call attached to is changed we need to recreate the 
	* task so that the feed item is added onto the new case as this action only happens
	* on insert of a task record
	*
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/96370689/callCaseChanged
	*/ 
	public void callCaseChanged()
	{
		System.debug('**GF** callCaseChanged:START');
		//loop though the trigger and get the Id's of any CTI tasks which have had the Case WhatId changed
		Set<Id> taskIds = new Set<Id>();
		for (Task t : (List<Task>)trigger.new)
		{
             String objId = (String)t.WhatId;
             if(!String.isEmpty(objId) && objId.indexOf(CasePrefix) == 0 && t.CTI_What_Id_Changed__c
			 && !String.isEmpty(t.cnx__CTIInfo__c) && t.CallDurationInSeconds != null)
			 {
				taskIds.add(t.Id);
			 }
		}


		//if we have any use the CTIUtil methods to create new tasks and feed items.
		if(taskIds.size() > 0)
		{
			
			System.debug('**GF** Calling new feedItem:' + taskIds);
			CTIUtil.newFeedItem(taskIds);
		}
		System.debug('**GF** callCaseChanged:END');
	}



	/**
	* @description set the date/time that a CTI phone call started
	*/ 
	public void setCallStartTime()
	{
		for (Task t : (List<Task>)trigger.new)
		{
			if(!String.isEmpty(t.cnx__CTIInfo__c) && t.Call_Start_Time__c == null)
			{
				t.Call_Start_Time__c = System.Now();
			}
		}
	}

	/**
	* @description set the date/time that a CTI phone call ended (the CallDurationInSeconds field is only updated when the call is ended)
	*/ 
	public void setCallEndTime()
	{
		//List<Task> updateTasks = new List<Task>();
		for (Task t : (List<Task>)trigger.new)
		{
			System.debug('**GF** setCassEndTime:START');
			if(!String.isEmpty(t.cnx__CTIInfo__c) && isChanged(t, 'CallDurationInSeconds') && t.Call_End_Time__c == null && t.CallDurationInSeconds != null)
			{
				t.Call_End_Time__c = System.Now();
			}
		}
	}

	/**
	* @description add a flag to the case when the WhatId is changed - we need to record if the WhatId is changed
	* so that we can clone the task to create a new feed item. However we cannot create a new clone each time
	* the whatId is changed because it breaks the link with the CTI - so we add a flag to indicate it has been changed
	* since insert and then when the call is over (the duration is set) we can clone using  callCaseChanged() above
	*/
	public void CTICaseChanged()
	{
		System.debug('**GF** CTICaseChanged:START');
		for (Task t : (List<Task>)trigger.new)
		{
			if(!String.isEmpty(t.cnx__CTIInfo__c) && isChanged(t, 'WhatId'))
			{
				t.CTI_What_Id_Changed__c = true;
			}
		}
		System.debug('**GF** CTICaseChanged:END');
	}

	public void warmTransferred()
	{
		System.debug('**GF** warmTransferred:START');
		Set<Id> caseIds = new Set<Id>();
		for (Task t : (List<Task>)trigger.new)
		{
			String objId = (String)t.WhatId;
			if(Trigger.isUpdate)
			{
				if(!String.isEmpty(objId) && objId.indexOf(CasePrefix) == 0 && !String.isEmpty(t.cnx__CTIInfo__c) && 
				(isChanged(t, 'CallDurationInSeconds') || (isChanged(t, 'WhatId') && t.CallDurationInSeconds != null)))
				{
					//we know its a CTI Call so we can see if the case has any others
					caseIds.add(t.WhatId);
				}
			}
			else 
			{
				if(!String.isEmpty(objId) && objId.indexOf(CasePrefix) == 0 && !String.isEmpty(t.cnx__CTIInfo__c) && t.CallDurationInSeconds != null)
				{
					//we know its a CTI Call so we can see if the case has any others
					caseIds.add(t.WhatId);
				}
			}
			
		}

		if(caseIds.size() > 0)
		{
			List<Case> updatedCases = new List<Case>();
			System.debug('**GF** warmTransferred:caseIds;' + caseIds);
			List<Case> callCases = [SELECT Id, First_Contact_Date__c, 
											Warm_Transferred__c, (SELECT Id, CreatedDate, 
											LastModifiedDate, CreatedBy.SCIEXNow_Role__c, 
											cnx__UniqueId__c, Call_Start_Time__c, Call_End_Time__c, CallDurationInSeconds 
											FROM Tasks WHERE cnx__CTIInfo__c != NULL ORDER BY CreatedDate) 
											FROM Case WHERE Id IN :CaseIds];
			for(Case c : callCases)
			{
				Boolean cUpdated = false;
				System.debug('**GF** warmTransferred:c.Warm_Transferred__c;' + c.Warm_Transferred__c);
				System.debug('**GF** warmTransferred:c.First_Contact_Date__c;' + c.First_Contact_Date__c);
				System.debug('**GF** warmTransferred:c.Tasks.size;' + c.Tasks.size());
				if(c.Tasks.size() > 1)
				{
					System.debug('**GF** warmTransferred:c.Tasks[1].CreatedBy.SCIEXNow_Role__c;' + c.Tasks[1].CreatedBy.SCIEXNow_Role__c);
					System.debug('**GF** warmTransferred:Times Cross;' + (c.Tasks[1].Call_Start_Time__c <= c.Tasks[0].Call_End_Time__c));
					System.debug('**GF** warmTransferred:Times Start;' + c.Tasks[1].Call_Start_Time__c);
					System.debug('**GF** warmTransferred:Times End;' + c.Tasks[0].Call_End_Time__c);
				}
				
				if(!c.Warm_Transferred__c && c.Tasks.size() == 2 && 
				(c.Tasks[1].CreatedBy.SCIEXNow_Role__c == 'TAC' || c.Tasks[1].CreatedBy.SCIEXNow_Role__c == 'SAS') && c.Tasks[1].Call_Start_Time__c <= c.Tasks[0].Call_End_Time__c)
				{
					c.Warm_Transferred__c = true;
					cUpdated = true;
				}
				if(c.First_Contact_Date__c == null)
				{
					for(Task t : c.Tasks)
					{
						if(t.CreatedBy.SCIEXNow_Role__c == 'SAS' || t.CreatedBy.SCIEXNow_Role__c == 'TAC')
						{
							c.First_Contact_Date__c = t.Call_Start_Time__c;
							cUpdated = true;
							break;
						}
					}
				}
				if(cUpdated)
				{
					updatedCases.add(c);
				}
			}
			if(updatedCases.size() > 0)
			{
				update updatedCases;
			}
		}
		System.debug('**GF** warmTransferred:END');
		
	}

	/**
	* @description Sets the DateTime that the first training activity was completed onto
	* the training case.
	*/ 
	public void setFirstActivityDate()
	{
		Set<Id> CaseIds = new Set<Id>();
		for (Task t : (List<Task>)trigger.new)
		{
			//need to ensure that the task is related to a Case, the status has been changed to completed and that its a training task
			//if so add it to the collection so that we can query the case details
			String objId = (String)t.WhatId;
             if(!String.isEmpty(objId) && objId.indexOf(CasePrefix) == 0 && isChanged(t, 'Status') && 
			 t.Status == 'Completed' && t.Training_Task_Type__c != null)
			 {
				CaseIds.add(t.WhatId);
			 }
		}
		//build a new collection for any further updates we need to do
		
		if(CaseIds.size() > 0)
		{
			setFirstActivityDateAsync(CaseIds, System.now());
		}
	}


	/*****************************************************************************************************
	* @description Updates the Cases (if required) from setFirstActivityDate - done Async as Case update
	* @param CasesToCheck the Ids of the cases we need to check
	* @param execTime the DateTime at which the trigger was executed (required as we are running the update async
	*/
	@Future static void setFirstActivityDateAsync(Set<Id> CasesToCheck, DateTime execTime)
	{
		List<Case> updaters = new List<Case>();
		List<Case> CaseDetails = [SELECT Id, RecordType.DeveloperName, First_Training_Activity_Completed__c FROM Case WHERE Id IN :CasesToCheck];
		for(Case c : CaseDetails)
		{
			if(c.RecordType.DeveloperName == 'Training' && c.First_Training_Activity_Completed__c == null)
			{
				c.First_Training_Activity_Completed__c = execTime;
				updaters.add(c);
			}
		}
		if(updaters.size() > 0)
		{
			update updaters;
		}
	}
    
}