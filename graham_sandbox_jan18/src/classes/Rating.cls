public with sharing class Rating {

    // set up properties to get the values from the calling vf page
    public string packId { get; set; }
    public string userId { get; set; }
    //public string rmId { get; set; }
    public integer maxRating { get; set; }
    public integer pp { get; set; }
    public integer cp { get; set; }
    public integer demo { get; set; }
    public integer summary { get; set; }
    public integer addRes { get; set; }
    public string comments { get; set; }
    public List<ratingwrapper> ratwrap { get; set; }
    public List<ratingwrapper> ratwrap1 { get; set; }
    public List<ratingwrapper> ratwrap2 { get; set; }
    public List<ratingwrapper> ratwrap3 { get; set; }
    public List<ratingwrapper> ratwrap4 { get; set; }
    private Rating_Management__c ratingManagementObj;

    // Class constructor
    public Rating() {}
    public void getRatingRecord() {
        packId = ApexPages.currentPage().getParameters().get('pckId');
        maxRating = 5;
        userid = userInfo.getuserId();
        comments = '';
        List<Rating_Management__c> rmc = [
           Select Archive__c, Id, Comments__c,
                PP__c, CP__c, Demo__c, Summary__c, Additional_Resource_Rating__c
           From Rating_Management__c
           Where user__c =: userId AND Package__c =: packId LIMIT 1
        ];
        if(rmc.size() > 0) {
           //rmId = rmc[0].Id;
           ratingManagementObj = rmc[0];
           if(rmc[0].comments__c != null && rmc[0].comments__c != '')
           comments = rmc[0].comments__c;
           callRating();
        }
        else if(packId != null) {
            /**
           Rating_Management__c rm = new Rating_Management__c();
           rm.Package__c = packId;
           rm.user__c = userId;
           //rm.Comments__c = '';
           try {
               insert rm;
               rmId = rm.Id;
               //comments = '';
           }
           catch (exception ex) {}
           **/
           ratingManagementObj = new Rating_Management__c(Package__c = packId, user__c = userId);
           callRating();
        }
        //if(rmId != null){
        //   callRating();
        //}
    }

    // save the new rating and reload the page
    public void SaveRating() {
        /**
        Rating_Management__c c = [
            SELECT Archive__c, Id, Comments__c,
                PP__c, CP__c, Demo__c, Summary__c, Additional_Resource_Rating__c
            FROM Rating_Management__c
            WHERE Id =: rmId LIMIT 1
        ];
        c.PP__c = pp;
        c.CP__c = cp;
        c.Demo__c = demo;
        c.Summary__c = summary;
        c.Additional_Resource_Rating__c = addres;
        c.Comments__c = comments;
        c.Archive__c = false;
        update c;
        **/
        ratingManagementObj.PP__c = pp;
        ratingManagementObj.CP__c = cp;
        ratingManagementObj.Demo__c = demo;
        ratingManagementObj.Summary__c = summary;
        ratingManagementObj.Additional_Resource_Rating__c = addres;
        ratingManagementObj.Comments__c = comments;
        ratingManagementObj.Archive__c = false;
        upsert ratingManagementObj;
    }

    public void DeleteAllRatings()
    {
        List<Rating_Management__c> allPackages = [
            SELECT Package__c, Id, PP__c, CP__c, Demo__c, Summary__c, Additional_Resource_Rating__c
            FROM Rating_Management__c
        ];

        for (Integer i = 0; i < allPackages.size(); i++)
        {
            Rating_Management__c c = allPackages[i];
            c.PP__c = null;
            c.CP__c = null;
            c.Demo__c = null;
            c.Summary__c = null;
            c.Additional_Resource_Rating__c = null;
            update c;
        }
    }

    public void callRating(){
/**
        Rating_Management__c c = [
            SELECT Id, PP__c, CP__c, Demo__c, Summary__c, Additional_Resource_Rating__c
            FROM Rating_Management__c
            WHERE Id =: rmId LIMIT 1
        ];
**/
        // Product Positioning
        ratWrap = new List<ratingWrapper>();
        if (pp == null) {
            if(ratingManagementObj.PP__c != null) pp = Integer.valueof(ratingManagementObj.PP__c);
        }
        for (Integer i = 1; i <= maxRating; i++) {
             string image = (i <= pp) ? 'goldStar' : 'grayStar';
             ratingwrapper rate = new ratingWrapper();
             rate.rating = string.valueOf(i);
             rate.ratingText = 'Set rating to ' + string.valueof(i);
             rate.ratingImage = '/resource/CIPStyleSheet/' + image + '.png';
             ratWrap.add(rate);
        }

        // Competitive Positioning
        ratWrap1 = new List<ratingWrapper>();
        if (cp == null) {
            if(ratingManagementObj.CP__c != null) cp = Integer.valueof(ratingManagementObj.CP__c);
        }
        for (Integer i = 1; i <= maxRating; i++) {
             string image1 = (i <= cp) ? 'goldStar' : 'grayStar';
             ratingwrapper rate1 = new ratingWrapper();
             rate1.rating = string.valueOf(i);
             rate1.ratingText = 'Set rating to ' + string.valueof(i);
             rate1.ratingImage = '/resource/CIPStyleSheet/' + image1 + '.png';
             ratWrap1.add(rate1);
        }

        // Demo Strategy
        ratWrap2 = new List<ratingWrapper>();
        if (demo == null) {
            if(ratingManagementObj.Demo__c != null) demo = Integer.valueof(ratingManagementObj.Demo__c);
            else demo = 0;
        }

        for (Integer i = 1; i <= maxRating; i++) {
             string image2 = (i <= demo) ? 'goldStar' : 'grayStar';
             ratingwrapper rate2 = new ratingWrapper();
             rate2.rating = string.valueOf(i);
             rate2.ratingText = 'Set rating to ' + string.valueof(i);
             rate2.ratingImage = '/resource/CIPStyleSheet/' + image2 + '.png';
             ratWrap2.add(rate2);
        }

        // Summary
        ratWrap3 = new List<ratingWrapper>();
        if (summary == null) {
            if(ratingManagementObj.Summary__c != null) summary = Integer.valueof(ratingManagementObj.summary__c);
        }
        for (Integer i = 1; i <= maxRating; i++) {
             string image3 = (i <= summary) ? 'goldStar' : 'grayStar';
             ratingwrapper rate3 = new ratingWrapper();
             rate3.rating = string.valueOf(i);
             rate3.ratingText = 'Set rating to ' + string.valueof(i);
             rate3.ratingImage = '/resource/CIPStyleSheet/' + image3 + '.png';
             ratWrap3.add(rate3);
         }

        // Additional Resources
        ratWrap4 = new List<ratingWrapper>();
        if (addres == null) {
            if(ratingManagementObj.Additional_Resource_Rating__c != null) addres = Integer.valueof(ratingManagementObj.Additional_Resource_Rating__c);
        }
        for (Integer i = 1; i <= maxRating; i++) {
             string image4 = (i <= addres) ? 'goldStar' : 'grayStar';
             ratingwrapper rate4 = new ratingWrapper();
             rate4.rating = string.valueOf(i);
             rate4.ratingText = 'Set rating to ' + string.valueof(i);
             rate4.ratingImage = '/resource/CIPStyleSheet/' + image4 + '.png';
             ratWrap4.add(rate4);
        }
    }

    public class ratingWrapper {
        public string rating {get;set;}
        public string ratingText {get;set;}
        public string ratingImage {get;set;}
    }
}