/**
** @author Reid Beckett, Cloudware Connections
** @created Jun 4/2014
**
** Test coverage for OpportunityContactRoleShadowHelper, OpportunityContactRoleShadowTrigger and SDSController
** 
**/
@isTest
public class SDSTests {
    private static Opportunity opp;

    public static testMethod void test1_save(){
        setUp();

        SDSController c  = new SDSController(new ApexPages.StandardController(opp));
        c.init();

        c  = new SDSController(new ApexPages.StandardController(opp));
        c.init();
        Test.startTest();
        c.save();
        Test.stopTest();
        c.saveForPdf();
        c.cancel();
         c.newContactRole();
    }
    
    public static testMethod void test2_saveAndReturn(){
        setUp();

        SDSController c  = new SDSController(new ApexPages.StandardController(opp));
        c.init();

        c  = new SDSController(new ApexPages.StandardController(opp));
        c.init();
        Test.startTest();
        c.saveAndReturn();
        Test.stopTest();
        c.saveForPdf();
        c.cancel();
         c.newContactRole();
    }

    public static testMethod void test2_saveForPdf(){
        setUp();

        SDSController c  = new SDSController(new ApexPages.StandardController(opp));
        c.init();

        c  = new SDSController(new ApexPages.StandardController(opp));
        c.init();
        Test.startTest();
        c.saveForPdf();
        Test.stopTest();
        c.cancel();
         c.newContactRole();
    }

    public static void setUp(){
        
        Account testAcct = new Account(Name = 'Test Account', BillingCountry = 'United States');
        insert testAcct;

        Contact cont = new Contact(AccountId = testAcct.Id, FirstName = 'John', LastName = 'Doe', Email = 'johndoe@example.com');
        insert cont;

        OpportunityStage openStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = false and DefaultProbability <= 50 order by DefaultProbability desc limit 1];
        opp = new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), StageName = openStage.MasterLabel, 
                    Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?');
        insert opp;

        OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = opp.Id, ContactId = cont.Id, IsPrimary = true, Role = 'Business User');
        insert ocr;

       
    }
}