// ===========================================================================
// Object: AbsorbTests
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Test coverage for Absorb code
// ===========================================================================
// Changes: 2016-01-30 Reid Beckett
//           Class created
// ===========================================================================
@isTest
public with sharing class AbsorbTests {

	//tests the AbsorbAPIClient createUser() and close() methods directly
	public static testMethod void test_createUser() 
	{
		setUp();
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		AbsorbModel.User u = new AbsorbModel.User();
		u.Username = 'reid';
		//u.Id = AbsorbUtil.generateGUID();
		u.DepartmentId = AbsorbUtil.generateGUID();
		u.FirstName = 'Reid';
		u.LastName = 'Beckett';
		u.EmailAddress = 'Reid.Beckett@example.com';
		client.createUser(u);
		client.close();
	}

	//tests the AbsorbAPIClient authenticate() method
	public static testMethod void test_authenticate_existingToken() 
	{
		setUpExistingToken(-2);
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		client.authenticate();
		client.close();
	}

	//tests the AbsorbAPIClient authenticate() method with an expired token
	public static testMethod void test_authenticate_expiredToken() 
	{
		setUpExistingToken(-6);
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		client.authenticate();
		client.close();
	}

	//tests the AbsorbAPIClient getAllCourses() method
	public static testMethod void test_getAllCourses() 
	{
		setUp();
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		client.getAllCourses();
		client.close();
	}

	//tests the AbsorbAPIClient getAvailableCourses() method
	public static testMethod void test_getAvailableCourses() 
	{
		setUp();
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		String userId = AbsorbUtil.generateGUID();
		client.getAvailableCourses(userId);
		client.close();
	}

	//tests the AbsorbAPIClient getCourseEnrollmentsByUser() method
	public static testMethod void test_getCourseEnrollmentsByUser() 
	{
		setUp();
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		String userId = AbsorbUtil.generateGUID();
		client.getCourseEnrollmentsByUser(userId);
		client.close();
	}

	//tests the AbsorbAPIClient callout() method
	public static testMethod void test_callout() 
	{
		AbsorbLogger.error('Test cover error log');
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		Http httpInstance = new Http();
		new AbsorbAPIClient().callout(httpInstance, req, 'authenticate');
	}

	//tests the AbsorbSyncManager syncCourses() method
	public static testMethod void test_syncCourses() 
	{
		setUp();
		LMS_Course__c c = new LMS_Course__c(Name = 'Test Course 1', LMS_Course_ID__c = AbsorbUtil.generateGUID(), Active__c = true);
		insert c;
		new AbsorbSyncManager().syncCourses();
	}

	//tests the AbsorbSyncEnrollmentsQueueable execute() method
	public static testMethod void test_AbsorbSyncEnrollmentsQueueable_execute() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Test.startTest();
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = true);
		insert c;

		Strategic_Customer_Profile__c scp = new Strategic_Customer_Profile__c(Contact__c = c.id, Key_Account__c = a.Id);
		insert scp;

		c.Key_Opinion_Leader_KOL__c = !(c.Key_Opinion_Leader_KOL__c != null && c.Key_Opinion_Leader_KOL__c);
		update c;        
		Test.stopTest();

		LMS_Course__c course = new LMS_Course__c(Name = 'Test Course 1', LMS_Course_ID__c = AbsorbUtil.generateGUID(), Active__c = true);
		insert course;

		//new AbsorbSyncManager().syncEnrollments();
		Database.executeBatch(new AbsorbSyncEnrollmentsQueueable(), AbsorbSyncEnrollmentsQueueable.ENROLLMENTS_COURSE_BATCH_SIZE);
	}

	//tests the ContactTriggerHandler triggering creation of an LMS user
	public static testMethod void test_ContactTrigger_createUser() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Test.startTest();
		AbsorbAPIClient.TestThrowErrorOnCreateUser = true;
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = true);
		insert c;
		Test.stopTest();
	}

	//tests the AbsorbCreateUserController createUser() method
	public static testMethod void test_AbsorbCreateUserController() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false);
		insert c;

		AbsorbCreateUserController cntr = new AbsorbCreateUserController(new ApexPages.StandardController(c));
		PageReference pageRef = cntr.createUser();
		cntr.backToContact();
		pageRef = cntr.createUser();
	}

	//tests the AbsorbCreateUserController createUser() method, error condition
	public static testMethod void test_AbsorbCreateUserController_error() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false);
		insert c;

		AbsorbAPIClient.TestThrowErrorOnCreateUser = true;

		AbsorbCreateUserController cntr = new AbsorbCreateUserController(new ApexPages.StandardController(c));
		PageReference pageRef = cntr.createUser();
	}

	//tests the AbsorbAPIClient enroll() method
	public static testMethod void test_enroll() 
	{
		setUp();
		AbsorbAPIClient.IAbsorbAPIClient client = AbsorbAPIClient.getInstance();
		client.enroll(AbsorbUtil.generateGUID(), AbsorbUtil.generateGUID());
		client.close();
	}

	//tests the AbsorbSyncEnrollmentsQueueable scheduleable
	public static testMethod void test_AbsorbSyncEnrollmentsQueueable_schedulable() 
	{
		setUp();
		Test.startTest();
		System.schedule('My Test Job at '+Datetime.now().format(), '0 0 12 * * ?', new AbsorbSyncEnrollmentsQueueable());
		Test.stopTest();
	}

	//tests the ContactTriggerHandler and AbsorbAPIClient.updateUser() method
	public static testMethod void test_UpdateUserFromContactUpdate() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		c.Email = 'test2@example.com';
		Test.startTest();
		update c;
		Test.stopTest();
	}

	//tests the OnboardTriggerHandler and AbsorbAPIClient.updateUser() method
	public static testMethod void test_UpdateUserFromOnboard() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123');
		insert cob;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Type__c = 'Primary Learner');
		Test.startTest();
		insert obc;
		Test.stopTest();
	}

	//tests the AbsorbEnrollController enroll() method
	public static testMethod void test_AbsorbEnrollController1() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Test.setCurrentPage(Page.AbsorbEnroll);
		AbsorbEnrollController cont = new AbsorbEnrollController(new ApexPages.StandardController(c));
		cont.initAction();
		cont.availableCourses[0].selected = true;
		cont.enroll();
		cont.cancel();
	}
    
	//tests the AbsorbEnrollController initAction() method
	public static testMethod void test_AbsorbEnrollController2() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = null);
		insert c;

		Test.setCurrentPage(Page.AbsorbEnroll);
		AbsorbEnrollController cont = new AbsorbEnrollController(new ApexPages.StandardController(c));
		cont.initAction();
	}

	//tests the BaseTriggerHandler collectIds() and collectParentIds() method
	public static testMethod void testCoverBaseTriggerHandler() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		MyTempTriggerHandler arth = new MyTempTriggerHandler();
		arth.collectIds(new Account[]{a});
		arth.collectParentIds(new Account[]{a}, 'LastModifiedById');
	}

	public class MyTempTriggerHandler extends BaseTriggerHandler {
		
	}
    
	//tests the TriggerHandler base methods
	public static testMethod void test_TriggerHandler() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		ContactTriggerHandler cth = new ContactTriggerHandler();
		cth.setMaxLoopCount(5);
		cth.addToLoopCount();
		cth.clearMaxLoopCount();
		TriggerHandler.isbypassed('ContactTriggerHandler');
		TriggerHandler.bypass('ContactTriggerHandler');
		TriggerHandler.clearbypass('ContactTriggerHandler');
		TriggerHandler.clearAllBypasses();
		cth.beforeDelete();
		cth.afterDelete();
		cth.afterUndelete();
		TriggerHandler.LoopCount lc = new TriggerHandler.LoopCount();
		lc.getCount();
		lc.getMax();
	}

	//tests the LMSCourseEnrollmentTriggerHandler
	public static testMethod void test_LMSCourseEnrollmentTriggerHandler() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		LMS_Course__c lmsCourse = new LMS_Course__c(LMS_Course_ID__c = AbsorbUtil.generateGUID(), Name = 'Test Course');
		insert lmsCourse;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
			LMS_Course__c = lmsCourse.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
			Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0);
		insert lmsCourseEnrollment;

		Test.startTest();
		lmsCourseEnrollment.LMS_Status__c = 'InProgress';
		lmsCourseEnrollment.LMS_Progress__c = 10;
		update lmsCourseEnrollment;
		Test.stopTest();
	}

	//tests the AbsorbPurgeBatchable batchable
	public static testMethod void test_AbsorbPurgeBatchable_batchable() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Test.startTest();
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = true);
		insert c;
        
		Strategic_Customer_Profile__c scp = new Strategic_Customer_Profile__c(Contact__c = c.id, Key_Account__c = a.Id);
		insert scp;

		c.Key_Opinion_Leader_KOL__c = !(c.Key_Opinion_Leader_KOL__c != null && c.Key_Opinion_Leader_KOL__c);
		update c;        

		LMS_Course__c course = new LMS_Course__c(Name = 'Test Course 1', LMS_Course_ID__c = AbsorbUtil.generateGUID(), Active__c = true);
		insert course;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
			LMS_Course__c = course.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
			Name = 'Test Course Enrollment', LMS_Status__c = 'Not Started', LMS_Progress__c = 0
		);
		insert lmsCourseEnrollment;

		LMS_Session__c lmsSession = new LMS_Session__c(
			LMS_Course__c = course.Id, LMS_Session_ID__c  = AbsorbUtil.generateGUID(), 
			Name = 'Test Session'
		);
		insert lmsSession;

        //new AbsorbSyncManager().syncEnrollments();
		Database.executeBatch(new AbsorbPurgeBatchable(), AbsorbPurgeBatchable.ENROLLMENTS_COURSE_BATCH_SIZE);
		Test.stopTest();
	}

	//tests the AbsorbPurgeBatchable schedulable
	public static testMethod void test_AbsorbPurgeBatchable_schedulable() 
	{
		setUp();
		Test.startTest();
		System.schedule('My Test Job at '+Datetime.now().format(), '0 0 12 * * ?', new AbsorbPurgeBatchable());
		Test.stopTest();
	}

	//tests the LMSCourseEnrollmentTriggerHandler
	public static testMethod void testEnrollmentToCase() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Case caseObj = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), Contactid = c.Id);
		insert caseObj;

		LMS_Course__c lmsCourse = new LMS_Course__c(LMS_Course_ID__c = AbsorbUtil.generateGUID(), Name = 'Test Course', LMS_Course_Type__c = '1');
		insert lmsCourse;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
		            LMS_Course__c = lmsCourse.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
		            Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0
		);

		Test.startTest();
		insert lmsCourseEnrollment;
		caseObj = [select Id, Pre_elearning_for_Field_Service_Engineer__c from Case where Id =:caseObj.Id];
		system.assertEquals(lmsCourseEnrollment.Id, caseObj.Pre_elearning_for_Field_Service_Engineer__c);
		lmsCourse.LMS_Course_Type__c = '2';
		update lmsCourse;
		update lmsCourseEnrollment;
		caseObj = [select Id, Field_Service_Engineer_On_site_Training__c from Case where Id =:caseObj.Id];
		system.assertEquals(lmsCourseEnrollment.Id, caseObj.Field_Service_Engineer_On_site_Training__c);
		lmsCourse.LMS_Course_Type__c = '3';
		update lmsCourse;
		update lmsCourseEnrollment;
		caseObj = [select Id, FAS_On_site_Training__c from Case where Id =:caseObj.Id];
		system.assertEquals(lmsCourseEnrollment.Id, caseObj.FAS_On_site_Training__c);
		Test.stopTest();
	}

	//tests the LMSCourseEnrollmentTriggerHandler
	public static testMethod void testEnrollmentToPreElearningCase() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Case trainingCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), Contactid = c.Id);
		insert trainingCase;

		Case installationCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(), Contactid = c.Id);
		insert installationCase;

		Customer_Onboard__c cobo = new Customer_Onboard__c(InstallationCase__c=installationCase.Id, Training_case__c = trainingCase.Id);
		insert cobo;

		LMS_Course__c lmsCourse = new LMS_Course__c(LMS_Course_ID__c = AbsorbUtil.generateGUID(), Name = 'Test Course', LMS_Course_Type__c = '1');
		insert lmsCourse;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
		            LMS_Course__c = lmsCourse.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
		            Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0
		);

		Test.startTest();
		insert lmsCourseEnrollment;
		lmsCourseEnrollment.LMS_Status__c = 'Complete';
		update lmsCourseEnrollment;

		trainingCase = [select Id, Pre_elearning_for_Field_Service_Engineer__c, Pre_Install_Course_Complete__c from Case where Id =:trainingCase.Id];
		system.assertEquals(lmsCourseEnrollment.Id, trainingCase.Pre_elearning_for_Field_Service_Engineer__c);
		system.assertEquals(true, trainingCase.Pre_Install_Course_Complete__c);
		installationCase = [select Id, Pre_elearning_for_Field_Service_Engineer__c, Pre_Install_Course_Complete__c from Case where Id =:installationCase.Id];
		system.assertEquals(true, installationCase.Pre_Install_Course_Complete__c);
		Test.stopTest();
	}

	//tests the CaseTriggerHandler
	public static testMethod void testCaseToCompleteFASEnrollment() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		LMS_Course__c lmsCourse = new LMS_Course__c(LMS_Course_ID__c = AbsorbUtil.generateGUID(), Name = 'Test Course', LMS_Course_Type__c = '1');
		insert lmsCourse;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
		            LMS_Course__c = lmsCourse.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
		            Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0
		);
		insert lmsCourseEnrollment;

		Case trainingCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), 
			Contactid = c.Id, FAS_On_site_Training__c = lmsCourseEnrollment.Id);
		insert trainingCase;

		Test.startTest();
		trainingCase.Actual_Onsite_Training_Date__c = Date.today();
		update trainingCase;
		Test.stopTest();

		system.assertEquals('Complete', [select LMS_Status__c from LMS_Course_Enrollment__c where Id = :lmsCourseEnrollment.Id].LMS_Status__c);
	}

	//tests the CaseTriggerHandler
	public static testMethod void testCaseToCompleteFSEEnrollment() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		LMS_Course__c lmsCourse = new LMS_Course__c(LMS_Course_ID__c = AbsorbUtil.generateGUID(), Name = 'Test Course', LMS_Course_Type__c = '1');
		insert lmsCourse;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
		            LMS_Course__c = lmsCourse.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
		            Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0
		);
		insert lmsCourseEnrollment;

		Case trainingCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), 
			Contactid = c.Id, Field_Service_Engineer_On_site_Training__c = lmsCourseEnrollment.Id);
		insert trainingCase;

		Case installationCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(), Contactid = c.Id);
		insert installationCase;

		Customer_Onboard__c cobo = new Customer_Onboard__c(InstallationCase__c=installationCase.Id, Training_case__c = trainingCase.Id);
		insert cobo;

		Test.startTest();
		installationCase.Status = 'Closed';
		update installationCase;
		Test.stopTest();

		system.assertEquals('Complete', [select LMS_Status__c from LMS_Course_Enrollment__c where Id = :lmsCourseEnrollment.Id].LMS_Status__c);
	}
    
    public static testMethod void testParseCourseDescription() {
        String descrip = '<p>' + '\n' +
			'</p>' + '\n' +
			'<p><strong></strong>' + '\n' +
			'</p>' + '\n' +
			'<p><strong>Product Number: </strong>TRN00115; <strong>Price: </strong>$2946 USD; <strong>Location </strong>: Redwood City, CA or Framingham, MA; <strong>Course Frequency</strong>: once per month' + '\n' +
			'</p>' + '\n' +
			'<p><strong>Objective</strong>: This is an introductory mass spectrometry class designed for attendees who will be using Analyst® Software for small molecule quantitation through MRM(s) or sMRM(s)™ using all SCIEX Triple Quad™ and/or QTRAP® systems.' + '\n' +
			'</p>' + '\n' +
			'<p>Attendees are provided with a technology overview of the instrument and software as it relates to analyzing small molecules. The course is divided between classroom based lectures providing LC/MS/MS theory and hands-on laboratory exercises designed to reinforce lecture fundamentals.Emphasis is placed on instrument optimization and method development for quantitative analysis including the Configure, Tune and Acquire Analyst® software modes, as well as MultiQuant™. In addition a preventative maintenance demonstration will be included. All analytes used in this class including calibrants and small molecule standards are provided by the instructor. The instrument model used during training may be different from your instrument model.' + '\n' +
			'</p>' + '\n' +
			'<p>Upon completion of the course, attendees should have an understanding of LC/MS/MS theory as it relates to operating the instrument and quantitation through (s)MRM™.The ultimate goal is that attendees will be capable of transferring the principles and practices learned throughout the course and apply them to their specific workflows.' + '\n' +
			'</p>';
        Map<String,String> m = AbsorbSyncManager.parseCourseDescription(descrip);
        system.assertEquals(true, m.containsKey('productNumber'));
        system.assertEquals('TRN00115', m.get('productNumber'));
        system.assertEquals(true, m.containsKey('price'));
        system.assertEquals('$2946 USD', m.get('price'));
        system.assertEquals(true, m.containsKey('location'));
        system.assertEquals('Redwood City, CA or Framingham, MA', m.get('location'));
        system.assertEquals(true, m.containsKey('courseFrequency'));
        system.assertEquals('once per month', m.get('courseFrequency'));
    }
        

	//set up creates custom settings
	public static void setUp() {
		AbsorbLMSSettings__c settings = AbsorbLMSSettings__c.getOrgDefaults();
		settings.Endpoint_URL__c = 'http://example.com';
		settings.API_Username__c = 'user';
		settings.API_Password__c = 'pass';
		settings.API_Token_Timeout__c = 240;
		upsert settings;
	}

	//set up creates custom settings with an existing token
	public static void setUpExistingToken(Integer tokenAgeInHours) {
		AbsorbLMSSettings__c settings = AbsorbLMSSettings__c.getOrgDefaults();
		settings.Endpoint_URL__c = 'http://example.com';
		settings.API_Username__c = 'user';
		settings.API_Password__c = 'pass';
		settings.API_Token_Timeout__c = 240;
		settings.API_Token_Generated_At__c = DateTime.now().addHours(-tokenAgeInHours);
		settings.API_Token__c = 'sometoken';
		upsert settings;
	}
}