/*********************************************************************
Name  : testAbQuotingDataConversions
Author: Appirio, Inc.
Date  : July 30, 2008 
Usage : Used to test the AbQuotingDataConversions

*********************************************************************/

public class testAbQuotingDataConversions{

public static testMethod void testformatDateString(){
  Date d = Date.valueof('2008-07-30');
  System.AssertEquals(AbQuotingDataConversions.formatDateString(d),'30-07-2008');
  System.AssertEquals( AbQuotingDataConversions.formatDateString(null),null );
  d = Date.valueof('2008-07-3');
  System.AssertEquals( AbQuotingDataConversions.formatDateString(d),'03-07-2008' );
}

public static testMethod void testformatDateStringYearFirst(){
  Date d = Date.valueof('2008-07-30');
  System.AssertEquals(AbQuotingDataConversions.formatDateStringYearFirst(d,true),'2008-07-30');
  System.AssertEquals(AbQuotingDataConversions.formatDateStringYearFirst(d,false),'20080730');
  d = Date.valueof('2008-07-3');
  System.AssertEquals(AbQuotingDataConversions.formatDateStringYearFirst(d,false),'20080703');
  string str = AbQuotingDataConversions.formatDateStringYearFirst(null,false);
  }

public static testMethod void testconvertDateStringToDate(){
  String strDate= '30-07-2008';
  Date dateObject = Date.newInstance(2008, 07, 30);
  Date d = AbQuotingDataConversions.convertDateStringToDate(strDate);
  System.AssertEquals(d ,dateObject);
  System.AssertEquals(AbQuotingDataConversions.convertDateStringToDate(null),null);
}

public static testMethod void testconvertSFDCPartnerToCDIPartner(){
  List<Quoting_SAP_Contact_Partners__c> SFDCPartner = new List<Quoting_SAP_Contact_Partners__c>() ;
  for(Integer i=0 ; i<2 ; i++){
    Quoting_SAP_Contact_Partners__c QP= new  Quoting_SAP_Contact_Partners__c() ;
    QP.Partner_Function__c = 'testPartnerfunction' + string.valueOf(i);
    QP.Sold_To__c ='testSold'+string.valueOf(i);
    QP.Contact_Number__c = 'ContactNumber'+string.valueOf(i);
    QP.Sales_Org__c = 'SalesOrg'+string.valueOf(i);
    QP.Sales_Group__c = 'SalesGroup'+string.valueOf(i);
    QP.Contact_Number__c='CN';
    QP.Sales_Org__c='SO';
    QP.Parent_Partner_ID__c=null;
    QP.Sales_Group__c='SG';
    QP.Sales_Office__c='SO';
    QP.Language_Key__c ='SHG';
    QP.First_Name__c='FN';
    QP.Last_Name__c='LN';
    QP.Name4__c ='Name4';
    QP.Name3__c ='Name3';
    QP.City__c='Udaipur';
    QP.Postal_Code__c='313444';
    QP.State_Province__c='Rajasthan';
    QP.Address1__c= 'wqqq';
    QP.Address2__c= 'state road';
    QP.Fax__c = '111234';
    QP.email__c= 'eeeee';
    QP.Country__c='ddd';
    QP.Dept_Number__c='43';
    QP.Telephone__c = '121212';
    QP.Term_Of_Payment__c ='tofpay';
    QP.INCO_Terms__c= 'INco';
    QP.Currency__c  = 'INDIAN';
    SFDCPartner.add(QP);
  }
  List<QuoteSupplementalServices.partners> listCDIPartner = AbQuotingDataConversions.convertSFDCPartnerToCDIPartner(SFDCPartner);
  System.AssertEquals(listCDIPartner.size(),2);
}

public static testMethod void testconvertCDIQuoteSearchResultsToSFDCQuote(){
   Account account1= new Account(Name='ABCAccount',RecordTypeId='01220000000CoEw' ,BillingCountry='France');
   insert account1;

   Account account2= new Account(Name='ABCAccount',RecordTypeId='01220000000CoEw' ,BillingCountry='France');
   insert account2;

   Contact contact = new Contact(FirstName='Test',LastName='Contact',AccountId=account1.Id,RecordTypeId = '012200000008dpe',MailingCountry='France');
   insert contact;
    
   List<QuoteSearchServices.listOfQuotes> locallistofQuotes = new List<QuoteSearchServices.listOfQuotes>();
  
   for(integer i = 0 ; i<2 ; i++){
    QuoteSearchServices.listOfQuotes listinstant = new QuoteSearchServices.listOfQuotes();
        listinstant.CDIQuoteNumber = 'i';
        listinstant.SAPQuoteNumber = 'i'+10;
        listinstant.soldToName = 'soldToName'+i;
        listinstant.soldToID = 'soldToID'+i;
        listinstant.accountName =account1.Name ;
        listinstant.accountID = account1.Id;
        listinstant.contactName = contact.FirstName;
        listinstant.contactID = contact.Id ;
        listinstant.quoteDescription = 'quoteDescription';
        listinstant.status = '';
        listinstant.quoteOwner= '';
        listinstant.department = '';
        listinstant.zipCode = '';
        locallistofQuotes.add(listinstant);
   }
  List<Quote__c> listQuotes = AbQuotingDataConversions.convertCDIQuoteSearchResultsToSFDCQuote(locallistofQuotes);
  System.AssertEquals(listQuotes.size(), 2);
}

public static testMethod void testconvertCDIProductToQuoteLineItem(){
  List<QuoteSearchServices.products> cdiProductList = new List<QuoteSearchServices.products>();
  for(integer i = 0; i < 2 ; i++){
    QuoteSearchServices.products  productinstant = new QuoteSearchServices.products();
    productinstant.productName = 'PN';
    productinstant.productCode = 'productCode';
    productinstant.baseUnitOfMeasure = 'baseUnitOfMeasure';
    if(i==0){
    productinstant.materialOvrrideIndCtr ='materialOvrrideIndCtr';
    }else{
    productinstant.materialOvrrideIndCtr ='X';}
    productinstant.igorCode= 'igorCode';
    productinstant.materialPricingGroup='materialPricingGroup' ;
    cdiProductList.add(productinstant);
  }
 List<Quote_Line_Item__c> Qlinelist =  AbQuotingDataConversions.convertCDIProductToQuoteLineItem(cdiProductList);
 System.AssertEquals(Qlinelist.size(), 2);
 
  List<QuoteSearchServices.quotes> lst11 = new List<QuoteSearchServices.quotes>();
  QuoteSearchServices.quotes qss = new QuoteSearchServices.quotes();
  lst11.add(qss);
  List<Quote__c> lst112 = AbQuotingDataConversions.convertSAPQuoteSearchResultsToSFDCQuote (lst11);
}

//******** Too many query rows: 1001 ************************
//public static testMethod void testwriteQuoteDetailsToSFDC(){
//  QuoteServices.SalesQuote SQ = new QuoteServices.SalesQuote();
//  SQ.quoteHeader = callquoteHeader();
//  SQ.partnerData  = callpartnerData();
//  SQ.termsAndConditions = callTermandConditions()  ;
//  SQ.lineItemDetails= new QuoteServices.ArrayOflineItemDetails()  ;
//  SQ.attachmentDetails =new QuoteServices.ArrayOfattachmentDetails();
//  SQ.outputMessage = 'outputMessage';
//  SQ.quoteModule = 'quoteModule'    ;
//  SQ.lineItemStartIndex ='' ;
//  SQ.lineItemEndIndex ='' ;
//  ID Id =  AbQuotingDataConversions.writeQuoteDetailsToSFDC(SQ);
//}
// **********************************************************
  
public static QuoteServices.partnerData  callpartnerData(){
  QuoteServices.partnerData pdata = new QuoteServices.partnerData();
  pdata.partnerInfo = new QuoteServices.ArrayOfpartnerInfo();
  pdata.shippingAddress = new QuoteServices.shippingAddress();
  pdata.shippingAddress.name1 = 'name1';
  pdata.shippingAddress.name2 = 'name2';
  pdata.shippingAddress.name3 = 'name3';
  pdata.shippingAddress.name4 = 'name4';
  pdata.shippingAddress.address1= 'address1';
  pdata.shippingAddress.address2= 'address2';
  pdata.shippingAddress.city= 'city';
  pdata.shippingAddress.state= 'state';
  pdata.shippingAddress.postalCode= 'postalCode';
  pdata.shippingAddress.country = 'country';
  pdata.shippingAddress.email= 'email';
  pdata.shippingAddress.telephone= 'telephone';
  pdata.shippingAddress.fax= 'fax';
   
  
  pdata.defaultShippingAddress = new  QuoteServices.defaultShippingAddress();
  return pdata ;
}
 
public static QuoteServices.quoteHeader callquoteHeader(){
  QuoteServices.quoteHeader QH = new QuoteServices.quoteHeader();
  QH.sender ='sender';
  QH.receiver = 'receiver';
  QH.cdiQuoteNumber = '122';
  QH.sapQuoteNumber = '23';
  QH.revisionNumber = '12';
  QH.createdByUserID = '005400000011hvy';
  QH.createdBySFDCUserID = '005400000011hvy';
  QH.docType = 'docType';
  QH.salesOrg = 'salesOrg';
  QH.region = 'region';
  QH.district_country = 'district_country';
  QH.shortDescription = 'shortDescription';
  QH.quoteDescription = 'quoteDescription';
  QH.opportunity = getOpportunity();
  QH.revisionSource= '';
  QH.startDate = '2008-07-24';
  QH.submitDate = '2008-07-30';
  QH.status = 'status';
  QH.statusDate = '2008-07-30';
  QH.netPrice = '30';
  QH.totalFreight = '2';
  QH.totalFees = '54';
  QH.totalSalesTax = '12';
  QH.headerDiscount = '2';
  QH.priceRefreshDate = '2008-07-30';
  QH.repRole = 'repRole';
  QH.ownedByUserID = '005400000011hvy';
  QH.ownedBySFDCUserID = '005400000011hvy';
  QH.optNetPrice = '79';
  QH.optTotalFreight= '75';
  QH.optTotalFees = '65';
  QH.optTotalSalesTax = '12';
  return QH;
}

public static string getOpportunity(){
Opportunity Opp = new Opportunity(Name ='Opp',AccountId = getAccount(),StageName='StageName', CloseDate= Date.today());
insert Opp;
return Opp.Id;

}

public static Id getAccount(){
Account account1= new Account(Name='ABCAccount',RecordTypeId='01220000000CoEw');
insert account1;
return  account1.Id;
}

public static QuoteServices.termsAndConditions callTermandConditions(){
  QuoteServices.termsAndConditions TC = new QuoteServices.termsAndConditions();
  TC.outputType = 'outputType';
  TC.outputMedium = 'outputMedium';
  TC.validStartDate = '2008-07-30';
  TC.validEndDate = '2008-07-31';
  TC.estimatedDeliveryDate = '2008-07-31';
  return TC;
}

public static  testMethod void testconvertSFDCQuoteToCDISalesQuote(){
 try{ 
  Account account1= new Account(Name='ABCAccount',RecordTypeId='01220000000CoEw' ,BillingCountry='France');
  insert account1;
  Contact contact = new Contact(FirstName='Test',LastName='Contact',RecordTypeId = '012200000008dpe',AccountId=account1.Id);
  insert contact;
  Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName',Account__c = account1.Id,Preview_Output__c= true, Contact__c =contact.Id);
  insert sfdcQuote;
  Quote_Line_Item__c itemLine = new Quote_Line_Item__c( Quote__c = sfdcQuote.Id );
  insert itemLine;
  List<Attachment> attachments = new List<Attachment>();
  Blob bl    = Blob.valueOf('Body');
  for(integer i= 0; i<2;i++){
    Attachment  at = new Attachment(Name='Name', ContentType= 'ContantType', ParentId = sfdcQuote.Id,Body= bl  );                  
    Insert at;
    attachments.add(at);
  }
  QuoteServices.SalesQuote SQ = AbQuotingDataConversions.convertSFDCQuoteToCDISalesQuote((sfdcQuote.Id));
  SQ = AbQuotingDataConversions.convertSFDCQuoteToCDISalesQuote(itemLine.Quote__c);
  attachments = [Select Id, ParentId from Attachment Limit 1];
  if(attachments.size() > 0){
    SQ = AbQuotingDataConversions.convertSFDCQuoteToCDISalesQuote(attachments.get(0).ParentId );
  }  
 }
 catch(Exception e){
 }
} 

public static  testMethod void testconvertAttachmentsForQuote(){
  Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName');
  insert sfdcQuote;
  List<Attachment> attachments = new List<Attachment>();
  Blob bl    = Blob.valueOf('Body');
  for(integer i= 0; i<2;i++){
    Attachment  at = new Attachment(Name='Name', ContentType= 'ContantType', ParentId = sfdcQuote.Id,Body= bl  );                  
    Insert at;
    attachments.add(at);
  }
  QuoteServices.ArrayOfattachmentDetails ADetails   =  AbQuotingDataConversions.convertAttachmentsForQuote(attachments);
  System.AssertEquals(ADetails.ArrayOfattachmentDetailsItem.size(), 2);
}


public static testMethod void testconvertTemplateItemtoQuoteLine(){
  QuoteSupplementalServices.itemListForTemplates tempitemlist = new QuoteSupplementalServices.itemListForTemplates();
  tempitemlist.price= '33';
  tempitemlist.itemShortText = 'A';
  tempitemlist.discPerctg ='23';
  tempitemlist.manualDiscountValue='34';
  tempitemlist.manualPriceOverride='12';
  tempitemlist.OrdrQntyInUnits='12';
  Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName');
  insert sfdcQuote;
  string sid= sfdcQuote.Id;
  Quote_Line_Item__c qli = AbQuotingDataConversions.convertTemplateItemtoQuoteLine(tempitemlist,23,28,sid,'','p');
}

public static testMethod void testcreateOptionsForPriceUpdate(){
  Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName');
  insert sfdcQuote;
  string sid= sfdcQuote.Id;

  Quote_Line_Item__c instantLineItem = new Quote_Line_Item__c(Name ='Lineitem', 
                                                            Line_Item__c = 34, 
                                                            List_Price__c =23, 
                                                            Manual_Price__c = 45,
                                                            Manual_Discount__c= 07,
                                                            Header_Discount_Value__c=05,
                                                            Product_Num__c = '43',
                                                            Manual_Discount_percent__c = 06,
                                                            Discount_Per_Unit__c =2,
                                                            Manual_Price_Override__c =21,
                                                            Line_Item_Seq__c=2,
                                                            Discount_Level__c= 2,
                                                            Approved_Discount__c = 2,Item_Net_Value__c =87 ,Freight__c =2, Fees__c =43 ,Tax__c = 9,
                                                            Quote__c =sid);
  insert instantLineItem;

  QuoteSupplementalServices.ArrayOfT_ITEMS ArrayItems = AbQuotingDataConversions.createOptionsForPriceUpdate(sfdcQuote.Id);

}

public static testMethod void testcreateItemsForPriceUpdate(){
  Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName');
  insert sfdcQuote;
  string sid= sfdcQuote.Id;
  
  Quote_Line_Item__c instantLineItem = new Quote_Line_Item__c(Name ='Lineitem', 
                                                            Line_Item__c = 34, 
                                                            List_Price__c =23, 
                                                            Manual_Price__c = 45,
                                                            Manual_Discount__c= 07,
                                                            Header_Discount_Value__c=05,
                                                            Product_Num__c = '43',
                                                            Manual_Discount_percent__c = 06,
                                                            Discount_Per_Unit__c =2,
                                                            Manual_Price_Override__c =21,
                                                            Line_Item_Seq__c=2,
                                                            Discount_Level__c= 2,
                                                            Approved_Discount__c = 2,Item_Net_Value__c =87 ,Freight__c =2, Fees__c =43 ,Tax__c = 9,
                                                            Optional_Flag__c = true,
                                                            Quote__c =sid);
  insert instantLineItem;
  
 QuoteSupplementalServices.ArrayOfT_ITEMS ArrayItems = AbQuotingDataConversions.createItemsForPriceUpdate((sfdcQuote.Id),true);

}







}