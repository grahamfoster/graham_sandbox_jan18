public class MySFDCYTDWinRateDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {
	
	public MySFDCYTDWinRateDashboard() {
		super();
		this.quota = 100;
		report = 'YTDWinRate';
	}

	/* custom fields */

	public String getReportTitle() {
		return 'YTD Win Rate';
	}

	/* the criteria for the query */
	protected override String criteria() {
		String crit = 'OwnerId in :userIds ' + 
			'and IsClosed = true ' +
			'and ((Shippable_Date__c >= :startDate and Shippable_Date__c <= :endDate) ' +
			'or (Shippable_Date__c = NULL and CloseDate >= :startDate and CloseDate <= :endDate))';

		if(MySFDCUtil.isServiceUser(filter.runAs)) {
			crit += ' and RecordType.Name = \'Service\'';
		}

		if(!String.isBlank(filter.marketVertical)) {
			if(filter.marketVertical == 'Clinical & Forensic') {
				crit += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(filter.marketVertical == 'Food & Environmental') {
				crit += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				crit += ' and Market_Vertical__c = :marketVertical';
			}
		}
		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry in :country';
			}
		}
		return crit;
	}

	public override void query() {
		this.actual = 0;
		String marketVertical = filter.marketVertical;
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		String query = 'select IsWon, SUM(Amount) amt from Opportunity where ' + criteria() + ' group by IsWon';

		Decimal wonAmount = 0;
		Decimal lostAmount = 0;
		for(AggregateResult aggr : Database.query(query)) {
			Boolean isWon = (Boolean)aggr.get('IsWon');
			Decimal amt = aggr.get('amt') != null ? (Decimal)aggr.get('amt') : 0;

			if(amt != null) {
				if(isWon) wonAmount = amt;
				else lostAmount = amt;
			}
		}
		if(wonAmount + lostAmount > 0) this.actual = (wonAmount/(wonAmount+lostAmount))*100;
	}

	/* the default sorting criteria for the query */
	protected override String defaultSorting() {
		return ' order by Shippable_Date__c desc, CloseDate desc';
	}

	/* the list of fields for the report */
	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	/* Generate the google chart data */
	public override GoogleGaugeChartData getGoogleGaugeChartData() {
		GoogleGaugePercentageChartData c = new GoogleGaugePercentageChartData(ABSciexDashboardFactory.YTD_WIN_RATE);
		c.gaugeLabel = '(%)';
		c.title = getReportTitle();
		c.actual = this.actual;
		c.quota = this.quota;
		c.greenFrom = 30;
		c.greenTo = 50;
		c.yellowFrom = 50;
		c.yellowTo = 70;
		return c;
	}

}