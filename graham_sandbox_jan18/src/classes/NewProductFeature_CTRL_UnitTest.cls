@isTest
public with sharing class NewProductFeature_CTRL_UnitTest {
	static TestMethod void NewProductFeature_CTRL_UnitTest1()
	{
		Product_Category__c prodCategRecObj = Util.CreateProductCategory();
		Specifications_Meta__c spec =util.CreateProductfeatureCategory(prodCategRecObj.id);
		
		ApexPages.currentPage().getParameters().put('catid',prodCategRecObj.Id);
		NewProductFeature_CTRL NewProductFeatureObj = new NewProductFeature_CTRL();
				
		NewProductFeatureObj.InsertSpecification();
				
		NewProductFeatureObj.specmetalist.clear();
		NewProductFeatureObj.specmetalist.add(spec); 
		NewProductFeatureObj.InsertSpecification();
		
		NewProductFeatureObj.InsertMoreSpecification();
		NewProductFeatureObj.RemoveSpecification();
	}
	
}