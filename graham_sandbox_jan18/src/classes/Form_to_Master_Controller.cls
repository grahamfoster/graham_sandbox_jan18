/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : October 4, 2012
Description    : controller for Form_to_Master_Control VF page
History        :

Modified	   : June 16, 2017 by Wes
*/


public class Form_to_Master_Controller 
{
    public Case caze {get;set;}
    
    public PageReference init(){
        String caseId = ApexPages.currentPage().getParameters().get('id');
        if(caseId != null) {
            Case[] cases = [select Id, Subject, CreatedDate, Description, Country__c, Asset.Name, 
                Contact.Name, Contact.Email, Contact.Phone, CaseNumber,SVC_Oracle_Service_Request_ID__c,
                Account.Name, Account.BillingStreet, Account.BillingState, Account.BillingCity, Account.BillingPostalCode, Account.BillingCountry from Case where id = :caseId];
            if(cases.size() > 0) {
                caze = cases[0];
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case not found'));
                return null;
            }
        }
        return null;
    }
    
    public String createdDate {
        get {
            return caze.CreatedDate != null ? caze.CreatedDate.format('yyyy-MM-dd') : null;
        }
    }
    
    public String createdDate2 {
        get {
            return caze.CreatedDate != null ? caze.CreatedDate.format('yyyy-MMM-dd') : null;
        }
    }

    public String formUrl {
        get {
            return 'http://amcon-mcswebp1.netadds.net/mc/main/index.cfm?event=startTask&ID=STOMERCOMPLAINTADV'; //production
            //return 'http://amcon-mcswebq1.netadds.net/mctest/main/index.cfm?event=startTask&ID=STOMERCOMPLAINTADV'; // test in sandbox
        }
    }
    /**
 public String Contact_Name 
 {
 get
 {    
    return Apexpages.currentPage().getparameters().get('con');     
 }
 set;
 }
 
 public String Account_Name 
 {
 get
 {
    return Apexpages.currentPage().getparameters().get('acc');       
 }
 set;
 }
 
 public String Contact_Email
 {
 get
 {
    return Apexpages.currentPage().getparameters().get('email');       
 }
 set;
 } 
 
 public String Contact_Phone
 {
 get
 {
    return Apexpages.currentPage().getparameters().get('phone');       
 }
 set;
 }
  
 public String Subject   
 {
 get
 {
    return Apexpages.currentPage().getparameters().get('subject');       
 }
 set;
 } 
 **/
 
     static testMethod void Var() 
    {
        Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert testAccount;
        
        Contact testContact = new Contact(AccountId = testAccount.Id, FirstName= 'Test', LastName = 'Contact');
        insert testContact;
        
        Case c = new Case(AccountId = testAccount.Id, ContactId = testContact.Id, Subject = 'Test case');
        insert c;
        
        ApexPages.currentPage().getParameters().put('id',c.Id);
        
//      ApexPages.currentPage().getParameters().put('con','Test Name');
//      ApexPages.currentPage().getParameters().put('con','Test Name');
//      ApexPages.currentPage().getParameters().put('AAA Inc.c','');
//      ApexPages.currentPage().getParameters().put('email','aff@wrqwer.by');  
//      ApexPages.currentPage().getParameters().put('phone','3651631651');
//      ApexPages.currentPage().getParameters().put('subject','Adfgfghetyhetyh'); 
      
     Form_to_Master_Controller  test = new Form_to_Master_Controller();
     test.init();
    String cd = test.createdDate;
    String cd2 = test.createdDate2;        
    String theURL = test.formUrl;        
//     String adsa1 = test.Contact_Name;
//     String adsa2 = test.Account_Name;
     //String adsa3 = test.Contact_Email;
     //String adsa4 = test.Contact_Phone;
     //String adsa5 = test.Subject;                    
    }
}