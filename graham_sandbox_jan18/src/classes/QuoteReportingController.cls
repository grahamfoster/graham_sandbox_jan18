public with sharing class QuoteReportingController {
    Boolean picklistsAlreadyRetrieved = false;

 //Login page fields
  String cdiUsername = 'CDI_Username';
  String cdiPassword;
  
  public String returnURLFromLogin;
  
  //Landing Page Search Fields 
  String filter_Account;
  String filter_Contact;
  String filter_SoldTo;
  String filter_ShortDesc;
  String filter_CDIQuote;
  String filter_SAPQuote;
  String filter_QuoteStatusQLP;
  String filter_Product;
  String filter_ProductCategory;
  String filter_ProductLine;
  String filter_PlanCode;
  String filter_IGORItemClass;
  String filter_IGORCode;
  String filter_Template;
  List<SelectOption> filter_QuoteStatus = null;
  List<Quote__c> result = null;
  String selectedQuoteId = null;
  String selectedCDIQuoteNum = null;
  
  String filter_District = null;
  String filter_salesRep = null;
  
  List<SelectOption> productLine = null;
  List<SelectOption> productLineDesc = null;
  List<SelectOption> planCode = null;
  List<SelectOption> planCodeDesc = null;
  List<SelectOption> iGORItemClass = null;
  List<SelectOption> iGORItemClassDesc = null;
  List<SelectOption> iGORCode = null;
  List<SelectOption> iGORCodeDesc = null;
  
  List<Product_Hierarchy__c> productHierarchy=null;
  String sProductLine='';
  String IGORItemClassValue ='';
  String ProductLineDescValue = '';
  String IGORItemClassDescValue ='';
  String PlanCodeValue ='';
  String IGORCodeValue ='%%';
  String PlanCodeDescValue = '';
  String IGORCodeDescValue = '%%';
  
  //IGORCode Range
  String IGORCodeRangeSelected;
  public void setIGORCodeRangeSelected(String range){
  	System.debug('IGOR Code Range: ' + range);
  	this.IGORCodeRangeSelected=range;
    iGORCode=getNewList();
    iGORCode.add(new SelectOption('0-9','IGOR Codes: 0-9'));
    iGORCode.add(new SelectOption('A-E','IGOR Codes: A-E'));
    iGORCode.add(new SelectOption('F-J','IGOR Codes: F-J'));
    iGORCode.add(new SelectOption('K-T','IGOR Codes: K-T'));
    iGORCode.add(new SelectOption('U-Z','IGOR Codes: U-Z'));
    iGORCode.add(new SelectOption('---','---------------'));
  }
  public String getIGORCodeRangeSelected(){
  	return this.IGORCodeRangeSelected;
  }
    
  public void setIGORCodeDescValue(String value){
  this.IGORCodeDescValue = value;
 }
 public String getIGORCodeDescValue(){
   return this.IGORCodeDescValue;
 }
 public void setPlanCodeDescValue(String value){
   this.PlanCodeDescValue = value;  
 }
 
 public String getPlanCodeDescValue(){
  return this.PlanCodeDescValue;
 }
 public void setIGORItemClassValue(String value){
   this.IGORItemClassValue = value;
 }
 
 public String getIGORItemClassValue(){
   return this.IGORItemClassValue;  
 }
 
 public void setProductLineDescValue(String value){ 
    this.ProductLineDescValue = value;
 }
 public String getProductLineDescValue(){
   return this.ProductLineDescValue;    
 }
 
 public void setIGORItemClassDescValue(String value){
   this.IGORItemClassDescValue = value;
 }
 public String getIGORItemClassDescValue(){
   return this.IGORItemClassDescValue;  
 }
 //public void setPlanCodeValue(String value){
 // this.PlanCodeValue = value;
 //}
 //public String getPlanCodeValue(){
 //return this.PlanCodeValue;
 //}
 
 public void setIGORCodeValue(String value){
   this.IGORCodeValue = value;
 }
 
 public String getIGORCodeValue(){
  return this.IGORCodeValue;
 }
  public void setProductLineValue(String value){
      this.sProductLine = value;
  }
  public String getProductLineValue(){
    return this.sProductLine;
  }
  
  //==========================================================  
  //================ Getter/Setter Methods ===================
  //==========================================================  

  
  //----- Landing/Search Page --------------
  public String getFilter_CDIQuote(){
    return filter_CDIQuote;
  }
  public void setfilter_CDIQuote(String newCdiQuoteNum){
    filter_CDIQuote = newCdiQuoteNum;
  }
  
  public String getFilter_District(){
    return filter_District;
  }
  public void setfilter_District(String d){
    filter_District = d;
  }
  
  public String getFilter_salesRep(){
    return filter_salesRep;
  }
  public void setfilter_salesRep(String s){
    filter_salesRep = s;
  }
  
  
  public List<SelectOption> getFilter_QuoteStatus(){

    filter_QuoteStatus = getNewList();

    List<Quoting_SAP_Country_Matrix__c> statusValues =[SELECT q.Value__c, q.Description__c 
                                                       FROM Quoting_SAP_Country_Matrix__c q 
                                                       WHERE value_type__c = :AbConstants.STATUS_VALUES 
                                                       ORDER BY q.Sequence_On_List__c];   

    for(Quoting_SAP_Country_Matrix__c statVal:statusValues){
      filter_QuoteStatus.add(new SelectOption(statVal.Value__c,statVal.Description__c));
    } 

    return filter_QuoteStatus;
  }
  
  public List<Quote__c> getResult() {
    return result;
  }
    
  
  public List<SelectOption> getproductLine(){
    retrieveProductPicklists();
    return productLine;
  }
  public List<SelectOption> getproductLineDesc(){
    retrieveProductPicklists();
    return productLineDesc;
  }

  public List<SelectOption> getplanCode(){
    retrieveProductPicklists();
    return planCode;
  }
  public List<SelectOption> getplanCodeDesc(){
    retrieveProductPicklists();
    return planCodeDesc;
  }
  
  public List<SelectOption> getiGORItemClass(){
    retrieveProductPicklists();
    return iGORItemClass;
  }
  public List<SelectOption> getiGORItemClassDesc(){
    retrieveProductPicklists();
    return iGORItemClassDesc;
  }  
  
  public List<SelectOption> getiGORCode(){
    retrieveProductPicklists();
    return iGORCode;
    
    /*
    if(IGORCodeRangeSelected == null){
    	iGORCode=getNewList();
    	iGORCode.add(new SelectOption('0-9','IGOR Codes: 0-9'));
    	iGORCode.add(new SelectOption('A-E','IGOR Codes: A-E'));
    	iGORCode.add(new SelectOption('F-J','IGOR Codes: F-J'));
    	iGORCode.add(new SelectOption('K-T','IGOR Codes: K-T'));
    	iGORCode.add(new SelectOption('U-Z','IGOR Codes: U-Z'));
    	return iGORCode;
    }else if(IGORCodeRangeSelected != '0-9' && IGORCodeRangeSelected != 'A-E' && IGORCodeRangeSelected != 'F-J' && IGORCodeRangeSelected != 'K-T' && IGORCodeRangeSelected != 'U-Z' && IGORCodeRangeSelected !='') {
        return iGORCode;
    }else{
		if (igorCodeValue == '0-9') {
          List<Product_Hierarchy__c> igorList = [Select IGOR_Code__c,IGOR_Code_Description__c from Product_Hierarchy__c where IGOR_Code__c LIKE '0%' OR IGOR_Code__c LIKE '1%' OR IGOR_Code__c LIKE '2%' OR IGOR_Code__c LIKE '3%' 
                        OR IGOR_Code__c LIKE '4%' OR IGOR_Code__c LIKE '5%' OR IGOR_Code__c LIKE '6%' OR IGOR_Code__c LIKE '7%' OR IGOR_Code__c LIKE '8%' OR IGOR_Code__c LIKE '9%' order by IGOR_Code__c LIMIT 996];
          for(Product_Hierarchy__c p: igorList){
              iGORCode.add(new SelectOption(p.IGOR_Code__c,p.IGOR_Code__c+', '+p.IGOR_Code_Description__c));
          }
      } else if (igorCodeValue == 'A-E') {
          List<Product_Hierarchy__c> igorList = [Select IGOR_Code__c,IGOR_Code_Description__c from Product_Hierarchy__c where IGOR_Code__c LIKE 'A%' OR IGOR_Code__c LIKE 'B%' OR IGOR_Code__c LIKE 'C%' OR IGOR_Code__c LIKE 'D%' 
                        OR IGOR_Code__c LIKE 'E%' order by IGOR_Code__c LIMIT 996];
          for(Product_Hierarchy__c p: igorList){
              iGORCode.add(new SelectOption(p.IGOR_Code__c,p.IGOR_Code__c+', '+p.IGOR_Code_Description__c));
          }         
      } else if (igorCodeValue == 'F-J') {
        List<Product_Hierarchy__c> igorList = [Select IGOR_Code__c,IGOR_Code_Description__c from Product_Hierarchy__c 
                                               where IGOR_Code__c LIKE 'F%' OR IGOR_Code__c LIKE 'G%' OR IGOR_Code__c LIKE 'H%' OR IGOR_Code__c LIKE 'I%' OR IGOR_Code__c LIKE 'J%' order by IGOR_Code__c LIMIT 996];
        for(Product_Hierarchy__c p: igorList){
            iGORCode.add(new SelectOption(p.IGOR_Code__c,p.IGOR_Code__c+', '+p.IGOR_Code_Description__c));
        }         

      } else if (igorCodeValue == 'K-T') {
          List<Product_Hierarchy__c> igorList = [Select IGOR_Code__c,IGOR_Code_Description__c from Product_Hierarchy__c where IGOR_Code__c LIKE 'K%' OR IGOR_Code__c LIKE 'L%' OR IGOR_Code__c LIKE 'M%' OR IGOR_Code__c LIKE 'N%' 
                        OR IGOR_Code__c LIKE 'O%' OR IGOR_Code__c LIKE 'P%' OR IGOR_Code__c LIKE 'Q%' OR IGOR_Code__c LIKE 'R%' OR IGOR_Code__c LIKE 'S%' OR IGOR_Code__c LIKE 'T%'  order by IGOR_Code__c LIMIT 996];
          for(Product_Hierarchy__c p: igorList){
              iGORCode.add(new SelectOption(p.IGOR_Code__c,p.IGOR_Code__c+', '+p.IGOR_Code_Description__c));
          }               
      } else if (igorCodeValue == 'U-Z') {
          List<Product_Hierarchy__c> igorList = [Select IGOR_Code__c,IGOR_Code_Description__c from Product_Hierarchy__c where IGOR_Code__c LIKE 'U%' OR IGOR_Code__c LIKE 'V%'
                        OR IGOR_Code__c LIKE 'W%' OR IGOR_Code__c LIKE 'X%' OR IGOR_Code__c LIKE 'Y%' OR IGOR_Code__c LIKE 'Z%' order by IGOR_Code__c LIMIT 996];
          for(Product_Hierarchy__c p: igorList){
              iGORCode.add(new SelectOption(p.IGOR_Code__c,p.IGOR_Code__c+', '+p.IGOR_Code_Description__c));
          }
      }               
      return iGORCode;		
    }
    */
  }
  
  public List<SelectOption> getiGORCodeDesc(){
    retrieveProductPicklists();
    return iGORCodeDesc;
  }  
  
  public void setFilter_SAPQuote (String value){    this.filter_SAPQuote = value;  }
  public String getFilter_SAPQuote (){    return this.filter_SAPQuote;  }
  
  public void setFilter_QuoteStatusQLP (String value){    this.filter_QuoteStatusQLP = value;  }
  public String getFilter_QuoteStatusQLP (){    return this.filter_QuoteStatusQLP;  }
  
  public void setPlanCodeValue(String value){    this.PlanCodeValue = value;  }
  public String getPlanCodeValue(){    return this.PlanCodeValue;  }
  
  public void setFilter_ProductCategory (String value){    this.filter_ProductCategory = value;  }
  public String getFilter_ProductCategory(){    return this.filter_ProductCategory;  }
  
  public void setFilter_Product(String value){    this.filter_Product = value;  }
  public String getFilter_Product (){    return this.filter_Product ;  }
  
  public void setFilter_ProductLine(String value){    this.filter_ProductLine = value;  }
  public String getFilter_ProductLine (){    return this.filter_ProductLine ;  }
   
  public void setFilter_PlanCode (String value){    this.filter_PlanCode = value;  }
  public String getFilter_PlanCode (){    return this.filter_PlanCode;  }
  
  public void setFilter_IGORItemClass(String value){    this.filter_IGORItemClass = value;  }
  public String getFilter_IGORItemClass(){    return this.filter_IGORItemClass;  }
  public void setFilter_IGORCode(String value){   this.filter_IGORCode = value;  }
  public String getFilter_IGORCode(){    return this.filter_IGORCode;  }
  public void setFilter_Template(String value){    this.filter_template = value;  }
  public String getFilter_Template(){   return this.filter_template;  }

 //==========================================================  
  //==== Methods to retrieve Product Picklists from SFDC db ==
  //==========================================================  
  private List<SelectOption> getNewList(){
    List<SelectOption> l=new List<SelectOption>();
    l.add(new SelectOption('','--None--'));
    return l;
  }
  private void retrieveProductPicklists(){ 
    if(picklistsAlreadyRetrieved){ return; }
  
    productLine=getNewList();
    productLineDesc=getNewList();

    for(Product_Line__c p:[Select Name,Desc__c from Product_Line__c Limit 999]){
       productLine.add(new SelectOption(p.Name,p.Name + ' - ' + p.Desc__c));
       productLineDesc.add(new SelectOption(p.Desc__c,p.Desc__c));
    } 

    planCode=getNewList();
      planCodeDesc=getNewList();
      for(Plan_Code__c p:[Select Name,Plan_Code_Description__c from Plan_Code__c order by Name]){
        planCode.add(new SelectOption(p.Name,p.Name + ' - ' + p.Plan_Code_Description__c));
        planCodeDesc.add(new SelectOption(p.Plan_Code_Description__c,p.Plan_Code_Description__c));
      }

    iGORItemClass=getNewList();
    iGORItemClassDesc=getNewList();
    for(Igor_Item_Class__c p:[Select Name, IGOR_Item_Class_Description__c from Igor_Item_Class__c]){
      iGORItemClass.add(new SelectOption(p.Name,p.Name + ' - ' + p.IGOR_Item_Class_Description__c));
      iGORItemClassDesc.add(new SelectOption(p.IGOR_Item_Class_Description__c,p.IGOR_Item_Class_Description__c));
    }
/*  
    iGORCode=getNewList();
    iGORCodeDesc=getNewList();
    for(Igor_Code__c p:[Select Name,Desc__c from Igor_Code__c Where name != null order by Name Limit 999]){
       iGORCode.add(new SelectOption(p.Name,p.Name + ' - ' + p.Desc__c));
       iGORCodeDesc.add(new SelectOption(p.Desc__c,p.Desc__c));
    }
*/
	iGORCode=getNewList();
    iGORCode.add(new SelectOption('0-9','IGOR Codes: 0-9'));
    iGORCode.add(new SelectOption('A-E','IGOR Codes: A-E'));
    iGORCode.add(new SelectOption('F-J','IGOR Codes: F-J'));
    iGORCode.add(new SelectOption('K-T','IGOR Codes: K-T'));
    iGORCode.add(new SelectOption('U-Z','IGOR Codes: U-Z'));
    	
    picklistsAlreadyRetrieved = true;
  }
  
  //** Debug utility method to print out all parameters selected by the user.
  //
  
  private void debugAllSearchParams(){
    //System.debug('============================================');
    //System.debug('============ Search Params =================');
    //System.debug('filter_CDIQuote=' + filter_CDIQuote);
    //System.debug('filter_QuoteStatus='+filter_QuoteStatus);
    //System.debug('============================================');
    //System.debug('============================================');
  }
  

  //==========================================================  
  //================ Action Methods ==========================
  //==========================================================  

  public List<Product_Hierarchy__c> getSearchResults(){
      return  productHierarchy;
  }
  public PageReference searchProductHierarchy(){
    String selProductLine = getProductLineValue()== null ? '%%' : getProductLineValue() ;
    String selIGORItemClassValue = getIGORItemClassValue()== null ? '%%' : getIGORItemClassValue() ;
    String selProductLineDescValue = getProductLineDescValue()== null ? '%%' : getProductLineDescValue() ;
    String selIGORItemClassDescValue = getIGORItemClassDescValue() == null ? '%%' : getIGORItemClassDescValue() ;
    String selPlanCodeValue = getPlanCodeValue()== null ? '%%' : getPlanCodeValue() ;
    String selIGORCodeValue = getIGORCodeValue()== null ? '%%' : getIGORCodeValue() ;
    String selPlanCodeDescValue = getPlanCodeDescValue()== null ? '%%' : getPlanCodeDescValue() ;
    String selIGORCodeDescValue = getIGORCodeDescValue()== null ? '%%' : getIGORCodeDescValue() ;
    for(Product_Hierarchy__c[] prodHieracgyBulk:[Select p.IGOR_CODE__c, p.IGOR_CODE_DESCRIPTION__c,
                     p.IGOR_ITEM_CLASS__c,p.IGOR_ITEM_CLASS_DESC__c, p.PLAN_CODE__c, 
                     p.PLAN_CODE_DESC__c, p.PRODLINE__c,
                     p.PRODLINE_DESC__c from Product_Hierarchy__c p 
                     where p.PRODLINE__c like :selProductLine 
                     and p.IGOR_CODE__c like :selIGORCodeValue 
                     and p.IGOR_CODE_DESCRIPTION__c like :selIGORCodeDescValue
                     and p.IGOR_ITEM_CLASS__c like :selIGORItemClassValue
                     and p.IGOR_ITEM_CLASS_DESC__c like :selIGORItemClassDescValue
                     and p.PLAN_CODE__c like :selPlanCodeValue
                     and p.PLAN_CODE_DESC__c like :selPlanCodeDescValue
                     and p.PRODLINE_DESC__c like :selProductLineDescValue]){
                        
        productHierarchy = prodHieracgyBulk;
    }
    

                     

    return null;
  } 
  
  public PageReference quoteSearchCDI() {
    debugAllSearchParams();
    result = (List<Quote__c>)[SELECT Account__c, Contact__c, CDI_Quote_Num__c, SAP_Quote_Num__c, Sold_To__c, Ship_To__c, Status__c, Create_Date__c, Expire_Date__c, Zip_Code__c, Short_Description__c FROM Quote__c LIMIT 300];
    //return QuotePageRedirects.LandingPage();
    
    return null;
  }
  
  public PageReference QuoteSearchSAP() {
    return QuotePageRedirects.LandingPage();
  }
  public PageReference QuoteNewHeader() {
    return QuotePageRedirects.NewHeader();
  }
  public PageReference QuoteNewPartner() {
    return QuotePageRedirects.NewPartner();
  }
  public PageReference QuoteNewOptions() {
    return QuotePageRedirects.NewOptions();
  }
  public PageReference QuoteProductSelect() {
    return QuotePageRedirects.ProductSelect();
  }
  public PageReference QuoteProductDiscount() {
    return QuotePageRedirects.ProductDiscount();
  }
  public PageReference QuoteProductSort() {
    return QuotePageRedirects.ProductSort();
  }
  public PageReference QuoteProductSearch() {
    return QuotePageRedirects.ProductSearch();
  }
  public PageReference QuoteTemplateSearch() {
    return QuotePageRedirects.TemplateSearch();
  }
  public PageReference QuoteMaterials() {
    return QuotePageRedirects.Materials();
  }
 
  
  //==========================================================  
  //================ Page Redirects ==========================
  //==========================================================  
  public PageReference redirectToLandingPage() {    
    return Page.QuoteReporting;
  }
  
  //may combine these 3
  public PageReference redirectToNewQuote1() {    
    return Page.QuoteNewHeader;
  }
  public PageReference redirectToNewQuote2() {    
    return Page.QuoteNewPartner;
  }
  public PageReference redirectToNewQuote3() {    
    return Page.QuoteNewOptions;
  }
  public PageReference redirectToProductSelect() {    
    return Page.QuoteProductSelect;
  }
 
  public PageReference redirectToProductDiscount() {    
    return Page.QuoteDiscount;
  }
  public PageReference redirectToProductSort() {    
    return Page.QuoteProductSort;
  }
  public PageReference redirectToProductSearch() {    
    return Page.QuoteProductSearch;
  }
  public PageReference redirectToTemplateSearch() {    
    return Page.QuoteTemplateSearch;
  }
  public PageReference redirectToMaterials() {    
    return Page.QuoteMaterials;
  }
 
  //========================================================== 
  //================ Proxy an Object ========================= 
  //========================== added by Tony Pederson ======== 
  public Quote__c getQuoteProxy() {
        return QuoteProxy;
  } 
  
  private final Quote__c QuoteProxy = new Quote__c();
  
  //========================================================== 
  //================ Get Report Login ======================== 
  //========================================================== 
  
  public String getCdiUsername(){    
    try{
      User currentUser = [SELECT u.Alias FROM User u WHERE Id = :UserInfo.getUserId()];
      return currentUser.Alias;
    } catch (Exception e){
      //System.debug('QUOTING: error retrieving user alias from User record for user: ' + UserInfo.getUserId());
    }
    return cdiUsername;  
  }
  public void setCdiUsername(String newUsername){    cdiUsername = newUsername;  }
  public String getCdiPassword(){    return cdiPassword;  }
  public void setCdiPassword(String newPswd){    cdiPassword= newPswd;  }
  
  //-------------------------------------------------------------
  //--------------- Login Page Actions ------------------------
  //-------------------------------------------------------------
  public PageReference loginToCDI() {  
  /*  
      errorMessage = '';
      try {
          ABQuotingUtils.loginToCDI(cdiUsername, cdiPassword); 
          errorMessage='';
          PageReference returnFromLoginPageRef;
          
          if(returnURLFromLogin == null) {
            return Page.QuoteReporting;
          } else {
            System.debug('QUOTING: returning to PageRef ' + returnURLFromLogin);
            
            returnFromLoginPageRef = new PageReference(returnURLFromLogin); 
            return returnFromLoginPageRef;          
          }
      } catch (QuotingAuthenticationException authException) {
        System.debug('QUOTING: exception "loggingIN" via retrieval of next CDI quote num ' + authException);
        errorMessage = authException.getMessage();    
        return null;
      } catch (Exception qe) {
        errorMessage = qe.getMessage();
        return null;      
      }
    */
    	PageReference pref = null;
    	try{
    		ABQuotingUtils.loginToCDI(cdiUsername, cdiPassword);
    		pref = new PageReference('/apex/quotereporting');
    		pref.setRedirect(true);
    	}catch(Exception e){
    		pref = new PageReference('/apex/quotereporting');
    		pref.setRedirect(true);
    	}
    	return pref;
    }
  
    String errorMessage = '';
    
    public void setErrorMessage(String value){    this.errorMessage = value;  }
    public String getErrorMessage(){    return this.errorMessage;  }
  
  //---------------------------------------------------------------------
  //--------- QuoteReporting --------------------------------------------
  //---------------------------------------------------------------------
    private PageReference pref;
    private String wmResponse;
    private String loadProgress;
    private String[] soldToList;
    private boolean isIgnored = false;		//This switch is used for deployment to bypass big query
    
    public void setIsIgnored(boolean b){ isIgnored = b; }
    
    public QuoteReportingController() {  

        String returnURLFromOtherPage = System.currentPageReference().getParameters().get(AbConstants.RETURN_URL_FROM_LOGIN);
        System.debug('QUOTING: returnURLFromOtherPage ' + returnURLFromOtherPage);
        if(returnURLFromOtherPage != null && returnURLFromOtherPage != ''){
            returnURLFromLogin = returnURLFromOtherPage; 
        }     
    }
    
    public String getWmResponse(){ return wmResponse; }
    
    public void deleteData(){
        QuoteReportUtils.deleteReportData();
    }
    
    public void deleteAll(){
		QuoteReportUtils.deleteAll();
    }
    
    public PageReference createReport(){
        System.debug('QuoteReportingController.createReport() is fired...');
        wmResponse = 'Please wait...';
        deleteData();
        
        PageReference pref = Page.QuoteReportStatus;
        //pref.setRedirect(true);
        return pref;
    }
    
    public String[] getSoldToList(){
    	System.debug('QuoteReportingController.getSoldToList() is fired...');
        Map<Integer,String> soldtos = new Map<Integer,String>();
        String sold_to_s = null;
        Integer i = 0;
        if(isIgnored){
        	for(Account acct : [Select a.Account_Sold_To__c From Account a Where a.Account_Sold_To__c != NULL Limit 100]){
        	//soldtos.put(i++, acct.Account_Sold_To__c);
        	if(sold_to_s != null)
        		sold_to_s += ',' + acct.Account_Sold_To__c;
        	else
				sold_to_s = acct.Account_Sold_To__c;
			        		
        	}
        }else{
        	for(Account acct : [Select a.Account_Sold_To__c From Account a Where a.Account_Sold_To__c != NULL Limit 9900]){
        	//soldtos.put(i++, acct.Account_Sold_To__c);
        	if(sold_to_s != null)
        		sold_to_s += ',' + acct.Account_Sold_To__c;
        	else
				sold_to_s = acct.Account_Sold_To__c;
			        		
        	}
        }
        soldtos.put(i, sold_to_s);
        
        return soldtos.values();
    }
    
    public String invokeWM(){

        System.debug('QuoteReportingController.invokeWM() is fired...');
      try{
		
		AbQuotingUtils.verifyAuthentication();
        	
        QuoteServices.createQuote myQuote = new QuoteServices.createQuote();  //createQuote is an object name, not a method!
    
        //----------- Instantiate calling class; set Basic Auth header ---------    
        QuoteReportServices.ABI_SalesQuotation_process_loadCDIQuotesReportInSFDCWsd_Port stub = new QuoteReportServices.ABI_SalesQuotation_process_loadCDIQuotesReportInSFDCWsd_Port();

        String authString = AbQuotingUtils.getCurrentUserAuthString(); 
        stub.inputHttpHeaders_x = new Map<String,String>();   
        stub.inputHttpHeaders_x.put('Authorization', 'Basic '+authString);     
        stub.inputHttpHeaders_x.put('Cookie', AbQuotingUtils.getCookieFromSFDC());
        
        QuoteReportServices.cdiQuotesReport data = new QuoteReportServices.cdiQuotesReport();
        QuoteReportServices.ArrayOfstring soldTos = new QuoteReportServices.ArrayOfstring();
        soldTos.ArrayOfstringItem = soldToList;

        //Trim data
        if(filter_CDIQuote != null)
            filter_CDIQuote.trim();
        if(filter_SAPQuote != null)
            filter_SAPQuote.trim();
        if(filter_Product != null)
            filter_Product.trim();
        if(filter_ProductCategory != null)
            filter_ProductCategory.trim();
        if(filter_ProductLine != null)
            filter_ProductLine.trim();
        if(filter_PlanCode != null)
            filter_PlanCode.trim();
        if(filter_IGORItemClass != null)
            filter_IGORItemClass.trim();
        if(filter_IGORCode != null)
            filter_IGORCode.trim();
        if(filter_Template != null)
            filter_Template.trim();
        if(filter_QuoteStatusQLP != null)
            filter_QuoteStatusQLP.trim();
        
        if(filter_District != null)
            filter_District.trim();
        if(filter_salesRep != null)
            filter_salesRep.trim();
            
        //Set data
        data.quoteOwnerID     	=   UserInfo.getUserId();
        //data.sourceSystem     =   ;
        data.contactID          =   QuoteProxy.Contact__c;
        data.accountID          =   QuoteProxy.Account__c;
        data.soldToID           =   soldTos;
        //data.quoteTitle;
        data.CDIQuoteNumber     =   filter_CDIQuote;
        data.SAPQuoteNumber     =   filter_SAPQuote;
        data.startCreateDate    =   QuoteProxy.Range_Created_Start__c;
        data.endCreateDate      =   QuoteProxy.Range_Created_End__c;
        data.startExpDate       =   QuoteProxy.Range_Expired_Start__c;
        data.endExpDate         =   QuoteProxy.Range_Expired_End__c;
        data.productNumber      =   filter_Product;
        data.productCategory    =   filter_ProductCategory;
        data.productLine        =   filter_ProductLine;
        data.planCode           =   filter_PlanCode;
        data.igorItemClass      =   filter_IGORItemClass;
        data.igorCode           =   filter_IGORCode;
        data.templateID         =   filter_Template;
        data.status             =   filter_QuoteStatusQLP;
        data.Territory_District = 	filter_District; 
        data.SalesRep 			= 	filter_salesRep;
        data.Region             =   AbQuotingUtils.getUsersRegion();
         
        System.debug('SOAP Request:' + data);
        
        return stub.loadCDIQuotesReportInSFDC(data);

      } catch (QuotingAuthenticationException authExcep){

        String exceptionMessage = authExcep.getMessage();
        //System.debug('QUOTING: exception message=' + exceptionMessage);
        if(exceptionMessage.contains('External server did not return any content')){
          throw new QuotingAuthenticationException(AbQuotingUtils.AUTH_ERROR_MESSAGE);
        } else {
          throw authExcep;
        }
      
      } catch (Exception ex){
          //System.debug('QUOTING: Exception from web service call: ' + ex);
          throw new QuotingException('Exception from web service call: ' + ex);
      } 
        
      return null;
    }
    
    public PageReference loadData(){
        System.debug('QuoteReportingController.loadData() is fired...');
        
        //soldToList = QuoteReportUtils.getSoldToList();
        soldToList = getSoldToList();
        
        if(soldToList.size() == 0){
        	pref = Page.QuoteReportingNotFound;
        	pref.setRedirect(true);
        	return pref;
        }
        
        try {
            wmResponse = invokeWM();
            
            System.debug('SOAP Response: ' + wmResponse);
            if('true'.equalsIgnoreCase(wmResponse) || 'done'.equalsIgnoreCase(wmResponse)){
                pref = new PageReference(QuoteReportUtils.STANDARD_QUOTING_REPORT);
                pref.setRedirect(true);
        		return pref;
            }else if(Pattern.matches('No Data Found in CDI DB', wmResponse)){
            	pref = new PageReference(QuoteReportUtils.STANDARD_QUOTING_REPORT);
            	pref.setRedirect(true);
        		return pref;
            }
        }  catch (QuotingAuthenticationException authException) {
            //System.debug('QUOTING: auth exception running search ' + authException);
            PageReference loginPageRef = Page.QuoteReportLogin;
            loginPageRef.setRedirect(true);
            errorMessage = authException.getMessage();
            returnURLFromLogin = Page.QuoteReporting.getUrl();
            //System.debug('QUOTING: going to login page');
            return loginPageRef;
        } catch (Exception e){
            errorMessage = e.getMessage();
        }
        
        pref = Page.QuoteReportStatusProgress;
        pref.setRedirect(true);
        
        return pref;
    }
    
    public PageReference checkProgress(){
    	Integer loadStatus = QuoteReportUtils.checkProgress(UserInfo.getUserId()); 
    	if(loadStatus == 0 || loadStatus == -1){
    		pref = new PageReference(QuoteReportUtils.STANDARD_QUOTING_REPORT);
    		pref.setRedirect(true);
        	return pref;
    	}
    	
    	return null;
    }
    
    public String getLoadProgress(){
    	if(loadProgress == null){
    		loadProgress = 'Please wait...';
    	}else{
    		loadProgress += '.';
    	}
    	return loadProgress;
    }
    
    public void setLoadProgress(String progress){
    	this.loadProgress = progress;
    }
    
    public PageReference cancelAndRetry(){
    	PageReference pref = Page.QuoteReporting;
    	pref.setRedirect(true);
    	return pref;
    }
    
    public PageReference verifyAuthentication(){
    	PageReference pref = null;
    	try{
    		if(AbQuotingUtils.verifyAuthentication() ==  false){
    			pref = new PageReference('/apex/quotereportlogin');
    			pref.setRedirect(true);
    		}
    	}catch(Exception e){
			pref = new PageReference('/apex/quotereportlogin');
    		pref.setRedirect(true);
    	}
    	
		return pref;    	
    }
    
    //End of QuoteReportData__c and WebServices Section
}