/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Feb 28/2014
 **
 ** Class to process Account_Opportunity_Load_Record__c records
**/
public class GLFileLoadHelper {
    private Map<String,Account> accountMap;
    //private Map<String,Opportunity> opportunityMap;
    private Map<String,OpportunitySet> opportunityMap;
    private Map<String,Product2> productMap;
    private Map<String, List<OpportunityLineItem>> lineItemMap;
    private Map<String, RecordType> oppRecordTypeMap;
    private Map<String, RecordType> acctRecordTypeMap;
    private Map<String, User> userMap;
    
    public GLFileLoadHelper(){
        this.accountMap = new Map<String,Account>();
        this.opportunityMap = new Map<String,OpportunitySet>();
        this.productMap = new Map<String,Product2>();
        this.lineItemMap = new Map<String,List<OpportunityLineItem>>();
        this.oppRecordTypeMap = new Map<String,RecordType>();
        this.acctRecordTypeMap = new Map<String,RecordType>();
        for(RecordType rt : [select Id, DeveloperName, Name, SobjectType from RecordType where SobjectType = 'Opportunity' or SobjectType = 'Account']) {
            if(rt.SobjectType == 'Opportunity')
                oppRecordTypeMap.put(rt.DeveloperName, rt);
            else if(rt.SobjectType == 'Account')
                acctRecordTypeMap.put(rt.Name, rt);
        }
        userMap = new Map<String,User>();
        for(User usr : [select Id, Email from User where IsActive = true]) {
            userMap.put(usr.Email.toLowerCase(), usr);
        }
    }
    
    /**
     ** Process a list of records
    **/
    public void processRecords(List<Account_Opportunity_Load_Record__c> aolRecords) {
        Set<String> accountSoldToIds = new Set<String>();
        Set<String> contractNumbers = new Set<String>();
        Set<String> productCodes = new Set<String>();
        
        for(Account_Opportunity_Load_Record__c aolRecord : aolRecords) {
            if(!String.isBlank(aolRecord.Instance_Sold_To_Number__c) && !accountSoldToIds.contains(aolRecord.Instance_Sold_To_Number__c)) accountSoldToIds.add(aolRecord.Instance_Sold_To_Number__c);
            if(!String.isBlank(aolRecord.Contract_Number__c) && !contractNumbers.contains(aolRecord.Contract_Number__c)) contractNumbers.add(aolRecord.Contract_Number__c); 
            if(!String.isBlank(aolRecord.Contract_Name__c) && !productCodes.contains(aolRecord.Contract_Name__c)) productCodes.add(aolRecord.Contract_Name__c); 
        }
        
        for(Account account : [select Id, Account_Sold_To__c, Name from Account where Account_Sold_To__c in :accountSoldToIds]) {
            accountMap.put(account.Account_Sold_To__c, account);
        }

        for(Opportunity opp : [select Id, Contract_Number__c, AccountId, Contract_Expiry_Date__c, Pricebook2Id, (select PricebookEntry.Product2.ProductCode, Model__c, Serial_Number__c from OpportunityLineItems) 
            from Opportunity where Contract_Number__c in :contractNumbers]) {
            OpportunitySet oppSet = opportunityMap.containsKey(opp.Contract_Number__c) ? opportunityMap.get(opp.Contract_Number__c) : new OpportunitySet(opp.Contract_Number__c);            
            oppSet.addExistingOpportunity(opp);
            opportunityMap.put(opp.Contract_Number__c, oppSet);
        }

        for(Product2 product : [select Id, ProductCode, (select Id, UnitPrice, CurrencyIsoCode, Pricebook2Id from PricebookEntries where Pricebook2.IsStandard = false and Pricebook2.Name = 'Service Price Book') from Product2 where ProductCode in :productCodes]) {
            productMap.put(product.ProductCode, product);
        }

        for(Account_Opportunity_Load_Record__c aolRecord : aolRecords) {
            processRecord(aolRecord);
        }

        //associate the Opportunities to the Account
        Map<String,Account> accountUpserts = new Map<String,Account>();
        for(Account_Opportunity_Load_Record__c aolRecord : aolRecords) {
            if(aolRecord.Processing_Status__c != 'Error' && accountMap.containsKey(aolRecord.Instance_Sold_To_Number__c)) {
                accountUpserts.put(aolRecord.Instance_Sold_To_Number__c, accountMap.get(aolRecord.Instance_Sold_To_Number__c));
            }
        }
        
        upsert accountUpserts.values();
        
        //associate the Opportunities to the Account
        for(Account_Opportunity_Load_Record__c aolRecord : aolRecords) {
            if(aolRecord.Processing_Status__c != 'Error') {
                system.debug('aolRecord.Instance_Sold_To_Number__c='+aolRecord.Instance_Sold_To_Number__c);
                Account account = accountMap.get(aolRecord.Instance_Sold_To_Number__c);
                system.debug('account='+account);
                OpportunitySet oppSet = opportunityMap.get(aolRecord.Contract_Number__c);
                if(oppSet.newOpps != null) {
                    for(Opportunity opp : oppSet.newOpps) {
                        if(opp.AccountId == null) opp.AccountId = account.Id;
                    }
                }
            }
        }
        
        List<Opportunity> opportunityInserts = new List<Opportunity>();
        for(OpportunitySet oppSet : opportunityMap.values()) {
            opportunityInserts.addAll(oppSet.getOpportunityInserts());
        } 
        insert opportunityInserts;
        
        List<OpportunityLineItem> lineItemInserts = new List<OpportunityLineItem>(); 
        for(OpportunitySet oppSet : opportunityMap.values()) {
            lineItemInserts.addAll(oppSet.getLineItemInserts());
        } 
        insert lineItemInserts;

        /*
        Set<String> uniqueness = new Set<String>();
        List<OpportunityLineItem> lineItemUpserts = new List<OpportunityLineItem>(); 
        for(Account_Opportunity_Load_Record__c aolRecord : aolRecords) {
            if(aolRecord.Processing_Status__c != 'Error') {
                Opportunity opp = opportunityMap.get(aolRecord.Contract_Number__c);
                String key = getUniqueKey(aolRecord);
                if(lineItemMap.containsKey(key)) {
                    for(OpportunityLineItem oppLineItem : lineItemMap.get(key)) {
                        String uniquenesskey = opp.Id + ':' + oppLineItem.PricebookEntryId + ':'+oppLineItem.Quantity+':'+oppLineItem.UnitPrice+':'+oppLineItem.Model__c+':'+oppLineItem.Serial_Number__c;
                        if(!uniqueness.contains(uniquenesskey)) {
                            uniqueness.add(uniquenesskey);
                            oppLineItem.OpportunityId = opp.Id;
                            lineItemUpserts.add(oppLineItem);
                        }
                    }
                }
            }
            aolRecord.Processed_Date__c = DateTime.now();
        }
        for(OpportunityLineItem li : lineItemUpserts) {
            system.debug(li);
        }
        insert lineItemUpserts;
        */

        //update aolRecords;
    }
    
    /**
     ** Process a single record
    **/
    public void processRecord(Account_Opportunity_Load_Record__c aolRecord) {
        if(String.isBlank(aolRecord.Sold_To_Account_Name__c)) {
            aolRecord.Processing_Status__c = 'Error';
            aolRecord.Processing_Message__c = 'Sold To Account Name is blank';
            return;
        }

        if(String.isBlank(aolRecord.Instance_Sold_To_Number__c)) {
            aolRecord.Processing_Status__c = 'Error';
            aolRecord.Processing_Message__c = 'Instance Sold To Number is blank';
            return;
        }
        
        if(String.isBlank(aolRecord.Contract_Number__c)) {
            aolRecord.Processing_Status__c = 'Error';
            aolRecord.Processing_Message__c = 'Contract Number is blank';
            return;
        }

        if(aolRecord.Contract_Expiry_Date__c == null) {
            aolRecord.Processing_Status__c = 'Error';
            aolRecord.Processing_Message__c = 'Contract Expiry Date is blank';
            return;
        }

        Account account = null;
        Opportunity opportunity = null;
        List<String> processingMessages = new List<String>();

        if(!accountMap.containsKey(aolRecord.Instance_Sold_To_Number__c)) {
            //create account
//          aolRecord.Processing_Message__c += 'Account created';
            account = mergeToAccount(null, aolRecord);
            accountMap.put(aolRecord.Instance_Sold_To_Number__c, account);
            processingMessages.add('Account created');
        }else{
            account = accountMap.get(aolRecord.Instance_Sold_To_Number__c);
//          account = mergeToAccount(account, aolRecord);
//          aolRecord.Account__c = account.Id;
            processingMessages.add('Account found');
        }
        
        String productCode = aolRecord.Contract_Name__c;
        Decimal price = aolRecord.Value_Local_Currency__c;

        opportunity = findOpportunity(aolRecord);

        system.debug('opportunityMap:' + opportunityMap);

        if(opportunity == null) {
            OpportunitySet oppSet = (!opportunityMap.containsKey(aolRecord.Contract_Number__c)) ? new OpportunitySet(aolRecord.Contract_Number__c) : opportunityMap.get(aolRecord.Contract_Number__c);
            opportunityMap.put(aolRecord.Contract_Number__c, oppSet);
            opportunity = new Opportunity(AccountId = account.Id, RecordTypeId = oppRecordTypeMap.get('Service').Id);
            opportunity = mergeToOpportunity(opportunity, account, aolRecord);
            system.debug('oppSet:' + oppSet);
            
            //create line item
            Product2 prod = productMap.get(productCode);
            if(prod == null) {
                aolRecord.Processing_Status__c = 'Error';
                aolRecord.Processing_Message__c = 'Product ['+productCode+'] not found';
                return;
            }
            String currencyCode = aolRecord.CurrencyIsoCode;
            PricebookEntry pricebookEntry = null;
            system.debug('looking for ' + currencyCode + ' in ' + prod.PricebookEntries);
            for(PricebookEntry pbe : prod.PricebookEntries) {
                if(pbe.CurrencyIsoCode == currencyCode) {
                    pricebookEntry = pbe;
                } 
            }
            if(pricebookEntry == null) {
                aolRecord.Processing_Status__c = 'Error';
                aolRecord.Processing_Message__c = 'Pricebook Entry ['+productCode+','+currencyCode+'] not found';
                return;
            }

            opportunity.Pricebook2Id = pricebookEntry.Pricebook2Id;
            
            if(price == null){
                aolRecord.Processing_Status__c = 'Error';
                aolRecord.Processing_Message__c = 'Unit price not specified';
                return;
            }
            
            if(String.isBlank(aolRecord.Model_Number__c)){
                aolRecord.Processing_Status__c = 'Error';
                aolRecord.Processing_Message__c = 'Model number not specified';
                return;
            }

            if(String.isBlank(aolRecord.Serial_Number__c)){
                aolRecord.Processing_Status__c = 'Error';
                aolRecord.Processing_Message__c = 'Serial number not specified';
                return;
            }

            oppSet.addNewOpportunity(opportunity);
            processingMessages.add('Opportunity created');

            //String key = getUniqueKey(aolRecord);
            oppSet.addLineItemToNew(pricebookEntry.Id, aolRecord);
            /*
            OpportunityLineItem oppLineItem = createOpportunityLineItem(opportunity.Id, pricebookEntry.Id, aolRecord);
            if(lineItemMap.containsKey(key)) {
                //lineItemMap.get(key).add(oppLineItem);
            } else lineItemMap.put(key, new List<OpportunityLineItem>{oppLineItem});
            */
        }else{
            OpportunitySet oppSet = opportunityMap.get(aolRecord.Contract_Number__c);
            //opportunity = opportunityMap.get(aolRecord.Contract_Number__c);
            //opportunity = oppSet.findOpportunity(aolRecord);
            //opportunity = mergeToOpportunity(opportunity, aolRecord);
            processingMessages.add('Opportunity found');
            
            //check for pricebook, same product code - if found, skip; else create
            Product2 prod = productMap.get(productCode);
            if(prod == null) {
                //skip?
                aolRecord.Processing_Status__c = 'Error';
                aolRecord.Processing_Message__c = 'Product ['+productCode+'] not found';
                return;
            }else{
                String currencyCode = aolRecord.CurrencyIsoCode;
                PricebookEntry pricebookEntry = null;
                for(PricebookEntry pbe : prod.PricebookEntries) {
                    if(pbe.CurrencyIsoCode == currencyCode && (opportunity.Pricebook2Id == null || pbe.Pricebook2Id == opportunity.Pricebook2Id)) {
                        //TODO: check the pricebook is matching?
                        pricebookEntry = pbe;
                    } 
                }
                if(pricebookEntry == null) {
                    aolRecord.Processing_Status__c = 'Error';
                    aolRecord.Processing_Message__c = 'Pricebook Entry ['+productCode+','+currencyCode+'] not found';
                    return;
                }

                if(price == null){
                    aolRecord.Processing_Status__c = 'Error';
                    aolRecord.Processing_Message__c = 'Unit price not specified';
                    return;
                }

                if(String.isBlank(aolRecord.Model_Number__c)){
                    aolRecord.Processing_Status__c = 'Error';
                    aolRecord.Processing_Message__c = 'Model number not specified';
                    return;
                }

                if(String.isBlank(aolRecord.Serial_Number__c)){
                    aolRecord.Processing_Status__c = 'Error';
                    aolRecord.Processing_Message__c = 'Serial number not specified';
                    return;
                }

                if(opportunity.Pricebook2Id == null) {
                    opportunity.Pricebook2Id = pricebookEntry.Id;
                }

                if(opportunity.Id != null)
                    oppSet.addLineItemToExisting(opportunity, pricebookEntry.Id, aolRecord);
                else
                    oppSet.addLineItemToNew(pricebookEntry.Id, aolRecord);
                /*
                boolean found = false;
                for(OpportunityLineItem lineItem : opportunity.OpportunityLineItems) {
                    if(lineItem.PricebookEntryId == pricebookEntry.Id) found = true;        
                }
                
                if(!found) {
                    String key = getUniqueKey(aolRecord);
                    OpportunityLineItem oppLineItem = createOpportunityLineItem(opportunity.Id, pricebookEntry.Id, aolRecord);
                    if(lineItemMap.containsKey(key)) {
                        //lineItemMap.get(key).add(oppLineItem);
                    } else lineItemMap.put(key, new List<OpportunityLineItem>{oppLineItem});
                }else{
                    //skip
                }
                */
            }
        }
        
        aolRecord.Processing_Status__c = 'Processed';
        aolRecord.Processing_Message__c = String.join(processingMessages,';');
    }


    private String getDefaultOwnerEmail() {
        //get from custom setting
        GLFileLoadSettings__c settings = GLFileLoadSettings__c.getOrgDefaults();
        return !String.isBlank(settings.Default_Owner_Email__c) ? settings.Default_Owner_Email__c : UserInfo.getUserEmail();
    }

    private Id getDefaultAccountRecordTypeId() {
        //get from custom setting
        GLFileLoadSettings__c settings = GLFileLoadSettings__c.getOrgDefaults();
        String accountRecordTypeName = (settings != null && !String.isBlank(settings.Default_Account_Record_Type__c)) ? settings.Default_Account_Record_Type__c : null;
        Id recordTypeId = accountRecordTypeName != null ? acctRecordTypeMap.get(accountRecordTypeName).Id : null;
        return recordTypeId;
    }
/*
    private String getUniqueKey(Account_Opportunity_Load_Record__c aolRecord) {
        String serialNumber = aolRecord.Serial_Number__c != null ? aolRecord.Serial_Number__c.trim() : '';
        return aolRecord.Contract_Number__c.trim() + '-' + aolRecord.Contract_Name__c.trim() + '-' + aolRecord.Model_Number__c.trim() + '-' + serialNumber;
    }
    
    private OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId, Account_Opportunity_Load_Record__c aolRecord) {
        OpportunityLineItem oppLineItem = new OpportunityLineItem(OpportunityId = opportunityId, PricebookEntryId = pricebookEntryId, 
            Quantity = 1, UnitPrice = aolRecord.Value_Local_Currency__c, Model__c = aolRecord.Model_Number__c, Serial_Number__c = aolRecord.Serial_Number__c);
            //added July 27/2014
        if(aolRecord.Type__c != null) {
            String oppLineItemType = null;
            if(aolRecord.Type__c == 'WCONV') oppLineItemType = 'Warranty Conversion';
            else if(aolRecord.Type__c == 'CRNWL') oppLineItemType = 'Contract Renewal';
            oppLineItem.Type__c = oppLineItemType;
        }
        return oppLineItem;
    }
*/

    private Account mergeToAccount(Account account, Account_Opportunity_Load_Record__c aolRecord){
        Id ownerId = null;
        if(!String.isBlank(aolRecord.Z1_Customer_Name__c) && userMap.containsKey(aolRecord.Z1_Customer_Name__c.toLowerCase())) {
            ownerId = userMap.get(aolRecord.Z1_Customer_Name__c.toLowerCase()).Id;
        }else if(!String.isBlank(getDefaultOwnerEmail()) && userMap.containsKey(getDefaultOwnerEmail().toLowerCase())) {
            ownerId = userMap.get(getDefaultOwnerEmail().toLowerCase()).Id;
        }else {
            ownerId = UserInfo.getUserId();
        }
        if(account == null) account = new Account(Name = aolRecord.Sold_To_Account_Name__c, Created_By_GL_Load_Script__c = true, RecordTypeId = getDefaultAccountRecordTypeId());
        account.OwnerId = ownerId;
        account.Account_Sold_To__c = aolRecord.Instance_Sold_To_Number__c;
        account.BillingCountry = aolRecord.Country_Name__c;
        account.BillingCity = aolRecord.City__c;
        account.BillingState = aolRecord.State__c;
        account.BillingPostalCode = aolRecord.Zip_Code__c;
        account.Site = aolRecord.City__c;
        return account;
    }

    private Opportunity mergeToOpportunity(Opportunity opportunity, Account acct, Account_Opportunity_Load_Record__c aolRecord){
        //opportunity.Name = aolRecord.Contract_Number__c;
        String oppName = aolRecord.Line_Item_End__c.year() + String.valueOf(aolRecord.Line_Item_End__c.month()).leftPad(2).replace(' ','0') + 
            String.valueOf(aolRecord.Line_Item_End__c.day()).leftPad(2).replace(' ','0');
        //String type = 'WCONV'; //CRNWL
        //String type = aolRecord.Type__c;
        String type = getTypeForOppName(aolRecord.Type__c);
        oppName += '_' + type + '_' + acct.Name;
        opportunity.Name = oppName;
        opportunity.CurrencyIsoCode = aolRecord.CurrencyIsoCode;
        opportunity.Contract_Number__c = aolRecord.Contract_Number__c;
        opportunity.Contract_Expiry_Date__c = aolRecord.Contract_Expiry_Date__c;
        opportunity.Explicit_Needs__c = '?';
        opportunity.Implications__c = '?';
        opportunity.CloseDate = aolRecord.Line_Item_End__c;
        opportunity.StageName = 'Recognition of Needs';
        Id ownerId = null;
        if(!String.isBlank(aolRecord.Z1_Customer_Name__c) && userMap.containsKey(aolRecord.Z1_Customer_Name__c.toLowerCase())) {
            ownerId = userMap.get(aolRecord.Z1_Customer_Name__c.toLowerCase()).Id;
        }else if(!String.isBlank(getDefaultOwnerEmail()) && userMap.containsKey(getDefaultOwnerEmail().toLowerCase())) {
            ownerId = userMap.get(getDefaultOwnerEmail().toLowerCase()).Id;
        }else {
            ownerId = UserInfo.getUserId();
        }
        opportunity.OwnerId = ownerId;
        //June 30/2015 change
        opportunity.FLIP_Engineer__c = aolRecord.FLIP_Engineer__c;
        return opportunity;
    }

    private Opportunity findOpportunity(Account_Opportunity_Load_Record__c aolRecord) {
        if(aolRecord.Contract_Number__c != null && opportunityMap.containsKey(aolRecord.Contract_Number__c)) {
            return opportunityMap.get(aolRecord.Contract_Number__c).findOpportunity(aolRecord);
        }
        return null;
    }

    class OpportunitySet {
        private String contractNumber {get;set;}
        private List<Opportunity> opps {get;set;}
        private List<Opportunity> newOpps {get;set;}
        private List<OpportunityLineItem> newLineItems {get;set;}
        private Map<String,List<OpportunityLineItem>> newLineItemMap {get;set;}

        public OpportunitySet(String contractNumber) {
            this.contractNumber = contractNumber;
        }

        public void addExistingOpportunity(Opportunity opp) {
            if(this.opps == null) this.opps = new List<Opportunity>();
            this.opps.add(opp);
        }

        public Opportunity findOpportunity(Account_Opportunity_Load_Record__c aolRecord) {
            if(this.opps != null) {
                for(Opportunity opp : this.opps) {
                    if(opp.Contract_Expiry_Date__c == aolRecord.Contract_Expiry_Date__c) return opp;
                }
            }
            if(this.newOpps != null) {
                for(Opportunity opp : this.newOpps) {
                    String oppKey = getUniqueOppKey(opp);
                    String aolOppKey = getUniqueOppKey(aolRecord);
                    if(oppKey == aolOppKey) return opp;
                }
            }

            return null;
        }

        public void addNewOpportunity(Opportunity opp) {
            if(this.newOpps == null) this.newOpps = new List<Opportunity>();
            this.newOpps.add(opp);
        }

        public void addLineItemToExisting(Opportunity opp, Id pricebookEntryId, Account_Opportunity_Load_Record__c aolRecord) {
            system.debug('addLineItemToExisting');
            system.debug('opp='+opp);
            system.debug('pricebookEntryId='+pricebookEntryId);
            system.debug('aolRecord='+aolRecord);
            boolean found = false;
            for(OpportunityLineItem lineItem : opp.OpportunityLineItems) {
                if(lineItem.PricebookEntryId == pricebookEntryId && lineItem.Serial_Number__c == aolRecord.Serial_Number__c) found = true;        
            }
            system.debug('found='+found);
            if(!found) {
                OpportunityLineItem lineItem = createOpportunityLineItem(opp.Id, pricebookEntryId, aolRecord);
                if(newLineItems == null) newLineItems = new List<OpportunityLineItem>();
                newLineItems.add(lineItem);
            }
        }

        private OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId, Account_Opportunity_Load_Record__c aolRecord) {
            OpportunityLineItem oppLineItem = new OpportunityLineItem(OpportunityId = opportunityId, PricebookEntryId = pricebookEntryId, 
                Quantity = 1, UnitPrice = aolRecord.Value_Local_Currency__c, Model__c = aolRecord.Model_Number__c, Serial_Number__c = aolRecord.Serial_Number__c);
                //added July 27/2014
            if(aolRecord.Type__c != null) {
                //String oppLineItemType = null;
                //if(aolRecord.Type__c == 'WCONV') oppLineItemType = 'Warranty Conversion';
                //else if(aolRecord.Type__c == 'CRNWL') oppLineItemType = 'Contract Renewal';
                //oppLineItem.Type__c = oppLineItemType;
                oppLineItem.Type__c = getTypeForLineItem(aolRecord.Type__c);
            }
            return oppLineItem;
        }

        public void addLineItemToNew(Id pricebookEntryId, Account_Opportunity_Load_Record__c aolRecord) {
            system.debug('addLineItemToNew');
            system.debug('pricebookEntryId='+pricebookEntryId);
            system.debug('aolRecord='+aolRecord);
            OpportunityLineItem lineItem = createOpportunityLineItem(null, pricebookEntryId, aolRecord);
            if(newLineItemMap == null) newLineItemMap = new Map<String,List<OpportunityLineItem>>();
            String lineItemKey = getUniqueLineItemKey(aolRecord);
            system.debug('addLineItemToNew: lineItemKey='+lineItemKey);
            system.debug('newLineItemMap.get(lineItemKey)='+newLineItemMap.get(lineItemKey));
            if(newLineItemMap.get(lineItemKey) == null) newLineItemMap.put(lineItemKey, new List<OpportunityLineItem>{ lineItem });
            else newLineItemMap.get(lineItemKey).add(lineItem);
            system.debug('newLineItemMap: '+newLineItemMap);
        }

        private String getUniqueLineItemKey(Account_Opportunity_Load_Record__c aolRecord){
            String serialNumber = aolRecord.Serial_Number__c != null ? aolRecord.Serial_Number__c.trim() : '';
            String contractExpiryDate = DateTime.newInstance(aolRecord.Contract_Expiry_Date__c.year(), aolRecord.Contract_Expiry_Date__c.month(), aolRecord.Contract_Expiry_Date__c.day()).format('yyyyMMdd');
            return aolRecord.Contract_Number__c.trim() + '-' + contractExpiryDate + '-' + aolRecord.Contract_Name__c.trim() + '-' + aolRecord.Model_Number__c.trim() + '-' + serialNumber;
        }

        private String getUniqueOppKey(Opportunity opp){
            String contractExpiryDate = DateTime.newInstance(opp.Contract_Expiry_Date__c.year(), opp.Contract_Expiry_Date__c.month(), opp.Contract_Expiry_Date__c.day()).format('yyyyMMdd');
            return opp.Contract_Number__c.trim() + '-' + contractExpiryDate;
        }

        private String getUniqueOppKey(Account_Opportunity_Load_Record__c aolRecord){
            String contractExpiryDate = DateTime.newInstance(aolRecord.Contract_Expiry_Date__c.year(), aolRecord.Contract_Expiry_Date__c.month(), aolRecord.Contract_Expiry_Date__c.day()).format('yyyyMMdd');
            return aolRecord.Contract_Number__c.trim() + '-' + contractExpiryDate;
        }

        public List<OpportunityLineItem> getLineItemInserts(){
            List<OpportunityLineItem> inserts = new List<OpportunityLineItem>();
            if(newLineItems != null) inserts.addAll(newLineItems);
            
            //match map to newly created opps
            Map<String,Opportunity> newOppsMap = new Map<String,Opportunity>();
            if(newOpps != null) {
                for(Opportunity opp : newOpps) {
                    if(opp.Id != null) {
                        newOppsMap.put(getUniqueOppKey(opp), opp);
                    }
                }
            }

            if(newLineItemMap != null) {
                for(String lineItemKey : newLineItemMap.keySet()) {
                    system.debug('lineItemKey='+lineItemKey);
                    String[] toks = lineItemKey.split('-');
                    if(toks.size() >= 2) {
                        String oppKey = toks[0] + '-' + toks[1];
                        system.debug('oppKey='+oppKey);
                        if(newOppsMap.containsKey(oppKey)) {
                            Opportunity opp = newOppsMap.get(oppKey);
                            for(OpportunityLineItem lineItem : newLineItemMap.get(lineItemKey)) {
                                lineItem.OpportunityId = opp.Id;
                                system.debug('adding to line item insert list');
                                inserts.add(lineItem);
                            }
                        }
                    }
                }
            }
            return inserts;
        }

        public List<Opportunity> getOpportunityInserts(){
            return newOpps != null ? newOpps : new List<Opportunity>();
        }

    }
    //new Type handling, June 30/2015
    private static final Set<String> AFTER_MARKET_SUBTYPES = new Set<String>{
        'Accessories and Consumables', 'Compliance', 'Parts and Upgrades', 'Professional Services and Workflow Support',
        'Software', 'Training', 'Computer Manuals'
    };
    private static String getTypeForLineItem(String typeFieldValue)
    {
        if(typeFieldValue == 'WCONV') return 'Warranty Conversion';
        else if(typeFieldValue == 'CRNWL') return 'Contract Renewal';
        else if(AFTER_MARKET_SUBTYPES.contains(typeFieldValue)) return typeFieldValue;
        else return null;
    }

    private static String getTypeForOppName(String typeFieldValue)
    {
        if(typeFieldValue == 'WCONV' || typeFieldValue == 'CRNWL') return typeFieldValue;
        else if(AFTER_MARKET_SUBTYPES.contains(typeFieldValue)) return 'AFMKT';
        else return '';
    }
}