global class QuotingWebServices {
  static final Integer FAILURE = -1;
  static final Integer SUCCESS = 0;
  
  global class UserResult {
    webService Integer returnCode     = SUCCESS;
    webService String  message        = '';
    webService String  exceptionType  = '';
    //webService List<User> managerUsers;
  }
  
  global class AttachmentResult {
    webService Integer returnCode     = SUCCESS;
    webService String  message        = '';
    webService String  exceptionType  = '';
    webService QuoteAttachment[] attachments;
  }
  
  global class QuoteAttachment {
    webService String id = '';
    webService String name = '';
    webService Integer bodyLength = 0;
    webService Blob content;
  }
  
  
  webservice static UserResult retrieveManagersForUser(String userIdentifier){
    System.debug('>>>>> INTO web service.  Input parameters:' + userIdentifier );
    UserResult result = new UserResult();
    result.message = 'TESTING A SIMPLE WEB SERVICE';
    System.debug('<<<< RETURN from web service with value:' + result );
    return result;
  }

  webservice static AttachmentResult retrieveAttachment(String[] attachmentIdList){
    System.debug('>>>>> INTO web service.  Input parameters:' + attachmentIdList );
    AttachmentResult result = new AttachmentResult();

    try{
      //Find the parent role id and return all users that are assigned to that role
      Attachment[] attchmnts = [SELECT a.Body, a.BodyLength, a.Id, a.Name FROM Attachment a  WHERE a.Id in :attachmentIdList];
      List<QuoteAttachment> quoteAttchList = new List<QuoteAttachment>();
      for (Attachment nextAttch : attchmnts){
        QuoteAttachment newQuoteAttachment = new QuoteAttachment();
        newQuoteAttachment.id = nextAttch.Id;
        newQuoteAttachment.name = nextAttch.Name;
        newQuoteAttachment.bodyLength = nextAttch.BodyLength;
        newQuoteAttachment.content = nextAttch.Body;
        quoteAttchList.add(newQuoteAttachment);
                
      }
      result.attachments = quoteAttchList;
      return result;
    } catch (Exception ex){
      result.returnCode = FAILURE;
      result.exceptionType = ex.getTypeName();
      result.message = ex.getMessage();
    }
    
    System.debug('<<<< RETURN from web service with value:' + result );
    return result;
  }
  
  public static testmethod void testcase(){
    QuotingWebServices quws=new QuotingWebServices();
    retrieveManagersForUser('UserIdentify');
    List<ID> attachListId = new List<ID>();
    for(Attachment attach:[SELECT Id FROM Attachment limit 2])
     attachListId.add(attach.Id);
    retrieveAttachment(attachListId);
    retrieveAttachment(null); 
  }
}