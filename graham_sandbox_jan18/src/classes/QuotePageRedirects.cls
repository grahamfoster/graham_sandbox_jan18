public class QuotePageRedirects {
    	
  //==========================================================  
  //================ Page Redirects ==========================
  //==========================================================  
  public static PageReference LandingPage() {    
    return Page.QuoteLandingPage;
  }
  
  //may combine these 3
  public static PageReference NewHeader() {    
    return Page.QuoteNewHeader;
  }
  public static PageReference NewPartner() {    
    PageReference nextPage = Page.QuoteNewPartner;
    nextPage.setRedirect(true);
    return Page.QuoteNewPartner;
  }
  public static PageReference NewOptions() {    
    PageReference nextPage = Page.QuoteNewOptions;
    nextPage.setRedirect(true);
    return Page.QuoteNewOptions;
  }
  
  public static PageReference ProductSelect() {    
    return Page.QuoteProductSelect;
  }
  public static PageReference productDiscount() {    
    return Page.QuoteDiscount;
  }
  public static PageReference goToProductDiscountZAUABD() {    
    return Page.QuoteDiscountZAUABD;
  }
  public static PageReference goToProductDiscountZDF1() {    
    return Page.QuoteDiscountZDF1;
  }
  public static PageReference ProductSort() {    
    return Page.QuoteProductSort;
  }
  public static PageReference OptionSort() {    
    return Page.QuoteOptionSort;
  }
  public static PageReference ProductSearch() {    
    return Page.QuoteProductSearch;
  }
  public static PageReference goToProductHierchy() {    
    return Page.QuoteHierarchySearch;
  }
  public static PageReference TemplateSearch() {    
    return Page.QuoteTemplateSearch;
  }
  public static PageReference Materials() {    
    return Page.QuoteMaterials;
  }
  public static PageReference Summary() {    
    return Page.QuoteSummary;
  }
  public static PageReference ApprovalLog() {    
    return Page.QuoteApprovalLog;
  }
  public static PageReference SessionLog() {    
    return Page.QuoteSessionLog;
  }
  public static PageReference ProductDetail() {    
    return Page.QuoteProductDetail;
  }
}