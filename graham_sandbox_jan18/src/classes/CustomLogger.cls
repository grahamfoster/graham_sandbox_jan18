public virtual class CustomLogger {

/*   Logger  - Utility for generating .CSV log Files
 * 
 */    
    public class Logger{
        Public String header  = 'Object,ObjectId,Parent Container,Action,Message'; 
        Public String process {get;set;}
        Public String subject {get;set;}
        Public String sendTo {get;set;}
        Public String loglevel {get;set;}
        Public String logfile {get;set;}
 		Public String msgBody {get;set;}        
		Public String attachToId {get;set;}
        Public Custom_Log__c logMsg= new Custom_Log__c();
        
        List<Custom_Log__c> Logs = new List<Custom_Log__c>();
        
        public void addLine(String Status, String obj, String objId, String container, String Action, String msg){
            Custom_Log__c line = new Custom_Log__c();
            line.Process__c = process;
            line.Result__c = status;
            line.Object__c = obj;
            line.ObjectId__c = objId;
            line.Container__c  = container;
            line.Action__c = Action;
            line.Message__c = msg;
            line.TimeStamp__c = Datetime.now();
            Logs.add(line);
        }

        public void SaveLog(){            
			List<Custom_Log__c> postLogs = new List<Custom_Log__c>();             
            for(Custom_Log__c l :Logs){                              
                if(l.Result__c == 'Info' && loglevel == 'Info'){
                   	postLogs.add(l);
                } else if(l.Result__c == 'Success' && (loglevel == 'Success' || loglevel == 'Info')){
                    postLogs.add(l);
                } else if (l.Result__c == 'Warning' && (loglevel == 'Success' || loglevel == 'Warning' || loglevel == 'Info')) {
                    postLogs.add(l);
                } else if (l.Result__c == 'Error') {
                    postLogs.add(l);
                }
            }
            if(postLogs.size()>0){
                insert postLogs;                
            }
    	}
        
        public void SendLog(String style){          
        	String Log ='';
            Log += 'RESULT,' + header + ', TimeStamp \n';
            for(Custom_Log__c l :Logs){
                if(l.Result__c == 'Info' && loglevel == 'Info'){
                    Log += l.Result__c + ',' + l.Object__c + ',' + l.ObjectId__c + ',' + l.Container__c + ',' + l.Action__c + ',' + l.Message__c +',' + l.TimeStamp__c + '\n';
                } else if(l.Result__c == 'Success' && (loglevel == 'Success' || loglevel == 'Info')){
                    Log += l.Result__c + ',' + l.Object__c + ',' + l.ObjectId__c + ',' + l.Container__c + ',' + l.Action__c + ',' + l.Message__c +',' + l.TimeStamp__c + '\n';
                } else if (l.Result__c == 'Warning' && (loglevel == 'Success' || loglevel == 'Warning' || loglevel == 'Info')) {
                    Log += l.Result__c + ',' + l.Object__c + ',' + l.ObjectId__c + ',' + l.Container__c + ',' + l.Action__c + ',' + l.Message__c +',' + l.TimeStamp__c + '\n';
                } else if (l.Result__c == 'Error') {
                    Log += l.Result__c + ',' + l.Object__c + ',' + l.ObjectId__c + ',' + l.Container__c + ',' + l.Action__c + ',' + l.Message__c +',' + l.TimeStamp__c + '\n';
                }
            }
			Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
			blob csvBlob = Blob.valueOf(Log);
			csvAttc.setFileName(logfile);
  		    csvAttc.setBody(csvBlob);
            
            if(style.toLowerCase() == 'email'){
				Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
				String[] toAddresses = new list<string> {sendTo};
				email.setSubject(subject);
				email.setToAddresses( toAddresses );
				email.setPlainTextBody(MsgBody);
				email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                try {
					Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});                    
                } catch (Exception e) {
                    //  
                }

            } else if(style.toLowerCase() == 'file'){
				Attachment attach = new Attachment(); 
    			attach.Name =logfile;  
    			attach.Body = csvBlob;
    			attach.ContentType= 'application/csv'; //Signal what the file's MIME type is
    			attach.ParentID = attachToId;
    			insert attach;               
            } else {
            	system.debug(Log);                
            }
         
        }
    }
}