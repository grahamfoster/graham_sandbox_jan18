public with sharing class feedbackpage_ctrl {

    public List<Battlecard__c> battleObjList{get;set;}
    public set<id> battleObjListIDs{get;set;}
    public set<id> packageObjListIDs{get;set;}
    public List<Rating_Management__c> ratingManageList{get;set;}
    public List<ratingbattlecardwrapper> ratingbattlecardwrapperList{get;set;}
    public Ratingbattlecardwrapper obj{get;set;}
    private String sortDirection = 'ASC';
    private String sortExp = '';

    public feedbackpage_ctrl()
    {
        battleObjList = [
            SELECT Battlecard_name__c, Package__c
            FROM Battlecard__c
            WHERE Package__r.isActive__c = true AND
                Package__r.Product__r.Product_Category__c !=  null AND
                Package__r.Product__c !=  null
        ];

        if (!battleObjList.isEmpty()) {
            packageObjListIDs = new set<id>();
            for(Battlecard__c battlecard: battleObjList){
                packageObjListIDs.add(battlecard.Package__c);
            }
        }

        if (packageObjListIDs != null) {
            ratingManageList = [
                SELECT CP__c, Demo__c, PP__c, Summary__c, Additional_Resource_Rating__c,
                    Comments__c, User__r.Name, User__r.Id, User__r.email, Package__c, LastModifiedDate
                FROM Rating_Management__c
                WHERE Package__c in : packageObjListIDs AND Archive__c = false
            ];
        }

        if (ratingManageList != null) {
            ratingbattlecardwrapperList =new list<Ratingbattlecardwrapper>();

            for (Rating_Management__c ratManag : ratingManageList) {
                obj = new Ratingbattlecardwrapper();
                obj.ratingManageId = ratManag.Id;
                obj.selected = false;
                obj.cp = ratManag.CP__c;
                obj.userName = ratManag.User__r.Name;
                obj.eMail = ratManag.User__r.email;
                obj.pp = ratManag.PP__c;
                obj.demo = ratManag.Demo__c;
                obj.sum = ratManag.Summary__c;
                obj.res  = ratManag.Additional_Resource_Rating__c;
                obj.comment = ratManag.Comments__c;
                obj.packageid  = ratManag.Package__c;
                string fixingdate = ratManag.LastModifiedDate.format('MM/dd/yyyy');
                obj.dated = fixingdate;

                for(Battlecard__c battle :battleObjList) {
                    if (battle.Package__c == ratManag.Package__c)
                    {
                        obj.battlename = battle.Battlecard_Name__c;
                    }
                }
                ratingbattlecardwrapperList.add(obj);
            }

            Ratingbattlecardwrapper.SortField = sortedField;
            Ratingbattlecardwrapper.SortDirection = sortDirection;
            ratingbattlecardwrapperList.sort();
        }
    }

    public void DeleteAllFeedbacks()
    {
        if (packageObjListIDs != null) {
            List<Rating_Management__c> ratingList = [
                SELECT CP__c, Demo__c, PP__c, Summary__c, Additional_Resource_Rating__c,
                    Comments__c, User__r.Name, User__r.Id, User__r.email, Package__c
                FROM Rating_Management__c
                WHERE Package__c in : packageObjListIDs
            ];

            for (Integer i = 0; i < ratingList.size(); i++)
            {
                Rating_Management__c c = ratingList[i];
                delete c;
            }
        }
    }

    public PageReference doArchive()
    {
        set<string> ratingUpdate = new set<string>();
        List<Rating_Management__c> ratingUpdatelist = new List<Rating_Management__c>();
        for(ratingbattlecardwrapper ratingbattle : ratingbattlecardwrapperList){
            if(ratingbattle.selected == true) {
                ratingUpdate.add(ratingbattle.ratingManageId);
            }
        }
        if(ratingUpdate != null) {
            ratingUpdatelist = [SELECT Archive__c from Rating_Management__c where id in :ratingUpdate];
        }
        if(ratingUpdatelist != null) {
            for(Rating_Management__c rat:ratingUpdatelist) {
                rat.Archive__c = true;
            }
            try{
                update ratingUpdatelist;
                PageReference pageRef = new PageReference('/apex/mainpageadmin?tabfocus=FeedbackTab');
                return pageRef;
            }
            catch(Exception Ex){System.debug('error in update:'+ex.getMessage());}
        }
        return null;
    }

    public String sortedField
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';

            sortExp = value;
        }
    }

    public String getSortDirection()
    {
        //if not column is selected
        if (sortedField == null || sortedField == '')
            return 'ASC';
        else
            return sortDirection;
    }

    public void setSortDirection(String value)
    {
        sortDirection = value;
    }

    public void sortThisColumn()
    {
        //build the full sort expression
        Ratingbattlecardwrapper.SortField = sortedField;
        Ratingbattlecardwrapper.SortDirection = sortDirection;
        ratingbattlecardwrapperList.sort();
    }
}