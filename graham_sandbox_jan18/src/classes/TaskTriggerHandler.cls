/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Aug 5/2013
 **
 ** This helper class modifies the owner of the Task as it is created by Eloqua
 ** The task should be updated to the owner of the Lead or Contact
**/
public class TaskTriggerHandler {
	public static final String ContactPrefix = Contact.SObjectType.getDescribe().getKeyPrefix();
	public static final String LeadPrefix = Lead.SObjectType.getDescribe().getKeyPrefix();
	private static final String ELOQUA_USER_NAME = 'Eloqua'; //the Name of the Eloqua user record, used to locate that user record's Id to identify the task as coming from Eloqua
	public static Id EloquaOwnerId = null;
	
	static {
		User elqUser = findEloquaUser();
		EloquaOwnerId = elqUser != null ? elqUser.Id : null;
	}
	
	public static User findEloquaUser(){
		User[] elqUsers = [select Id from User where Name = :ELOQUA_USER_NAME limit 1];
		return (elqUsers.size() > 0) ? elqUsers[0] : null;
	}
	
	/**
	 ** This method will trigger an update on the Contact and/or Lead and the Task(s)
	 ** Set Task.OwnerId of the Task to the OwnerId of the Contact/Lead
	 ** Set Contact/Lead.Eloqua_Time_Stamp__c to current time
	 ** Setting Eloqua_Time_Stamp__c will cause a workflow rule to trigger an email alert to the rep (owner of the Task) via Workflow Rule/Email Alert/Email Template config
	**/
	public static void onBeforeInsert(Task[] tasks) {
		Set<Id> leadIds = new Set<Id>();
		Set<Id> contactIds = new Set<Id>();
		
		for(Task t : tasks) {
			if(t.WhoId != null){
				if(String.valueOf(t.WhoId).indexOf(ContactPrefix) == 0) {
					contactIds.add(t.WhoId);
				}else if(String.valueOf(t.WhoId).indexOf(LeadPrefix) == 0) {
					leadIds.add(t.WhoId);
				}
			}
		}

		//load the Leads and Contacts into a map for reference to get the OwnerId		
		List<EloquaCampaign__c> elqCampaigns = EloquaCampaign__c.getAll().values(); 
		Map<Id, Lead> leadsMap = (leadIds.size() > 0) ? new Map<Id,Lead>([select Id, OwnerId from Lead where id in :leadIds]) : new Map<Id,Lead>();
		Map<Id, Contact> contactsMap = (contactIds.size() > 0) ? new Map<Id,Contact>([select Id, OwnerId from Contact where id in :contactIds]) : new Map<Id,Contact>();
		
		Map<Id,Lead> leadUpdates = new Map<Id,Lead>();
		Map<Id,Contact> contactUpdates = new Map<Id,Contact>();
		for(Task t : tasks) {
			//only the ELQ tasks should trigger an update
			if(t.WhoId != null && EloquaOwnerId != null && t.OwnerId == EloquaOwnerId){
				//check the task subject matches a campaign in one of the EloquaCampaign__c custom settings
				boolean foundCampaign = false;
				for(EloquaCampaign__c elqCampaign : elqCampaigns) {
					if(!String.isBlank(elqCampaign.Task_Subject_Key__c) && !String.isBlank(t.Subject) && t.Subject.indexOf(elqCampaign.Task_Subject_Key__c) >= 0) {
						foundCampaign = true;
						break;
					}
				}
				if(foundCampaign) {
					String whoIdStr = String.valueOf(t.WhoId);
					if(whoIdStr.indexOf(ContactPrefix) == 0) {
						if(contactsMap.containsKey(t.WhoId)) {
							//update the Task owner to the Contact.OwnerId
							t.OwnerId = contactsMap.get(t.WhoId).OwnerId;
							contactUpdates.put(t.WhoId, new Contact(Id = t.WhoId, Eloqua_Task_Stamp__c = DateTime.now()));
						}
					}else if(whoIdStr.indexOf(LeadPrefix) == 0) {
						if(leadsMap.containsKey(t.WhoId)) {
							//update the Task owner to the Lead.OwnerId
							t.OwnerId = leadsMap.get(t.WhoId).OwnerId;
							leadUpdates.put(t.WhoId, new Lead(Id = t.WhoId, Eloqua_Task_Stamp__c = DateTime.now()));
						}
					}
				}
			}
		}
		
		//update the Lead/Contact records timestamp to trigger the workflow rule email notification
		if(leadUpdates.size() > 0) update leadUpdates.values();
		if(contactUpdates.size() > 0) update contactUpdates.values();
	}
}