public with sharing class BattleCardRelatedProductList {

   List<Product_Category__c> prcat {get; set;}
   List<Product2> pro {get; set;}
   Set<Id> prcId = new Set<Id>();
   public string procId {get; set;}
   public map<String,list<ProdWrapper>> productMap{get;set;}
   // the values of the selected items
   public string selectedLevel1 {get; set;}
   public boolean clickedCell {get; set;}
   public string ProductName {get; set;}
   public string ProductImage {get; set;}
   public string ProductId {get; set;}
   public String errormsg{get;set;}
    
   public BattleCardRelatedProductList(){
        procId = ApexPages.currentPage().getParameters().get('proCatId');
        prcat = [select Id, Name, Product_Category_Name__c from Product_Category__c Where Id =: procId and isActive__c = true];
        for(Product_Category__c pr: prcat){
            prcId.add(pr.Id);
        }
        
        pro = [Select Id, Name, Product_Category__c, Image__c, Competitor_Manufacturer__c From Product2 Where isActive =: true AND Product_Category__c In: prcId AND Competitor_Manufacturer__c != ''];
        //Set<Id> mfrId = new Set<Id>();
        Set<string> mfrname = new Set<string>();
        for(Product2 pr : pro){
            mfrname.add(pr.Competitor_Manufacturer__c);
        }
        //List<Manufacturer__c> mfr = [Select Id, Name, Manufacturer_Name__c  From Manufacturer__c Where Name !=: 'AB Sciex' AND isActive__c = true and Id IN: mfrId];
        productMap = new map<String,list<ProdWrapper>>();
           if(mfrname != null && mfrname.size() > 0)
           {
                for(String mf: mfrname){
                    list<ProdWrapper> prodList = new list<ProdWrapper>();
                    for(Product2 prod : pro){ 
                       if(mf == prod.Competitor_Manufacturer__c ){
                           prodWrapper proWrap = new prodWrapper();
                           proWrap.ManufacturerName = mf; 
                           proWrap.ProductName = prod.Name;
                           proWrap.ProductImage = prod.Image__c;
                           //proWrap.ManufacturerId = mf.Id;
                           proWrap.productId = prod.Id;
                           prodList.add(prowrap);
                       }
                    }
                    // productMap.put(mf.Manufacturer_Name__c,prodList); 
                    productMap.put(mf,prodList);
                    errormsg='Select Competitor Model';
                }
           }
           else{
                 errormsg='Competitor model is not available for selected AB Sciex Model.';
           }
   }
   
   public List<selectOption> level1Items {
        get {
            List<selectOption> options = new List<selectOption>();
 
                //options.add(new SelectOption('','-- Choose a Category --'));
                for (Product_Category__c pcat : [select Id, Name, Product_Category_Name__c from Product_Category__c Order By Product_Category_Name__c])
                    options.add(new SelectOption(pcat.Id,pcat.Product_Category_Name__c));
 
            return options;           
        }
        set;
   }
   
   /*public List<Product_Category__c> getProdCat(){
       
       prcat = [select Id, Name, Product_Category_Name__c from Product_Category__c Order By Product_Category_Name__c];
       return prcat;
   
   }*/
   
   public List<selectOption> getlevel2Items (){
        //get{
            List<selectOption> options = new List<selectOption>();
 
                //options.add(new SelectOption('','-- Choose a Category --'));
                for (Product2 prod : [select Id, Name from Product2 Order By Name])
                    options.add(new SelectOption(prod.Id,prod.Name));
 
            return options;           
         //}
         //set;   
   }
   
   public void parentpage(){
      clickedcell = true;
      System.debug(procId); 
      System.debug(clickedCell);
      system.debug(productName);
      //return null; 
      //pagereference pageref = new pagereference('/apex/absciexProductList?id='+procId);
      //pageref.setredirect(true);
      //return pageref;
         
   }
   
    public class ProdWrapper {
        public String ManufacturerName{get;set;}
        public String ProductName{get;set;}
        public String ProductImage{get;set;}
        //public Id ManufacturerId{get;set;}
        public Id productId{get;set;}
    }
   
}