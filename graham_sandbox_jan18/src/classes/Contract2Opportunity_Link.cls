/**
 * @author Brett Moore
 * @created - July 2016
 * @Revision  
 * @Last Revision 
 * 
 * Class to link created Opportunities to the Contract Line Items on the associated Contract
 *  
**/
public class Contract2Opportunity_Link  extends Contract2Opportunity_Utils {

    public Contract2Opportunity_Configuration__c c2oConfig;		// Contract2Opportunity general Configuration
    public List <Opportunity_IB__c> oppIBList;
    public Map<Id,SVMXC__Service_Contract_Products__c> allLineItems; 
    private Map<String,Opportunity> updateOpps;
    public Map<Id, Opportunity> allOpportunities;
	public CustomLogger.Logger OpportunityLog = new CustomLogger.Logger();
        
    public Contract2Opportunity_Link(Map<Id, Opportunity> initialQuery) {  
        // Initialization
        this.allOpportunities = new Map<Id, Opportunity>();
		allOpportunities.putAll(initialQuery);
        this.c2oConfig = c2oConfiguration();
        this.ceProducts = ceSettings();
        this.updateOpps = new Map<String,Opportunity>();
        this.oppIBList = new List <Opportunity_IB__c>();
        this.allLineItems = new  Map<Id,SVMXC__Service_Contract_Products__c>([
            	SELECT 
					id, SVMXC__Product__r.SVC_OrclSerInstruModelName__c, SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c, 
            		SVMXC__Service_Contract__r.Parent_Contract_Number__c
         		FROM SVMXC__Service_Contract_Products__c 
            	WHERE SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c IN :initialQuery.keySet()]);        
        // Initialize custom Logging for Contracts
      	OpportunityLog.process = 'Link Opportunities';
		OpportunityLog.loglevel = c2oConfig.Log_Level__c;
system.debug('!!! - allLineItems: ' +  allLineItems);        
system.debug('!!! - initialQuery.keySet(): ' +  initialQuery.keySet());        
        if(allLineItems.size()>0){
        	for(SVMXC__Service_Contract_Products__c line :allLineItems.values()){
	            Opportunity_IB__c oppIB = new Opportunity_IB__c();
	            oppIB.Covered_Product__c = line.id;
	            oppIB.Opportunity__c = line.SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c;
	            oppIBList.add(oppIB);
	            if(isCE(line.SVMXC__Product__r.SVC_OrclSerInstruModelName__c)){
	                OpportunityLog.addLine('Info','Opportunity' , line.SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c,line.SVMXC__Service_Contract__c ,'Prepare','CE Opportunity');
	                allOpportunities.get(line.SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c).c2o_CE_Opportunity__c = TRUE;
	            }
	        }
        }
        // Insert Opportunity IBs
        if(oppIBList.size() > 0){
            Database.SaveResult[] srList = Database.insert(oppIBList, FALSE);
	        // Iterate through each returned result
			for (Integer c = 0; c < srList.size(); c++) {
				if (srList.get(c).isSuccess() ) {
					// Operation was successful, post to Log
					OpportunityLog.addLine('Success','Covered Product', oppIBList.get(c).Covered_Product__c , oppIBList.get(c).Opportunity__c, 'INSERT', 'Opportunity IB Created ' + srList.get(c).getId()); 
                    if(!updateOpps.containsKey(oppIBList.get(c).Opportunity__c)){
                    	allOpportunities.get(oppIBList.get(c).Opportunity__c).c2o_AutoGeneration_Status__c='Linked';
                    	updateOpps.put(oppIBList.get(c).Opportunity__c, allOpportunities.get(oppIBList.get(c).Opportunity__c));
                    }    
				} else {
	                // Operation failed, post errors to Log
	                Database.Error[] err = srList.get(c).getErrors();
	                String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
	                String escapeErr = errorMsg.replace(',','.');
					OpportunityLog.addLine('Error','Covered Product' , oppIBList.get(c).Covered_Product__c  , oppIBList.get(c).Opportunity__c , 'INSERT', escapeErr);                 
				}
			}
        }
        if(updateOpps.size() > 0){
            List<Opportunity> OppList = updateOpps.values();
            Database.SaveResult[] srList = Database.update(OppList, FALSE);
            // Iterate through each returned result
			for (Integer c = 0; c < srList.size(); c++) {
				if (srList.get(c).isSuccess() ) {
					// Operation was successful, post to Log
					OpportunityLog.addLine('Success','Opportunity' , OppList.get(c).id , OppList.get(c).Contract_Number__c , 'UPDATE' , 'Opportunity set to Linked ' + srList.get(c).getId()); 
				} else {
	                // Operation failed, post errors to Log
	                Database.Error[] err = srList.get(c).getErrors();
	                String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
	                String escapeErr = errorMsg.replace(',','.');
					OpportunityLog.addLine('Error','Opportunity' , OppList.get(c).id  , OppList.get(c).Contract_Number__c  , 'UPDATE' , escapeErr);                 
				}
			}
        }
		OpportunityLog.saveLog();   
   	}
}

/*
 * Code for invoking via Annonymus execution
 * 
   Map<Id, Opportunity> initialQuery = new Map<Id, Opportunity>([	SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,
																			c2o_CE_Opportunity__c
  																		FROM 
																			Opportunity 
																		WHERE 
																			c2o_AutoGeneration_Status__c = 'Created' 
																		]);

   new Contract2Opportunity_Link(initialQuery);
 *
 */
/* OR

String Query = 'SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,c2o_CE_Opportunity__c FROM Opportunity WHERE c2o_AutoGeneration_Status__c = \'Created\'';
Integer scope = 200;
String process = 'Link';
        
Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
id batchInstanceId = database.executebatch(b,scope);

*/