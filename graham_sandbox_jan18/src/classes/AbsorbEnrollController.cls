// ===========================================================================
// Object: AbsorbContactTriggerHandler
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Controller to enroll in available courses, also to display current course enrollments
// ===========================================================================
// Changes: 2016-02-11 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class AbsorbEnrollController 
{
	public Contact cont {get;set;}
	public List<AbsorbModel.CourseEnrollment> currentEnrollments {get;set;}
	public List<CourseWrapper> availableCourses {get;set;}

	public AbsorbEnrollController(ApexPages.StandardController stdController) 
	{
		this.cont = (Contact)stdController.getRecord();	
	}

	public PageReference initAction()
	{
		if(this.cont.LMS_User_ID__c == null) 
		{
			SfdcUtil.addErrorMessageToPage('This contact is not an Absorb LMS user');
			return null;
		}

		AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
		try {
			this.currentEnrollments = absorbClient.getCourseEnrollmentsByUser(this.cont.LMS_User_ID__c);
			this.availableCourses = new List<CourseWrapper>();
			List<AbsorbModel.Course> availCourses = absorbClient.getAvailableCourses(this.cont.LMS_User_ID__c);
			for(AbsorbModel.Course ac : availCourses)
			{
				this.availableCourses.add(new CourseWrapper(ac));
			}
		}catch(Exception e) {
			SfdcUtil.addErrorMessageToPage(e.getMessage());
			return null;
		}finally{
			absorbClient.close();
		}
		
		return null;
	}

	public PageReference enroll()
	{
		AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
		for(CourseWrapper cw : this.availableCourses)
		{
			if(cw.selected)
			{

				try {
					// enroll API call
					absorbClient.enroll(this.cont.LMS_User_ID__c, cw.course.Id);
					SfdcUtil.addConfirmMessageToPage('Enrolled in ' + cw.course.Name + ' successfully');
				}catch(Exception e){
					SfdcUtil.addErrorMessageToPage(e.getMessage());
				}
			}
		}

		absorbClient.close();
		return initAction();
	}

	public PageReference cancel()
	{
		return new PageReference('/'+this.cont.Id);
	}

	public class CourseWrapper {
		public Boolean selected {get;set;}
		public AbsorbModel.Course course {get;set;}

		public CourseWrapper(AbsorbModel.Course course)
		{
			this.course = course;
			this.selected = false;
		}
	}
}