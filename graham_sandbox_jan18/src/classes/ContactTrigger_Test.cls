/*
 *	ContactTrigger_Test
 *	
 *	Test class for ContactTrigger and ContactTriggerHandler.
 *
 *	If there are other test classes related to ContactTrigger, please document it here (as comments).
 * 
 * 	Created by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
private class ContactTrigger_Test {

	// IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] ContactTrigger_Test.test() [Begin]');
    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'Canada');
    	insert testAccount;
    	Contact iudu = new Contact(LastName = 'Last', AccountId = testAccount.Id);
		insert iudu;
		update iudu;
		delete iudu;
		undelete iudu;
        system.debug('[TEST] AccountTrigger_Test.test() [End]');
     }


	// **************************************************************************
	// 	KeyOpinionLeader() testing
	// **************************************************************************
    private static Contact c;
 	private static void setUp_KeyOpinionLeader(Boolean kol)
    {
    	Account testKeyAccount = new Account(Name = 'Test Key Account', BillingCountry = 'United States');
        insert testKeyAccount;

        Account testLocationAccount = new Account(Name = 'Test Location Account', BillingCountry = 'United States', ParentId = testKeyAccount.Id);
        insert testLocationAccount;

        c = new Contact(FirstName = 'John', LastName = 'Doe', AccountId = testLocationAccount.Id, Key_Opinion_Leader_KOL__c = kol);        
        insert c;
    }
    public static testMethod void test_KeyOpinionLeader()
    {
        setUp_KeyOpinionLeader(true);
        system.assertEquals(1, [select Id from Strategic_Customer_Profile__c where Contact__c = :c.Id].size());
        c.Key_Opinion_Leader_KOL__c = false;
        update c;
        system.assertEquals(0, [select Id from Strategic_Customer_Profile__c where Contact__c = :c.Id].size());
        c.Key_Opinion_Leader_KOL__c = true;
        update c;
        system.assertEquals(1, [select Id from Strategic_Customer_Profile__c where Contact__c = :c.Id].size());
    }

	@IsTest public static void test_pendingAccountAssignmentProcessing()
	{
		
		Country_Mapping__c cmap = new Country_Mapping__c(Name = 'Canada', Qualtrics_Language__c = 'EN-US', 
                                                         Country_Code__c = 'CA', Permutations__c = 'Canada',
                                                         Sales_Region__c = 'AMERICAS', SCIEXNow_Region__c = 'AMERICAS');
		insert cmap;

		Account acct = new Account(Name = 'Pending Account Assignment', BillingCountry = 'Canada', Country_Mapping__c = cmap.Id);
		Account acct1 = new Account(Name = 'Pending Account Assignment - AMERICAS', BillingCountry = 'Canada', Country_Mapping__c = cmap.Id);
		insert new List<Account>{acct, acct1};

		Contact c = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1', MailingCountry = 'Canada');
        insert c;

		Contact cIns = [SELECT AccountId FROM Contact WHERE Id = :c.Id LIMIT 1];

		System.assertEquals(acct1.Id, cIns.AccountId);
	}
    

}