/**
 * @author Brett Moore
 * @created - July 2016
 * @Revision  
 * @Last Revision 
 * 
 * Testing for the following classes:
 * 		C2O_Schedule_Assign
 * 		C2O_Schedule_Create
 * 		C2O_Schedule_Link
 * 		C2O_Schedule_Sanitize
 * 		C2O_Schedule_Populate
 **/
@isTest
public class C2O_Schedule_tests {
    
    	// Prepare initial sample data for running tests:
		@testSetup static void setup() {
    	     Contract2Opportunity_Configuration__c c2oConfig = new Contract2Opportunity_Configuration__c(
				name= 'ABC',
	            Assign_Query_Filter__c = 'c2o_AutoGeneration_Status__c = \'Populated\'',
				Create_Query_Filter__c = 'Renewal_Opportunity_Created__c = FALSE AND SVMXC__Active__c = TRUE AND c2o_First_Product_Expiration__c < 2017-01-01  AND  c2o_First_Product_Expiration__c > TODAY ORDER BY PARENT_CONTRACT_NUMBER__c ASC',
	            Link_Query_Filter__c = 'c2o_AutoGeneration_Status__c = \'Created\'',
	            Log_Level__c = 'Warning',
				Populate_Query_Filter__c ='c2o_AutoGeneration_Status__c = \'Sanitized\'',
	            Sanitize_Query_Filter__c ='c2o_AutoGeneration_Status__c = \'Linked\'',
    	        Scope__c = 200 );
            insert c2oConfig;    
    } 
       
    static testmethod void testSchedule(){

		system.debug('***@@@ ----------------- Start Schedule' );      
		Test.startTest();
	        C2O_Schedule_Assign sa = new C2O_Schedule_Assign();
	        C2O_Schedule_Create sc = new C2O_Schedule_Create();
	        C2O_Schedule_Link sl = new C2O_Schedule_Link();
	        C2O_Schedule_Sanitize ss = new C2O_Schedule_Sanitize();
	        C2O_Schedule_Populate sp = new C2O_Schedule_Populate();
	        String sch = '20 30 8 10 2 ?';
        
        	String jobID = system.schedule('schedule batch Job1',sch ,sa);
        	jobID = system.schedule('schedule batch Job2',sch ,sc);
        	jobID = system.schedule('schedule batch Job3',sch ,sl);
        	jobID = system.schedule('schedule batch Job4',sch ,ss);
        	jobID = system.schedule('schedule batch Job5',sch ,sp);
        Test.stopTest();        
		system.debug('***@@@ ----------------- Stop Schedule' );         
    } 
    
    
}