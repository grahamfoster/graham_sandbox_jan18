// ===========================================================================
// Object: COBOUtil
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Shared static Utility methods for Customer onboarding (COBO) trigger
// functionality
// ===========================================================================
// Changes: 2017-04-27 Reid Beckett
//           Class created
// ===========================================================================
public class COBOUtil {
    public static final String FAST_COBO_RECORD_TYPE_NAME = 'COBO-F';
    public static final String FAST_OPPORTUNITY_RECORD_TYPE_NAME = 'Fast';

    //get the record type ID for the Customer_Onboard__c "Fast" (COBO-F)
    public static Id getFastCOBORecordTypeId(){
        return Schema.SObjectType.Customer_Onboard__c.getRecordTypeInfosByName().get(FAST_COBO_RECORD_TYPE_NAME).getRecordTypeId();
    }
    
    //get the record type ID for the Opportunity "Fast"
    public static Id getFastOpportunityRecordTypeId(){
        return Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(FAST_OPPORTUNITY_RECORD_TYPE_NAME).getRecordTypeId();
    }
    
    //get the record type ID for the SR Case "Service Request Case"
    public static Id getServiceRequestCaseRecordTypeId() {
		return Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId();        
    }
    
    //get the custom setting Product Family Settings based on the product family
    public static ProductFamilySettings__c getProductFamilySettings(String productFamily) {
        if(String.isBlank(productFamily)) return null;
		return ProductFamilySettings__c.getAll().get(productFamily);
    }

    //determine if the product is an instrument based on the family and the custom setting Product Family Settings
    public static Boolean isInstrument(String productFamily) {
		ProductFamilySettings__c pfs = getProductFamilySettings(productFamily);
        return (pfs != null && pfs.Instrument__c);
    }

}