public class EditCategory{
    public String CategoryID{get;set;}
    public List<Specifications_Meta__c> prodfeaturelist{get;set;}
    public List<Product_Meta__c> prodmetalist{get;set;}
    public List<SpecificationWrapper> wrapperlist{get;set;}
    public Product_Category__c prodcat{get;set;}
    public Boolean isproductseleteed{get;set;}
    public string attachid{get;set;}
    Attachment AttchObj = new Attachment();
    public Attachment attach {get;set;}
    public blob file1{get;set;}
    public String fname{get;set;}   
    public string contentype{get;set;} 
    public Boolean isComplete{get; private set;}
    public String selApp{get;set;}
    public String selProdFeatId{get;set;}
    public Integer isComplete1{get; set;}
    public Integer isComplete2{get; set;}
     
     
     /*---- delete category -----*/
     public String hiddenmetaId{get;set;}
     public String hiddeProductFeatureId{get;set;}
    //This shows how to insert an Attachment
    public void upload()
    {
        attach = new Attachment();
        attach.Name = fname ;
        attach.ContentType = contentype;
        attach.body=file1; 
        if (attach.Body.size() > 2000000 ) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'You can upload up to 2mb'));
        }       
    }   


    public EditCategory(){
       isComplete1 =0;
       isComplete = false;
       CategoryID=ApexPages.currentPage().getParameters().get('catid');
       wrapperlist= new List<SpecificationWrapper>();
    //   CategoryID='a00b0000002tD6R';
       prodcat =[select Product_Category_Name__c, isActive__c, Category_Icon__c from Product_Category__c where id = :CategoryID];
       //prodfeaturelist=[select id,isActive__c,Product_Category__c,Title__c,Specifications_Meta_Name__c from Specifications_Meta__c where Product_Category__c=:CategoryID];
      
       for(Specifications_Meta__c ac:[select id,isActive__c,(select id,isActive__c,Product_Meta_Name__c,Type__c,Specifications_Meta__c from Products_Meta__r ORDER BY CreatedDate),Product_Category__c,Title__c,Specifications_Meta_Name__c from Specifications_Meta__c where Product_Category__c=:CategoryID order by createddate])
          {            
               wrapperlist.add(new SpecificationWrapper(ac,ac.isActive__c,ac.Products_Meta__r));
                
          }
        //prodmetalist=[select id,isActive__c,Product_Meta_Name__c,Type__c,Specifications_Meta__c from Product_Meta__c where Specifications_Meta__c IN:prodfeaturelist];
       // wrapperlist.add(new SpecificationWrapper(prodfeaturelist,prodmetalist));
        System.debug('wrapper list'+wrapperlist);
       
    }
    
    
    public void refreshCatList(){
      iscomplete1=iscomplete2;
      system.debug('iscomplete2  '+iscomplete2);
      system.debug('iscomplete VVSADFSDF  '+iscomplete1);
      wrapperlist= new List<SpecificationWrapper>();
       
       //prodfeaturelist=[select id,isActive__c,Product_Category__c,Title__c,Specifications_Meta_Name__c from Specifications_Meta__c where Product_Category__c=:CategoryID];
      for(Specifications_Meta__c ac:[select id,isActive__c,(select id,isActive__c,Product_Meta_Name__c,Type__c,Specifications_Meta__c from Products_Meta__r ORDER BY CreatedDate),Product_Category__c,Title__c,Specifications_Meta_Name__c from Specifications_Meta__c where Product_Category__c=:CategoryID order by createddate])
          {            
               wrapperlist.add(new SpecificationWrapper(ac,ac.isActive__c,ac.Products_Meta__r));
                
          }
        //prodmetalist=[select id,isActive__c,Product_Meta_Name__c,Type__c,Specifications_Meta__c from Product_Meta__c where Specifications_Meta__c IN:prodfeaturelist];
       // wrapperlist.add(new SpecificationWrapper(prodfeaturelist,prodmetalist));
        System.debug('wrapper list'+wrapperlist);    
    }
    
    public void checkAllcheckbox(){
        system.debug('>>>>>>%%%%%>>'+wrapperlist);
    }
    
    
    public class SpecificationWrapper{
        public Specifications_Meta__c prodfeat{get;set;}
        public List<Product_Meta__c> prodmeta{get;set;}
        public Boolean Isselectedspec{get;set;}
        public SpecificationWrapper(Specifications_Meta__c prodfeatlist,Boolean Isselectedspec1,List<Product_Meta__c> prodmetalist){
            this.prodfeat=prodfeatlist;
            this.prodmeta=prodmetalist;
            this.Isselectedspec=Isselectedspec1;
        }
    }
   
    public void ok(){
        System.debug('ok call');
        isComplete = false;
        List<Specifications_Meta__c> selectedprodfeaturelist = new  List<Specifications_Meta__c>();
        List<Specifications_Meta__c> deselectedprodfeaturelist=new  List<Specifications_Meta__c>();
        List<Specifications_Meta__c> featurelist =new  List<Specifications_Meta__c>();
        List<Product_Meta__c> seletedprodmetalist= new  List<Product_Meta__c>();
         List<Product_Meta__c> seletedprodmetalist1= new  List<Product_Meta__c>();
         List<Product_Meta__c>  seletedprodmetalist11 = new  List<Product_Meta__c>();
         list<Product_Meta__c> listProductMeta ;
         
        for(SpecificationWrapper c :wrapperlist){
            c.prodfeat.isActive__c = c.Isselectedspec;
            featurelist.add(c.prodfeat);
           
            for(Product_Meta__c  pm: c.prodmeta) {
                listProductMeta =  new list<Product_Meta__c>();
                seletedprodmetalist.add(pm);
            }
          
       }
     
       
      /* for(Specifications_Meta__c ap:[select id,isactive__c from Specifications_Meta__c where id IN:templist]){
           ap.isactive__c=true;
           List<Product_Meta__c> plist=[select id,isactive__c,Product_Meta_Name__c from Product_Meta__c where Specifications_Meta__c =:ap.id order by createddate];
           for(Product_Meta__c p1:plist){
               p1.isactive__c=true;
               seletedprodmetalist11.add(p1);
           }
                 
           selectedprodfeaturelist.add(ap);
       }
                    
                
       if(selectedprodfeaturelist !=null && seletedprodmetalist11 !=null){
              update selectedprodfeaturelist;
              update seletedprodmetalist11;
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Your information has been saved successfully'));
       
       }
       for(Specifications_Meta__c ap:[select id,isactive__c from Specifications_Meta__c where id IN: templist1]){
           ap.isactive__c=false;
           seletedprodmetalist= new  List<Product_Meta__c>();
           List<Product_Meta__c> plist=[select id,isactive__c,Product_Meta_Name__c from Product_Meta__c where Specifications_Meta__c =:ap.id order by createddate];
           
           for(Product_Meta__c p1:plist){
               p1.isactive__c=false;
               seletedprodmetalist1.add(p1);
           }
           
           deselectedprodfeaturelist.add(ap);
       }
                  
                     
                
       if(deselectedprodfeaturelist !=null && seletedprodmetalist1 !=null){
             update deselectedprodfeaturelist;
             update seletedprodmetalist1;
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Your information has been saved successfully'));
       
       }*/
      if(seletedprodmetalist != null){
          update seletedprodmetalist;
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Your information has been saved successfully'));
      }

      if(!featurelist.isEmpty()){
          upsert featurelist;
      }
      
      if(prodcat != null){
          update prodcat;
      }
      if(file1 != null){    
            upload();
            attach.ParentId = prodcat.Id;
            system.debug('attach.ContentType....' +attach.ContentType);
            if(attach != null && (attach.ContentType == 'image/jpg' || attach.ContentType =='image/jpeg' || attach.ContentType == 'image/png' || attach.ContentType == 'image/gif')){
                upsert attach;  
                attachid = attach.id;  
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
            }            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Upload only .jpg/.jpeg/.png/.gif type of Image'));
     }
            
         // to insert the image link in the image filed of the product =====
         if(attachid != null && attachid.trim() != '' && prodcat.id != null )
         {
              prodcat.Category_Icon__c = '/servlet/servlet.FileDownload?file='+attachid;
                
              update prodcat;
            }
       isComplete = true;
     
    }
    
    
     public void dodelete(){
       
        
    
    }
     public PageReference doCancel() {
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
            return new PageReference (cancelURL);
        }
        return null;    
    }
    
     public PageReference delSelMeta(){
       if(String.isNotEmpty(selApp)){
          try { 
              Product_Meta__c metaObj = new Product_Meta__c();
              metaObj.Id = selApp;
              delete metaObj;
             
              PageReference metaPage = new PageReference('/apex/EditCategory?catid='+CategoryID);
              String TabFocus = ApexPages.currentPage().getParameters().get('battletab');
              if(String.isNotEmpty(TabFocus)){
                  metaPage.getParameters().put('tabfocus',TabFocus);
              }
              refreshCatList();
              metaPage.setRedirect(true);
              return metaPage;
          } catch(Exception e){ ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));}        
       }
       selApp = null;
       return null;
   }


public PageReference delproductfeatureCategory(){
        try {
            if(String.isNotEmpty(selProdFeatId)){
                Specifications_Meta__c specObj = new Specifications_Meta__c();
                specObj.Id = selProdFeatId;                                            
                delete specObj;
              PageReference catPage = new PageReference('/apex/EditCategory?catid='+CategoryID);
              refreshCatList();
              selProdFeatId = null;
              catPage.setRedirect(true);
              return catPage;
              
            }
                
        } catch(Exception e){
           ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));     
        }
        return null;
    }

}