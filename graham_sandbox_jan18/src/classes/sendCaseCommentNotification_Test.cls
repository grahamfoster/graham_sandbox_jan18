/*
 *	sendCaseCommentNotification_Test
 *	
 *	Test class for sendCaseCommentNotification
 *
 *	If there are other test classes related to sendCaseCommentNotification, please document it here (as comments).
 * 
 * 	Created by Graham Foster 2017-05-09 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

 @isTest
public class sendCaseCommentNotification_Test {

	@testSetup static void setup() 
	{
		
		Country_Mapping__c countryM = new Country_Mapping__c(Name='United States',Permutations__c='UNITED STATES; USA; US; UNITED STATES OF AMERICA',
                Country_Code__c='US',Country__c='United States',Support_Region__c='AMERICAS', Case_Comment_Email_Template__c = 'dev_name');
        insert countryM;

    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'United States',CurrencyIsoCode = 'USD');
        insert testAccount;

        testAccount = [SELECT Name, Country_Mapping__c, BillingCountry ,CurrencyIsoCode, Country_Mapping__r.Default_Service_Price_Book__c FROM account LIMIT 1];
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = testAccount.Id);
        insert testContact;

		RecordType rt = SfdcUtil.getRecordType('Case', 'CICCase');
        Case ccase = new Case(RecordTypeId = rt.Id, AccountId = testAccount.Id, ContactId = testContact.Id, Subject = 'TestCase', Type = 'TAC');
        insert ccase;

	}

	@isTest static void sendCaseCommentEmail_Test()
	{
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
		 User u = new User(Alias = 'standt', Email='ccEmailtest@sciex.com', 
		EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
		LocaleSidKey='en_US', ProfileId = p.Id, 
		TimeZoneSidKey='America/Los_Angeles', UserName='ccEmailtest@sciex.com');


      System.runAs(u) {
		EmailTemplate testEmailTemplate = new EmailTemplate(isActive = true, Name = 'name', DeveloperName = 'dev_name',
			TemplateType = 'text', FolderId = UserInfo.getUserId());
		insert testEmailTemplate;
      }
		
		Test.startTest();
		Case c = [SELECT Id FROM Case WHERE Subject = 'TestCase' LIMIT 1];
		CaseComment cc = new CaseComment(
		ParentId = c.Id, CommentBody = 'Test Comment',IsPublished = false);
		INSERT cc;
		sendCaseCommentNotification.sendCaseCommentEmail(new List<Id> {cc.Id});
		Integer invocations = Limits.getEmailInvocations();
		Test.stopTest();

		system.assertEquals(1, invocations, 'An email should be sent');
	}



}