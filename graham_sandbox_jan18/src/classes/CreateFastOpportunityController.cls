public class CreateFastOpportunityController {
    public Id id {get;set;}
    
    public CreateFastOpportunityController(ApexPages.StandardController stdController){
        this.id = stdController.getId();
    }
    
    public PageReference init() {
        LMS_Course_Enrollment__c lmsCourseEnrollment = findLMSCourseEnrollment();
        try {
            Opportunity opp = createOpportunity(lmsCourseEnrollment);
            return new ApexPages.StandardController(opp).view();
        }catch(DMLException e) {
            SfdcUtil.addDMLExceptionMessagesToPage(e);
            return null;
        }catch(Exception e) {
            SfdcUtil.addExceptionMessageToPage(e);
            return null;
        }
    }
    
    private LMS_Course_Enrollment__c findLMSCourseEnrollment(){
        LMS_Course_Enrollment__c[] results = [
            select LMS_Course__c, LMS_User__c, LMS_User__r.AccountId, Asset__c,
            LMS_Course__r.Product_Number__c, LMS_Course__r.Price__c,
            LMS_User__r.Account.BillingCountry, LMS_User__r.Account.BillingState,
            LMS_User__r.Account.Name, LMS_User__r.Name, Asset__r.SerialNumber
            from LMS_Course_Enrollment__c 
            where Id = :id
        ];
        return results.isEmpty() ? null : results.get(0);
    }
    
    private Opportunity createOpportunity(LMS_Course_Enrollment__c lmsCourseEnrollment) {
        Id recordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
        Opportunity opp = new Opportunity(
            RecordTypeId = recordTypeId,
            Name = generateOpportunityName(lmsCourseEnrollment),
            AccountId = lmsCourseEnrollment.LMS_User__r.AccountId,
			Primary_Contact__c = lmsCourseEnrollment.LMS_User__c,
            Asset__c = lmsCourseEnrollment.Asset__c,
            StageName = 'Final Negotiation Phase',
            CloseDate = Date.today().addDays(7),
			Reason_for_not_including_Serial_Number__c = 'Quoting customer software or training',
			Lead_Origin__c = 'SciexU',
            Serial_Number__c = lmsCourseEnrollment.Asset__r.SerialNumber
        );
        CountryStateSettings s = getCountryStateSettings(lmsCourseEnrollment);
        OpportunityLineItem oppLineItem = null; 
        if(s != null){
			if(s.opportunityOwnerId != null) {
            	opp.OwnerId = s.opportunityOwnerId;
            }
			if(s.pricebookId != null) {
            	opp.Pricebook2Id = s.pricebookId;
            }
			if(s.currencyIsoCode != null) {
            	opp.CurrencyIsoCode = s.currencyIsoCode;
            }
        }
        insert opp;

        if(s.pricebookId != null) {
            PricebookEntry pricebookEntry = getPricebookEntry(s.pricebookId, lmsCourseEnrollment.LMS_Course__r);
            if(pricebookEntry != null) {
                //insert pricebookEntry;
                oppLineItem = new OpportunityLineItem(
                    OpportunityId = opp.Id,
                    PricebookEntryId = pricebookEntry.Id,
                    Quantity = 1,
                    UnitPrice = pricebookEntry.UnitPrice
                );                    
                insert oppLineItem;
            }
        }

        return opp;
    }
    
    private PricebookEntry getPricebookEntry(Id pricebookId, LMS_Course__c lmsCourse) {
        String productCode = lmsCourse.Product_Number__c;
        if(productCode == null) return null;
        Decimal price = 0;
        try {
            price = Decimal.valueOf(lmsCourse.Price__c);
        }catch(Exception e){}
        Product2[] products = [select Id from Product2 where ProductCode = :productCode and IsActive = true limit 1];
        if(products.isEmpty()) return null;
        
        Id productId = products.get(0).Id;
        return [select Id,UnitPrice from PricebookEntry where Pricebook2Id = :pricebookId and Product2Id = :productId];
    }

    private CountryStateSettings getCountryStateSettings(LMS_Course_Enrollment__c lmsCourseEnrollment){
        system.debug('getCountryStateSettings:'+lmsCourseEnrollment.LMS_User__r.Account);
        String country = lmsCourseEnrollment.LMS_User__r.Account.BillingCountry;
        String state = lmsCourseEnrollment.LMS_User__r.Account.BillingState;
        return getCountryStateSettings(country, state);
    }    
    
    private CountryStateSettings getCountryStateSettings(String country, String state){
        system.debug('getCountryStateSettings:'+country+','+state);
        String stateFilter = '%' + state + '%';
        String countryFilter = '%' + country + '%';
        System.debug('stateFilter='+stateFilter);
        Country_Mapping__c[] countryMappings = [
            select Id, Country__c, FAST_Opportunity_Training_Owner__c, Default_Service_Price_Book__c,
            (select Id, State_Province_Permutations__c, FAST_Opportunity_State_Training_Owner__c 
            from State_Mapping__r where State_Province_Permutations__c like :stateFilter limit 1)
            from Country_Mapping__c 
            where Country__c = :country or Permutations__c like :countryFilter
            limit 1
		];
        if(countryMappings.isEmpty()) return null;
        Country_Mapping__c countryMapping = countryMappings.get(0);
        CountryStateSettings s = new CountryStateSettings();
        s.pricebookId = countryMapping.Default_Service_Price_Book__c;
        if(s.pricebookId != null) {
            if(Test.isRunningTest()) s.currencyIsoCode = UserInfo.getDefaultCurrency();
            else {
                Pricebook2 pricebook = [select Id, CurrencyIsoCode from Pricebook2 where Id = :s.pricebookId];
            	s.currencyIsoCode = pricebook.CurrencyIsoCode;
            }
        }

        if(countryMapping.State_Mapping__r.isEmpty()) {
            s.opportunityOwnerId = countryMapping.FAST_Opportunity_Training_Owner__c;
        }else{
            for(State_Mapping__c stateMapping : countryMapping.State_Mapping__r) {
                s.opportunityOwnerId = stateMapping.FAST_Opportunity_State_Training_Owner__c;
            }
        }
        return s;
    }    

    private String generateOpportunityName(LMS_Course_Enrollment__c lmsCourseEnrollment) {
        return lmsCourseEnrollment.LMS_User__r.Account.Name + ' - ' + lmsCourseEnrollment.LMS_User__r.Name + ' - ' + 'Training';
    }

    class CountryStateSettings {
        public Id opportunityOwnerId {get;set;}
        public Id pricebookId {get;set;}
        public String currencyIsoCode {get;set;}
    }
}