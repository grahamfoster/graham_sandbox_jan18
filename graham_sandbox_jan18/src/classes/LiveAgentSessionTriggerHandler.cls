/*
 *	LiveAgentSessionTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Graham Foster on 2018-01-03
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

public class LiveAgentSessionTriggerHandler  extends TriggerHandler {

	public LiveAgentSessionTriggerHandler() {
		}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void afterInsert() {
		updateBusinessHoursCoverage();
	}
	override protected void afterUpdate() {
	}


	// **************************************************************************
	// 		private methods
	// **************************************************************************
    
	/**
	* @description Collects the business hours definitions for all of the local sciexnow implementations and calculates the gmt business hours for the 
	* current day.  Then collects all of the live agent sessions that have been made in the last 24hrs and calculates the coverage in a percentage of the
	* total mins during the day where someone was live online.  This is then written into a Live_Agent_Session_Coverage__c object.
	* Requires the SCIEXNow_Test Business hours to be configured in the org as Business Hours cannot be inserted in test classes and if real SCIEXNow
	* ones were used then tests would fail on saturday and sunday.
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/201064449/updateBusinessHoursCoverage
	*/ 
	private void updateBusinessHoursCoverage()
	{
		Datetime currTime = System.now();
		Datetime Previous24 = currTime.addHours(-24);
		String tDay = currTime.format('EEEE');
		//get a list of the live agent sessions that have been completed today
		List<LiveAgentSession> sessions = [SELECT LoginTime,LogoutTime FROM LiveAgentSession 
		WHERE LoginTime > :Previous24];
		String soqlQuery = 'SELECT Id,CreatedById,CreatedDate,MondayStartTime,MondayEndTime,TuesdayStartTime,TuesdayEndTime,' + 
		'WednesdayStartTime,WednesdayEndTime,ThursdayStartTime,ThursdayEndTime,FridayStartTime,FridayEndTime,SaturdayStartTime,' + 
		'SaturdayEndTime,SundayStartTime,SundayEndTime,' + 
		'Name,TimeZoneSidKey FROM BusinessHours WHERE IsActive = true AND Name LIKE \'SCIEXNow%\'';
		List<BusinessHours> bh = Database.query(soqlQuery);
		Set<Id> bhIds = new Set<Id>();
		for(BusinessHours b : bh)
		{
			bhIds.add(b.Id);
		}
		List<Live_Agent_Session_Coverage__c> existingCC = [SELECT Id, Business_Hours__c, Coverage_Date__c, Percent_Coverage__c
		FROM Live_Agent_Session_Coverage__c WHERE Business_Hours__c IN :bhIds 
		AND (Coverage_Date__c = TODAY OR Coverage_Date__c = YESTERDAY)];
		List<Live_Agent_Session_Coverage__c> newCC = new List<Live_Agent_Session_Coverage__c>();
		for(BusinessHours b : bh)
		{
			//get the offset from GMT for the timezone
			Datetime offsetTime = Datetime.newInstanceGmt(System.today(), (Time)b.get(tDay + 'StartTime'));
			Timezone tz = TimeZone.getTimeZone(b.TimeZoneSidKey);
			Integer timeoffset = tz.getOffset(offsetTime);
			//get the start an end times for today according to the Business hours
			Date correctedDay = System.today();
			//make sure its not Sunday GMT (Asia logging in early)- If it is reset it to monday
			if(tDay == 'Sunday') 
			{
				tDay = 'Monday';
				correctedDay = correctedDay.addDays(1);
			}
			//make sure its not Saturday GMT (Americas logging in late)- If it is reset it to friday
			if(tDay == 'Saturday') 
			{
				tDay = 'Friday';
				correctedDay = correctedDay.addDays(-1);
			}
			Time eTime = (Time)b.get(tDay + 'EndTime');
			Time sTime = (Time)b.get(tDay + 'StartTime');
			//create gmt instances using the timezone offsets
			Datetime gmtStart = DateTime.newInstanceGMT(System.today(), sTime);
			gmtStart = gmtStart.addSeconds((timeoffset * -1)/1000);
			Datetime gmtEnd = DateTime.newInstanceGMT(System.today(), eTime);
			gmtEnd = gmtEnd.addSeconds((timeoffset * -1)/1000);
			Datetime LoopTime = gmtStart;
			Integer minCoveredCount = 0;
			Integer totalMinCount = 0;
			//loop all of the mins in the business hours and using mincovered method calculate the coverage
			do{
				minCoveredCount += minCovered(sessions, LoopTime);
				totalMinCount += 1;
				LoopTime = LoopTime.addMinutes(1);
			} while (LoopTime <= gmtEnd);
			System.debug('Business Hours:' + b.Name);
			System.debug('StartTime:' + gmtStart);
			System.debug('EndTime:' + gmtEnd);
			System.debug('Timezone:' + b.TimeZoneSidKey);
			System.debug('Total Minute Count:' + totalMinCount);
			System.debug('ConveredMins:' + minCoveredCount);
			//create or update the coverage record
			Live_Agent_Session_Coverage__c las = getLiveAgentCoverageRecord(correctedDay, b.Id, existingCC);
			Decimal decPerCovered = ((Decimal)minCoveredCount / (Decimal)totalMinCount) * 100;
			las.Percent_Coverage__c = decPerCovered.round(System.RoundingMode.HALF_UP);
			//System.debug('Coverage:' + ((Decimal)minCoveredCount / (Decimal)totalMinCount) * 100);
			System.debug('lasCoverage' + las.Percent_Coverage__c);
			newCC.add(las);
		}
		if(newCC.size() > 0)
		{
			upsert newCC;
		}
	}

	/**
	* @description gets the Live Agent Coverage record for this Date and business hours.
	* @param coverageDate The date we are calculating the coverage for
	* @param bHoursId The Id of the Business Hours we are checking
	* @param calcs The existing collection of Live Agent Coverage Records
	* @return Either a new one to add the coverage percentage to or the existing Live Agent Session Coverage record to update.
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/197492753/getLiveAgentCoverageRecord
	*/ 
	private Live_Agent_Session_Coverage__c getLiveAgentCoverageRecord(Date coverageDate, Id bHoursId, List<Live_Agent_Session_Coverage__c> calcs)
	{
		for(Live_Agent_Session_Coverage__c la : calcs)
		{
			if(la.Coverage_Date__c == coverageDate && la.Business_Hours__c == bHoursId)
			{
				return la;
			}
		}
		return new Live_Agent_Session_Coverage__c(Coverage_Date__c = coverageDate, Business_Hours__c = bHoursId);
	}

	/**
	* @description checks if the time we are checking was covered by one of the live agent sessions
	* @param sessions The last 24 hrs of live agent sessions
	* @param qTime the time we are checking
	* @return 1 to add to the running total if the minute was covered - otherwise 0
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/200048660/minCovered
	*/ 
	private Integer minCovered(List<LiveAgentSession> sessions, Datetime qTime)
	{
		for(LiveAgentSession s : sessions)
		{
			if(s.LoginTime <= qTime && s.LogoutTime >= qTime)
			{
				return 1;
			}
		}
		return 0;
	}

}