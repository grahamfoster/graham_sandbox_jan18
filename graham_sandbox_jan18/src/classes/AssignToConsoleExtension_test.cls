@isTest
private class AssignToConsoleExtension_test {

	@TestSetup
	private static void setup()
	{
		Group g1 = new Group(Name='SASQueue', type='Queue');
		Group g2 = new Group(Name='TACQueue', type='Queue');
		Group g3 = new Group(Name='CXCQueue', type='Queue');
		Group g4 = new Group(Name='OfficeQueue', type='Queue');
        insert new List<Group>{g1,g2,g3,g4};
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
		QueuesObject q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Case');
		QueuesObject q3 = new QueueSObject(QueueID = g3.id, SobjectType = 'Case');
		QueuesObject q4 = new QueueSObject(QueueID = g4.id, SobjectType = 'Case');
		insert new List<QueuesObject> {q1,q2,q3,q4};
	}
	
	
	@isTest
	private static void testTheClass() {

		List<BusinessHours> b = [SELECT Id FROM BusinessHours WHERE IsDefault = true LIMIT 1];
		Country_Mapping__c cmap = new Country_Mapping__c(Name = 'Canada', Qualtrics_Language__c = 'EN-US', 
                                                         Country_Code__c = 'CA', Permutations__c = 'Canada',
                                                         Sales_Region__c = 'AMERICAS', SCIEXNow_Region__c = 'AMERICAS',
                                                        SAS_Queue__c = 'SASQueue', TAC_Queue__c = 'TACQueue',
														CXC_Queue__c = 'CXCQueue',  Office_Queue__c = 'OfficeQueue',
														Business_Hours__c = b[0].Id);
		Country_Mapping__c cmap2 = new Country_Mapping__c(Name = 'United Kingdom', Qualtrics_Language__c = 'EN-US', 
                                                         Country_Code__c = 'UK', Permutations__c = 'United Kingdom',
                                                         Sales_Region__c = 'EMEAI', SCIEXNow_Region__c = 'EMEA',
														Business_Hours__c = b[0].Id);
		insert new List<Country_Mapping__c>{cmap,cmap2};

		Account a = new Account(Name = 'Tester Account', BillingCountry = 'Canada');
		Account a1 = new Account(Name = 'Tester Account1', BillingCountry = 'United Kingdom');
		insert new List<Account>{a,a1};
		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', 
		AccountId = a.Id, Email = 'tc@example.com');
		Contact c1 = new Contact(FirstName = 'Test', LastName = 'Contact', 
		AccountId = a1.Id, Email = 'tc2@example.com');
		insert new List<Contact>{c,c1};
		Case cs1 = new Case(ContactId = c.Id, AccountId = a.Id,  
                            Subject = 'CXCtest', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs2 = new Case(ContactId = c.Id, AccountId = a.Id,  
                            Subject = 'SAStest', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs3 = new Case(ContactId = c.Id, AccountId = a.Id,  
                            Subject = 'TACtest', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs4 = new Case(ContactId = c.Id, AccountId = a.Id,  
                            Subject = 'Officetest', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs5 = new Case(ContactId = c1.Id, AccountId = a.Id,  
                            Subject = 'CXCtest - Blank', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs6 = new Case(ContactId = c1.Id, AccountId = a.Id,  
                            Subject = 'SAStest - Blank', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs7 = new Case(ContactId = c1.Id, AccountId = a.Id,  
                            Subject = 'TACtest - Blank', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		Case cs8 = new Case(ContactId = c1.Id, AccountId = a.Id,  
                            Subject = 'Officetest - Blank', Description = 'Case Test', 
                            RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId());
		insert new List<Case>{cs1,cs2,cs3,cs4,cs5,cs6,cs7,cs8};
		
		
		Test.startTest();
		AssignToConsoleExtension.assignToCXAgent(cs1.Id);
		AssignToConsoleExtension.assignToSAS(cs2.Id);
		AssignToConsoleExtension.assignToTAC(cs3.Id);
		AssignToConsoleExtension.assignToCXOffice(cs4.Id);
		AssignToConsoleExtension.assignToCXAgent(cs5.Id);
		AssignToConsoleExtension.assignToSAS(cs6.Id);
		AssignToConsoleExtension.assignToTAC(cs7.Id);
		AssignToConsoleExtension.assignToCXOffice(cs8.Id);
		List<Group> groups = [SELECT Id, Name FROM Group WHERE Name = 'SASQueue' OR Name = 'TACQueue' 
		OR Name = 'CXCQueue' OR Name = 'OfficeQueue'];
		List<Case> test1 = [SELECT Subject, OwnerId FROM Case WHERE ContactId = :c.Id];
		for(Case ca : test1)
		{
			if(ca.Subject == 'CXCtest')
			{
				Id i1;
				for(Group gr : groups)
				{
					if(gr.Name == 'CXCQueue')
					{
						i1 = gr.Id;
					}
				}
				System.assertEquals(i1,ca.OwnerId);
				//change this case owner to myself and create a call record
				Date dateToday = Date.today();
				String sMonth = String.valueof(dateToday.month());
				String sDay = String.valueof(dateToday.day());
				if(sMonth.length()==1){
  					sMonth = '0' + sMonth;
				}
				if(sDay.length()==1){
  				sDay = '0' + sDay;
				}
				String sToday = 'Call ' + String.valueof(dateToday.year()) + '-' + sMonth + '-' + sDay + '11111111';
				Task t = new Task(Subject = sToday, cnx__CTIInfo__c = 'ANI: 447818507838, DNIS: 1925237611, DialedNumber: 1925237611');
				insert t;
				AssignToConsoleExtension.assignToSelf(ca.Id);
			}
			else if(ca.Subject == 'SAStest')
			{
				Id i2;
				for(Group gr : groups)
				{
					if(gr.Name == 'SASQueue')
					{
						i2 = gr.Id;
					}
				}
				System.assertEquals(i2,ca.OwnerId);
				//assign this case to spam
				AssignToConsoleExtension.assignToSpam(ca.Id);
			}
			else if(ca.Subject == 'TACtest')
			{
				Id i3;
				for(Group gr : groups)
				{
					if(gr.Name == 'TACQueue')
					{
						i3 = gr.Id;
					}
				}
				System.assertEquals(i3,ca.OwnerId);
			}
			else if(ca.Subject == 'Officetest')
			{
				Id i4;
				for(Group gr : groups)
				{
					if(gr.Name == 'OfficeQueue')
					{
						i4 = gr.Id;
					}
				}
				System.assertEquals(i4,ca.OwnerId);
			}
		}
		List<Case> test2 = [SELECT Subject, OwnerId FROM Case WHERE Subject = 'CXCtest' OR Subject = 'SAStest'];
		for(Case ca : test2)
		{
			if(ca.Subject == 'CXCtest')
			{
				Task t = [SELECT Id, WhatId FROM Task WHERE cnx__CTIInfo__c = 'ANI: 447818507838, DNIS: 1925237611, DialedNumber: 1925237611' LIMIT 1];
				system.assertEquals(ca.Id, t.WhatId);
				System.assertEquals(UserInfo.getUserId(),ca.OwnerId);
			}
			else if(ca.Subject == 'SAStest')
			{
				System.assertEquals('00GF0000004vCDqMAM',ca.OwnerId);
			}
		}
		List<Case> test3 = [SELECT Subject, OwnerId FROM Case WHERE ContactId = :c1.Id];
		for(Case ca : test3)
		{
			//check that executing user is the owner (so it has not been changed)
			System.assertEquals(UserInfo.getUserId(),ca.OwnerId);
		}
		Test.stopTest();


	}
}