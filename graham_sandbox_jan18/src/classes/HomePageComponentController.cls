/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : October, 2012
Description    : controller for HomePageComponent VF page
History        :

*/

with sharing public class HomePageComponentController
{
   public String  aID {get; set;}   
   
   //Creating a list Sales News
   Public List<OutputNews> Sales_News
   {
    get
    {
        List<Corporate_News__c> asd = new List<Corporate_News__c>();
        asd = [SELECT ID, Name, Description__c, CreatedDate, Type__c FROM Corporate_News__c WHERE Type__c = 'Sales News' ORDER BY CreatedDate DESC limit 6];
            List<OutputNews> dsa = new List<OutputNews>();
            for(Corporate_News__c a: asd)
            {
                dsa.add(new OutputNews(a));
            }
        return dsa;
    }
    set;
   }
   
   //Creating a list Product News   
    public List<OutputNews> Product_News
   {
        get
        {
            List<Corporate_News__c> asd2 = new List<Corporate_News__c>();
            asd2 = [SELECT ID, Name, Description__c, CreatedDate, Type__c FROM Corporate_News__c WHERE Type__c = 'Product News' ORDER BY CreatedDate DESC limit 6];
                List<OutputNews> dsa2 = new List<OutputNews>();
                for(Corporate_News__c a: asd2)
                {
                    dsa2.add(new OutputNews(a));
                }
            return dsa2;
        }
        set;
   }    
   
   //Creating a list Competitive News   
   public List<OutputNews> Competitive_News
   {
    get
    {
        List<Corporate_News__c> asd3 = new List<Corporate_News__c>();
        asd3 = [SELECT ID, Name, Description__c, CreatedDate, Type__c FROM Corporate_News__c WHERE Type__c = 'Competitive News' ORDER BY CreatedDate DESC limit 6];
            List<OutputNews> dsa3 = new List<OutputNews>();
            for(Corporate_News__c a: asd3)
            {
                dsa3.add(new OutputNews(a));
            }           
        return dsa3;
    }
    set;
   }    
   //Creating a list Open Leads   
   public List<OutputLead> OpenLeads
   {
    get
    {
        String UserID = UserInfo.getUserId();
        list<Lead> Leads = new list<Lead>();
        Leads = [SELECT CreatedDate, Name, Company, New_Lead__c, Market_Segment__c, Intent_To_Purchase__c, Camp__c, LeadSource, Department__c,  Products__c, Email, ID FROM Lead Where Status = 'Open' and   Owner.Id =: UserID ORDER BY CreatedDate DESC limit 5 ];
            List<OutputLead> LeadsOut = new List<OutputLead>();     
            for(Lead a: Leads)
            {
                LeadsOut.add(new OutputLead(a));
            }  

        return LeadsOut;
    }
    set;
   }
   //Creating a list Opportunity 
   public List<OutputOpp> OpenOpp
   {
    get
    {
        String UserID = UserInfo.getUserId();
        list<Opportunity> Opp = new list<Opportunity>();
        Opp = [SELECT CreatedDate, Name, Account.Name,  CloseDate,  Amount, StageName, ID FROM Opportunity WHERE Owner.Id =: UserID 
                                                                                                            AND StageName != 'Deal Won'
                                                                                                            AND StageName != 'Deal Lost'
                                                                                                            AND StageName != 'Dead/Cancelled'                                                                                                                                                                                                                       
                                                                                                            AND ((Mgr_Upside__c = true 
                                                                                                            OR Rep_Upside__c = true)
                                                                                                            OR (In_Forecast_Mgr__c = true 
                                                                                                            OR In_Forecast_Rep__c = true))                                                                                                             
                                                                                                            ORDER BY CloseDate DESC limit 5 ];

            List<OutputOpp> OppOut = new List<OutputOpp>();     
            for(Opportunity a: Opp)
            {
                OppOut.add(new OutputOpp(a));
            }           
        return OppOut;
    }
    set;
   }   
   

   //The transition to record object Lead
   public PageReference OpenActivityDetailOpenLeads() 
   {
     aID = System.currentPageReference().getParameters().get('aID');
     string Prefix; 
     PageReference ActPage;
     ActPage = new PageReference('/'+ aID); 
     
     return ActPage;
   } 
   //The transition to record object Opportunity
   public PageReference OpenActivityDetailOpp() 
   {
     aID = System.currentPageReference().getParameters().get('aID');

     PageReference ActPage;
     ActPage = new PageReference('/' + aID); 
     
     return ActPage;
   }    
   //The transition to List Views Sales News   
   public PageReference MoreSalesNews()
   {

    PageReference ActDescPage = new PageReference('/a1B?fcf=00BF0000006ZDV2');
    return ActDescPage;
   } 
    //The transition to List Views Product News   
   public PageReference MoreProductNews()
   {

    PageReference ActDescPage = new PageReference('/a1B?fcf=00BF0000006ZDV1');
    return ActDescPage;
   }    
     //The transition to List Views Competitive News  
   public PageReference CompetitiveNews()
   {

    PageReference ActDescPage = new PageReference('/a1B?fcf=00BF0000006ZDV0');
    return ActDescPage;
   }  
       //The transition to List Views Opportunities  
   public PageReference MoreOpp()
   {
    

    PageReference ActDescPage = new PageReference('/006?fcf=00BF0000006ZDVJ');
    return ActDescPage;
   } 
         //The transition to List Views Leads  
   public PageReference MoreLead()
   {
    PageReference ActDescPage = new PageReference('/00Q?fcf=00BF0000006ZDVG');
    return ActDescPage;
   }       
         //Classes form a list item  List<OutputLead>
   private class OutputNews  
   {
    public Date TimeNow {get; set;}
    public Integer InTime {get; set;}
    public Integer InTimeCR {get; set;}   
    public Integer InTimeAll {get; set;}  
    public Corporate_News__c Obj {get; set;}
    public Boolean OutNew {get; set;}
    
    public OutputNews(Corporate_News__c o)
    {
        TimeNow = Date.today();
        Obj = o;
        String NowYear = String.valueOf(TimeNow.year());
        String NowMonth = String.valueOf(TimeNow.month());
        If(NowMonth.length() != 2)
        {
            NowMonth = '0' + NowMonth;
        }
        String NowDay = String.valueOf(TimeNow.day());
        If(NowDay.length() != 2)
        {
            NowDay = '0' + NowDay;
        }
        String Datee = NowYear + NowMonth + NowDay;
        InTime = Integer.valueOf(Datee);
        
        String CrYear = String.valueOf(Obj.CreatedDate.year());
        String CrMonth = String.valueOf(Obj.CreatedDate.month());
        If(CrMonth.length() != 2)  
        {
            CrMonth = '0' + CrMonth;
        }       
        String CrDay = String.valueOf(Obj.CreatedDate.day());
        If(CrDay.length() != 2)  
        {
            CrDay = '0' + CrDay;
        }           
        String DateeCR = CrYear + CrMonth + CrDay;
        InTimeCR = Integer.valueOf(DateeCR);        
        InTimeAll = InTime - InTimeCR;

        if((this.InTimeAll >= 0 & this.InTimeAll <= 3) || (this.InTimeAll >= 70 & this.InTimeAll <= 72))
        {
            this.OutNew = true;
        }
        else
        {
            this.OutNew = false;
        }
 
                    
    }
   } 
          //Classes form a list item  List<OutputOpp>  
   private class OutputOpp  
   {
    public Date TimeNow {get; set;}
    public Integer InTime {get; set;}
    public Integer InTimeCR {get; set;}   
    public Integer InTimeAll {get; set;}  
    public Opportunity Obj {get; set;}
    public Boolean OutNew {get; set;}
    
    public OutputOpp(Opportunity o)
    {
        this.Obj = o;
    }
   } 
   
   public class OutputLead  
   {
    public Date TimeNow {get; set;}
    public Integer InTime {get; set;}
    public Integer InTimeCR {get; set;}   
    public Integer InTimeAll {get; set;}   
    public Boolean NewLead {get; set;}
    public Boolean NewLeadF {get; set;}    
    public Lead Obj {get; set;}
    public Boolean OutNew {get; set;}
    
    public OutputLead(Lead o)
    {
        this.TimeNow = Date.today();
        this.Obj = o;
        
        String NowYear = String.valueOf(TimeNow.year());
        String NowMonth = String.valueOf(TimeNow.month());
        If(NowMonth.length() != 2)
        {
            NowMonth = '0' + NowMonth;
        }
        String NowDay = String.valueOf(TimeNow.day());
        If(NowDay.length() != 2)
        {
            NowDay = '0' + NowDay;
        }
        String Datee = NowYear + NowMonth + NowDay;
        InTime = Integer.valueOf(Datee);
        
        String CrYear = String.valueOf(Obj.CreatedDate.year());
        String CrMonth = String.valueOf(Obj.CreatedDate.month());
        If(CrMonth.length() != 2)  
        {
            CrMonth = '0' + CrMonth;
        }       
        String CrDay = String.valueOf(Obj.CreatedDate.day());
        If(CrDay.length() != 2)  
        {
            CrDay = '0' + CrDay;
        }           
        String DateeCR = CrYear + CrMonth + CrDay;
        InTimeCR = Integer.valueOf(DateeCR);        
        InTimeAll = InTime - InTimeCR;
    

        if((this.InTimeAll >= 0 & this.InTimeAll <= 3) || (this.InTimeAll >= 70 & this.InTimeAll <= 72))
        {
            this.OutNew = true;
        }
        else
        {
            this.OutNew = false;
        }
        
        //We assign a value of NewLeadF - true or false: true - if Lead created 15 days or less from today, false - more than 15 days        
        if(Obj.New_Lead__c == 1)
        {
            this.NewLeadF = true;
        }
        else
        {
            this.NewLeadF = false;
        }
        
    }
   } 
      
      
     static testMethod void Var() 
    {
    String UserID = UserInfo.getUserId();
        
    Corporate_News__c Sales = new Corporate_News__c();
    Sales.Name = 'Test';
    Sales.Description__c = 'dsf f gdf gdfs gsdf gsdfg sdg sdfg sdf ';
    Sales.Type__c = 'Sales News';
    insert Sales;
    
    Corporate_News__c Product = new Corporate_News__c();
    Product.Name = 'Test';
    Product.Description__c = 'dsf f gdf gdfs gsdf gsdfg sdg sdfg sdf ';
    Product.Type__c = 'Product News';
    insert Product; 
    
    
    Corporate_News__c Competitive = new Corporate_News__c();
    Competitive.Name = 'Test';
    Competitive.Description__c = 'dsf f gdf gdfs gsdf gsdfg sdg sdfg sdf ';
    Competitive.Type__c = 'Competitive News';
    insert Competitive; 
    
        Account acc2 = new Account();
        acc2.Name = 'OOO Test';
        acc2.BillingCountry='Canada';       
        insert acc2;    
        
        Contact con = new Contact();
        Con.LastName = 'ewerwer';
        Con.AccountId = acc2.id;
        insert con;    
    
         Opportunity kk = new Opportunity( 
         AccountId = acc2.id,         
         name = 'Heyllo',
         OwnerID = UserID, 
         StageName='Prospect/Budget Requested',
         Shippable_Date__c = date.today(),
         Mgr_Upside__c = true,
         CloseDate=date.newinstance(1960, 2, 17)
         );  
         insert kk;
         
    Lead leads = new Lead();
    
    leads.Ownerid = UserID;
    leads.CurrencyIsoCode = 'USD';
    leads.Email = 'testemai@test.com';
    leads.LastName = 'et ywrtyh';   
    leads.Company = 'Test Company';   
    leads.Intent_To_Purchase__c = 'D - Demand Active Marketing Lead';   
    leads.Status = 'Open';
    leads.LeadSource = 'Customer Survey';
    leads.Products__c= '4000 QTRAP';
    leads.CreatedDate = date.today();
    Insert leads;
    

    
    HomePageComponentController comp = new HomePageComponentController();
    List<OutputNews> Sales_News = comp.Sales_News;
    List<OutputNews> Product_News = comp.Product_News;
    List<OutputNews> Competitive_News = comp.Competitive_News;
    List<OutputLead> OpenLeads = comp.OpenLeads;
    List<OutputOpp> OpenOpp = comp.OpenOpp;    
    Pagereference a = comp.MoreLead();
    Pagereference b = comp.MoreOpp();
    Pagereference c = comp.MoreProductNews();
    Pagereference d = comp.MoreSalesNews();
    Pagereference i = comp.CompetitiveNews();
    ApexPages.currentPage().getParameters().put('aID',leads.Id);
    Pagereference g = comp.OpenActivityDetailOpenLeads();
    ApexPages.currentPage().getParameters().put('aID',kk.Id);   
    Pagereference h = comp.OpenActivityDetailOpp();
    
    OutputLead Output = new OutputLead(leads);
    Output.OutNew = true;
    
    }      
}