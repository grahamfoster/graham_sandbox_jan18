global class IncrementReportExport implements Schedulable {
   public static final String NIGHTLY_CRON = '0 0 2 * * ?';

   global void execute(SchedulableContext ctx) {
      System.debug('Entered Cron trigger');
      ReportExport__c r = [SELECT Id, Name, ReportExportTrigger__c FROM ReportExport__c WHERE Name = 'ThisReport' LIMIT 1];
      r.ReportExportTrigger__c += 1;
      System.debug('updating trigger to: ' + r.ReportExportTrigger__c );
      update r;
      
      // this section of code will abort the current schedule job 
      try { 
          system.abortJob(ctx.getTriggerId()); 
      } 
      catch (exception e) {
          system.debug('#### schedule job exception while aborting:' + e);
      } 
        
      // reschedule the job 
      system.debug('#### schedule job executing'); 
      scheduleNow(); 
      } 

      global static void scheduleNow() { 

            // this section of code will schedule the next execution 1 minute from now 
            datetime thisTime = system.now().addHours(24); 
            integer minute = thisTime.minute(); 
            integer second = thisTime.second(); 
            integer hour = thisTime.hour(); 
            integer year = thisTime.year(); 
            integer month = thisTime.month(); 
            integer day = thisTime.day(); 
            
            String timeStamp = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year; 
            string jobName = 'IncrementReportExport Job at ' + timeStamp; 
            
            IncrementReportExport iRes = new IncrementReportExport(); 
            system.schedule(jobName, timeStamp , iRes); 
       }

      global static void scheduleNightly() { 
            system.schedule('IncrementReportExport Job nightly', NIGHTLY_CRON , new IncrementReportExport()); 
       }
}