/**
** @author Reid Beckett, Cloudware Connections
** @created Mar 7/2014
**
** Test coverage for CiP controller class
** 
**/
@isTest(SeeAllData=true)
public class CIP2Tests {

	public static testMethod void test1() {
		Test.startTest();
		createProductCategories();
		createTestPricebook();
		createCompetitiveNews();
		createSpecs();
		createPackages();
		createOpportunities();
		Test.stopTest();

		String abpid = getTestPricebookEntry(0).Product2Id;
		String cpid = getTestPricebookEntry(1).Product2Id; 

		ApexPages.currentPage().getParameters().put('abpid', abpid);
		ApexPages.currentPage().getParameters().put('cpid', cpid);

		/* initial setup calls */
		CIP2 cip_controller = new CIP2();
		System.debug(cip_controller.Competitive_News);
		cip_controller.CompetitiveNews();
		system.debug(cip_controller.getListOfRegions());
		CIP2.getCompetitveMatrixItems('Global');
		CIP2.getProductMenuItems();
		CIP2.getCompetitorProductMenuItems();
		
		/* action calls */
		cip_controller.viewSpecs();
		CIP2.getWinLossChartData(abpid, cpid);
		CIP2.getLostReasonData(abpid, cpid);


		system.debug(cip_controller.addToFavorites());
		system.debug(cip_controller.removeFromFavorites());

	}

	private static void createOpportunities(){
		Account testAcct = new Account(Name = 'Test Account', BillingCountry = 'United States');
		insert testAcct;

		OpportunityStage openStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = false and DefaultProbability <= 50 order by DefaultProbability desc limit 1];
		OpportunityStage wonStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = true and IsWon = true];
		OpportunityStage lostStage = [select MasterLabel from OpportunityStage where IsActive = true and IsClosed = true and IsWon = false];
		
		PricebookEntry abEntry = getTestPricebookEntry(0);
		PricebookEntry compEntry = getTestPricebookEntry(1);
        
		List<Opportunity> opps = new List<Opportunity>{
			new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 1', CloseDate = Date.today(), StageName = openStage.MasterLabel, Select_Competitor_that_Won_Order__c = compEntry.Product2.Competitor_Manufacturer__c, Competitor_Instrument_Type__c = compEntry.Product2.Name, 
					Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', CurrencyISOCode='USD'),
			new Opportunity(AccountId = testAcct.Id, Name = 'Test Opp 2', CloseDate = Date.today(), StageName = openStage.MasterLabel, Select_Competitor_that_Won_Order__c = compEntry.Product2.Competitor_Manufacturer__c, Competitor_Instrument_Type__c = compEntry.Product2.Name, 
					Explicit_Needs__c = '?', Implications__c = '?', Implied_Needs__c = '?', Need_Payoff_Value_of_Solution__c = '?', CurrencyISOCode='USD',
					Competitor_Amount__c = 1000, Lost_Order_Detail__c = 'Lost detail')
		};
		insert opps;

		List<OpportunityLineItem> lineItems = new List<OpportunityLineItem> {
			new OpportunityLineItem(OpportunityId = opps[0].Id, PricebookEntryId = abEntry.Id, Quantity = 1, UnitPrice = 2000),
			new OpportunityLineItem(OpportunityId = opps[1].Id, PricebookEntryId = abEntry.Id, Quantity = 1, UnitPrice = 1000)
		};
		insert lineItems;

		opps[0].StageName = wonStage.MasterLabel;
		opps[1].StageName = lostStage.MasterLabel;

		update opps;
	}

	private static void createPackages(){
		Map<String,Application__c> applications = new Map<String,Application__c> {
			'Omics' => new Application__c(Application_Name__c='Omics', isActive__c = true, Image__c = '/servlet/servlet.FileDownload?file=00PF000000CP9xkMAD'),
			'Pharma' => new Application__c(Application_Name__c='Pharma', isActive__c = true, Image__c = '/servlet/servlet.FileDownload?file=00PF000000CP9xkMAD'),
			'Food & Beverage' => new Application__c(Application_Name__c='Food & Beverage', isActive__c = true, Image__c = '/servlet/servlet.FileDownload?file=00PF000000CP9xkMAD'),
			'Environmental' => new Application__c(Application_Name__c='Environmental', isActive__c = true, Image__c = '/servlet/servlet.FileDownload?file=00PF000000CP9xkMAD'),
			'Forensic' => new Application__c(Application_Name__c='Forensic', isActive__c = true, Image__c = '/servlet/servlet.FileDownload?file=00PF000000CP9xkMAD'),
			'Clinical' => new Application__c(Application_Name__c='Clinical', isActive__c = true, Image__c = '/servlet/servlet.FileDownload?file=00PF000000CP9xkMAD')
		};

		for(Application__c app : [select Application_Name__c from Application__c where isActive__c = true]){
			if(applications.containsKey(app.Application_Name__c)) applications.remove(app.Application_Name__c);
		}
		if(applications.size() > 0) insert applications.values();

		Map<String,Application_Variants__c> appVariants = new Map<String,Application_Variants__c> {
			'Global Metabolomics' => new Application_Variants__c(Application_Variant_Name__c = 'Global Metabolomics', Application__c = getApplication('Omics').Id, isActive__c = true)
		};
		for(Application_Variants__c av : [select Application_Variant_Name__c from Application_Variants__c where isActive__c = true]) {
			if(appVariants.containsKey(av.Application_Variant_Name__c)) appVariants.remove(av.Application_Variant_Name__c);
		}
		if(appVariants.size() > 0) insert appVariants.values();


		List<Package__c> packages = new List<Package__c> {
			new Package__c(isActive__c = true, Product__c = getTestPricebookEntry(0).Product2Id, Competitor_Product__c = getTestPricebookEntry(1).Product2Id)
		};
		insert packages;
	}

	private static Application__c getApplication(String applicationName) {
		return [select Id from Application__c where Application_Name__c = :applicationName limit 1];
	}

	/*
	private static Package__c getPackage(Id productId){
		return [select Id from Package__c where Product__c = :productId and isActive__c = true limit 1];
	}
	*/

	private static void createCompetitiveNews(){
		List<Corporate_News__c> cnews = new List<Corporate_News__c>{
			new Corporate_News__c(Name = 'Test 1', Description__c = 'Test 1', Type__c = 'Competitive News')
		};
		insert cnews;			
	}

	private static void createSpecs(){


		List<Specifications_Meta__c> specMetas = new List<Specifications_Meta__c> {
			new Specifications_Meta__c(Specifications_Meta_Name__c = 'General', isActive__c = true)
		};
		insert specMetas;

		List<Product_Meta__c> prodMetas = new List<Product_Meta__c> {
			new Product_Meta__c(Specifications_Meta__c = specMetas[0].Id, Product_Meta_Name__c = 'Type of MS', isActive__c = true)
		};
		insert prodMetas;

		List<Product_Value__c> prodValues = new List<Product_Value__c>();
		for(PricebookEntry testPBE : [select Product2Id from PricebookEntry where Pricebook2.Name = 'Test Price Book' order by ProductCode asc]) {
			Product_Value__c pv = new Product_Value__c(isActive__c = true, Product_Meta__c = prodMetas[0].id, Product__c = testPBE.Product2Id);
			prodValues.add(pv);
		}
		insert prodValues;
	}

	private static void createProductCategories(){
		Map<String, Product_Category__c> prodCatsMap = new Map<String,Product_Category__c> {
			'Mass Spectrometry' => new Product_Category__c(Product_Category_Name__c = 'Mass Spectrometry', isActive__c = true),
			'Software' => new Product_Category__c(Product_Category_Name__c = 'Software', isActive__c = true),
			'Service & Support' => new Product_Category__c(Product_Category_Name__c = 'Service & Support', isActive__c = true),
			'LC Separations' => new Product_Category__c(Product_Category_Name__c = 'LC Separations', isActive__c = true),
			'Analyzers' => new Product_Category__c(Product_Category_Name__c = 'Analyzers', isActive__c = true),
			'CE Separations' => new Product_Category__c(Product_Category_Name__c = 'CE Separations', isActive__c = true)
		};
		for(Product_Category__c pcat : [select Product_Category_Name__c from Product_Category__c where isActive__c = true]) {
			if(prodCatsMap.containsKey(pcat.Product_Category_Name__c)) prodCatsMap.remove(pcat.Product_Category_Name__c);
		}
		if(prodCatsMap.size() > 0) insert prodCatsMap.values();
	}
	
	private static void createTestPricebook(){
		Pricebook2 stdPricebook = [select Id from Pricebook2 where IsStandard = true limit 1];

		Pricebook2 pricebook = new Pricebook2(Name = 'Test Price Book', CurrencyIsoCode = 'USD', IsActive = true);
		insert pricebook; 
		
		Map<String, Product_Category__c> prodCatsMap = new Map<String,Product_Category__c>();
		for(Product_Category__c pcat : [select Product_Category_Name__c from Product_Category__c where isActive__c = true]) {
			prodCatsMap.put(pcat.Product_Category_Name__c, pcat);
		}


		List<Product2> testProducts = new List<Product2> {
			new Product2(Name = 'Test product 1', Family = 'Test product 1', ProductCode = 'test-1', IsActive = true, CurrencyIsoCode = 'USD', Product_Category__c = prodCatsMap.get('Mass Spectrometry').Id),
			new Product2(Name = 'Test product 2', Family = 'Test product 1', ProductCode = 'test-2', IsActive = true, CurrencyIsoCode = 'USD', Competitor_Manufacturer__c = 'Agilent', Product_Category__c = prodCatsMap.get('Mass Spectrometry').Id),
			new Product2(Name = 'Test product 3', Family = 'Test product 1', ProductCode = 'test-3', IsActive = true, CurrencyIsoCode = 'USD', Product_Category__c = prodCatsMap.get('Mass Spectrometry').Id),
			new Product2(Name = 'Test product 4', Family = 'Test product 1', ProductCode = 'test-4', IsActive = true, CurrencyIsoCode = 'USD', Competitor_Manufacturer__c = 'Agilent', Product_Category__c = prodCatsMap.get('Mass Spectrometry').Id),
			new Product2(Name = 'Test product 5', Family = 'Test product 1', ProductCode = 'test-5', IsActive = true, CurrencyIsoCode = 'USD', Product_Category__c = prodCatsMap.get('Mass Spectrometry').Id),
			new Product2(Name = 'Test product 6', Family = 'Test product 1', ProductCode = 'test-6', IsActive = true, CurrencyIsoCode = 'USD', Competitor_Manufacturer__c = 'Waters', Product_Category__c = prodCatsMap.get('Mass Spectrometry').Id)
			
		};
		insert testProducts;
		
		List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
		for(Product2 tp : testProducts) {
			pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
		}
		insert pricebookEntries;

		List<PricebookEntry> testPricebookEntries = new List<PricebookEntry>();
		for(Product2 tp : testProducts) {
			testPricebookEntries.add(new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
		}
		insert testPricebookEntries;
	}
	
	private static PricebookEntry getTestPricebookEntry(Integer idx) {
		List<PricebookEntry> pricebookEntries = [select Id, Product2Id, Product2.Name, Pricebook2Id, Product2.Competitor_Manufacturer__c from PricebookEntry where Pricebook2.Name = 'Test Price Book' order by ProductCode asc];
		if(pricebookEntries.size() > idx) {
			return pricebookEntries.get(idx);
		}else{
			return null;
		}
	}
}