/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Sep 11/2014
 **
 ** Test coverage for CSVStream and IncrementReportExport classes
**/
@isTest
public class ReportExportTests {
    public static testMethod void testScheduledExport(){
        new CSVStream().CSVStream();
        Test.startTest();
        
        ReportExport__c rexport = new ReportExport__c(Name = 'ThisReport', ReportExportTrigger__c = 0);
        insert rexport;

        datetime thisTime = system.now().addHours(24); 
        integer minute = thisTime.minute(); 
        integer second = thisTime.second(); 
        integer hour = thisTime.hour(); 
        integer year = thisTime.year(); 
        integer month = thisTime.month(); 
        integer day = thisTime.day(); 
        
        String timeStamp = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year; 
        string jobName = 'IncrementReportExport Job Test'; 
        
        IncrementReportExport iRes = new IncrementReportExport(); 
        system.schedule(jobName, timeStamp , iRes); 

        Test.stopTest();
    }
    
    
}