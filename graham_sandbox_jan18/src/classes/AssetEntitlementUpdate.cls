global class AssetEntitlementUpdate implements Database.Batchable<SObject> {
	
	global AssetEntitlementUpdate() {
		
	}
	
	/**
	 * @description gets invoked when the batch job starts
	 * @param context contains the job ID
	 * @returns the record set as a QueryLocator object that will be batched for execution
	 * https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/160792586/start
	 */ 
	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, Installed_Product__c, NEWSCIEXNow_Hardware_Entitled__c, NEWSCIEXNow_Software_Entitled__c, NEWSCIEXNOWSoftware_Upgrade_Entitled__c, Name FROM Asset');
	}

	/**
	 * @description gets invoked when the batch job executes and operates on one batch of records. Contains or calls the main execution logic for the batch job.
	 * @param context contains the job ID
	 * @param scope contains the batch of records to process.
	 * https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/160825367/execute
	 */ 
   	global void execute(Database.BatchableContext context, List<Asset> scope) {
		
		//loop through the scope and collect the Installed Product Ids
		Set<Id> insProductIds = new Set<Id>();
		for(Asset a : scope)
		{
			insProductIds.add(a.Installed_Product__c);
		}

		if(insProductIds.size() > 0)
		{
			//Create a Map with the Installed product Id as the key to avoid looping the whole 
			//query everytime we need a value.
			Map<Id, List<Product2>> conMap = createMap(insProductIds);
			List<Asset> updatedAsset = new List<Asset>();
			for(Asset a : scope)
			{
				if(conMap != null && conMap.containsKey(a.Installed_Product__c))
				{
					//get the list of service contract products from the query result map
					List<Product2> conProds = conMap.get(a.Installed_Product__c);
					System.debug('**GF** Contrats for asset ' + a.Name + ':' + conProds);
					Boolean hwEntitled = false;
					Boolean swEntitled = false;
					Boolean supEntitled = false;
					//loop through the results and if we have a positive result for entitlement
					// set the boolean to true
					for(Product2 sCon : conProds)
					{
						if(sCon.SCIEXNow_Hardware_Entitled__c)
						{
							hwEntitled = true;
						}
						if(sCon.SCIEXNow_Software_Entitled__c)
						{
							swEntitled = true;
						}
						if(sCon.SCIEXNOW_Software_Upgrade_Entitled__c)
						{
							supEntitled = true;
						}
					}
					//if any of the products showed entitlement set the entitlement checkbox
					//on the Asset and add to the collection for update.
					System.debug('**GF** Asset Entitlement Update:' + a.Name + ' || hwEntitled:' + hwEntitled + ' || swEntitled:' + swEntitled);
					if(hwEntitled != a.NEWSCIEXNow_Hardware_Entitled__c || swEntitled != a.NEWSCIEXNow_Software_Entitled__c || supEntitled != a.NEWSCIEXNOWSoftware_Upgrade_Entitled__c)
					{
						updatedAsset.add(new Asset(Id = a.Id, NEWSCIEXNow_Hardware_Entitled__c = hwEntitled, NEWSCIEXNow_Software_Entitled__c = swEntitled, NEWSCIEXNOWSoftware_Upgrade_Entitled__c = supEntitled));
					}
				}
				
			}

			System.debug('**GF** Asset to Update:' + updatedAsset);
			if(updatedAsset.size() > 0)
			{
				update updatedAsset;
			}
				
		}
	}

	/**
	* @description Builds a map of Installed Product Id and Collection of Products to avoid looping the query result everytime 
	* we want a value
	* @param insProductIds List of the productId's from the Assets in the scope of the batch 
	* @return map of Installed Product Id and Collection of Products
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/160858124/createMap
	*/ 
	global Map<Id, List<Product2>> createMap(Set<Id> insProductIds)
	{
		Map<Id, List<Product2>> rtrn = new Map<Id, List<Product2>>();
		List<SVMXC__Service_Contract_Products__c> contracts = [SELECT SVMXC__Installed_Product__c,
		SVC_Item_Master_Name__r.Name,  
		SVC_Item_Master_Name__r.SCIEXNow_Hardware_Entitled__c,
		SVC_Item_Master_Name__r.SCIEXNOW_Software_Upgrade_Entitled__c, 
		SVC_Item_Master_Name__r.SCIEXNow_Software_Entitled__c
		FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Installed_Product__c IN :insProductIds AND SVC_Contract_Applicability__c = 'Current' 
		ORDER BY SVMXC__Installed_Product__c];

		if(contracts.size() > 0)
		{
			//variables to hold the loop values whilst going through the query
			Id installedProdId;
			List<Product2> tempList;
		

			for(SVMXC__Service_Contract_Products__c sConn : contracts)
			{
				System.debug('**GF** Installed Product:' + installedProdId);
				
				System.debug('**GF** Contract Product:' + sConn.SVC_Item_Master_Name__r);
				if(!String.isEmpty(installedProdId) && installedProdId != sConn.SVMXC__Installed_Product__c && tempList.size() > 0)
				{
					System.debug('**GF** AddingContracts for:' + installedProdId);
					System.debug('**GF** Contracts added:' + tempList);
					rtrn.put(installedProdId,tempList);
					installedProdId = null;
				}
			
				if(String.isEmpty(installedProdId))
				{
					installedProdId = sConn.SVMXC__Installed_Product__c;
					tempList = new List<Product2>();
					tempList.add(sConn.SVC_Item_Master_Name__r);
				} else 
				{
					tempList.add(sConn.SVC_Item_Master_Name__r);
				}
			}

			//the last one is not added to the collection as we dont go back to the start of
			// the loop so add it here.
			if(rtrn != null && !rtrn.containsKey(installedProdId) && tempList.size() > 0)
			{
				rtrn.put(installedProdId,tempList);
			}
		}
		

		return rtrn;
	}
	
	/**
	 * @description gets invoked when the batch job finishes. Place any clean up code in this method.
	 * @param context contains the job ID
	 */ 
	global void finish(Database.BatchableContext context) {
		
	}
}