public class AbDebugUtils {

    
    //============================================================================================
    //========================= Verbose DEBUG Messages ===========================================
    //============================================================================================
 
    //---- DEBUG - print all returned pricing details
    public static void debugPriceDetails(QuoteSupplementalServices.detailOfLineItems[] arrayOfUpdatedLineItems){
     try{
     /*
      for(QuoteSupplementalServices.detailOfLineItems nextPriceDetail : arrayOfUpdatedLineItems){
       
        System.debug('QUOTING: ------- NEXT PRICE DETAIL ------------------- ');
        System.debug('QUOTING: price detail- lineItemNumber=' + nextPriceDetail.lineItemNumber);
        System.debug('QUOTING: price detail- lineItemType=' + nextPriceDetail.lineItemType);
        System.debug('QUOTING: price detail- lineItemSequenceNumber=' + nextPriceDetail.lineItemSequenceNumber);
        System.debug('QUOTING: price detail- quantity=' + nextPriceDetail.quantity);
        System.debug('QUOTING: price detail- TID=' + nextPriceDetail.TID);
        System.debug('QUOTING: price detail- lstPrice=' + nextPriceDetail.lstPrice);
        System.debug('QUOTING: price detail- mPrice=' + nextPriceDetail.mPrice);
        System.debug('QUOTING: price detail- discountLevel=' + nextPriceDetail.discountLevel);
        System.debug('QUOTING: price detail- approvedDiscountAmount=' + nextPriceDetail.approvedDiscountAmount);
        System.debug('QUOTING: price detail- manualDiscountPercentage=' + nextPriceDetail.manualDiscountPercentage);
        System.debug('QUOTING: price detail- manualDiscountAmount=' + nextPriceDetail.manualDiscountAmount);
        System.debug('QUOTING: price detail- manualDiscountValuePerUnit=' + nextPriceDetail.manualDiscountValuePerUnit);
        System.debug('QUOTING: price detail- manualPriceOverride_CustPrc=' + nextPriceDetail.manualPriceOverride_CustPrc);
        System.debug('QUOTING: price detail- headerDiscountValue_Hdr=' + nextPriceDetail.headerDiscountValue_Hdr);
        System.debug('QUOTING: price detail- itemNetValue_NetPrc=' + nextPriceDetail.itemNetValue_NetPrc);
        System.debug('QUOTING: price detail- freight=' + nextPriceDetail.freight);
        System.debug('QUOTING: price detail- fees=' + nextPriceDetail.fees);
        System.debug('QUOTING: price detail- tax=' + nextPriceDetail.tax);

        System.debug('QUOTING: price detail- productNumber=' + nextPriceDetail.productNumber);
        System.debug('QUOTING: price detail- productCategory=' + nextPriceDetail.productCategory);
        System.debug('QUOTING: price detail- IGORCode=' + nextPriceDetail.IGORCode);
        System.debug('QUOTING: price detail- planCode=' + nextPriceDetail.planCode);
        System.debug('QUOTING: price detail- description=' + nextPriceDetail.description);
        System.debug('QUOTING: price detail- newDescription=' + nextPriceDetail.newDescription);
        System.debug('QUOTING: price detail- productLine=' + nextPriceDetail.productLine);
        System.debug('QUOTING: price detail- IGORItemClass=' + nextPriceDetail.IGORItemClass);
        System.debug('QUOTING: price detail- lineItemText=' + nextPriceDetail.lineItemText);
        System.debug('QUOTING: price detail- templateType=' + nextPriceDetail.templateType);
        System.debug('QUOTING: price detail- materialOverrideIndicator=' + nextPriceDetail.materialOverrideIndicator);    
         
      } 
     */ 
     }
     catch(Exception e){
     }
    }

    public static void debugPriceInput(QuoteSupplementalServices.priceInputDoc priceInput){
     try{
      System.debug('QUOTING: priceInput.refreshApproval='+ priceInput.refreshApproval);
      System.debug('QUOTING: priceInput.salesOrg='+ priceInput.salesOrg);
      System.debug('QUOTING: priceInput.division='+  priceInput.division);
      System.debug('QUOTING: priceInput.salesDocType='+ priceInput.salesDocType);
      System.debug('QUOTING: priceInput.soldTo='+  priceInput.soldTo);
      System.debug('QUOTING: priceInput.payer='+ priceInput.payer);
      System.debug('QUOTING: priceInput.billTo='+ priceInput.billTo);
      System.debug('QUOTING: priceInput.dateForPricingAndexchangeRate='+ priceInput.dateForPricingAndexchangeRate);
      System.debug('QUOTING: priceInput.incoTerms='+ priceInput.incoTerms);
      System.debug('QUOTING: priceInput.shipTo='+ priceInput.shipTo);
      System.debug('QUOTING: priceInput.hdrDiscAmnt='+ priceInput.hdrDiscAmnt);
     
      //priceInput.numberOfContactPerson = '401162'; 
      //priceInput.priceListType = 'tstest';  //n/a
      //priceInput.shippingConditions = 'tstest';
      //priceInput.documentNmbrRefDocument = 'tstest';
      //priceInput.custOrdrMethod = 'tstest';
      //priceInput.hdrDiscAmnt = localQuote.Header_Discount__c;
      //priceInput.hdrDiscPctg = 'tstest';
      //priceInput.currency - MISSING FROM WSDL

      List<QuoteSupplementalServices.T_ITEMS> tItemsObject =  priceInput.T_ITEMS.ArrayOfT_ITEMSItem;
      
      for (QuoteSupplementalServices.T_ITEMS nextLI : tItemsObject){
        System.debug('QUOTING: line item nextLI.itemNumber=' + nextLI.lineItemNumber);
        System.debug('QUOTING: line item nextLI.productID=' +  nextLI.productNumber);
        System.debug('QUOTING: line item nextLI.quantity=' + nextLI.quantity);
        System.debug('QUOTING: line item nextLI.approvedDiscountAmount=' + nextLI.approvedDiscountAmount);
        System.debug('QUOTING: line item nextLI.mprice=' + nextLI.mPrice);
        System.debug('QUOTING: line item nextLI.manualPriceOverride_CustPrc=' + nextLI.manualPriceOverride_CustPrc);
        System.debug('QUOTING: line item nextLI.manualDiscountPercentage=' + nextLI.manualDiscountPercentage);
        System.debug('QUOTING: line item nextLI.manualDiscountAmount=' + nextLI.manualDiscountAmount);
        System.debug('QUOTING: line item nextLI.manualDiscountValuePerUnit=' + nextLI.manualDiscountValuePerUnit);
        System.debug('QUOTING: line item nextLI.headerDiscountValue_Hdr=' + nextLI.headerDiscountValue_Hdr);
        System.debug('QUOTING: line item nextLI.itemNetValue_NetPrc=' + nextLI.itemNetValue_NetPrc);

      }  
     }
     catch(Exception e){
     }
    }

    
    //================= DEBUG ==================================================
    public static void debugQuoteSearchResults(QuoteSearchServices.listOfQuotes[] quoteList){
     try{
      for (QuoteSearchServices.listOfQuotes nextQuote : quoteList ){
      
        System.debug('QUOTING: ====================');
        System.debug('QUOTING: CDIQuoteNumber =' + nextQuote.CDIQuoteNumber);
        System.debug('QUOTING: accountID =' + nextQuote.accountID);
        System.debug('QUOTING: quoteDescription =' + nextQuote.quoteDescription);
        System.debug('QUOTING: createDate =' + nextQuote.createDate);
        System.debug('QUOTING: status =' + nextQuote.status);
        System.debug('QUOTING: quoteOwner =' + nextQuote.quoteOwner);
        System.debug('QUOTING: department =' + nextQuote.department);
        System.debug('QUOTING: zipCode =' + nextQuote.zipCode);
        System.debug('QUOTING: ====================');
        
      }
     }
     catch(Exception e){
     }
    }         


    public static void printSalesQuoteDebug(QuoteServices.SalesQuote quoteToPrint){
      System.debug('QUOTING-salesquote: quoteHeader ' + quoteToPrint.quoteHeader);
      System.debug('QUOTING-salesquote: quoteHeader cdiQuoteNumber ' + quoteToPrint.quoteHeader.cdiQuoteNumber);
      System.debug('QUOTING-salesquote: quoteHeader sapQuoteNumber ' + quoteToPrint.quoteHeader.sapQuoteNumber);
      System.debug('QUOTING-salesquote: quoteHeader salesOrg ' + quoteToPrint.quoteHeader.salesOrg);
      System.debug('QUOTING-salesquote: quoteHeader region ' + quoteToPrint.quoteHeader.region);
      System.debug('QUOTING-salesquote: quoteHeader district_country ' + quoteToPrint.quoteHeader.district_country);
      System.debug('QUOTING-salesquote: quoteHeader quoteDescription ' + quoteToPrint.quoteHeader.quoteDescription);
      System.debug('QUOTING-salesquote: quoteHeader shortDescription ' + quoteToPrint.quoteHeader.shortDescription);
      
      System.debug('QUOTING-salesquote: partnerData ' + quoteToPrint.partnerData);
      if(quoteToPrint.partnerData != null
         && quoteToPrint.partnerData.partnerInfo != null
         && quoteToPrint.partnerData.partnerInfo.ArrayOfpartnerInfoItem != null){
        for(QuoteServices.partnerInfo anotherPartner : quoteToPrint.partnerData.partnerInfo.ArrayOfpartnerInfoItem){
          System.debug('QUOTING-salesquote: partnerType ' + anotherPartner.partnerType);
          System.debug('QUOTING-salesquote: partnerID ' + anotherPartner.partnerID);
        }        
      }
      
      System.debug('QUOTING-salesquote: termsAndConditions ' + quoteToPrint.termsAndConditions);
      System.debug('QUOTING-salesquote: lineItemDetails ' + quoteToPrint.lineItemDetails);
      System.debug('QUOTING-salesquote: attachmentDetails ' + quoteToPrint.attachmentDetails);
    }

    public static void printSalesQuoteLIDebug(QuoteServices.SalesQuote quoteToPrint){
     /*
      for(QuoteServices.lineItemDetails li :  quoteToPrint.lineItemDetails.ArrayOflineItemDetailsItem){
      
        System.debug('QUOTING-salesquote: =================================');
        System.debug('QUOTING-salesquote: ========== LINE ITEM ============');
        System.debug('QUOTING-salesquote: ==================================');
        System.debug('QUOTING-salesquote: li.lineItemNumber = ' + li.lineItemNumber);
        System.debug('QUOTING-salesquote: li.alt_To_Item = ' + li.alt_To_Item);
        System.debug('QUOTING-salesquote: li.optionalFlag = ' + li.optionalFlag);
        System.debug('QUOTING-salesquote: li.productNumber = ' + li.productNumber);
        System.debug('QUOTING-salesquote: li.productCategory = ' + li.productCategory);
        System.debug('QUOTING-salesquote: li.IGORCode = ' + li.IGORCode);
        System.debug('QUOTING-salesquote: li.planCode = ' + li.planCode);
        System.debug('QUOTING-salesquote: li.IGORItemClass = ' + li.IGORItemClass);
        System.debug('QUOTING-salesquote: li.lineItemText = ' + li.lineItemText);
        System.debug('QUOTING-salesquote: li.productLine = ' + li.productLine);
        System.debug('QUOTING-salesquote: li.lineItemSequenceNumber = ' + li.lineItemSequenceNumber);
        System.debug('QUOTING-salesquote: li.discountLevel = ' + li.discountLevel);
        System.debug('QUOTING-salesquote: li.description = ' + li.description);
        System.debug('QUOTING-salesquote: li.newDescription = ' + li.newDescription);
        System.debug('QUOTING-salesquote: li.quantity = ' + li.quantity);
        System.debug('QUOTING-salesquote: li.measuringUnit = ' + li.measuringUnit);
        System.debug('QUOTING-salesquote: li.TID = ' + li.TID);
        System.debug('QUOTING-salesquote: li.lstPrice = ' + li.lstPrice);
        System.debug('QUOTING-salesquote: li.mPrice = ' + li.mPrice);
        System.debug('QUOTING-salesquote: li.approvedDiscountAmount = ' + li.approvedDiscountAmount);
        System.debug('QUOTING-salesquote: li.manualDiscountPercentage = ' + li.manualDiscountPercentage);
        System.debug('QUOTING-salesquote: li.manualDiscountAmount = ' + li.manualDiscountAmount);
        System.debug('QUOTING-salesquote: li.manualDiscountValuePerUnit = ' + li.manualDiscountValuePerUnit);
        System.debug('QUOTING-salesquote: li.manualPriceOverride_CustPrc = ' + li.manualPriceOverride_CustPrc);
        System.debug('QUOTING-salesquote: li.headerDiscountValue_Hdr = ' + li.headerDiscountValue_Hdr);
        System.debug('QUOTING-salesquote: li.itemNetValue_NetPrc = ' + li.itemNetValue_NetPrc);
        System.debug('QUOTING-salesquote: li.freight = ' + li.freight);
        System.debug('QUOTING-salesquote: li.fees = ' + li.fees);
        System.debug('QUOTING-salesquote: li.tax = ' + li.tax);
        System.debug('QUOTING-salesquote: li.templateType = ' + li.templateType);
        System.debug('QUOTING-salesquote: li.productDescriptionOverride = ' + li.productDescriptionOverride);      
      } 
     */
    }
 

}