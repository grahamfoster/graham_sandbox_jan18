/*
 *	WorkOrderTrigger_Test
 *	
 *	Test class for WorkOrderTrigger and WorkOrderTriggerHandler.
 *
 *	If there are other test classes related to WorkOrderTrigger, please document it here (as comments).
 * 
 * 	Created by Brett Moore 2016-05-20 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *  Brett Moore 2016-12-08 - Added findZonePriceTest() and updateCaseDatesTest()
 */
@isTest
public class WorkOrderTrigger_Test {
	// Prepare initial sample data for running tests:
    @testSetup static void setup() {		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
        insert u;         
                Pricebook2 pb = new Pricebook2 (Name='United States Price Book 2',CURRENCYISOCODE='USD',isActive=TRUE);
		insert pb;
        Country_Mapping__c countryM = new Country_Mapping__c(Name='United States',Permutations__c='UNITED STATES; USA; US; UNITED STATES OF AMERICA',
                Country_Code__c='US',Country__c='United States',Support_Region__c='AMERICAS',Default_Service_Price_Book__c=pb.id);
        insert countryM;
 
    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'United States');
        insert testAccount;
        
		Product2 prod = new Product2(Name='ABSX WARRANTY 2',ProductCode='ABSX WARRANTY 2');
        insert prod;
         
		SVMXC__Installed_Product__c comp = New SVMXC__Installed_Product__c ( NAME='API3000 - AF28211410',
                                                                            CURRENCYISOCODE ='USD',	
                                                                            SVMXC__COMPANY__C=testAccount.Id,	
                                                                            SVMXC__PRODUCT__C=prod.id,	
                                                                            SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', 
																			SVC_Service_Zone__c='Zone 1',
                                                                            SVC_Operating_Unit__c = 'SX US OU');        
        insert comp;

        List <workOrderFSRemailNotifications__c> testWON = new List<workOrderFSRemailNotifications__c>{
            new workOrderFSRemailNotifications__c(email_1__c = 'Test@test.com',email_2__c = 'Test@test.com',SEP_Email__c= 'Test@test.com', Name = 'US',Operating_Unit__c='SX US OU', Country__c='United States'),
            new workOrderFSRemailNotifications__c(email_1__c = 'Test@test.com',email_2__c = 'Test@test.com',SEP_Email__c= 'Test@test.com', Name = 'US2',Operating_Unit__c='SX US OU', Country__c= '') };
        insert testWON;

        
        SVMXC__Site__c site = new SVMXC__Site__c(Name = 'Test Location',SVMXC__Account__c = testAccount.id,SVMXC__Stocking_Location__c = true,
                                SVMXC__State__c = 'NY',SVMXC__Service_Engineer__c = u.Id,SVMXC__Country__c = 'United States',
                                SVMXC__Zip__c = '12345');
        insert site;         
         
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c(
                                                    Name = 'ABC',
                                                    SVMXC__Active__c = true,
                                                    SVMXC__State__c = 'NY',
                                                    SVMXC__Country__c = 'United States',
                                                    SVMXC__Zip__c = '12345');
        insert serviceTeam;
         
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(
                                                        SVMXC__Active__c = true,
                                                        Name = 'Test Technician',
                                                        SVMXC__Service_Group__c = serviceTeam.Id,
                                                        SVMXC__Salesforce_User__c = u.Id,
                                                        SVMXC__Inventory_Location__c = site.Id);
        insert technician;

     
        RecordType rt = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
        Case ccase = new Case(RecordTypeId = rt.Id, AccountId = testAccount.Id, CurrencyIsoCode = 'USD', SVMXC__Component__c = comp.Id);
        insert ccase;
		SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(CurrencyIsoCode = 'CAD', SVMXC__Case__c = ccase.Id, SVC_Problem_Code__c = 'Electrical', SVC_Resolution_Code__c = 'Replaced Part(s)', SVC_Resolution_Summary__c = 'abc',SVMXC__Service_Group__c=serviceTeam.Id);
        insert wo;
        SVMXC__Service_Order_Line__c wDetail = new SVMXC__Service_Order_Line__c(SVMXC__Service_Order__c=wo.Id, SVMXC__End_Date_and_Time__c=datetime.now(), SVMXC__Start_Date_and_Time__c = datetime.now());
        insert wDetail;
    }
     @isTest  static void IUDU_Test() {

        Case ccase = [SELECT Id, RecordTypeId, AccountId , CurrencyIsoCode, SVMXC__Component__c FROM Case LIMIT 1];
		SVMXC__Service_Group__c serviceTeam = [SELECT Name, SVMXC__Active__c, SVMXC__State__c, SVMXC__Country__c, SVMXC__Zip__c FROM SVMXC__Service_Group__c LIMIT 1];         
		
        test.startTest();        
        	SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(CurrencyIsoCode = 'CAD', SVMXC__Case__c = ccase.Id, SVC_Problem_Code__c = 'Electrical', SVC_Resolution_Code__c = 'Replaced Part(s)', SVC_Resolution_Summary__c = 'abc',SVMXC__Service_Group__c=serviceTeam.Id);
        	insert wo;
        	update wo;
        	delete wo;
        	undelete wo;
		test.stopTest();        
     }
    
     @isTest  static void populateData_Test() {    
 		 SVMXC__Service_Order__c wo = [SELECT CurrencyIsoCode, SVMXC__Case__c, SVC_Problem_Code__c, SVC_Resolution_Code__c, SVC_Resolution_Summary__c, SVMXC__Service_Group__c,SVMXC__Order_Status__c FROM SVMXC__Service_Order__c LIMIT 1];        
         test.startTest();        
        	wo.SVC_Problem_Code__c = 'System Performance';
         	wo.SVC_Resolution_Code__c = 'Software bug';
         	wo.SVC_Resolution_Summary__c = 'new summary';
         	wo.SVMXC_Scheduled_Date_c__c = Date.today();
         	wo.SVC_Scheduled_End__c = Date.today();
        	update wo;
		test.stopTest(); 
     }
    
     @isTest  static void FSR_Test() {     
		SVMXC__Service_Group__c serviceTeam = [SELECT Name, SVMXC__Active__c, SVMXC__State__c, SVMXC__Country__c, SVMXC__Zip__c FROM SVMXC__Service_Group__c LIMIT 1];         
	 	SVMXC__Service_Order__c wo = [SELECT CurrencyIsoCode, SVMXC__Case__c, SVC_Problem_Code__c, SVC_Resolution_Code__c, SVC_Resolution_Summary__c, SVMXC__Service_Group__c,SVMXC__Order_Status__c FROM SVMXC__Service_Order__c LIMIT 1];        
        SVMXC__Service_Group_Members__c technician = [SELECT SVMXC__Active__c, Name, SVMXC__Service_Group__c, SVMXC__Salesforce_User__c, SVMXC__Inventory_Location__c FROM SVMXC__Service_Group_Members__c LIMIT 1];
     
         test.startTest();        
			wo.SVC_Operating_Unit__c = 'SX US OU';
         	wo.SVMXC__Group_Member__c = technician.id;
        	update wo;
         	serviceTeam.Name = 'SEP';
         	update serviceTeam;
         	update wo;
         	wo.SVMXC__Country__c ='XXX';
         	update wo;
       		serviceTeam.Name = 'WEST';
			update serviceTeam;         
         	update wo;
         test.stopTest(); 
     }

    
    @isTest  static void findZonePriceTest() {
        
     
    }
    
	@isTest  static void updateCaseDatesTest() {
        
		SVMXC__Service_Order__c wo = [SELECT CurrencyIsoCode, SVMXC__Case__c, SVC_Problem_Code__c, SVC_Resolution_Code__c, SVC_Resolution_Summary__c, SVMXC__Service_Group__c,SVMXC__Order_Status__c FROM SVMXC__Service_Order__c LIMIT 1];
        
        test.startTest();        
        	
        	wo.SVMXC__Order_Status__c = 'Completed';
			update wo;
		test.stopTest();  
    }  
    
    @isTest static void ruleToEnsureAssignedTechIsWoOwner(){
                
        SVMXC__Service_Group_Members__c tech = [SELECT id, SVMXC__Salesforce_User__r.Id FROM SVMXC__Service_Group_Members__c LIMIT 1];
        SVMXC__Service_Order__c wo = [SELECT OwnerId, SVMXC__Group_Member__c FROM SVMXC__Service_Order__c WHERE OwnerId != : tech.SVMXC__Salesforce_User__r.Id LIMIT 1];
        
        test.startTest();
        wo.SVMXC__Group_Member__c = tech.Id;
        update wo;
        test.stopTest();
    }  

}