public class StatusScopeAlarm {

	public static StatusScopeAlarm parse(String json) {
		return new StatusScopeAlarm(System.JSON.createParser(json));
	}

	public String alarm_description {get;set;} 
	public String alarm_name {get;set;} 
	public String alarm_severity {get;set;} 
	public String alarm_type {get;set;} 
	public String serial_number {get;set;} 
	public String contact {get;set;} 
	public String asset_id {get;set;}

		public static void consumeObject(JSONParser parser) {
		Integer depth = 0;
		do {
			JSONToken curr = parser.getCurrentToken();
			if (curr == JSONToken.START_OBJECT || 
				curr == JSONToken.START_ARRAY) {
				depth++;
			} else if (curr == JSONToken.END_OBJECT ||
				curr == JSONToken.END_ARRAY) {
				depth--;
			}
		} while (depth > 0 && parser.nextToken() != null);
	}


	public StatusScopeAlarm(JSONParser parser) {
		while (parser.nextToken() != JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != JSONToken.VALUE_NULL) {
					if (text == 'alarm_description') {
						alarm_description = parser.getText();
					} else if (text == 'alarm_name') {
						alarm_name = parser.getText();
					} else if (text == 'alarm_severity') {
						alarm_severity = parser.getText();
					} else if (text == 'alarm_type') {
						alarm_type = parser.getText();
					} else if (text == 'serial_number') {
						serial_number = parser.getText().trim();
                        serial_number = serial_number.replace(' SQ','');
						serial_number = serial_number.replace(' GW','');
						serial_number = serial_number.replace(' LC','');
						serial_number = serial_number.replace(' WS','');
					} else if (text == 'contact') {
						contact = parser.getText();
					} else if (text == 'asset_id') {
						asset_id = parser.getText();
					} else {
						System.debug(LoggingLevel.WARN, 'Root consuming unrecognized property: '+text);
						consumeObject(parser);
					}
				}
			}
		}
	}
}