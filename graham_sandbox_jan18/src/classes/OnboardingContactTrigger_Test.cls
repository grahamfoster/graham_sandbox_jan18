/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 10/2016
**
** Test coverage for OnboardingContactTrigger trigger logic
** Trigger also covered in AbsorbTests and CaseCloneTests
**/
@isTest
public class OnboardingContactTrigger_Test 
{
	@testSetup
	static void setUp() {
		AbsorbLMSSettings__c settings = AbsorbLMSSettings__c.getOrgDefaults();
		settings.Endpoint_URL__c = 'http://example.com';
		settings.API_Username__c = 'user';
		settings.API_Password__c = 'pass';
		settings.API_Token_Timeout__c = 240;
		upsert settings;
	}

	@isTest
	public static void test() 
	{
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cob;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Type__c = 'Primary Learner');
		Test.startTest();
		insert obc;
		update obc;
		delete obc;
		undelete obc;		
	}
}