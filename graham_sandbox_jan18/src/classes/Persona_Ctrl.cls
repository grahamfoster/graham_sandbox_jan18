public with sharing class Persona_Ctrl
{
    Public Persona__c personaRecord{get;set;}
    Public Boolean isError{get;set;}
    public Persona_Ctrl(ApexPages.StandardController controller)
    {
        isError = true;
        personaRecord = (Persona__c)Controller.getRecord();            
    }
    public PageReference doSaveAndNew() {        
        try{
            isError = true;
            insert personaRecord;
            return new pagereference('/apex/persona');
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }   
        return null;
    }
    
    
    public PageReference doSave(){
        try {
            isError = true;
            
            if(personaRecord.Persona_Name__c != null && personaRecord.Persona_Name__c != '')
            {
                upsert personaRecord;
                isError = false;
            }
            else
             {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Enter Persona Name'));
                
                isError = true;
             }
            
            /*if(personaRecord.Id != null || (personaRecord.Id) != '')
            return new PageReference('/'+personaRecord.Id);*/           
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));     
            isError = true;       
        }
        return null;
    }
    public PageReference doCancel(){
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
            return new PageReference (cancelURL);
        }
        return null;
    }
    
    public void doDelete(){
        if(personaRecord != null && personaRecord.Id != null){
            try{
                delete personaRecord;} catch(Exception e){ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));    
            }  
         } 
    } 
 }