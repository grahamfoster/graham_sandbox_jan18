/**
* @author graham.foster@sciex.com
* @description webservice invoked by JS button on training Case for the user to make a second case if the order contained more that one training part 
*/ 
global class case_splitTrainingCase {

	/**
	* @description The webservice called by the JS on the custom button on training case layout
	* @param parentCaseId the Id of the originating case
	* @return the Id of the new case that has been created
	*/ 
	webservice static String createTheCase(String parentCaseId)
	{
		if(!String.isEmpty(parentCaseId))
		{
			String caseId = splitTrainingCase(parentCaseId);
			return caseId;
		}
		return null;
	}

	/**
	* @description Performs the split/creation of the second training using the field values from the originating case but the Additional training product 
	* @param parentCaseId The Id of the originating case
	* @return The Id of the new case
	*/ 
	public static String splitTrainingCase(String parentCaseId)
	{
		List<Case> lstCases = [SELECT Id, ContactId, Customer_Onboard__c, Origin, Oracle_Order__c, 
								Installation_Case__c, Type, AssetId, Additional_Training_Product__c, 
								Additional_Training_Product__r.Name, Additional_Training_Product__r.Description  
								FROM Case WHERE Id = :parentCaseId LIMIT 1];
		
		if(lstCases.size() > 0)
		{
			Case originCase = lstCases[0];
			Case newCase = new Case(
			RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Training').getRecordTypeId(),
			ContactId = originCase.ContactId,
			Customer_Onboard__c = originCase.Customer_Onboard__c,
			Origin = originCase.Origin,
			Oracle_Order__c = originCase.Oracle_Order__c,
			Installation_Case__c = originCase.Installation_Case__c,
			Type = originCase.Type,
			AssetId = originCase.AssetId,
			Training_Part__c = originCase.Additional_Training_Product__c,
			ParentId = originCase.Id,
			Subject = originCase.Additional_Training_Product__r.Name,
			Description = originCase.Additional_Training_Product__r.Description
			);
			//set the original additional training case to null
			originCase.Additional_Training_Product__c = null;
			UPDATE originCase;
			Database.DMLOptions dmo = new Database.DMLOptions();
			dmo.assignmentRuleHeader.useDefaultRule = true;
			Database.insert(newCase, dmo);
			createCaseContacts(parentCaseId, newCase);
			return newCase.Id;
		}
		return null;
	} 

	/**
	* @description Copies the Case contacts that are present on the originating case by creating new ones one the child case
	* @param parentCaseId the Originating case (to query the existing case contacts)
	* @param newCase the new case which has been created by splitTrainingCase
	*/ 
	public static void createCaseContacts(Id parentCaseId, Case newCase)
	{
		List<Case_Contact__c> contacts = [SELECT Contact__c, Contact_Email__c FROM Case_Contact__c WHERE Case__c = :parentCaseId];
		System.debug('**GF** Original Case Contacts:' + contacts);
		List<Case_Contact__c> newContacts = new List<Case_Contact__c>();
		for(Case_Contact__c cc : contacts)
		{
			newContacts.add(new Case_Contact__c(
			Case__c = newCase.Id,
			Contact__c = cc.Contact__c
			));
		}
		if(newContacts.size() > 0)
		{
			System.debug('**GF** newContacts:' + newContacts);
			insert newContacts;
		}
	}

}