/*********************************************************************
Name  : testQuotingController
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuotingController     

*********************************************************************/

public class testQuotingController{

public static testMethod void checkQuotingController() {
  try{
    Test.setCurrentPageReference(new PageReference('Page.QuoteProductSearch'));
    Quote__c Quote = new Quote__c(Name = 'QuoteName');
    insert Quote;
    ApexPages.currentPage().getParameters().put('quoteId',Quote.Id);
    ApexPages.currentPage().getParameters().put('opt','true');

    QuotingController myController = new QuotingController();  
    string str = '';
    str = myController.getTemplateID();
    str = myController.getTemplateName();
    str = myController.getTemplateDescription();
    str = myController.getPromoCode();
    str = myController.getSelectedTemplateId();
    myController.setTemplateClassList(myController.getTemplateClassList());
    myController.getSelectedSAPQuoteNum ();    
    myController.setSelectedSAPQuoteNum('');
    myController.setTemplateID('');
    myController.setTemplateName('');
    myController.setTemplateDescription('');
    myController.setPromoCode('');
    myController.setSelectedTemplateId('');
    List<Quote_Line_Item__c> lst2 = myController.getProductResults();
    myController.setErrorMessage('');
    str = myController.getErrorMessage();
    myController.setProduct('');
    str = myController.getProduct();
    myController.setProductCategory('');
    str = myController.getProductCategory();
    myController.setProductDesciption('');
    str = myController.getProductDesciption();
    myController.setProductHierarchyCriteria('');
    str = myController.getProductHierarchyCriteria();
    myController.setIGORCodeDescValue('');
    str = myController.getIGORCodeDescValue();
    myController.setPlanCodeDescValue('');
    str = myController.getPlanCodeDescValue();
    myController.setIGORItemClassValue('');
    str = myController.getIGORItemClassValue();
    myController.setProductLineDescValue('');
    str = myController.getProductLineDescValue();
    myController.setIGORItemClassDescValue('');
    str = myController.getIGORItemClassDescValue();
    myController.setPlanCodeValue('');
    str = myController.getPlanCodeValue();
    myController.setIGORCodeValue('');
    str = myController.getIGORCodeValue();
    myController.setIGORCodeHiddenValue('');
    str = myController.getIGORCodeHiddenValue();
    myController.setIGORCodeDescHiddenValue('');
    str = myController.getIGORCodeDescHiddenValue();
    myController.setProductLineValue('');
    str = myController.getProductLineValue();
    List<Quote_Line_Item__c> lstQLI = new List<Quote_Line_Item__c>();
    myController.setProductResults(lstQLI );
    lstQLI = myController.getProductResults();
    str = myController.getCdiUsername();
    myController.setCdiUsername('');
    str = myController.getCdiPassword();
    myController.setCdiPassword('');
    myController.setFilter_Account('');
    str = myController.getFilter_Account();
    myController.setFilter_Contact('');
    str = myController.getFilter_Contact();
    myController.setFilter_soldTo('');
    str = myController.getFilter_soldTo();
    myController.setFilter_ShortDesc('');
    str = myController.getFilter_ShortDesc();
    myController.setFilter_CDIQuote('');
    str = myController.getFilter_CDIQuote();
    myController.setFilter_SAPQuote('');
    myController.setFilter_QuoteStatusQLP('');
    str = myController.getFilter_SAPQuote();
    str = myController.getFilter_QuoteStatusQLP();
    myController.setFilter_Product('');
    str = myController.getFilter_Product();
    myController.setFilter_ProductCategory('');
    str = myController.getFilter_ProductCategory();
    myController.setFilter_ProductLine('');
    str = myController.getFilter_ProductLine();
    myController.setFilter_PlanCode('');
    str = myController.getFilter_PlanCode();
    myController.setFilter_IGORItemClass('');
    str = myController.getFilter_IGORItemClass();
    myController.setFilter_IGORCode('');
    str = myController.getFilter_IGORCode();
    myController.setFilter_Template('');
    str = myController.getFilter_Template();
    str = myController.getSelectedQuoteId();
    myController.setSelectedQuoteId(null);
    str = myController.getSelectedCDIQuoteNum();
    myController.setSelectedCDIQuoteNum('');
    List<SelectOption> lstSelectOption = myController.getFilter_QuoteStatus();
    
    // ********** Following methods thrown System.Exception: Too many query rows: 501 ***********
    //lstSelectOption = myController.getproductLine();
    //lstSelectOption = myController.getproductLineDesc();
    //lstSelectOption = myController.getProdCategory_List();
    //lstSelectOption = myController.getplanCode();
    //lstSelectOption = myController.getplanCodeDesc();
    //lstSelectOption = myController.getiGORItemClass();
    //lstSelectOption = myController.getiGORItemClassDesc();
    //lstSelectOption = myController.getiGORCode();
    //lstSelectOption = myController.getiGORCodeDesc();
    // *****************************************************************************************
    
    List<Quote__c>  lst3 = myController.getResult();
    myController.setQuoteId('');
    str = myController.getQuoteId();
    Boolean bln = myController.getImportProducts();
    bln = myController.getShowingLocalQuotes();
    myController.setIGORCodeValue('AAAAA');
    myController.getIGORCodeValue();
    myController.getiGORCode();
    PageReference pgRef = null;
    pgRef = myController.loginToCDI();
    pgRef = myController.searchCDIProducts();
    pgRef = myController.addProductsToQuote();
    pgRef = myController.goToProductHierarchyScreen();
    pgRef = myController.returnCodeToProductSearch();
    pgRef = myController.searchProductHierarchy();
    pgRef = myController.quoteSearchCDI();
    pgRef = myController.quoteSearchReset();
    pgRef = myController.doSorting();
    pgRef = myController.viewQuote();
    pgRef = myController.cancelQuote();
    pgRef = myController.viewSAPQuote();
    myController.getUserSelection();
    List<Product_Hierarchy__c> lst45 = myController.getSearchResults();
    pgRef = myController.editExistingQuote();
    pgRef = myController.AddProductsFromQuote();
    pgRef = myController.CancelProductsFromQuote();
    pgRef = myController.CloneQuote();
    str = myController.getOriginalId();
    myController.CloneProducts('','');
    pgRef = myController.QuoteSearchSAP();
    pgRef = myController.QuoteNewHeader();
    pgRef = myController.QuoteNewPartner();
    pgRef = myController.QuoteNewOptions();
    pgRef = myController.QuoteProductSelect();
    pgRef = myController.QuoteProductDiscount();
    pgRef = myController.QuoteProductSort();
    pgRef = myController.QuoteProductSearch();
    pgRef = myController.QuoteTemplateSearch();
    pgRef = myController.QuoteMaterials();
    pgRef = myController.QuoteReturnLog();
    pgRef = myController.ImportTemplates();
    pgRef = myController.createNewQuote();
    pgRef = myController.getSelectedTemplate();
    pgRef = myController.FindListTemplates();
    QuotingController.listOfTemplatesInnerClass  cls12 = new QuotingController.listOfTemplatesInnerClass();
    str = cls12.getTemplateID();
    cls12.setTemplateID(str);
    str = cls12.getTemplateName();
    cls12.setTemplateName(str);
    str = cls12.getTemplateDescription();
    cls12.setTemplateDescription(str);
    str = cls12.getTemplateType();
    cls12.setTemplateType(str);
    str = cls12.getPromoCode();
    cls12.setPromoCode(str);
    str = cls12.getCurrency_x();
    cls12.setCurrency_x(str);
    Date dt1 = cls12.getQuoteValidityDate();
    cls12.setQuoteValidityDate(dt1);
    dt1 = cls12.getValidToDate();
    cls12.setValidToDate(dt1);
    str = cls12.getOutputMessage();
    cls12.setOutputMessage(str);
  }
  catch(Exception e){}
}  

   public static testMethod void checkQuotingController2() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'CDI_Quote_Num__c';
     myController.showLocalDraftQuotes(); 
     mycontroller.isAscending = false;
     myController.showLocalDraftQuotes();
     
       new QuoteServices.quoteHeader2();   
		  new QuoteServices.attachmentDetails();
		  new QuoteServices.defaultShippingAddress();
		  new QuoteServices.createSalesQuoteResponse();
		  new QuoteServices.ABI_SalesQuotation_process_sq1Wsd_Port();
		  new QuoteServices.quoteHeader();
		  new QuoteServices.shippingAddress();
		  new QuoteServices.termsAndConditions();
		  new QuoteServices.ArrayOflineItemDetails();
		  new QuoteServices.submitStatus();
		  new QuoteServices.submitQuote();
		  new QuoteServices.getSalesQuoteDtlsResponse();
		  new QuoteServices.createSalesQuote();
		  new QuoteServices.lineItemDetails();
		  new QuoteServices.partnerData();
		  new QuoteServices.getSalesQuoteDtls();
		  new QuoteServices.partnerInfo();
		  new QuoteServices.quoteDetails_SFDC();
		  new QuoteServices.createQuote();
		  new QuoteServices.quoteInfo();
		  new QuoteServices.ArrayOfpartnerInfo();
		  new QuoteServices.ArrayOfattachmentDetails();
		  new QuoteServices.submitQuoteResponse();
		  new QuoteServices.SalesQuote();
     
   }
   
   public static testMethod void checkQuotingController3() {
     QuotingController myController = new QuotingController(); 
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'SAP_Quote_Num__c';
     myController.showLocalDraftQuotes(); 
     mycontroller.isAscending = false;
     
     new QuotingWebServices.UserResult();
     new QuotingWebServices.AttachmentResult();
     new QuotingWebServices.QuoteAttachment();
      QuotingWebServices.retrieveManagersForUser('');
      QuotingWebServices.retrieveAttachment(new String[]{''});
    
     
     
   }
   public static testMethod void checkQuotingController4() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Sold_To__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController5() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Account__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController6() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Contact__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController7() {
     QuotingController myController = new QuotingController(); 
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Ship_To__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController8() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Status__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController9() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'CreatedDate';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController10() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Valid_End__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController11() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Zip_Code__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController12() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = true;
     mycontroller.sortField  = 'Short_Description__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController13() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'CDI_Quote_Num__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController14() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'SAP_Quote_Num__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController15() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Sold_To__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController16() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Account__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController17() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Contact__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController18() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Ship_To__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController19() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Status__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController20() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'CreatedDate';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController21() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Valid_End__c';
     myController.showLocalDraftQuotes(); 
   }
   public static testMethod void checkQuotingController22() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Zip_Code__c';
     myController.showLocalDraftQuotes();
     myController.convertDateToDateTime(System.Today());
     myController.convertDateToDateTimeWithAddedTime(System.Today(),3,2,4); 
   }
   public static testMethod void checkQuotingController23() {
     QuotingController myController = new QuotingController();  
     mycontroller.isAscending = false;
     mycontroller.sortField  = 'Short_Description__c';
     myController.showLocalDraftQuotes(); 
     Quote__c q = myController.getquoteProxy(); 
     Date dt = myController.getEffectiveDate(); 
     myController.setEffectiveDate(dt); 
     
   }
   
 public static testMethod void checkQuotingController01() {
  try{
    Test.setCurrentPageReference(new PageReference('Page.QuoteProductSearch'));
    Quote__c Quote = new Quote__c(Name = 'QuoteName');
    insert Quote;
    ApexPages.currentPage().getParameters().put('quoteId',Quote.Id);
    ApexPages.currentPage().getParameters().put('opt','true');
    QuotingController myController = new QuotingController();  
    string str = '';

    PageReference pgRef = null;
    pgRef = myController.loginToCDI();
    pgRef = myController.searchCDIProducts();
    pgRef = myController.addProductsToQuote();
    pgRef = myController.goToProductHierarchyScreen();
    pgRef = myController.returnCodeToProductSearch();
    pgRef = myController.searchProductHierarchy();
    pgRef = myController.quoteSearchCDI();
    pgRef = myController.quoteSearchReset();
    pgRef = myController.doSorting();
    pgRef = myController.viewQuote();
    pgRef = myController.cancelQuote();
    pgRef = myController.viewSAPQuote();
    myController.getUserSelection();
    List<Product_Hierarchy__c> lst45 = myController.getSearchResults();
    pgRef = myController.editExistingQuote();
    pgRef = myController.AddProductsFromQuote();
    pgRef = myController.CancelProductsFromQuote();
    pgRef = myController.CloneQuote();
    str = myController.getOriginalId();
    myController.CloneProducts('','');
    pgRef = myController.QuoteSearchSAP();
    pgRef = myController.QuoteNewHeader();
    pgRef = myController.QuoteNewPartner();
    pgRef = myController.QuoteNewOptions();
    pgRef = myController.QuoteProductSelect();
    pgRef = myController.QuoteProductDiscount();
    pgRef = myController.QuoteProductSort();
    pgRef = myController.QuoteProductSearch();
    pgRef = myController.QuoteTemplateSearch();
    pgRef = myController.QuoteMaterials();
    pgRef = myController.QuoteReturnLog();
    pgRef = myController.ImportTemplates();
    pgRef = myController.createNewQuote();
    pgRef = myController.getSelectedTemplate();
    pgRef = myController.FindListTemplates();
    QuotingController.listOfTemplatesInnerClass  cls12 = new QuotingController.listOfTemplatesInnerClass();
    str = cls12.getTemplateID();
    cls12.setTemplateID(str);
    str = cls12.getTemplateName();
    cls12.setTemplateName(str);
    str = cls12.getTemplateDescription();
    cls12.setTemplateDescription(str);
    str = cls12.getTemplateType();
    cls12.setTemplateType(str);
    str = cls12.getPromoCode();
    cls12.setPromoCode(str);
    str = cls12.getCurrency_x();
    cls12.setCurrency_x(str);
    Date dt1 = cls12.getQuoteValidityDate();
    cls12.setQuoteValidityDate(dt1);
    dt1 = cls12.getValidToDate();
    cls12.setValidToDate(dt1);
    str = cls12.getOutputMessage();
    cls12.setOutputMessage(str);
  }     
  catch(Exception e){}
 }
}