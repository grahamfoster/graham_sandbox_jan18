/** Container class for a comment and a time spent object.
 */
public class CommentContainer 
{
    private CaseComment comment;
    private Time_Spent__c timeSpent;
    
    public CommentContainer(CaseComment c, Time_Spent__c ts)
    {
        comment = c;
        timeSpent = ts;
    }
    
    public CaseComment getComment()
    {
        return comment;
    }
    
    public Time_Spent__c getTimeSpent()
    {
        return timeSpent;
    }
    
    public String getTimeSpentText()
    {
        if (timeSpent == null || timeSpent.Time_Spent_Text__c == null)
           return '';
        else
           return timeSpent.Time_Spent_Text__c;
    }
    
    public static testMethod void testIt()
    {
        CaseComment com = new CaseComment();
        Time_Spent__c ts = new Time_Spent__c();
        CommentContainer cc = new CommentContainer(com, ts);
        cc.getComment();
        cc.getTimeSpent();
        cc.getTimeSpentText();
        ts.Time_In_Minutes__c = 60;
        cc.getTimeSpentText();
    }
}