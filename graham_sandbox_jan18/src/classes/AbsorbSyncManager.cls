// ===========================================================================
// Object: AbsorbSyncManager
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Synch Absorb LMS data to SFDC
// ===========================================================================
// Changes: 2016-02-09 Reid Beckett
//           Class created
// ===========================================================================
public class AbsorbSyncManager 
{
	public AbsorbSyncManager() 
	{
	}

	/*
	public void syncAll()
	{
		syncCourses();
		syncEnrollments();
	}
	*/

	//Called by AbsorbSyncEnrollmentsQueueable batchable interface to update data at the beginning
	public void syncCourses()
	{
		AbsorbLogger.startMethod('syncCourses');

		AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
		List<AbsorbModel.Course> coursesList = absorbClient.getAllCourses();
		
		Map<String,LMS_Course__c> sfCoursesMap = new Map<String,LMS_Course__c>();
		
		//for each course, collect into a map by Id for upsert
		for(AbsorbModel.Course c : coursesList) {
			if(!sfCoursesMap.containsKey(c.Id)) 
			{
				String recordName = c.Name;
				if(recordName.length() > 80) recordName = recordName.substring(0,80);
				//the Vendor field contains the courseType and courseDuration tokenized by ;
				String vendor = c.Vendor;
				String courseType = null;
				String courseDuration = null;
				if(vendor != null) {
					String[] toks = vendor.split(';');
					if(toks.size() > 0) courseType = toks[0];
					if(toks.size() > 1) courseDuration = toks[1];
				}
                
                LMS_Course__c lmsCourse = new LMS_Course__c(Name = recordName, Course_Name__c = c.Name, 
					LMS_Course_ID__c = c.Id, 
					Description__c = c.Description,
					Active__c = (c.ActiveStatus == 0),
					Notes__c = c.Notes,
					Type__c = courseType,
					Duration__c = courseDuration,
					LMS_Course_Type__c = c.Goals,
					Audience__c = c.Audience                                                         
				);
                
                Map<String,String> descriptionPieces = parseCourseDescription(c.Description);
                lmsCourse.Product_Number__c = descriptionPieces.get('productNumber');
                if(lmsCourse.Product_Number__c != null && lmsCourse.Product_Number__c.length() > SfdcUtil.getFieldLength(LMS_Course__c.Product_Number__c)) {
                	lmsCourse.Product_Number__c = lmsCourse.Product_Number__c.substring(0, SfdcUtil.getFieldLength(LMS_Course__c.Product_Number__c));
                }
                lmsCourse.Price__c = descriptionPieces.get('price');
                if(lmsCourse.Price__c != null && lmsCourse.Price__c.length() > SfdcUtil.getFieldLength(LMS_Course__c.Price__c)) {
                	lmsCourse.Price__c = lmsCourse.Price__c.substring(0, SfdcUtil.getFieldLength(LMS_Course__c.Price__c));
                }
                lmsCourse.Location__c = descriptionPieces.get('location');
                if(lmsCourse.Location__c != null && lmsCourse.Location__c.length() > SfdcUtil.getFieldLength(LMS_Course__c.Location__c)) {
                	lmsCourse.Location__c = lmsCourse.Location__c.substring(0, SfdcUtil.getFieldLength(LMS_Course__c.Location__c));
                }
                lmsCourse.Course_Frequency__c = descriptionPieces.get('courseFrequency');
                if(lmsCourse.Course_Frequency__c != null && lmsCourse.Course_Frequency__c.length() > SfdcUtil.getFieldLength(LMS_Course__c.Course_Frequency__c)) {
                	lmsCourse.Course_Frequency__c = lmsCourse.Course_Frequency__c.substring(0, SfdcUtil.getFieldLength(LMS_Course__c.Course_Frequency__c));
                }
				sfCoursesMap.put(c.Id, lmsCourse);
			}
		}
		//perform the upsert
		upsert sfCoursesMap.values() LMS_Course__c.LMS_Course_ID__c;
		///remove any existing courses not found in the LMS
		delete [select Id from LMS_Course__c where LMS_Course_ID__c not in :sfCoursesMap.keySet()];
		absorbClient.close();
		AbsorbLogger.endMethod('syncCourses');
	}
    
    @TestVisible
    private static Map<String,String> parseCourseDescription(String descrip) {
        Map<String,String> m = new Map<String,String>();
        if(String.isBlank(descrip)) return m;

        m.put('productNumber', findStringBetween(descrip, 'Product Number'));
        m.put('price', findStringBetween(descrip, 'Price'));
        m.put('location', findStringBetween(descrip, 'Location'));
        m.put('courseFrequency', findStringBetween(descrip, 'Course Frequency'));
        
        return m;
    }

    private static String findStringBetween(String s, String labelTag) {
        String needle = s.substringBetween('<strong>'+labelTag+': </strong>', ';');
        if(needle == null) needle = s.substringBetween('<strong>'+labelTag+':</strong>', ';');
        if(needle == null) needle = s.substringBetween('<strong>'+labelTag+'</strong>:', ';');
        if(needle == null) needle = s.substringBetween('<strong>'+labelTag+' </strong>:', ';');
		if(needle == null) needle = s.substringBetween('<strong>'+labelTag+': </strong>', '\n');
        if(needle == null) needle = s.substringBetween('<strong>'+labelTag+':</strong>', '\n');
        if(needle == null) needle = s.substringBetween('<strong>'+labelTag+'</strong>:', '\n');    
        if(needle != null) needle = needle.trim();
        return needle;
    }
    
}