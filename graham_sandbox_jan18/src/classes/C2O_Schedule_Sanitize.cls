/**
 * @author Brett Moore
 * @created - Sept 2016
 * @Revision  
 * @Last Revision 
 * 
 * Schedulable APEX Class that launches the Sanitize Module. Used to set an auto-fire schedule for the C2O application. 
 * 
**/
Global class C2O_Schedule_Sanitize implements Schedulable{

    global void execute(SchedulableContext sc) {
        String preQuery = 'SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c, c2o_CE_Opportunity__c, Account_Region__c,Previous_Contract__c FROM Opportunity WHERE ';
        Contract2Opportunity_Configuration__c config = ([SELECT id, Sanitize_Query_Filter__c, scope__c FROM Contract2Opportunity_Configuration__c Limit 1]);
        String Filter = config.Sanitize_Query_Filter__c;
        Integer scope = config.scope__c.intValue();
        String Query = preQuery + Filter;
        String process = 'Sanitize';
        
        Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
        id batchInstanceId = database.executebatch(b,scope);
    }

}