/*
 *	WorkOrderNoteTrigger_Test
 *	
 *	Test class for WorkOrderNoteTrigger and WorkOrderNoteTriggerHandler.
 *
 *	If there are other test classes related to WorkOrderNoteTrigger, please document it here (as comments).
 * 
 * 	Created by Brett Moore 2016-12-08 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest 
public class WorkOrderNoteTrigger_Test {

    // Prepare initial sample data for running tests:
    @testSetup static void setup() {
        
     	// Populate all referenced objects needed to create a WO Note record
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
        insert u;         
    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'United States');
        insert testAccount;
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = testAccount.Id);
        insert testContact;
 		Product2 prod = new Product2(Name='ABSX WARRANTY 2',ProductCode='ABSX WARRANTY 2');
        insert prod;
        Pricebook2 pb = new Pricebook2 (Name='United States Price Book 2',CURRENCYISOCODE='USD',isActive=TRUE);
		insert pb;         
		SVMXC__Installed_Product__c comp = New SVMXC__Installed_Product__c ( NAME='API3000 - AF28211410',
                                                                            CURRENCYISOCODE ='USD',	
                                                                            SVMXC__COMPANY__C=testAccount.Id,	
                                                                            SVMXC__PRODUCT__C=prod.id,	
                                                                            SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', 

                                                                            SVC_Operating_Unit__c = 'ABC');        
        insert comp;
        SVMXC__Site__c site = new SVMXC__Site__c(Name = 'Test Location',SVMXC__Account__c = testAccount.id,SVMXC__Stocking_Location__c = true,
                                SVMXC__State__c = 'NY',SVMXC__Service_Engineer__c = u.Id,SVMXC__Country__c = 'United States',
                                SVMXC__Zip__c = '12345');
        insert site;         
        SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c(
                                                    Name = 'ABC',
                                                    SVMXC__Active__c = true,
                                                    SVMXC__State__c = 'NY',
                                                    SVMXC__Country__c = 'United States',
                                                    SVMXC__Zip__c = '12345');
        insert serviceTeam;
        SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c(
                                                        SVMXC__Active__c = true,
                                                        Name = 'Test Technician',
                                                        SVMXC__Service_Group__c = serviceTeam.Id,
                                                        SVMXC__Salesforce_User__c = u.Id,
                                                        SVMXC__Inventory_Location__c = site.Id);
        insert technician;
        RecordType rt = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
        Case ccase = new Case(RecordTypeId = rt.Id, AccountId = testAccount.Id, CurrencyIsoCode = 'USD', SVMXC__Component__c = comp.Id);
        insert ccase;
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(CurrencyIsoCode = 'CAD', SVMXC__Case__c = ccase.Id, SVC_Problem_Code__c = 'Electrical', SVC_Resolution_Code__c = 'Replaced Part(s)', SVC_Resolution_Summary__c = 'abc',SVMXC__Service_Group__c=serviceTeam.Id);
        insert wo;
    }
    
    @isTest  static void woNoteIUDUTest() {
        List<SVMXC__Service_Order__c> wo = new List<SVMXC__Service_Order__c>([SELECT id FROM SVMXC__Service_Order__c]);
		// Create the WO Note object
 		SVC_WO_Note__c woNote = new SVC_WO_Note__c(SVC_Work_Order__c=wo[0].Id);
        //Start Test
        test.startTest();
			insert woNote;       			
        	update woNote;
        	delete woNote;
        	undelete woNote;
        test.stopTest();
    }

}