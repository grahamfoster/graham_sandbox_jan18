public class AbQuotingPartnerUtils { 
  public static List<Quoting_SAP_Contact_Partners__c>  get_SOLD_TO_PartnerList(String soldTo){
    System.debug('QUOTING: find SP partner records for SoldTo:' + soldTo);
    //  -------------------  SOLD TO - SP Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> soldToSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c = :AbConstants.SOLD_TO_PARTNER_TYPE
                                                       LIMIT 999];
    System.debug('QUOTING: returning SoldTo partners:' + soldToSFDCPartnerList);
    return soldToSFDCPartnerList;
  }

  public static List<Quoting_SAP_Contact_Partners__c>  get_SOLD_TO_PartnerListwithSalesOrg(String soldTo, String salesOrg){
    System.debug('QUOTING: find SP partner records for SoldTo:' + soldTo);
    //  -------------------  SOLD TO - SP Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> soldToSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Sales_Org__c = :salesOrg
                                                         AND Partner_Function__c = :AbConstants.SOLD_TO_PARTNER_TYPE
                                                       LIMIT 999];
    System.debug('QUOTING: returning SoldTo partners:' + soldToSFDCPartnerList);
    return soldToSFDCPartnerList;
  }

  public static List<Quoting_SAP_Contact_Partners__c> get_SHIP_TO_PartnerListLOCAL(String soldTo, String salesOrg){
    //  -------------------  SHIP TO - SH Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> shipToSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.SHIP_TO_PARTNER_TYPE
                                                         AND Sales_Org__c = :salesOrg
                                                    ORDER BY First_Name__c, Last_Name__c,Address1__c, Name3__c, Name4__c
                                                       LIMIT 999];
    //Map<String, String> resultMap = new Map<String, String>();
    return shipToSFDCPartnerList;
/*      for (Quoting_SAP_Contact_Partners__c nextPartner : shipToSFDCPartnerList){
      //found a partner for this parentId - add to the result map
      String[] splits = nextPartner.Contact_Number__c.split('-',0);//60012543-SH - need to strip the "-SH"
      if(nextPartner.Sales_Org__c == salesOrg){            
          String idValue = splits[0];
          String displayValue = generatePartnerDisplayValue(nextPartner);
          resultMap.put(idValue, displayValue);
      }
    }
    return resultMap; */

  }
  
  public static List<Quoting_SAP_Contact_Partners__c> get_PAYER_PartnerListLOCAL(String soldTo, String salesOrg){
    //  -------------------  PAYER - PY Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> payerSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.PY_PARTNER_TYPE
                                                         AND Sales_Org__c = :salesOrg
                                                    ORDER BY First_Name__c, Last_Name__c,Address1__c, Name3__c, Name4__c
                                                       LIMIT 999];
    return payerSFDCPartnerList;
  }

  public static List<Quoting_SAP_Contact_Partners__c> get_BILL_TO_PartnerListLOCAL(String payer){
    //  -------------------  BILL TO Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> billToSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                              Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                              Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                              Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                              Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                         FROM Quoting_SAP_Contact_Partners__c
                                                        WHERE Parent_Partner_Id__c = :payer
                                                          AND Partner_Function__c =  :AbConstants.BILL_TO_PARTNER_TYPE
                                                     ORDER BY First_Name__c
                                                        LIMIT 999];
    return billToSFDCPartnerList;
  }

  public static String generatePartnerDisplayValue(Quoting_SAP_Contact_Partners__c nextPartner){ 
    String displayValue;
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.First_Name__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Last_Name__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Name3__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Name4__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Address1__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Address2__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.City__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.State_Province__c);
    displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Postal_Code__c);
    return displayValue;    
  }
  
  
  public static Map<String, String> get_DEST_COUNTRY_PartnerListLOCAL(String soldTo){
    //  -------------------  DEST COUNTRY - Z6 Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> destCountrySFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.DEST_COUNTRY_PARTNER_TYPE
                                                    ORDER BY First_Name__c
                                                       LIMIT 999];
    Map<String, String> resultMap = new Map<String, String>();
    for (Quoting_SAP_Contact_Partners__c nextPartner : destCountrySFDCPartnerList){
      //found a partner for this parentId - add to the result map
      String[] splits = nextPartner.Contact_Number__c.split('-',0);//60012543-Z6 - need to strip the "-Z6"
      String idValue = splits[0];
      String name1 = '';
      if(nextPartner.First_Name__c != null) {name1 = nextPartner.First_Name__c;}
      String displayValue =  name1;
      resultMap.put(idValue, displayValue);
    }
    return resultMap; 
  }

  public static Map<String, String> get_SALES_REP_PartnerListLOCAL(String soldTo){
    //  -------------------  SALES REP - Z1 Partners -------------------------------------------------------------
    List<Quoting_SAP_Contact_Partners__c> salesRepSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.SALES_REP_PARTNER_TYPE
                                                    ORDER BY Last_Name__c
                                                       LIMIT 999];
    Map<String, String> resultMap = new Map<String, String>();
    for (Quoting_SAP_Contact_Partners__c nextPartner : salesRepSFDCPartnerList){
      //found a partner for this parentId - add to the result map
      String[] splits = nextPartner.Contact_Number__c.split('-',0);//60012543-Z1 - need to strip the "-Z1"
      String idValue = splits[0];
      
      String displayValue;
      displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.Last_Name__c);
      displayValue = AbSearchUtils.concatDisplayFieldAndComma(displayValue, nextPartner.First_Name__c);
      resultMap.put(idValue, displayValue);
    }
    return resultMap; 
    
  }
 
 /* old code.
  public static List<Quoting_SAP_Contact_Partners__c> get_SAP_CONTACT_ONE_K_PartnerListLOCAL(String soldTo){
    
    List<Quoting_SAP_Contact_Partners__c> sapContactSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.SAP_CONTACT_PARTNER_TYPE
                                                    ORDER BY First_Name__c
                                                       LIMIT 999];
    return sapContactSFDCPartnerList;
  }
 */
  
  public static Quoting_SAP_Contact_Partners__c getQuotingSAPContactPartners(String id) 
  {
  	String likeId = '%' + id + '%';
  	List<Quoting_SAP_Contact_Partners__c> sapContactSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Contact_Number__c LIKE :likeId];          
       if(sapContactSFDCPartnerList != null && sapContactSFDCPartnerList.size() > 0) return sapContactSFDCPartnerList[0];
       
       return null;
  }
  
  public static List<Quoting_SAP_Contact_Partners__c> get_SAP_CONTACT_ONE_K_PartnerListLOCAL(String soldTo, String sapContactValue ){
    
     if (sapContactValue == 'A-L' ) { // && sapContactValue != 'M-Z') {
	     
    	List<Quoting_SAP_Contact_Partners__c> sapContactSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.SAP_CONTACT_PARTNER_TYPE AND
		   		   										(First_Name__c LIKE 'A%' OR First_Name__c LIKE 'B%' OR First_Name__c LIKE 'C%' OR First_Name__c LIKE 'D%' OR First_Name__c LIKE 'E%' 
	       		  										 OR First_Name__c LIKE 'F%' OR First_Name__c LIKE 'G%' OR First_Name__c LIKE 'H%' OR First_Name__c LIKE 'I%'
	      		  										 OR First_Name__c LIKE 'J%' OR First_Name__c LIKE 'K%' OR First_Name__c LIKE 'L%' OR First_Name__c LIKE 'a%'
	       		  										 OR First_Name__c LIKE 'b%' OR First_Name__c LIKE 'c%' OR First_Name__c LIKE 'd%' OR First_Name__c LIKE 'e%'
	       		   										 OR First_Name__c LIKE 'f%' OR First_Name__c LIKE 'g%' OR First_Name__c LIKE 'h%' OR First_Name__c LIKE 'i%'
		  		   										OR First_Name__c LIKE 'j%' OR First_Name__c LIKE 'k%' OR First_Name__c LIKE 'l%')
                                                    ORDER BY First_Name__c
                                                       LIMIT 999];
    	return sapContactSFDCPartnerList;
     } else if (sapContactValue == 'M-Z') {
     	List<Quoting_SAP_Contact_Partners__c> sapContactSFDCPartnerList = [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                             Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                             Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                             Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                             Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                        FROM Quoting_SAP_Contact_Partners__c
                                                       WHERE Sold_To__c = :soldTo
                                                         AND Partner_Function__c =  :AbConstants.SAP_CONTACT_PARTNER_TYPE AND
		   		   										(First_Name__c LIKE 'M%' OR First_Name__c LIKE 'N%' OR First_Name__c LIKE 'O%' OR First_Name__c LIKE 'P%' OR First_Name__c LIKE 'Q%' 
	       		  										 OR First_Name__c LIKE 'R%' OR First_Name__c LIKE 'S%' OR First_Name__c LIKE 'T%' OR First_Name__c LIKE 'U%'
	      		  										 OR First_Name__c LIKE 'V%' OR First_Name__c LIKE 'W%' OR First_Name__c LIKE 'X%' OR First_Name__c LIKE 'Y%' OR First_Name__c LIKE 'Z%' OR First_Name__c LIKE 'm%'
	       		  										 OR First_Name__c LIKE 'n%' OR First_Name__c LIKE 'o%' OR First_Name__c LIKE 'p%' OR First_Name__c LIKE 'q%'
	       		   										 OR First_Name__c LIKE 'r%' OR First_Name__c LIKE 's%' OR First_Name__c LIKE 't%' OR First_Name__c LIKE 'u%'
		  		   										OR First_Name__c LIKE 'v%' OR First_Name__c LIKE 'w%' OR First_Name__c LIKE 'x%'  OR First_Name__c LIKE 'y%' OR First_Name__c LIKE 'z%'    )
                                                    ORDER BY First_Name__c
                                                       LIMIT 999];
    	return sapContactSFDCPartnerList;
     }
     
     return null;
  }
  
    public static String createDisplayValueForContacts(String name1, String name2, String sapid) {
      String displayValue = sapid+',';
      if (name1 != null) {
          displayValue+=name1+',';
      } else {
          displayValue+=',';
      }
      
      if (name2 != null) {
          displayValue+=name2;
      }
      System.debug('--------------------'+displayValue+'------------------'); 
      return displayValue;
      
  }

    public static String getPartnerCountryBasedOnSoldTo(String soldTo){
      
      List<Quoting_SAP_Contact_Partners__c> soldToSFDCPartnerList = [SELECT  Country__c
                                                                       FROM Quoting_SAP_Contact_Partners__c
                                                                      WHERE Sold_To__c = :soldTo
                                                                        AND Partner_Function__c = :AbConstants.SOLD_TO_PARTNER_TYPE
                                                                      LIMIT 999];
      for (Quoting_SAP_Contact_Partners__c nextSoldToPartner: soldToSFDCPartnerList){
        if(nextSoldToPartner.Country__c != null){
          return nextSoldToPartner.Country__c;
        }
      }
      return null;
      
    }
    
    
    //return a Set<String> of Sales Orgs based on the given SoldTo
    public static Set<String> getSalesOrgBasedOnSoldTo(String soldTo){

      System.debug('QUOTING: find Sales orgs for sold to: ' + soldTo);
      Set<String> sOrgSet = new Set<String>();
      for(List<Quoting_SAP_Contact_Partners__c> soldToSFDCPartnerList : [SELECT Partner_Function__c, Sold_To__c, Parent_Partner_ID__c, Contact_Number__c, 
                                                                     Sales_Org__c, Sales_Group__c, Sales_Office__c, Language_Key__c, First_Name__c, 
                                                                     Last_Name__c, Name3__c, Name4__c, City__c, Postal_Code__c, State_Province__c, 
                                                                     Address1__c, Address2__c, Telephone__c, Fax__c, email__c, Country__c, Dept_Number__c, 
                                                                     Term_Of_Payment__c, INCO_Terms__c, Currency__c
                                                                FROM Quoting_SAP_Contact_Partners__c
                                                               WHERE Sold_To__c = :soldTo]){

        for(Quoting_SAP_Contact_Partners__c nextPartner : soldToSFDCPartnerList){
          if(nextPartner.Sales_Org__c != null && nextPartner.Sales_Org__c != ''){
            sOrgSet.add(nextPartner.Sales_Org__c);
          }
        }

      }
      System.debug('QUOTING: returning set of Sales Orgs: ' + sOrgSet);
      return sOrgSet;
    }
  

}