@isTest(SeeAllData=true)
public class LeadAssignment_Test {
    
static testmethod void Lead_State_Users() {
Profile prof = [select Id from Profile where Name ='System Administrator'];  
User u1 = new User(LastName = '111',Alias = '1',Email = 'test@111.com',username = '111@testingtonpalace12345.com',
                          CommunityNickname = '1',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                          TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u1;

User u2 = new User(LastName = '22442',Alias = '2', Email = 'test@44222.com',username = '22442@testingtonpalace12345.com',
                   CommunityNickname = '24',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                   TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u2;    
Group queue1 = [Select ID, Name from Group where name = 'Americas Canada East Leads'];
Group queue2 = [Select ID, Name from Group where name = 'Americas Canada West Leads'];
    
Country_Mapping__c country = new Country_Mapping__c(Name = 'TestCountry', Permutations__c = 'TESTCOUNTRY', Country_Code__c = 'XX');
insert country;
     
Country_Mapping__c c = [Select ID from Country_Mapping__c where Name = 'TestCountry' LIMIT 1];
State_Mapping__c state = new State_Mapping__c(
    CE_Owner__c =u2.id,
    CE_Queue__c= queue2.Name,
    Name = 'TestState',
    Country_Mapping__c = country.id,
    MS_Owner__c=u1.id,
    MS_Queue__c= queue1.Name,
    State_Province_Permutations__c = 'TESTSTATE');
insert state;  
 
    //test assignment of users to both primary and secondary from STATE          
    Group DefaultLeadOwner = [Select ID from Group where name = 'AssignTriggerQueue' Limit 1];
    Lead lea = new Lead(LastName='LeadTest',
        Company = 'TestLead',
        Status = 'Open',
        Country='TestCountry',
        State = 'TestState',
        Business_Segment__c = 'MS',
        CE_Intent_to_Purchase__c = 'B - Warm (7-12 months purchase)', //Secondary
        Intent_to_Purchase__c = 'A - Hot (0-6 months purchase)', //Primary
        Marketing_Lead_Score__c='1',
        OwnerID = DefaultLeadOwner.Id);
    insert lea;
        
    lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Primary_Routing__c, Secondary_Routing__c, Name,
            OwnerID, Lead_Secondary_Owner_ID__c, Lead_Secondary_Owner__c, Lead_Secondary_Email__c, Dual_Intent__c
                            from Lead where Id =: lea.Id];
        User pri = [select ID, Name from User where ID =: state.MS_Owner__c];
        User sec = [select ID, Name, Email from user where ID =: state.CE_Owner__c];
    system.debug(pri.Name);
    system.debug(sec.Name);
        system.debug(pri.id);
    system.debug(sec.id);
    System.assertEquals('MS', lea.Primary_Routing__c);
    System.assertEquals('CE', lea.Secondary_Routing__c);
    System.assertEquals(True, lea.Dual_Intent__c);
    System.assertEquals(pri.Id, lea.OwnerID);        
    System.assertEquals(sec.Id, lea.Lead_Secondary_Owner_ID__c);
    System.assertEquals(sec.Name, lea.Lead_Secondary_Owner__c);
    System.assertEquals(sec.Email, lea.Lead_Secondary_Email__c);        
}     
static testmethod void Lead_Country_Users() {
//test assignment of users to both primary and secondary from Country          
Profile prof = [select Id from Profile where Name ='System Administrator'];  
User u1 = new User(LastName = '111',Alias = '1',Email = 'test@111.com',username = '111@testingtonpalace12345.com',
                          CommunityNickname = '1',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                          TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u1;

User u2 = new User(LastName = '22442',Alias = '2', Email = 'test@44222.com',username = '22442@testingtonpalace12345.com',
                   CommunityNickname = '24',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                   TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u2;    
    
Group queue1 = [Select ID, Name from Group where name = 'Americas Canada East Leads'];
Group queue2 = [Select ID, Name from Group where name = 'Americas Canada West Leads'];
    
Country_Mapping__c country = new Country_Mapping__c(
    CE_Owner__c =u2.id,
    CE_Queue__c= queue2.Name,
    Name = 'TestCountry',
    MS_Owner__c=u1.id,
    MS_Queue__c= queue1.Name,
    Permutations__c = 'TESTCOUNTRY', Country_Code__c = 'XX');
insert country;
    
    Group DefaultLeadOwner = [Select ID from Group where name = 'AssignTriggerQueue' Limit 1];
    Lead lea = new Lead(LastName='LeadTest',
        Company = 'TestLead',
        Lead_Type__c = 'Existing Contact',                
        Status = 'Open',
        Country='TestCountry',
        State = 'OT',
        Business_Segment__c = 'MS',
        CE_Intent_to_Purchase__c = 'B - Warm (7-12 months purchase)', //Secondary
        Intent_to_Purchase__c = 'A - Hot (0-6 months purchase)', //Primary
        Marketing_Lead_Score__c='1',
		Email = 'neededfortesting@test.com',                        
        OwnerID = DefaultLeadOwner.Id);
    insert lea;
        
    lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Primary_Routing__c, Secondary_Routing__c, Name,
            OwnerID, Lead_Secondary_Owner_ID__c, Lead_Secondary_Owner__c, Lead_Secondary_Email__c, Dual_Intent__c
                            from Lead where Id =: lea.Id];
        User pri = [select ID, Name from User where ID =: country.MS_Owner__c];
        User sec = [select ID, Name, Email from user where ID =: country.CE_Owner__c];
        
    System.assertEquals('MS', lea.Primary_Routing__c);
    System.assertEquals('CE', lea.Secondary_Routing__c);
    System.assertEquals(True, lea.Dual_Intent__c);
    System.assertEquals(pri.Id, lea.OwnerID);        
    System.assertEquals(sec.Id, lea.Lead_Secondary_Owner_ID__c);
    System.assertEquals(sec.Name, lea.Lead_Secondary_Owner__c);
    System.assertEquals(sec.Email, lea.Lead_Secondary_Email__c);     
    
}   
static testmethod void Lead_State_Queues() {
//test assignment of users to both primary and secondary from STATE          
Profile prof = [select Id from Profile where Name ='System Administrator'];  
User u1 = new User(LastName = '111',Alias = '1',Email = 'test@111.com',username = '111@testingtonpalace12345.com',
                          CommunityNickname = '1',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                          TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u1;

User u2 = new User(LastName = '22442',Alias = '2', Email = 'test@44222.com',username = '22442@testingtonpalace12345.com',
                   CommunityNickname = '24',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                   TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u2;    
Group queue1 = [Select ID, Name from Group where name = 'Americas Canada East Leads'];
Group queue2 = [Select ID, Name from Group where name = 'Americas Canada West Leads'];
    
Country_Mapping__c country = new Country_Mapping__c(Name = 'TestCountry', Permutations__c = 'TESTCOUNTRY', Country_Code__c = 'XX');
insert country;
     
    Country_Mapping__c c = [Select ID from Country_Mapping__c where Name = 'TestCountry' LIMIT 1];
State_Mapping__c state = new State_Mapping__c(
    CE_Owner__c =null,
    CE_Queue__c= queue2.Name,
    Name = 'TestState',
    Country_Mapping__c = country.id,
    MS_Owner__c=null,
    MS_Queue__c= queue1.Name,
    State_Province_Permutations__c = 'TESTSTATE');
    insert state; 
    
    Group DefaultLeadOwner = [Select ID from Group where name = 'AssignTriggerQueue' Limit 1];
    Lead lea = new Lead(LastName='LeadTest',
        Company = 'TestLead',
        Status = 'Open',
        Country='TestCountry',
        State = 'TestState',
        Business_Segment__c = 'MS',
        CE_Intent_to_Purchase__c = 'B - Warm (7-12 months purchase)', //Secondary
        Intent_to_Purchase__c = 'A - Hot (0-6 months purchase)', //Primary
        Marketing_Lead_Score__c='1',
        OwnerID = DefaultLeadOwner.Id);
    insert lea;
        
    lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Primary_Routing__c, Secondary_Routing__c, Name,
            OwnerID, Lead_Secondary_Owner_ID__c, Lead_Secondary_Owner__c, Lead_Secondary_Email__c, Dual_Intent__c
                            from Lead where Id =: lea.Id];
        Group pri = [select ID, Name from Group where Name =: state.MS_Queue__c];
        Group sec = [select ID, Name from Group where Name =: state.CE_Queue__c];
    
    System.assertEquals('MS', lea.Primary_Routing__c);
    System.assertEquals('CE', lea.Secondary_Routing__c);
    System.assertEquals(True, lea.Dual_Intent__c);
    System.assertEquals(pri.Id, lea.OwnerID);        
    System.assertEquals(sec.id, lea.Lead_Secondary_Owner_ID__c);
    System.assertEquals(sec.Name, lea.Lead_Secondary_Owner__c);
    System.assertEquals(NULL, lea.Lead_Secondary_Email__c);        
}   
static testmethod void Lead_Country_Queues() {
Profile prof = [select Id from Profile where Name ='System Administrator'];  
User u1 = new User(LastName = '111',Alias = '1',Email = 'test@111.com',username = '111@testingtonpalace12345.com',
                          CommunityNickname = '1',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                          TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u1;

User u2 = new User(LastName = '22442',Alias = '2', Email = 'test@44222.com',username = '22442@testingtonpalace12345.com',
                   CommunityNickname = '24',ProfileID = prof.Id,LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US',
                   TimeZoneSidKey = 'GMT',isActive=True,EmailEncodingKey='ISO-8859-1');
insert u2;    
Group queue1 = [Select ID, Name from Group where name = 'Americas Canada East Leads'];
Group queue2 = [Select ID, Name from Group where name = 'Americas Canada West Leads'];
Country_Mapping__c country = new Country_Mapping__c(
    CE_Owner__c =null,
    CE_Queue__c= queue2.Name,
    Name = 'TestCountry',
    MS_Owner__c=null,
    MS_Queue__c= queue1.Name,
    Permutations__c = 'TESTCOUNTRY', Country_Code__c = 'XX');
insert country;
    
    Group DefaultLeadOwner = [Select ID from Group where name = 'AssignTriggerQueue' Limit 1];
    Lead lea = new Lead(LastName='LeadTest',
        Company = 'TestLead',
        Status = 'Open',
        Country='TestCountry',
        State = 'OT',
        Email = 'neededfortesting@test.com',
        Business_Segment__c = 'MS',
        CE_Intent_to_Purchase__c = 'B - Warm (7-12 months purchase)', //Secondary
        Intent_to_Purchase__c = 'A - Hot (0-6 months purchase)', //Primary
        Marketing_Lead_Score__c='1',
        OwnerID = DefaultLeadOwner.Id);
    insert lea;
        
    lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Primary_Routing__c, Secondary_Routing__c, Name,
            OwnerID, Lead_Secondary_Owner_ID__c, Lead_Secondary_Owner__c, Lead_Secondary_Email__c, Dual_Intent__c
                            from Lead where Id =: lea.Id];
        Group pri = [select ID, Name from Group where Name =: country.MS_Queue__c];
        Group sec = [select ID, Name from Group where Name =: country.CE_Queue__c];
    
    System.assertEquals('MS', lea.Primary_Routing__c);
    System.assertEquals('CE', lea.Secondary_Routing__c);
    System.assertEquals(True, lea.Dual_Intent__c);
    System.assertEquals(pri.Id, lea.OwnerID);        
    System.assertEquals(sec.id, lea.Lead_Secondary_Owner_ID__c);
    System.assertEquals(sec.Name, lea.Lead_Secondary_Owner__c);
    System.assertEquals(NULL, lea.Lead_Secondary_Email__c);         
     
}        
}