public interface IMySFDCDashboard {
	void setRunAs(User runAs);
	void setStartDate(Date d);
	void setEndDate(Date d);
	void query();

	Decimal getQuota();
	void setQuota(Decimal quota);
	void setConversionRate(Decimal conversionRate);
	Decimal getActual();

	GoogleGaugeChartData getGoogleGaugeChartData();
	String getReportTitle();
}