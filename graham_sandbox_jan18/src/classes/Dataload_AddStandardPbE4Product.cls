/*
 * Dataload_AddStandardPbE4Product
 *
 * This Batch Apex will add Standard PricebookEntry to each Product for each current type.
 * This Batch Apex could be run repeatly, but we only need to run this once.
 * This Batch Apex should be deployed/run before deploy "Product2 After Insert" trigger, 
 * or with "Product2 After Insert" trigger set to Inactive, since the trigger will cause 
 * the unit test program to fail.
 * 
 * Run the following from anonymous apex:
 * String standardPBId = [Select Id From Pricebook2 where IsStandard = true][0].Id;
 * Integer batchSize = 30;
 * Id batchInstanceId = Database.executeBatch(new Dataload_AddStandardPbE4Product(standardPBId),batchSize);
 *
 * For email to work, set access level for email to "All email".
 *
 * Created by Yong Chen on 2015-09-15
 *
 *  [Modification history]
 *  [Yong Chen] [2016-06-22] Add Oracle_External_ID__c when creating Standard PricebookEntry 
 *
 */
global class Dataload_AddStandardPbE4Product implements Database.Batchable<sObject>{
	global final String Query;
	global final String standardPBId;

	global Dataload_AddStandardPbE4Product(String s){
		standardPBId = s;
		Query = 'SELECT id,Oracle_Item_Number__c FROM Product2';
	}

	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope){
		List<PricebookEntry> toInsert = new List<PricebookEntry>();
		List<Product2> AllProduct2 = new List<Product2>();

		for(sObject s : scope) {
			AllProduct2.add((Product2)s);
		}
		
		// get Currency Types
		CurrencyType[] activeCT = [SELECT Id,IsActive,IsoCode FROM CurrencyType WHERE IsActive=TRUE ORDER BY IsoCode];

		List<PricebookEntry> relatedPBE = [SELECT Id,Product2Id,Pricebook2Id,CurrencyIsoCode 
				FROM PricebookEntry 
				WHERE Product2Id IN :AllProduct2 AND Pricebook2Id = :standardPBId
				ORDER BY Product2Id,CurrencyIsoCode 
				];

		for(Product2 p : AllProduct2) {
			// for each updated product, loop through currencytypes, check pricebookentry 
			for (CurrencyType ct : activeCT) {
				Boolean standardPBEFound = false;
				for (sObject s : relatedPBE) {
					PricebookEntry pb = (PricebookEntry)s;
					if (pb.Product2Id == p.Id && pb.CurrencyIsoCode == ct.IsoCode) {
						standardPBEFound = true;
						break;
					}
			    }
	
				if (!standardPBEFound) {
					toInsert.add(new PricebookEntry (
							CurrencyIsoCode = ct.IsoCode,
							Product2Id = p.Id, 
							Pricebook2Id = standardPBId,
							IsActive = True, 
							UnitPrice = 0,
							Oracle_External_ID__c = 'STD-'+p.Oracle_Item_Number__c+'-'+ct.IsoCode
							));
				} 
			}
		}   
		
		if (toInsert.size() > 0) {
			insert toInsert;
		}
		
	}

	global void finish(Database.BatchableContext BC){
		/*
			By default, in sandbox, the access level to "Access to Send Email" is set to "System email Only".
			To send email, go to "Setup | Administer | Email Administration | Deliverability", change the access level to "All email".
			In Production, the access level is set to "All email".
		*/
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(new String[] {'yong.chen@sciex.com'});
		mail.setReplyTo('yong.chen@sciex.com');
		mail.setSenderDisplayName('Batch Processing');
		mail.setSubject('Yong Batch Process Completed: Dataload_AddStandardPbE4Product');
		mail.setPlainTextBody('Yong Batch Process has completed: Dataload_AddStandardPbE4Product');
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}