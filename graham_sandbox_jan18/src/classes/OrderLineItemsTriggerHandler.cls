// ===========================================================================
// Object: OrderLineItemsTriggerHandler
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Handles Order Line Item trigger logic to rollup values from the Order_Line_Items__c to the parent Oracle_Order__c record
// ===========================================================================
// Changes: 2017-02-07 Reid Beckett
//           Class created
// ===========================================================================
public class OrderLineItemsTriggerHandler extends TriggerHandler
{
    override protected void afterUpdate()
    {
        rollupToOracleOrder((List<Order_Line_Items__c>)Trigger.new);
    }

    override protected void afterInsert()
    {
        rollupToOracleOrder((List<Order_Line_Items__c>)Trigger.new);
    }

    override protected void afterDelete()
    {
        rollupToOracleOrder((List<Order_Line_Items__c>)Trigger.old);
    }

    override protected void afterUndelete()
    {
        rollupToOracleOrder((List<Order_Line_Items__c>)Trigger.new);
    }

    //rolls up fields from the line items up to the parent Oracle Order records    
    private void rollupToOracleOrder(List<Order_Line_Items__c> orderLineItems)
    {
        Set<Id> oracleOrderIds = collectOracleOrderIds(orderLineItems);
        Map<Id, Oracle_Order__c> oracleOrdersById = new Map<Id, Oracle_Order__c>();
        Map<Id, List<OrderLineWrapper>> instrumentOrderLinesByOrderId = new Map<Id, List<OrderLineWrapper>>();
        Map<Id, List<OrderLineWrapper>> trainingOrderLinesByOrderId = new Map<Id, List<OrderLineWrapper>>();
        
        for(Order_Line_Items__c lineItem : [select Id, Oracle_Order__c, Total_Number_of_FAS_Days_Purchased__c, Actual_Shipment_Date__c, Requested_Shipment_Date__c, Scheduled_Shipment_Date__c, Product__r.Igor_Class__c, Product__r.ProductCode, Status__c, Product__r.Part_Priority__c, Product__r.Family from Order_Line_Items__c where Oracle_Order__c in :oracleOrderIds and Status__c != 'Cancelled'])
        {
            Oracle_Order__c oracleOrder = null;
            if(!oracleOrdersById.containsKey(lineItem.Oracle_Order__c)) {
                oracleOrder = new Oracle_Order__c(Id = lineItem.Oracle_Order__c, Total_Number_of_FAS_Days_Purchased__c = 0, No_Of_Training_Parts__c = '0',
                   MS_Instrument_Status__c = null, Requested_Shipment_Date__c = null
                   );
                oracleOrdersById.put(oracleOrder.Id, oracleOrder);
            }else {
                oracleOrder = oracleOrdersById.get(lineItem.Oracle_Order__c);
            }

            //filters on training product line items
            if(isTrainingLineItem(lineItem)) 
            {
                if(!trainingOrderLinesByOrderId.containsKey(lineItem.Oracle_Order__c)) {
                    trainingOrderLinesByOrderId.put(lineItem.Oracle_Order__c, new List<OrderLineWrapper>{ new OrderLineWrapper(lineItem) });
                }else{
                    trainingOrderLinesByOrderId.get(lineItem.Oracle_Order__c).add(new OrderLineWrapper(lineItem));
                }
            }
            
            //filter on instrument line items
            if(COBOUtil.isInstrument(lineItem.Product__r.Family)) 
            {
                if(!instrumentOrderLinesByOrderId.containsKey(lineItem.Oracle_Order__c)) {
                    instrumentOrderLinesByOrderId.put(lineItem.Oracle_Order__c, new List<OrderLineWrapper>{ new OrderLineWrapper(lineItem) });
                }else{
                    instrumentOrderLinesByOrderId.get(lineItem.Oracle_Order__c).add(new OrderLineWrapper(lineItem));
                }
            }
        }

        //process the training parts and roll them up
        for(Id oracleOrderId : trainingOrderLinesByOrderId.keySet())
        {
            Oracle_Order__c oracleOrder = oracleOrdersById.get(oracleOrderId);
            if(oracleOrder != null) 
            {
                List<OrderLineWrapper> orderLines = trainingOrderLinesByOrderId.get(oracleOrderId);
                orderLines.sort();

                //COUNT of records rolled up to No_Of_Training_Parts__c
                oracleOrder.No_Of_Training_Parts__c = String.valueOf(orderLines.size());

                //SUM of Total_Number_of_FAS_Days_Purchased__c rolled up to Total_Number_of_FAS_Days_Purchased__c
                oracleOrder.Total_Number_of_FAS_Days_Purchased__c = 0;
                for(OrderLineWrapper orderLine : orderLines)
                {
                    if(orderLine.lineItem.Total_Number_of_FAS_Days_Purchased__c != null)  {
                        oracleOrder.Total_Number_of_FAS_Days_Purchased__c += orderLine.lineItem.Total_Number_of_FAS_Days_Purchased__c;
                    }
                }

                if(orderLines.size() > 0) {
                    Order_Line_Items__c lineItem = orderLines.get(0).lineItem;
                    oracleOrder.Training_Product__c = lineItem.Product__r.ProductCode;
                }

                if(orderLines.size() > 1) {
                    Order_Line_Items__c lineItem = orderLines.get(1).lineItem;
                    oracleOrder.Additional_Training_Product__c = lineItem.Product__r.ProductCode;
                }

                oracleOrdersById.put(oracleOrderId, oracleOrder);
            }
        }

        //process the instruments and roll them up
        for(Id oracleOrderId : instrumentOrderLinesByOrderId.keySet()) 
        {
            Oracle_Order__c oracleOrder = oracleOrdersById.get(oracleOrderId);
            if(oracleOrder != null) 
            {
                List<OrderLineWrapper> orderLines = instrumentOrderLinesByOrderId.get(oracleOrderId);
                orderLines.sort();

                //Rollup from fIrst Instrument
                if(orderLines.size() > 0) {
                    Order_Line_Items__c lineItem = orderLines.get(0).lineItem;
                    oracleOrder.MS_Product__c = lineItem.Product__r.ProductCode;
                    oracleOrder.Shipment_Status__c = lineItem.Status__c;

                    oracleOrder.MS_Instrument_Status__c = lineItem.Actual_Shipment_Date__c;
                    oracleOrder.Requested_Shipment_Date__c = lineItem.Requested_Shipment_Date__c;
                    oracleOrder.Scheduled_Shipment_Date__c = lineItem.Scheduled_Shipment_Date__c;
                }

                //Rollup from second Instrument
                if(orderLines.size() > 1) {
                    oracleOrder.MS_Product_2__c = orderLines.get(1).lineItem.Product__r.ProductCode;
                }

                //Rollup from third Instrument
                if(orderLines.size() > 2) {
                    oracleOrder.MS_Product_3__c = orderLines.get(2).lineItem.Product__r.ProductCode;
                }

                oracleOrdersById.put(oracleOrderId, oracleOrder);
            }
        }

        update oracleOrdersById.values();
    }

    //Determine if the line item is a training part
    private static Boolean isTrainingLineItem(Order_Line_Items__c lineItem) {
        return (lineItem.Product__r.Igor_Class__c == 'H');
    }

    //collect the parent Oracle Order ID's into a set for processing    
    private Set<Id> collectOracleOrderIds(List<Order_Line_Items__c> orderLineItems) 
    {
        Set<Id> idSet = new Set<Id>();
        for(Order_Line_Items__c lineItem : orderLineItems)
        {
            if(lineItem.Oracle_Order__c != null) idSet.add(lineItem.Oracle_Order__c);
        }
        return idSet;
    }
    
    //Wrapper for the Order_Line_Items__c to provide sorting capability
    //Instrument is sorted by Product__r.Family based on the corresponding ProductFamilySettings__c's priority
    //Training parts are sorted by Product__r.Part_Priority__c
    @TestVisible
    class OrderLineWrapper implements Comparable 
    {
        public Order_Line_Items__c lineItem {get;set;}
        public Integer priority {get;set;}
        
        public OrderLineWrapper(Order_Line_Items__c lineItem) 
        {
            this.lineItem = lineItem;
            if(COBOUtil.isInstrument(lineItem.Product__r.Family)) {
                ProductFamilySettings__c pfs = COBOUtil.getProductFamilySettings(lineItem.Product__r.Family);
                if(pfs != null) {
                    Decimal priority = pfs.Priority__c;
                    if(priority != null) this.priority = Integer.valueOf(priority);
                }
            }

            if(isTrainingLineItem(lineItem)) {
                if(!String.isBlank(lineItem.Product__r.Part_Priority__c))
                    this.priority = Integer.valueOf(lineItem.Product__r.Part_Priority__c);
            }
        }

        //implements sorting by priority
        public Integer compareTo(Object that)
        {
            if(that == null) return 1;
            OrderLineWrapper thatOLW = (OrderLineWrapper)that;
            return this.priority - thatOLW.priority;
        }
    }
}