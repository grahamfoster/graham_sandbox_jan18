public class CTIUtil {

	public CTIUtil(){}

	public static Task CurrentCallTask()
	{
		Id usId = UserInfo.getUserId();
        Date dateToday = Date.today();
		String sMonth = String.valueof(dateToday.month());
		String sDay = String.valueof(dateToday.day());
		if(sMonth.length()==1){
  			sMonth = '0' + sMonth;
		}
		if(sDay.length()==1){
  		sDay = '0' + sDay;
		}
		String sToday = 'Call ' + String.valueof(dateToday.year()) + '-' + '%';
		System.debug('**GF** CurrentCallTask: sToday;' + sToday);
        List<Task> identifiedCalls = [SELECT Id, CreatedDate, LastModifiedDate, cnx__CTIInfo__c FROM Task 
                                      WHERE OwnerId = :usId AND CallDurationInSeconds = null AND cnx__CTIInfo__c != null
									  ORDER BY CreatedDate DESC LIMIT 1];
        List<Task> updateTask = new List<Task>();

		if(identifiedCalls.size() == 1)
		{
			return identifiedCalls[0];
		}

		return null;
	}

	public static string callNumber(Task callTask)
	{
		if(callTask.cnx__CTIInfo__c.Contains('ANI:'))
		{
			return '+' + callTask.cnx__CTIInfo__c.substringBetween('ANI: ', ',');
		}
		return null;
	}


	/**
	* @description copies existing task and feed item to a new record and deletes the original.  This is required because the creation of
	* the case feed item only seems to happen on insert of the task record so changing the WhatId doesnt create a new feed item.
	* @param tasks a collection of task ids that require clone/deletion of task and feeditem objects.
	*
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/94437383/newFeedItem
	*/ 
	public static void newFeedItem(Set<Id> tasks)
	{
		System.debug('**GF** newFeedItem:START');
		//get all of the fields on task and create a select query
		Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap();
		String query = 'SELECT';
		// Grab the fields from the describe method and append them to the queryString one by one.
		for(String s : objectFields.keySet()) 
		{
		   query += ' ' + s + ', ';
		}
		//remove the trailing ,
		query = query.substringBeforeLast(',') + ' FROM Task WHERE Id IN :tasks';
		List<Task> existingTasks = Database.query(query);

		//get all of the fields on feeditem and create a select query
		Map<String, Schema.SObjectField> feobjectFields = Schema.getGlobalDescribe().get('FeedItem').getDescribe().fields.getMap();
		String fequery = 'SELECT';
		// Grab the fields from the describe method and append them to the queryString one by one.
		for(String s : feobjectFields.keySet()) 
		{
		   fequery += ' ' + s + ', ';
		}

		fequery = fequery.substringBeforeLast(',') + ' FROM FeedItem WHERE ParentId IN :tasks';
		List<FeedItem> feeditems = Database.query(fequery);
		
		//create a map with the ParentId of the Task as the key
		Map<Id, FeedItem> feedMap = new Map<Id, FeedItem>();
		for(FeedItem f : feeditems)
		{
			feedMap.put(f.ParentId, f);
		}

		//new collections to hold objects which need to be modified
		List<SObject> objectsToDelete = new List<SObject>();
		List<Task> newTasks = new List<Task>();
		List<FeedItem> newFeedItems = new List<FeedItem>();
		for(Task t : existingTasks)
		{
			if(!feedMap.containsKey(t.Id))
			{
				newFeedItems.add(new FeedItem(Type = 'CallLogPost', Visibility = 'AllUsers', Status = 'Published', NetworkScope = 'AllNetworks'));
			} else {
				FeedItem f = feedMap.get(t.Id);
				newFeedItems.add(f.clone(false,true));
			}
			Task newT = t.clone(false,true);
			newT.CTI_What_Id_Changed__c = false;
			newTasks.add(newT);
			objectsToDelete.add(t);
			
			
		}

		//delete the existing tasks
		System.debug('**GF** newFeedItem:objectsToDelete' + objectsToDelete);
		System.debug('**GF** newFeedItem:newTasks' + newTasks);
		if(objectsToDelete.size() > 0)
		{
			delete objectsToDelete;
		}
		if(newTasks.size() > 0)
		{
			insert newTasks;
			// now that we have id's for the task clones we can set the parentId of the 
			// feeditem clones.
			for (Integer i = 0; i < newTasks.size(); i++)
			{
				newFeedItems[i].ParentId = newTasks[i].Id;
			}
			System.debug('**GF** newFeedItem:newFeedItems' + newFeedItems);
			insert newFeedItems;
		}
		System.debug('**GF** newFeedItem:END');
	}
}