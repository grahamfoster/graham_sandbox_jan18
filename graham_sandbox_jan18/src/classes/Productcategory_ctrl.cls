public with sharing class Productcategory_ctrl {   
    public Product_Category__c ProductCategoryRecord{get;set;}
    public list<Specifications_Meta__c> prodFeatureCategList{get;set;}
    public boolean proFeaCategClickAction{get;set;}
    public Boolean isComplete{get; private set;}
    public String pageDirection{get;set;}
   
    public Productcategory_ctrl(ApexPages.StandardController Controller){
        ProductCategoryRecord= (Product_Category__c)controller.getRecord();
        pageDirection = 'false';
         isComplete = false;
        proFeaCategClickAction = false;   
        prodFeatureCategList = new list<Specifications_Meta__c>();    
    }
    
    //===============================================   
    public Attachment attach {get;set;}
    public blob file1{get;set;}
    public String fname{get;set;}   
    public string contentype{get;set;} 
    public string attachid{get;set;}
    //This shows how to insert an Attachment
    public void upload()
    {
        attach = new Attachment();
        attach.Name = fname ;
        attach.ContentType = contentype;
        attach.body=file1; 
        if (attach.Body.size() > 2000000 ) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'You can upload up to 2mb'));
        }       
    }   
    //===============================================
    
    
    public void donext(){        
        
        if(ProductCategoryRecord.Product_Category_Name__c != null)
        {
            doSave();
        }
        else{
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Enter the Product Category Name to go next'));
        }
    }    
    
    public void saveProduct_Fea_Cat(){  
        prodFeatureCategList.add(new Specifications_Meta__c());
        proFeaCategClickAction = true;
    }
    
    public PageReference doSaveAndNew(){
        try{
            if(ProductCategoryRecord.Product_Category_Name__c != null && ProductCategoryRecord.Product_Category_Name__c.trim() != '')
            {
                insert ProductCategoryRecord;
            
                system.debug('>>>>>>>'+ProductCategoryRecord);
                if(!prodFeatureCategList.isEmpty()){
                    for(Specifications_Meta__c prodFCL: prodFeatureCategList){
                        prodFCL.Product_Category__c = ProductCategoryRecord.id;
                        prodFCL.isActive__c = true ;                
                    }
                    insert prodFeatureCategList;
                    system.debug('>>>>>>>'+prodFeatureCategList);
                }     
                return new pagereference('/apex/Product_Category');
            }
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Enter the Product category name if you want to go next'));
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
        }        
        return Null;
    }
    public PageReference doSave(){
        try{              
               isComplete = false;
                if(ProductCategoryRecord.Product_Category_Name__c != null)
                {
                    insert ProductCategoryRecord;
                }
               /* 
                // insert attachment starts here 
                if(file1 != null && ProductCategoryRecord.Id != null ){    
                    upload();
                    attach.ParentId = ProductCategoryRecord.Id;
                    system.debug('attach.ContentType....' +attach.ContentType);
                    if(attach != null && (attach.ContentType == 'image/jpg' || attach.ContentType =='image/jpeg' || attach.ContentType == 'image/png' || attach.ContentType == 'image/gif')){
                        upsert attach;  
                        attachid = attach.id;  
                        system.debug('attachid....' +attachid);
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
                    }
                    else{            
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Upload only .jpg/.jpeg/.png/.gif type of Image'));
                    }
                }
                // insert attachment ends here 
                // to insert the image link in the image filed of the product =====
                if(attachid != null && attachid.trim() != '' && ProductCategoryRecord.id != null )
                {
                    ProductCategoryRecord.Category_Icon__c = '/servlet/servlet.FileDownload?file='+attachid;
                    system.debug('&&&&&&&&&&'+ProductCategoryRecord);
                    update ProductCategoryRecord;
                }
                // =======
                if(!prodFeatureCategList.isEmpty()){
                     system.debug('>>insideif>>>>>');
                    for(Specifications_Meta__c prodFCL: prodFeatureCategList){
                        prodFCL.Product_Category__c = ProductCategoryRecord.id;
                        prodFCL.isActive__c = true ;                
                    }
                    insert prodFeatureCategList;
                    system.debug('>>>>>>>'+prodFeatureCategList);
                }             
                /* if(ProductCategoryRecord.Id!= null || (ProductCategoryRecord.Id)!='')
                {
                return new PageReference('/' +ProductCategoryRecord.Id);
                }*/
              pageDirection = 'true';        
               isComplete = true;
        }
        catch(Exception e){
            isComplete = false;
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            pageDirection = 'false';             
        }
        return null;
    }
    public pageReference doCancel(){
        pageDirection = 'true';
        string CancelURL=ApexPages.currentPage().getParameters().get('retURL');
        if(CancelURL !=null && CancelURL.trim()!=''){
            return new PageReference(CancelURL);
        }
        return null;
        }
    }