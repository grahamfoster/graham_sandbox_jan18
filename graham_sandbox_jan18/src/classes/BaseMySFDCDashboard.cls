public abstract class BaseMySFDCDashboard {
	protected User runAs;
	protected Set<Id> userIds;
	protected Date startDate;
	protected Date endDate;
	protected Decimal quota;
	protected Decimal actual;
	protected Decimal conversionRate;
	public Map<String,Decimal> conversionRateMap {get;set;}
	public MySFDCFilter filter {get;set;}

	//paging
	protected List<Opportunity> reportOpps {get;set;}
	protected Integer pageNumber;
	protected Integer pageSize;
	protected Integer totalNumberOfRecords;
	protected String sortColumn1;
	protected String sortDirection1;
	protected String sortColumn2;
	protected String sortDirection2;
	protected String report;
	
	public BaseMySFDCDashboard() {
		this.pageSize = 50;
		this.pageNumber = 1;
	}

	public abstract GoogleGaugeChartData getGoogleGaugeChartData();

	public void setRunAs(User runAs) {
		this.runAs = runAs;
		
		userIds = new Set<Id> { runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}
	}


	protected virtual String criteria() {
		//by default get all opps that are not closed lost
		return 'OwnerId in :userIds and (IsClosed = false or IsWon = false)';
	}

	protected virtual Decimal applyConversionRate(AggregateResult aggResult, Decimal amount) {
		return amount;
	}

	public virtual void query() {
		this.actual = 0;
		String marketVertical = this.filter.marketVertical;
		String productType = filter.productType;
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
		List<String> country = RegionUtil.getCountryAliases(filter.country);

		String queryCQ = 'select IsWon, SUM(Amount) amt, SUM(ExpectedRevenue) erev from Opportunity '+
			'where ' + criteria() + ' group by IsWon';

		for(AggregateResult aggr : Database.query(queryCQ)) {
			Boolean isWon = (Boolean)aggr.get('IsWon');
			Decimal amt = aggr.get('amt') != null ? (Decimal)aggr.get('amt') : 0;
			Decimal erev = aggr.get('erev') != null ? (Decimal)aggr.get('erev') : 0;

			this.actual += applyConversionRate(aggr, amt);
			//if(!isWon) this.actual += amt * (conversionRate==null ? 1 : conversionRate);
			//else this.actual += amt;
		}
	}

	public virtual Integer getRecordCount() {
		String marketVertical = filter.marketVertical;
		String productType = filter.productType;
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		system.debug('userIds:'+userIds);
		system.debug('startDate:'+startDate);
		system.debug('endDate:'+endDate);
		String soqlCountQuery = 'select COUNT(Id) c from Opportunity where ' + criteria();
		AggregateResult totalNumberOfRecordsAR = (AggregateResult)Database.query(soqlCountQuery);
		return (Integer)totalNumberOfRecordsAR.get('c');
	}

	protected virtual String defaultSorting() {
		return ' order by CloseDate desc';
	}

	protected virtual String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName';
	}

	public virtual void fetchRecords() {
		String marketVertical = filter.marketVertical;
		String productType = filter.productType;
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		this.setTotalNumberOfRecords(getRecordCount());
		
		String soqlQuery = 'select ' + selectFieldList() + ' from Opportunity where ' + criteria();

		Integer queryLimit = pageSize * pageNumber;
		if(!String.isBlank(sortColumn1) && !String.isBlank(sortDirection1)) {
			soqlQuery +=  ' order by '+sortColumn1+' ' + sortDirection1;
		}
		if(!String.isBlank(sortColumn2) && !String.isBlank(sortDirection2)) {
			if(!String.isBlank(sortColumn1) && !String.isBlank(sortDirection1)) {
				soqlQuery += ', ' + sortColumn2 + ' ' + sortDirection2;
			}else{
				soqlQuery += ' order by ' + sortColumn2 + ' ' + sortDirection2;
				sortColumn1 = sortColumn2;
				sortDirection1 = sortDirection2;
				sortColumn2 = null;
				sortDirection2 = null;
			}
		}

		if( (String.isBlank(sortColumn1) || String.isBlank(sortDirection1)) && (String.isBlank(sortColumn2) || String.isBlank(sortDirection2)) ){
			soqlQuery += defaultSorting();
		}

		soqlQuery += ' limit :queryLimit';
		system.debug('userIds:'+userIds);
		system.debug('startDate:'+startDate);
		system.debug('endDate:'+endDate);
		system.debug('**** soqlQuery ****');
		system.debug(soqlQuery);
		List<Opportunity> queriedOpps = Database.query(soqlQuery);
		Integer queriedOppsSize = queriedOpps.size();
		if(pageNumber == 1) reportOpps = queriedOpps;
		else{
			reportOpps = new List<Opportunity>();
			integer startIdx = (pageNumber-1) * pageSize;
			integer endIdx = (pageNumber * pageSize) - 1;
			for(integer i=startIdx; i<endIdx && i < queriedOppsSize; i++){
				reportOpps.add(queriedOpps[i]);
			}
		}

	}

	public virtual PageReference sortAction() {
		//fetchRecords();
		String url = '/apex/MySFDCReport?';
		url += 'r=' + EncodingUtil.urlEncode(report, 'UTF-8');
		url += '&u=' + EncodingUtil.urlEncode(runAs.Id, 'UTF-8');
		if(!String.isBlank(sortColumn1) && !String.isBlank(sortDirection1)) {
			url += '&s1='+EncodingUtil.urlEncode(sortColumn1,'UTF-8')+'&sd1='+sortDirection1;
		}
		if(!String.isBlank(sortColumn2) && !String.isBlank(sortDirection2)) {
			url += '&s2='+EncodingUtil.urlEncode(sortColumn2,'UTF-8')+'&sd2='+sortDirection2;
		}
		url += '&p='+this.pageNumber;
		return new PageReference(url).setRedirect(true);
		//return new PageReference(url);
	}

	public void setStartDate(Date d) {
		this.startDate = d;
	}

	public void setEndDate(Date d) {
		this.endDate = d;
	}

	public Decimal getQuota() {
		return quota;
	}

	public void setQuota(Decimal quota) {
		this.quota = quota;
	}

	public void setConversionRate(Decimal conversionRate) {
		this.conversionRate = conversionRate;
	}

	public Decimal getActual() {
		return actual;
	}

	/* paging */

	public PageReference nextAction() {
		//TODO
		this.pageNumber++;
		fetchRecords();
		return sortAction();
	}

	public PageReference previousAction() {
		//TODO
		this.pageNumber--;
		fetchRecords();
		return sortAction();
	}
	public void setPageNumber(Integer pageNumber){
		this.pageNumber = pageNumber;
	}

	public Integer getPageNumber() {
		return this.pageNumber;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getPageSize() {
		return this.pageSize;
	}

	public void setTotalNumberOfRecords(Integer totalNumberOfRecords) {
		this.totalNumberOfRecords = totalNumberOfRecords;
	}

	public Integer getTotalNumberOfRecords() {
		return this.totalNumberOfRecords;
	}

	public Integer getNumberOfPages() {
		return Integer.valueOf(Math.floor((totalNumberOfRecords - 1)/pageSize) + 1);
	}

	public Boolean getHasNextPage() {
		return getNumberOfPages() > pageNumber;
	}

	public Boolean getHasPreviousPage() {
		return pageNumber > 1;
	}

	public void setSortColumn1(String sortColumn1) {
		this.sortColumn1 = sortColumn1;
	}

	public String getSortColumn1() {
		return this.sortColumn1;
	}

	public void setSortDirection1(String sortDirection1) {
		this.sortDirection1 = sortDirection1;
	}

	public String getSortDirection1() {
		return this.sortDirection1;
	}

	public void setSortColumn2(String sortColumn2) {
		this.sortColumn2 = sortColumn2;
	}

	public String getSortColumn2() {
		return this.sortColumn2;
	}

	public void setSortDirection2(String sortDirection2) {
		this.sortDirection2 = sortDirection2;
	}

	public String getSortDirection2() {
		return this.sortDirection2;
	}

	public List<Opportunity> getReportOpps(){
		return reportOpps;
	}

	public void setReportOpps(List<Opportunity> reportOpps){
		this.reportOpps = reportOpps;
	}

	public String getReport(){
		return report;
	}

	public void setReport(String report){
		this.report = report;
	}
}