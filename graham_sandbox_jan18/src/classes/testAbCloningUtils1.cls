/*********************************************************************
Name  : testAbCloningUtils1
Author: Appirio, Inc.
Date  : July 30, 2008 
Usage : Used to test the AbCloningUtils1 

*********************************************************************/

public class testAbCloningUtils1{

  public static testMethod void checkAbCloningUtils1(){
   try{
    Quote__c sfdcRevisedQuote = new Quote__c();
    AbCloningUtils.reviseQuote(sfdcRevisedQuote );
    Contact c=[select Id from contact limit 1];
       sfdcRevisedQuote.Contact__c = c.Id;
    AbCloningUtils.reviseQuote(sfdcRevisedQuote );
    
    

    Quote__c q = new Quote__c(Name = 'QuoteName');
    insert q;

    String str = AbCloningUtils.cloneQuote(q.Id);
    
    AbCloningUtils.cloneProducts(q.Id, q.Id);
    
    Quote_Line_Item__c p = new Quote_Line_Item__c();
    Quote_Line_Item__c qli = AbCloningUtils.updateProductData(p);
   }
   catch(Exception e){
   }
  }
}