@isTest private class TrialEmailControllerTests {

    static Contact CreateFakeContact() {
        Contact contact = new Contact();
        contact.FirstName = 'Test';
        contact.LastName = 'Test';
        contact.Email = 'test@sciex.com';
        contact.Phone = '9056609005';
        contact.MailingStreet = '71 Four Valley Drive';
        contact.MailingCity = 'Toronto';
        contact.MailingState = 'Ontario';
        contact.MailingCountry = 'Canada';
        contact.MailingPostalCode = 'L4K 4V8';
        insert contact;
        return contact;
    }

    static LicenseActivationSettings__c CreateFakeTestSettings() {
        LicenseActivationSettings__c settings = new LicenseActivationSettings__c();
        System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
        settings.Name = UserInfo.getOrganizationId();
        settings.ActivateLicenseServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/ActivateLicense';
        settings.ActivationDetailsServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/ActivationDetails';
        settings.GetTrialDetailsServletURI__c = 'https://licensingqa.sciex.com/LicenseActivationServlet/GetTrialDetails';
        insert settings;
        return settings;
    }

    static testMethod void testTrialEmailControllerConstructor() {
        test.startTest();
        TrialEmailController controller = new TrialEmailController();
        test.stopTest();
    }
    
    static testMethod void testTrial() {
        RestRequest request = new RestRequest();
        RestContext.request = request;
        RestResponse response = new RestResponse();
        RestContext.response = response;
        request.params.put('activationId', '');
        request.params.put('sendEmail', 'true');
        CreateFakeContact();
        CreateFakeTestSettings();
        LicenseActivationMockJSONGenerator mockServlet = new LicenseActivationMockJSONGenerator(
            '{"success":true,"productName":"LibraryView","productVersion":"1.0","shipToEmail":"test@sciex.com"}',
            200
        );
        Test.setMock(HttpCalloutMock.class, mockServlet);
        
        test.startTest();
        TrialEmailController.trial();
        test.stopTest();
    }
}