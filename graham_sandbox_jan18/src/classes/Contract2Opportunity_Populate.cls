/**
 * @author Brett Moore
 * @created 
 * @Revision  
 * @Last Revision 
 * 
 * Class to populate Opportunities Line Items based on associated Service Contract Line Items
**/

public class Contract2Opportunity_Populate  extends Contract2Opportunity_Utils{

    public Contract2Opportunity_Configuration__c c2oConfig;                 // Contract2Opportunity general Configuration
    public Map<String,PriceBookEntry> PBEs;                                 //  PriceBook Entries, mapped by Product Code
    public Map<Id,SVMXC__Service_Contract_Products__c> contractLineItems; 
    public Map<Id,Opportunity_IB__c> opportunityIBs;     
    public CustomLogger.Logger productLog = new CustomLogger.Logger();
    public Integer runstate = 1;
    public List<OpportunityLineItem> insertLines;
    
    public Contract2Opportunity_Populate(Map<Id, Opportunity> initialQuery) {  
        // Initialization
        this.c2oConfig = c2oConfiguration();        
        this.ceProducts = ceSettings();        
        this.warrantyMap = wLookups();
        this.insertLines = new  List<OpportunityLineItem>();
        this.opportunityIBs = new  Map<Id,Opportunity_IB__c>();
        // Initialize custom Logging for Contracts
        productLog.loglevel = c2oConfig.Log_Level__c;
        productLog.process = 'Populate Opportunities';
        this.contractLineItems = new  Map<Id,SVMXC__Service_Contract_Products__c>([SELECT 
                    id, SVC_Item_Master_Name__r.ProductCode, SVMXC__Installed_Product__c,SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c,
                    SVC_Item_Master_Name__c, SVMXC__Line_Price__c, SVMXC__Product__c, SVMXC__Product__r.SVC_OrclSerInstruModelName__c, 
                    SVMXC__Service_Contract__c, SVMXC__Service_Contract__r.SVC_CATEGORY__C, 
                    SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c, SVMXC__Service_Contract__r.Parent_Contract_Number__c
                FROM SVMXC__Service_Contract_Products__c WHERE SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c IN :initialQuery.keySet() AND 	SVC_Contract_Subline_Status__c IN ('ACTIVE','EXPIRED','MULTIPLE YEAR - GOT PO')]);

        if(contractLineItems.size() > 0 ){      
            List<Opportunity_IB__c> oppIBs = new List<Opportunity_IB__c>([
                SELECT id, Not_in_Pricebook__c, Opportunity__c,Notes__c,On_Opportunity__c,Covered_Product__c
                FROM Opportunity_IB__c WHERE Opportunity__c IN :initialQuery.keySet()]);   
            
            for(Opportunity_IB__c IB : oppIBs){
                opportunityIBs.put(IB.Covered_Product__c, IB);
            }

            if(oppIBs.size() < 1){
                String err = 'Error- No Opportunity IBs found. Consider Reverting AutoGeneration Status to -Linked-  ';
                List<id> record = new List<id>(initialQuery.keySet());
                productLog.addLine('Error','Opportunity', record.get(0) , ' N/A ', 'Prepare' , err);
                runstate = 0;
            }
            // Check for valid runstate

            if(runstate == 1){      
                // Parse initial Opportunities for generating Pricebook Entry Query
                List <ID> pricebooks = new List <ID>();
                List <String> currencies = new List <String>();        
                for(Opportunity o :initialQuery.values()){
                    pricebooks.add(o.PRICEBOOK2ID);
                    currencies.add(o.CURRENCYISOCODE);
                }

                List <PriceBookEntry> entries = new List <PriceBookEntry>([
                    SELECT Id, Name, PRICEBOOK2ID, CURRENCYISOCODE, unitprice, productCode 
                    FROM PricebookEntry         WHERE PRICEBOOK2ID IN :pricebooks AND CURRENCYISOCODE IN :currencies]);
                this.PBEs = new Map<String,PriceBookEntry>();
                For(PricebookEntry p :entries){
                    PBEs.put(p.ProductCode, p);
                }   

                // Iterate through each Line Item
                For(SVMXC__Service_Contract_Products__c line :contractLineItems.values() ){
                    OpportunityLineItem currentProduct = new OpportunityLineItem();
                    currentProduct.OpportunityId = line.SVMXC__Service_Contract__r.c2o_Renewal_Opportunity__c;
                    currentProduct.Covered_Product__c = line.id;
                    currentProduct.Opportunity_IB__c = opportunityIBs.get(line.id).id;
                    currentProduct.Quantity = 1;
                    currentProduct.Serial_Number__c = line.SVMXC__Installed_Product__r.SVMXC__Serial_Lot_Number__c;
                    currentProduct.Model__c = line.SVMXC__Product__r.SVC_OrclSerInstruModelName__c;
                    if(line.SVMXC__Service_Contract__r.SVC_CATEGORY__C == 'Service Agreement'){  // For Contract Renewals
                        currentProduct.Type__c = 'Contract Renewal';
                        String pCode = line.SVC_Item_Master_Name__r.ProductCode + '|' + currentProduct.Model__c;
                        if(PBEs.containsKey(pCode)){                
                            currentProduct.PricebookEntryId = PBEs.get(pCode).id;
                            currentProduct.UnitPrice = line.SVMXC__Line_Price__c;
                        } else {
                            //  product not found
                            productLog.addLine('Warning','Opportunity IB', opportunityIBs.get(line.id).id , line.SVMXC__Service_Contract__r.Parent_Contract_Number__c , 'Prepare' , 'Product not in Pricebook');
                            opportunityIBs.get(line.id).Not_in_Pricebook__c = TRUE;                                     
                        }                    
                    } else {
                        currentProduct.Type__c = 'Warranty Conversion';
                        String pCode = warrantyLookup(line.SVC_Item_Master_Name__r.ProductCode,initialQuery.get(currentProduct.OpportunityId).Account_Region__c,currentProduct.Model__c);
                        if(PBEs.containsKey(pCode)){ 
                            currentProduct.PricebookEntryId = PBEs.get(pCode).id;
                            currentProduct.UnitPrice = PBEs.get(pCode).UnitPrice;
                        } else {
                            //  product not found 
                            productLog.addLine('Warning','Opportunity IB', opportunityIBs.get(line.id).id , line.SVMXC__Service_Contract__r.Parent_Contract_Number__c , 'Prepare' , 'Product not in Pricebook');
                            opportunityIBs.get(line.id).Not_in_Pricebook__c = TRUE;
                        }                                        
                    }
                    if(validLine(currentProduct)){
                        insertLines.add(currentProduct);  
                    } else {
                        productLog.addLine('Error','Opportunity IB' , opportunityIBs.get(line.id).id , line.SVMXC__Service_Contract__r.Parent_Contract_Number__c , 'Prepare' ,'invalid data');                 
                    }
                }
            }        
            // Insert Opportunity Line Items
            if(insertLines.size() > 0){
                Database.SaveResult[] srList = Database.insert(insertLines, false);        
                // Iterate through each returned result
                for (Integer c = 0; c < srList.size(); c++) {
                    if (srList.get(c).isSuccess() ) {
                        // Operation was successful, post to Log
                        productLog.addLine('Success','Opportunity Product', insertLines.get(c).PricebookEntryId , insertLines.get(c).OpportunityId , 'INSERT' , 'Opportunity Product Created ' + srList.get(c).getId()); 

system.debug('!!! - insertLines.get(c) : ' + insertLines.get(c).Covered_Product__c);                      
system.debug('!!! - opportunityIBs : ' + opportunityIBs.keySet());
                      
                        
                        opportunityIBs.get(insertLines.get(c).Covered_Product__c).On_Opportunity__c = TRUE;
                    } else {
                        // Operation failed, post errors to Log
                        Database.Error[] err = srList.get(c).getErrors();
                        String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
                        String escapeErr = errorMsg.replace(',','.');
                        productLog.addLine('Error','Opportunity Product' , insertLines.get(c).PricebookEntryId  , insertLines.get(c).OpportunityId , 'INSERT' ,  escapeErr);                 
                    }
                }
            }
        } 
        // Update Opportunity IBs
        if(OpportunityIBs.size() > 0){
            List<Opportunity_IB__c> IBList = OpportunityIBs.values();
            Database.SaveResult[] srList = Database.update(IBList, false);        
            // Iterate through each returned result
            for (Integer c = 0; c < srList.size(); c++) {
                if (srList.get(c).isSuccess() ) {
                    // Operation was successful, post to Log
                    productLog.addLine('Success','Opportunity IB' , IBList.get(c).Id , IBList.get(c).Opportunity__c , 'UPDATE' , 'Opportunity IB Updated ' + srList.get(c).getId()); 
                    initialQuery.get(IBList.get(c).Opportunity__c).c2o_AutoGeneration_Status__c = 'Populated';
                } else {
                    // Operation failed, post errors to Log
                    Database.Error[] err = srList.get(c).getErrors();
                    String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
                    String escapeErr = errorMsg.replace(',','.');
                    productLog.addLine('Error','Opportunity IB' , IBList.get(c).Id  , IBList.get(c).Opportunity__c , 'UPDATE' , escapeErr);                 
                }
            }
        }
        // Update Opportunity AutoGeneration Status
        if(OpportunityIBs.size() > 0){
            List<Opportunity> updateOpp = new List<Opportunity>(initialQuery.values());
            Database.SaveResult[] srList = Database.update(updateOpp, false);        
            // Iterate through each returned result
            for (Integer c = 0; c < srList.size(); c++) {
                if (srList.get(c).isSuccess() ) {
                    // Operation was successful, post to Log
                    productLog.addLine('Success','Opportunity' , updateOpp.get(c).Id , updateOpp.get(c).Previous_Contract__c , 'UPDATE' , 'Opportunity Updated ' + srList.get(c).getId()); 
                } else {
                    // Operation failed, post errors to Log
                    Database.Error[] err = srList.get(c).getErrors();
                    String errorMsg = err[0].getStatusCode() + ': ' + err[0].getMessage(); 
                    String escapeErr = errorMsg.replace(',','.');
                    productLog.addLine('Error','Opportunity IB' , updateOpp.get(c).Id , updateOpp.get(c).Previous_Contract__c , 'UPDATE' , escapeErr);                 
                }
            }
        }
        productLog.saveLog();   
    }
    
}

/*
 * Code for invoking via Annonymus execution
 * 
   Map<Id, Opportunity> initialQuery = new Map<Id, Opportunity>([   
        SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,
            c2o_CE_Opportunity__c, Account_Region__c,Previous_Contract__c
        FROM  
            Opportunity 
        WHERE 
            c2o_AutoGeneration_Status__c = 'Linked' 
        Limit 1
        ]);

   new Contract2Opportunity_Populate(initialQuery);
 *
 */
/* OR
  String Query = 'SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c, c2o_CE_Opportunity__c, Account_Region__c,Previous_Contract__c FROM Opportunity WHERE c2o_AutoGeneration_Status__c = \'Sanitized\'';
  Integer scope = 1;
  String process = 'Populate';
  Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
  id batchInstanceId = database.executebatch(b,scope);
 * 
 */