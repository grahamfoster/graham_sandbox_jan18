@isTest
public with sharing class product_Ctrl_test {
	public static testMethod Void Testproduct_Ctrl_test2(){
	
		Product_Category__c prodCategRecObj = Util.CreateProductCategory();
	    prodCategRecObj.isActive__c = true;
	    update prodCategRecObj;
		
		Product2 pro = new Product2(Name ='Test',Product_Category__c = prodCategRecObj.Id, IsActive = true);
	    insert pro;
	    
	    RecordType rt =[Select Id from recordtype where sobjecttype = 'Product2' and Name = 'Competitor' limit 1];
	    
	    pro.RecordTypeId = rt.Id;
	    update pro;
	    
	    Specifications_Meta__c productfeatureobj=util.CreateProductfeatureCategory(prodCategRecObj.id);
		
		Product_Meta__c prdmetaobj=util.CreateProductfeature(productfeatureobj.id);
		
		Product_Value__c prdvalueobj= util.CreateProductValue(prdmetaobj.id);
		prdvalueobj.Product__c=pro.Id;
		update prdvalueobj;
	    
	    
	    Attachment attach = new Attachment();
		attach.Name='assf';
		attach.ContentType = 'image/jpg';
		attach.ParentId = pro.Id;
		attach.Body = Blob.valueOf('sdfghj');
		insert attach;
		
		
	    ApexPages.StandardController sc = new ApexPages.standardController(pro);
	
	    Product_Ctrl prdc=new Product_Ctrl(sc);
	    prdc.doSave();
	   
	}
    public static testMethod Void Testproduct_Ctrl_test(){
	
	Product_Category__c prodCategRecObj = Util.CreateProductCategory();
	prodCategRecObj.isActive__c = true;
	update prodCategRecObj;
	ApexPages.currentPage().getParameters().put('catid',prodCategRecObj.id);
	
	Manufacturer__c mnufrecobj=util.createManufacturerABX();
	Product2 prdrecobj= util.createProduct2 (mnufrecobj.id , prodCategRecObj.id);
	
	Specifications_Meta__c productfeatureobj=util.CreateProductfeatureCategory(prodCategRecObj.id);
	Product_Meta__c prdmetaobj=util.CreateProductfeature(productfeatureobj.id);
	Product_Value__c prdvalueobj= util.CreateProductValue(prdmetaobj.id);
	prdvalueobj.Product__c=prdrecobj.Id;
	update prdvalueobj;
	
	Attachment attach = new Attachment();
	attach.Name='assf';
	attach.ContentType = 'image/jpg';
	attach.ParentId = prdrecobj.Id;
	attach.Body = Blob.valueOf('sdfghj');
	insert attach;
	
	
	ApexPages.StandardController sc = new ApexPages.standardController(prdrecobj);
	
	Product_Ctrl prdc=new Product_Ctrl(sc);
	//ApexPages.currentPage().getParameters().put('catid',prodCategRecObj.Id);
	
	
	prdc.file1=Blob.valueOf('asdsafsdfgsdghdfhdfhd'); 
	prdc.fname='sdfsdf';
	prdc.contentype='image/jpg';
	prdc.attachid=attach.id;
	prdc.prodCategoryId='fgfhfgf';
	
	prdc.doSave();
	prdc.doCancel();
	prdc.dodelete();
	prdc.check();
	//prdrecobj.IsActive = true;
	//update prdrecobj;
	prdc.recordTypeName= 'Competitor';
	prdc.check();
	prdc.recordTypeName= 'Competitor';
	prdc.doSave();
	
	prdc.recordTypeName = 'None';
	prdc.check();

	System.debug(attach+',,,,,,,,,,');
	System.debug(attach.ContentType+'///////////');
	System.debug(attach.Body.size()+'....................//');
	new Product_Ctrl.ProductMetaWrapper(new Product_Meta__c());
	new Product_Ctrl.ProductMetaWrapperup(new Product_Meta__c(),new Product_Value__c());
	
	prdc.doSave();
	prdc.doCancel();
	prdc.dodelete();
	prdc.getcheckup();
	prdc.getcheckup1();
	prdc.getrecordTypes();
	prdc.getBrand();
	prdc.getfamily();
	prdc.ok();
	
	Product_Category__c prodCategRecObj2= null;
	prdc.doSave();
	
	prdc.recordTypeName= 'Competitor';
	new Product_Ctrl.ProductMetaWrapper(new Product_Meta__c());
	
}
public static testMethod Void Testproduct_Ctrl_test1(){
	
	Product_Category__c prodCategRecObj = Util.CreateProductCategory();
	prodCategRecObj.isActive__c = true;
	update prodCategRecObj;
	ApexPages.currentPage().getParameters().put('catid',prodCategRecObj.id);
	
	Manufacturer__c mnufrecobj=util.createManufacturerABX();
	Product2 prdrecobj= util.createProduct2 (mnufrecobj.id , prodCategRecObj.id);
	Specifications_Meta__c productfeatureobj=util.CreateProductfeatureCategory(prodCategRecObj.id);
	Product_Meta__c prdmetaobj=util.CreateProductfeature(productfeatureobj.id);
	Product_Value__c prdvalueobj= util.CreateProductValue(prdmetaobj.id);
	prdvalueobj.Product__c=prdrecobj.Id;
	update prdvalueobj;
	
	Attachment attach = new Attachment();
	attach.Name='assf';
	attach.ContentType = 'image/jpg';
	attach.ParentId = prdrecobj.Id;
	attach.Body = Blob.valueOf('sdfghj');
	insert attach;
	
	ApexPages.StandardController sc = new ApexPages.standardController(prdrecobj);
	
	Product_Ctrl prdc=new Product_Ctrl(sc);

	prdc.doSave();
	prdc.doCancel();
	prdc.dodelete();
	
	prdc.file1=Blob.valueOf('asdsafsdfgsdghdfhdfhd'); 
	prdc.fname='sdfsdf';
	prdc.contentype='image/jpg';
	prdc.attachid=attach.id;
	prdc.prodCategoryId='fgfhfgf';

	System.debug(attach+',,,,,,,,,,');
	System.debug(attach.ContentType+'///////////');
	System.debug(attach.Body.size()+'....................//');
	new Product_Ctrl.ProductMetaWrapper(new Product_Meta__c());
	new Product_Ctrl.ProductMetaWrapperup(new Product_Meta__c(),new Product_Value__c());
	
	prdc.doSave();
	prdc.doCancel();
	prdc.dodelete();
	
	
	Product_Category__c prodCategRecObj2= null;
	prdc.doSave();
	
	
}

}