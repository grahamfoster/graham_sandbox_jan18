public with sharing class BattleCardProductList {

   List<Product_Category__c> prcat {get; set;}
   List<Product2> pro {get; set;}
   Set<Id> prcId = new Set<Id>();
   public string procId {get; set;}
   public map<String,list<ProdWrapper>> productMap{get;set;}
   public string selectedLevel1 {get; set;}
   public boolean clickedCell {get; set;}
   public string ProductImage {get; set;}
   public string ProductName {get; set;}
   public string prodcatId {get; set;}
   public String errormsg{get;set;}
    
   public BattleCardProductList(){
        clickedCell = false;
        prcat = [select Id, Name, Product_Category_Name__c from Product_Category__c where isActive__c = true Order By Product_Category_Name__c];
        for(Product_Category__c pr: prcat){
            prcId.add(pr.Id);
        }
        //Manufacturer__c mfr = [Select Id, Name From Manufacturer__c Where Manufacturer_Name__c =: 'ABSciex'];
        //list<Manufacturer__c> mfr = [Select Id, Name From Manufacturer__c Where Name =: 'AB Sciex' AND isActive__c =:True ];
          pro = [Select Id, Name, Product_Category__c, Image__c From Product2 Where isActive =: true AND (RecordType.Name =: 'AB Sciex' OR RecordType.Name =: 'NA PSM')AND Product_Category__c In: prcId];
        //if(mfr != null && mfr.size() > 0)
        if(pro != null && pro.size() > 0)
        {
            //pro = [Select Id, Name, Product_Category__c, Image__c From Product2 Where isActive =: true AND Manufacturer__c =: mfr[0].Id AND Product_Category__c In: prcId];
            
            if(pro != null && pro.size() > 0){
                productMap = new map<String,list<ProdWrapper>>();
                for(Product_Category__c pr: prcat){
                    list<ProdWrapper> prodList = new list<ProdWrapper>();
                    for(Product2 prod : pro){ 
                       if(pr.Id == prod.Product_Category__c ){
                           prodWrapper proWrap = new prodWrapper();
                           proWrap.ProductCategoryName = pr.Product_Category_Name__c; 
                           proWrap.ProductName = prod.Name;
                           proWrap.ProductImage = prod.Image__c;
                           proWrap.ProductCategId = pr.Id;
                           proWrap.productId = prod.Id;
                           prodList.add(prowrap);
                       }
                    }
                    productMap.put(pr.Product_Category_Name__c, prodList); 
                    errormsg='Select AB SCIEX Model';
                }
            }
            else{
                 errormsg='';
                 errormsg='No product list exist for AB Sciex manufacturer.';
           }
       } 
       else{
            errormsg='';
            errormsg='AB Sciex manufacturer does not exist.';
       
       }
       
   }
   
   public List<selectOption> level1Items {
        get {
            List<selectOption> options = new List<selectOption>();
 
                //options.add(new SelectOption('','-- Choose a Category --'));
                for (Product_Category__c pcat : [select Id, Name, Product_Category_Name__c from Product_Category__c Order By Product_Category_Name__c])
                    options.add(new SelectOption(pcat.Id,pcat.Product_Category_Name__c));
 
            return options;           
        }
        set;
   }
   
   /*public List<Product_Category__c> getProdCat(){
       
       prcat = [select Id, Name, Product_Category_Name__c from Product_Category__c Order By Product_Category_Name__c];
       return prcat;
   
   }*/
   
   public List<selectOption> getlevel2Items (){
        //get{
            List<selectOption> options = new List<selectOption>();
 
                //options.add(new SelectOption('','-- Choose a Category --'));
                for (Product2 prod : [select Id, Name from Product2 Order By Name])
                    options.add(new SelectOption(prod.Id,prod.Name));
 
            return options;           
         //}
         //set;   
   }
   
   public void parentpage(){
      clickedcell = true;
      System.debug(procId); 
      System.debug(clickedCell);
      //return null; 
      //pagereference pageref = new pagereference('/apex/absciexProductList?id='+procId);
      //pageref.setredirect(true);
      //return pageref;
         
   }
   
   
    
   
    public class ProdWrapper {
        public String ProductCategoryName{get;set;}
        public String ProductName{get;set;}
        public String ProductImage{get;set;}
        public Id ProductCategId{get;set;}
        public Id productId{get;set;}
    }
   
   
}