// ===========================================================================
// Object: AbsorbAPIClient
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Client to interact with the Absorb LMS API
// ===========================================================================
// Changes: 2016-01-30 Reid Beckett
//           Class created
// ===========================================================================
public virtual with sharing class AbsorbAPIClient implements IAbsorbAPIClient
{
	public static String apiToken = null;
	private static DateTime tokenGeneratedAt = null;
	public static Boolean TestThrowErrorOnCreateUser = false;

	//method to get new insstance
	public static IAbsorbAPIClient getInstance() 
	{
		return Test.isRunningTest() ? new TestAbsorbAPIClient() : new AbsorbAPIClient();
	}
	
	@TestVisible
	private AbsorbAPIClient() 
	{
	}

	//authenticate callout method, retrieves an api token but does not persist it until close() is called
	public void authenticate()
	{
		AbsorbLogger.startMethod('authenticate');
		if(apiToken == null) 
		{
			String existingApiToken = AbsorbUtil.getApiToken();
			if(existingApiToken == null)
			{
				HttpRequest req = new HttpRequest();
				req.setMethod('POST');
				req.setEndpoint(AbsorbUtil.getAPIAuthenticateURL());
				req.setBody(AbsorbUtil.getAuthenticateBody());
				Http httpInstance = new Http();
				HttpResponse httpRes = callout(httpInstance, req, 'authenticate');
				if(httpRes.getStatusCode() != 200) throw new AuthenticateException(httpRes.getStatusCode() + ':' + httpRes.getBody());
				apiToken = httpRes.getBody();
				if(apiToken != null && apiToken.startsWith('"') && apiToken.endsWith('"')) apiToken = apiToken.substring(apiToken.indexOf('"')+1, apiToken.lastIndexOf('"'));
				tokenGeneratedAt = DateTime.now();
			}else{
				apiToken = existingApiToken;
			}
		}
		AbsorbLogger.debug('apiToken = ' + apiToken);
		AbsorbLogger.endMethod('authenticate');
	}

	//create a user callout
	public AbsorbModel.CreateUserResponse createUser(AbsorbModel.User absorbUser)
	{
		String[] userErrors = absorbUser.validate();
		if(!userErrors.isEmpty()) throw new CreateUserException(String.join(userErrors,';'));
		authenticate();
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(AbsorbUtil.getAPICreateUserURL());
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		req.setHeader('Content-Type', 'application/json');
		String jsonBody = JSON.serializePretty(absorbUser);
		req.setBody(jsonBody);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'createUser');
		if(httpRes.getStatusCode() != 201 && httpRes.getStatusCode() != 200) throw new CreateUserException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		AbsorbModel.CreateUserResponse jsonResp = (AbsorbModel.CreateUserResponse)JSON.deserialize(httpRes.getBody(), AbsorbModel.CreateUserResponse.class);
		//if(!jsonResp.IsSuccessStatusCode) throw new CreateUserException('Did not receive a success response: '+jsonResp.StatusCode);
		return jsonResp;
	}

	//update a user callout
	public String updateUser(AbsorbModel.User absorbUser)
	{
		String[] userErrors = absorbUser.validate();
		if(!userErrors.isEmpty()) throw new UpdateUserException(String.join(userErrors,';'));
		authenticate();
		HttpRequest req = new HttpRequest();
		req.setMethod('PUT');
		req.setEndpoint(AbsorbUtil.getAPIUpdateUserURL(absorbUser.Id));
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		req.setHeader('Content-Type', 'application/json');
		String jsonBody = JSON.serializePretty(absorbUser);
		req.setBody(jsonBody);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'createUser');
		if(httpRes.getStatusCode() != 201 && httpRes.getStatusCode() != 200) throw new CreateUserException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		//AbsorbModel.CreateUserResponse jsonResp = (AbsorbModel.CreateUserResponse)JSON.deserialize(httpRes.getBody(), AbsorbModel.CreateUserResponse.class);
		//if(!jsonResp.IsSuccessStatusCode) throw new CreateUserException('Did not receive a success response: '+jsonResp.StatusCode);
		//return jsonResp;
		return httpRes.getBody();
	}

	//get all the courses callout
	public List<AbsorbModel.Course> getAllCourses()
	{
		return getCourses(new AbsorbModel.CourseFilter());
	}

	//get all available courses by user callout
	public List<AbsorbModel.Course> getAvailableCourses(String userId)
	{
		AbsorbModel.CourseFilter filter = new AbsorbModel.CourseFilter();
		filter.userId = userId;
		return getCourses(filter);
	}

	//get courses using the filter by callout
	public List<AbsorbModel.Course> getCourses(AbsorbModel.CourseFilter filter)
	{
		authenticate();
		String endpointURL = AbsorbUtil.getCoursesURL(filter.id, filter.categoryId, filter.fromDt, filter.toDt);
		if(!String.isBlank(filter.userId)) endpointURL = AbsorbUtil.getCoursesAvailableURL(filter.userId);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getCourses');
		if(httpRes.getStatusCode() != 200) throw new GetCoursesException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.Course> jsonResp = (List<AbsorbModel.Course>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.Course>.class);
		jsonResp.sort();
		return jsonResp;
	}

	//get course enrollments by user
	public List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByUser(String userId)
	{
		if(String.isBlank(userId)) throw new GetEnrollmentsException('userId is required');
		authenticate();
		String endpointURL = AbsorbUtil.getAPIEnrollmentsByUserURL(userId, null, null);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getCourseEnrollments');
		if(httpRes.getStatusCode() != 200) throw new GetEnrollmentsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.CourseEnrollment> jsonResp = (List<AbsorbModel.CourseEnrollment>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.CourseEnrollment>.class);
		return jsonResp;
	}

	//get course enrollments by course
	public List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByCourse(String courseId)
	{
		return getCourseEnrollmentsByCourse(courseId, null);
	}

	//get course enrollments by course modified since callout
	public List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByCourse(String courseId, Date modifiedSince)
	{
		if(String.isBlank(courseId)) throw new GetEnrollmentsException('courseId is required');
		authenticate();
		String endpointURL = AbsorbUtil.getAPIEnrollmentsByCourseURL(courseId, null, modifiedSince);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getCourseEnrollments');
		if(httpRes.getStatusCode() == 404) return new List<AbsorbModel.CourseEnrollment>();
		if(httpRes.getStatusCode() != 200) throw new GetEnrollmentsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.CourseEnrollment> jsonResp = (List<AbsorbModel.CourseEnrollment>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.CourseEnrollment>.class);
		return jsonResp;
	}

	//get courses enrollments by course and session
	public List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByCourseAndSession(String courseId, String sessionId)
	{
		if(String.isBlank(courseId)) throw new GetEnrollmentsException('courseId is required');
		if(String.isBlank(sessionId)) throw new GetEnrollmentsException('sessionId is required');
		authenticate();
		String endpointURL = AbsorbUtil.getAPIEnrollmentsByCourseAndSessionURL(courseId, sessionId, null, null);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getCourseEnrollments');
		if(httpRes.getStatusCode() == 404) return new List<AbsorbModel.CourseEnrollment>();
		if(httpRes.getStatusCode() != 200) throw new GetEnrollmentsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.CourseEnrollment> jsonResp = (List<AbsorbModel.CourseEnrollment>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.CourseEnrollment>.class);
		return jsonResp;
	}

	//enroll in a course callout
	public void enroll(String userId, String courseId)
	{
		if(String.isBlank(userId)) throw new GetEnrollmentsException('userId is required');
		if(String.isBlank(courseId)) throw new GetEnrollmentsException('courseId is required');
		authenticate();
		String endpointURL = AbsorbUtil.getAPIEnrollURL(userId, courseId);
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'enroll');
		if(httpRes.getStatusCode() != 200 && httpRes.getStatusCode() != 201) throw new EnrollException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		//Map<String,Object> jsonResp = JSON.deserializeUntyped(httpRes.getBody());
		//return jsonResp;
	}

	//get the departments callout
	public List<AbsorbModel.Department> getDepartments(String departmentName)
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPIDepartmentsURL(departmentName);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getDepartments');
		if(httpRes.getStatusCode() != 200) throw new GetDepartmentsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.Department> jsonResp = (List<AbsorbModel.Department>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.Department>.class);
		return jsonResp;
	}

	//get countires callout
	public List<AbsorbModel.Country> getCountries()
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPICountriesURL();
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getCountries');
		if(httpRes.getStatusCode() != 200) throw new GetCountriesException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.Country> jsonResp = (List<AbsorbModel.Country>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.Country>.class);
		return jsonResp;
	}

	//get provinces callout
	public List<AbsorbModel.Province> getProvinces(String countryId)
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPIProvincesURL(countryId);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getProvinces');
		if(httpRes.getStatusCode() != 200) throw new GetProvincesException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.Province> jsonResp = (List<AbsorbModel.Province>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.Province>.class);
		return jsonResp;
	}

	//get sessions callout
	public List<AbsorbModel.Session> getSessions(String courseId)
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPISessionsURL(courseId);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getSessions');
		if(httpRes.getStatusCode() == 404) return new List<AbsorbModel.Session>(); //prefer none found then a 404 error
		if(httpRes.getStatusCode() != 200) throw new GetSessionsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.Session> jsonResp = (List<AbsorbModel.Session>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.Session>.class);
		return jsonResp;
	}

	//NOTE: this method throws size limit error
	/*
	public List<AbsorbModel.SessionSchedule> getSessionSchedules()
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPISessionSchedulesURL();
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getSessionSchedules');
		//if(httpRes.getStatusCode() == 404) return new List<AbsorbModel.SessionSchedule>(); //prefer none found then a 404 error
		if(httpRes.getStatusCode() != 200) throw new GetSessionsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		List<AbsorbModel.SessionSchedule> jsonResp = (List<AbsorbModel.SessionSchedule>)JSON.deserialize(httpRes.getBody(), List<AbsorbModel.SessionSchedule>.class);
		return jsonResp;
	}*/

	//get the session schedule callout
	public AbsorbModel.SessionSchedule getSessionSchedule(String sessionScheduleId)
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPISessionScheduleURL(sessionScheduleId);
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'getSessionSchedule');
		if(httpRes.getStatusCode() == 404) return null;
		if(httpRes.getStatusCode() != 200) throw new GetSessionsException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		AbsorbModel.SessionSchedule jsonResp = (AbsorbModel.SessionSchedule)JSON.deserialize(httpRes.getBody(), AbsorbModel.SessionSchedule.class);
		return jsonResp;
	}

	//update the enrollments callout, triggered by update of LMS Course Enrollment in SFDC
	@future(callout=true)
	public static void updateEnrollmentFuture(String userId, String enrollmentId, String courseId, Integer status, Decimal progress) 
	{
		AbsorbAPIClient.getInstance().updateEnrollment(userId, enrollmentId, courseId, status, progress);
		AbsorbAPIClient.getInstance().close();
	}

	//update enrollment callout
	public void updateEnrollment(String userId, String enrollmentId, String courseId, Integer status, Decimal progress) 
	{
		authenticate();
		String endpointURL = AbsorbUtil.getAPIUpdateEnrollmentURL(userId);
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(endpointURL);
		req.setHeader('Authorization', AbsorbAPIClient.apiToken);
		req.setHeader('Content-Type', 'application/json');
		Map<String,Object> bodyMap = new Map<String,Object>{
			'Id' => enrollmentId,
			'CourseId' => courseId,
			'Status' => status,
			'Progress' => progress
		};
		req.setBody(JSON.serializePretty(bodyMap));
		Http httpInstance = new Http();
		HttpResponse httpRes = callout(httpInstance, req, 'updateEnrollment');
		if(httpRes.getStatusCode() != 200 && httpRes.getStatusCode() != 201) throw new UpdateEnrollmentException(httpRes.getStatusCode() + ':' + httpRes.getBody());
		String jsonResp = httpRes.getBody();
	}

	//called at the end of the transaction, make sure not to call before all callouts are made
	public void close() 
	{
		if(tokenGeneratedAt != null) {
			AbsorbUtil.updateToken(apiToken, tokenGeneratedAt);
		}
		AbsorbLogger.flush();
	}

	//generic callout method, returns the HttpResponse 
	public virtual HttpResponse callout(Http httpInstance, HttpRequest req, String methodName) 
	{
		AbsorbLogger.info('callout to ' + req.getEndpoint());
		AbsorbLogger.info('Authorization:' + req.getHeader('Authorization'));
		AbsorbLogger.info('body:'+req.getBody());
		HttpResponse resp = Test.isRunningTest() ? null : httpInstance.send(req);
		AbsorbLogger.info('response:'+(resp != null ? resp.getBody() : 'null'));
		return resp;
	}

	public class AuthenticateException extends Exception {}
	public class CreateUserException extends Exception {}
	public class UpdateUserException extends Exception {}
	public class GetCoursesException extends Exception {}
	public class GetEnrollmentsException extends Exception {}
	public class EnrollException extends Exception {}
	public class GetDepartmentsException extends Exception {}
	public class GetCountriesException extends Exception {}
	public class GetProvincesException extends Exception {}
	public class GetSessionsException extends Exception {}
	public class UpdateEnrollmentException extends Exception {}
	
	//interface definition
	public interface IAbsorbAPIClient {
		void close();
		void authenticate();
		AbsorbModel.CreateUserResponse createUser(AbsorbModel.User absorbUser);
		String updateUser(AbsorbModel.User absorbUser);
		void enroll(String userId, String courseId);
		List<AbsorbModel.Course> getAllCourses();
		List<AbsorbModel.Course> getCourses(AbsorbModel.CourseFilter filter);
		List<AbsorbModel.Course> getAvailableCourses(String userId);
		List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByUser(String userId);
		List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByCourse(String courseId);
		List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByCourse(String courseId, Date modifiedSince);
		List<AbsorbModel.CourseEnrollment> getCourseEnrollmentsByCourseAndSession(String courseId, String sessionId);
		List<AbsorbModel.Department> getDepartments(String departmentName);
		List<AbsorbModel.Country> getCountries();
		List<AbsorbModel.Province> getProvinces(String countryId);
		List<AbsorbModel.Session> getSessions(String courseId);
		//List<AbsorbModel.SessionSchedule> getSessionSchedules();
		AbsorbModel.SessionSchedule getSessionSchedule(String sessionScheduleId);
		void updateEnrollment(String userId, String enrollmentId, String courseId, Integer status, Decimal progress);
	}
	
	//Test method implementation of IAbsorbAPIClient
	public class TestAbsorbAPIClient extends AbsorbAPIClient implements IAbsorbAPIClient 
	{
		private List<AbsorbModel.Course> getTestCourses()
		{
			List<AbsorbModel.Course> courses = new List<AbsorbModel.Course>();
			for(LMS_Course__c lmsCourseSObj : [select Id, Name, LMS_Course_ID__c from LMS_Course__c]) 
			{
				AbsorbModel.Course c = new AbsorbModel.Course();
				c.ActiveStatus = 0;
				c.Id = lmsCourseSObj.LMS_Course_ID__c;
				c.Name = lmsCourseSObj.Name;
				courses.add(c);
			}

			AbsorbModel.Course c1 = new AbsorbModel.Course();
			c1.ActiveStatus = 0;
			c1.Id = AbsorbUtil.generateGUID();
			c1.Name = 'Test Course';
			c1.ExternalId = 'C1111';
			c1.CategoryId = AbsorbUtil.generateGUID();
			courses.add(c1);

			AbsorbModel.Course c2 = new AbsorbModel.Course();
			c2.ActiveStatus = 0;
			c2.Id = AbsorbUtil.generateGUID();
			c2.Name = 'Test Learning Path Course';
			c2.ExternalId = 'C1112';
			c2.CategoryId = AbsorbUtil.generateGUID();
			courses.add(c2);

            		return courses;
		}

		private List<AbsorbModel.CourseEnrollment> createTestEnrollments()
		{
			List<AbsorbModel.CourseEnrollment> courseEnrollments = new List<AbsorbModel.CourseEnrollment>();
			List<AbsorbModel.Course> courses = getTestCourses();
			if(courses.size() > 0)
			{
				Contact[] contacts = [select Id, LMS_User_ID__c from Contact where LMS_User_ID__c != null and CreatedDate >= :DateTime.now().addHours(-1)];
				for(Contact c : contacts)
				{
					AbsorbModel.CourseEnrollment enrollment = new AbsorbModel.CourseEnrollment();
					enrollment.Id = AbsorbUtil.generateGUID();
					enrollment.CourseId = courses[0].Id;
					enrollment.UserId = c.LMS_User_ID__c;
					enrollment.CourseName = courses[0].Name;
					enrollment.CourseVersionId = AbsorbUtil.generateGUID();
					enrollment.FullName = 'Test full name';
					enrollment.Score = 0;
					enrollment.DateStarted = '2016-02-12T22:52:43.33';
					enrollment.Status = 0;
					system.debug(enrollment.DateStartedDt);
					courseEnrollments.add(enrollment);
				}
			}
			return courseEnrollments;
		}

		private List<AbsorbModel.Session> getTestSessions()
		{
			AbsorbModel.Session s1 = new AbsorbModel.Session();
			s1.Id = AbsorbUtil.generateGUID();
			s1.SessionScheduleIds = new List<String>{ AbsorbUtil.generateGUID(), AbsorbUtil.generateGUID() };
			return new AbsorbModel.Session[]{ s1 };
		}

		private List<AbsorbModel.SessionSchedule> getTestSessionSchedules()
		{
			AbsorbModel.SessionSchedule s1 = new AbsorbModel.SessionSchedule();
			s1.Id = AbsorbUtil.generateGUID();
			s1.SessionId = AbsorbUtil.generateGUID();
			s1.VenueId = AbsorbUtil.generateGUID();
			s1.VenueName = 'Staples Center';
			return new AbsorbModel.SessionSchedule[]{ s1 };
		}

		//provides mock responses for test methods
		public override HttpResponse callout(Http httpInstance, HttpRequest req, String methodName) 
		{
			HttpResponse mockResponse = new HttpResponse();
			mockResponse.setStatusCode(200);
			if(methodName == 'authenticate') mockResponse.setBody('ok');
			else if(methodName == 'createUser') {
				if(AbsorbAPIClient.TestThrowErrorOnCreateUser) {
					mockResponse.setStatusCode(500);
					mockResponse.setBody('Test error on create user');
					return mockResponse;
				}
				AbsorbModel.User usr = (AbsorbModel.User)JSON.deserialize(req.getBody(), AbsorbModel.User.class);
				AbsorbModel.CreateUserResponse resp = new AbsorbModel.CreateUserResponse();
				resp.Id = AbsorbUtil.generateGUID();
				resp.Username = usr.Username;
				mockResponse.setBody(JSON.serializePretty(resp));
			}else if(methodName == 'getCourses') {
				mockResponse.setBody(JSON.serializePretty(getTestCourses()));
			}else if(methodName == 'getCourseEnrollments') {
				mockResponse.setBody(JSON.serializePretty(createTestEnrollments()));
			}else if(methodName == 'getDepartments') {
				List<AbsorbModel.Department> depts = new List<AbsorbModel.Department>();
				AbsorbModel.Department sciex = new AbsorbModel.Department();
				sciex.Id = AbsorbUtil.generateGUID();
				sciex.Name = 'Sciex';
				depts.add(sciex);
				mockResponse.setBody(JSON.serializePretty(depts));
			}else if(methodName == 'getCountries') {
				List<AbsorbModel.Country> countries = new List<AbsorbModel.Country>();
				AbsorbModel.Country can = new AbsorbModel.Country();
				can.Id = AbsorbUtil.generateGUID();
				can.Name = 'Canada';
				can.CountryCode = 'CA';
				countries.add(can);
				AbsorbModel.Country us = new AbsorbModel.Country();
				us.Id = AbsorbUtil.generateGUID();
				us.Name = 'United States';
				us.CountryCode = 'US';
				countries.add(us);
				mockResponse.setBody(JSON.serializePretty(countries));
			}else if(methodName == 'getProvinces') {
				List<AbsorbModel.Province> provs = new List<AbsorbModel.Province>();
				AbsorbModel.Province can = new AbsorbModel.Province();
				can.Id = AbsorbUtil.generateGUID();
				can.Name = 'Ontario';
				provs.add(can);
				AbsorbModel.Province us = new AbsorbModel.Province();
				us.Id = AbsorbUtil.generateGUID();
				us.Name = 'California';
				provs.add(us);
				mockResponse.setBody(JSON.serializePretty(provs));
			}else if(methodName == 'getSessions') {
				mockResponse.setBody(JSON.serializePretty(getTestSessions()));
			}else if(methodName == 'getSessionSchedules') {
				mockResponse.setBody(JSON.serializePretty(getTestSessionSchedules()));
			}else if(methodName == 'getSessionSchedule') {
				mockResponse.setBody(JSON.serializePretty(getTestSessionSchedules().get(0)));
			}
			return mockResponse;
		}
	}
}