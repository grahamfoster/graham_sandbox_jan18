/*
 *	OpportunityTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Graham Foster on 2016-10-01
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

public class OpportunityTriggerHandler extends TriggerHandler
{
    // **************************************************************************
	// 		context overrides 
	// **************************************************************************

	override protected void afterUpdate() {
        CloseCOBOWhenOppLost();
        createCOBOForFastOpportunityWhenOppWon();
		createCOBOforStandardOpportunity();
		updateCOBOonOppOwnerChange();
		reopenCOBOWhenNotLost();
	}
	
    override protected void beforeUpdate() {
        validateContactRoles();
	}
    
	override protected void beforeInsert() {
        setOpportunityRecordType();
	}
    
    //method for checking if a field has been changed
    public Boolean isChanged(sObject newSObj, String fieldName)
    {
        if(Trigger.isInsert) return true;
        else {
            sObject oldSobj = Trigger.oldMap.get(newSObj.Id);
            return newSObj.get(fieldName) != oldSobj.get(fieldName);
        }
    }
    
    /**
    ** For a record type fast opportunity
    ** If the system solution is SciexUniversity training
    ** Before the stage can be moved to Closed Won
    ** There needs to be a Contact Role added of Primary Learner to the Opportunity
    **/
    public void validateContactRoles() 
    {
        Map<Id, OpportunityContactRole> contactRolesByOppId = new Map<Id, OpportunityContactRole>();
        for(OpportunityContactRole ocr : [select Id, OpportunityId, Role from OpportunityContactRole where OpportunityId in :Trigger.newMap.keySet() and Role = 'Primary Learner'])
        {
            contactRolesByOppId.put(ocr.OpportunityId, ocr);
        }

        Id fastRecordTypeId = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('Fast').getRecordTypeId();
        for(Opportunity o : (List<Opportunity>)Trigger.new)
        {
             
            //changed on May 12/2017, check that Number_of_Training_Parts__c > 0 instead of Product_System__c == 'SCIEXUniversity Training'
            if(o.RecordTypeId == fastRecordTypeId && (o.Number_of_Training_Parts__c != null && o.Number_of_Training_Parts__c > 0) && isChanged(o, 'IsWon') && o.IsWon == true)
            {
                //check that there is a contact role of primary learner
                if(!contactRolesByOppId.containsKey(o.Id)) {
                    o.addError('For SciexUniversity Opportunity you must add a Primary Learner in Contact Roles');
                }
            }
        }
    }
        
    /*
     * CloseCOBOWhenOppLost
     * Created by Graham Foster - October 2016
     * Run after update when a Opportunity is set 
     * to deal lost or dead/Cancelled close any
     * Customer Onboarding (COBO) Records associated 
     * with this onboard
     * 
     */
    public void CloseCOBOWhenOppLost(){
        Set<Id> OppIds = new Set<Id>();
        for(Opportunity o : (List<Opportunity>)trigger.new){
            if(isChanged(o, 'StageName') && 
               (o.StageName == 'Deal Lost' || o.StageName == 'Dead/Cancelled')){
                OppIds.add(o.Id);
            }
        }
        //get a list of onboards
        List<Customer_Onboard__c> cobos = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Opportunity__c IN :OppIds];
        //close the cobo because the opp is lost
        for(Customer_Onboard__c co : cobos){
                co.Status__c = 'Lost/Cancelled';
            }
        update cobos;
    }

	/**
	* @description re-opens the Customer Onboard Record if an opportunity is un-cancelled
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/185696282/reopenCOBOWhenNotLost
	*/ 
	public void reopenCOBOWhenNotLost()
	{
		Set<Id> OppIds = new Set<Id>();
        for(Opportunity o : (List<Opportunity>)trigger.new)
		{
            //get the old value
			Opportunity oldOp = (Opportunity)trigger.oldMap.get(o.Id);
			if(isChanged(o, 'StageName') && 
               (o.StageName != 'Deal Lost' || o.StageName != 'Dead/Cancelled') &&
			   (oldOp.StageName == 'Deal Lost' || oldOp.StageName == 'Dead/Cancelled')){
                OppIds.add(o.Id);
            }
        }
		if(OppIds.size() > 0)
		{
			//get a list of onboards
			List<Customer_Onboard__c> cobos = [SELECT Id, Status__c FROM Customer_Onboard__c WHERE Opportunity__c IN :OppIds];
			for(Customer_Onboard__c co : cobos){
					if(co.Status__c == 'Lost/Cancelled')
					{
						co.Status__c = 'New';
					}
				}
			update cobos;
		}
	}

	/**
	* @description Updates the Owner, Sales Rep and Sales Manager information on the customer onboarding
	* record if the owner of an opportunity is changed
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/185630722/updateCOBOonOppOwnerChange
	*/ 
	public void updateCOBOonOppOwnerChange()
	{
		Map<Id,Opportunity> ownerChanges = new Map<Id,Opportunity>();
		for (Opportunity o : (List<Opportunity>)trigger.new)
		{
			if(isChanged(o, 'OwnerId'))
			{
				ownerChanges.put(o.Id,o);
			}
		}

		if(ownerChanges.size() > 0)
		{
			List<Customer_Onboard__c> existingCOBOS = [SELECT Id, Opportunity__c FROM Customer_Onboard__c WHERE Opportunity__c IN :ownerChanges.keySet()];
			
			for(Customer_Onboard__c cob : existingCOBOS)
			{
				Opportunity o = ownerChanges.get(cob.Opportunity__c);
				cob.OwnerId = o.OwnerId;
				cob.Sales_Manager__c = o.Manager__c;
				cob.Sales_Representative__c = o.OwnerId;
			}

			update existingCOBOS;
		}

	}

	/**
	* @description Creates a standard cobo record when an opportunity is changed to forecast or upside.  Ensures that if a 
	* cobo already exists for the opportunity that a new one is not created.
	* https://sciexbase.atlassian.net/wiki/spaces/SFDC/pages/182976513/createCOBOforStandardOpportunity
	*/ 
	public void createCOBOforStandardOpportunity()
	{
		Set<Id> OppIds = new Set<Id>();
		//get the opp record type Ids for the ones whch we want to build cobo records for.
		Id EMEAOpp = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('EU ABS Opportunity').getRecordTypeId();
		Id AMEROpp = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName().get('NA PSM ABS').getRecordTypeId();
		for(Opportunity o : (List<Opportunity>)trigger.new)
		{
            if(((isChanged(o, 'In_Forecast_Mgr__c') && o.In_Forecast_Mgr__c) || (isChanged(o, 'Mgr_Upside__c') && o.Mgr_Upside__c)) 
			&& (o.Product_System__c == 'LC MS' || o.Product_System__c == 'MS only') 
			&& (o.RecordTypeId == EMEAOpp || o.RecordTypeId == AMEROpp))
			{
                OppIds.add(o.Id);
            }
        } 

		if(OppIds.size() > 0)
		{
			List<Customer_Onboard__c> newCobos = new List<Customer_Onboard__c>();
			//list to check that the cobo record doesnt already exsit
			List<Customer_Onboard__c> existingCOBOS = [SELECT Id, Opportunity__c FROM Customer_Onboard__c WHERE Opportunity__c IN :OppIds];
			//build a set of the opp ids so we can check if one already exists
			Set<Id> existingCOBOSOppId = new Set<Id>();
			for(Customer_Onboard__c c : existingCOBOS)
			{
				existingCOBOSOppId.add(c.Opportunity__c);
			}
			//record type Id for any new records we will create.
			Id coboRecType = Customer_Onboard__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('COBO').getRecordTypeId();
			for(Opportunity o : (List<Opportunity>)trigger.new)
			{
				if(((isChanged(o, 'In_Forecast_Mgr__c') && o.In_Forecast_Mgr__c) || (isChanged(o, 'Mgr_Upside__c') && o.Mgr_Upside__c)) 
				&& (o.Product_System__c == 'LC MS' || o.Product_System__c == 'MS only') 
				&& (o.RecordTypeId == EMEAOpp || o.RecordTypeId == AMEROpp)
				&& !existingCOBOSOppId.contains(o.Id))
				{
					String region = 'EMEAI';
					if(o.RecordTypeId == AMEROpp) region = 'AMERICAS';
					newCobos.add(new Customer_Onboard__c(
					Opportunity__c = o.Id,
					Opportunity_Account__c = o.AccountId,
					Opportunity_MS__c = o.MS_Product__c,
					Opp_Order_No__c = o.Opportunity_ID_Number__c,
					OwnerId = o.OwnerId,
					Primary_Contact_from_Opportunity__c = o.Primary_Contact__c,
					Region__c = region,
					Sales_Manager__c = o.Manager__c,
					Sales_Representative__c = o.OwnerId,
					Status__c = 'New',
					RecordTypeId = coboRecType
					));
				}
			}
			if (newCobos.size() > 0)
			{
				insert newCobos;
			}
		}
	}

    /*
     * On the event of a "Fast" Opportunity being closed won, 
     * create a Customer Onboarding (COBO) record.  
     * The Opportunity's contact roles should be copied to the COBO as Onboarding Contacts records.  
     * The trigger should only allow 1 COBO to be created per Opportunity.
     * If the Opportunity to Asset lookup relationship is blank and the Serial Number is not blank, 
     * then the trigger should query for the Asset and use the asset record found to populate the COBO.
     */
    public void createCOBOForFastOpportunityWhenOppWon() {

        Set<Id> oppIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        Set<String> assetSerialNumbersForSearch = new Set<String>();
        
        Id fastRecordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
        for(Opportunity opp : (List<Opportunity>)Trigger.new) {
            //changed on May 15/2017, check that Number_of_Training_Parts__c > 0 instead of Product_System__c == 'SCIEXUniversity Training'
            if(isChanged(opp, 'IsWon') && opp.IsWon && opp.RecordTypeId == fastRecordTypeId && (opp.Number_of_Training_Parts__c != null && opp.Number_of_Training_Parts__c > 0)) {
                oppIds.add(opp.Id);
                accountIds.add(opp.AccountId);
                if(opp.Asset__c == null && !String.isBlank(opp.Serial_Number__c)) {
                    assetSerialNumbersForSearch.add(opp.Serial_Number__c);
                }
            }
        }
        
        //load the parent accounts
        Map<Id, Account> accountsById = new Map<Id, Account>([select Id, Sales_Region__c from Account where Id in :accountIds]);
        
        //load the assets by serial number
        Map<Id, Map<String, Asset>> assetsBySerialNumberByAccountId = new Map<Id, Map<String, Asset>>();
        for(Asset assetObj : [select Id, SerialNumber, AccountId from Asset where SerialNumber in :assetSerialNumbersForSearch and AccountId in :accountIds]) {
            if(assetsBySerialNumberByAccountId.containsKey(assetObj.AccountId)) {
                assetsBySerialNumberByAccountId.get(assetObj.AccountId).put(assetObj.SerialNumber, assetObj);
            }else{
                assetsBySerialNumberByAccountId.put(assetObj.AccountId, new Map<String, Asset>{ assetObj.SerialNumber => assetObj });
            }
        }
        
        //collect existing COBOs
        Map<Id, List<Customer_Onboard__c>> cobosByOppId = collectCOBOsByOppId(oppIds);
        //collect existing contact roles
        Map<Id, List<OpportunityContactRole>> contactRolesByOpportunityId = collectContactRolesByOppId(oppIds);
        
        //create new COBOs
        Map<Id, Customer_Onboard__c> cobosForInsertByOppId = new Map<Id, Customer_Onboard__c>();
        
        for(Id oppId : oppIds) {
            if(!cobosByOppId.containsKey(oppId)) {
                //create COBO
                Opportunity opp = (Opportunity)Trigger.newMap.get(oppId);
                Account acct = accountsById.get(opp.AccountId);
                Asset assetObj = null;
                if(opp.Asset__c != null) assetObj = new Asset(Id=opp.Asset__c);
                else if(!String.isBlank(opp.Serial_Number__c)) {
                    Map<String, Asset> assetsBySerialNumber = assetsBySerialNumberByAccountId.get(opp.AccountId);
                    if(assetsBySerialNumber != null) {
                        assetObj = assetsBySerialNumber.get(opp.Serial_Number__c);
                    }
                }
				
                //April 26/2017 - find contact roles to get the primary learner email
                Contact primaryLearnerContact = null;
                for(OpportunityContactRole ocr : contactRolesByOpportunityId.get(oppId)) {
                    if(ocr.Role == 'Primary Learner') {
                        primaryLearnerContact = ocr.Contact;
                    }
                }    

                Customer_Onboard__c cobo = populateCOBO(new Customer_Onboard__c(), opp, acct, assetObj, primaryLearnerContact);
                cobosForInsertByOppId.put(oppId, cobo);
            }
        }

        
        if(!cobosForInsertByOppId.isEmpty()) {
            insert cobosForInsertByOppId.values();
        }

		//load all existing Onboarding contacts, whether from existing or newly created COBOs, querying by opportunity ID        
        Map<Id, Map<Id, OnBoarding_Contacts__c>> onboardingContactsByContactIdByCOBOId = collectOnboardingContactsByContactIdByCOBOId(oppIds);
            
        //create contact roles for cobos
        Map<Id, Contact> contactUpdatesById = new Map<Id,Contact>();
        List<OnBoarding_Contacts__c> obcUpserts = new List<OnBoarding_Contacts__c>();        
        for(Id oppId : oppIds) {
            if(cobosForInsertByOppId.containsKey(oppId)) {
                //new COBO so create all OB contacts
	            Customer_Onboard__c cobo = cobosForInsertByOppId.get(oppId);
                for(OpportunityContactRole ocr : contactRolesByOpportunityId.get(oppId)) {
                    OnBoarding_Contacts__c obc = populateOnboardingContact(new OnBoarding_Contacts__c(Customer_Onboard__c = cobo.Id), ocr);
                    obcUpserts.add(obc);
                    if(shouldUpdateContact(ocr)) {
                        contactUpdatesById.put(ocr.ContactId, createContactUpdate(ocr.ContactId));
                    }
                }    
            }else{
                //existing COBO so create only the new OB contacts
                List<Customer_Onboard__c> existingCOBOs = cobosByOppId.get(oppId); //should be a list of 1, but if not we just use the first one
                if(existingCOBOs != null && !existingCOBOs.isEmpty()) {
                    Customer_Onboard__c cobo = existingCOBOs.get(0);
                	Map<Id, OnBoarding_Contacts__c> existingCOBOsByContactId = onboardingContactsByContactIdByCOBOId.get(cobo.Id);
					for(OpportunityContactRole ocr : contactRolesByOpportunityId.get(oppId)) {
                        if(existingCOBOsByContactId == null || !existingCOBOsByContactId.containsKey(ocr.ContactId)) {
                            //create new OBC
                            OnBoarding_Contacts__c obc = populateOnboardingContact(new OnBoarding_Contacts__c(Customer_Onboard__c = cobo.Id), ocr);
                            obcUpserts.add(obc);
                            if(shouldUpdateContact(ocr)) {
                                contactUpdatesById.put(ocr.ContactId, createContactUpdate(ocr.ContactId));
                            }
                        }/*else{
                            //update existing OBC?  necessary? Maybe the role would change?
                            OnBoarding_Contacts__c obc = populateOnboardingContact(existingCOBOsByContactId.get(ocr.ContactId), ocr);
                            obcUpserts.add(obc);
                        }*/
                    }
                }
            }
        }
        
        if(!obcUpserts.isEmpty()) upsert obcUpserts;
        
        if(!contactUpdatesById.isEmpty()) update contactUpdatesById.values();
    }
    
    //Collect the COBO's by Opportunity ID into a map
    private Map<Id, List<Customer_Onboard__c>> collectCOBOsByOppId(Set<Id> oppIds) {
        Map<Id, List<Customer_Onboard__c>> cobosByOppId = new Map<Id, List<Customer_Onboard__c>>();   
        if(oppIds.isEmpty()) return cobosByOppId;

        if(!oppIds.isEmpty()) {
            for(Customer_Onboard__c cobo : [select Id, Opportunity__c from Customer_Onboard__c where Opportunity__c in :oppIds]) {
                if(cobosByOppId.containsKey(cobo.Opportunity__c)) {
                    cobosByOppId.get(cobo.Opportunity__c).add(cobo);
                }else{
                    cobosByOppId.put(cobo.Opportunity__c, new List<Customer_Onboard__c>{ cobo });
                }
            }
        }
		return cobosByOppId;
    }

    //Collect the OpportunityContactRole's by Opportunity ID into a map
    private Map<Id, List<OpportunityContactRole>> collectContactRolesByOppId(Set<Id> oppIds) {
        Map<Id, List<OpportunityContactRole>> m = new Map<Id, List<OpportunityContactRole>>();   
        if(oppIds.isEmpty()) return m;

        if(!oppIds.isEmpty()) {
            for(OpportunityContactRole ocr : [select Id, ContactId, Contact.Email, OpportunityId, Role from OpportunityContactRole where OpportunityId in :oppIds]) {
                if(m.containsKey(ocr.OpportunityId)) {
                    m.get(ocr.OpportunityId).add(ocr);
                }else{
                    m.put(ocr.OpportunityId, new List<OpportunityContactRole>{ ocr });
                }
            }
        }
		return m;
    }
    
    //Collect the OnBoarding_Contacts__c's by Contact ID, and by Opportunity ID into a nested map
    private Map<Id, Map<Id, OnBoarding_Contacts__c>> collectOnboardingContactsByContactIdByCOBOId(Set<Id> oppIds) {
        Map<Id, Map<Id, OnBoarding_Contacts__c>> m = new Map<Id, Map<Id, OnBoarding_Contacts__c>>();
        for(OnBoarding_Contacts__c obc : [select Id, OnBoard_Contact__c, Customer_Onboard__c from OnBoarding_Contacts__c where Customer_Onboard__r.Opportunity__c in :oppIds]) {
            if(m.containsKey(obc.Customer_Onboard__c)) {
                m.get(obc.Customer_Onboard__c).put(obc.OnBoard_Contact__c, obc);
            }else {
                m.put(obc.Customer_Onboard__c, new Map<Id, OnBoarding_Contacts__c>{ obc.OnBoard_Contact__c => obc });
            }
        }
        return m;
    }

    //Set the values from the OpportunityContactRole to the Onboarding_Contacts__c
    private Onboarding_Contacts__c populateOnboardingContact(Onboarding_Contacts__c onboardingContact, OpportunityContactRole ocr) {
        onboardingContact.Type__c = ocr.Role;
        onboardingContact.Role__c = ocr.Role;
        onboardingContact.OnBoard_Contact__c = ocr.ContactId;
        return onboardingContact;
    }
    
    //Set the values from the Opportunity, Account, Asset, Contact to the Customer_Onboard__c
    private Customer_Onboard__c populateCOBO(Customer_Onboard__c cobo, Opportunity opp, Account acct, Asset assetObj, Contact primaryLearnerContact) {
        cobo.RecordTypeId = COBOUtil.getFastCOBORecordTypeId();
        cobo.Opportunity__c = opp.Id;
        cobo.OwnerId = opp.OwnerId;
        cobo.Opp_Order_No__c = opp.Opportunity_ID_Number__c;
        cobo.Opportunity_Account__c = opp.AccountId;
        cobo.Opportunity_MS__c = opp.MS_Product__c;
        cobo.Primary_Contact_from_Opportunity__c = opp.Primary_Contact__c;
        if(acct != null) cobo.Region__c = acct.Sales_Region__c;
        cobo.Sales_Manager__c = opp.Manager__c;
        cobo.Sales_Representative__c = opp.OwnerId;
        cobo.Status__c = 'New';
        if(assetObj != null) {
			cobo.Asset__c = assetObj.Id;
        }
        //populate Priamry Learner Email field
        if(primaryLearnerContact != null){
            cobo.Primary_Learner_Email__c = primaryLearnerContact.Email;
        }
        return cobo;
    }
    
    //return true if on creating the Onboarding Contact, the Contact should also be updated
    private Boolean shouldUpdateContact(OpportunityContactRole ocr) {
        return (ocr.Role == 'Primary Learner' || ocr.Role == 'Additional Learner');
    }

    //Create the contact update sobject, updating the Portal Status
    private Contact createContactUpdate(Id contactId) {
        //May 16 - remove setting Portal_Status__c = 'Requested'
        return new Contact(Id = contactId, LMS_Primary_Workflow__c = null);
    }

    private void setOpportunityRecordType(){
        for(Opportunity opp : (List<Opportunity>)Trigger.new) {
            if(opp.Internal_Lead_Share__c && !String.isBlank(opp.Business_Segment__c) && (opp.Business_Segment__c.contains('Software') || opp.Business_Segment__c.contains('Training'))) {
				opp.RecordTypeId = COBOUtil.getFastOpportunityRecordTypeId();
            }
        }
    }
}