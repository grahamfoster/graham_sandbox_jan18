/**
** @author Reid Beckett, Cloudware Connections
** @created Sep 11/2015
** 
** Test coverage for case clone functionality
**/
@isTest
public class CaseCloneTests 
{
    public static testMethod void test_CloneUtil()
    {
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert acct;
        
        Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
        Contact c2 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe2');
        insert new Contact[]{c1,c2};
        
        String caseType = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        Case testCase = new Case(AccountId = acct.Id, Oracle_Reference__c = 'ABC', Type = caseType, Subject = 'Test Coverage');
        insert testCase;
        system.assertEquals(caseType, testCase.Type);
        system.assertEquals('Test Coverage', testCase.Subject);
        
        CaseComment[] ccomms = new CaseComment[]{
          	new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #1'),
            new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #2')
        };
        insert ccomms;
        
        Attachment[] atts = new Attachment[]{
          	new Attachment(ParentId = testCase.Id, Name = 'A1', Body = Blob.valueOf('abc123')),
            new Attachment(ParentId = testCase.Id, Name = 'A2', Body = Blob.valueOf('def456'))
        };
        insert atts;

        CaseContactRole[] ccrs = new CaseContactRole[]{
          	new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c1.Id),
            new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c2.Id)
        };
        insert ccrs;
        
        EmailMessage[] emsgs = new EmailMessage[]{
        	new EmailMessage(ParentId = testCase.Id, TextBody='Test 1', HtmlBody='<b>Test 1</b>', Subject = 'Test Subject #1', ToAddress = 'test1@example.com'),
        	new EmailMessage(ParentId = testCase.Id, TextBody='Test 2', HtmlBody='<b>Test 2</b>', Subject = 'Test Subject #2', ToAddress = 'test2@example.com')
        };
		insert emsgs;            
        
        Test.startTest();
        Case clonedCase = new CaseCloneUtil().cloneCase(testCase.Id);
        system.assertEquals(caseType, clonedCase.Type);
        system.assertEquals('Test Coverage', clonedCase.Subject);
        Test.stopTest();
        
        system.assertEquals(ccomms.size() + 1, [select Id from CaseComment where ParentId = :clonedCase.Id].size());
        system.assertEquals(atts.size(), [select Id from Attachment where ParentId = :clonedCase.Id].size());
        system.assertEquals(ccrs.size(), [select Id from CaseContactRole where CasesId = :clonedCase.Id].size());
        system.assertEquals(emsgs.size(), [select Id from EmailMessage where ParentId = :clonedCase.Id].size());
    }
    
	public static testMethod void test_trigger_fromClosedEmail2Case()
    {
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert acct;
        
        Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
        Contact c2 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe2');
        insert new Contact[]{c1,c2};
        
        String caseType = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        Case testCase = new Case(AccountId = acct.Id, Oracle_Reference__c = 'ABC', Type = caseType, Subject = 'Test Coverage', Origin = 'Email to Case');
        insert testCase;
        system.assertEquals(caseType, testCase.Type);
        system.assertEquals('Test Coverage', testCase.Subject);
        
        CaseComment[] ccomms = new CaseComment[]{
          	new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #1'),
            new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #2')
        };
        insert ccomms;
        
        Attachment[] atts = new Attachment[]{
          	new Attachment(ParentId = testCase.Id, Name = 'A1', Body = Blob.valueOf('abc123')),
            new Attachment(ParentId = testCase.Id, Name = 'A2', Body = Blob.valueOf('def456'))
        };
        insert atts;

        CaseContactRole[] ccrs = new CaseContactRole[]{
          	new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c1.Id),
            new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c2.Id)
        };
        insert ccrs;

        CaseStatus cstatus = [select Id, MasterLabel from CaseStatus where IsClosed = true limit 1];
        testCase.Status = cstatus.MasterLabel;
        update testCase;

        Test.startTest();
        EmailMessage[] emsgs = new EmailMessage[]{
        	new EmailMessage(ParentId = testCase.Id, TextBody='Test 1', HtmlBody='<b>Test 1</b>', Subject = 'Test Subject #1', ToAddress = 'test1@example.com'),
        	new EmailMessage(ParentId = testCase.Id, TextBody='Test 2', HtmlBody='<b>Test 2</b>', Subject = 'Test Subject #2', ToAddress = 'test2@example.com')
        };
		insert emsgs;            
        Test.stopTest();
        /*
        Case clonedCase = [select Id from Case where CreatedDate >= :testCase.CreatedDate and Id != :testCase.Id];
        
        system.assertEquals(ccomms.size() + 1, [select Id from CaseComment where ParentId = :clonedCase.Id].size());
        system.assertEquals(atts.size(), [select Id from Attachment where ParentId = :clonedCase.Id].size());
        system.assertEquals(ccrs.size(), [select Id from CaseContactRole where CasesId = :clonedCase.Id].size());
        system.assertEquals(emsgs.size(), [select Id from EmailMessage where ParentId = :clonedCase.Id].size());
		*/
    }

	public static testMethod void test_trigger_fromTriggerCloneField()
    {
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert acct;
        
        Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
        Contact c2 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe2');
        insert new Contact[]{c1,c2};
        
        String caseType = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        Case testCase = new Case(AccountId = acct.Id, Oracle_Reference__c = 'ABC', Type = caseType, Subject = 'Test Coverage', Origin = 'Email to Case');
        insert testCase;
        system.assertEquals(caseType, testCase.Type);
        system.assertEquals('Test Coverage', testCase.Subject);
        
        CaseComment[] ccomms = new CaseComment[]{
          	new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #1'),
            new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #2')
        };
        insert ccomms;
        
        Attachment[] atts = new Attachment[]{
          	new Attachment(ParentId = testCase.Id, Name = 'A1', Body = Blob.valueOf('abc123')),
            new Attachment(ParentId = testCase.Id, Name = 'A2', Body = Blob.valueOf('def456'))
        };
        insert atts;

        CaseContactRole[] ccrs = new CaseContactRole[]{
          	new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c1.Id),
            new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c2.Id)
        };
        insert ccrs;

        EmailMessage[] emsgs = new EmailMessage[]{
        	new EmailMessage(ParentId = testCase.Id, TextBody='Test 1', HtmlBody='<b>Test 1</b>', Subject = 'Test Subject #1', ToAddress = 'test1@example.com'),
        	new EmailMessage(ParentId = testCase.Id, TextBody='Test 2', HtmlBody='<b>Test 2</b>', Subject = 'Test Subject #2', ToAddress = 'test2@example.com')
        };
		insert emsgs;            
        
        Test.startTest();
        testCase.TriggerClone__c = true;
        update testCase;
        Test.stopTest();

        testCase = [select Id, TriggerClone__c, CreatedDate from Case where Id = :testCase.Id];
        system.assertEquals(true, testCase.TriggerClone__c);
        
        Case clonedCase = [select Id from Case where CreatedDate >= :testCase.CreatedDate and Id != :testCase.Id];
        
        system.assertEquals(ccomms.size() + 1, [select Id from CaseComment where ParentId = :clonedCase.Id].size());
        system.assertEquals(atts.size(), [select Id from Attachment where ParentId = :clonedCase.Id].size());
        system.assertEquals(ccrs.size(), [select Id from CaseContactRole where CasesId = :clonedCase.Id].size());
        system.assertEquals(emsgs.size(), [select Id from EmailMessage where ParentId = :clonedCase.Id].size());
    }
    
    public static testMethod void test_trigger_limits()
    {
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
        insert acct;
        
        Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
        Contact c2 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe2');
        insert new Contact[]{c1,c2};
        
        String caseType = Case.Type.getDescribe().getPicklistValues().get(0).getValue();
        Case[] cases = new Case[]{};
            
		for(integer i=0;i<=20;i++){
	        Case testCase = new Case(AccountId = acct.Id, Oracle_Reference__c = 'ABC', Type = caseType, Subject = 'Test Coverage '+i, Origin = 'Email to Case');
            cases.add(testCase);
		}
        insert cases;

        EmailMessage[] emsgs = new EmailMessage[]{};
        CaseComment[] ccomms = new CaseComment[]{};
        Attachment[] atts = new Attachment[]{};
        CaseContactRole[] ccrs = new CaseContactRole[]{};
        for(Case testCase : cases){
          	ccomms.add(new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #1'));
            ccomms.add(new CaseComment(ParentId = testCase.Id, IsPublished=true, CommentBody = 'Comment #2'));
          	atts.add(new Attachment(ParentId = testCase.Id, Name = 'A1', Body = Blob.valueOf('abc123')));
            atts.add(new Attachment(ParentId = testCase.Id, Name = 'A2', Body = Blob.valueOf('def456')));
          	ccrs.add(new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c1.Id));
            ccrs.add(new CaseContactRole(CasesId = testCase.Id, Role = 'Other', ContactId = c2.Id));
            emsgs.add(new EmailMessage(ParentId = testCase.Id, TextBody='Test 1', HtmlBody='<b>Test 1</b>', Subject = 'Test Subject #1', ToAddress = 'test1@example.com'));
            emsgs.add(new EmailMessage(ParentId = testCase.Id, TextBody='Test 2', HtmlBody='<b>Test 2</b>', Subject = 'Test Subject #2', ToAddress = 'test2@example.com'));
        }        
        insert ccomms;
        insert atts;
        insert ccrs;
        insert emsgs;

        Test.startTest();
        
        CaseStatus cstatus = [select Id, MasterLabel from CaseStatus where IsClosed = true limit 1];
        for(Case testCase : cases)
        {
	        testCase.Status = cstatus.MasterLabel;
        }
        update cases;
        Test.stopTest();
    }
}