// ===========================================================================
// Object: AbsorbPurgeBatchable
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Purges deleted records from SF.  Records that are deleted in LMS are deleted from SF by this job
// ===========================================================================
// Changes: 2016-06-17 Reid Beckett
//           Class created
// ===========================================================================
global class AbsorbPurgeBatchable implements Database.AllowsCallouts, Database.Batchable<sObject>, Schedulable
{
	global static final Integer ENROLLMENTS_COURSE_BATCH_SIZE = 10;
	
	//Batchable interface implementation
	global Database.QueryLocator start(Database.BatchableContext BC) 
	{
		AbsorbLogger.startMethod('AbsorbPurgeBatchable.start');
		new AbsorbSyncManager().syncCourses();
		return Database.getQueryLocator([select Id, LMS_Course_ID__c from LMS_Course__c]);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
		List<LMS_Course_Enrollment__c> deletes = new List<LMS_Course_Enrollment__c>();
		List<LMS_Session__c> sessionDeletes = new List<LMS_Session__c>();
		
		LMS_Course_Enrollment__c[] courseEnrollments = [select Id, LMS_Enrollment_ID__c from LMS_Course_Enrollment__c where LMS_Course__c in :scope];
		LMS_Session__c[] courseSessions = [select Id, LMS_Session_ID__c from LMS_Session__c where LMS_Course__c in :scope];
		Map<String,AbsorbModel.CourseEnrollment> enrollmentsMap = new Map<String,AbsorbModel.CourseEnrollment>();
		Map<String,AbsorbModel.Session> sessionsMap = new Map<String,AbsorbModel.Session>();
		
		for(LMS_Course__c lmsCourse : (List<LMS_Course__c>) scope)
		{
			List<AbsorbModel.CourseEnrollment> enrollments = absorbClient.getCourseEnrollmentsByCourse(lmsCourse.LMS_Course_ID__c, null);
			for(AbsorbModel.CourseEnrollment enrollment : enrollments) 
			{
				enrollmentsMap.put(enrollment.Id, enrollment);
			}		
            
            //Aug 15/2017 - add purge of LMS Sessions
            List<AbsorbModel.Session> sessions = absorbClient.getSessions(lmsCourse.LMS_Course_ID__c);
            for(AbsorbModel.Session s : sessions) {
                sessionsMap.put(s.Id, s);
            }
		}

		for(LMS_Course_Enrollment__c sfEnrollment : courseEnrollments) 
		{
			if(!enrollmentsMap.containsKey(sfEnrollment.LMS_Enrollment_ID__c)) 
			{
				deletes.add(sfEnrollment);
			}
		}	
				
		for(LMS_Session__c sfSession : courseSessions) 
		{
			if(!sessionsMap.containsKey(sfSession.LMS_Session_ID__c)) 
			{
				sessionDeletes.add(sfSession);
			}
		}	

        if(!deletes.isEmpty()) {
			delete deletes;
			Database.emptyRecycleBin(deletes);
		}

    	if(!sessionDeletes.isEmpty()) {
			delete sessionDeletes;
			Database.emptyRecycleBin(sessionDeletes);
		}
	}

	global void finish(Database.BatchableContext BC) {}
	
	//schedulable interface implementation
	global void execute(SchedulableContext sc) 
	{
		Database.executeBatch(new AbsorbPurgeBatchable(), ENROLLMENTS_COURSE_BATCH_SIZE);
	}
}