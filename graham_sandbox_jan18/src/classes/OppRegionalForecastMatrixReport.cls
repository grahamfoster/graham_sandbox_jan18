/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : December, 2012
Description    : This class is the controller page OppRegionalForecastMatrixReport.
History        :

*/
public class OppRegionalForecastMatrixReport
{ 

  Public String RolButName
  {
    get
    {
      String d =  Apexpages.currentPage().getparameters().get('id'); 
      return d;
    }
  }
    public User LogUser 
    {
        get
        {

            User Us = [SELECT ID, Name,DefaultCurrencyIsoCode, Title, UserRole.Name, UserRole.Id, Profile.Name, UserRoleId, License_Region__c From User WHERE Id=:UserInfo.getUserId()  limit 1];
            return Us;
        }
        set;
    }  
    
    public String VisHomeBut {get; set;}             
             
             
    //Create a collection of type OppHelper  
    public List<OppHelper> AmericasSales 
    {
        get
        {
      List<CurrencyType> Curr = new List<CurrencyType>();
      Curr = [Select IsoCode, ConversionRate From CurrencyType];
      map<String,double> CurrMap = new map<String,double>();
      for(CurrencyType a: Curr)
      {
        CurrMap.put(a.IsoCode, a.ConversionRate);
      }
          List<UserRole> Ur = new List<UserRole>();
            List<OppHelper> asas = new List<OppHelper>();
          if((RolButName == LogUser.UserRole.Id) || (RolButName == null))
          {       
            VisHomeBut = 'none';     
              asas.add(new OppHelper(LogUser.UserRole, CurrMap, LogUser)); 
              if(DochRole(LogUser.UserRole.Id) != null)
              {
              Ur.addall(DochRole(LogUser.UserRole.Id));              
              }                                           
              for(UserRole b: Ur)   
              {
                  asas.add(new OppHelper(b, CurrMap, LogUser)); 
              }    
          }  
          else
          {
            VisHomeBut = '';
            UserRole RoleDown = [SELECT ID, Name FROM UserRole WHERE ID=:RolButName limit 1];
              asas.add(new OppHelper(RoleDown, CurrMap, LogUser));
              if(DochRole(RolButName) != null)
              {
              Ur.addall(DochRole(RolButName));              
              }               
                            
              for(UserRole b: Ur)   
              {
                  asas.add(new OppHelper(b, CurrMap, LogUser));
              }            
          }                  
            return asas;   
        }
        set;
    }  
    //Create a List which contains the child roles of the first level.
    public List<UserRole> DochRole(Id IDLog)
    {
          List<UserRole> PerUr = new List<UserRole>();
          PerUr = [Select ID, Name  FROM UserRole WHERE ParentRoleId =: IDLog AND  Name !='Global Support'];     
          if(PerUr != null)
          {
            return PerUr;
          }
          else
          {
            return null;
          }         
    } 
    //Create a List which contains the child roles of the two level.
    public List<UserRole> Ur2DochRole(List<UserRole> ListRol)
    {
          List<UserRole> PerUr = new List<UserRole>();
          List<ID> PerUr2Id = new List<ID>(); 
          if(ListRol != null)
            {
            for(UserRole zxc: ListRol) 
            {
              PerUr2Id.add(zxc.Id);
            }        
            PerUr = [Select ID, Name, ParentRoleId  FROM UserRole WHERE ParentRoleId IN: PerUr2Id]; 
            if(PerUr.size()>0)
            {
              return PerUr;
            }
            else
            {
              return null;
            }
          }
          else
          {
            return null;
          }       
    }
    
 
}