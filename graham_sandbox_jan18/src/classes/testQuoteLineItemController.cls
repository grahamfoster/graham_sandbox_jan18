/*********************************************************************
Name  : testQuotingLineItemController
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuotingLineItemController     

*********************************************************************/

public class testQuoteLineItemController{

  public static testMethod void methodQuoteLineItemController() {
 
    Test.setCurrentPageReference(new PageReference('Page.QuoteDiscountZAUABD'));
    Quote__c qTest = new Quote__c();
    Database.SaveResult qInserted = Database.insert(qTest,false); 
    Quote_Line_Item__c  QLI = new Quote_Line_Item__c();
    QLI.Quote__c = qInserted.getId();
    insert QLI;
    ApexPages.StandardController sc = new ApexPages.StandardController(QLI);
    QuoteLineItemController  QLExt = new QuoteLineItemController(sc);     
    String str=''; 
    str = QLExt.getQuoteId();
    str = QLExt.getErrorMessage();
    QLExt.setQuoteId('');
    QLExt.saveProduct();
    PageReference reference = QLExt.QuoteSaveDiscount();
    reference = QLExt.QuoteProductSelect();
    reference = QLExt.QuoteSaveMaterials();
    reference = QLExt.QuoteRefreshSingleLineItemPricingDRAFT();
    QuotePageRedirects q=new QuotePageRedirects();
                       QuotePageRedirects.LandingPage();
                       QuotePageRedirects.NewHeader();
                       QuotePageRedirects.NewPartner();
                       QuotePageRedirects.NewOptions();
                       QuotePageRedirects.ProductSelect();
                       QuotePageRedirects.productDiscount();
                       QuotePageRedirects.goToProductDiscountZAUABD();
                       QuotePageRedirects.goToProductDiscountZDF1();
						QuotePageRedirects.ProductSort();
						QuotePageRedirects.OptionSort();
						QuotePageRedirects.ProductSearch();
						QuotePageRedirects.goToProductHierchy();
						QuotePageRedirects.TemplateSearch();
						
						                       
   QuotingController quot=new QuotingController();
  
					quot.quoteSearchCDI();
					quot.quoteSearchSAP();quot.quoteSearchReset();
					quot.doSorting();quot.sortResult();
					quot.showLocalDraftQuotes();
					quot.viewQuote();quot.cancelQuote();
					quot.viewSAPQuote();quot.getUserSelection();
					quot.editExistingQuote();quot.AddProductsFromQuote();
					quot.CloneQuote();quot.QuoteProductSearch();
					quot.QuoteReturnLog();quot.getquoteProxy();
					quot.getSelectedTemplate();quot.getErrorMessage();
					quot.getProduct();quot.getProductCategory();
					quot.getProductDesciption();
					quot.getProductHierarchyCriteria();
					quot.getIGORCodeDescValue();quot.getPlanCodeDescValue();
					quot.getIGORItemClassValue();quot.getProductLineDescValue();
					quot.getIGORItemClassDescValue();quot.getPlanCodeValue();
					quot.getIGORCodeValue();quot.getIGORCodeHiddenValue();
					quot.getProductLineValue();quot.getCdiUsername();
					quot.getFilter_Account();quot.getFilter_Contact();
                    quot.getiGORCode();quot.getFilter_QuoteStatus();
                   // quot.retrieveProductPicklists();
                      quot.loginToCDI();
                     quot.addProductsToQuote();
                    quot.goToProductHierarchyScreen();quot.searchProductHierarchy();
                    
                    
   
  }

}