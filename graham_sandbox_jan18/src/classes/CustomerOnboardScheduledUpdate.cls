/* Class to update the Customer_Onboard Object with data from
 * other objects using references
 *
 * Class simply switches the value of Update_Record__c to force
 * the record update and the process builder to re-evaluate
 * the status of the record
 * 
 * Author Graham Foster August 2016
 * 
 * EDIT January 2017 - Added Update to Case Object
 * 
 */

public class CustomerOnboardScheduledUpdate implements Schedulable{
    
    public void execute(SchedulableContext sc){
         runCoboUpdateAsync.futureUpdateCobo();     
    }

}