/*
 *	EmailMessageTrigger_Test
 *	
 *	Test class for EmailMessageTrigger and EmailMessageTriggerHandler.
 *
 *	If there are other test classes related to EmailMessageTrigger, please document it here (as comments).
 * 
 * 	Created by Brett Moore 2016-08-16 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
public class EmailMessageTrigger_Test {

    	// IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] EmailMessageTrigger_Test.test() [Begin]');
		RecordType SRRecType = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
        Case testCase = new Case(Subject = 'Test Case', RecordTypeId=SRRecType.id );
        insert testCase;
    	EmailMessage iudu = new EmailMessage(ParentId = testCase.Id, TextBody = 'Hello', Subject = 'test', FromAddress = 'sciexnow@absciex.com',ToAddress='Test@Test.com');
		insert iudu;
		update iudu;
		delete iudu;
		undelete iudu;
        system.debug('[TEST] EmailMessageTrigger_Test.test() [End]');
    }
	@isTest  static void test_cloneClosedSR() {
        //first create a case to assign the emails to 
        RecordType SRRecType = SfdcUtil.getRecordType('Case', 'SVC_Service_Request');
        Case testCase = new Case(Subject = 'Test Case', RecordTypeId=SRRecType.id );
        insert testCase;
        //now we test some things
        //first that a portal email is created for a case (the test case)
        EmailMessage test1 = new EmailMessage(ParentId = testCase.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.\n\n\n' +
														'-----Ursprüngliche Nachricht-----\n' + 
														'Von: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne', Subject = 'test [ ref: 01234]', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='Test@Test.com');
            
        test.startTest();
        	system.debug('[TEST] EmailMessageTrigger_Test.test_cloneClosedSR() [Begin]');        
			// Insert Case
        	insert test1;
			//close the Case
			testCase.Status='Closed';
        	// update Case
        	update testCase;
        	//Send another Email Message to the Case
        	EmailMessage test2 = new EmailMessage(ParentId = testCase.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.\n\n\n' +
														'-----Ursprüngliche Nachricht-----\n' + 
														'Von: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne', Subject = 'test [ ref: 01234]', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='Test@Test.com');
        	insert test2;
        
			//Verify that there are now 2 cases (original plus cloned case)
        	List<Case> cases = [SELECT Id FROM Case];
        	system.assert(cases.size() == 2);
        	system.debug('[TEST] EmailMessageTrigger_Test.test_cloneClosedSR() [End]');          
		test.stopTest();
        
    	}

		@isTest static void test_createPortalBody()
		{
			Portal_Email_Settings__c em1 = new Portal_Email_Settings__c(Name = 'test1', Setting_Type__c = 'From Splitter', Value__c = 'VON:');
			Portal_Email_Settings__c em2 = new Portal_Email_Settings__c(Name = 'test2', Setting_Type__c = 'Exclude Subject Line', Value__c = 'test [ ref: 0123456]');
			Portal_Email_Settings__c em3 = new Portal_Email_Settings__c(Name = 'test3', Setting_Type__c = 'From Splitter', Value__c = 'FROM:');
			insert new List<Portal_Email_Settings__c>{em1,em2,em3};
			
			Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
			insert acct;

			Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1', Email = 'jdoe@example.com');
			insert c1;
        
			List<RecordType> rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'CICCase' LIMIT 1];
        
			Case cs1 = new Case(ContactId = c1.Id, AccountId = acct.Id,  
								Subject = 'test', Description = 'Case Test', 
								RecordTypeId = rt[0].Id);
			insert cs1;

			EmailMessage test1 = new EmailMessage(ParentId = cs1.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.', 
														Subject = 'test [ ref: 01234]', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='Test@Test.com');
			EmailMessage test2 = new EmailMessage(ParentId = cs1.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.\n\n\n' +
														'-----Ursprüngliche Nachricht-----\n' + 
														'Von: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne', Subject = 'test [ ref: 012345]', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='jdoe@example.com');
			EmailMessage test3 = new EmailMessage(ParentId = cs1.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.\n\n\n' +
														'-----Ursprüngliche Nachricht-----\n' + 
														'From: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne' + 
														'Von: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne', Subject = 'test [ ref: 0123456]', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='jdoe@example.com');
			EmailMessage test4 = new EmailMessage(ParentId = cs1.Id,
													TextBody = 'Hallo Frau Bernard\nFrom: SCIEX Now [mailto:sciexnow@absciex.com]', Subject = 'test [ ref: 01234567]', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='jdoe@example.com');

			insert new List<EmailMessage>{test1,test2,test3,test4};

			//now lets test some stuff
			List<EmailMessage> testEm = [SELECT Show_On_Portal__c, Portal_Email_Body__c, Subject FROM EmailMessage WHERE ParentId = :cs1.Id];

			for(EmailMessage em : testEm)
			{
				if(em.Subject == 'test [ ref: 012345]')
				{
					System.assert(em.Show_On_Portal__c);
				} else if (em.Subject == 'test [ ref: 0123456]'){
					System.assert(!em.Show_On_Portal__c);
				}
				else if (em.Subject == 'test [ ref: 01234]'){
					System.assert(!em.Show_On_Portal__c);
				}
				if(em.Subject == 'test [ ref: 01234567]')
				{
					System.assertEquals('Hallo Frau Bernard',em.Portal_Email_Body__c);	
				} else {
					System.assertEquals('Hallo Frau Bernard,\n\n' +
										'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                        'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.', em.Portal_Email_Body__c);
				}
			}


		}
    
}