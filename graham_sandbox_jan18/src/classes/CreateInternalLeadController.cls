// ===========================================================================
// Object: CreateInternalLeadController
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: standard controller extension for the VF page off of the custom
// buttons "Create Internal Lead" on the Case and Contact.
// ===========================================================================
// Changes: 2017-03-24 Reid Beckett
//           Class created
// ===========================================================================
public class CreateInternalLeadController 
{
    private static final String SECTION_HEADER_SUBTITLE = 'Create Internal Lead';
    private static final String PAGE_BLOCK_TITLE = 'Create a lead for this contact: Please select below which sales teams should receive a lead for immediate follow up.';
    private static final String BUSINESS_UNIT_LABEL = 'Check which business segment should receive this lead first (Primary Routing)';
    private static final String COMMENTS_LABEL = 'Insert your comments regarding the lead below';
    private static final String ROLE_LABEL = 'What is your role?';    
    private static final String BUTTON_LABEL = 'Submit Lead';

//    private static final String LEAD_OWNER_QUEUE_NAME = 'AssignTriggerQueue';

    private static final String ERR_BUSINESS_UNIT_REQUIRED = 'Business Unit is required';
    private static final String ERR_ROLE_REQUIRED = 'Your Role is required';    
    private static final String ERR_BUSINESS_UNIT_INVALID = 'The selected Business Unit {0} is invalid';
    private static final String ERR_QUEUE_NOT_FOUND = 'The queue named {0} was not found';

    private static final String ERR_CAMPAIGN_NOT_FOUND = 'The campaign named {0} was not found';

/*    private static final Map<String, String> ContactToLeadRecordTypeMapping = new Map<String, String> {
        'AMERICAS Contact Record' => 'ABS NA MASS SPEC',
        'APAC Contact Record' => 'APAC AB Sciex Lead Record Type',
        'EMEAI Contact Record' => 'EU ABS Lead Record Type',
        'JAPAN Contact Record' => 'JP ABS Lead Record Type'            
    };
    */

        //Align User's License Region field to appropriate Lead Record Type
    private static final Map<String, String> ContactToLeadRecordTypeMapping = new Map<String, String> {
        'Americas' => 'ABS NA MASS SPEC',
        'ROA' => 'APAC AB Sciex Lead Record Type',
        'EMEA' => 'EU ABS Lead Record Type',
        'Japan' => 'JP ABS Lead Record Type',
        'India' => 'EU ABS Lead Record Type',
        'China' => 'APAC AB Sciex Lead Record Type',
        'Global' => 'ABS NA MASS SPEC'            
    };       

    //find the region on the User -- this will default the RecType since the RecType on the contact may not be correct
    User u = [Select License_Region__c, Name, Email from User where ID =: UserInfo.getUserId()];        

    Id sobjectId;
    Contact theContact;
    Case theCase;
    public String businessUnit {get;set;}
    public String comments {get;set;}
    public String role {get;set;}    
    String campName;
    String Business_Segment;
    String UTM_Term_Most_Recent;
    

    public CreateInternalLeadController(ApexPages.StandardController stdController)
    {
        this.sobjectId = stdController.getId();
    }

    public PageReference view() 
    {
        if(isCase) theCase = findCase(this.sobjectId);
        else if(isContact) theContact = findContact(this.sobjectId);
        return null;
    }
    
    public PageReference save()
    {

        try {
            if(String.isBlank(businessUnit))
            {
                SfdcUtil.addErrorMessageToPage(ERR_BUSINESS_UNIT_REQUIRED);
                return null;
            }

            if(String.isBlank(role))
            {
                SfdcUtil.addErrorMessageToPage(ERR_ROLE_REQUIRED);
                return null;
            }            
            
            CreateInternalLeadBusinessUnit__c bizUnitSetting = CreateInternalLeadBusinessUnit__c.getAll().get(businessUnit);
            if(bizUnitSetting == null) 
            {
                String errorMessage = String.format(ERR_BUSINESS_UNIT_INVALID, new String[]{ businessUnit });
                SfdcUtil.addErrorMessageToPage(errorMessage);
                return null;
            }

                    //find the campaign
                    List<Campaign> campaigns = [select Id, Name from Campaign where Name = :bizUnitSetting.Lead_Assignment_Queue_Name__c and IsActive = true];
                    if(campaigns.isEmpty()) {
                        String errorMessage = String.format(ERR_CAMPAIGN_NOT_FOUND, new String[]{ bizUnitSetting.Lead_Assignment_Queue_Name__c });
                        SfdcUtil.addErrorMessageToPage(errorMessage);
                        return null;
                    }
                    campName = campaigns.get(0).Name;
                    Id campaignId = campaigns.get(0).Id;
            
                    //Jun/2017 - change to find matching lead by email
                    String email = isContact ? theContact.Email : theCase.Contact.Email;
                    Lead[] leadMatches = [select Id, Email, Status from Lead where Email = :email and IsConverted = false];
                    
                    //create Lead
                    Lead leadObj = new Lead(Lead_Description__c = comments);

                    //populate lead with Case or Contact info
                    if(isContact) 
                    {
                        populateLead(leadObj, theContact, (leadMatches.isEmpty() ? null : leadMatches.get(0)));
                    }
                    
                    if(isCase) 
                    {
                        populateLead(leadObj, theCase.Contact, (leadMatches.isEmpty() ? null : leadMatches.get(0)));
                    }     
            
                    Database.DMLOptions dmo = new Database.DMLOptions();
                    if(leadObj.Id == null) {
                //NEW
                dmo.assignmentRuleHeader.useDefaultRule = false;
                Database.insert(leadObj, dmo);
                    //Commenting out to replace triggering lead assignment rules via process builder
                    //Current issue, lead assignment rules fire TWICE for some reason
                    
        
        if(bizUnitSetting != null && !String.isBlank(bizUnitSetting.Business_Segment__c)) {
            Business_Segment = bizUnitSetting.Business_Segment__c;
            UTM_Term_Most_Recent = bizUnitSetting.Business_Segment__c;
        }else{
            Business_Segment = 'MS';
            UTM_Term_Most_Recent = 'MS';            
        }
                        
                        
                //create CampaignMember
                CampaignMember campMember = new CampaignMember(
                    LeadId = leadObj.Id, 
                    CampaignId = campaignId, 
                    Internal_Lead_Share__c = TRUE,
                    DHR_Sales_Rep_Name__c = u.Name,
                    DHR_Sales_Rep_Email__c = u.Email,
                    DHR_Sales_Role__c = role,                    
                    UTM_Campaign_Most_Recent__c = 'Generic',
        			UTM_Content_Most_Recent__c = 'Internal Lead Share',
        			UTM_Medium_Most_Recent__c = 'Partners',
        			UTM_Source_Most_Recent__c = 'SFDC',
                    UTM_Term_Most_Recent__c = UTM_Term_Most_Recent
                );
                insert campMember;
                        
                        }else {
                //UPDATE
                if(!leadMatches.isEmpty() && leadMatches.get(0).Status == 'Nurture' ){
                    dmo.assignmentRuleHeader.useDefaultRule = false;
                    //Commenting out to replace triggering lead assignment rules via process builder
                    //Current issue, lead assignment rules fire TWICE for some reason
                }
                Database.update(leadObj, dmo);
                        
                CampaignMember[] existingCampaignMembers = [select Id from CampaignMember where LeadId = :leadObj.Id and CampaignId = :campaignId];

                if(existingCampaignMembers.isEmpty()) {
                    //create CampaignMember
                    CampaignMember campMember = new CampaignMember(
                        LeadId = leadObj.Id, 
                        CampaignId = campaignId, 
                        Internal_Lead_Share__c = TRUE,
                        DHR_Sales_Rep_Name__c = u.Name,
                        DHR_Sales_Rep_Email__c = u.Email,
                        DHR_Sales_Role__c = role,  
                    	UTM_Campaign_Most_Recent__c = 'Generic',
        				UTM_Content_Most_Recent__c = 'Internal Lead Share',
        				UTM_Medium_Most_Recent__c = 'Partners',
        				UTM_Source_Most_Recent__c = 'SFDC',
                    	UTM_Term_Most_Recent__c = UTM_Term_Most_Recent                        
                        );
                    insert campMember;
                }
                    }
            
                    return new ApexPages.StandardController(leadObj).view();
                }catch(DMLException e){
                    SfdcUtil.addDMLExceptionMessagesToPage(e);
                    return null;
                }catch(Exception e){
                    SfdcUtil.addExceptionMessageToPage(e);
                    return null;
                }

            }


    private void populateLead(Lead leadObj, Contact c, Lead matchingExistingLead) 
            { 
            CreateInternalLeadBusinessUnit__c bizUnitSetting = CreateInternalLeadBusinessUnit__c.getAll().get(businessUnit);
                
            if(matchingExistingLead != null) {
                    leadObj.Id = matchingExistingLead.Id;
                    if(matchingExistingLead.Status == 'Open' || matchingExistingLead.Status == 'In-Progress' || matchingExistingLead.Status == 'Nurture') {
                        leadObj.Camp__c = campName;
                leadObj.Internal_Lead_Share__c  = true;
                leadObj.DHR_Sales_Rep_Name__c = UserInfo.getName();
                leadObj.DHR_Sales_Rep_Email__c  = UserInfo.getUserEmail();
                leadObj.DHR_Sales_Share_Date__c = system.now();          
        		leadObj.Marketing_Channel_Most_Recent__c = 'Partners';
		        leadObj.UTM_Campaign_Most_Recent__c = 'Generic';
        		leadObj.UTM_Content_Most_Recent__c = 'Internal Lead Share';
		        leadObj.UTM_Medium_Most_Recent__c = 'Partners';                
        		leadObj.UTM_Source_Most_Recent__c = 'SFDC'; 
				leadObj.DHR_Sales_Sharer_Role__c = role;                        
                        
        			if(bizUnitSetting != null && !String.isBlank(bizUnitSetting.Business_Segment__c)) {
            			leadObj.Business_Segment__c = bizUnitSetting.Business_Segment__c;
            			leadObj.UTM_Term_Most_Recent__c = bizUnitSetting.Business_Segment__c;
        			}else{
            			leadObj.Business_Segment__c = 'MS';
            			leadObj.UTM_Term_Most_Recent__c = 'MS';            
        			} 
                        
                    if(matchingExistingLead.Status == 'Nurture') {
                        	leadObj.ILS_Assignment_Needed__c = TRUE;
                        	leadObj.Lead_Assigned__c = FALSE;
                    }
       }
       return;
        }else{
             leadObj.Camp__c = campName;
             leadObj.Internal_Lead_Share__c  = true;
             leadObj.DHR_Sales_Rep_Name__c = UserInfo.getName();
         	 leadObj.DHR_Sales_Rep_Email__c  = UserInfo.getUserEmail();
   	         leadObj.DHR_Sales_Share_Date__c = system.now(); 
   		     leadObj.Marketing_Channel_Most_Recent__c = 'Partners';
   		     leadObj.UTM_Campaign_Most_Recent__c = 'Generic';
   		     leadObj.UTM_Content_Most_Recent__c = 'Internal Lead Share';
   		     leadObj.UTM_Medium_Most_Recent__c = 'Partners';                
   		     leadObj.UTM_Source_Most_Recent__c = 'SFDC';            
			 leadObj.DHR_Sales_Sharer_Role__c = role;   
            
        if(bizUnitSetting != null && !String.isBlank(bizUnitSetting.Business_Segment__c)) {
            leadObj.Business_Segment__c = bizUnitSetting.Business_Segment__c;
            leadObj.UTM_Term_Most_Recent__c = bizUnitSetting.Business_Segment__c;
        }else{
            leadObj.Business_Segment__c = 'MS';
            leadObj.UTM_Term_Most_Recent__c = 'MS';            
        }            
        }

        String leadRecordTypeName = ContactToLeadRecordTypeMapping.get(u.License_Region__c);
        if(leadRecordTypeName != null) {
            Schema.RecordTypeInfo rtInfo = Lead.SObjectType.getDescribe().getRecordTypeInfosByName().get(leadRecordTypeName);
            if(rtInfo != null) leadObj.RecordTypeId = rtInfo.getRecordTypeId();
        }

        leadObj.Lead_Type__c = 'Existing Contact';
        leadObj.Company = c.Account.Name;
        leadObj.FirstName = c.FirstName;
        leadObj.Camp__c = campName;
        leadObj.LastName = c.LastName;        
        leadObj.Email = c.Email;
        leadObj.Street = c.MailingStreet;
        leadObj.PostalCode = c.MailingPostalCode;
        leadObj.City = c.MailingCity;
        leadObj.Country = c.MailingCountry;
        leadObj.State = c.MailingState;
        leadObj.Phone = c.Phone;
        leadObj.MobilePhone = c.MobilePhone;
        leadObj.Internal_Lead_Share__c = TRUE;
        leadObj.Status = 'Open';
        leadObj.Marketing_Lead_Score__c = 'A1';
        leadObj.Lead_Assignment_Date__c = Date.today();
        leadObj.Market_Segment__c = c.Market_Segment_St__c;
        leadObj.Title = c.Title;
        leadObj.Department__c = c.Department;
        leadObj.Job_Function__c = c.Job_Functions_07__c;
        leadObj.DHR_Sales_Rep_Name__c = UserInfo.getName();
        leadObj.DHR_Sales_Rep_Email__c  = UserInfo.getUserEmail(); 
        leadObj.DHR_Sales_Share_Date__c = system.now();    
        leadObj.Influence__c = c.Influence__c;
        leadObj.Product_Interest_Software__c = c.Product_Interest_Software__c;
        leadObj.Product_Interest_Systems__c = c.Product_Interest_Systems__c;
        leadObj.Product_Ownership_Software__c = c.Product_Ownership_Software__c;
        leadObj.Product_Ownership_Systems__c = c.Product_Ownership_Systems__c;
        leadObj.Marketing_Channel_Most_Recent__c = 'Partners';
        leadObj.UTM_Campaign_Most_Recent__c = 'Generic';
        leadObj.UTM_Content_Most_Recent__c = 'Internal Lead Share';
        leadObj.UTM_Medium_Most_Recent__c = 'Partners';                
        leadObj.UTM_Source_Most_Recent__c = 'SFDC';
        leadObj.ILS_Assignment_Needed__c = TRUE;                
		leadObj.DHR_Sales_Sharer_Role__c = role;   
                
        if(bizUnitSetting != null && !String.isBlank(bizUnitSetting.Intent_Field__c)) {
            leadObj.put(bizUnitSetting.Intent_Field__c, 'A - Hot (0-6 months purchase)');
        }else{
            leadObj.Intent_To_Purchase__c = 'A - Hot (0-6 months purchase)';
        }
        
        if(bizUnitSetting != null && !String.isBlank(bizUnitSetting.Business_Segment__c)) {
            leadObj.Business_Segment__c = bizUnitSetting.Business_Segment__c;
            leadObj.UTM_Term_Most_Recent__c = bizUnitSetting.Business_Segment__c;
        }else{
            leadObj.Business_Segment__c = 'MS';
            leadObj.UTM_Term_Most_Recent__c = 'MS';            
        }
    }

    public List<SelectOption> businessUnitOptions {
        get {
            List<SelectOption> opts = new List<SelectOption>();
            List<BusinessUnitWrapper> bizUnitList = new List<BusinessUnitWrapper>();
            for(CreateInternalLeadBusinessUnit__c bizUnit : CreateInternalLeadBusinessUnit__c.getAll().values())
            {
                bizUnitList.add(new BusinessUnitWrapper(bizUnit.Name));
            }
            bizUnitList.sort();

            for(BusinessUnitWrapper bizUnit : bizUnitList)
            {
                opts.add(new SelectOption(bizUnit.businessUnit, bizUnit.businessUnit));
            }
            return opts;
        }
    }

	public List<SelectOption> getroleOptions() {
        List<SelectOption> roleOptions = new List<SelectOption>();
        //roleOptions.add(new SelectOption('','-- SELECT ONE --'));
        roleOptions.add(new SelectOption('Sales','Sales'));
        roleOptions.add(new SelectOption('Service','Service'));
        roleOptions.add(new SelectOption('Applications Support','Applications Support'));
        roleOptions.add(new SelectOption('Other','Other'));

        return roleOptions;
    }
    
    class BusinessUnitWrapper implements Comparable {
        public String businessUnit {get;set;}

        public BusinessUnitWrapper(String businessUnit) {
            this.businessUnit = businessUnit;    
        }

        //implements sorting
        public Integer compareTo(Object that)
        {
            if(that == null) return 1;
            if(this.businessUnit == 'Other') return 1;
            BusinessUnitWrapper thatOLW = (BusinessUnitWrapper)that;
            if(thatOLW.businessUnit == 'Other') return -1;
            return this.businessUnit.compareTo(thatOLW.businessUnit);
        }
    }
    
    private Case findCase(Id caseId) 
    {
        return [select Id, CaseNumber, Contact.Email, Contact.Name, Contact.FirstName, Contact.LastName, Contact.Account.Name, Contact.RecordType.Name,
        Contact.MailingStreet, Contact.MailingPostalCode, Contact.MailingCity, Contact.MailingCountry, Contact.MailingState,
        Contact.Phone, Contact.MobilePhone, Contact.Market_Segment_St__c, Contact.Title, Contact.Department, Contact.Job_Functions_07__c,
        Contact.Influence__c, Contact.Product_Interest_Software__c, Contact.Product_Interest_Systems__c, Contact.Product_Ownership_Systems__c,
        Contact.Product_Ownership_Software__c, Contact.Marketing_Campaign_Most_Recent__c, Contact.Business_Segment_Most_Recent__c 
        from Case where Id = :caseId];    
    }
    
    private Contact findContact(Id contactId) 
    {
        return [select Id, Email, Name, FirstName, LastName, Account.Name, RecordType.Name,
        MailingStreet, MailingPostalCode, MailingCity, MailingCountry, MailingState,
        Phone, MobilePhone, Market_Segment_St__c, Title, Department, Job_Functions_07__c,
        Influence__c, Product_Interest_Software__c, Product_Interest_Systems__c, Product_Ownership_Systems__c,
        Product_Ownership_Software__c, Marketing_Campaign_Most_Recent__c, Contact.Business_Segment_Most_Recent__c 
        from Contact where Id = :contactId];    
    }

    public Boolean isCase
    {
        get {
            return String.valueOf(sobjectId).startsWith(Case.SObjectType.getDescribe().getKeyPrefix());
        }
    }

    public Boolean isContact
    {
        get {
            return String.valueOf(sobjectId).startsWith(Contact.SObjectType.getDescribe().getKeyPrefix());
        }
    }
    
    public String sectionHeaderTitle {
        get {
            return isContact ? theContact.Name : theCase.CaseNumber;
        }
    }

    public String sectionHeaderSubtitle {
        get {
            return SECTION_HEADER_SUBTITLE;
        }
    }

    public String pageBlockTitle {
        get {
            String val = isContact ? 'Contact' : 'Case';
            String s = String.format(PAGE_BLOCK_TITLE, new String[]{ val });
            return s;
        }
    }

    public String businessUnitLabel {
        get {
            return BUSINESS_UNIT_LABEL;
        }
    }

    public String commentsLabel {
        get {
            return COMMENTS_LABEL;
        }
    }

    public String roleLabel {
        get {
            return ROLE_LABEL;
        }
    }    
    
    public String buttonLabel {
        get {
            return BUTTON_LABEL;
        }
    }

}