/**
 ** @author Grahan Foster
 ** @created Oct 14/2015
 **
 ** Publisher Action for closing a case
 ** 
**/

public with sharing class CloseCaseExtension {
	private final Case caseRec;
    public CaseComment comment {get; set;}
    public Case sendSurvey {get; set;}
    
    public CloseCaseExtension(ApexPages.StandardController controller) {
		caseRec = (Case)controller.getRecord();
        sendSurvey = [SELECT QTRICS_Send_Survey__c, IsClosed, Master_Control_ID__c, 
                      Sales_Order_Number__c, Training_Part_Number__c, Complaint__c, 
					  Resolution_Code__c, Training_Issue__c
                      FROM Case WHERE Id=:caseRec.Id LIMIT 1];
        comment = new CaseComment();
        comment.parentid = caseRec.id;
    }
	
    public PageReference closeCase(){
        
        String validation = formValidation();
        if(validation != ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,validation));
        } else {
            performTheCaseUpdate();
        }    
        return null;
    }
    
    public void performTheCaseUpdate(){
        //insert the case comment
		if(!String.isEmpty(comment.commentbody))
		{
			comment.IsPublished = false;
			insert comment;
		}
        
        Case caseClose = new Case(Id = caseRec.id);
        //set up the fields for the closure according to what is on the form
        caseClose.Status = 'Closed';
        caseClose.QTRICS_Send_Survey__c = sendSurvey.QTRICS_Send_Survey__c;
        caseClose.Master_Control_ID__c = sendSurvey.Master_Control_ID__c;
        caseClose.Sales_Order_Number__c = sendSurvey.Sales_Order_Number__c;
        caseClose.Training_Part_Number__c = sendSurvey.Training_Part_Number__c;
        caseClose.Complaint__c = sendSurvey.Complaint__c;
		caseClose.Resolution_Code__c = sendSurvey.Resolution_Code__c;
		caseClose.Training_Issue__c = sendSurvey.Training_Issue__c;
        //close the case
        update caseClose;
        sendSurvey = [SELECT QTRICS_Send_Survey__c, IsClosed, Master_Control_ID__c, 
                      Sales_Order_Number__c, Training_Part_Number__c, Complaint__c, 
					  Resolution_Code__c, Training_Issue__c
                      FROM Case WHERE Id=:caseRec.Id LIMIT 1];
        comment = new CaseComment();
        comment.parentid = caseRec.id;
    }
    
    public String formValidation(){
        if(sendSurvey.IsClosed == true){
            return 'You cannot update a closed case';
        } else {
                if(sendSurvey.Resolution_Code__c == 'NOT SET'){
            		return 'You must set the Resolution Code';
                } else {
                    if(sendSurvey.Resolution_Code__c != 'General inquiry' && 
                       sendSurvey.Training_Issue__c == 'NOT SET'){
            			return 'Declare if this case was related to a training issue';
                	}
                }
            }
        return '';
    }

	public List<SelectOption> getResCodes()
	{
		List<SelectOption> options = new List<SelectOption>();
        
		Schema.DescribeFieldResult fieldResult =
		Case.Resolution_Code__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('NOT SET', '--None--'));
		for( Schema.PicklistEntry f : ple)
		{
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
	}
    
    public List<SelectOption> getTrainingIssue()
    {
        List<SelectOption> options = new List<SelectOption>();
        
		Schema.DescribeFieldResult fieldResult =
		Case.Training_Issue__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('NOT SET', '--None--'));
		for( Schema.PicklistEntry f : ple)
		{
			options.add(new SelectOption(f.getLabel(), f.getValue()));
		}       
		return options;
        
    }
}