// ===========================================================================
// Object: AbsorbSyncEnrollmentsQueueable
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Synch Absorb LMS data to SFDC
// ===========================================================================
// Changes: 2016-03-03 Reid Beckett
//           Class created
// ===========================================================================
global class AbsorbSyncEnrollmentsQueueable implements Database.AllowsCallouts, Database.Batchable<sObject>, Schedulable
{
    global static final Integer ENROLLMENTS_COURSE_BATCH_SIZE = Test.isRunningTest() ? 5 : 1;
    private List<LMS_Course__c> lmsCourses;
    private Map<String, Contact> sfContactsMap;
    private Date modifiedSinceDate {get;set;}
    private Boolean forScheduledJob {get;set;}

    global AbsorbSyncEnrollmentsQueueable()
    {
    }

    global AbsorbSyncEnrollmentsQueueable(Date modifiedSinceDate)
    {
        this.modifiedSinceDate = modifiedSinceDate;
    }

    global AbsorbSyncEnrollmentsQueueable(Boolean forScheduledJob)
    {
        this.forScheduledJob = forScheduledJob;
        if(forScheduledJob != null && forScheduledJob) this.modifiedSinceDate = Date.today();
    }

    //Batchable interface implementation
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        AbsorbLogger.startMethod('AbsorbSyncEnrollmentsQueueable.start');
        new AbsorbSyncManager().syncCourses();
        return Database.getQueryLocator([select Id, Name, LMS_Course_ID__c from LMS_Course__c]);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        Map<String, Contact> sfContactsMap = new Map<String,Contact>();
        for(Contact c : [select Id, LMS_User_ID__c from Contact where LMS_User_ID__c != null])
        {
            sfContactsMap.put(c.LMS_User_ID__c, c);
        }

        Map<String, LMS_Session__c> sessionsMap = new Map<String, LMS_Session__c>();
        Map<String, List<LMS_Schedule__c>> schedulesMap = new Map<String, List<LMS_Schedule__c>>();
        Map<String,LMS_Course_Enrollment__c> allEnrollments = new Map<String,LMS_Course_Enrollment__c>();
        Map<String, String> enrollmentSessionMap = new Map<String, String>();

        for(LMS_Course__c lmsCourse : (List<LMS_Course__c>)scope)
        {
            Map<String,LMS_Course_Enrollment__c> enrollments = syncCourseEnrollments(lmsCourse, sfContactsMap);
            allEnrollments.putAll(enrollments);

            AbsorbModel.Session[] sessions = AbsorbAPIClient.getInstance().getSessions(lmsCourse.LMS_Course_ID__c);
            for(AbsorbModel.Session s : sessions) {
                String description = s.Description == null ? '' : s.Description;
                String sessionName = s.Name;
                if(sessionName != null && sessionName.length() > LMS_Session__c.fields.Name.getDescribe().getLength()) 
                    sessionName = sessionName.substring(0,LMS_Session__c.fields.Name.getDescribe().getLength());
                sessionsMap.put(s.Id, new LMS_Session__c(LMS_Course__c = lmsCourse.Id, Name = sessionName, LMS_Session_ID__c = s.Id, 
                    Enrollment_Start_Date__c = s.EnrollmentStartDateDt, Enrollment_End_Date__c = s.EnrollmentEndDateDt,
                    Description__c = description
                    ));

                if(s.SessionScheduleIds != null)
                {
                    for(String scheduleId : s.SessionScheduleIds)
                    {
                        AbsorbModel.SessionSchedule sessionSched = AbsorbAPIClient.getInstance().getSessionSchedule(scheduleId);
                        if(sessionSched != null)
                        {
                            if(schedulesMap.containsKey(s.Id)) {
                                schedulesMap.get(s.Id).add(createLMSSchedule(sessionSched));
                            }else{
                                schedulesMap.put(s.Id, new LMS_Schedule__c[]{ createLMSSchedule(sessionSched) });
                            }
                        }
                    }
                }

                //get the enrollments for the session
                AbsorbModel.CourseEnrollment[] sessionEnrollments = AbsorbAPIClient.getInstance().getCourseEnrollmentsByCourseAndSession(lmsCourse.LMS_Course_ID__c, s.Id);
                for(AbsorbModel.CourseEnrollment sessionEnrollment : sessionEnrollments)
                {
                    if(!String.isBlank(sessionEnrollment.CourseEnrollmentId)) enrollmentSessionMap.put(sessionEnrollment.CourseEnrollmentId, s.Id);
                }
            }
        }

        upsert sessionsMap.values() LMS_Session__c.fields.LMS_Session_ID__c;
        List<LMS_Schedule__c> scheduleUpserts = new List<LMS_Schedule__c>();

        for(String sessionId : schedulesMap.keySet())
        {
            for(LMS_Schedule__c lmsSchedule : schedulesMap.get(sessionId))
            {
                lmsSchedule.LMS_Session__c = sessionsMap.get(sessionId).Id;                
                scheduleUpserts.add(lmsSchedule);
            }
        }
        upsert scheduleUpserts LMS_Schedule__c.LMS_Schedule_ID__c;

        Map<String,LMS_Course_Enrollment__c> unmodifiedEnrollmentsToLink = new Map<String,LMS_Course_Enrollment__c>();
        for(String enrollmentId : enrollmentSessionMap.keySet())
        {
            if(sessionsMap.containsKey(enrollmentSessionMap.get(enrollmentId)))
            {
                if(allEnrollments.containsKey(enrollmentId)) {
                    allEnrollments.get(enrollmentId).LMS_Session__c = sessionsMap.get(enrollmentSessionMap.get(enrollmentId)).Id;
                } else {
                    unmodifiedEnrollmentsToLink.put(enrollmentId, new LMS_Course_Enrollment__c(
                        LMS_Enrollment_ID__c = enrollmentId, 
                        LMS_Session__c = sessionsMap.get(enrollmentSessionMap.get(enrollmentId)).Id
                    ));
                }
            }
        }
        upsert allEnrollments.values() LMS_Course_Enrollment__c.LMS_Enrollment_ID__c;
        if(!unmodifiedEnrollmentsToLink.isEmpty()) {
            Map<String, LMS_Course_Enrollment__c> enrollmentsByLMSID = new Map<String, LMS_Course_Enrollment__c>();
            for(LMS_Course_Enrollment__c enr : [select Id, LMS_Enrollment_ID__c, LMS_Session__c from LMS_Course_Enrollment__c where LMS_Enrollment_ID__c in :unmodifiedEnrollmentsToLink.keySet()]) {
                enr.LMS_Session__c = unmodifiedEnrollmentsToLink.get(enr.LMS_Enrollment_ID__c).LMS_Session__c;                
                enrollmentsByLMSID.put(enr.LMS_Enrollment_ID__c, enr);
            }
            //upsert unmodifiedEnrollmentsToLink.values() LMS_Course_Enrollment__c.LMS_Enrollment_ID__c;
            update enrollmentsByLMSID.values();
        }

        AbsorbAPIClient.getInstance().close();
    }

    private static LMS_Schedule__c createLMSSchedule(AbsorbModel.SessionSchedule sessionSched)
    {
        return new LMS_Schedule__c(LMS_Schedule_ID__c=sessionSched.Id, Venue_Name__c = sessionSched.VenueName,
                                    Start_Date__c = sessionSched.DateStartDt, End_Date__c = sessionSched.DateEndDt, Time_Zone_ID__c = sessionSched.TimeZoneId);
    }

    global void finish(Database.BatchableContext BC) {}

    //schedulable interface implementation
    global void execute(SchedulableContext sc) 
    {
        Database.executeBatch(new AbsorbSyncEnrollmentsQueueable(forScheduledJob), ENROLLMENTS_COURSE_BATCH_SIZE);
    }

    //business logic method to sync course enrollment records
    private Map<String,LMS_Course_Enrollment__c> syncCourseEnrollments(LMS_Course__c lmsCourse, Map<String, Contact> sfContactsMap)
    {
        AbsorbAPIClient.IAbsorbAPIClient absorbClient = AbsorbAPIClient.getInstance();
        Map<String,LMS_Course_Enrollment__c> enrollmentsMap = new Map<String,LMS_Course_Enrollment__c>();
        List<AbsorbModel.CourseEnrollment> enrollments = absorbClient.getCourseEnrollmentsByCourse(lmsCourse.LMS_Course_ID__c, modifiedSinceDate);
        AbsorbLogger.info(enrollments.size() + ' enrollments for ' + lmsCourse.LMS_Course_ID__c);
        for(AbsorbModel.CourseEnrollment enrollment : enrollments){
            if(sfContactsMap.containsKey(enrollment.UserId)) {
                Contact c = sfContactsMap.get(enrollment.UserId);
                if(enrollmentsMap.containsKey(enrollment.Id)) {
                    LMS_Course_Enrollment__c sfEnrollment = enrollmentsMap.get(enrollment.Id);
                    sfEnrollment.LMS_Progress__c = enrollment.Progress;
                    sfEnrollment.LMS_Status__c = enrollment.enrollmentStatus;
                }else{
                    LMS_Course_Enrollment__c sfEnrollment = new LMS_Course_Enrollment__c(LMS_Enrollment_ID__c =enrollment.Id, LMS_User__c = c.Id, LMS_Course__c = lmsCourse.Id);
                    sfEnrollment.Name = lmsCourse.Name;
                    sfEnrollment.LMS_Progress__c = enrollment.Progress;
                    sfEnrollment.LMS_Status__c = enrollment.enrollmentStatus;
                    if(sfEnrollment.LMS_User__c !=null && sfEnrollment.LMS_Course__c != null) enrollmentsMap.put(sfEnrollment.LMS_Enrollment_ID__c, sfEnrollment);
                }
            }
        }
        return enrollmentsMap;
    }
}