public class testUpdateQuoteReportData {
	
	public static testMethod void testTrigger(){
		QuoteReportData__c d2 = new QuoteReportData__c(
			//Template_Type__c 	=	d1.Template_Type__c,
			//Template_Number__c	=	d1.Template_Number__c, 
			Status__c			=	'Released By Workflow',
			Sold_To__c			=	'8002569',
			Sold_To_Name__c		=	'Thomas D Nguyen',
			Ship_To__c			=	'100014557',
			Ship_To_State__c	=	'CA',
			Ship_To_Phone__c	=	'(650)555-1212',
			Ship_To_LastName__c	=	'Biosystems',
			Ship_To_FirstName__c=	'Applied',
			Ship_To_City__c		=	'Foster City',
			//Sales_Rep__c		=	'Bret Wadleigh',
			Sales_Office__c		=	'Foster City',
			//SAP_Quote_Num__c	=	'2078401',
			Report_Status__c	=	'COMPLETE',
			Quote_Line_Item_Number__c =	'1',
			Quantity__c			=	1,
			Product_Number__c	=	'400912',
			Product_Line__c		=	'MCD',
			Product_Desc__c		=	'Sample Preperation',
			IGOR_Class_Desc__c 	=	'CONSUMABLE',
			//Opportunity__c		=	'',
			//OpportunityID__c	=	'',
			Net_Value__c		=	160.89
			//Name				=	'a0K40000000F0Ag',
			//CDI_Quote_Num__c	=	'2008101836',
			//Account__c			=	'0014000000IZri7AAD'
			);
			
		insert d2;
	}
}