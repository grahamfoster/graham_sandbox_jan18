/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 10/2016
**
** Test coverage for LMSCourseEnrollment trigger logic
** Trigger also covered in AbsorbTests and CaseCloneTests
**/
@isTest
public class LMSCourseEnrollmentTrigger_Test {
	@testSetup
	static void setUp() {
		AbsorbLMSSettings__c settings = AbsorbLMSSettings__c.getOrgDefaults();
		settings.Endpoint_URL__c = 'http://example.com';
		settings.API_Username__c = 'user';
		settings.API_Password__c = 'pass';
		settings.API_Token_Timeout__c = 240;
		upsert settings;
	}

	@isTest
	public static void test() 
	{
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Case trainingCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), Contactid = c.Id);
		insert trainingCase;

		Case installationCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(), Contactid = c.Id);
		insert installationCase;

		Customer_Onboard__c cobo = new Customer_Onboard__c(InstallationCase__c=installationCase.Id, Training_case__c = trainingCase.Id);
		insert cobo;

		LMS_Course__c lmsCourse = new LMS_Course__c(LMS_Course_ID__c = AbsorbUtil.generateGUID(), Name = 'Test Course', LMS_Course_Type__c = '1');
		insert lmsCourse;

		LMS_Course_Enrollment__c lmsCourseEnrollment = new LMS_Course_Enrollment__c(
		            LMS_Course__c = lmsCourse.Id, LMS_User__c = c.Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
		            Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0
		);

		Test.startTest();
		insert lmsCourseEnrollment;
		lmsCourseEnrollment.LMS_Status__c = 'Complete';
		update lmsCourseEnrollment;

		trainingCase = [select Id, Pre_elearning_for_Field_Service_Engineer__c, Pre_Install_Course_Complete__c from Case where Id =:trainingCase.Id];
		system.assertEquals(lmsCourseEnrollment.Id, trainingCase.Pre_elearning_for_Field_Service_Engineer__c);
		system.assertEquals(true, trainingCase.Pre_Install_Course_Complete__c);
		installationCase = [select Id, Pre_elearning_for_Field_Service_Engineer__c, Pre_Install_Course_Complete__c from Case where Id =:installationCase.Id];
		system.assertEquals(true, installationCase.Pre_Install_Course_Complete__c);
		delete lmsCourseEnrollment;
		undelete lmsCourseEnrollment;

		Test.stopTest();
		
	}
}