/*
 *	CaseContactTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Graham Foster on 2016-11-09
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */

public class CaseContactTriggerHandler extends TriggerHandler 
{
    public CaseContactTriggerHandler() {
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void afterInsert() {
		createOnboardingContact();
	}
	override protected void afterUpdate() {

	}


	// **************************************************************************
	// 		private methods
	// **************************************************************************
    
    /*
     * Creates a customer onboarding contact if a new case contact
     * is added to a case with a cobo connected to it
     * 
     * Graham Foster Nov 2016
     * 
     */ 
    private void createOnboardingContact()
    {
        //get a set of case Id's so that we can ensure that
        //these cases have a customer onboard record
        Set<Id> caseId = new Set<Id>();
        List<Case_Contact__c> lstCaseContacts = new List<Case_Contact__c>();
        for (Case_Contact__c cc : (List<Case_Contact__c>)trigger.new){
            caseId.add(cc.Case__c);
        }
        //only querying the cases that have a cobo
        List<Case> cases = [SELECT Id, Customer_Onboard__c FROM Case 
                            WHERE Customer_Onboard__c != NULL AND Id IN :caseId];
        
        //get a list of the existing cobo contacts so that we
        //can compare and ensure we are not duplicating contacts
        Set<Id> coboId = new Set<Id>();
        for (Case c : cases){
            coboId.add(c.Customer_Onboard__c);
        }
        List<OnBoarding_Contacts__c> obc = [SELECT OnBoard_Contact__c, Customer_Onboard__c, Role__c 
                                            FROM OnBoarding_Contacts__c WHERE Customer_Onboard__c IN :coboId];
        //new list to hold the values to insert
        List<OnBoarding_Contacts__c> newObc = new List<OnBoarding_Contacts__c>();
        
        //loop through the trigger again and check the case has a cobo and
        //that the onboarding contact does not exist.  If these are met
        //create a new onboarding contact and add it to the list to be inserted
        for (Case_Contact__c cc : (List<Case_Contact__c>)trigger.new){
            if(HasCustomerOnboard(cases, cc.Case__c) && 
               !OnBoardContactExists(cases, obc, cc.Case__c, cc.Contact__c)){
                   for(Case c : cases){
                       if(c.Id == cc.Case__c){
                           newObc.add(new OnBoarding_Contacts__c(OnBoard_Contact__c = cc.Contact__c,
                                                       Customer_Onboard__c = c.Customer_Onboard__c,
                                                       Role__c = 'Additional Learner'));
                       }
                   }     
               }
        }
        if(newObc.size() > 0){
            insert newObc;
            //need to sign each of them up for the LMS
        	List<Contact> updateContacts = new List<Contact>();
        	for(OnBoarding_Contacts__c lObc : newObc ){
            	updateContacts.add(new Contact(Id = lObc.OnBoard_Contact__c,
                                          LMS_Learner_Level__c = '',
                							LMS_Primary_Workflow__c = '',
                							Portal_Status__c = 'Requested'));
        	}
            update updateContacts;
        }

    }
    
    private Boolean HasCustomerOnboard(List<Case> lCases, Id CaseContactCaseId){
        for(Case c : lCases){
            if(c.Id == CaseContactCaseId && !String.isEmpty(c.Customer_Onboard__c)){
                return true;
            }
        }
        return false;
    }
    
    private Boolean OnBoardContactExists(List<Case> lCases, List<OnBoarding_Contacts__c> lObc, Id CaseId, Id ContactId)
    {
        //find the correct case
        Case fCase = null;
        for(Case c : lCases){
            if(c.Id == CaseId){
                fCase = c;
                Break;
            }
        }
        //check that the Customer Onboard record matches the case
        //customer onboard reference and that the Contact and
        //role matches additional learner
        for (Onboarding_Contacts__c vObc : lObc){
            if(vObc.Customer_Onboard__c == fCase.Customer_Onboard__c && 
              vObc.OnBoard_Contact__c == ContactId &&
               vObc.Role__c.contains('Learner')){
                   return true;
               }
        }
        return false;
    }

}