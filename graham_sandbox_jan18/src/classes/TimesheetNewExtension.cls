/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Nov 12/2013
 **
 ** Timesheet extension for new record
**/
public with sharing class TimesheetNewExtension {
	public Timesheet__c timesheet {get;set;}
	public String retURL {get;set;}

	public TimesheetNewExtension(ApexPages.StandardController stdController){
		this.timesheet = (Timesheet__c)stdController.getRecord();
		retURL = ApexPages.currentPage().getParameters().containsKey('retURL') ? EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('retURL'), 'UTF-8') : null;
	}

	public PageReference newTimesheet(){
		this.timesheet.Name = UserInfo.getName();
		DateTime dtt = DateTime.now();
		//set to the past Monday
		integer safeCounter = 0; //not sure if we always can count on "mon"
		while(dtt.format('E').toLowerCase() != 'mon' && safeCounter < 7) {
			dtt = dtt.addDays(-1);
			safeCounter++;
		}
		this.timesheet.Week_Start__c = dtt.date();
		this.timesheet.OwnerId = UserInfo.getUserId();
		return null;
	}
	
	public PageReference save(){
		try {
			insert this.timesheet;
			return new PageReference('/' + this.timesheet.Id);
			//return new PageReference(retURL);
		}catch(DmlException e){
			SfdcUtil.addDMLExceptionMessagesToPage(e);
			return null;
		}
	}

}