/**
** @author Reid Beckett, Cloudware Connections
** @created Dec 2/2014
**
** Utilities to work with the Countries and Regions, using the Country__c custom settings
** 
**/
public class RegionUtil {

	public static List<String> getAllRegions() {
		List<String> regions = new List<String>();
     		for(Schema.PicklistEntry ple : User.License_Region__c.getDescribe().getPicklistValues()){
     			regions.add(ple.getValue());
     		}
		regions.sort();
		return regions;
	}

	public static List<String> getCountryAliases(String country) {
		if(String.isBlank(country)) return new List<String>();
		if(!Country__c.getAll().containsKey(country)) return new List<String>{country};
		List<String> countries = new List<String>();
		Country__c c = Country__c.getAll().get(country);
		countries.add(c.Name);
		if(!String.isBlank(c.Aliases__c)) {
			for(String alias : c.Aliases__c.split(';')) {
				if(!String.isBlank(alias.trim())) countries.add(alias.trim());
			}
		}
		countries.sort();
		return countries;
	}

	public static List<String> getAllCountriesWithAliases(String region) {
		List<String> countries = new List<String>();
		for(Country__c c : Country__c.getAll().values()) {
			if(c.Region__c == region) {
				countries.add(c.Name);
				if(!String.isBlank(c.Aliases__c)) {
					for(String alias : c.Aliases__c.split(';')) {
						if(!String.isBlank(alias.trim())) countries.add(alias.trim());
					}
				}
			}
		}
		countries.sort();
		return countries;
	}

	public static List<String> getAllCountries(String region) {
		List<String> countries = new List<String>();
		for(Country__c c : Country__c.getAll().values()) {
			if(c.Region__c == region) countries.add(c.Name);
		}
		countries.sort();
		return countries;
	}
}