public class runCoboUpdateAsync {

	@Future
	public static void futureUpdateCobo()
	{
		 //get a list of current onboards
        List<Customer_Onboard__c> onboardsToUpdate = [SELECT Id, Update_Record__c 
                                                      FROM Customer_Onboard__c 
                                                      WHERE (Status__c != 'Closed' OR Status__c != 'Success Confirmation') 
													  AND Pre_Site_Completion_Date__c != null];
        if (onboardsToUpdate.size() > 0){
            for(Customer_Onboard__c cu : onboardsToUpdate){
            //We use the Update_Record__c field to cause a Record Update on
            //the COBO record - this will then fire the Process Builder flow
            //which updates all of the status's on the COBO record
            cu.Update_Record__c = !cu.Update_Record__c;
        	}
        	//update the records
        	update onboardsToUpdate; 
        }
        
        //get a list of Cases which need to be updated
        List<Case> casesToUpdate = [SELECT Id, Update_Record__c
                                   FROM Case WHERE RecordType.DeveloperName = 'Training' AND IsClosed = false 
								   AND Training_Part__c != null];
        if(casesToUpdate.size() > 0){
            for(Case c : casesToUpdate){
            //We use the Update_Record__c field to cause a Record Update on
            //the Case record - this will then fire the Process Builder flow
            //which updates all of the status's on the Case record
            c.Update_Record__c = !c.Update_Record__c;
        	}
        	update casesToUpdate;
        }  
	}

}