public with sharing class Additional_Resource_Ctrl {
    Public Additional_Resource__c addResourceRecord{get;set;} 
    public String pageDirection{get;set;}   
    public String ResourceCategoryId{get;set;}
    public Additional_Resource_Ctrl(ApexPages.StandardController controller)
    {   
        ResourceCategoryId = ApexPages.currentPage().getParameters().get('catid');     
        addResourceRecord = (Additional_Resource__c)Controller.getRecord(); 
        pageDirection = 'false';            
    }
    public PageReference doSaveAndNew(){        
        try{
            insert addResourceRecord;
            return new pagereference('/apex/addResourceRecord');
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }   
        return null;
    }
    public PageReference doSave(){
        try{
           if(addResourceRecord != null){
           
               addResourceRecord.Resource_Category__c=ResourceCategoryId;
               insert addResourceRecord;
            }
            pageDirection = 'true';
            /* if(addResourceRecord.Id != null || (addResourceRecord.Id) != '')
            return new PageReference('/'+addResourceRecord.Id);     */       
        }
        catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));            
        }
        return null;
    }
    public PageReference doCancel(){
        String cancelURL =  ApexPages.currentPage().getParameters().get('retURL');
        if(cancelURL != null && cancelURL.trim() != ''){
            return new PageReference (cancelURL);
        }
        return null;    
    }
}