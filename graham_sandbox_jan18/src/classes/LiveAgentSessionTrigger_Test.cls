@isTest 
private class LiveAgentSessionTrigger_Test {

	@isTest
	private static void test_updateBusinessHoursCoverage() {

	
		List<BusinessHours> bh = [SELECT TimeZoneSidKey FROM BusinessHours WHERE Name LIKE 'SCIEXNow Test_%' AND IsActive = true];
		
		List<LiveAgentSession> las = new List<LiveAgentSession>();
		for(BusinessHours b : bh)
		{
			Date coverageDate = System.today();
			Timezone tz = TimeZone.getTimeZone(b.TimeZoneSidKey);
			Integer timeoffset = tz.getOffset(coverageDate);
			Time sTime = Time.newInstance(9,0,0,0);
			las.add(new LiveAgentSession(AgentId = UserInfo.getUserId(),LoginTime = Datetime.newInstance(System.today(),sTime.addMilliseconds(timeoffset)),
			LogoutTime = Datetime.newInstance(System.today(),sTime.addMilliseconds(timeoffset)).addHours(4)));
		}
		insert las;

		List<Live_Agent_Session_Coverage__c> coverage = [SELECT Percent_Coverage__c FROM Live_Agent_Session_Coverage__c WHERE Business_Hours__r.Name LIKE 'SCIEXNow Test_%'];
		System.assertEquals(bh.size(), coverage.size());
		for(Live_Agent_Session_Coverage__c lc : coverage)
		{
			System.assertEquals(lc.Percent_Coverage__c, 50);
		}

		// now add another session to each which overlaps
		List<LiveAgentSession> las2 = new List<LiveAgentSession>();
		for(BusinessHours b : bh)
		{
			Date coverageDate = System.today();
			Timezone tz = TimeZone.getTimeZone(b.TimeZoneSidKey);
			Integer timeoffset = tz.getOffset(coverageDate);
			Time sTime = Time.newInstance(12,0,0,0);
			las2.add(new LiveAgentSession(AgentId = UserInfo.getUserId(), LoginTime = Datetime.newInstance(System.today(),sTime.addMilliseconds(timeoffset)), 
			LogoutTime = Datetime.newInstance(System.today(),sTime.addMilliseconds(timeoffset)).addHours(3)));
		}
		insert las2;
		
		coverage = [SELECT Percent_Coverage__c FROM Live_Agent_Session_Coverage__c WHERE Business_Hours__r.Name LIKE 'SCIEXNow Test_%'];
		System.assertEquals(bh.size(), coverage.size());
		for(Live_Agent_Session_Coverage__c lc : coverage)
		{
			System.assertEquals(lc.Percent_Coverage__c, 75);
		}

	}
}