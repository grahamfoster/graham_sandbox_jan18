/*
Version        : 1.0
Company        : WebSolo Inc.
Date           : December, 2012
Description    : This class is designed to update Primary_Contact__c from the Opportunity's ContactRole
History        :

*/
public class OppPrimaryContactHelper 
{
    public Opportunity opp;
    public OppPrimaryContactHelper( ApexPages.StandardController stdController ) 
    {
        opp = ( Opportunity )stdController.getRecord();        
    }
    public void rollupOppContacts(){
        OppPrimaryContactHelper.rollupOppContactsFuture( opp.Id );    
    }
    //Change Primary Contact if changed OpportunityContactRole
    @future public static void rollupOppContactsFuture( Id oppId ) 
    {
        //list<Contact> contactList = new list<Contact>();
        //list<OpportunityContactRole> OpRol = new list<OpportunityContactRole>();
        //OpRol = [SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId = :oppId  AND IsPrimary = true];
        OpportunityContactRole[] OpRol = [SELECT ContactId, IsPrimary FROM OpportunityContactRole WHERE OpportunityId = :oppId];
        Contact[] contactUpdates = new Contact[]{};
        
        if(OpRol.size() > 0)
        {
	        Opportunity opp = [ SELECT Id, Primary_Contact__c FROM Opportunity WHERE Id = :oppId ];                   
        	for(OpportunityContactRole ocr : OpRol) {
        		if(ocr.IsPrimary) {
        			opp.Primary_Contact__c = ocr.ContactId;
        		}
        		contactUpdates.add(new Contact(Id = ocr.ContactId, Contact_Status__c = 'Active'));
        	}
        	try {
	        	update contactUpdates;
				OpportunityMarketVerticalTriggerHandler.PrimaryContactUpdate = true;
				update opp;
        	}catch(Exception e){
        	}
        	
        	/**
	        contactList = [SELECT Id FROM Contact WHERE Id=:OpRol[0].ContactId];                      
	        Opportunity opp = [ SELECT Id, Primary_Contact__c FROM Opportunity WHERE Id = :oppId ];                   
	        for( Contact contact : contactList ) 
	        {
	            opp.Primary_Contact__c = contact.id;
	            contact.Contact_Status__c = 'Active';
	        }
	        try     
	        {
	          if(contactList.size() > 0) update contactList;
	          OpportunityMarketVerticalTriggerHandler.PrimaryContactUpdate = true;
	          update opp;
	        }  
	        catch(Exception e)
	        {
	        	
	        }
			**/
        }
    }
}