/*********************************************************************
Name  : testAbQuotingUtils
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the testAbQuotingUtils

*********************************************************************/

public class testAbQuotingUtils{

public static testMethod void testloginToCDI(){
  try{
    AbQuotingUtils.loginToCDI('test@test.com','123');
  }
  catch(Exception e){  
  }
}

public static testMethod void testgetCurrentUserAuthString(){
 try{
  String str= AbQuotingUtils.getCurrentUserAuthString();
  str = AbQuotingUtils.retrieveNewQuoteNumFromCDI('test');
 } catch(Exception e) {
 }
}

public static testMethod void test1createAuthString(){
  String str= AbQuotingUtils.createAuthString('smolgom1','cmg123');
  str = AbQuotingUtils.convertDateToString(system.today());
}


public static testMethod void testgetUsersSalesOffice(){
  String str = AbQuotingUtils.getUsersSalesOffice();
  List<Account> lstAcc = [Select Id, Account_Sold_To__c From Account Limit 10];
  QuoteSearchServices.ArrayOfT_SOLDTO qss = AbQuotingUtils.createSoldToNumbers(lstAcc);
}

public static testMethod void testgetUsersSalesGroup(){
  String str = AbQuotingUtils.getUsersSalesGroup();
  str = AbSearchUtils.createContactName('test','test','test');
}

public static testMethod void testgetUsersSalesGroupRaw(){
 try{ 
  Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName');
  insert sfdcQuote;
  
  Quote_Line_Item__c itemLine = new Quote_Line_Item__c( Quote__c = sfdcQuote.Id );
  insert itemLine;  

  String[] str = AbQuotingUtils.getUsersSalesGroupRaw();
  
  string str1 = AbQuotingUtils.refreshSingleLineItemPricingInCDI(sfdcQuote.Id, itemLine, 'test');
  AbQuotingUtils.sortQuoteItems(sfdcQuote.Id);
 }
 catch(Exception e){
 }
}

public static testMethod void testgetUsersSixPlusTwo(){
  String str = AbQuotingUtils.getUsersSixPlusTwo();
  Account a = [Select Id From Account Limit 1];
  List<SelectOption> lst = AbQuotingUtils.getOpportunityList(a.Id);
  
}


public static testMethod void testconsolidateLineItemText(){
  List<QuoteServices.lineItemDetails> fullQuoteLineItems = new List<QuoteServices.lineItemDetails>();
  List<QuoteServices.lineItemDetails> qliTextItems = new  List<QuoteServices.lineItemDetails>() ;
  AbQuotingUtils.consolidateLineItemText(fullQuoteLineItems, qliTextItems);
}

/********************************************************************
public static testMethod void testgetUsersRegion(){
  try{
    Quoting_Auth__c qa = new Quoting_Auth__c(Auth_String__c = 'test', Type__c='test', User_Id__c = 'test');
    insert qa;
    String str= AbQuotingUtils.getUsersRegion();
    QuotingController c = new QuotingController();
    Quote__c q = new Quote__c();
    List<Quote__c> lst = AbQuotingUtils.retrieveQuoteSearchResultsFromSAP(c,'test',q);
  }
  catch(Exception e){
   System.assertEquals('Test', e.getMessage());
  }
}

public static testMethod void  testsendSubmitQuote(){
  try{
    Quoting_Auth__c qa = new Quoting_Auth__c(Auth_String__c = 'test', Type__c='test', User_Id__c = 'test');
    insert qa;
    QuoteServices.SalesQuote SQ  = new QuoteServices.SalesQuote(); 
    SQ.quoteHeader = new QuoteServices.quoteHeader();
    SQ.quoteHeader.submitDate = '2008-07-27';
    String str =  AbQuotingUtils.sendSubmitQuote(SQ);
  }
  catch(Exception e){
   System.assertEquals('Test', e.getMessage());
  }
}

public static testMethod void testretrieveQuoteDetailsFromCDI(){
  try{
    Quoting_Auth__c qa = new Quoting_Auth__c(Auth_String__c = 'test', Type__c='test', User_Id__c = 'test');
    insert qa;
    ID id =  AbQuotingUtils.retrieveQuoteDetailsFromCDI('4', '44', 'e');
  }
  catch(Exception e){
   System.assertEquals('Test', e.getMessage());
  }
}

public static testMethod void  testsearchCDIProducts(){
  try{
    Quoting_Auth__c qa = new Quoting_Auth__c(Auth_String__c = 'test', Type__c='test', User_Id__c = 'test');
    insert qa;
    String[] a = new String[1];
    List<Quote_Line_Item__c> listtest = AbQuotingUtils.searchCDIProducts('prodName', 'prodCode', 'prodCategory', a, 'salesOrg', 'region','76');
  }
  catch(Exception e){
   System.assertEquals('Test', e.getMessage());
  }
}

public static testMethod void  testsearchCDITemplates(){
  try{
    Quoting_Auth__c qa = new Quoting_Auth__c(Auth_String__c = 'test', Type__c='test', User_Id__c = 'test');
    insert qa;
    QuoteSearchServices.templateList tlist = AbQuotingUtils.searchCDITemplates('121','templateName','templateDescription','2132', Date.Today());
  }
  catch(Exception e){
   System.assertEquals('Test', e.getMessage());
  }
}

public static testMethod void  testsearchTemplateDetails(){
  try{
    Quoting_Auth__c qa = new Quoting_Auth__c(Auth_String__c = 'test', Type__c='test', User_Id__c = 'test');
    insert qa;
    QuoteSupplementalServices.templateDetails tDetails = AbQuotingUtils.searchTemplateDetails('111','templateModule', 'templateModule2');
  }
  catch(Exception e){
   System.assertEquals('Test', e.getMessage());
  }
}

******************************************************************/

public static testMethod void  testcreateDisplayValueForContacts(){
  String str = AbQuotingPartnerUtils.createDisplayValueForContacts('name1', 'name2', '000021');
}

public static testMethod void  testconcatDisplayFieldAndComma(){
  String str = AbSearchUtils.concatDisplayFieldAndComma( 'origText', 'valueToAdd');
}


public static testMethod void  testQuoting_SAP_Contact_Partners(){
  Quoting_SAP_Contact_Partners__c QSAPC =new Quoting_SAP_Contact_Partners__c(Contact_Number__c='121212-th',First_Name__c='firstName');
  SelectOption  SO = AbSearchUtils.createOption(QSAPC);
}

public static testMethod void  testcreateLongOption(){
  Quoting_SAP_Contact_Partners__c QSAPC = new Quoting_SAP_Contact_Partners__c(Contact_Number__c='121212-th',First_Name__c='firstName');
  SelectOption  SO = AbSearchUtils.createLongOption(QSAPC);
}

public static testMethod void  testgetPartnerCountryBasedOnSoldTo(){
  String str = AbQuotingPartnerUtils.getPartnerCountryBasedOnSoldTo('soldTo');
}

public static testMethod void  testgetSalesOrgBasedOnSoldTo(){
  Set<String> str = AbQuotingPartnerUtils.getSalesOrgBasedOnSoldTo(' soldTo');
}

public static testMethod void  testrefreshPricingInCDI(){
 try{
   Quote__c sfdcQuote = new Quote__c(Name = 'QuoteName');
   insert sfdcQuote; 
   String str  = AbQuotingUtils.refreshPricingInCDI( sfdcQuote , new List<Quote_Line_Item__c>(),'refreshApproval');
 }
 catch(Exception e){
 }
}

public static testMethod void  testprintSalesQuoteDebug(){
 QuoteServices.SalesQuote SQ  = new QuoteServices.SalesQuote(); 
 SQ.quoteHeader = new QuoteServices.quoteHeader();
 SQ.quoteHeader.submitDate = '2008-07-27';
 SQ.quoteHeader.cdiQuoteNumber ='12';
 SQ.quoteHeader.sapQuoteNumber = '21';
 SQ.quoteHeader.salesOrg ='sales';
 SQ.quoteHeader.region ='r';
 SQ.quoteHeader.district_country='r';
 SQ.quoteHeader.quoteDescription='r';
 SQ.quoteHeader.shortDescription='r';
 AbDebugUtils.printSalesQuoteDebug(SQ);
}

public static testMethod void  testconvertStatusMessages(){
  List<Quote__c> qlist = AbQuotingUtils.convertStatusMessages( new List<Quote__c>());
 
}

	


}