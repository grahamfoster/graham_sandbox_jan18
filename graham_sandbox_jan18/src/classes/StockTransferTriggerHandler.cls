/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 27/2015
**
** When a Stock Transfer record (managed custom object) is created or edited, if the stock transfer pick list = “Submitted” then set the status of all line items to “Submitted”
** 
**/
public class StockTransferTriggerHandler extends BaseTriggerHandler 
{
	public void onAfter()
    {
        Set<Id> stockTransferIds = new Set<Id>();
        for(SVMXC__Stock_Transfer__c stockTransfer : (List<SVMXC__Stock_Transfer__c>)Trigger.new)
        {
            if(stockTransfer.SVC_Stock_Transfer__c == 'Submitted')
            {
                stockTransferIds.add(stockTransfer.Id);
            }
        }
        
        SVMXC__Stock_Transfer_Line__c[] stockTransferLines = [
            select Id, SVC_Status__c  
			from SVMXC__Stock_Transfer_Line__c where SVC_Status__c != 'Submitted' and SVMXC__Stock_Transfer__c in :stockTransferIds
        ];
        for(SVMXC__Stock_Transfer_Line__c stockTransferLine : stockTransferLines)
        {
            stockTransferLine.SVC_Status__c = 'Submitted';
        }
        update stockTransferLines;
    }

}