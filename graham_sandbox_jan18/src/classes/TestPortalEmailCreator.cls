/**
 * TestPortalEmailCreator
 * 
 * Test class for portal email creator
 * 
 * Author: Graham Foster (graham.foster@sciex.com)
 * 
 * 
**/
@isTest
public class TestPortalEmailCreator {
    
    static testMethod void TestPortalEmailCreator(){
        //first create a case to assign the emails to
        Case testCase = new Case(Subject = 'Test Case');
        insert testCase;
        //now we test some things
        //first that a portal email is created for a case (the test case)
        EmailMessage test1 = new EmailMessage(ParentId = testCase.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.\n\n\n' +
														'-----Ursprüngliche Nachricht-----\n' + 
														'Von: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne', Subject = 'test', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='Test@Test.com');
            
        insert test1;
        //Check that the portal email was created
        List<Portal_Email__c> testPortalEmails = [SELECT Id, Email_Body__c FROM Portal_Email__c WHERE CaseId__c=:testCase.Id];
        system.assert(testPortalEmails.size() == 1);
        //Check that the body has been reduced
        system.assertNotEquals(test1.TextBody.length(), testPortalEmails[0].Email_Body__c.length());
        
        //create a new email to check where split does not occur
        EmailMessage test2 = new EmailMessage(ParentId = testCase.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.', 
                                              			Subject = 'test', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='Test@Test.com');
            
        insert test2;
        List<Portal_Email__c> testPortalEmails2 = [SELECT Id, Email_Body__c FROM Portal_Email__c WHERE CaseId__c=:testCase.Id AND EmailMessageId__c=:test2.Id];
        system.assert(testPortalEmails2.size() == 1);
        //Check that the body has not been reduced
        system.assertEquals(test2.TextBody.length(), testPortalEmails2[0].Email_Body__c.length());
        
        //create a new email to test when subsequant splits appear after the first
        EmailMessage test3 = new EmailMessage(ParentId = testCase.Id,
                                              TextBody = 'Hallo Frau Bernard,\n\n' +
														'ja ich habe die Meta. Und es besteht weiterhin das Problem, ' + 
                                              			'dass die Massen meines internen Standard im negativen Modus eine deutlich höhere Massenabweichung haben.\n\n\n' +
														'-----Ursprüngliche Nachricht-----\n' + 
														'From: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
                                              			'Von: SCIEX Now [mailto:sciexnow@absciex.com]\n' +
														'Gesendet: Dienstag, 4. November 2014 16:31\n' +
														'An: Brüggen, Susanne', Subject = 'test', FromAddress = 'sciexnow@absciex.com',
                                             			ToAddress='Test@Test.com');
            
        insert test3;
        List<Portal_Email__c> testPortalEmails3 = [SELECT Id, Email_Body__c FROM Portal_Email__c WHERE CaseId__c=:testCase.Id AND EmailMessageId__c=:test3.Id];
        system.assert(testPortalEmails3.size() == 1);
        //Check that the body has been reduced
        system.assertNotEquals(test3.TextBody.length(), testPortalEmails3[0].Email_Body__c.length());
    }

}