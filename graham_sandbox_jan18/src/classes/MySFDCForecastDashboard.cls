public class MySFDCForecastDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {

	public MySFDCForecastDashboard() {
		super();
	}

	/* custom fields */
	public Boolean isCurrentQuarter {get;
		set {
			if(value == null || value) report = 'CurrentForecast';
			else report = 'NextForecast';
		}
	}
	public Integer quarterNumber {get;set;}

	public String getReportTitle() {
		return 'Q' + quarterNumber + ' Forecast';
	}

	protected override Decimal applyConversionRate(AggregateResult aggResult, Decimal amount) {
		Boolean isWon = (Boolean)aggResult.get('IsWon');
		if(!isWon) {
			Decimal conversionRate = this.conversionRateMap.get('Forecast');
			if(conversionRate == null) conversionRate = 1.0;
			return amount * conversionRate;
		}else{
			return amount;
		}
	}
	
	/* the criteria for the query */
	protected override String criteria() {
		String crit = 'OwnerId in :userIds ' + 
			'and StageName != \'Dead/Cancelled\' and StageName != \'Deal Lost\' ' +
			'and ((Shippable_Date__c >= :startDate and Shippable_Date__c <= :endDate) ' +
			'or (Shippable_Date__c = NULL and CloseDate >= :startDate and CloseDate <= :endDate)) ' + 
			'and In_Forecast_Mgr__c = true and Mgr_Upside__c = false';

		if(MySFDCUtil.isServiceUser(filter.runAs)) {
			crit += ' and RecordType.Name = \'Service\'';
		}

		String marketVertical = filter.marketVertical;
		if(!String.isBlank(marketVertical)) {
			if(marketVertical == 'Clinical & Forensic') {
				crit += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(marketVertical == 'Food & Environmental') {
				crit += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				crit += ' and Market_Vertical__c = :marketVertical';
			}
		}

		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry in :country';
			}
		}
		return crit;
	}

	/* the default sorting criteria for the query */
	protected override String defaultSorting() {
		return ' order by Shippable_Date__c desc, CloseDate desc';
	}

	/* the list of fields for the report */
	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	/* Generate the google chart data */
	public override GoogleGaugeChartData getGoogleGaugeChartData() {
		GoogleGaugeChartData c11 = new GoogleGaugeChartData(report);
		c11.gaugeLabel = '($M)';
		c11.title = 'Q'+quarterNumber+' Forecast';
		c11.actual = this.actual;
		c11.quota = this.quota;
		return c11;
	}
}