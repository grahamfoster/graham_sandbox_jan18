@isTest
public with sharing class testopportunitytrigger {
	
public static testmethod void  testopporttrigger(){
	Persona__c pers=new persona__c();
	pers.Persona_Name__c = 'Lab Tech';
	insert pers;
	
	Persona__c pers1 = new persona__c();
	pers1.Persona_Name__c = 'abc';
	insert pers1;
	
	Opportunity opp=new Opportunity();
	opp.Name = 'Test';
	opp.StageName = 'Deal Won';
	opp.CloseDate = system.today();
	opp.Persona__c = pers.Id;
	insert opp;
	}
	
public static testmethod void  testopporttrigger1(){
	Persona__c pers=new persona__c();
	pers.Persona_Name__c = 'Lab Tech';
	insert pers;

	Opportunity opp=new Opportunity();
	opp.Name = 'Test';
	opp.StageName = 'Deal Won';
	opp.CloseDate = system.today();
	opp.Persona__c = pers.Id;
	insert opp;
	}

}