@isTest(SeeAllData=true)

public class LeadSetCountryMap_Test {

    static testmethod void Lead_Proper_Country() {
      // Create a test Lead
      Lead lea = new Lead(LastName='LeadTest',
                          Company = 'TestLead',
                          Status = 'Open',
                          Country='ca',
                          State = 'on');
      insert lea;
	  
   	  Country_Mapping__c Country = [Select ID, Name, Forecast_Region__c, Forecast_Territory__c from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];
        
      lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Name, Region__c, Forecast_Region__c from Lead where Id =: lea.Id];
      System.assertEquals('Canada', lea.Country);
      System.assertEquals('Ontario', lea.State);
      System.assertEquals(Country.ID, lea.Country_Mapping__c);
      System.assertEquals(State.ID, lea.State_Mapping__c);
      System.assertEquals(Country.Forecast_Region__c, lea.Forecast_Region__c);
      System.assertEquals(Country.Forecast_Territory__c, lea.Region__c);
    }     
    
      static testmethod void Lead_Improper_Country() {
      // Create a test Lead
      Lead lea = new Lead(LastName='LeadTest',
                          Company = 'TestLead',
                          Status = 'Open',
                          Country='something',
                          State='ON');
      insert lea;
	  
      lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Name from Lead where Id =: lea.Id];
      //System.assertEquals(NULL, lea.Country);
      //Lead Country Values should not be nulled out - can be utilized for scrubbing later on/adjustment of permutations
      System.assertEquals(NULL, lea.Country_Mapping__c);
      System.assertEquals('ON', lea.State);
      System.assertEquals(NULL, lea.State_Mapping__c);
    }    
    
      static testmethod void Lead_NULL_Country() {
      // Create a test Lead
      Lead lea = new Lead(LastName='LeadTest',
                          Company = 'TestLead',
                          Status = 'Open',
                          Country=NULL,
                          State=NULL);
      insert lea;
	  
      lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Name from Lead where Id =: lea.Id];
      System.assertEquals(NULL, lea.Country);
      System.assertEquals(NULL, lea.Country_Mapping__c);
      System.assertEquals(NULL, lea.State);
      System.assertEquals(NULL, lea.State_Mapping__c);
    }    
    
        static testmethod void Lead_ImProper_State() {
      // Create a test Lead
      Lead lea = new Lead(LastName='LeadTest',
                          Company = 'TestLead',
                          Status = 'Open',
                          Country='ca',
                          State = 'ot');
      insert lea;
	  
   	  Country_Mapping__c Country = [Select ID, Name from Country_Mapping__c where Country_Code__c = 'CA'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];
        
      lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Name from Lead where Id =: lea.Id];
      System.assertEquals('Canada', lea.Country);
      System.assertNotEquals('Ontario', lea.State);
      System.assertEquals(Country.ID, lea.Country_Mapping__c);
      System.assertNotEquals(State.Id, lea.State_Mapping__c);
    }
    
            static testmethod void Lead_MisMatch_State() {
      // Create a test Lead
      Lead lea = new Lead(LastName='LeadTest',
                          Company = 'TestLead',
                          Status = 'Open',
                          Country='us',
                          State = 'Ontario');
      insert lea;
	  
   	  Country_Mapping__c Country = [Select ID, Name from Country_Mapping__c where Country_Code__c = 'US'];
      State_Mapping__c State = [Select ID, Name from State_Mapping__c where Name = 'Ontario'];
        
      lea = [select Id, State, State_Mapping__c, Country, Country_Mapping__c, Name from Lead where Id =: lea.Id];
      System.assertEquals('United States', lea.Country);
      System.assertEquals('Ontario', lea.State);
      System.assertEquals(Country.ID, lea.Country_Mapping__c);
      System.assertNotEquals(State.Id, lea.State_Mapping__c);
    }
}