@isTest
public class ValueMapperAction_test {

	@isTest
	public static void ValueMapperAction_testing()
	{
		Value_Mapper__c vm1 = new Value_Mapper__c(Name = 'testMap1', Output_DataType__c = 'String', 
		Set_Name__c = 'StringMapping', Source_Field__c = 'Status', Source_Value__c = 'New', Target_Field__c = 'QTRICS_Callback_Info__c', 
		Target_Value__c = 'TestNewOutput', sObjectType__c = 'Case');
		Value_Mapper__c vm2 = new Value_Mapper__c(Name = 'testMap2', Output_DataType__c = 'Boolean', 
		Set_Name__c = 'BoolMapping', Source_Field__c = 'Status', Source_Value__c = 'New', Target_Field__c = 'QTRICS_ManagerCall__c', 
		Target_Value__c = 'true', sObjectType__c = 'Case');
		Value_Mapper__c vm3 = new Value_Mapper__c(Name = 'testMap3', Output_DataType__c = 'Integer', 
		Set_Name__c = 'IntMapping', Source_Field__c = 'Status', Source_Value__c = 'Test', Target_Field__c = 'QTRICS_Recommend__c', 
		Target_Value__c = '1', sObjectType__c = 'Case');
		Value_Mapper__c vm4 = new Value_Mapper__c(Name = 'testMap4', Output_DataType__c = 'Decimal', 
		Set_Name__c = 'IntMapping', Source_Field__c = 'Status', Source_Value__c = 'Test', Target_Field__c = 'Total_Number_of_FAS_Days__c', 
		Target_Value__c = '1.5', sObjectType__c = 'Case');
		Value_Mapper__c vm5 = new Value_Mapper__c(Name = 'testMap5', Output_DataType__c = 'Decimal', 
		Set_Name__c = 'IntMapping', Source_Field__c = 'Status', Source_Value__c = 'TestError', Target_Field__c = 'Total_Number_of_FAS_Days__c', 
		Target_Value__c = 'xx', sObjectType__c = 'Case');
		Value_Mapper__c vm6 = new Value_Mapper__c(Name = 'testMap6', Output_DataType__c = 'Integer', 
		Set_Name__c = 'IntMapping', Source_Field__c = 'Status', Source_Value__c = 'TestError1', Target_Field__c = 'Total_Number_of_FAS_Days__c', 
		Target_Value__c = 'xx', sObjectType__c = 'Case');
		Value_Mapper__c vm7 = new Value_Mapper__c(Name = 'testMap7', Output_DataType__c = 'Boolean', 
		Set_Name__c = 'IntMapping', Source_Field__c = 'Status', Source_Value__c = 'TestError2', Target_Field__c = 'Total_Number_of_FAS_Days__c', 
		Target_Value__c = 'xx', sObjectType__c = 'Case');
		insert new List<Value_Mapper__c>{vm1,vm2,vm3,vm4,vm5,vm6,vm7};
		
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;

		Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
        insert c1;
        
        List<RecordType> rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'CICCase' LIMIT 1];
        
        Case cs1 = new Case(ContactId = c1.Id, AccountId = acct.Id,  
                            Subject = 'test', Description = 'Case Test', 
                            RecordTypeId = rt[0].Id, Status = 'New', QTRICS_ManagerCall__c = false);
        insert cs1;

		//check the values now (so testint the after inset trigger)
		Case cs2 = [SELECT QTRICS_Callback_Info__c, QTRICS_ManagerCall__c FROM Case WHERE Id = :cs1.Id];
		System.assertEquals('TestNewOutput', cs2.QTRICS_Callback_Info__c);
		System.assert(cs2.QTRICS_ManagerCall__c);

		//change the status so we can check after insert
		Case cs3 = new Case(Id = cs1.Id, Status = 'Test');
		update cs3;
		Case cs4 = [SELECT QTRICS_Recommend__c, Total_Number_of_FAS_Days__c FROM Case WHERE Id = :cs1.Id];
		System.assertEquals(1, cs4.QTRICS_Recommend__c);
		System.assertEquals(1.5, cs4.Total_Number_of_FAS_Days__c);
		try{	        
			Case cs5 = new Case(Id = cs1.Id, Status = 'TestError');
			update cs5;
		}
		catch (Exception  e){

			Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Decimal in Value Map') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		try{	        
			Case cs5 = new Case(Id = cs1.Id, Status = 'TestError1');
			update cs5;
		}
		catch (Exception  e){

			Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Integer in Value Map') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		try{	        
			Case cs5 = new Case(Id = cs1.Id, Status = 'TestError2');
			update cs5;
		}
		catch (Exception  e){

			Boolean expectedExceptionThrown =  e.getMessage().contains('Invalid Boolean in Value Map') ? true : false;
			System.AssertEquals(expectedExceptionThrown, true);
		}
		

	}

}