/*  UserTriggerHandler_Test
 *	
 *	Test class for UserTrigger and UserTriggerHandler.
 *	If there are other test classes related to UserTrigger, please document it here (as comments).
 * 
 * 	Created by Daniel Lachcik 2017-01-01 (HAPPY 150th BDAY CANADA) based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *  Daniel Lachcik 2017-01-01 - Celebrated Canada's 150th, drank beer, ate poutine with maple syrup, rode moose (twice) 
 *  Daniel Lachcik 2017-01-01 - Created for testing CPQChatterGroupAddition() method
 */

@isTest(SeeAllData=true)
public class UserTriggerHandler_Test {

@istest static void UserCreated_noCategory() {		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u1 = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test', 
                           Approval_Category__c = NULL);
        insert u1;        
        
        Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
        List<CollaborationGroupMember> CollabMbr1 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(0, CollabMbr1.size());
    }

@istest static void UserCreated_wCategory() {		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u1 = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test', 
                           Approval_Category__c = '5 - District Sales Manager');
        insert u1;        
        
        Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
        List<CollaborationGroupMember> CollabMbr1 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(1, CollabMbr1.size());
    }

@istest static void UserUpdated_wCategory() {		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u1 = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test', 
                           Approval_Category__c = NULL);
        insert u1;        
        
        Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
        List<CollaborationGroupMember> CollabMbr1 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(0, CollabMbr1.size());
    
        u1.Approval_Category__c = '5 - District Sales Manager';
        Update u1;

        List<CollaborationGroupMember> CollabMbr2 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(1, CollabMbr2.size());
    }

@istest static void UserUpdated_noCategory() {		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u1 = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test', 
                           Approval_Category__c = '5 - District Sales Manager');
        insert u1;        
        
        Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
        List<CollaborationGroupMember> CollabMbr1 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(1, CollabMbr1.size());
    
        u1.Approval_Category__c = NULL;
        Update u1;

        List<CollaborationGroupMember> CollabMbr2 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(0, CollabMbr2.size());
    }    

@istest static void UserUpdated_InActive() {		
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		User u1 = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test', 
                           Approval_Category__c = '5 - District Sales Manager', IsActive = FALSE);
        insert u1;        
        
        Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
        List<CollaborationGroupMember> CollabMbr1 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u1.Id ];   
        system.assertEquals(0, CollabMbr1.size());
    
/*        u1.IsActive = FALSE;
        Update u1;
		User u2 = [Select ID from user where ID =: u1.id];
        List<CollaborationGroupMember> CollabMbr2 = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: u2.Id ];   
        system.assertEquals(0, CollabMbr2.size());
*/    }      
}