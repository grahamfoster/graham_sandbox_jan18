global class BatchSMContract2Opportunity implements Database.Batchable<sObject> {
    
    global final String Query;

    global BatchSMContract2Opportunity(String q){
        Query=q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
      	return Database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
		Map<ID,SVMXC__Service_Contract__c> ContractMap = new Map<ID,SVMXC__Service_Contract__c>((List<SVMXC__Service_Contract__c>)scope);
        new createOpportunityfromServiceContract(ContractMap);
    }
   
    global void finish(Database.BatchableContext BC){
   	}	

}