public with sharing class userfavorites {

   public Id userId {get; set;}
   public List<Add_to_Favorites_Junction__c> userfav {get; set;}
   public List<favWrapper> fav_list;
   List<Package__c> pckgList {get; set;}
   public Set<Id> packageId = new Set<Id>();
   public List<Rating_Management__c> rm {get; set;}
   
   public userfavorites(){
       userId = userInfo.getuserId();
       userfav = [SELECT Id, Name, Package__c, User__c FROM Add_to_Favorites_Junction__c WHERE User__c =: userId AND isActive__c =: true ORDER BY LastModifiedDate LIMIT 20];
   }
   
   public List<favWrapper> getfav(){
         fav_list = new List<favWrapper>();
         for(Add_to_Favorites_Junction__c fv : userfav){
             packageId.add(fv.Package__c);
         }
         rm = [SELECT Id, Name, Package__c, Rating__c, User__c FROM Rating_Management__c WHERE Package__c IN: packageId AND User__c =: userId AND isActive__c =: true ORDER BY Rating__c DESC]; 
         pckgList = [SELECT Id, Name, Product__r.Product_Category__c, Product__r.Product_Category__r.Name, Product__r.Product_Category__r.Category_Icon__c, Product__r.Name,
                                      Competitor_Product__r.Name, Package_Name__c, Rating__c, Application_Variants__r.Application__r.Image__c, Application_Variants__r.Application_Variant_Name__c,
                                      Application_Variants__r.Color__c,Application_Variants__r.Application__r.Color__c FROM Package__c WHERE Id IN: packageId AND isActive__c =: true AND Product__c != null AND Product__r.Product_Category__c != null AND
                                      Competitor_Product__c != null ORDER BY Rating__c DESC Limit 20];         
         
         for(package__c pk :pckgList){
             //package__c pkc;
             integer fstr = 0;
             integer sstr = 0;
             integer nstr = 0;
             
             if(pk.Rating__c > 0){
             fstr = (integer)pk.Rating__c;
             if((pk.Rating__c - pk.Rating__c.round(roundingMode.DOWN))>0)
             sstr = 1;
             nstr = 5 - (integer)pk.Rating__c.round(roundingMode.DOWN) - sstr;
             fav_list.add(new favWrapper(pk,fstr,sstr,nstr,(decimal)pk.Rating__c));
             }else
             fav_list.add(new favWrapper(pk,0,0,5,0));
         }
         /*for(Rating_Management__c rmc : rm){
             package__c pkc;
             integer fstr = 0;
             integer sstr = 0;
             integer nstr = 0;
             for(package__c pk :pckgList){
                 if(pk.Id == rmc.Package__c){
                    pkc = new Package__c();
                    pkc = pk;
                 }   
             }
             if(rmc != null){
             fstr = (integer)rmc.Rating__c.round(roundingMode.DOWN);
             if((rmc.Rating__c - (integer)rmc.Rating__c.round(roundingMode.DOWN))>0)
             sstr = 1;
             nstr = 5 - (integer)rmc.Rating__c.round(roundingMode.DOWN) - sstr;
             fav_List.add(new favWrapper(pkc,rmc,fstr,sstr,nstr,(decimal)rmc.Rating__c));
             }else
             fav_List.add(new favWrapper(pkc,null,0,0,5,0));
         }
         for(package__c pk :pckgList){
             boolean flag = false;
             for(favWrapper fav: fav_List){
                 if(fav.pck == pk)
                 flag = true;
             }
             if(flag == false)
             fav_List.add(new favWrapper(pk,null,0,0,5,0));
         } */
         
         return fav_List;
   }
   
   public class favwrapper{
       public Package__c pck {get; set;} //Package object
       public Rating_Management__c rmc {get; set;} //Rating Management Object
       public integer fullStar {get; set;} //Rating stars
       public integer semiStar {get; set;} //Rating stars
       public integer noStar {get; set;} //Rating stars
       public decimal rating {get; set;} //Rating
        
        //public favwrapper(Package__c p, Rating_Management__c r, integer f, integer s, integer n, decimal ra) {
        public favwrapper(Package__c p, integer f, integer s, integer n, decimal ra) {
            this.pck = p; //assign package
            //this.rmc = r; //assign Rating Management
            this.fullStar = f; //assign number of full stars
            this.semiStar = s; //assign number of semi stars
            this.noStar = n; //assign number of no stars
            this.rating = ra;//assign the rating
        }
   }
}