@RestResource(urlMapping='/activations/register')
global class RegistrationController {
        
    private static void Finalize(Boolean success, String message, String diagnostics) {
        String result = '{ "success": ' + String.valueOf(success) + 
            (message != null ? ', "message": "' + message + '"' : '') + 
            (diagnostics != null ? ', "diagnostics": "' + diagnostics + '"' : '') + 
            ' }';
        RestContext.response.responseBody = Blob.valueOf(result);
    }
    
    private static void Finalize(Boolean success, String message) {
        Finalize(success, message, null);
    }
    
    private static void Finalize(Boolean success) {
        Finalize(success, null, null);
    }

    @HttpGet
    @HttpPost
    global static void register() {
    
        try {
        
            RestContext.response.addHeader('Content-Type', 'application/json');
        
            // Parse parameters.
            String activationId = RestContext.request.params.get('activationId');
            String fieldServiceUserEmail = RestContext.request.params.get('userEmail');
            
            // Validate parameters.
            if (activationId == null) {
                Finalize(false, 'An activation ID must be supplied.');
                return;
            }
            
            // Set up our variables.        
            Boolean detailsResponseSuccess = false;
            Boolean detailsResponseActivated = false;
            String detailsResponseMessage = null;
            String detailsResponseShipToEmail = null;
            String detailsResponseProductName = null;
            String detailsResponseProductVersion = null;
            String detailsResponseLicenseText = null;
            String detailsResponseLicenseFilename = 'License.lic';
            String detailsResponseLicenseFileLocation = null;
            String detailsResponseFulfillmentID = null;
            String detailsResponseExpirationDate = null;
            String detailsResponseHostID = null;
            String detailsResponseSerialNumber = null;
            String detailsResponseProductDisplayName = null;
            
            // Call our web service.
            HttpRequest request = new HttpRequest();
            request.setMethod('GET');
            System.debug('UserInfo.getOrganizationId() is: ' + UserInfo.getOrganizationId());
            List<LicenseActivationSettings__c> settingsList = Database.query('SELECT ActivationDetailsServletURI__c FROM LicenseActivationSettings__c WHERE Name = \'' + UserInfo.getOrganizationId() + '\'');
            LicenseActivationSettings__c settings = settingsList[0];
            String url = settings.ActivationDetailsServletURI__c + '?activationId=' + EncodingUtil.urlEncode(activationId, 'UTF-8'); 
            request.setEndpoint(url);
            Http http = new Http();
            HTTPResponse response = http.send(request);
            String jsonString = response.getBody();
            JSONParser parser = JSON.createParser(jsonString);
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() != JSONToken.FIELD_NAME)
                    continue;
                String fieldName = parser.getText();
                if (fieldName == 'success') {
                    parser.nextToken();
                    detailsResponseSuccess = parser.getBooleanValue();
                }
                if (fieldName == 'message') {
                    parser.nextToken();
                    detailsResponseMessage = parser.getText();
                }
                if (fieldName == 'productName') {
                    parser.nextToken();
                    detailsResponseProductName = parser.getText();
                }
                if (fieldName == 'productVersion') {
                    parser.nextToken();
                    detailsResponseProductVersion = parser.getText();
                }
                if (fieldName == 'licenseText') {
                    parser.nextToken();
                    detailsResponseLicenseText = parser.getText();
                }
                if (fieldName == 'licenseFilename') {
                    parser.nextToken();
                    detailsResponseLicenseFilename = parser.getText();
                }
                if (fieldName == 'licenseFileLocation') {
                    parser.nextToken();
                    detailsResponseLicenseFileLocation = parser.getText();
                }
                if (fieldName == 'shipToEmail') {
                    parser.nextToken();
                    detailsResponseShipToEmail = parser.getText();
                }
                if (fieldName == 'activated') {
                    parser.nextToken();
                    detailsResponseActivated  = parser.getBooleanValue();
                }
                if (fieldName == 'expirationDate') {
                    parser.nextToken();
                    detailsResponseExpirationDate = parser.getText();
                }
                if (fieldName == 'hostID') {
                    parser.nextToken();
                    detailsResponseHostID = parser.getText();
                }
                if (fieldName == 'fulfillmentID') {
                    parser.nextToken();
                    detailsResponseFulfillmentID = parser.getText();
                }
                if (fieldName == 'serialNumber') {
                    parser.nextToken();
                    detailsResponseSerialNumber = parser.getText();
                }
                if (fieldName == 'productDisplayName') {
                    parser.nextToken();
                    detailsResponseProductDisplayName = parser.getText();
                }
            }
                    
            if (!detailsResponseActivated) {
                Finalize(false, 'Activation record not found in FlexNet Operations.');
                return;
            }
                
            List<Contact> contacts = Database.query('SELECT Id, Email, FirstName, LastName, Account.Id FROM Contact WHERE Email = \'' + detailsResponseShipToEmail + '\'');
            
            if (contacts.size() <= 0) {
                Finalize(false, 'No contacts found matching ' + detailsResponseShipToEmail + '.');
                return;
            }
                
            Contact contact = contacts[0];
            
            Asset asset = null;
            SVMXC__Installed_Product__c installedProduct = null;
            if (detailsResponseSerialNumber != null) {
                List<Asset_MultipleContacts__c> contactAssets = Database.query('SELECT Asset__r.Id, Asset__r.Installed_Product__r.Id FROM Asset_MultipleContacts__c WHERE Contact__c = \'' + contact.Id + '\' AND Asset__r.SerialNumber = \'' + detailsResponseSerialNumber + '\'');
                if (contactAssets.size() > 0) {
                    asset = contactAssets[0].Asset__r;
                    installedProduct = contactAssets[0].Asset__r.Installed_Product__r;
                }
            }
            
            User user = null;
            
            if (fieldServiceUserEmail != null) {
                List<User> users = Database.query('SELECT Id FROM User WHERE Email = \'' + fieldServiceUserEmail + '\'');
                if (users.size() <= 0) {
                    Finalize(false, 'No field service user found matching ' + fieldServiceUserEmail + '.');
                    return;
                }
                user = users[0];
            }
            
            // Format is dd/MM/yy, for example: 16/08/2017 or 01/01/1900.

           
            Software_Activation__c activationRecord = new Software_Activation__c();

            
            activationRecord.Type__c = 'Activated';            
            //activationRecord.Expiration_Date__c = null;         
            
            if(detailsResponseExpirationDate != NULL && detailsResponseExpirationDate.length() > 0)
            {
            	String[] dateValues = detailsResponseExpirationDate.Split('/');
            	Integer expirationDay = Integer.valueOf(dateValues[0]);
            	Integer expirationMonth = Integer.valueOf(dateValues[1]);
            	Integer expirationYear = Integer.valueOf(dateValues[2]);
            	Date expirationDate = Date.newInstance(expirationYear, expirationMonth, expirationDay);
            	activationRecord.Expiration_Date__c = expirationDate;      

            }


            if (contact != null)
                activationRecord.Contact__c = contact.Id;
            if (contact.Account != null)
                activationRecord.Account__c = contact.Account.Id;
            activationRecord.Activation_Date__c = date.today();
            activationRecord.Activation_ID__c = activationId;
            if (asset != null)
                activationRecord.Software_Asset__c = asset.Id;
            if (installedProduct != null)
                activationRecord.Associated_Installed_Product__c = installedProduct.Id;
            activationRecord.Computer_ID__c = detailsResponseHostID;
            activationRecord.Contact_Email__c = contact.Email;
            activationRecord.Contact_First_Name__c = contact.FirstName;
            activationRecord.Contact_Last_Name__c = contact.LastName;     
            activationRecord.Fulfillment_ID__c = detailsResponseFulfillmentID;
            activationRecord.License_File_Location__c = detailsResponseLicenseFileLocation;
            activationRecord.License_Filename__c = detailsResponseLicenseFilename;
            activationRecord.License_Text__c = detailsResponseLicenseText;
            activationRecord.Product_Name__c = detailsResponseProductName;
            activationRecord.Product_Version__c = detailsResponseProductVersion;
            activationRecord.Serial_Number__c = detailsResponseSerialNumber;
            activationRecord.Product_Display_Name__c = detailsResponseProductDisplayName;

            if (user != null)
                activationRecord.Field_Service_Engineer__c = user.Id;
            
            insert activationRecord;
            
        }
        catch (Exception e) {
            System.debug(e);
            Finalize(false, e.getMessage(), e.getStackTraceString());
            return;
        }
        
        Finalize(true);
    }
}