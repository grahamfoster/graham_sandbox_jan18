/**
** @author Reid Beckett, Cloudware Connections
** @created Dec 28/2014
**
** Test coverage CostCurrencyHelper
** 
**/
@isTest(SeeAllData=true)
public class CostCurrencyHelperTests {
	public static testMethod void test1() {
		createTestPricebook();
		createTestCostRecords('USD');
		createTestCostRecords('GBP');

		Product2[] products = [
			select Id, ProductCode,
			(select Id, CurrencyIsoCode, Pricebook2Id, Product2Id from PricebookEntries where Pricebook2.IsStandard = true),
			(select Id, CurrencyIsoCode, SBQQ__UnitCost__c, Oracle_Item_Number__c from SBQQ__Costs__r)
			from Product2 where ProductCode like 'test-%'
		];

		CostCurrencyHelper h = new CostCurrencyHelper(products);
		h.process();

		CurrencyType cadRate = [select ConversionRate from CurrencyType where IsoCode = 'CAD'];
		Decimal expectedCostCAD = cadRate.ConversionRate;

		CurrencyType gbpRate = [select ConversionRate from CurrencyType where IsoCode = 'GBP'];
		Decimal expectedCostGBP = gbpRate.ConversionRate;

		system.assertEquals(5, [select Id from SBQQ__Cost__c where SBQQ__Product__r.ProductCode like 'test-%' and CurrencyIsoCode = 'CAD' ].size());
		system.assertEquals(5, [select Id from SBQQ__Cost__c where SBQQ__Product__r.ProductCode like 'test-%' and CurrencyIsoCode = 'USD' ].size());

		system.assertEquals(expectedCostCAD, [select SBQQ__UnitCost__c from SBQQ__Cost__c where SBQQ__Product__r.ProductCode = 'test-1' and CurrencyIsoCode = 'CAD' ].SBQQ__UnitCost__c);
		system.assertEquals(expectedCostGBP, [select SBQQ__UnitCost__c from SBQQ__Cost__c where SBQQ__Product__r.ProductCode = 'test-1' and CurrencyIsoCode = 'GBP' ].SBQQ__UnitCost__c);
	}

	public static testMethod void test2_batchable() {
		createTestPricebook();
		createTestCostRecords('USD');
		createTestCostRecords('GBP');

		Test.startTest();
		Database.executeBatch(new CostCurrencyBatchable(), 5);
		Test.stopTest();

		CurrencyType cadRate = [select ConversionRate from CurrencyType where IsoCode = 'CAD'];
		Decimal expectedCostCAD = cadRate.ConversionRate;

		CurrencyType gbpRate = [select ConversionRate from CurrencyType where IsoCode = 'GBP'];
		Decimal expectedCostGBP = gbpRate.ConversionRate;

		system.assertEquals(5, [select Id from SBQQ__Cost__c where SBQQ__Product__r.ProductCode like 'test-%' and CurrencyIsoCode = 'CAD' ].size());
		system.assertEquals(5, [select Id from SBQQ__Cost__c where SBQQ__Product__r.ProductCode like 'test-%' and CurrencyIsoCode = 'USD' ].size());

		system.assertEquals(expectedCostCAD, [select SBQQ__UnitCost__c from SBQQ__Cost__c where SBQQ__Product__r.ProductCode = 'test-1' and CurrencyIsoCode = 'CAD' ].SBQQ__UnitCost__c);
		system.assertEquals(expectedCostGBP, [select SBQQ__UnitCost__c from SBQQ__Cost__c where SBQQ__Product__r.ProductCode = 'test-1' and CurrencyIsoCode = 'GBP' ].SBQQ__UnitCost__c);
	}

	public static testMethod void test3_schedulable() {
		createTestPricebook();
		createTestCostRecords('USD');
		createTestCostRecords('GBP');

		Test.startTest();
		CostCurrencySchedulable.scheduleJob('Test cost currency sched');
		Test.stopTest();
	}

	private static void createTestCostRecords(String currencyIsoCode){
		List<SBQQ__Cost__c> costs = new List<SBQQ__Cost__c>();
		for(Product2 p : [select Id, ProductCode from Product2 where ProductCode like 'test-%']) {
			String externalId = p.ProductCode + '-' + currencyIsoCode;
			system.debug('externalId='+externalId);
			SBQQ__Cost__c c = new SBQQ__Cost__c(SBQQ__Product__c = p.Id, CurrencyIsoCode = currencyIsoCode,
				SBQQ__UnitCost__c = 1, SBQQ__Active__c = true, Oracle_Item_Number__c = externalId);
			costs.add(c);
		}
		insert costs;
	}

	private static void createTestPricebook(){
		Pricebook2 stdPricebook = [select Id from Pricebook2 where IsStandard = true limit 1];

		Pricebook2 pricebook = new Pricebook2(Name = 'Test Price Book', CurrencyIsoCode = 'USD', IsActive = true);
		insert pricebook; 

		List<Product2> testProducts = new List<Product2> {
			new Product2(Name = 'Test product 1', ProductCode = 'test-1', IsActive = true, CurrencyIsoCode = 'USD'),
			new Product2(Name = 'Test product 2', ProductCode = 'test-2', IsActive = true, CurrencyIsoCode = 'USD'),
			new Product2(Name = 'Test product 3', ProductCode = 'test-3', IsActive = true, CurrencyIsoCode = 'USD'),
			new Product2(Name = 'Test product 4', ProductCode = 'test-4', IsActive = true, CurrencyIsoCode = 'USD'),
			new Product2(Name = 'Test product 5', ProductCode = 'test-5', IsActive = true, CurrencyIsoCode = 'USD')
		};
		insert testProducts;

		List<PricebookEntry> pricebookEntries = new List<PricebookEntry>();
		for(Product2 tp : testProducts) {
			pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
			pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'CAD', UnitPrice = 1, IsActive = true));
			pricebookEntries.add(new PricebookEntry(Pricebook2Id = stdPricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'GBP', UnitPrice = 1, IsActive = true));
		}
		insert pricebookEntries;

		List<PricebookEntry> testPricebookEntries = new List<PricebookEntry>();
		for(Product2 tp : testProducts) {
			testPricebookEntries.add(new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'USD', UnitPrice = 1, IsActive = true));
			testPricebookEntries.add(new PricebookEntry(Pricebook2Id = pricebook.Id, Product2Id = tp.Id, CurrencyIsoCode = 'CAD', UnitPrice = 1, IsActive = true));
		}
		insert testPricebookEntries;
	}
}