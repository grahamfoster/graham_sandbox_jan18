@isTest
private class createOppFromSC_test {
    // Test for Auto generation of Opportunities from expiring Service/Maintenance Contracts
    // This test fires the BatchSMContract2Opportunity Class, which in turn calls the createOpportunityfromServiceContract2 class

    @testSetup static void setup() {
    	// Prepare initial sample data for running tests:

        //  c2oSettings__c
        	List<c2oSettings__c> c2o = new List<c2oSettings__c> {
                new c2oSettings__c( Name = 'Contract Renewals', Contract_Type__c = 'Service Agreement', Product_Type__c = 'Contract', RecordType__c = '012F00000011o28', Stage_Name__c = 'Recognition of Needs'),
                new c2oSettings__c( Name = 'Warranty Conversion', Contract_Type__c = 'Warranty and Extended Warranty', Product_Type__c = 'Contract', RecordType__c = '012F00000011o28', Stage_Name__c = 'Recognition of Needs')};
			insert c2o;                                                       

        //c2oExceptions__c
            List<c2oExceptions__c> c2oE = new List<c2oExceptions__c> {
                new c2oExceptions__c( Name = 'LIF 488 SS',  Exception_Type__c='CE', SalesRepID__c = ''),
                new c2oExceptions__c( Name = 'CEQ 8800', Exception_Type__c='CE', SalesRepID__c = '')};
			insert c2oE;   
        
        //  User
        	Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
			User u = new User(	Alias = 'sadmin', Email='sadmin@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
								LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='UniqueUn@testorg.test');
        	insert u;
       	
        //  Account
        	List<Account> acct = new List<Account> {
                new Account(Name = 'Test Co #1',CurrencyIsoCode = 'USD', BillingCountry = 'United States'),
                new Account(Name = 'Test Co #2',CurrencyIsoCode = 'USD', BillingCountry = 'United States'),
                new Account(Name = 'Test Co #3',CurrencyIsoCode = 'USD', BillingCountry = 'United States'),
                new Account(Name = 'Test Co #4',CurrencyIsoCode = 'CAD', BillingCountry = 'Canada'),
                new Account(Name = 'Test Co #5',CurrencyIsoCode = 'CAD', BillingCountry = 'Canada')};
        	insert acct;
        
        // Contact
        	List<Contact> c = new List<Contact>{
                new Contact(Account= acct.get(0), firstname='Bob', lastname='Smith', Contact_Status__C='Active'),
				new Contact(Account= acct.get(1), firstname='Bob', lastname='Smith', Contact_Status__C='Active'),
				new Contact(Account= acct.get(2), firstname='Bob', lastname='Smith', Contact_Status__C='Active'),
				new Contact(Account= acct.get(3), firstname='Bob', lastname='Smith', Contact_Status__C='Active'),
				new Contact(Account= acct.get(4), firstname='Bob', lastname='Smith', Contact_Status__C='Active')};
        	insert c;
        
        //  Product2
        List<Product2> prod = new List<Product2>{
            new Product2(Name='ABSX WARRANTY',ProductCode='ABSX WARRANTY'),
			new Product2(Name='Assurance 0PM',ProductCode='ABSX ASSURANCE 0PM|API3000'),
			new Product2(Name='Assurance 0PM',ProductCode='ABSX ASSURANCE 0PM|5800'),
			new Product2(Name='Assurance 0PM',ProductCode='ABSX ASSURANCE 0PM|SHIMXR'),
			new Product2(Name='Total Service Coverage',ProductCode='ST 8X5|LIF 488 SS'),
            new Product2(Name='ABSX ASSURANCE 0PM',ProductCode='ABSX ASSURANCE 0PM'),    
            new Product2(Name='Total Service Coverage',ProductCode='ST 8X5')    };
        insert prod;
        
        //  Pricebook
        List<Pricebook2> pb = new List<Pricebook2>{
            new Pricebook2 (Name='United States Price Book',CURRENCYISOCODE='USD',isActive=TRUE),
            new Pricebook2 (Name='Canada Price Book',CURRENCYISOCODE='CAD',isActive=TRUE)};
        	insert pb;

        
        //  PricebookEntry
        List<PricebookEntry> pbe = new List<PricebookEntry>{ 
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(0).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=4325.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(1).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(1).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=325.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(2).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(2).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=425.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(3).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(3).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=435.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(4).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(4).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=432.85),                   

            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(5).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(5).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=435.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(6).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.get(0).CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.get(0).Id,Product2id=prod.get(6).id,CurrencyIsoCode=pb.get(0).CurrencyIsoCode,isActive=TRUE,UnitPrice=432.85),                   

                
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(0).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(0).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=4325.85),                
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(1).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(1).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=325.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(2).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(2).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=425.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(3).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(3).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=435.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(4).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(4).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=432.85), 

            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(5).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(5).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=435.85),
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = prod.get(6).Id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode, UnitPrice = 10000, IsActive = true, UseStandardPrice = false),
            new PricebookEntry(Pricebook2ID=pb.get(1).Id,Product2id=prod.get(6).id,CurrencyIsoCode=pb.get(1).CurrencyIsoCode,isActive=TRUE,UnitPrice=432.85)                 
               
            };
        insert pbe;
    	
        //  Territory2
           	List<Territory2> t = [SELECT Name, Id, Default_Service_Pricebook__c  FROM Territory2 LIMIT 2];
        	
        //  ObjectTerritory2Association
        List<ObjectTerritory2Association> ot2a = new List<ObjectTerritory2Association> { 
            new ObjectTerritory2Association(ObjectId= acct.get(0).id, Territory2Id = t.get(0).id,AssociationCause='Territory2Manual'),
			new ObjectTerritory2Association(ObjectId= acct.get(1).id, Territory2Id = t.get(0).id,AssociationCause='Territory2Manual'),
			new ObjectTerritory2Association(ObjectId= acct.get(2).id, Territory2Id = t.get(0).id,AssociationCause='Territory2Manual'),
			new ObjectTerritory2Association(ObjectId= acct.get(3).id, Territory2Id = t.get(1).id,AssociationCause='Territory2Manual'),
			new ObjectTerritory2Association(ObjectId= acct.get(4).id, Territory2Id = t.get(1).id,AssociationCause='Territory2Manual') };
        insert ot2a;

        //  SVMXC__Installed_Product__c
        List<SVMXC__Installed_Product__c> ip = new List<SVMXC__Installed_Product__c> { 
        	new SVMXC__Installed_Product__c ( NAME='API3000 - AF28211410',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=acct.get(0).id,	SVMXC__PRODUCT__C=prod.get(1).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(0).id),
            new SVMXC__Installed_Product__c ( NAME='5800 - L2023555890',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=acct.get(0).id,	SVMXC__PRODUCT__C=prod.get(2).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(0).id  ),
			new SVMXC__Installed_Product__c ( NAME='SHIMXR - L2023535890',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=acct.get(1).id,	SVMXC__PRODUCT__C=prod.get(3).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(1).id),
			new SVMXC__Installed_Product__c ( NAME='LIF 488 SS - L235355890',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=acct.get(1).id,	SVMXC__PRODUCT__C=prod.get(4).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(1).id),
			new SVMXC__Installed_Product__c ( NAME='API3000 - L202355890',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=acct.get(2).id,	SVMXC__PRODUCT__C=prod.get(1).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(2).id),
			new SVMXC__Installed_Product__c ( NAME='5800 - L20235890',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=acct.get(2).id,	SVMXC__PRODUCT__C=prod.get(2).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(2).id),
			new SVMXC__Installed_Product__c ( NAME='SHIMXR - L2023890',CURRENCYISOCODE ='CAD',	SVMXC__COMPANY__C=acct.get(3).id,	SVMXC__PRODUCT__C=prod.get(3).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(3).id ),                  
			new SVMXC__Installed_Product__c ( NAME='LIF 488 SS - L2025890',CURRENCYISOCODE ='CAD',	SVMXC__COMPANY__C=acct.get(3).id,	SVMXC__PRODUCT__C=prod.get(4).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(3).id),
			new SVMXC__Installed_Product__c ( NAME='API3000 - L202353558',CURRENCYISOCODE ='CAD',	SVMXC__COMPANY__C=acct.get(4).id,	SVMXC__PRODUCT__C=prod.get(1).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(4).id),
			new SVMXC__Installed_Product__c ( NAME='5800 - L35355890',CURRENCYISOCODE ='CAD',	SVMXC__COMPANY__C=acct.get(4).id,	SVMXC__PRODUCT__C=prod.get(2).id,	SVMXC__SERIAL_LOT_NUMBER__C='TESTCLASS', SVMXC__Contact__c=c.get(4).id) };
       	insert ip;
        
        //  SVMXC__Service_Contract__c
        	
        	Date useDate = date.valueOf('2016-03-12');
        	Date useDate1 = date.valueOf('2016-10-09');
        	List<SVMXC__Service_Contract__c> SMC =  new List<SVMXC__Service_Contract__c>{ 	
                new SVMXC__Service_Contract__c(NAME='35305846',CURRENCYISOCODE='USD',SVMXC__COMPANY__C=acct.get(0).id,SVMXC__END_DATE__C=useDate,EXTERNAL_ID__C='11243.2',PARENT_CONTRACT_NUMBER__C='35305846',SVC_CATEGORY__C='Warranty and Extended Warranty',SVC_ITEM_MASTER_NAME__C=prod.get(0).id),
                new SVMXC__Service_Contract__c(NAME='35305847',CURRENCYISOCODE='USD',SVMXC__COMPANY__C=acct.get(1).id,SVMXC__END_DATE__C=useDate1,EXTERNAL_ID__C='11244.2',PARENT_CONTRACT_NUMBER__C='35305847',SVC_CATEGORY__C='Service Agreement',SVC_ITEM_MASTER_NAME__C=prod.get(0).id),
                new SVMXC__Service_Contract__c(NAME='35305848',CURRENCYISOCODE='USD',SVMXC__COMPANY__C=acct.get(2).id,SVMXC__END_DATE__C=useDate,EXTERNAL_ID__C='11245.2',PARENT_CONTRACT_NUMBER__C='35305847',SVC_CATEGORY__C='Service Agreement',SVC_ITEM_MASTER_NAME__C=prod.get(0).id),
				new SVMXC__Service_Contract__c(NAME='35305849',CURRENCYISOCODE='CAD',SVMXC__COMPANY__C=acct.get(3).id,SVMXC__END_DATE__C=useDate1,EXTERNAL_ID__C='11246.2',PARENT_CONTRACT_NUMBER__C='35305849',SVC_CATEGORY__C='Service Agreement',SVC_ITEM_MASTER_NAME__C=prod.get(0).id),                    
                new SVMXC__Service_Contract__c(NAME='35304741',CURRENCYISOCODE='CAD',SVMXC__COMPANY__C=acct.get(4).id,SVMXC__END_DATE__C=useDate,EXTERNAL_ID__C='11298.2',PARENT_CONTRACT_NUMBER__C='35304741',SVC_CATEGORY__C='Service Agreement',SVC_ITEM_MASTER_NAME__C=prod.get(0).id)};
            insert SMC;
        
        //  SVMXC__Service_Contract_Products__c   
			List<SVMXC__Service_Contract_Products__c> SMCprod =  new List<SVMXC__Service_Contract_Products__c>{ 	
                new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(1).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(1)',	SVC_ITEM_MASTER_NAME__C=prod.get(0).id,SVMXC__Installed_Product__c=ip.get(0).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.get(0).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(2).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(2)',	SVC_ITEM_MASTER_NAME__C=prod.get(0).id,SVMXC__Installed_Product__c=ip.get(1).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.get(1).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(3).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(0)',	SVC_ITEM_MASTER_NAME__C=prod.get(5).id,SVMXC__Installed_Product__c=ip.get(2).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.get(1).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(4).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(3)',	SVC_ITEM_MASTER_NAME__C=prod.get(6).id,SVMXC__Installed_Product__c=ip.get(3).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.get(2).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(1).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(4)',	SVC_ITEM_MASTER_NAME__C=prod.get(5).id,SVMXC__Installed_Product__c=ip.get(4).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.get(2).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(2).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(5)',	SVC_ITEM_MASTER_NAME__C=prod.get(5).id,SVMXC__Installed_Product__c=ip.get(5).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='CAD',SVMXC__SERVICE_CONTRACT__C=SMC.get(3).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(3).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(6)',	SVC_ITEM_MASTER_NAME__C=prod.get(5).id,SVMXC__Installed_Product__c=ip.get(6).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='CAD',SVMXC__SERVICE_CONTRACT__C=SMC.get(3).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(4).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(7)',	SVC_ITEM_MASTER_NAME__C=prod.get(6).id,SVMXC__Installed_Product__c=ip.get(7).id),
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='CAD',SVMXC__SERVICE_CONTRACT__C=SMC.get(4).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(1).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(8)',	SVC_ITEM_MASTER_NAME__C=prod.get(5).id,SVMXC__Installed_Product__c=ip.get(8).id),                    
				new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='CAD',SVMXC__SERVICE_CONTRACT__C=SMC.get(4).id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=prod.get(2).id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643733(9)',	SVC_ITEM_MASTER_NAME__C=prod.get(5).id,SVMXC__Installed_Product__c=ip.get(9).id)};    
           insert SMCprod;   

 		// Opportunities
        List<Opportunity> opp = new List<Opportunity>{
                new Opportunity(Account= acct.get(0), Pricebook2=pb.get(0), CurrencyIsoCode='USD', Previous_Contract__c=SMC.get(0).id, name='Test1',stagename='Recognition of Needs', CloseDate=useDate),
                new Opportunity(Account= acct.get(0), Pricebook2=pb.get(0), CurrencyIsoCode='USD', Previous_Contract__c=SMC.get(0).id, name='Test2',stagename='Recognition of Needs', CloseDate=useDate)};
        	insert opp;        
        
 		// Opportunity Lines
        List<OpportunityLineItem> oppL = new List<OpportunityLineItem>{
                new OpportunityLineItem(Opportunityid=Opp.get(0).id, PricebookEntryid=pbe.get(0).id, quantity=1, TotalPrice=10 ),
				new OpportunityLineItem(Opportunityid=Opp.get(1).id, PricebookEntryid=pbe.get(0).id, quantity=1, TotalPrice=10 )                    
                };
        	insert oppL;            
 
    } 
   
    static testmethod void testEmptyQuery(){
        Map<ID,SVMXC__Service_Contract__c> ContractMap = new Map< ID , SVMXC__Service_Contract__c >([SELECT id, Ownerid, CurrencyISOCode, SVMXC__COMPANY__C, SVMXC__CONTACT__C, SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__C, SVC_ITEM_MASTER_NAME__C,SVC_SALESPERSON__C,SVC_CATEGORY__C,Renewal_Opportunity_Created__c From SVMXC__Service_Contract__c WHERE SVMXC__Active__c = TRUE AND SVC_CATEGORY__C LIKE '%Not_Here%']);
        System.debug('TEST: testEmptyQuery() -Running test using - ' + ContractMap.size() + ' - records');
        
        Test.startTest();
        new createOpportunityfromServiceContract(ContractMap);    
        Test.stopTest();
        
        List<Opportunity> opp = [SELECT Id FROM Opportunity];
        System.assertEquals(2,opp.size());
    }
    
    static testmethod void testnoContractLineItems(){
	    Map<ID,SVMXC__Service_Contract__c> ContractMap = new Map< ID , SVMXC__Service_Contract__c >([SELECT id, Ownerid, CurrencyISOCode, SVMXC__COMPANY__C, SVMXC__CONTACT__C, SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__C, SVC_ITEM_MASTER_NAME__C,SVC_SALESPERSON__C,SVC_CATEGORY__C,Renewal_Opportunity_Created__c From SVMXC__Service_Contract__c]);
        List<SVMXC__Service_Contract_Products__c> SMCProd = new List<SVMXC__Service_Contract_Products__c>([SELECT Id FROM SVMXC__Service_Contract_Products__c]);
        delete SMCProd;
        System.debug('TEST: testnoContractLineItems() - Running test using - ' + ContractMap.size() + ' - records, 0 Line Items');
        
        Test.startTest();
        new createOpportunityfromServiceContract(ContractMap);    
        Test.stopTest();
        
        List<Opportunity> opp = [SELECT Id FROM Opportunity];
        System.assertEquals(ContractMap.size()+2,opp.size());        
		List<OpportunityLineItem> oppLine = [SELECT Id FROM OpportunityLineItem];
        System.assertEquals(2,oppLine.size());        
    }


    static testmethod void testNoInstalledProduct(){
      	Map<ID,SVMXC__Service_Contract__c> ContractMap = new Map< ID , SVMXC__Service_Contract__c >([SELECT id, Ownerid, CurrencyISOCode, SVMXC__COMPANY__C, SVMXC__CONTACT__C, SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__C, SVC_ITEM_MASTER_NAME__C,SVC_SALESPERSON__C,SVC_CATEGORY__C,Renewal_Opportunity_Created__c From SVMXC__Service_Contract__c]);
        List<SVMXC__Service_Contract_Products__c> SMCProd = new List<SVMXC__Service_Contract_Products__c>([SELECT Id,SVMXC__Installed_Product__c FROM SVMXC__Service_Contract_Products__c]);
        SMCProd.get(0).SVMXC__Installed_Product__c=null;
        update SMCProd;
        System.debug('TEST: testNoInstalledProduct() - Running test using - Contract Line item with missing Installed Product');
 system.debug('***@@@ ----------------- Start NoInstalledProduct' );        
        Test.startTest();
        new createOpportunityfromServiceContract(ContractMap);    
        Test.stopTest();
 system.debug('***@@@ ----------------- Start NoInstalledProduct' );       
        
    }
 /*
    static testmethod void testBatchable(){

List<Pricebook2> pb = [SELECT Id, name, isActive From Pricebook2];
List<PricebookEntry> pbe =  [SELECT id, Name, PRICEBOOK2ID, PRODUCTCODE, CurrencyISOCode,PRODUCT2ID, UnitPrice, ISACTIVE FROM PricebookEntry];             

system.debug('***@@@ ----------------- Start Batchable' );      
		Test.startTest();
        id batchInstanceId = Database.executeBatch(new BatchSMContract2Opportunity('SELECT id, Ownerid, CurrencyISOCode, SVMXC__COMPANY__C, SVMXC__CONTACT__C, SVMXC__END_DATE__C,PARENT_CONTRACT_NUMBER__C, SVC_ITEM_MASTER_NAME__C,SVC_SALESPERSON__C,SVC_CATEGORY__C,Renewal_Opportunity_Created__c From SVMXC__Service_Contract__c LIMIT 5'),50);
        Test.stopTest();        
system.debug('***@@@ ----------------- Stop Batchable' );         
    }
     
    static testmethod void testSchedule(){

system.debug('***@@@ ----------------- Start Schedule' );      
		Test.startTest();
        SchedulableSMC2Opportunity so = new SchedulableSMC2Opportunity();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('schedule batch Job',sch ,so);
        Test.stopTest();        
system.debug('***@@@ ----------------- Stop Schedule' );         
    }

    static testmethod void testMerge(){

system.debug('***@@@ ----------------- Start Merge' );      
		Test.startTest();
	    new SMC_Merge_Opps();
        Test.stopTest();        
system.debug('***@@@ ----------------- Stop Schedule' );         

}
*/       
}