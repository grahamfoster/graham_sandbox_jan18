global Class WSConvertContact2Lead{
    WebService static Id contact2Lead(String contactId){
        DateTime dt=System.now();
        Contact contactSelected = [select c.Account.Name, c.LastName,c.FirstName, c.Phone, c.Email,c.Fax,c.MobilePhone,c.Job_Functions_07__c,c.MailingStreet,c.MailingCity,c.MailingState,c.MailingPostalCode,c.MailingCountry,c.DM_Opt_Out__c from Contact c  where Id =: contactId];
        Lead newlead=new Lead();
        newlead.LastName=contactSelected .LastName;
        newlead.Phone=contactSelected.phone;
        newlead.Email=contactSelected.Email;
        newlead.Fax=contactSelected.Fax;
        newlead.MobilePhone=contactSelected.MobilePhone;
        newlead.DateEntered__c=dt;
        newlead.Company=contactSelected.Account.Name;
        newlead.Status  ='Open';
        newlead.Intent_To_Purchase__c='A - Hot (0-6 months purchase)';
        if(contactSelected.DM_Opt_Out__c==true){    
            newlead.HasOptedOutOfEmail =true;
        }
        newlead.FirstName=contactSelected.FirstName;
        newlead.Street=contactSelected.MailingStreet;
        newlead.City =contactSelected.MailingCity;
        newlead.State=contactSelected.MailingState;
        newlead.PostalCode=contactSelected.MailingPostalCode;
        newlead.Country=contactSelected.MailingCountry;
        newlead.Job_Function__c=contactSelected.Job_Functions_07__c;
        insert newlead;

        return newlead.Id;
    }
     static testmethod void testWSConvertContact2Lead(){
        //Contact con =new Contact(LastName='Cotact Last Name',MailingCity='PP',MailingCountry='Denmark');
       Contact contactSelected1 = [select Id,LastName,FirstName, Phone, Email,Fax,MobilePhone,Job_Functions_07__c,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,DM_Opt_Out__c,AccountId from Contact  limit 1];
        
        Id returnId=WSConvertContact2Lead.contact2Lead(contactSelected1.Id);    
    }
    static testmethod void testOnlyOnetrigger(){//trigger copytoforecastCategory on ForecastCategory__c
        ForecastCategory__c fc=new ForecastCategory__c();
        fc.label__c='FC Test';
        insert fc;
        ForecastCategory__c sfc=[Select Name From ForecastCategory__c where Id=:fc.Id];
        sfc.Name='TestForecastCategory';
        update sfc;
    }
   }