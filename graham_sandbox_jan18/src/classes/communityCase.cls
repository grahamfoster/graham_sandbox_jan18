global class communityCase {

	webservice static String createTheCase(String postId)
	{
		if(!String.isEmpty(postId))
		{
			String caseId = CreateCaseFromPost(postId);
			return caseId;
		}
		return null;
	} 

	public static String CreateCaseFromPost(String PostId)
	{
		//start by getting the reference to the post and its contents
		List<Community_Post__c> cp = [SELECT Id, Contact__c, Community_Thread__c, Community_Thread__r.Thread_Name__c, 
		Community_Thread__r.Community_URL__c, Community_Thread__r.Description__c, Community_Thread__r.Contact__r.Email, 
		Post_Body__c FROM Community_Post__c WHERE Id = :PostId LIMIT 1];
		
		Set<Id> threadIds = new Set<Id>();
		List<Case> newCases = new List<Case>();
		for (Community_Post__c post : cp)
		{
			// add the thread id to the set
			threadIds.add(post.Community_Thread__c);
			//create a new case
			Case c = new Case();
			c.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CIC Case').getRecordTypeId();
			c.ContactId = post.Contact__c;
			c.Subject = 'SCIEX Community - ' + post.Community_Thread__r.Thread_Name__c;
			c.Description = post.Community_Thread__r.Community_URL__c + '\n\n' + post.Post_Body__c;
			c.Origin = 'Community';
			newCases.add(c);
		}
		//add the new cases
		INSERT newCases;
		//add the existing posts as case comments
		List<Community_Post__c> threadPosts = [SELECT Contact__r.Email, Community_Thread__c, Post_Body__c FROM Community_Post__c 
		WHERE Community_Thread__c IN :threadIds ORDER BY PostId__c DESC];

		List<CaseComment> comments = new List<CaseComment>();

		//now add a reference to the post to show the case
		List<Community_Post__c> updatePosts = new List<Community_Post__c>();
		for (Integer i = 0; i < cp.size(); i++)
		{
			updatePosts.Add(new Community_Post__c(Id = cp[i].Id, SCIEXNow_Case__c = newCases[i].Id));
			
			for(Community_Post__c post1 : threadPosts)
			{
				if(post1.Community_Thread__c == cp[i].Community_Thread__c)
				{
					CaseComment cc1 = new CaseComment(ParentId = newCases[i].Id);
					cc1.IsPublished = false;
					cc1.CommentBody = 'Contact:' + post1.Contact__r.Email + '\n\n' + post1.Post_Body__c;
					comments.add(cc1);
				}
				
				
			}
			CaseComment cc = new CaseComment(ParentId = newCases[i].Id);
					cc.IsPublished = false;
					cc.CommentBody = 'Contact:' + cp[i].Community_Thread__r.Contact__r.Email + '\n\n' + cp[i].Community_Thread__r.Description__c;
					comments.add(cc); 
		}
		INSERT comments;
		UPDATE updatePosts;
		return newCases[0].Id;

	}

}