/**
** @author Reid Beckett, Cloudware Connections
** @created Apr 12/2017
**
** Test coverage for Asset trigger logic
**/
@isTest
public class AssetTrigger_Test 
{
	@isTest
	public static void test() 
	{
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;
        
        Asset ast = new Asset(Name = 'Test Asset', AccountId = acct.Id);

		Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1');
		insert new Contact[]{c1};

		insert ast;
		update ast;
		delete ast;
		undelete ast;			
	}

	@isTest
	public static void test_IP_Trigger() 
	{
		Account acct = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert acct;
        
		Contact c1 = new Contact(AccountId = acct.Id, FirstName = 'John', LastName = 'Doe1', LMS_Content_Entitled__c = false);
		insert new Contact[]{c1};

		system.debug('***** TEST: before installed product DML insert');
		SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(
            SVMXC__Company__c = acct.Id
        );
        insert ip;  //triggers create asset
		system.debug('***** TEST: after installed product DML insert');
        
        Asset ast = [select Id from Asset where Installed_Product__c = :ip.Id];

        Asset_MultipleContacts__c amc = new Asset_MultipleContacts__c(Contact__c = c1.Id, Asset__c = ast.Id);
        insert amc;
        
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(false, c1.LMS_Content_Entitled__c);

		//lines added by graham to mimic hardward entitlement as these
		//are no longer based on Coverage Start/End Date Jan 2018
		ast.NEWSCIEXNow_Hardware_Entitled__c = true;
		update ast;
        //end modifications
        AssetTriggerHandler.firstRun = true;
		ip.Current_Coverage_Start_Date__c = Date.today().addDays(-1); 
		ip.Current_Coverage_End_Date__c = Date.today().addYears(1);
        update ip;
		
        
        //contact will have content entitled = true via process builder process "Contact Entitled"
        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);

		system.debug('***** TEST: before asset DML update');
        update ast;        
		system.debug('***** TEST: after asset DML update');

        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
        system.assertEquals(true, c1.LMS_Content_Entitled__c);

        ip.Current_Coverage_End_Date__c = Date.today().addDays(-1);
		system.debug('***** TEST: before installed product DML update');
        update ip;
		system.debug('***** TEST: after installed product DML update');

        c1 = [select LMS_Content_Entitled__c from Contact where Id = :c1.Id];
//        system.assertEquals(false, c1.LMS_Content_Entitled__c);

	}


}