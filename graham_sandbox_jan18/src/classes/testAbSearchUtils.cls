/*********************************************************************
Name  : testAbSearchUtils
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the AbSearchUtils 

*********************************************************************/

public class testAbSearchUtils{

public static testMethod void testsearchAscendingCDIQuote(){
  List<Quote__c> listQuote =    AbSearchUtils.searchAscendingCDIQuote('searchCDIQuoteNum', 'searchSoldTo', ' searchShortDesc', Date.Today() ,
                                                                     Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                    'searchContactId', ' searchProduct', ' searchTemplate');
}                                 

public static testMethod void  testsearchDescendingCDIQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingCDIQuote ('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                    Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                   'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingSAPQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingSAPQuote ('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingSAPQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingSAPQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingSoldToQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingSoldToQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingSoldToQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingSoldToQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingAccountQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingAccountQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingAccountQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingAccountQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingContactQuote (){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingContactQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingContactQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingContactQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingShipToQuote (){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingShipToQuote ('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingShipToQuote (){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingShipToQuote ('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}


public static testMethod void  testsearchAscendingStatusQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingStatusQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingStatusQuote (){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingStatusQuote ('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}


public static testMethod void  testsearchAscendingCreatedDateQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingCreatedDateQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingCreatedDateQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingCreatedDateQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingValidEndQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingValidEndQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingValidEndQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingValidEndQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingShortDescQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingShortDescQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingShortDescQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingShortDescQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchAscendingZipQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchAscendingZipQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDescendingZipQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDescendingZipQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}

public static testMethod void  testsearchDefaultQuote(){
  List<Quote__c> quoteList = AbSearchUtils.searchDefaultQuote('searchCDIQuoteNum',  'searchSoldTo', 'searchShortDesc', Date.Today(),
                                                                   Date.Today(), System.now(), System.now(), 'searchAccountId',
                                                                  'searchContactId', ' searchProduct', ' searchTemplate'); 
}


}