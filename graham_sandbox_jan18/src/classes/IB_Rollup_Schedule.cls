/**
 * @author Brett Moore
 * @created - Mar 2017
 * @Revision  
 * @Last Revision 
 * 
 * Schedulalble APEX Class that 'rolls-up' contract coverage data from contracts to Install Base. 
 * 
 * 
**/
Global class IB_Rollup_Schedule implements Schedulable {
   
    global void execute(SchedulableContext sc) {

		Integer scope = 10;
        
		IB_Rollup_Batch b = new IB_Rollup_Batch();
		id batchInstanceId = database.executebatch(b,scope);
	}
}