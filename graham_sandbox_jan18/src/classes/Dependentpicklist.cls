public class Dependentpicklist { 
   
    public string recordTypeName{get;set;}
    public string brandName{get;set;}
    public string familyName{get;set;}
    List<SelectOption> options{get;set;}
    public opportunity oppList{get;set;}
    public string checkup{get;set;}
    public string checkup1{get;set;}
    public Dependentpicklist(){
        oppList = new Opportunity();
        oppList.name = 'Test';
        oppList.Type = 'New Business';
        oppList.StageName = 'Deal Lost';
        oppList.Amount = 10;
        oppList.CloseDate = Date.Today(); 
        checkup = 'false';
        checkup1 = 'false';
    }    
    
    public void check(){
        system.debug('test1>>>'+recordTypename);
        if(recordTypename == 'AB Sciex' || recordTypename== 'NA PSM'){
            checkup = 'true';
            checkup1 = 'false';
            system.debug('test1>>>'+checkup);
            system.debug('test1>>>'+checkup1);             
        }
        else if (recordTypename == 'Competitor'){
            checkup1 = 'true';
            checkup = 'false';
        }        
    }
    
    public string getcheckup(){
        return checkup;
    }
    
    public string getcheckup1(){
        return checkup1;
    }    
            
    Public List<SelectOption> getrecordTypes(){
        List<RecordType> rt = [Select name from recordtype where sobjecttype = 'Product2'];    
        options = new List<SelectOption>();
        options.add(new SelectOption('None','--- None ---')); 
        if(!rt.isEmpty())
        { 
            for(Recordtype r:rt){                 
                options.add(new SelectOption(r.name,r.name));
            }       
        }
        return options;
    }
    
    public List<SelectOption> getBrand(){
      options = new List<SelectOption>();
      Schema.sObjectType objType = Product2.getSObjectType();        
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();             
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
      list<Schema.PicklistEntry> values = fieldMap.get('Brand__c').getDescribe().getPickListValues();      
      system.debug('values>>>'+values);
      for (Schema.PicklistEntry a : values)
      {          
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }      
      return options;
    }
    
    public List<SelectOption> getfamily(){
      options = new List<SelectOption>();
      Schema.sObjectType objType = Product2.getSObjectType();        
      Schema.DescribeSObjectResult objDescribe = objType.getDescribe();             
      map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
      list<Schema.PicklistEntry> values = fieldMap.get('Family').getDescribe().getPickListValues();      
      system.debug('values>>>'+values);
      for (Schema.PicklistEntry a : values)
      {          
         options.add(new SelectOption(a.getLabel(), a.getValue())); 
      }      
      return options;
    }
    
    public void ok(){     
        system.debug('test>>'+oppList.Select_Competitor_that_Won_Order__c);
        system.debug('test1>>'+oppList.Competitor_Instrument_Type__c);
    }
}