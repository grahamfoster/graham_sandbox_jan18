/**
** @author Reid Beckett, Cloudware Connections
** @created Mar 3/2015
**
** Encapsulates data for a Goolge Visualization combo chart
** Assumes stacked bar chart, and the last data point is a line chart
** 
**/
public class GoogleComboChartData {
	public String id {get;set;}
	public String title {get;set;}
	public String hName {get;set;}
	public String vName {get;set;}
	public List<String> hLabels {get;set;}
	public List<String> vLabels {get;set;}
	public List<List<Double>> data {get;set;}
	public List<String> colors {get;set;}
	public String numberFormat {get;set;}

	public GoogleComboChartData(String id) 
	{
		this.id = id;
		hLabels = new List<String>();
		vLabels = new List<String>();
		data = new List<List<Double>>();
		colors = new List<String>();
	}

	/* assumes the last data point is a line series */
	public Integer lineSeriesIndex {
		get {
			return vLabels.size() - 1;
		}
	}

	public String colorsArray {
		get {
			List<String> cstrings = new List<String>();
			for(String c : colors) {
				cstrings.add('\'' + c + '\'');
			}
			return String.join(cstrings,',');
		}
	}
}