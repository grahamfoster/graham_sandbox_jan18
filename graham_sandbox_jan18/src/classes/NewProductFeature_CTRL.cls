public class NewProductFeature_CTRL{
    public Product_Category__c prodcat{get;set;}
    public Specifications_Meta__c specmeta{get;set;}
    public List<Specifications_Meta__c> specmetalist{get;set;}
    public String CategoryID{get;set;}
    public boolean errflag{get;set;}
    public Boolean isComplete{get; private set;}

    public NewProductFeature_CTRL(){
        CategoryID=ApexPages.currentPage().getParameters().get('catid');
        prodcat =[select Product_Category_Name__c,isActive__c  from Product_Category__c where id=:CategoryID];
        specmetalist= new List<Specifications_Meta__c>();
        specmeta= new Specifications_Meta__c();
        specmetalist.add(specmeta); 
        isComplete = false;
        errflag=false;
    }
    
   
    public PageReference InsertSpecification(){
    isComplete = false;
    Integer count1=specmetalist.size();  
    List<Specifications_Meta__c> specmetalist1= new List<Specifications_Meta__c>();
        try{
            if(specmetalist != null){
              
                for(Specifications_Meta__c ab:specmetalist){
                    if(ab.Specifications_Meta_Name__c != null) {
                        ab.Product_Category__c=CategoryID;
                        specmetalist1.add(ab);
                        if(count1 ==specmetalist1.size())
                         errflag=false;
                    }
                    else{
                       errflag=true;
                       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please Insert Product Feature Category Name');
                       ApexPages.Addmessage(myMsg);   
                       return null;
                    }
                }
                if(errflag==false){
                    insert specmetalist1;
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You information has been saved successfully');
                    ApexPages.Addmessage(myMsg);                
                }
            }
            isComplete = true; 
        }
         catch(Exception e){
            isComplete = false; 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage())); 
             
        
        
        }
        return null;
    }    
     // Four methods for add and remove row
    public void InsertMoreSpecification(){
        specmetalist.add(new Specifications_Meta__c());
    
    }
    public void RemoveSpecification(){
         integer a=specmetalist.size();
         if(a>1){
             specmetalist.remove(specmetalist.size()-1);
         }
     
    }
  
       
}