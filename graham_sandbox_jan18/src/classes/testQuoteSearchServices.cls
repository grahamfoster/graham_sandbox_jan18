/*********************************************************************
Name  : testQuoteSearchServices  
Author: Appirio, Inc.
Date  : July 30, 2008 
Usage : Used to test the QuoteSearchServices  

*********************************************************************/
public class testQuoteSearchServices  {

  public static testMethod void checkQuoteSearchServices() {

    QuoteSearchServices.listTemplatesOutput  cls1 = new QuoteSearchServices.listTemplatesOutput();
    QuoteSearchServices.quoteList  cls2 = new QuoteSearchServices.quoteList();
    QuoteSearchServices.searchParameters2  cls3 = new QuoteSearchServices.searchParameters2();
    QuoteSearchServices.searchQuoteInSAP  cls4 = new QuoteSearchServices.searchQuoteInSAP();
    QuoteSearchServices.template  cls5 = new QuoteSearchServices.template();
    QuoteSearchServices.igor  cls6 = new QuoteSearchServices.igor();
    QuoteSearchServices.listProductsOutput  cls7 = new QuoteSearchServices.listProductsOutput();
    QuoteSearchServices.ArrayOflistOfTemplates  cls8 = new QuoteSearchServices.ArrayOflistOfTemplates();
    QuoteSearchServices.products  cls9 = new QuoteSearchServices.products();
    QuoteSearchServices.ArrayOfproducts  cls10 = new QuoteSearchServices.ArrayOfproducts();
    QuoteSearchServices.ArrayOfIGORCODE  cls11 = new QuoteSearchServices.ArrayOfIGORCODE();
    QuoteSearchServices.searchQuoteInSAPResponse  cls12 = new QuoteSearchServices.searchQuoteInSAPResponse();
    QuoteSearchServices.templateList  cls13 = new QuoteSearchServices.templateList();
    QuoteSearchServices.listOfQuotes  cls14 = new QuoteSearchServices.listOfQuotes();
    QuoteSearchServices.ArrayOftemplate  cls15 = new QuoteSearchServices.ArrayOftemplate();
    QuoteSearchServices.IGORCODE2  cls16 = new QuoteSearchServices.IGORCODE2();
    QuoteSearchServices.ArrayOfquotes  cls17 = new QuoteSearchServices.ArrayOfquotes();
    QuoteSearchServices.listTemplatesInput  cls18 = new QuoteSearchServices.listTemplatesInput();
    QuoteSearchServices.searchQuoteInCDIResponse  cls19 = new QuoteSearchServices.searchQuoteInCDIResponse();
    QuoteSearchServices.ArrayOflistOfQuotes  cls20 = new QuoteSearchServices.ArrayOflistOfQuotes();
    QuoteSearchServices.ArrayOfigor  cls21 = new QuoteSearchServices.ArrayOfigor();
    QuoteSearchServices.sapQuoteList  cls22 = new QuoteSearchServices.sapQuoteList();
    QuoteSearchServices.IGORCODE  cls23 = new QuoteSearchServices.IGORCODE();
    QuoteSearchServices.productInfo  cls24 = new QuoteSearchServices.productInfo();
    QuoteSearchServices.ABI_SalesQuotation_process_sq2Wsd_Port  cls25 = new QuoteSearchServices.ABI_SalesQuotation_process_sq2Wsd_Port();
    
    QuoteSearchServices.templateInfo templateInfo = new QuoteSearchServices.templateInfo();
    QuoteSearchServices.searchQuote searchQuote = new QuoteSearchServices.searchQuote();
    QuoteSearchServices.productInfo productInfo = new QuoteSearchServices.productInfo();
    QuoteSearchServices.searchParameters2 searchParameters = new QuoteSearchServices.searchParameters2();
    QuoteSearchServices.templateList res1 = cls25.listTemplates(templateInfo);
    QuoteSearchServices.quoteList res2 = cls25.searchQuoteInCDI(searchQuote );
    QuoteSearchServices.productList res3 = cls25.listProducts(productInfo );
    QuoteSearchServices.sapQuoteList res4 = cls25.searchQuoteInSAP(searchParameters );
    
    QuoteSearchServices.ArrayOfESTAT  cls26 = new QuoteSearchServices.ArrayOfESTAT();
    QuoteSearchServices.searchQuoteInCDI  cls27 = new QuoteSearchServices.searchQuoteInCDI();
    QuoteSearchServices.searchQuote  cls28 = new QuoteSearchServices.searchQuote();
    QuoteSearchServices.listProductsInput  cls29 = new QuoteSearchServices.listProductsInput();
    QuoteSearchServices.ArrayOfIGORCODE2  cls30 = new QuoteSearchServices.ArrayOfIGORCODE2();
    QuoteSearchServices.quotes  cls31 = new QuoteSearchServices.quotes();
    QuoteSearchServices.ESTAT  cls32 = new QuoteSearchServices.ESTAT();
    QuoteSearchServices.templateInfo  cls33 = new QuoteSearchServices.templateInfo();
    QuoteSearchServices.productList  cls34 = new QuoteSearchServices.productList();
    QuoteSearchServices.searchParameters  cls35 = new QuoteSearchServices.searchParameters();
    QuoteSearchServices.listOfTemplates  cls36 = new QuoteSearchServices.listOfTemplates();
  }  
}