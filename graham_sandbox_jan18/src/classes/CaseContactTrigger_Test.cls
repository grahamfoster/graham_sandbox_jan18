/*
 *	CaseContactTrigger_Test
 *	
 *	Test class for ContactTrigger and ContactTriggerHandler.
 *
 *	If there are other test classes related to ContactTrigger, please document it here (as comments).
 * 
 * 	Created by Graham Foster 2017-01-06
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
private class CaseContactTrigger_Test {

	// IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] CaseContactTrigger_Test.test() [Begin]');
    	Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'Canada');
    	insert testAccount;
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', 
                                          AccountId = testAccount.Id, MailingCountry = 'Canada');
        insert testContact;
        Case testCase = new Case(ContactId = testContact.Id, Subject = 'Test', Description = 'Case');
        insert testCase;
    	Case_Contact__c iudu = new Case_Contact__c(Case__c = testCase.Id, Contact__c = testContact.Id);
		insert iudu;
		update iudu;
		delete iudu;
		undelete iudu;
        system.debug('[TEST] CaseContactTrigger_Test.test() [End]');
     }
    
    @isTest
    static void createOnboardingContact_test()
    {
        Account testAccount = new Account(Name = 'Test Account', BillingCountry = 'Canada');
    	insert testAccount;
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', 
                                          AccountId = testAccount.Id, MailingCountry = 'Canada');
        insert testContact;
        Customer_Onboard__c cobo = new Customer_Onboard__c();
        insert cobo;
        Case testCase = new Case(ContactId = testContact.Id, Subject = 'Test', Description = 'Case', Customer_Onboard__c = cobo.Id);
        insert testCase;
    	Case_Contact__c testCaseContact = new Case_Contact__c(Case__c = testCase.Id, Contact__c = testContact.Id);
        insert testCaseContact;
        //make sure that a cobo contact has been made
        List<OnBoarding_Contacts__c> testObc1 = [SELECT Id FROM OnBoarding_Contacts__c WHERE Customer_Onboard__c = :cobo.Id AND OnBoard_Contact__c = :testContact.Id];
        //create a new contact
        Contact testContact2 = new Contact(FirstName = 'Test2', LastName = 'Contact2', 
                                          AccountId = testAccount.Id, MailingCountry = 'Canada'); 
        insert testContact2;
        OnBoarding_Contacts__c testObc = new OnBoarding_Contacts__c(OnBoard_Contact__c = testContact2.Id, 
                                                                    Customer_Onboard__c = cobo.Id, 
                                                                    Role__c = 'Additional Learner');
        insert testObc;
        Case_Contact__c testCaseContact2 = new Case_Contact__c(Case__c = testCase.Id, Contact__c = testContact2.Id);
        insert testCaseContact2;
        //make sure that a cobo contact has not been made
        List<OnBoarding_Contacts__c> testObc2 = [SELECT Id FROM OnBoarding_Contacts__c WHERE Customer_Onboard__c = :cobo.Id AND OnBoard_Contact__c = :testContact2.Id];
        //test the first contact (one should have been made when we added the new case contact)
        system.assertEquals(1, testObc1.size());
        //now the second should equal 1 also as the onboard contact should not have been made
        //because it would be a duplicate
        system.assertEquals(1, testObc2.size());
    }
}