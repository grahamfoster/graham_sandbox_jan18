/**
 * PortalEmailCreator
 * 
 * Class designed to create a new object 'Portal_Email__C' for each new
 * email which is received if the email is associated with a 
 * case object.  The Portal Email will then be used on Sciex.com as
 * a representation of the EMailMessage.  This class removes subsequant
 * messages or replies from the text body so that only the most recent
 * message is shown in the customers view.
 * 
 * Author: Graham Foster (graham.foster@sciex.com)
 * 
 * 
**/

public class PortalEmailCreator {
    
    public void CreateEmails (List<EmailMessage> newEmailMessages){
        //create a list of PortalEmail objects to hold any
        //new objects we create for insert
        List<Portal_Email__c> newPortalEmails = new List<Portal_Email__c>();
        
        //sort through each of the emails and test if they are
		//related to cases - if they are not we can ignore them
		//a shortcut to deciding is to use the first 3 char of the
		//id - 500 indicates it is the child of a case object
		for(EmailMessage newEmail : newEmailMessages){
			if(String.valueOf(newEmail.ParentId).substring(0,3) == '500'){
			//the email is a child of a case so therefore could be
			//exposed to website so we need to cut down to only the most
			//recent communication
				system.debug('Creating portal email for case:' + newEmail.ParentId + ' - EmailMessage:' + newEmail.Id); 
				newPortalEmails.add(GeneratePortalEmail(newEmail));
            }
    	}
        try{
           database.insert(newPortalEmails); 
        } catch (DMLException e) {
            //log the error and send an email if something goes wrong
            system.debug('DML Exception when inserting Portal Emails:' + newPortalEmails);
     		Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
     		String[] toAddresses = new String[] {'sfops@sciex.com'};
     		mail.setToAddresses(toAddresses);
     		mail.setReplyTo('sfops@sciex.com');
     		mail.setSenderDisplayName('Portal Email error message');
     		mail.setSubject('Error Creating Portal Email');
     		mail.setPlainTextBody(e.getMessage());
     		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
	}
    
    private Portal_Email__c GeneratePortalEmail(EmailMessage newCaseEmail){
		//create a new empty portal email to add the email info to.
		//unfortuantely we need to duplicate any values we need as
		//EmailMessage does not support MasterDetail
		Portal_Email__c portalMail = new Portal_Email__c(
            CaseId__c = newCaseEmail.ParentId,
        	CC_Address__c = newCaseEmail.CcAddress,
        	EmailMessageId__c = newCaseEmail.Id,
        	From_Address__c = newCaseEmail.FromAddress,
        	Has_Attachment__c = newCaseEmail.HasAttachment,
        	To_Address__c = newCaseEmail.ToAddress
        	);
        system.debug('Creating portal email object for EmailMessage:' + newCaseEmail.Id);
        Integer splitIndex = -1;
        if(!String.isBlank(newCaseEmail.TextBody)){
            splitIndex = FindFirstFromIndex(newCaseEmail.TextBody);
                }
        system.debug('The split index is calculated as:' + splitIndex);
        if(splitIndex == -1){
            portalMail.Email_Body__c = newCaseEmail.TextBody;
        }else{
            portalMail.Email_Body__c = PortalEmailBody(newCaseEmail.TextBody,splitIndex);
        }
        
        return portalMail;
    }
    private integer FindFirstFromIndex(string messageBody){
        //use this function to find the first instance of
        //the From: entry so that we can split off the most
        //recent message
        
        //an array of from strings so that we can 
        //seperate from in different languages
        
        	List<String> seperators = new List<String>
        	{'VON:', 'FROM:', 'DE:', 'DA:', 'WROTE:'};
        
                String UpperMBody = messageBody.toUpperCase();     
        Integer Marker = -1;
        for(String s : seperators){
            if(UpperMBody.contains(s)){
                Integer sIndex = UpperMBody.indexOf(s);
                system.debug(s + ' found at index:' + sIndex);
                if(Marker == -1){
                    Marker = sIndex;
                }else{
                    if(sIndex < Marker){
                        Marker = sIndex;
                    }
                }
            }
        }
        return Marker;
    }
    
    private String PortalEmailBody(String messageBody, Integer splitPoint){
        //split the email string according to the position of
        //the seperator
        String body = messageBody.substring(0, splitPoint);
        //now just remove the last line as it always a blank line
        //or -----------Orignal Message------------ in local language
        String rtrnStr = '';
        List<String> bodyLines = body.split('\n');
        for(integer i = 0;i < bodyLines.size() - 2; i++){
            rtrnStr = rtrnStr + bodyLines[i] + '\n';
        }
        return rtrnStr;
    }
}