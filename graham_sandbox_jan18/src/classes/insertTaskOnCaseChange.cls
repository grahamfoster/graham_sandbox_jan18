public class insertTaskOnCaseChange{

public static Task createTaskOnChange( SObject newObj, SObject oldObj, String fieldName, String fieldLabel, Id CaseId, Id CaseOwnerId)
    {
    System.Debug('create new task = ' + CaseId);
    Task insertTask = new Task();
    insertTask.WhatId = CaseId;
    insertTask.OwnerId = CaseOwnerId; //newObj.get('OwnerId');
    insertTask.Status = 'Completed';
    insertTask.ActivityDate = System.today();
    
    String TaskSubject = fieldLabel + ' changed from "' + oldObj.get(fieldName) + '" to "' + newObj.get(fieldName) + '"';
    
    if (TaskSubject.length() > 80)
        insertTask.Subject = TaskSubject.subString(0,79);
    else
        insertTask.Subject = TaskSubject;
    
    //insertTask.Subject = fieldLabel + ' changed from ' + oldObj.get(fieldName) + ' to ' + newObj.get(fieldName);
    insertTask.Priority = 'Normal';
    insertTask.CurrencyISOCode = 'AUD';
    insertTask.IsReminderSet = False;
    insertTask.MH_Action__c = False;
    insertTask.Description = fieldLabel + ' changed from "' + oldObj.get(fieldName) + '" to "' + newObj.get(fieldName) + '"';
    System.Debug('about to return the task');

    return insertTask;
    }

static testMethod void testCreateTaskOnChange()
    {
        RecordType[] anz_sqi_case_record_type = [select id from RecordType where name = 'ANZ SQI Case' and IsActive = TRUE];
        id SQIRecordTypeId = anz_sqi_case_record_type.size() > 0 ? anz_sqi_case_record_type[0].Id : null;

        if (SQIRecordTypeId == null)
            return;

    	// This code to get a proper user id for the case to runs as the system user
    	
      //Profile p = [SELECT Id FROM Profile WHERE Name='Astadia Super User']; 
      Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
      User u = new User(Alias = 'utst123', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='utst123@absciex.com');
    	
    	System.runAs(u) {
    	
        Case case1 = new Case( 
        Subject='SQI Case', 
        RecordTypeId= SQIRecordTypeId, //'012R0000000CpkEIAS', 
        //SQI_backup_owner__c = '005A0000000zhuo', //'00520000000hXtRAAU',
        SQI_potential_SQI__c='Y',
        SQI_assessment_Info__c='Y', 
        SQI_local_impact__c='Y',
        SQI_SAP_reports__c='Y',
        SQI_Service_Bulletin__c='Y',
        SQI_Action_plan__c='Y',
        SQI_Communication_list__c='Y',
        SQI_Customers_notified__c='Y',
        SQI_Global_Product__c='Y',
        SQI_product_quality__c='Y',
        SQI_stock_quarantined__c='Y',
        SQI_blocked_orders__c='Y',
        SQI_stock_replenishment__c='Y',
        SQI_customers_product__c='Y',
        SQI_QNotes_completed__c='Y',
        SQI_ordered_service_parts__c='Y',
        SQI_Service_Bulletin_FSE__c='Y',
        SQI_confirmed_customers__c='Y',
        SQI_customer_return__c='Y',
        SQI_resolved_in_eyes_of_customer__c='Y',
        SQI_escalated_to_country_manager__c='Y',
        SQI_child_cases_closed__c='Y',
        SQI_Lessons_Learnt__c='Y',
        SQI_CAPA_report__c='Y',
        SQI_child_cases_linked__c='Y',
        SQI_assessment_info_details__c='abcd',
        SQI_local_impact_details__c='abcd',
        Communication_list_details__c='abcd',
        SQI_Customer_notification_comments__c='abcd',
        SQI_Global_product_comments__c='abcd',
        SQI_PQT_comments__c='abcd',
        SQI_stock_quarantined_details__c='abcd',
        SQI_blocked_order_details__c='abcd',
        SQI_stock_replenishment_details__c='abcd',
        SQI_customer_product_details__c='abcd',
        SQI_QNotes_details__c='abcd',
        SQI_ordered_service_details__c='abcd',
        SQI_confirmed_customers_comments__c='abcd',
        SQI_confirmation_of_returned__c='abcd',
        SQI_CAPA_Ref_No__c='abcd'
        );

        insert case1;

        if (case1.RecordTypeId != SQIRecordTypeId)
            return;
        
        case1.SQI_potential_SQI__c='N';
        case1.SQI_assessment_Info__c='N';
        case1.SQI_local_impact__c='N';
        case1.SQI_SAP_reports__c='N';
        case1.SQI_Service_Bulletin__c='N';
        case1.SQI_Action_plan__c='N';
        case1.SQI_Communication_list__c='N';
        case1.SQI_Customers_notified__c='N';
        case1.SQI_Global_Product__c='N';
        case1.SQI_product_quality__c='N';
        case1.SQI_stock_quarantined__c='N';
        case1.SQI_blocked_orders__c='N';
        case1.SQI_stock_replenishment__c='N';
        case1.SQI_customers_product__c='N';
        case1.SQI_QNotes_completed__c='N';
        case1.SQI_ordered_service_parts__c='N';
        case1.SQI_Service_Bulletin_FSE__c='N';
        case1.SQI_confirmed_customers__c='N';
        case1.SQI_customer_return__c='N';
        case1.SQI_resolved_in_eyes_of_customer__c='N';
        case1.SQI_escalated_to_country_manager__c='N';
        case1.SQI_child_cases_closed__c='N';
        case1.SQI_Lessons_Learnt__c='N';
        case1.SQI_CAPA_report__c='N';
        case1.SQI_child_cases_linked__c='N';
        case1.SQI_assessment_info_details__c='def';
        case1.SQI_local_impact_details__c='def';
        case1.Communication_list_details__c='def';
        case1.SQI_Customer_notification_comments__c='def';
        case1.SQI_Global_product_comments__c='def';
        case1.SQI_PQT_comments__c='def';
        case1.SQI_stock_quarantined_details__c='def';
        case1.SQI_blocked_order_details__c='def';
        case1.SQI_stock_replenishment_details__c='def';
        case1.SQI_customer_product_details__c='def';
        case1.SQI_QNotes_details__c='def';
        case1.SQI_ordered_service_details__c='def';
        case1.SQI_confirmed_customers_comments__c='def';
        case1.SQI_confirmation_of_returned__c='def';
        case1.SQI_CAPA_Ref_No__c='def';
        
        update case1;
        
    	}

   //Other trigger test

        // Get CIC Case record type id
        Id cicRecordTypeId = [select id from RecordType where name = 'CIC Case' and sObjectType = 'Case'].Id;

        if (cicRecordTypeId == null)
            return;


        //Get an existing contact        
        Contact c1= [Select Id, accountId From Contact  where accountid <> null limit 1];
       

        // Create a product 
        Product2 p1 = new Product2(Name='dna analyzer');
        insert p1;

        // Create an asset and linke to product and account
        Asset a1 = new Asset(Name='dna analyzer 12432134', Product2Id = p1.Id, AccountId=c1.accountId);
        insert a1;
        
                  
        // Insert: Case with Asset 
        Case c1a = new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId ,  AssetId=a1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
        
        insert c1a;   
                        
        // Insert: Case with Asset and Active Contract
        Case c1b = new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId , AssetId=a1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
        
        insert c1b;   


        // Insert: Case with Product
        Case c2 = new Case(  ContactId=c1.Id,RecordTypeId=cicRecordTypeId , Product__c=p1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
        
        insert c2;                   

        // Insert: Case with non-AB Product
        Case c3 = new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId , Non_AB_Product__c=true, Case_Type__c='Troubleshoot', Subject='Reagent is too thick', Description='How do it make it less viscous?');
        
        insert c3; 



        // Get non-Case record type id
        Id fasRecordTypeId = [select id from RecordType where name = 'FAS' and sObjectType = 'Case'].Id;

        If (fasRecordTypeId != null)
        {
            // Insert: non-CIC Case
            Case c4 = new Case(  ContactId=c1.Id, RecordTypeId=fasRecordTypeId , Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
            
            insert c4;  
        }
         

       // Update: Case with Asset 
        c1a.Description=c1a.Description+' problem disappeared.';       
        update c1a;
        
        
       // Update: Case with Asset and Active Contract
        c1b.Description=c1b.Description+' problem disappeared.';       
        update c1b;
                
       // Update: Case with Product 
        c2.Description=c2.Description+' problem disappeared.';           
        update c2;
       
        // Insert: Case with non-AB Product
        c3.Description=c3.Description+' problem disappeared.';
        update c3;       
   
    }
}