public class dataScreenAdmin_Ctrl{
    public List<Persona__c> personalist{get;set;}
    public List<Application__c> applicationlist{get;set;}
    //public List<Manufacturer__c> Manufacturerlist{get;set;}
    public List<Application_Variants__c> appvarlist{get;set;}
   // public map<String,list<Application_Variants__c>> Appmap{set;get;}
    
    public String delPersonaId{get;set;}
    public String selApp{get;set;}
    public String selPersonaId{get;set;}
    public String selManufacturerId{get;set;}
    public String selAppVariantId{get;set;}
      
    
    public dataScreenAdmin_Ctrl(){
        personalist  =[select id,isActive__c,Name,Persona_Name__c from Persona__c];
        //Manufacturerlist=[select id,isActive__c,Manufacturer_Name__c from Manufacturer__c];
        System.debug('applicationlist....'+applicationlist);
       // Appmap= new map<String,list<Application_Variants__c>>();
    }
    
   /* public List<Manufacturer__c> getManufacturerlist() {
        List<Manufacturer__c> Mnflist=[select id,isActive__c,Manufacturer_Name__c,Name from Manufacturer__c]; 
        return Mnflist;  
    }*/
    
    public void returnPersonaList() {
        personalist  =[select id,isActive__c,Name,Persona_Name__c from Persona__c];   
    }
    
    
    public void delPersona() {
        try {
            if(String.isNotEmpty(delPersonaId)) {
                Persona__c personaRecord =  new Persona__c(Id = delPersonaId);
                delete personaRecord;    
            } else if(String.isNotEmpty(selPersonaId)){
                Persona__c personaRecord =  new Persona__c(Id = selPersonaId);
                delete personaRecord;         
            }
        } catch(Exception e) {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));           
        }
        delPersonaId = null;
        selPersonaId = null;
        returnPersonaList();
    }
    
    
   /* public void delManufacturer(){
        try {
            if(String.isNotEmpty(selManufacturerId)) {
                Manufacturer__c mnfRecord =  new Manufacturer__c(Id = selManufacturerId);
                delete mnfRecord;    
            }
            selManufacturerId = null;
        } catch(Exception e){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));     
        }
    }*/
    
    public void delAppCategory(){
        Savepoint sp = Database.setSavepoint();
        try {
            if(String.isNotEmpty(selApp)){
                List<Application_Variants__c> varList = [SELECT Id FROM Application_Variants__c
                                                            WHERE Application__c = :selApp];
                delete varList;
                                                            
                Application__c app = new Application__c(Id = selApp);
                delete app;      
            }
            selApp = null;    
        } catch(Exception e){
            Database.rollback(sp);
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));     
        }
    }
    
    public void delAppVariant(){
        try {
            if(String.isNotEmpty(selAppVariantId)){
                Application_Variants__c variantObj = new Application_Variants__c(Id = selAppVariantId);
                delete variantObj;  
            }  
            selAppVariantId = null;  
        } catch(Exception e){ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));}      
    }
    
    public map<string, map<string, list<Application_Variants__c>>> getAppmap() {
        applicationlist=[select id,isActive__c,Application_Name__c,(select id,Application_Variant_Name__c,isActive__c,Application__c from Application_Variants__r order by  createdDate desc),Description__c,Type__c from Application__c where isActive__c = true order by  createdDate desc];
        map<string, map<string, list<Application_Variants__c>>> Appmap = new map<string, map<string, list<Application_Variants__c>>>();
        appvarlist= new List<Application_Variants__c>();
        map<string, list<Application_Variants__c>> appvarMap;
        
        if (applicationlist.size()>0){
        
            for (Application__c a :applicationlist){
                 if(a.Application_Variants__r != null && !a.Application_Variants__r.isEmpty()){
                        
                     for(Application_Variants__c  apvr :a.Application_Variants__r){
                        if(Appmap.containsKey(a.Id)){
                            appvarMap = Appmap.get(a.Id);
                            appvarlist = appvarMap.get(a.Application_Name__c);      
                        } else {
                            appvarlist= new List<Application_Variants__c>();  
                            appvarMap = new map<string, list<Application_Variants__c>>();
                        } 
                        appvarlist.add(apvr);
                        appvarMap.put(a.Application_Name__c, appvarlist);
                        Appmap.put(a.Id, appvarMap); 
                     }
                 }
                 else {
                    appvarMap = new map<string, list<Application_Variants__c>>();
                    appvarMap.put(a.Application_Name__c, new List<Application_Variants__c>());
                    Appmap.put(a.Id ,appvarMap);
                 }
            }
        }
        
       
        return Appmap;
    
    }
    
    
    
    public pagereference redirectadmin(){
        pagereference pageref = new pagereference('/apex/mainpageadmin?tabfocus=DataTab');
        return pageref;
        
    }


}