/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 24/2016
**
** Test coverage for Oracle Order trigger logic
**/
@isTest
public class OracleOrderTrigger_Test 
{
	//test all 4 trigger operations
	@isTest
	static void test()
	{
		setUp();
		Product2[] testProducts = new Product2[]{
			new Product2(ProductCode = 'TRNLP004', IsActive = true, Name = 'Test Training Product'),
			new Product2(ProductCode = 'DEF123', IsActive = true, Name = 'Test Ordered Product 1')
		};
		insert testProducts;

		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cob;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert obc;

		Oracle_Order__c oo = new Oracle_Order__c(MS_Product__c = 'DEF123');
		insert oo;

		oo.Opportunity__c = opp.Id;
		update oo;

		Test.stopTest();
		cob = [select Id, Product__c, Ordered_Product__c from Customer_Onboard__c where Id = :cob.Id];
		

		system.assertEquals(testProducts[0].Id, cob.Product__c);

		oo.Training_Product__c = null;
		update oo;

		oo = [select Training_Product__c from Oracle_Order__c where Id = :oo.Id];
		system.assertEquals('TRNLP004', oo.Training_Product__c);

		delete oo;
		undelete oo;

	}

	@isTest
	public static void test_cascadeToCOBO() 
	{
		setUp();
		Product2[] testProducts = new Product2[]{
			new Product2(ProductCode = 'ABC123', IsActive = true, Name = 'Test Training Product 1'),
			new Product2(ProductCode = 'ABC456', IsActive = true, Name = 'Test Training Product 2'),
			new Product2(ProductCode = 'DEF123', IsActive = true, Name = 'Test Ordered Product 1'),
			new Product2(ProductCode = 'DEF456', IsActive = true, Name = 'Test Ordered Product 2'),
			new Product2(ProductCode = 'GHI123', IsActive = true, Name = 'Test Additional Training Product 1'),
			new Product2(ProductCode = 'GHI456', IsActive = true, Name = 'Test Additional Training Product 2')
		};
		insert testProducts;

		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		Opportunity opp2 = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		insert new List<Opportunity>{opp, opp2};

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		Contact c2 = new Contact(FirstName = 'Test2', LastName = 'Contact2', AccountId = a.Id, Email='test2@example.com', LMS_Username__c = 'test2@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert new List<Contact>{c,c2};

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		Customer_Onboard__c cob2 = new Customer_Onboard__c(Opp_Order_No__c = '456', Opportunity__c = opp2.Id);
		insert new List<Customer_Onboard__c> {cob,cob2};

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		OnBoarding_Contacts__c obc2 = new OnBoarding_Contacts__c(Customer_Onboard__c = cob2.Id, OnBoard_Contact__c = c2.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert new List<OnBoarding_Contacts__c>{obc,obc2};

		Oracle_Order__c oo = new Oracle_Order__c();
		Oracle_Order__c oo2 = new Oracle_Order__c();
		insert new List<Oracle_Order__c>{oo,oo2};

		oo.Training_Product__c = 'ABC123';
		oo.MS_Product__c = 'DEF123';
		oo.Additional_Training_Product__c = 'GHI123';
		oo.Opportunity__c = opp.Id;
		oo2.Training_Product__c = 'ABC123';
		oo2.MS_Product__c = 'DEF123';
		oo2.Additional_Training_Product__c = 'GHI123';
		oo2.Opportunity__c = opp2.Id;
		update new List<Oracle_Order__c>{oo,oo2};

		//cob = [select Id, Product__c, Ordered_Product__c, Additional_Training_Product__c from Customer_Onboard__c where Id = :cob.Id];
		//system.assertEquals(testProducts[0].Id, cob.Product__c);
		//system.assertEquals(testProducts[2].Id, cob.Ordered_Product__c);
		//system.assertEquals(testProducts[4].Id, cob.Additional_Training_Product__c);

		oo.Training_Product__c = 'ABC456';
		oo.MS_Product__c = 'DEF456';
		oo.Additional_Training_Product__c = 'GHI456';
		update oo;
		Test.stopTest();
		
		cob = [select Id, Product__c, Ordered_Product__c, Additional_Training_Product__c from Customer_Onboard__c where Id = :cob.Id];
		system.assertEquals(testProducts[1].Id, cob.Product__c);
		system.assertEquals(testProducts[3].Id, cob.Ordered_Product__c);
		system.assertEquals(testProducts[5].Id, cob.Additional_Training_Product__c);

		cob2 = [select Id, Product__c, Ordered_Product__c, Additional_Training_Product__c from Customer_Onboard__c where Id = :cob2.Id];
		system.assertEquals(testProducts[0].Id, cob2.Product__c);
		system.assertEquals(testProducts[2].Id, cob2.Ordered_Product__c);
		system.assertEquals(testProducts[4].Id, cob2.Additional_Training_Product__c);

		//Test.stopTest();
	}

	@isTest
	public static void test_cascadeToCOBO_onInsert() 
	{
		setUp();
		Product2[] testProducts = new Product2[]{
			new Product2(ProductCode = 'TRNLP004', IsActive = true, Name = 'Test Training Product'),
			new Product2(ProductCode = 'DEF123', IsActive = true, Name = 'Test Ordered Product 1')
		};
		insert testProducts;

		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cob;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert obc;

		Oracle_Order__c oo = new Oracle_Order__c(Opportunity__c = opp.Id, MS_Product__c = 'DEF123');
		insert oo;

		cob = [select Id, Oracle_Order__c, Product__c from Customer_Onboard__c where Id = :cob.Id];
		system.assertEquals(oo.Id, cob.Oracle_Order__c);
		system.assertEquals(testProducts[0].Id, cob.Product__c);
		Test.stopTest();
	}

	@isTest
	public static void test_cascadeToCOBO_prioritized() 
	{
		setUp();
		Product2[] testProducts = new Product2[]{
			new Product2(ProductCode = 'ABC123', IsActive = true, Part_Priority__c = '2', Name = 'Test Training Product 1'),
			new Product2(ProductCode = 'ABC456', IsActive = true, Part_Priority__c = '1', Name = 'Test Training Product 2'),
			new Product2(ProductCode = 'ABC789', IsActive = true, Name = 'Test Training Product 3')
		};
		insert testProducts;

		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cob;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert obc;

		Oracle_Order__c oo = new Oracle_Order__c();
		insert oo;

		oo.Training_Product__c = 'ABC123 ABC456';
		oo.Opportunity__c = opp.Id;
		update oo;

		cob = [select Id, Product__c, Ordered_Product__c, Additional_Training_Product__c from Customer_Onboard__c where Id = :cob.Id];
		//system.assertEquals(testProducts[1].Id, cob.Product__c);
		//system.assertEquals(testProducts[0].Id, cob.Additional_Training_Product__c);

		oo.Training_Product__c = 'ABC456 ABC123';
		update oo;

		cob = [select Id, Product__c, Ordered_Product__c, Additional_Training_Product__c from Customer_Onboard__c where Id = :cob.Id];
		//system.assertEquals(testProducts[1].Id, cob.Product__c);
		//system.assertEquals(testProducts[0].Id, cob.Additional_Training_Product__c);

		cob.Additional_Training_Product__c = null;
        update cob;
        
        oo.Training_Product__c = 'ABC456 ABC789';
		update oo;

		cob = [select Id, Product__c, Ordered_Product__c, Additional_Training_Product__c from Customer_Onboard__c where Id = :cob.Id];
		//system.assertEquals(testProducts[1].Id, cob.Product__c);
		//system.assertEquals(testProducts[2].Id, cob.Additional_Training_Product__c);

		Test.stopTest();
	}    
    
	@isTest
	public static void test_OracleOrderToCOBO_on_MSProductUpdate() 
	{
		setUp();
		Product2[] testProducts = new Product2[]{
			new Product2(ProductCode = 'ABC123', IsActive = true, Name = 'Test Training Product 1'),
			new Product2(ProductCode = 'ABC456', IsActive = true, Name = 'Test Training Product 2'),
			new Product2(ProductCode = 'DEF123', IsActive = true, Name = 'Test Ordered Product 1'),
			new Product2(ProductCode = 'DEF456', IsActive = true, Name = 'Test Ordered Product 2'),
			new Product2(ProductCode = 'GHI123', IsActive = true, Name = 'Test Additional Training Product 1'),
			new Product2(ProductCode = 'GHI456', IsActive = true, Name = 'Test Additional Training Product 2')
		};
		insert testProducts;

		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel);
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cob = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cob;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cob.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert obc;

		Oracle_Order__c oo = new Oracle_Order__c(Opportunity__c = opp.Id);
		insert oo;

		oo.MS_Product__c = 'DEF123';
		update oo;
		Test.stopTest();
		cob = [select Id, Oracle_Order__c from Customer_Onboard__c where Id = :cob.Id];
		system.assertEquals(oo.Id, cob.Oracle_Order__c);
		
	}

    //Test that COBO->Oracle Order relationship gets set on Fast Opp without MS_Product__c on the Oracle Order
	@isTest
	public static void test_COBOToOrder_Fast() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel, LC_Vendor__c = 'Other',
                                         RecordTypeId = COBOUtil.getFastOpportunityRecordTypeId());
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cobo = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cobo;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cobo.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert obc;

		Oracle_Order__c oo = new Oracle_Order__c();
		insert oo;

		oo.Opportunity__c = opp.Id;
		update oo;
		Test.stopTest();
		cobo = [select Id, Oracle_Order__c from Customer_Onboard__c where Id = :cobo.Id];
		system.assertEquals(oo.Id, cobo.Oracle_Order__c);
		
	}

    //Negative Test that COBO->Oracle Order relationship does not get set on NA PSM ABS Opp without MS_Product__c on the Oracle Order
	public static void test_COBOToOrder_NonFast() 
	{
		setUp();
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		OpportunityStage oppStage = [select MasterLabel from OpportunityStage where IsActive = true and IsWon = false and IsClosed = false order by DefaultProbability desc limit 1];
		Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = a.Id, CloseDate = Date.today(), StageName = oppStage.MasterLabel,
                                         RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('NA PSM ABS').getRecordTypeId());
		insert opp;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		Customer_Onboard__c cobo = new Customer_Onboard__c(Opp_Order_No__c = '123', Opportunity__c = opp.Id);
		insert cobo;

		OnBoarding_Contacts__c obc = new OnBoarding_Contacts__c(Customer_Onboard__c = cobo.Id, OnBoard_Contact__c = c.Id, Role__c = 'Primary Learner');
		Test.startTest();
		insert obc;

		Oracle_Order__c oo = new Oracle_Order__c();
		insert oo;

		oo.Opportunity__c = opp.Id;
		update oo;

		cobo = [select Id, Oracle_Order__c from Customer_Onboard__c where Id = :cobo.Id];
		system.assertEquals(null, cobo.Oracle_Order__c);
		Test.stopTest();
	}    
    
    //set up creates custom settings
	public static void setUp() {
		AbsorbLMSSettings__c settings = AbsorbLMSSettings__c.getOrgDefaults();
		settings.Endpoint_URL__c = 'http://example.com';
		settings.API_Username__c = 'user';
		settings.API_Password__c = 'pass';
		settings.API_Token_Timeout__c = 240;
		upsert settings;
	}

	@isTest
	public static void test_updateTrainingCaseFASDays()
	{
		Test.startTest();
		Oracle_Order__c oo = new Oracle_Order__c(Total_Number_of_FAS_Days_Purchased__c = 1);
		insert oo;
		Case c1 = new Case(Subject = 'Test', Description = 'Test', Number_of_FAS_Days__c = 1, Oracle_Order__c = oo.Id);
        insert c1;
		oo.Total_Number_of_FAS_Days_Purchased__c = 2;
		update oo;
		Test.stopTest();
		case cU = [SELECT Total_Number_of_FAS_Days__c FROM Case WHERE Id = :c1.Id LIMIT 1];
		system.assertEquals(2, cU.Total_Number_of_FAS_Days__c);
	}

	@IsTest static void test_setCOBOStatusToShipment()
	{
		Oracle_Order__c oo  = new Oracle_Order__c();
		insert oo;
		Customer_Onboard__c cobo = new Customer_Onboard__c(Status__c = 'Order Confirmed', Oracle_Order__c = oo.Id);
		insert cobo;
		
		Test.startTest();
		oo.MS_Instrument_Status__c = System.today();
		update oo;
		Test.stopTest();
		Customer_Onboard__c cobo1 = [SELECT Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals('Shipment', cobo1.Status__c);
	}

	@isTest static void test_setCOBOStatusToInstallation()
	{
		Oracle_Order__c oo  = new Oracle_Order__c();
		insert oo;
		Customer_Onboard__c cobo = new Customer_Onboard__c(Status__c = 'Shipment', Oracle_Order__c = oo.Id);
		insert cobo;
		
		Test.startTest();
		oo.Shipment_Status__c = 'Closed';
		update oo;
		Test.stopTest();
		Customer_Onboard__c cobo1 = [SELECT Status__c FROM Customer_Onboard__c WHERE Id = :cobo.Id];
		System.assertEquals('Installation', cobo1.Status__c);
	}
}