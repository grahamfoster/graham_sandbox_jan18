/**
** @author Reid Beckett, Cloudware Connections
** @created July 14/2014
**
** Refactored out of MySFDCController
** 
**/
public class MySFDCQuota {
	
	public Decimal currentQuarterQuota {get;set;}
	public Decimal nextQuarterQuota {get;set;}
	public Decimal ytdQuota {get;set;}

	/* added for service */
	public Decimal thirdQuarterQuota {get;set;} //quota for 3rd quarter from this quarter
	public Decimal fourthQuarterQuota {get;set;}//quota for 4th quarter from this quarter

	/* for grouping by type, for Service */
	public Decimal fullYearQuota {get;set;}
	public Map<String, Decimal> currentQuarterQuotaByType {get;set;}
	public Map<String, Decimal> nextQuarterQuotaByType {get;set;}
	public Map<String, Decimal> thirdQuarterQuotaByType {get;set;}
	public Map<String, Decimal> fourthQuarterQuotaByType {get;set;}
	public Map<String, Decimal> ytdQuotaByType {get;set;}
	public Map<String, Decimal> fullYearQuotaByType {get;set;}

	public MySFDCQuota()
	{
		currentQuarterQuotaByType = new Map<String,Decimal>();
		nextQuarterQuotaByType = new Map<String,Decimal>();
		thirdQuarterQuotaByType = new Map<String,Decimal>();
		fourthQuarterQuotaByType = new Map<String,Decimal>();
		ytdQuotaByType = new Map<String,Decimal>();
		fullYearQuotaByType = new Map<String,Decimal>();
	}

	public void finalizeCalculations()
	{
		//TODO: if there are some by type, take the total.  Otherwise take the null type value
		if(currentQuarterQuotaByType != null && currentQuarterQuotaByType.size() > 0) 
		{
			Decimal total = 0;
			for(Decimal q : currentQuarterQuotaByType.values())
			{
				total += q;
			}
			currentQuarterQuota = total;
		}

		if(nextQuarterQuotaByType != null && nextQuarterQuotaByType.size() > 0) 
		{
			Decimal total = 0;
			for(Decimal q : nextQuarterQuotaByType.values())
			{
				total += q;
			}
			nextQuarterQuota = total;
		}

		if(thirdQuarterQuotaByType != null && thirdQuarterQuotaByType.size() > 0) 
		{
			Decimal total = 0;
			for(Decimal q : thirdQuarterQuotaByType.values())
			{
				total += q;
			}
			thirdQuarterQuota = total;
		}

		if(fourthQuarterQuotaByType != null && fourthQuarterQuotaByType.size() > 0) 
		{
			Decimal total = 0;
			for(Decimal q : fourthQuarterQuotaByType.values())
			{
				total += q;
			}
			fourthQuarterQuota = total;
		}

		if(ytdQuotaByType != null && ytdQuotaByType.size() > 0) 
		{
			Decimal total = 0;
			for(Decimal q : ytdQuotaByType.values())
			{
				total += q;
			}
			ytdQuota = total;
		}

		if(fullYearQuotaByType != null && fullYearQuotaByType.size() > 0) 
		{
			Decimal total = 0;
			for(Decimal q : fullYearQuotaByType.values())
			{
				total += q;
			}
			fullYearQuota = total;
		}

		if(fullYearQuota == null) fullYearQuota = 0;
		if(ytdQuota == null) ytdQuota = 0;
	}

	public void addCurrentQuarterQuota(Quota__c qta)
	{
		if(qta.Quota__c == null) return;
		if(String.isBlank(qta.Type__c)) {
			if(currentQuarterQuota == null) currentQuarterQuota = 0;
			currentQuarterQuota += qta.Quota__c;
		} else {
			if(currentQuarterQuotaByType == null) currentQuarterQuotaByType = new Map<String, Decimal>();
			currentQuarterQuotaByType.put(qta.Type__c, qta.Quota__c);
		}
	}

	public void addNextQuarterQuota(Quota__c qta)
	{
		if(qta.Quota__c == null) return;
		if(String.isBlank(qta.Type__c)) {
			if(nextQuarterQuota == null) nextQuarterQuota = 0;
			nextQuarterQuota += qta.Quota__c;
		} else {
			if(nextQuarterQuotaByType == null) nextQuarterQuotaByType = new Map<String, Decimal>();
			nextQuarterQuotaByType.put(qta.Type__c, qta.Quota__c);
		}
	}

	public void addThirdQuarterQuota(Quota__c qta)
	{
		if(qta.Quota__c == null) return;
		if(String.isBlank(qta.Type__c)) {
			if(thirdQuarterQuota == null) thirdQuarterQuota = 0;
			thirdQuarterQuota += qta.Quota__c;
		} else {
			if(thirdQuarterQuotaByType == null) thirdQuarterQuotaByType = new Map<String, Decimal>();
			thirdQuarterQuotaByType.put(qta.Type__c, qta.Quota__c);
		}
	}

	public void addFourthQuarterQuota(Quota__c qta)
	{
		if(qta.Quota__c == null) return;
		if(String.isBlank(qta.Type__c)) {
			if(fourthQuarterQuota == null) fourthQuarterQuota = 0;
			fourthQuarterQuota += qta.Quota__c;
		} else {
			if(fourthQuarterQuotaByType == null) fourthQuarterQuotaByType = new Map<String, Decimal>();
			fourthQuarterQuotaByType.put(qta.Type__c, qta.Quota__c);
		}
	}

	public void addYTDQuarterQuota(Quota__c qta)
	{
		if(qta.Quota__c == null) return;
		if(String.isBlank(qta.Type__c)) {
			if(ytdQuota == null) ytdQuota = 0;
			ytdQuota += qta.Quota__c;
		} else {
			if(ytdQuotaByType == null) ytdQuotaByType = new Map<String, Decimal>();
			ytdQuotaByType.put(qta.Type__c, qta.Quota__c);
		}
	}

	public void addFullYearQuota(Quota__c qta)
	{
		if(qta.Quota__c == null) return;
		if(String.isBlank(qta.Type__c)) {
			if(fullYearQuota == null) fullYearQuota = 0;
			fullYearQuota += qta.Quota__c;
		} else {
			if(fullYearQuotaByType == null) fullYearQuotaByType = new Map<String, Decimal>();
			Decimal quota = fullYearQuotaByType.containsKey(qta.Type__c) ? fullYearQuotaByType.get(qta.Type__c) : 0;
			quota += qta.Quota__c;
			fullYearQuotaByType.put(qta.Type__c, quota);
		}
	}
}