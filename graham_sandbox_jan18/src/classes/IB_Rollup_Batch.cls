/**
 * @author Brett Moore
 * @created - Mar 2017
 * @Revision  
 * @Last Revision 
 * 
 * Batchable APEX Class that 'rolls-up' contract coverage data from contracts to Install Base. 
 * 
 * 
**/
global class IB_Rollup_Batch implements Database.Batchable<sObject> {
   
    global final String Query;
    global final String Process;

    global IB_Rollup_Batch(){
        Query='SELECT id, Current_Coverage_Start_Date__c, Current_Coverage_End_Date__c,Current_Item_Master__c,Current_Coverage_Description__c,Document_Number__c ,Seconds_Line_Contract__c FROM SVMXC__Installed_Product__c ORDER BY LastModifiedDate ASC';
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
      	return Database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
		Map<Id, SVMXC__Installed_Product__c> ibMap = new Map<Id, SVMXC__Installed_Product__c>((List<SVMXC__Installed_Product__c>)scope);
        new InstallBase_Rollup(ibMap);
    }
   
    global void finish(Database.BatchableContext BC){
   	}	
    
}