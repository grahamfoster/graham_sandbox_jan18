/*
 *	OpportunityLineItemTrigger_Test
 *	
 *	Test class for OpportunityLineItemTrigger and OpportunityLineItemTriggerHandler.
 *
 *	If there are other test classes related to OpportunityLineItemTrigger, please document it here (as comments).
 * 
 * 	Created by Brett Moore 2016-09-26 based on Framework by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
@isTest
public class OpportunityLineItemTrigger_Test {
    
    // Prepare initial sample data for running tests:	
    @testSetup static void setup() {
    	Account a = new Account(Name = 'Test Co #1',CurrencyIsoCode = 'USD', BillingCountry = 'United States');
        insert a;
        Contact c = new Contact(Account= a, firstname='Bob', lastname='Smith', Contact_Status__C='Active');
        insert c;
        Product2 p = new Product2(Name='ABSX WARRANTY',ProductCode='ABSX WARRANTY');
        insert p;
        Pricebook2 pb = new Pricebook2 (Name='United States Price Book',CURRENCYISOCODE='USD',isActive=TRUE);
        insert pb;
        List<PricebookEntry> pbe = new List<PricebookEntry>{ 
            new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = p.Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode=pb.CurrencyIsoCode),
            new PricebookEntry(Pricebook2ID=pb.Id,Product2id=p.id,CurrencyIsoCode=pb.CurrencyIsoCode,isActive=TRUE,UnitPrice=4325.85)};
		insert pbe;
        SVMXC__Installed_Product__c IB = new SVMXC__Installed_Product__c ( NAME='API3000 - AF28211410',CURRENCYISOCODE ='USD',	SVMXC__COMPANY__C=a.id,	SVMXC__PRODUCT__C=p.id,	SVMXC__SERIAL_LOT_NUMBER__C='123456', SVMXC__Contact__c=c.id);
    	insert IB;
        Date useDate = date.valueOf('2016-03-12');
       	SVMXC__Service_Contract__c SMC = new SVMXC__Service_Contract__c (NAME='35305846',CURRENCYISOCODE='USD',SVMXC__COMPANY__C=a.id,SVMXC__END_DATE__C=useDate,EXTERNAL_ID__C='11243.2',PARENT_CONTRACT_NUMBER__C='35305846',SVC_CATEGORY__C='Warranty and Extended Warranty',SVC_ITEM_MASTER_NAME__C=p.id);
 		insert SMC;
        SVMXC__Service_Contract_Products__c SMCline = new SVMXC__Service_Contract_Products__c(CURRENCYISOCODE='USD',SVMXC__SERVICE_CONTRACT__C=SMC.id,	SVMXC__END_DATE__C=useDate,SVMXC__PRODUCT__C=p.id,SVMXC__START_DATE__C=useDate,EXTERNAL_ID__C='337534904486792582499610120361032643784(1)',	SVC_ITEM_MASTER_NAME__C=p.id,SVMXC__Installed_Product__c=ib.id);
 		insert SMCline;
        Opportunity o = new Opportunity(Account= a, Pricebook2=pb, CurrencyIsoCode='USD', Previous_Contract__c=SMC.id, name='Test1',stagename='Recognition of Needs', CloseDate=useDate);
        insert o;
        Opportunity_IB__c oib = new Opportunity_IB__c(Covered_Product__c=SMCline.id , Opportunity__c = o.id);
        insert oib;

    }
    
    
    // IUDU (Insert Update Delete Undelete) test
    @isTest static void test() {
        system.debug('[TEST] OpportunityLineItemTrigger_Test.test() [Begin]');
        List <Opportunity> o = new List<Opportunity>([SELECT id FROM Opportunity ]);
        List <PricebookEntry> pbe = new List <PricebookEntry>([SELECT id FROM PricebookEntry]);
        Test.startTest();
	        OpportunityLineItem oLine = new OpportunityLineItem(Opportunityid = o.get(0).id, PricebookEntryid= pbe.get(0).Id, Quantity= 1, TotalPrice = 53 );
	        insert oLine;
	    	oLine.Quantity = 5;
			update oLine;
			delete oLine;
        	//  no undelete, Entity type is not undeletable
        Test.stopTest();
        system.debug('[TEST] OpportunityLineItemTrigger_Test.test() [End]');
    }
    
    // IUDU (Insert Update Delete Undelete) test
    @isTest static void setOnOpportunity_test() {
        system.debug('[TEST] OpportunityLineItemTrigger_Test.setOnOpportunity() [Begin]');
        List <Opportunity> o = new List<Opportunity>([SELECT id FROM Opportunity ]);
        List <PricebookEntry> pbe = new List <PricebookEntry>([SELECT id FROM PricebookEntry]);
        List <Opportunity_IB__c> oib = new List <Opportunity_IB__c> ([SELECT id, On_Opportunity__c FROM Opportunity_IB__c]);
        Test.startTest();
	        OpportunityLineItem oLine = new OpportunityLineItem(Opportunity_IB__c = oib.get(0).id, Opportunityid = o.get(0).id, PricebookEntryid= pbe.get(0).Id, Quantity= 1, TotalPrice = 53 );
	        insert oLine;
        	delete oLine;
        Test.stopTest();
        system.debug('[TEST] OpportunityLineItemTrigger_Test.tesetOnOpportunity() [End]');
    }
    
}