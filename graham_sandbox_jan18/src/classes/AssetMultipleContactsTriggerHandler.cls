/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 15/2017
**
** Handler for AssetMultipleContacts trigger logic
** When an AssetMultipleContacts is changed in any way, re-calculate if the related contact is content entitled based on it's linked assets
**/
public class AssetMultipleContactsTriggerHandler extends TriggerHandler {

    override protected void afterInsert() {
        updateContactEntitlement();
    }

	override protected void afterUpdate() {
        updateContactEntitlement();
    }

    override protected void afterDelete() {
        updateContactEntitlement();
    }
	
    override protected void afterUndelete() {
        updateContactEntitlement();
    }
    
    public void updateContactEntitlement() {
        //Aug 15/2017 - query all the linked contacts, then query ALL the assets linked to those contact
        Set<Id> linkedContactIds = new Set<Id>();
        List<Asset_MultipleContacts__c> lst = Trigger.isDelete ? (List<Asset_MultipleContacts__c>)Trigger.old : (List<Asset_MultipleContacts__c>)Trigger.new;
        for(Asset_MultipleContacts__c contactLink : lst) 
        {
            linkedContactIds.add(contactLink.Contact__c);
        }            

        Map<Id, Map<Id, Asset>> contactsAssets = new Map<Id, Map<Id, Asset>>();
        Set<Id> allAssetIds = new Set<Id>();
        for(Asset_MultipleContacts__c assetForContactLink : [
            select Id, Contact__c, Contact__r.LMS_Content_Entitled__c, Asset__c from Asset_MultipleContacts__c 
            where Asset__c != null and Contact__c in :linkedContactIds]) 
        {
            allAssetIds.add(assetForContactLink.Asset__c);
            if(contactsAssets.containsKey(assetForContactLink.Contact__c)) {
                contactsAssets.get(assetForContactLink.Contact__c).put(assetForContactLink.Asset__c, new Asset());
            }else{
                contactsAssets.put(assetForContactLink.Contact__c, new Map<Id, Asset>{ assetForContactLink.Asset__c => new Asset() });
            }
        }
        
        //query all the assets
        Map<Id, Asset> allAssetsById = new Map<Id, Asset>([
            select Id, SCIEXNow_Hardware_Entitled__c, SCIEXNow_Software_Entitled__c, SCIEXNOWSoftware_Upgrade_Entitled__c 
            from Asset where Id in :allAssetIds]);

        //update the full asset into the contactsAssets
        for(Map<Id, Asset> assetMap : contactsAssets.values()) {
            for(Id assetId : assetMap.keySet()){
            	if(allAssetsById.containsKey(assetId)) assetMap.put(assetId, allAssetsById.get(assetId));            
            }
        }
        
        //update all the linked contacts
        Map<Id, Contact> contactUpdates = new Map<Id, Contact>();
        for(Id contactId : contactsAssets.keySet()) {
            Map<Id, Asset> assetMap = contactsAssets.get(contactId);
            Boolean anyEntitled = false;
            for(Asset assetObj : assetMap.values()) {
                if(isContentEntitled(assetObj)) {
                    anyEntitled = true;
                }
            }
            contactUpdates.put(contactId, new Contact(Id = contactId, LMS_Content_Entitled__c = anyEntitled));
        }
        
        Map<Id, Contact> existingContacts = new Map<Id,Contact>([select Id, Content_Eligible_Approved__c from Contact where Id in :contactUpdates.keySet()]);
        for(Id contactId : contactUpdates.keySet()) {
            if(existingContacts.containsKey(contactId)) {
                Contact existingContact = existingContacts.get(contactId);
                if(existingContact.Content_Eligible_Approved__c != null && existingContact.Content_Eligible_Approved__c) {
                    contactUpdates.get(contactId).LMS_Content_Entitled__c = true;
                }
            }            
        }
        
		if(!contactUpdates.isEmpty()) update contactUpdates.values();        
    }

    private Boolean isContentEntitled(Asset ast) {
        return ast.SCIEXNow_Hardware_Entitled__c || ast.SCIEXNow_Software_Entitled__c || ast.SCIEXNOWSoftware_Upgrade_Entitled__c;
    }
}