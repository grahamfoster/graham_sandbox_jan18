/*
 *  UserTriggerHandler
 *  
 *      Trigger Handler class that implements Trigger Framework.
 *          https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *          
 *      According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *      The Trigger Handler class should extend TriggerHandler class.
 * 
 *  Created by Daniel Lachcik 2017-06-28
 *
 *  [Modification history]
 *  [Name] [Date] Description
 *
 */
public with sharing class UserTriggerHandler extends TriggerHandler {
    
    public UserTriggerHandler() {

    }
    
    // **************************************************************************
    //      context overrides 
    // **************************************************************************
    override protected void afterUpdate() {
      CPQChatterGroupAddition();
    }
    
    override protected void afterInsert(){
      CPQChatterGroupAddition();
    }
    
    override protected void afterDelete(){        

    }
        
    override protected void beforeInsert(){        

    }    
    override protected void beforeUpdate(){        

    }

private void CPQChatterGroupAddition()
{
    List<User> UserToUpdate = new list<User>();
    List<User> UserToInactivate = new list<User>();
    
for( User newUser : (List<User>)Trigger.new){
   User oldUser = NULL;

   //If update, grab old record for comparison
   if(Trigger.IsUpdate) 
   { 
      oldUser = ((Map<Id, User>)Trigger.oldMap).get(newUser.ID);  
   }
   
   //Create Add User List
   if((Trigger.IsInsert && newUser.Approval_Category__c != NULL) || //User is Activated w Approval Category set
      (Trigger.IsUpdate && oldUser.Approval_Category__c == NULL 
      	&& newUser.Approval_Category__c != NULL && newUser.Isactive == TRUE)) //User's Approval Category is SET, User still Active       
   {    
     UserToUpdate.add(newUser);
   }
	
   //Create Remove User List 
   if((Trigger.IsUpdate && oldUser.Approval_Category__c != newUser.Approval_Category__c 
       && newUser.Approval_Category__c == NULL && newUser.Isactive == TRUE) || //User's Approval Category is REMOVED, User still Active
      (Trigger.IsUpdate && newUser.Approval_Category__c != NULL && newUser.Isactive == FALSE)) //User is Inactivated w Approval Category set 
   {
    UserToInactivate.add(newUser);   
   }
}  

if(UserToUpdate.size()>0){ //Process list of new CPQ users (users added to CPQ)   
    List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();
    Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
   
    // NEED Validation that Approval Category change doesn't re-create duplicate record

    for(User u: UserToUpdate){
        if(u.isActive == True && u.Approval_Category__c != NULL){  
            cgm.add(new CollaborationGroupMember (CollaborationGroupId = cgID, MemberId = u.Id));   
        } 
    
        if (cgm.size() > 0){
            //add user to CPQ Chatter group
            insert cgm;
        }  
      }
    }  

if(UserToInactivate.size()>0){ //Process list of Un-CPQ users (users removed from CPQ)
    List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();
    Id cgID = [Select Id FROM CollaborationGroup WHERE Name = 'Salesforce CPQ Users' LIMIT 1 ].ID;
    
    for(User u: UserToInactivate){
    	List<CollaborationGroupMember> MemberToDelete = [Select Id from CollaborationGroupMember 
                                                             WHERE CollaborationGroupId =: cgID 
                                                             AND MemberId =: U.Id ];            
    if (MemberToDelete.size() > 0){
        //remove user from CPQ Chatter Group
            delete MemberToDelete;
           }  
    }
   }    
}    
    
    
}