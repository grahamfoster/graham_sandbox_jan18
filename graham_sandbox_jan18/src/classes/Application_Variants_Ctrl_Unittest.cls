@isTest
public with sharing class Application_Variants_Ctrl_Unittest {
	Static TestMethod void Application_Unittest1()
	{
		Application__c appRecObj = Util.createApplication();
		Application_Variants__c appVarRecObj = Util.createAppVariants(appRecObj.Id);
		ApexPages.currentPage().getParameters().put('appCat',appRecObj.Id);
		ApexPages.StandardController controller=new ApexPages.StandardController(appVarRecObj);
		Application_Variants_Ctrl appVarClass = new Application_Variants_Ctrl(controller); 
		appVarClass.doSaveAndNew();
		appVarClass.doCancel();
		appVarClass.doSave();
		appVarClass.doDelete();
	}
	Static TestMethod void Application_Unittest2()
	{
		Application__c appRecObj = Util.createApplication();
		Application_Variants__c appVarRecObj = Util.createAppVariants(appRecObj.Id);
		ApexPages.currentPage().getParameters().put('appCat',appRecObj.Id);
		ApexPages.StandardController controller=new ApexPages.StandardController(appVarRecObj);
		Application_Variants_Ctrl appVarClass = new Application_Variants_Ctrl(controller); 
		 
		appVarClass.doSaveAndNew();
		ApexPages.currentPage().getParameters().put('retURL',appRecObj.id);
		appVarClass.doCancel();
		appVarClass.AppVariantRecord = null;
		appVarClass.doSave();
		appVarClass.doDelete();
	}	
}