//Generated by wsdl2apex

public class QuoteAttachments {
    public class ArrayOffileAttachments {
        public QuoteAttachments.fileAttachments[] ArrayOffileAttachmentsItem;
        private String[] ArrayOffileAttachmentsItem_type_info = new String[]{'ArrayOffileAttachmentsItem','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','fileAttachments','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'ArrayOffileAttachmentsItem'};
    }
    public class individualFileOutputs {
        public String attachmentFileName;
        public String status;
        private String[] attachmentFileName_type_info = new String[]{'attachmentFileName','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] status_type_info = new String[]{'status','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'attachmentFileName','status'};
    }
    public class receiveAttachments {
        public QuoteAttachments.attachmentInputs attachmentInputs;
        private String[] attachmentInputs_type_info = new String[]{'attachmentInputs','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','attachmentInputs','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'attachmentInputs'};
    }
    public class receiveAttachmentsResponse {
        public QuoteAttachments.attachmentOutputs attachmentOutputs;
        private String[] attachmentOutputs_type_info = new String[]{'attachmentOutputs','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','attachmentOutputs','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'attachmentOutputs'};
    }
    public class attachmentOutputs {
        public QuoteAttachments.ArrayOfindividualFileOutputs individualFileOutputs;
        public String outputMessage;
        private String[] individualFileOutputs_type_info = new String[]{'individualFileOutputs','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','ArrayOfindividualFileOutputs','1','1','true'};
        private String[] outputMessage_type_info = new String[]{'outputMessage','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'individualFileOutputs','outputMessage'};
    }
    public class fileAttachments {
        public String fileContent;
        public String attachmentFileName;
        private String[] fileContent_type_info = new String[]{'fileContent','http://www.w3.org/2001/XMLSchema','base64Binary','1','1','true'};
        private String[] attachmentFileName_type_info = new String[]{'attachmentFileName','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'fileContent','attachmentFileName'};
    }
    public class ABI_SalesQuotation_process_receiveAttachmentsWsd_Port {
        //public String endpoint_x = 'https://wmdev.appliedbiosystems.com:443/ws/ABI_SalesQuotation.process:receiveAttachmentsWsd';
        public String endpoint_x = AbConstants.ENDPOINT+'/ws/ABI_SalesQuotation.process:receiveAttachmentsWsd';
        public Map<String,String> inputHttpHeaders_x  = new Map<String, String>();
        public Map<String,String> outputHttpHeaders_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        private String[] ns_map_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd', 'QuoteAttachments'};
        public QuoteAttachments.attachmentOutputs receiveAttachments(QuoteAttachments.attachmentInputs attachmentInputs) {
            QuoteAttachments.receiveAttachments request_x = new QuoteAttachments.receiveAttachments();
            QuoteAttachments.receiveAttachmentsResponse response_x;
            request_x.attachmentInputs = attachmentInputs;
            Map<String, QuoteAttachments.receiveAttachmentsResponse> response_map_x = new Map<String, QuoteAttachments.receiveAttachmentsResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'ABI_SalesQuotation_process_receiveAttachmentsWsd_Binder_receiveAttachments',
              'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd',
              'receiveAttachments',
              'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd',
              'receiveAttachmentsResponse',
              'QuoteAttachments.receiveAttachmentsResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.attachmentOutputs;
        }
    }
    public class ArrayOfindividualFileOutputs {
        public QuoteAttachments.individualFileOutputs[] ArrayOfindividualFileOutputsItem;
        private String[] ArrayOfindividualFileOutputsItem_type_info = new String[]{'ArrayOfindividualFileOutputsItem','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','individualFileOutputs','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'ArrayOfindividualFileOutputsItem'};
    }
    public class attachmentInputs {
        public QuoteAttachments.ArrayOffileAttachments fileAttachments;
        public String CDIQuoteNumber;
        private String[] fileAttachments_type_info = new String[]{'fileAttachments','http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','ArrayOffileAttachments','1','1','true'};
        private String[] CDIQuoteNumber_type_info = new String[]{'CDIQuoteNumber','http://www.w3.org/2001/XMLSchema','string','1','1','true'};
        private String[] apex_schema_type_info = new String[]{'http://asa011g.ab.applera.net/ABI_SalesQuotation/process/receiveAttachmentsWsd','false'};
        private String[] field_order_type_info = new String[]{'fileAttachments','CDIQuoteNumber'};
    }
}