/*********************************************************************************************
 * Name             : SCIEXTEstUtils
 * Created By       : Buan Consulting
 * Created Date     : 11 March 2016
 * Purpose          : Utility class to help on creation of objects for test classes
 * Notes            : 
**********************************************************************************************/
public class SCIEXTestUtils {

    
    //================================================================//
    // Create a new Account (This method will not do any DML operation)
    //================================================================//
    public static Account createAccount(){
        Account account = new Account(Name = 'Test Account');

        account.BillingStreet = '123 test street';
        account.BillingCity = 'Ankeny';
        account.BillingState = 'IA';
        account.BillingPostalCode = '50023';
        account.BillingCountry = 'United States';
        account.ShippingStreet = '123 test street';
        account.ShippingCity = 'Ankeny';
        account.ShippingState = 'IA';
        account.ShippingPostalCode = '50023';
        account.ShippingCountry = 'United States';
        return account;

    }

    
  /*****************************************************************************
    Method Name : createOpportunity  
    Description : Contain actions need to create an Opportunity
  *****************************************************************************/
    public static Opportunity createOpportunity(String name, Account acc){
        Opportunity opportunity = new Opportunity();
        opportunity.Name = name;
        opportunity.CloseDate = Date.today().addDays(5);
        opportunity.AccountId = acc.Id;
        opportunity.StageName = 'Resolution of Concerns';
        opportunity.Explicit_Needs__c = '?';
        opportunity.Implications__c = '?';
        opportunity.Implied_Needs__c = '?';
        opportunity.Need_Payoff_Value_of_Solution__c = '?';
        
        return opportunity;
    
    
    }

  /*****************************************************************************
    Method Name : createSBQQData  
    Description : Contain actions need to create an Steelbrick objects
  *****************************************************************************/
    public static void createSBQQData(Opportunity opp, Account acc){    
    
        Product2[] products = new Product2[0];
        Product2 p1 = new Product2(Name='test1',ProductCode='test1',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p2 = new Product2(Name='test2',ProductCode='test2',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p3 = new Product2(Name='test3',ProductCode='test3',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p4 = new Product2(Name='test4',ProductCode='test4',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p5 = new Product2(Name='test5',ProductCode='test5',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p6 = new Product2(Name='test6',ProductCode='test6',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p7 = new Product2(Name='test7',ProductCode='test7',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');
        Product2 p8 = new Product2(Name='test8',ProductCode='test8',SBQQ__PricingMethod__c='List',SBQQ__SubscriptionPricing__c='Fixed Price');


        products.add(p1);
        products.add(p2);
        products.add(p3);
        products.add(p4);
        products.add(p5);
        products.add(p6);
        products.add(p7);
        products.add(p8);
        insert products;
        
        p2.SBQQ__IncludeInMaintenance__c = true;
        update products;
        
        SBQQ__Quote__c quote1 = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id, SBQQ__StartDate__c = System.Today());
        //quote1.Payment_Frequency__c = 'Quarterly';
        //quote1.Order_Type__c = 'Standard (includes Promos)';
        quote1.SBQQ__StartDate__c = System.today();
        insert quote1;
        
        SBQQ__QuoteLine__c[] lines1 = new SBQQ__QuoteLine__c[0];
        SBQQ__QuoteLine__c line1 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p1.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 0, SBQQ__Quantity__c=1);
        SBQQ__QuoteLine__c line2 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p2.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 100, SBQQ__Quantity__c=10);
        SBQQ__QuoteLine__c line3 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p3.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 0, SBQQ__Quantity__c=1);
        SBQQ__QuoteLine__c line4 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p4.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 10, SBQQ__Quantity__c=1);
        SBQQ__QuoteLine__c line5 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p5.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 10, SBQQ__Quantity__c=1);
        SBQQ__QuoteLine__c line6 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p6.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 10, SBQQ__Quantity__c=1);
        SBQQ__QuoteLine__c line7 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p7.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 10, SBQQ__Quantity__c=1);
        SBQQ__QuoteLine__c line8 = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote1.Id, SBQQ__Product__c=p8.Id ,SBQQ__Incomplete__c=true, SBQQ__ListPrice__c = 10, SBQQ__Quantity__c=1);
        lines1.add(line1);
        lines1.add(line2);
        lines1.add(line3);
        lines1.add(line4);
        lines1.add(line5);
        lines1.add(line6);
        lines1.add(line7);
        lines1.add(line8);
        insert lines1;
        


         //Contract
        Contract contr=new Contract();
        contr.Accountid=acc.id;
        contr.Status='Draft';
        contr.StartDate=system.today().adddays(-1);
        contr.ContractTerm=12;
        insert contr;

        //Subscription
        SBQQ__Subscription__c subscrip=new SBQQ__Subscription__c();
        subscrip.SBQQ__Account__c=acc.id;
        subscrip.SBQQ__Contract__c=contr.id;

        subscrip.SBQQ__QuoteLine__c=lines1.get(0).id;
        subscrip.SBQQ__Product__c=p1.id;
        subscrip.SBQQ__Quantity__c=1;
        insert subscrip;


        lines1[0].SBQQ__Incomplete__c = false;
        lines1[0].SBQQ__RenewedSubscription__c=subscrip.id;
        lines1[0].SBQQ__Discount__c=.01;
        //lines1[0].Database_Size_Label__c='sizelabel';
        //lines1[0].Bundle_Name__c='bundlename';
        lines1[1].SBQQ__Incomplete__c = false;
        lines1[2].SBQQ__Incomplete__c = false;
        lines1[3].SBQQ__Incomplete__c = false;
        lines1[1].SBQQ__OptionLevel__c = 1;
        lines1[2].SBQQ__OptionLevel__c = 1;
        lines1[3].SBQQ__OptionLevel__c = 1;
        lines1[1].SBQQ__ProrateMultiplier__c = 1;
        lines1[2].SBQQ__ProrateMultiplier__c = 1;
        lines1[3].SBQQ__ProrateMultiplier__c = 1;
        lines1[1].SBQQ__DiscountScheduleType__c = 'Slab';
        lines1[1].SBQQ__SubscriptionPricing__c = 'Fixed Price';
        lines1[1].SBQQ__DefaultSubscriptionTerm__c = 12;
        lines1[1].SBQQ__RenewedSubscription__c=subscrip.id;
        //lines1[1].Include_in_Bundle_Price__c=true;


        //lines1[2].Manual_Rollup__c=true;
        //lines1[2].Product_Multiple__c=2;
        //Setting line 0 as the Bundle Parent
        lines1[1].SBQQ__RequiredBy__c = lines1[0].id;
        lines1[2].SBQQ__RequiredBy__c = lines1[0].id;
        lines1[3].SBQQ__RequiredBy__c = lines1[0].id;
        lines1[4].SBQQ__RequiredBy__c = lines1[0].id;
        lines1[5].SBQQ__RequiredBy__c = lines1[0].id;
        lines1[6].SBQQ__RequiredBy__c = lines1[0].id;
        lines1[7].SBQQ__RequiredBy__c = lines1[0].id;
        update lines1;  
    }
}