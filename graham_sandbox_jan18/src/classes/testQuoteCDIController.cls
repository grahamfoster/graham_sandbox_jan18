/*********************************************************************
Name  : testQuoteCDIController
Author: Appirio, Inc.
Date  : July 31, 2008 
Usage : Used to test the QuoteCDIController  

*********************************************************************/

public class testQuoteCDIController {

 
public static testMethod void methodQuoteCDIController () {
  try{  
    Test.setCurrentPageReference(new PageReference('Page.QuoteProductDetail')); 
    Quote__c Quote = new Quote__c(Name = 'QuoteName');
    insert Quote;    
    ApexPages.StandardController sc = new ApexPages.StandardController(Quote);
    Contact cont = [Select Id From Contact Limit 1];
    Opportunity opp = [Select Id From Opportunity Limit 1];
    ApexPages.currentPage().getParameters().put('ContactId',cont.Id);
    ApexPages.currentPage().getParameters().put('id',Quote.Id);
    ApexPages.currentPage().getParameters().put('OpportunityId',opp.Id);
    QuoteCDIController  QExt = new QuoteCDIController(sc);     
    String Str = '';
    Boolean bool = true;
    List<SelectOption> soption = new List<SelectOption>();
    
    QExt.setThisProd('val');
    QExt.setErrorMessage('val');
    Str = QExt.getErrorMessage();
    Str = QExt.getErrorPartners();
    QExt.setErrorPartners('value') ;
    QExt.setOverrideAdd(bool);
    bool =  QExt.getOverrideAdd();
    bool = QExt.getEuropeRegion();
    bool = QExt.getIsFromOpp();
    bool = QExt.getHasTmplt();
    bool = QExt.getLoggedIn();
    Str = QExt.getCdiUsername();
    QExt.setCdiUsername('test');
    Str = QExt.getCdiPassword();
    QExt.setCdiPassword(str);
    PageReference reference = QExt.quoteNewHeader();
    reference = QExt.quoteReturnHeader() ;
    QExt.saveQuote(); 
    bool  = QExt.checkRequiredFields(AbConstants.NA_REGION);
    bool  = QExt.checkRequiredFields('partner');
    bool  = QExt.checkRequiredFields('summary');
    bool  = QExt.checkRequiredFields('options');
    QExt.setDefaultValuesOnNewQuote() ;
    reference = QExt.quoteNewPartner();
    QExt.setSoldToBasedDefaults(Quote, 'soldTo');
    QExt.setPayerBasedDefaults(Quote);
    reference = QExt.saveHeader();
    reference = QExt.QuoteSaveCDI();
    QExt.retrievePartnerDataFromCDI();
    QExt.overrideAddress();
    QExt.showSecondaryAddress();
    QExt.getStatusDisplayValue();
    QExt.getStatusDisplayValueForSummaryPage();
    QExt.getPaymentTermsDisplayValue();
    soption = QExt.getSales_Org_CDI_List();
    soption = QExt.getSales_Org_CDI_List();
    soption = QExt.getShip_to_CDI_List();
    soption = QExt.getPayer_CDI_List();
    soption = QExt.getSAP_Contact_CDI_List();
    Str = QExt.getSAP_Contact_Email();
    soption = QExt.getBillTo_CDI_List();
    soption = QExt.getDestCountry_CDI_List();
    soption = QExt.getSalesRepCDI_List();
    soption = QExt.getSalesOffice_List();
    soption = QExt.getSalesGroup_List();
    reference = QExt.refreshPartnerPicklists();
    reference = QExt.quoteExitPartner();
  
    List<Quoting_SAP_Country_Matrix__c> listQSAP = QExt.findCountryMatrixValuesBasedOnCountry('valueType','country');
    listQSAP = QExt.findCountryMatrixValuesBasedOnCountryAndLang('valueType','country', 'lang');
    listQSAP = QExt.findCountryMatrixValuesBasedOnSalesOrg('valueType','salesOrg');
    listQSAP = QExt.findCountryMatrixValuesBasedOnRegion('valueType');
    listQSAP = QExt.findCountryMatrixValuesBasedOnRegionAndLang( 'valueType','lang');
    soption = QExt.getINCOTerms_List();
    soption = QExt.getPaymentTerms_List();
    bool = QExt.getIsRegionNA();
    bool = QExt.getIsRegionEU();
    bool = QExt.getIsRegionAP();
    soption = QExt.getOutputType_List() ;
    soption = QExt.getOutputMedium_List() ; 
    soption = QExt.getCurrency_List();
    soption = QExt.getLanguage_List() ;
    soption = QExt.getPriceOutput_List();
    Date d = QExt.getQuoteStartDate();
    QExt.setQuoteStartDate(d);
    QExt.QuoteValidDates();
    Str = QExt.getOutputEmail();
    reference = QExt.saveTermsAndConditions() ;
    reference = QExt.reviseQuote();
    reference = QExt.submitQuote();
    reference = QExt.refreshPricingFinal();
    reference = QExt.refreshPricingDraft();
    List<Quote_Line_Item__c> QLI = QExt.getLineItems();
    QLI = QExt.getOptionLineItems();
    QLI = QExt.getMyLineItems();
    List<Attachment>  attachments = QExt.getAttachments();
    reference = QExt.CancelSort(); 
    reference = QExt.quoteSummary();
    reference = QExt.quoteOptionSearch(); 
    reference = QExt.quoteProductSearch();  
    reference = QExt.quoteProductSort(); 
    reference = QExt.quoteOptionSort();
    reference = QExt.productDetail();
    reference = QExt.checkDiscount() ; 
    reference = QExt.quoteProductDiscount();  
    reference = QExt.quoteTemplateSearch(); 
    reference = QExt.quoteLandingPage();
    reference = QExt.returnToProductSelect(); 
    Str = QExt.checkProductDescOverrides();
    reference = QExt.reviewQuote(); 
    reference = QExt.loginToCDI(); 
    reference = QExt.checkMaterials() ;
    reference = QExt.quoteMaterials();
    reference = QExt.removeLineItem();
    reference = QExt.validateProductSort();
    reference = QExt.quoteSaveProducts();
    reference = QExt.addOptionFromOpp();
    reference = QExt.ImportFromQuote();
    QExt.copyOppName();
    reference = QExt.AddProductFromOpp();
    reference = QExt.quoteExitHeader();
    reference = QExt.quoteAttachment();
    reference = QExt.attachmentDetail();
    reference = QExt.refreshPicklists();
    reference = QExt.ApprovalLog(); 
    reference = QExt.SessionLog();
    reference = QExt.refreshAddressInfo();
    reference = QExt.reloadPartners(); 
    QExt.optionFocused();
    QExt.lineItemFocused() ;
    reference = QExt.ValidateTCPage();
    bool =  QExt.getAlreadyRetrievedPartners();
    QExt.setAlreadyRetrievedPartners(bool) ;
    List<Status_Log__c> slog = QExt.getStatusLogs();
    Str = QExt.getLoadingMessage(); 
    QExt.reloadAttachments();
    Quote.Sales_Org__c = null;
    Quote.Dest_Country__c = null;
    Quote.Language__c = null;
    Quote.INCO_Terms__c = null;
    Quote.Currency__c = null;
    Quote.Est_Delivery_Date__c = null;
    Quote.Warranty__c = null;
    Quote.Installation__c = null;
  }
  catch(Exception e){}
} 

public static testMethod void methodQuoteCDIController1() {
  try{
    Test.setCurrentPageReference(new PageReference('Page.QuoteProductDetail')); 
    Quote__c Quote = new Quote__c(Name = 'QuoteName');
    insert Quote;
    ApexPages.StandardController sc = new ApexPages.StandardController(Quote);
    Contact cont = [Select Id From Contact Limit 1];
    Opportunity opp = [Select Id From Opportunity Limit 1];
    ApexPages.currentPage().getParameters().put('ContactId',cont.Id);
    ApexPages.currentPage().getParameters().put('id',Quote.Id);
    ApexPages.currentPage().getParameters().put('OpportunityId',opp.Id);
    QuoteCDIController  QExt = new QuoteCDIController(sc);     
    List<SelectOption> soption = new List<SelectOption>();
    String Str = '';
    Boolean bool = true;
    PageReference reference = QExt.quoteNewHeader();
    reference = QExt.refreshPartnerPicklists();
    reference = QExt.quoteExitPartner();
    List<Quoting_SAP_Country_Matrix__c> listQSAP = QExt.findCountryMatrixValuesBasedOnCountry('valueType','country');
    listQSAP = QExt.findCountryMatrixValuesBasedOnCountryAndLang('valueType','country', 'lang');
    listQSAP = QExt.findCountryMatrixValuesBasedOnSalesOrg('valueType','salesOrg');
    listQSAP = QExt.findCountryMatrixValuesBasedOnRegion('valueType');
    listQSAP = QExt.findCountryMatrixValuesBasedOnRegionAndLang( 'valueType','lang');
    soption = QExt.getINCOTerms_List();
    soption = QExt.getPaymentTerms_List();
    bool = QExt.getIsRegionNA();
    bool = QExt.getIsRegionEU();
    bool = QExt.getIsRegionAP();
    soption = QExt.getOutputType_List() ;
    soption = QExt.getOutputMedium_List() ; 
    soption = QExt.getCurrency_List();
    soption = QExt.getPriceOutput_List();
    Date d = QExt.getQuoteStartDate();
    QExt.setQuoteStartDate(d);
    QExt.QuoteValidDates();
    Str = QExt.getOutputEmail();
    reference = QExt.saveTermsAndConditions() ;
    reference = QExt.reviseQuote();
    reference = QExt.submitQuote();
    reference = QExt.refreshPricingFinal();
    reference = QExt.refreshPricingDraft();
    List<Quote_Line_Item__c> QLI = QExt.getLineItems();
    QLI = QExt.getOptionLineItems();
    QLI = QExt.getMyLineItems();
    List<Attachment>  attachments = QExt.getAttachments();
    reference = QExt.CancelSort(); 
    reference = QExt.quoteSummary();
    reference = QExt.quoteOptionSearch(); 
    reference = QExt.quoteProductSearch();  
    reference = QExt.quoteProductSort(); 
    reference = QExt.quoteOptionSort();
    reference = QExt.productDetail();
    reference = QExt.checkDiscount() ; 
    reference = QExt.quoteProductDiscount();  
    reference = QExt.quoteTemplateSearch(); 
    reference = QExt.quoteLandingPage();
    reference = QExt.returnToProductSelect(); 
    Str = QExt.checkProductDescOverrides();
    reference = QExt.reviewQuote(); 
    reference = QExt.loginToCDI(); 
    reference = QExt.checkMaterials() ;
    reference = QExt.quoteMaterials();
    reference = QExt.removeLineItem();
    reference = QExt.validateProductSort();
    reference = QExt.quoteSaveProducts();
    reference = QExt.addOptionFromOpp();
    reference = QExt.ImportFromQuote();
    QExt.copyOppName();
    reference = QExt.AddProductFromOpp();
    reference = QExt.quoteExitHeader();
    reference = QExt.quoteAttachment();
    reference = QExt.attachmentDetail();
    //reference = QExt.refreshPicklists();
    reference = QExt.ApprovalLog(); 
    reference = QExt.SessionLog();
    reference = QExt.refreshAddressInfo();
    reference = QExt.reloadPartners(); 
    QExt.optionFocused();
    QExt.lineItemFocused() ;
    reference = QExt.ValidateTCPage();
    bool =  QExt.getAlreadyRetrievedPartners();
    QExt.setAlreadyRetrievedPartners(bool) ;
    List<Status_Log__c> slog = QExt.getStatusLogs();
    Str = QExt.getLoadingMessage(); 
    QExt.reloadAttachments();
    Quote.Sales_Org__c = null;
    Quote.Dest_Country__c = null;
    Quote.Language__c = null;
    Quote.INCO_Terms__c = null;
    Quote.Currency__c = null;
    Quote.Est_Delivery_Date__c = null;
    Quote.Warranty__c = null;
    Quote.Installation__c = null;
  } 
  catch(Exception e){}
}

public static testMethod void methodQuoteCDIController2() {
  try{
    Test.setCurrentPageReference(new PageReference('Page.QuoteProductDetail')); 
    Quote__c Quote = new Quote__c(Name = 'QuoteName');
    insert Quote;    
    ApexPages.StandardController sc = new ApexPages.StandardController(Quote);
    Contact cont = [Select Id From Contact Limit 1];
    Opportunity opp = [Select Id From Opportunity Limit 1];
    ApexPages.currentPage().getParameters().put('ContactId',cont.Id);
    ApexPages.currentPage().getParameters().put('id',Quote.Id);
    ApexPages.currentPage().getParameters().put('OpportunityId',opp.Id);
    QuoteCDIController  QExt = new QuoteCDIController(sc);     
    String Str = '';
    Boolean bool = true;
    List<SelectOption> soption = new List<SelectOption>();
    PageReference reference = QExt.quoteNewHeader();
    reference = QExt.loginToCDI(); 
    reference = QExt.checkMaterials() ;
    reference = QExt.quoteMaterials();
    reference = QExt.removeLineItem();
    reference = QExt.validateProductSort();
    reference = QExt.quoteSaveProducts();
    reference = QExt.addOptionFromOpp();
    reference = QExt.ImportFromQuote();
    QExt.copyOppName();
    reference = QExt.AddProductFromOpp();
    reference = QExt.quoteExitHeader();
    reference = QExt.quoteAttachment();
    reference = QExt.attachmentDetail();
    reference = QExt.ApprovalLog(); 
    reference = QExt.SessionLog();
    reference = QExt.refreshAddressInfo();
    reference = QExt.reloadPartners(); 
    QExt.optionFocused();
    QExt.lineItemFocused() ;
    reference = QExt.ValidateTCPage();
    bool =  QExt.getAlreadyRetrievedPartners();
    QExt.setAlreadyRetrievedPartners(bool) ;
    List<Status_Log__c> slog = QExt.getStatusLogs();
    Str = QExt.getLoadingMessage(); 
    QExt.reloadAttachments();
    Quote.Sales_Org__c = null;
    Quote.Dest_Country__c = null;
    Quote.Language__c = null;
    Quote.INCO_Terms__c = null;
    Quote.Currency__c = null;
    Quote.Est_Delivery_Date__c = null;
    Quote.Warranty__c = null;
    Quote.Installation__c = null;
  }
  catch(Exception e){}
}

public static testMethod void methodQuoteCDIController3() {
  try{
    Test.setCurrentPageReference(new PageReference('Page.QuoteProductDetail')); 
    Quote__c Quote = new Quote__c(Name = 'QuoteName');
    insert Quote;    
    ApexPages.StandardController sc = new ApexPages.StandardController(Quote);
    Contact cont = [Select Id From Contact Limit 1];
    Opportunity opp = [Select Id From Opportunity Limit 1];
    ApexPages.currentPage().getParameters().put('ContactId',cont.Id);
    ApexPages.currentPage().getParameters().put('id',Quote.Id);
    ApexPages.currentPage().getParameters().put('OpportunityId',opp.Id);
    QuoteCDIController  QExt = new QuoteCDIController(sc);     
    String Str = '';
    Boolean bool = true;
    List<SelectOption> soption = new List<SelectOption>();
    PageReference reference = QExt.quoteNewHeader();
    reference = QExt.reloadPartners(); 
    QExt.optionFocused();
    QExt.lineItemFocused() ;
    reference = QExt.ValidateTCPage();
    bool =  QExt.getAlreadyRetrievedPartners();
    QExt.setAlreadyRetrievedPartners(bool) ;
    List<Status_Log__c> slog = QExt.getStatusLogs();
    Str = QExt.getLoadingMessage(); 
    
    QExt.reloadAttachments();
    Quote.Sales_Org__c = null;
    Quote.Dest_Country__c = null;
    Quote.Language__c = null;
    Quote.INCO_Terms__c = null;
    Quote.Currency__c = null;
    Quote.Est_Delivery_Date__c = null;
    Quote.Warranty__c = null;
    Quote.Installation__c = null;
  }
  catch(Exception e){}
}
}