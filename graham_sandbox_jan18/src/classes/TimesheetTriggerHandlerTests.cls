/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Oct 24/2013
 **
 ** Test coverage for the Timesheet trigger
**/
@isTest
public class TimesheetTriggerHandlerTests{
    public static testMethod void test_weekstartvalidation(){
        Date d = Date.today();
        //change to a non-Monday
        if(DateTime.newInstance(d.year(), d.month(), d.day()).format('E').toLowerCase() == 'mon') {
            d = d.addDays(-1);
        }

        try {
    		Timesheet__c ts = new Timesheet__c(Name = 'a', Week_Start__c = Date.today());
            insert ts;
            //system.assertEquals(false, true);
        }catch(DmlException e){
        }
    }

    public static testMethod void test_nameset(){
        Date d = Date.today();
        //change to a Monday
        while(DateTime.newInstance(d.year(), d.month(), d.day()).format('E').toLowerCase() != 'mon') {
            d = d.addDays(-1);
        }

        Timesheet__c ts = new Timesheet__c(Name = 'a', Week_Start__c = d);
        insert ts;
        
        ts = [select Name from Timesheet__c where Id = :ts.Id];
        
        String usersName = UserInfo.getName();
        
        String expectedName = usersName + ' - ' + d.month() + '/' + d.day() + '/' + d.year();
        system.assertEquals(expectedName, ts.Name);
    }
}