public class InstallBase_Rollup {
    
    public InstallBase_Rollup(Map<Id, SVMXC__Installed_Product__c> initialQuery){
        Map<Id, Map <Id,SVMXC__Service_Contract_Products__c>> mapCPbyIB = new Map<Id, Map <Id,SVMXC__Service_Contract_Products__c>>();
        List<SVMXC__Service_Contract_Products__c> contractProduct = new List<SVMXC__Service_Contract_Products__c>([
            	SELECT 
            		Id, SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMXC__Installed_Product__c,Document_Number__c,Current_Item_Master__c,SVC_Coverage_Description__c,svc_contract_applicability__c, SVC_Contract_Subline_Status__c
            	FROM 
            		SVMXC__Service_Contract_Products__c 
            	WHERE 
            		
            		SVMXC__Installed_Product__c IN: initialQuery.values()
            	ORDER BY SVMXC__End_Date__c DESC NULLS LAST ]);
system.debug('!!! - initialQuery ' +initialQuery);
system.debug('!!! - contractProduct ' +contractProduct);
        
        // Create a map of Covered Products, grouped by Install Base, to make it easy to loop through multiple coverages by instrument
        for (SVMXC__Service_Contract_Products__c i :contractProduct){
            if(!mapCPbyIB.containsKey(i.SVMXC__Installed_Product__c)){
                mapCPbyIB.put(i.SVMXC__Installed_Product__c, new Map <Id,SVMXC__Service_Contract_Products__c>{i.id => i} );
            } else {
                mapCPbyIB.get(i.SVMXC__Installed_Product__c).put(i.id,i);
            }
        } 
        // Loop through each Instrument
        FOR(Id ip :mapCPbyIB.keySet()){
            //null out existing field values
            initialQuery.get(ip).Current_Coverage_Start_Date__c = date.valueof('2199-01-01');
            initialQuery.get(ip).Current_Coverage_End_Date__c =  date.valueof('1199-01-01');
            initialQuery.get(ip).Current_Item_Master__c = '';
            initialQuery.get(ip).Current_Coverage_Description__c = '';
            initialQuery.get(ip).Document_Number__c = '';
            initialQuery.get(ip).Seconds_Line_Contract__c = '';
           
        	FOR(Id cp :mapCPbyIB.get(ip).keySet()){
				if(mapCPbyIB.get(ip).get(cp).svc_contract_applicability__c == 'Current' && 
                   mapCPbyIB.get(ip).get(cp).SVC_Contract_Subline_Status__c == 'ACTIVE' ){
system.debug('!!! - did this run? a' );                                              
               		//  No Filter Rollups
					//  Start Date Rollup
                	if(mapCPbyIB.get(ip).get(cp).SVMXC__Start_Date__c < initialQuery.get(ip).Current_Coverage_Start_Date__c){
                    	initialQuery.get(ip).Current_Coverage_Start_Date__c = mapCPbyIB.get(ip).get(cp).SVMXC__Start_Date__c;
                	}
					//  End Date Rollup
                	if(mapCPbyIB.get(ip).get(cp).SVMXC__End_Date__c > initialQuery.get(ip).Current_Coverage_End_Date__c){
	                    initialQuery.get(ip).Current_Coverage_End_Date__c = mapCPbyIB.get(ip).get(cp).SVMXC__End_Date__c;
	                }
					//  != 'Response 24' Rollups
	                if(mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Response 24'){
	                    // Document Number Rollup
    	                initialQuery.get(ip).Document_Number__c += mapCPbyIB.get(ip).get(cp).Document_Number__c + ' ';
	                    if(initialQuery.get(ip).Document_Number__c.length() > 154)initialQuery.get(ip).Document_Number__c = initialQuery.get(ip).Document_Number__c.left(155);
	                
		                // Current Coverage Description Rollup
        	            initialQuery.get(ip).Current_Coverage_Description__c += mapCPbyIB.get(ip).get(cp).SVC_Coverage_Description__c + ' ';
						if(initialQuery.get(ip).Current_Coverage_Description__c.length() > 154) initialQuery.get(ip).Current_Coverage_Description__c = initialQuery.get(ip).Current_Coverage_Description__c.left(155);

                	}			    
					//  NOT IN	('Response 24' , 'Response48' , 'Recertification/PM' , 'Qualification' ) Rollups
					if(mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Response 24' 
                		&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Response 24' 
                    	&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Response 48' 
                    	&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Recertification/PM' 
                    	&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Qualification' 
                    	&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Additional PQ' 
                    	&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Additional PM' 
                    	&& mapCPbyIB.get(ip).get(cp).current_item_master__c != 'Additional OQ' ){
                		// Current Item Master Rollup
                		initialQuery.get(ip).Current_Item_Master__c  += mapCPbyIB.get(ip).get(cp).Current_Item_Master__c + ' ';
						if(initialQuery.get(ip).Current_Item_Master__c.length() > 154)  initialQuery.get(ip).Current_Item_Master__c = initialQuery.get(ip).Current_Item_Master__c.left(155);
                       
	                }
    	            //  IN	('Response 24' , 'Response48' , 'Recertification/PM' , 'Qualification' ) Rollups
	                if(mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Response 24' 
    	            	|| mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Response 24' 
        	            || mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Response 48' 
            	        || mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Recertification/PM' 
                	    || mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Qualification' 
                    	|| mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Additional PQ' 
                    	|| mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Additional PM' 
                    	|| mapCPbyIB.get(ip).get(cp).current_item_master__c == 'Additional OQ' ){
                		//  Second Line Contract Rollup
                		initialQuery.get(ip).Seconds_Line_Contract__c += mapCPbyIB.get(ip).get(cp).Current_Item_Master__c + ' ';
						if(initialQuery.get(ip).Seconds_Line_Contract__c.length() > 154) initialQuery.get(ip).Seconds_Line_Contract__c = initialQuery.get(ip).Seconds_Line_Contract__c.left(155);
                       
                	}
				}	
            }
			
			if(initialQuery.get(ip).Current_Coverage_Start_Date__c == date.valueof('2199-01-01')) initialQuery.get(ip).Current_Coverage_Start_Date__c = NULL;
            if(initialQuery.get(ip).Current_Coverage_End_Date__c ==  date.valueof('1199-01-01')) initialQuery.get(ip).Current_Coverage_End_Date__c =  NULL;			
			
        	update initialQuery.values();   
        }
    }
}
/*
 * Code for invoking via Annonymus execution
 *  
Map<Id, SVMXC__Installed_Product__c> initialQuery = new Map<Id, SVMXC__Installed_Product__c>([SELECT id, Current_Coverage_Start_Date__c, Current_Coverage_End_Date__c
    FROM 
        SVMXC__Installed_Product__c   
ORDER BY
        ID ASC LIMIT 100]);

  new InstallBase_Rollup(initialQuery);
*/