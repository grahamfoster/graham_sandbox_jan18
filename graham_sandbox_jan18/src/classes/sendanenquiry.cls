public with sharing class sendanenquiry {

   public string packId {get; set;}
   public string enquiry {get; set;}
   public Package__c pck {get; set;}
   
   public sendanenquiry(){
       packId = ApexPages.currentPage().getParameters().get('pckId');
       if (packId.equals(''))
       {
       	pck = new Package__c();
       }
       else
       {
       	List<Package__c> pckList = [Select Id, Name, Package_Name__c, product__r.Name, Competitor_Product__r.Name, Application_Variants__r.Application_Variant_Name__c, Persona__r.Persona_Name__c From Package__c Where isActive__c =: true ANd Id =: packId];
        if(pckList.size()>0){
           pck = pckList[0];
        }
       }
       system.debug('pck>>>>'+pck);
   } 
   
   public void sendenquiry(){
       List<String> emailIds = new List<String>();
       emailIds.add('Competitive.Intelligence@absciex.com');
	   emailIds.add('s.reynolds-09@alumni.lboro.ac.uk');
       //emailIds.add('ws@softconsultgroup.com');
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       mail.setToAddresses(emailIds);
       mail.setTargetObjectId(UserInfo.getUserId());
       mail.setHtmlBody('Package: ' + pck.Package_Name__c +' <br/> AB Sciex Product: ' + pck.product__r.Name + 
                         '<br/> Competitor Product: ' + pck.Competitor_Product__r.Name + '<br/> Application Name: ' + 
                         pck.Application_Variants__r.Application_Variant_Name__c + '<br/> Persona: ' + pck.Persona__r.Persona_Name__c
                          + '<br/>Enquiry: ' + enquiry);
       mail.setPlainTextBody(enquiry);
       mail.setSubject('Competitive Intelligence Portal Enquiry');
       system.debug('mail>>>>'+mail + enquiry);
       mail.setSaveAsActivity(false);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }
   
}