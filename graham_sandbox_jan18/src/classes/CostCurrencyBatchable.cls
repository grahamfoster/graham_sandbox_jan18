/**
** @author Reid Beckett, Cloudware Connections
** @created Dec 28/2014
**
** Batchable job for upserting Cost records
** 
**/
global class CostCurrencyBatchable implements Database.Batchable<sObject> {
	global integer recordLimit;
	global DateTime notUpdatedSince;

	global CostCurrencyBatchable() {
	}

	global CostCurrencyBatchable(integer recordLimit, DateTime notUpdatedSince) {
		this.recordLimit = recordLimit;
		this.notUpdatedSince = notUpdatedSince;
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		String soql = 'select Id, ProductCode,'+
            '(select Id, CurrencyIsoCode, Pricebook2Id, Product2Id from PricebookEntries where Pricebook2.IsStandard = true), '+
            '(select Id, CurrencyIsoCode, SBQQ__UnitCost__c, Oracle_Item_Number__c from SBQQ__Costs__r) '+
            'from Product2 where IsActive = true ';
        if(Test.isRunningTest()) soql += 'and ProductCode like \'test-%\'';
        else soql += 'and ProductCode != null';
        if(this.notUpdatedSince != null) {
        	DateTime ts = this.notUpdatedSince;
        	soql += ' and (Costs_Last_Updated__c = NULL or Costs_Last_Updated__c < :ts)';
		}
        if(recordLimit != null) soql += ' limit ' + recordLimit;
        return Database.getQueryLocator(soql);
	}

	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		CostCurrencyHelper h = new CostCurrencyHelper((List<Product2>)scope);
		h.process();
	}

	global void finish(Database.BatchableContext BC) {
	}
}