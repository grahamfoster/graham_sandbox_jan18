/**
 * @author Brett Moore
 * @created - July 2016
 * @Revision  
 * @Last Revision 
 * 
 * Class to route created Opportunities to the correct Service Sales Rep
 * 
 *  [ Legacy ] - to be updated
 * 
**/
public class Contract2Opportunity_Assign {

    public List<Opportunity> updateOpps;
    
    public Contract2Opportunity_Assign(Map<Id, Opportunity> initialQuery) { 
        
                this.updateOpps = new List<Opportunity>();
        List<Id> AccountList = new List<ID>();
        for(Opportunity opp :initialQuery.values()){                          
            AccountList.add(opp.accountid);
        } 
		Map<Id,ObjectTerritory2Association> OT2AList = new Map<Id,ObjectTerritory2Association>([SELECT Id, ObjectId, Territory2Id FROM ObjectTerritory2Association WHERE ObjectId IN :AccountList]);    
        List<ID> neededT = new List<ID>();
        for(ObjectTerritory2Association ct :OT2AList.values()){
            neededT.add(ct.Territory2Id);
        }
        Map<Id,Territory2> TerList = new Map<ID,Territory2>([SELECT id, Name,Default_Service_Sales_Rep__C, Default_CE_Service_Sales_Rep__C FROM Territory2 where id IN :neededT ]);
        Map<ID,Territory2> OT2A = new Map<ID,Territory2>();
        For (ObjectTerritory2Association x :OT2AList.values()){
            OT2A.put(x.ObjectID, TerList.get(x.Territory2Id));            
        }
		for(Opportunity opp :initialQuery.values()){    
            if(opp.c2o_CE_Opportunity__c){
                if(string.isNotBlank(OT2A.get(opp.accountid).Default_CE_Service_Sales_Rep__C)){
	                opp.OwnerId = OT2A.get(opp.accountid).Default_CE_Service_Sales_Rep__C;
                    opp.c2o_AutoGeneration_Status__c='Assigned';
                    updateOpps.add(opp);
                } else {
system.debug('!!! - Default CE Rep BLANK!');                                        
                }
            } else {
                if(string.isNotBlank(OT2A.get(opp.accountid).Default_Service_Sales_Rep__C)){
                	opp.OwnerId = OT2A.get(opp.accountid).Default_Service_Sales_Rep__C;
                    opp.c2o_AutoGeneration_Status__c='Assigned';
                    updateOpps.add(opp);
                } else {
system.debug('!!! - Default Rep BLANK!');                                        
                } 
            }
        } 
        if(updateOpps.size()>0){
            update updateOpps;
        }       
    }
}
/*
 * Code for invoking via Annonymus execution
 * 
   Map<Id, Opportunity> initialQuery = new Map<Id, Opportunity>([	SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,
																			c2o_CE_Opportunity__c, accountid
  																		FROM 
																			Opportunity 
																		WHERE 
																			c2o_AutoGeneration_Status__c = 'Populated' 
																		]);

   new Contract2Opportunity_Routing(initialQuery);
 *
 */
/*  OR
 * 
 * 
        String Query = 'SELECT id, CurrencyISOCode, pricebook2id, c2o_AutoGeneration_Status__c,c2o_CE_Opportunity__c, accountid	FROM Opportunity WHERE c2o_AutoGeneration_Status__c = \'Populated\'';
        Integer scope = 200;
        String process = 'Assign';
        
        Contract2Opportunity_Batch b = new Contract2Opportunity_Batch(Query,process);
		id batchInstanceId = database.executebatch(b,scope);
*/