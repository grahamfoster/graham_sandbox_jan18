/**
** @author Reid Beckett, Cloudware Connections
** @created Aug 27/2015
**
** When a Parts Order record (managed custom object) is created or edited, if the record type = “RMA” and the order status = “Submitted” then set the line status of all line items to “Submitted”
** 
**/
public class PartsOrderTriggerHandler extends BaseTriggerHandler 
{
    public void onAfter()
    {
        boolean isInformaticaRobot = UserInfo.getUserName().indexOf('informatica.robot') >= 0;
        if(!isInformaticaRobot)
        {
            Set<Id> partsOrderIds = new Set<Id>();
            RecordType rmaRT = SfdcUtil.getRecordType('SVMXC__RMA_Shipment_Order__c', 'RMA');
            if(rmaRT == null) return;
            for(SVMXC__RMA_Shipment_Order__c partsOrder : (List<SVMXC__RMA_Shipment_Order__c>)Trigger.new)
            {
                if(partsOrder.RecordTypeId == rmaRT.Id && partsOrder.SVMXC__Order_Status__c == 'Submitted')
                {
                    partsOrderIds.add(partsOrder.Id);
                }
            }
            
            SVMXC__RMA_Shipment_Line__c[] partsOrderLines = [select Id, SVMXC__Line_Status__c 
                                                             from SVMXC__RMA_Shipment_Line__c where 
                                                              SVMXC__Line_Status__c != 'Submitted' AND SVMXC__Line_Status__c != 'Closed' AND  SVMXC__Line_Status__c != 'Void' 
                                                               and SVMXC__RMA_Shipment_Order__c in :partsOrderIds];
            for(SVMXC__RMA_Shipment_Line__c poLine : partsOrderLines)
            {
                poLine.SVMXC__Line_Status__c = 'Submitted';
            }
            update partsOrderLines;
        }
    }
}