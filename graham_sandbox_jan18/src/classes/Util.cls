public with sharing class Util {

public static User createUser()
    {
      Map<String,ID> profiles = new Map<String,ID>();
      List<Profile> ps = [select id, name from Profile where name = 'System Administrator' or name = 'Standard User'];
      for(Profile p : ps)
      {
         profiles.put(p.name, p.id);
      }
      User standard = new User(alias = 'standard',email='standarduser1@testorg.com',emailencodingkey='UTF-8',lastname='Testing', languagelocalekey='en_US',
      localesidkey='en_US',
      profileId = profiles.get('Standard User'),
      timezonesidkey='America/Los_Angeles',
      username='standarduserTesting@testorg.com');
      insert standard;     
      return standard;
    }
    
    public static Manufacturer__c createManufacturer()
    {
        Manufacturer__c manuObj = new Manufacturer__c();
        manuObj.Name = 'test Manufacturer';
        manuObj.Manufacturer_Name__c = 'test Manufacturer';
        manuObj.isActive__c =  true;
        
        insert manuObj;
        return manuObj;
    }
    
     public static Manufacturer__c createManufacturerABX()
    {
        Manufacturer__c manuABXObj = new Manufacturer__c();
        manuABXObj.Name = 'AB SCIEX';
        manuABXObj.Manufacturer_Name__c = 'AB SCIEX';
        manuABXObj.isActive__c =  true;     
    
        insert manuABXObj;
        return manuABXObj;
    }
     
      public static Product2 createProduct2(String manufacturer , String ProductCategory)
    {
        Product2 prodObj = new Product2();
        prodObj.Name = 'test Prodduct';
        prodObj.Description  = 'test Prodduct Description';
        prodObj.Manufacturer__c = manufacturer;
        prodObj.Model__c = 'Model : 001';
        prodObj.Product_Category__c = ProductCategory;
        prodObj.IsActive = true;
                
        insert prodObj;
        return prodObj;
    } 
    
    public static Product_Category__c CreateProductCategory(){
        Product_Category__c prodCatObj = new Product_Category__c();
        prodCatObj.isActive__c = true;
        prodCatObj.Product_Category_Name__c = 'test product cat';
        
        insert prodCatObj;
        return prodCatObj;
    }
    
    public static Specifications_Meta__c CreateProductfeatureCategory(String ProductCategory){
        Specifications_Meta__c prodfeatureCatObj = new Specifications_Meta__c();
        prodfeatureCatObj.isActive__c = true;
        prodfeatureCatObj.Specifications_Meta_Name__c = 'test product feature cat';
        prodfeatureCatObj.Title__c = 'test Title on Prod feature cat';
        prodfeatureCatObj.Product_Category__c = ProductCategory;
                
        insert prodfeatureCatObj;
        return prodfeatureCatObj;
    }
    
    public static Product_Meta__c CreateProductfeature(String ProductfeatureCategory){
        Product_Meta__c prodfeatureObj = new Product_Meta__c();
        prodfeatureObj.isActive__c = true;
        prodfeatureObj.Product_Meta_Name__c = 'test product feature';
        prodfeatureObj.Specifications_Meta__c = ProductfeatureCategory;
                
        insert prodfeatureObj;
        return prodfeatureObj;
    }
    
    public static Package__c CreatePackage(String Application,String Applicationvar,String Persona,String Product,String Comroduct){
        Package__c packageObj = new Package__c();
        packageObj.isActive__c = true;
        packageObj.Application__c = Application;
        packageObj.Application_Variants__c = Applicationvar;
        packageObj.persona__c = persona;
        packageObj.Competitor_Product__c = Comroduct;
        packageObj.Product__c = Product;
        packageObj.Freeform_Text__c = 'test Freeform_Text__c ';
        packageObj.Freeform_Text__c = 'test data';
        packageObj.Package_Name__c = 'test Package';
        packageObj.Rating__c = 0;
                
        insert packageObj;
        return packageObj;
    }
     /*
     public static Feedback__c CreateFeedback(String BattleCard){
        Feedback__c featureObj = new Feedback__c();
        featureObj.isActive__c = true;
        featureObj.Battlecard__c = BattleCard;
        featureObj.Comments__c = 'test Comment';
        featureObj.CP__c = 2;
        featureObj.Demo__c = 3; 
        featureObj.PP__c = 3;
        featureObj.Res__c = 4; 
        featureObj.Sum__c = '5';
        featureObj.Feedback_Name__c = 'test feedbackName'; 
                
        insert featureObj;
        return featureObj;
    }
    */
    
     public static Product_Value__c CreateProductValue(String Productfeature){
        Product_Value__c prodValueObj = new Product_Value__c();
        prodValueObj.isActive__c = true;
        prodValueObj.Value__c = 'test product feature';
        prodValueObj.Product_Value_Name__c = 'Product Test value';
        prodValueObj.Product_Meta__c = Productfeature;
                
        insert prodValueObj;
        return prodValueObj;
    }
    public static Application__c createApplication()
    {
        Application__c appObj = new Application__c();
        appObj.Application_Name__c = 'Test App- 01';
        appObj.Color__c = 'E5E1FF';
        appObj.isActive__c = true;
        appObj.Image__c = '/servlet/servlet.FileDownload?file=00Pb0000000cfPQEAY';
        insert appObj;
        return appObj;
    }
    
    public static Persona__c createPersona()
    {
        Persona__c personaObj = new Persona__c();
        personaObj.Persona_Name__c = 'lab Technician';
        personaObj.isActive__c = true;
        insert personaObj;
        return personaObj;
    }
    
    public static Application_Variants__c createAppVariants(String applicationId)
    {
        Application_Variants__c appVariantsObj = new Application_Variants__c();
        appVariantsObj.Application_Variant_Name__c = 'Drug Metabolism';
        appVariantsObj.isActive__c = true;
        appVariantsObj.Application__c = applicationId;      
        insert appVariantsObj;
        return appVariantsObj;
    }
    
    public static Battlecard__c createBattlecard(String packageId)
    {
        Battlecard__c battlecardObj = new Battlecard__c();
        battlecardObj.isActive__c = true;
        battlecardObj.Package__c = packageId;
        battlecardObj.Battlecard_Name__c = 'New Battlecard - 01';
        insert battlecardObj;
        return battlecardObj;
    }
    
    public static Summary__c createSummary(String battlecardId)
    {
        Summary__c summaryObj = new Summary__c();
        summaryObj.isActive__c = true;
        summaryObj.Battlecard__c = battlecardId;
        summaryObj.Summary__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        insert summaryObj;
        return summaryObj;
    }
    
    public static Demo_Strategy__c createDemoStrategy(String battlecardId)
    {
        Demo_Strategy__c DemoStrategyObj = new Demo_Strategy__c();
        DemoStrategyObj.isActive__c = true;
        DemoStrategyObj.Battlecard__c = battlecardId;
        DemoStrategyObj.Product_Features__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        DemoStrategyObj.What_to_avoid__c =  'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        DemoStrategyObj.What_to_push_at_demo__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
                            
        insert DemoStrategyObj;
        return DemoStrategyObj;
    }
    
    public static Positioning__c createPositioning(String battlecardId)
    {
        Positioning__c positioningObj = new Positioning__c();
        positioningObj.isActive__c = true;
        positioningObj.Battlecard__c = battlecardId;
        positioningObj.Customer_s_Need_Long__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        positioningObj.Addressing_Need__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        positioningObj.Competitor_s_Need_Short__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        positioningObj.Key_Features__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        positioningObj.Respond_With__c =    'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
        
        positioningObj.Competitor_Say__c = 'Lorem ipsum dolor sit amet, consectetur' 
                                +'adipiscing elit. Proin volutpat, magna ut'
                                +'malesuada lobortis, urna tellus malesuada'
                                +'massa, et mattis mi diam pellentesque libero.'
                                +' Vivamus venenatis risus suscipit, porttitor justo vitae, dictum massa.';
                            
        insert positioningObj;
        return positioningObj;
    }
    
     public static Resource_Category__c createResCategory(String battlecardId)
    {
        Resource_Category__c resCategoryObj = new Resource_Category__c();
        resCategoryObj.isActive__c = true;
        resCategoryObj.Battlecard__c = battlecardId;
        resCategoryObj.Resource_Category_Name__c = 'res Category - 01';
        insert resCategoryObj;
        return resCategoryObj;
    } 
    
     public static Additional_Resource__c createAddresource(String battlecardId, String ResourceCategoryId)
    {
        Additional_Resource__c AddresourceObj = new Additional_Resource__c();
        AddresourceObj.isActive__c = true;
        AddresourceObj.Battlecard__c = battlecardId;
        AddresourceObj.Resource_Category__c = ResourceCategoryId;
        AddresourceObj.Additional_Resource_Name__c = 'Testing';
        AddresourceObj.Data__c = 'www.google.com';
        AddresourceObj.Type__c = 'Hyperlink';
        AddresourceObj.Description__c = 'This is test';     
        insert AddresourceObj;
        return AddresourceObj;
    }
    
    public static Rating_Management__c createRatingManagement(String PackageId, String UserID)
    {
        Rating_Management__c ratingManagementObj = new Rating_Management__c();
        ratingManagementObj.isActive__c = true;
        ratingManagementObj.Package__c = PackageId;
        ratingManagementObj.User__c = UserID;
        ratingManagementObj.Rating_Management_Name__c = 'Testing';
        ratingManagementObj.Additional_Resource_Rating__c = 1.2;
        ratingManagementObj.CP__c = 1;
        ratingManagementObj.PP__c = 2; 
        ratingManagementObj.Demo__c = 3;
                
        insert ratingManagementObj;
        return ratingManagementObj;
    }
    
    public static Add_to_Favorites_Junction__c createAddtoFavjuntion(String PackageId, String UserID)
    {
        Add_to_Favorites_Junction__c addToFavJunctionObj = new Add_to_Favorites_Junction__c();
        addToFavJunctionObj.isActive__c = true;
        addToFavJunctionObj.Package__c = PackageId;
        addToFavJunctionObj.User__c = UserID;
        addToFavJunctionObj.Add_to_Favorites_Name__c = 'Testing - 01';      
                
        insert addToFavJunctionObj;
        return addToFavJunctionObj;
    }
    
     
}