/**
 ** @author Reid Beckett, Cloudware Connections
 ** @created Jan. 29/2013
 **
 ** Called by CampaignMemberTrigger trigger after delete, after insert
 ** When a Lead is associated (insert) or unassociated (delete) from a Campaign, this trigger syncs the Strategy ROI record
**/
public class CampaignMemberTriggerHandler {
    public CampaignMember[] campaignMembers {get;set;}
    public Map<Id, Lead> leadMap {get;set;}
    public Map<Id, Campaign> campaignMap {get;set;}
    
    /**
     ** Constructor that takes in the triggered records
    **/
    public CampaignMemberTriggerHandler(CampaignMember[] campaignMembers) {
        this.campaignMembers = campaignMembers; 
    }
    
    /**
     ** Load the lead records associated to the records triggered
    **/
    public void loadLeads(){
        leadMap = new Map<Id, Lead>();
        Set<Id> leadIds = new Set<Id>();
        for(CampaignMember campaignMember : campaignMembers){
//            leadMap.put(campaignMember.LeadId, null);           
            leadIds.add(campaignMember.LeadId);
        }
        
        Lead[] leads = [select Id, Market_Segment__c, Strategy_ROI__c from Lead where Id in :leadIds];
        for(Lead lead : leads) {
            leadMap.put(lead.Id, lead);
        }
    }
    
    /**
     ** Load the campaign records associated to the records triggered
    **/
    public void loadCampaigns(){
        campaignMap = new Map<Id, Campaign>();
        for(CampaignMember campaignMember : campaignMembers){
            campaignMap.put(campaignMember.CampaignId, null);           
        }
        
        Campaign[] campaigns = [select Id, (select Id, Vertical__c from Strategy_ROI_s__r) from Campaign where Id in :campaignMap.keySet()];
        for(Campaign campaign : campaigns) {
            campaignMap.put(campaign.Id, campaign);
        }
    }
    
    /**
     ** Associates the Leads with the Strategy_ROI__c
    **/
    public void onAfterInsert(){
        Set<Id> sroiIds = new Set<Id>();
        for(CampaignMember campaignMember : campaignMembers) {
            Lead lead = leadMap.get(campaignMember.LeadId);
            Campaign campaign = campaignMap.get(campaignMember.CampaignId);
            for(Strategy_ROI__c sroi : campaign.Strategy_ROI_s__r){
                if(lead != null && sroi.Vertical__c == lead.Market_Segment__c) {
                    lead.Strategy_ROI__c = sroi.Id;
                    leadMap.put(lead.Id, lead);
                    sroiIds.add(sroi.Id);
                }
            }
        }
        update leadMap.values();
        updateCounts(sroiIds);
    }

    /**
     ** Unassociates the Leads from the Strategy_ROI__c
    **/
    public void onAfterDelete(){
        Set<Id> sroiIds = new Set<Id>();
        for(CampaignMember campaignMember : campaignMembers) {
            Lead lead = leadMap.get(campaignMember.LeadId);
            Campaign campaign = campaignMap.get(campaignMember.CampaignId);
            for(Strategy_ROI__c sroi : campaign.Strategy_ROI_s__r){
                if(lead != null && sroi.Vertical__c == lead.Market_Segment__c) {
                    lead.Strategy_ROI__c = null;
                    leadMap.put(lead.Id, lead);
                    sroiIds.add(sroi.Id);
                }
            }
        }
        update leadMap.values();
        updateCounts(sroiIds);
    }

    /**
     ** After associating leads to Strategy_ROI__c, this loops through them and updates their counts
    **/
    public void updateCounts(Set<Id> sroiIds){
        Strategy_ROI__c[] srois = [select Id, of_Opportunities__c, of_Leads__c, (select Id, Amount from Opportunities__r), (select Id from Leads__r) from Strategy_ROI__c where Id in :sroiIds];
        for(Strategy_ROI__c sroi : srois){
            sroi.of_Leads__c = sroi.Leads__r.size();
        }
        update srois;
    }        

    /**
     ** Static method called by trigger
    **/
    public static void onAfterInsert(CampaignMember[] campaignMembers) {
        CampaignMemberTriggerHandler handler = new CampaignMemberTriggerHandler(campaignMembers);
        handler.loadLeads();
        handler.loadCampaigns();
        handler.onAfterInsert();
    }
    
    /**
     ** Static method called by trigger
    **/
    public static void onAfterDelete(CampaignMember[] campaignMembers) {
        CampaignMemberTriggerHandler handler = new CampaignMemberTriggerHandler(campaignMembers);
        handler.loadLeads();
        handler.loadCampaigns();
        handler.onAfterDelete();
    }

}