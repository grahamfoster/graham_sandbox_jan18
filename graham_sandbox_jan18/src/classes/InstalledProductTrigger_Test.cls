/**
** @author Reid Beckett, Cloudware Connections
** @created Nov 11/2016
**
** Test coverage for the InstalledProduct Trigger
** 
**/
@isTest
private class InstalledProductTrigger_Test {
	
	@isTest static void test() {
		// Implement test code
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;
        
        Asset assetObj = new Asset(Name = 'Test Asset', AccountId = a.Id, SerialNumber = 'ABC123', Status = 'Active');
        insert assetObj;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVMXC__Serial_Lot_Number__c = 'ABC123');
		insert ip;
		update ip;
		delete ip;
		undelete ip;
        
        assetObj = [select Status from Asset where Id = :assetObj.Id];
        system.assertEquals('Inactive', assetObj.Status);
	}
	/*
	@isTest static void test_COBO_update_NoLongerTopLevel() {
		// Implement test code
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id);
		insert ip1;		

		Case installationCase = new Case(AccountId = a.Id, 
			RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(), 
			Contactid = c.Id,
			Sales_Order_Number__c = '123456',
			SVMXC__Component__c = ip1.Id
		);
		insert installationCase;

		SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = installationCase.Id, SVMXC__Order_Type__c = 'Installation');
		insert workOrder;

		Case trainingCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), Contactid = c.Id, Installation_Case__c = installationCase.Id);
		insert trainingCase;
		
		Test.startTest();
		Oracle_Order__c oo = new Oracle_Order__c(Oracle_Order_Number__c = '123456');
		insert oo;

		Customer_Onboard__c cobo = new Customer_Onboard__c(InstallationCase__c = installationCase.Id, Training_Case__c = trainingCase.Id, Oracle_Order__c = oo.Id);
		insert cobo;

		cobo = [select Id, InstallationCase__c from Customer_Onboard__c	 where Id = :cobo.Id];
		system.assertEquals(installationCase.Id, cobo.InstallationCase__c);

		SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id);
		insert ip2;

		ip1.SVMXC__Top_Level__c = ip2.Id;
		update ip1;
		Test.stopTest();

		cobo = [select Id, InstallationCase__c from Customer_Onboard__c	 where Id = :cobo.Id];
		system.assertEquals(null, cobo.InstallationCase__c);
	}
	
	@isTest static void test_COBO_update_becomeTopLevel() {
		// Implement test code
		Account a = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a;

		Contact c = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = a.Id, Email='test@example.com', LMS_Username__c = 'test@test.com', LMS_Trigger_User_Create__c = false, LMS_User_ID__c = AbsorbUtil.generateGUID());
		insert c;

		SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id);
		insert ip2;

		SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a.Id, SVMXC__Top_Level__c = ip2.Id);
		insert ip1;		

		Case installationCase = new Case(AccountId = a.Id, 
			RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Service Request Case').getRecordTypeId(), 
			Contactid = c.Id,
			Sales_Order_Number__c = '123456',
			SVMXC__Component__c = ip1.Id
		);
		insert installationCase;

		SVMXC__Service_Order__c workOrder = new SVMXC__Service_Order__c(SVMXC__Case__c = installationCase.Id, SVMXC__Order_Type__c = 'Installation');
		insert workOrder;

		Case trainingCase = new Case(AccountId = a.Id, RecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('Training').getRecordTypeId(), Contactid = c.Id, Installation_Case__c = installationCase.Id);
		insert trainingCase;
		
		Test.startTest();
		Oracle_Order__c oo = new Oracle_Order__c(Oracle_Order_Number__c = '123456');
		insert oo;

		Customer_Onboard__c cobo = new Customer_Onboard__c(Training_Case__c = trainingCase.Id, Oracle_Order__c = oo.Id);
		insert cobo;

		cobo = [select Id, InstallationCase__c from Customer_Onboard__c	 where Id = :cobo.Id];
		system.assertEquals(null, cobo.InstallationCase__c);

		ip1.SVMXC__Top_Level__c = null;
		update ip1;

		cobo = [select Id, InstallationCase__c from Customer_Onboard__c	 where Id = :cobo.Id];
		system.assertEquals(installationCase.Id, cobo.InstallationCase__c);
		Test.stopTest();
	}
	*/
    
    @isTest static void test_updateAsset() {
		Account a1 = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a1;

		Account a2 = new Account(Name = 'Test Account #2', BillingCountry = 'Canada');
		insert a2;

        SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(SVMXC__Company__c = a1.Id);
		insert ip;
        
        Asset assetObj = [select Id, AccountId from Asset where Installed_Product__c = :ip.Id];
        system.assertEquals(a1.Id, assetObj.AccountId);
        
        ip.SVMXC__Company__c = a2.Id;
        update ip;

		assetObj = [select Id, AccountId from Asset where Installed_Product__c = :ip.Id];
        system.assertEquals(a2.Id, assetObj.AccountId);
	}

    @isTest static void test_createUpdateAsset_parentAsset() {
		Account a1 = new Account(Name = 'Test Account', BillingCountry = 'Canada');
		insert a1;

		Account a2 = new Account(Name = 'Test Account #2', BillingCountry = 'Canada');
		insert a2;

        SVMXC__Installed_Product__c ip1 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a1.Id);
		insert ip1;
        
        SVMXC__Installed_Product__c ip2 = new SVMXC__Installed_Product__c(SVMXC__Company__c = a1.Id, SVMXC__Top_Level__c = ip1.Id);
		insert ip2;

        Asset assetObj1 = [select Id, AccountId, ParentId from Asset where Installed_Product__c = :ip1.Id];
        Asset assetObj2 = [select Id, AccountId, ParentId from Asset where Installed_Product__c = :ip2.Id];
        
        system.assertEquals(null, assetObj1.ParentId);
        system.assertEquals(assetObj1.Id, assetObj2.ParentId);
        
        //unset the top-level value
        ip2.SVMXC__Top_Level__c = null;
        update ip2;
        
        assetObj2 = [select Id, AccountId, ParentId from Asset where Installed_Product__c = :ip2.Id];
        system.assertEquals(null, assetObj2.ParentId);

		//switch the top-level value
        ip1.SVMXC__Top_Level__c = ip2.Id;
        update ip1;

        assetObj1 = [select Id, AccountId, ParentId from Asset where Installed_Product__c = :ip1.Id];
        system.assertEquals(assetObj2.Id, assetObj1.ParentId);
	}
}