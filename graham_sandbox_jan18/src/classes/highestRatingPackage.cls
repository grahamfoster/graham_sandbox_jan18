public with sharing class highestRatingPackage {

   public List<pckWrapper> pck_list;
   List<Package__c> pckgList {get; set;}
   public Set<Id> packageId = new Set<Id>();

   public highestRatingPackage() {}

   public List<pckWrapper> getpck(){
         pck_list = new List<pckWrapper>();

         pckgList = [
            SELECT Id, Name, Product__r.Product_Category__c, Product__r.Product_Category__r.Name,
                Product__r.Product_Category__r.Category_Icon__c, Product__r.Name,
                Competitor_Product__r.Name, Package_Name__c, Rating__c,
                Application_Variants__r.Application__r.Image__c,
                Application_Variants__r.Application_Variant_Name__c,
                Application_Variants__r.Color__c,
                Application_Variants__r.Application__r.Color__c
            FROM Package__c
            WHERE isActive__c =: true AND Product__c != null AND
                Product__r.Product_Category__c != null AND Competitor_Product__c != null AND
                Rating__c != null AND Rating__c > 0
            ORDER BY Rating__c DESC Limit 20
         ];

         for(package__c pk :pckgList){
             //package__c pkc;
             integer fstr = 0;
             integer sstr = 0;
             integer nstr = 0;

             if(pk.Rating__c != null && pk.Rating__c > 0){
                fstr = (integer)pk.Rating__c;
                if((pk.Rating__c - pk.Rating__c.round(roundingMode.DOWN))>0)
                sstr = 1;
                nstr = 5 - (integer)pk.Rating__c.round(roundingMode.DOWN) - sstr;
                pck_List.add(new pckWrapper(pk, fstr, sstr, nstr, (double)pk.Rating__c));
             } else {
                pck_List.add(new pckWrapper(pk,0,0,5,0));
             }
         }

         return pck_List;
    }

    public class pckWrapper{
        public Package__c pck {get; set;}
        public integer fullStar {get; set;}
        public integer semiStar {get; set;}
        public integer noStar {get; set;}
        public double rating {get; set;}

        public pckwrapper(Package__c p, integer f, integer s, integer n, double ra) {
            this.pck = p; //assign package
            this.fullStar = f; //assign number of full stars
            this.semiStar = s; //assign number of semi stars
            this.noStar = n; //assign number of no stars
            this.rating = ra;//assign the rating
        }
    }
}