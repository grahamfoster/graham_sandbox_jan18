/**
** @author Reid Beckett, Cloudware Connections
** @created Mar 6/2015
**
** Service-only dashboard showing the YTD total against the quarterly cumulative quotas
** 
**/
public class MySFDCServiceCumulativeYTDQuotaDashboard extends BaseMySFDCDashboard implements IMySFDCDashboard, Pager {
	public MySFDCQuota quotas {get;set;}

	public override GoogleGaugeChartData getGoogleGaugeChartData()
	{
		return null;
	}
	
	public String getReportTitle()
	{
		return 'Cumulative YTD vs Quarterly Quotas';
	}

	protected override String selectFieldList() {
		return 'Id, Name, AccountId, Account.Name, StageName, In_Forecast_Rep__c, In_Forecast_Mgr__c, Rep_Upside__c, Mgr_Upside__c, Funding__c, Competitive_Position__c, Timing__c, Amount, Manager__c, Market_Vertical__c, Owner.Name, Probability';
	}

	protected override String criteria() {
		//by default get all opps that are not closed lost
		String crit = 'OwnerId in :userIds and IsWon = true and RecordType.Name = \'Service\'';
		
		if(!String.isBlank(filter.marketVertical)) {
			if(filter.marketVertical == 'Clinical & Forensic') {
				crit += ' and (Market_Vertical__c = \'Clinical\' or Market_Vertical__c = \'Forensic\')';
			}else if(filter.marketVertical == 'Food & Environmental') {
				crit += ' and (Market_Vertical__c = \'Food/Beverage\' or Market_Vertical__c = \'Environmental/Industrial\')';
			}else {
				crit += ' and Market_Vertical__c = :marketVertical';
			}
		}

		String productType = filter.productType;
		if(!String.isBlank(productType)) {
			crit += ' and Product_Type__c = :productType';
		}

		if(!String.isBlank(filter.region)) {
			if(String.isBlank(filter.country)) {
				//all countries in the region
				crit += ' and Account.BillingCountry in :countries';
			}else{
				//specific country
				crit += ' and Account.BillingCountry in :country';
			}
		}
		
		return crit;
	}

	public GoogleBarChartData getGoogleBarChartData()
	{
		GoogleBarChartData cd = new GoogleBarChartData('cumulative_quota');
		cd.title = 'Cumulative YTD vs Quarterly Quotas';
		cd.hName = 'Quarter';
		cd.vName = 'Cumulative Total';
		cd.hLabel = 'Cumulative Total ($K)';
		cd.numberFormat = '$#0.000K';
		//TODO: figure out the ticks, set the data.
		Set<Id> userIds = new Set<Id> { filter.runAs.Id };
		List<User> usersInRoleHierarchy = MySFDCUtil.findUsersInRoleHierarchy(filter.runAs.UserRoleId);
		for(User subUser : usersInRoleHierarchy) {
			userIds.add(subUser.Id);
		}

		String marketVertical = filter.marketVertical;
		String productType = filter.productType;
		List<String> country = RegionUtil.getCountryAliases(filter.country);
		List<String> countries = String.isBlank(filter.region) ? new List<String>() : RegionUtil.getAllCountriesWithAliases(filter.region);

		Date jan1 = Date.newInstance(Date.today().year(), 1, 1);
		Date today = Date.today();
		String soql = 'select SUM(Amount) a from Opportunity where CloseDate >= :jan1 and CloseDate <= :today and '+ criteria();
		AggregateResult[] aggrResults = Database.query(soql);
		Double total = (aggrResults.size() > 0 && aggrResults[0].get('a') != null ) ? (Double)aggrResults[0].get('a')/1000.0 : 0;

		//load the quotas for the current FY
		Double q1Quota = 0, q2Quota = 0, q3Quota = 0, q4Quota = 0;
		String year = String.valueOf(Date.today().year());
		List<AggregateResult> quotasAggrResults = [select Fiscal_Quarter__c qtr, SUM(Quota__c) quota from Quota__c where 
			Fiscal_Year__c = :year and OwnerId = :filter.runAs.Id and Type__c = :productType
			group by Fiscal_Quarter__c
			order by Fiscal_Quarter__c asc];
		for(AggregateResult aggr : quotasAggrResults) {
			String qtr = (String)aggr.get('qtr');
			if(qtr == '1') {
				if(aggr.get('quota') != null) q1Quota = (Double)aggr.get('quota')/1000.0;
			}else if(qtr == '2') {
				if(aggr.get('quota') != null) q2Quota = (Double)aggr.get('quota')/1000.0;
			}else if(qtr == '3') {
				if(aggr.get('quota') != null) q3Quota = (Double)aggr.get('quota')/1000.0;
			}else if(qtr == '4') {
				if(aggr.get('quota') != null) q4Quota = (Double)aggr.get('quota')/1000.0;
			}
		}

		Double q2Q = q1Quota + q2Quota;
		Double q3Q = q2Q + q3Quota;
		Double q4Q = q3Q + q4Quota;

		//Determine the current expected amount vs. quotas
		Period currentQuarter = MySFDCUtil.getCurrentQuarter();
		Period currentFiscalWeek = [select Id, Number, StartDate, EndDate from Period
			where FiscalYearSettings.Name = :year 
			and StartDate <= :today and EndDate >= :today
			and Type = 'Week'
			limit 1];
		Double expectedQuota = calculateExpectedQuota(new Double[]{q1Quota, q2Quota, q3Quota, q4Quota}, currentFiscalWeek.Number);
		cd.vName = 'Cumulative Total. Expected Actual in Week #' + currentFiscalWeek.Number + ' is $' + expectedQuota + 'K';
		String color = total >= expectedQuota ? 'green' : 'red';

		cd.addTick(getTickLabel(1,q1Quota), q1Quota);
		cd.addTick(getTickLabel(2,q2Q), q2Q);
		cd.addTick(getTickLabel(3,q3Q), q3Q);
		cd.addTick(getTickLabel(4,q4Q), q4Q);
		cd.colors = new List<String>{color};
		cd.data = new List<List<Double>> {
			new List<Double> { total }
		};
		return cd;
	}

	private static String getTickLabel(Integer quarter, Double quota)
	{
		String s = 'Q' + quarter + '\\n($';
		if(quota >= 1000) s += String.valueOf(quota/1000.0) + 'M';
		else s += String.valueOf(Integer.valueOf(quota)) + 'K';
		s += ')';
		return s;
	}

	private static Double calculateExpectedQuota(Double[] quarterlyQuotas, Integer week)
	{
		Double q2Q = quarterlyQuotas[0] + quarterlyQuotas[1];
		Double q3Q = quarterlyQuotas[0] + quarterlyQuotas[1] + quarterlyQuotas[2];
		if(week <= 13) return calculateExpectedQuotaForQuarter(week, quarterlyQuotas[0]);
		else if(week <= 26) return quarterlyQuotas[0] + calculateExpectedQuotaForQuarter(week-13, quarterlyQuotas[1]);
		else if(week <= 39) return q2Q + calculateExpectedQuotaForQuarter(week-26, quarterlyQuotas[2]);
		else return q3Q + calculateExpectedQuotaForQuarter(week-39, quarterlyQuotas[3]);
	}

	private static Double calculateExpectedQuotaForQuarter(Integer week, Double quarterlyQuota)
	{
		MySFDCSettings__c settings = !Test.isRunningTest() ? MySFDCSettings__c.getInstance() : new MySFDCSettings__c(Month_1_Quota_Pct__c=0.2, Month_2_Quota_Pct__c=0.25, Month_3_Quota_Pct__c=0.55);
		Double m1Pct = settings.Month_1_Quota_Pct__c/100.0;
		Double m2Pct = settings.Month_2_Quota_Pct__c/100.0;
		Double m3Pct = settings.Month_3_Quota_Pct__c/100.0;

		Double pct = 0;
		if(week <= 4) {
			pct = m1Pct * week/4.0;
		}else if(week <= 8){
			pct = m1Pct + m2Pct * ((week-4)/4.0);
		}else {
			pct = m1Pct + m2Pct + m3Pct * ((week-8)/5.0);
		}

		return pct*quarterlyQuota;
	}
}