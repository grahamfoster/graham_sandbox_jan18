public class AbCloningUtils {

  //This method will take as input a local Quote__c object that has been copied from another quote.  
  //  We will set various flags and save the quote as a revision
  public static void reviseQuote(Quote__c sfdcRevisedQuote) {
   try{
    System.debug('QUOTING: revised quote id: ' + sfdcRevisedQuote.Id);
    System.debug('QUOTING: revise SAP quote: ' + sfdcRevisedQuote.SAP_Quote_Num__c);
    System.debug('QUOTING: revise CDI quote: ' + sfdcRevisedQuote.CDI_Quote_Num__c);
    
    String quoteRevisionType = AbConstants.SAP_REVISION_TYPE;
    try{ 
    //Determine if this is an SFDC, CDI, or SAP quote revision
    if(!sfdcRevisedQuote.Saved_to_CDI__c){
      //----- SFDC Revision ----------
      quoteRevisionType = AbConstants.SFDC_REVISION_TYPE;
      sfdcRevisedQuote.Revision_Num__c = Double.valueOf(sfdcRevisedQuote.CDI_Quote_Num__c); //revision num is original CDI Quote Num
      sfdcRevisedQuote.SAP_Quote_Num__c = '';
         
    } else if (sfdcRevisedQuote.Contact__c == null) { 
      //----- SAP Revision does not save Contact----------
      quoteRevisionType = AbConstants.SAP_REVISION_TYPE;
      sfdcRevisedQuote.Revision_Num__c = Double.valueOf(sfdcRevisedQuote.SAP_Quote_Num__c); //revision num is original SAP Quote
      sfdcRevisedQuote.SAP_Quote_Num__c = '';   
    } else if (sfdcRevisedQuote.Contact__c != null) { 
      //----- CDI Revision does save the Contact----------
      quoteRevisionType = AbConstants.CDI_REVISION_TYPE;
      sfdcRevisedQuote.Revision_Num__c = Double.valueOf(sfdcRevisedQuote.CDI_Quote_Num__c); //revision num is original CDI Quote Num
      sfdcRevisedQuote.SAP_Quote_Num__c = '';   
    }
    } catch(Exception e){
    }
            
    sfdcRevisedQuote.Submit_Date__c = null;
    sfdcRevisedQuote.Temporary_Quote__c = false;
    sfdcRevisedQuote.Revision_Source__c = quoteRevisionType;
    sfdcRevisedQuote.CDI_Quote_Num__c = null;
    sfdcRevisedQuote.Saved_To_CDI__c = null;
      
    System.debug('QUOTING: REVISE: Revision_Num__c: ' + sfdcRevisedQuote.Revision_Num__c);
    System.debug('QUOTING: REVISE: SAP_Quote_Num__c: ' + sfdcRevisedQuote.SAP_Quote_Num__c);
	
    /*if(sfdcRevisedQuote.Sold_To__c != null){
      Account currentAcct = [SELECT Id, Account_Sold_To__c FROM Account WHERE Account_Sold_To__c = :sfdcRevisedQuote.Sold_To__c];
      sfdcRevisedQuote.Account__c = currentAcct.Id;
    }*/    

    sfdcRevisedQuote.Created_By_CDI_User__c = AbQuotingUtils.getUsersSixPlusTwo();
    sfdcRevisedQuote.Owned_By_CDI_User__c = AbQuotingUtils.getUsersSixPlusTwo();
    sfdcRevisedQuote.Start_Date__c = System.Today();
    sfdcRevisedQuote.Status__c = AbConstants.DRAFT_STATUS;
    sfdcRevisedQuote.Status_Date__c = Date.Today();
    sfdcRevisedQuote.Region__c = AbQuotingUtils.getUsersRegion();
    /*try{
      sfdcRevisedQuote.CDI_Quote_Num__c = AbQuotingUtils.retrieveNewQuoteNumFromCDI(null); 
      System.debug('QUOTING: cdi quote number is ' + sfdcRevisedQuote.CDI_Quote_Num__c);
    } catch (Exception e){
      System.debug('QUOTING: Exception retrieving new quote number from CDI: ' + e);
      throw new QuotingException('Error retrieving next quote number from CDI: ' + e);
    }*/
   }
   catch(Exception e){
   }
  }

    public static String cloneQuote(Id sfdcRevisedQuoteId) {
      String newId = '';
    
      String quoteRevisionType = AbConstants.SAP_REVISION_TYPE;
      //Determine if this is an SFDC, CDI, or SAP quote revision      
      Quote__c oldQuote = [
        SELECT Zip_Code__c, Warranty__c, Valid_Start__c, Valid_End__c, Use_Secondary_Shipping__c, 
          Total_Tax__c, Total_Freight__c, Total_Fees__c, Tender_Reference__c, Temporary_Quote__c, 
          Submit_Date__c, Status_Date__c, Start_Date__c, Special_Instructions__c, 
          Single_Use__c, Short_Description__c, Shipping_Zip__c, Shipping_State__c, 
          Shipping_Phone__c, Shipping_Name4__c, Shipping_Name3__c, Shipping_Name2__c, 
          Shipping_Name1__c, Shipping_Fax__c, Shipping_Email__c, Shipping_Country__c, 
          Shipping_City__c, Shipping_Address2__c, Shipping_Address1__c, Ship_To__c, 
          Sec_Shipping_Zip__c, Sec_Shipping_State__c, Sec_Shipping_Phone__c, Sec_Shipping_Name4__c, 
          Sec_Shipping_Name3__c, Sec_Shipping_Name2__c, Sec_Shipping_Name1__c, Sec_Shipping_Fax__c, 
          Sec_Shipping_Email__c, Sec_Shipping_Country__c, Sec_Shipping_Copied__c, Sec_Shipping_City__c, 
          Sec_Shipping_Address2__c, Sec_Shipping_Address1__c, Sales_Rep__c, 
          Sales_Org__c, Sales_Office__c, Sales_Group__c, SAP_Contact__c, Saved_To_CDI__c,
          Revision_Source__c, Revision_Num__c, Region__c, Range_Expired_Start__c, 
          Range_Expired_End__c, Range_Created_Start__c, Range_Created_End__c, Quote_Output_Title__c, 
          Quote_Introduction__c, Price_Output__c, SAP_Quote_Num__c, CDI_Quote_Num__c,
          Preview_Output__c, Payment_Terms__c, Payer__c, Owned_By_CDI_User__c, Output_Type__c, 
          Output_Medium__c, Output_Email2__c, Originally_Created_By_SFDC_User__c, Opt_Total_Tax__c, 
          Opt_Total_Freight__c, Opt_Total_Fees__c, Opt_Net_Price__c, Opportunity__c, Net_Price__c, 
          Name, Line_Item_Seq__c, Language__c, LI_Total_Tax__c, LI_Total_Freight__c, LI_Total_Fees__c, 
          LI_Net_Price__c, Installation__c, INCO_Terms__c, Header_Text__c, 
          Header_Discount__c, Header_Discount_Total__c, Footer_Text__c, Expire_Date__c, 
          Est_Delivery_Date__c, Dest_Country__c, Description__c, Currency__c, Created_By_CDI_User__c, 
          Create_Date__c, Contact__c, Bill_To__c, Account__c, Sold_To__c, Id, Status__c,
          SAP_Contact_Display_Value__c, Bill_To_Display_Value__c  
          From Quote__c WHERE Id = :sfdcRevisedQuoteId
        ]; 

      System.debug('QUOTING: revised quote id: ' + oldQuote.Id);
      System.debug('QUOTING: revise SAP quote: ' + oldQuote.SAP_Quote_Num__c);
      System.debug('QUOTING: revise CDI quote: ' + oldQuote.CDI_Quote_Num__c);
      
      if(oldQuote != null){
        List<Quote__c> oldQuoteList = new List<Quote__c>();
        oldQuoteList.add(oldQuote);
        List<Quote__c> newQuote = oldQuoteList.deepClone(false);
        
        newQuote[0].CDI_Quote_Num__c = null;
        newQuote[0].Status__c = AbConstants.DRAFT_STATUS;
        //System.debug('----------Use Shipping Address-----------'+newQuote[0].Use_Secondary_Shipping__c);        
        newQuote[0].Saved_to_CDI__c = false;
        newQuote[0].Submit_Date__c = null;
        newQuote[0].Temporary_Quote__c = false;
		newQuote[0].Revision_Source__c = quoteRevisionType;
    	newQuote[0].Created_By_CDI_User__c = AbQuotingUtils.getUsersSixPlusTwo();
    	newQuote[0].Owned_By_CDI_User__c = AbQuotingUtils.getUsersSixPlusTwo();
    	newQuote[0].Start_Date__c = System.Today();
    	newQuote[0].Status_Date__c = Date.Today();
    	newQuote[0].Region__c = AbQuotingUtils.getUsersRegion();
		
        if(!oldQuote.Saved_to_CDI__c){
          //----- SFDC Revision ----------
          quoteRevisionType = AbConstants.SFDC_REVISION_TYPE;
          if (oldQuote.CDI_Quote_Num__c != null)
          	newQuote[0].Revision_Num__c = Double.valueOf(oldQuote.CDI_Quote_Num__c); //revision num is original CDI Quote Num
          newQuote[0].SAP_Quote_Num__c = '';
        } else if (oldQuote.Contact__c == null) { 
          //----- SAP Revision does not save Contact----------
          quoteRevisionType = AbConstants.SAP_REVISION_TYPE;
          newQuote[0].Revision_Num__c = Double.valueOf(oldQuote.SAP_Quote_Num__c); //revision num is original SAP Quote
          newQuote[0].SAP_Quote_Num__c = '';   
        } else if (oldQuote.Contact__c != null) { 
          //----- CDI Revision does save the Contact----------
          quoteRevisionType = AbConstants.CDI_REVISION_TYPE;
          newQuote[0].Revision_Num__c = Double.valueOf(oldQuote.CDI_Quote_Num__c); //revision num is original CDI Quote Num
          newQuote[0].SAP_Quote_Num__c = '';   
        }
      
	    System.debug('QUOTING: REVISE: Revision_Num__c: ' + newQuote[0].Revision_Num__c);
    	System.debug('QUOTING: REVISE: SAP_Quote_Num__c: ' + newQuote[0].SAP_Quote_Num__c);

    	/*if(oldQuote.Sold_To__c != null){
      		List<Account> currentAcct = [SELECT Id, Account_Sold_To__c FROM Account WHERE Account_Sold_To__c = :oldQuote.Sold_To__c];
      		newQuote[0].Account__c = currentAcct[0].Id;
    	} */     

        insert newQuote;
        newId = newQuote[0].Id;
        
        //System.debug('TONYTEST: newQuote Id = '+newId);
      
        cloneProducts(newId, sfdcRevisedQuoteId);
      }   
      return newId; 
    }

    public static void cloneProducts(String returnId, Id sfdcQuoteId){
      if(sfdcQuoteId != null && returnId != null){
        List<Quote_Line_Item__c> myItems = [
          SELECT Name, Line_Item__c, Product_Description__c, Product_Desc_Override__c, Template_Id__c, 
          List_Price__c, Manual_Price__c, Approved_Discount__c, Manual_Discount_Percent__c, Manual_Discount__c, 
          Discount_Level__c, Discount_Per_Unit__c, Header_Discount_Value__c, Manual_Price_Override__c, 
          Item_Net_Value__c, Freight__c, Fees__c, Tax__c, Quantity__c, Line_Item_Seq__c, Line_Item_Text__c, 
          Product_Num__c, New_Description__c, Final_Description__c, Optional_Flag__c, Template_Type__c, Igor_Code__c 
          FROM Quote_Line_Item__c WHERE Quote__c = :sfdcQuoteId 
          ORDER BY Line_Item_Seq__c ASC LIMIT 300];
          
        List<Quote_Line_Item__c> liMax = (List<Quote_Line_Item__c>)[
          SELECT Line_Item_Seq__c FROM Quote_Line_Item__c 
          WHERE Quote__c = :returnId AND Optional_Flag__c = false 
          ORDER BY Line_Item_Seq__c DESC LIMIT 1];
        //if(liMax != null){ liMax[
          
        List<Quote_Line_Item__c> optMax = (List<Quote_line_Item__c>)[
          SELECT Line_Item_Seq__c FROM Quote_Line_Item__c 
          WHERE Quote__c = :returnId AND Optional_Flag__c = true 
          ORDER BY Line_Item_Seq__c DESC LIMIT 1];
          
        //System.debug('TONYTEST: ReturnId '+returnId);
        if(myItems != null){
          Integer liSeq = 1;
          Integer optSeq = AbConstants.OPTIONS_STARTING_SEQ+1; 
          
          if(liMax != null){
          for(Quote_Line_Item__c bla :liMax){
            if(bla.Line_Item_Seq__c != null){ liSeq = bla.Line_Item_Seq__c.intValue() +1; }  
          }}
          if(optMax != null){
          for(Quote_Line_Item__c alb :optMax){
            if(alb.Line_Item_Seq__c != null){ optSeq = alb.Line_Item_Seq__c.intValue() +1; } 
          }}
          
          System.debug('Line Item Sequence: '+liSeq);
          System.debug('Line Item Option Sequence: '+optSeq);
          
          List<Quote_Line_Item__c> retItems = myItems.deepClone(false);
          for(Quote_Line_Item__c li :retItems){
          	li = updateProductData(li);
            li.Quote__c = String.valueOf(returnId);
            if(li.Optional_Flag__c == false){ 
              li.Line_Item_Seq__c = liSeq; 
              li.Line_Item__c = liSeq++; 
            }else{
              li.Line_Item_Seq__c = optSeq;
              li.Line_Item__c = optSeq++;
            }
            //li.Id = null;
          }
          insert retItems;
        }
      }
    }

    public static Quote_Line_Item__c updateProductData(Quote_Line_Item__c p){
        String igor = p.IGOR_Code__c;
        if(igor != null){
          Product_Hierarchy__c details = [
            SELECT IGOR_CODE__c, IGOR_ITEM_CLASS__c, PLAN_CODE__c, PRODLINE__c 
            FROM Product_Hierarchy__c where IGOR_CODE__c = :igor
          ];
          p.IGOR_Item_Class__c = details.IGOR_ITEM_CLASS__c;
          p.Plan_Code__c = details.PLAN_CODE__c;
          p.Product_Line__c = details.PRODLINE__c;
        }
        return p;
    }

}