public with sharing class MainPageAdmin_Ctrl{


        public list<Product_Category__c> ProductCategList{set;get;}
        public list<String> cateIdList{set;get;}
        //public map<String,list<ProductWrapper>> productMap{set;get;}
        public map<String,list<ProductMetaCat>> catMetaMap{set;get;}
        public list<ProductWrapper> productsList{set;get;}
        public list<ProductMetaCat> metaListTemp{set;get;}
        public ProductWrapper productClass{set;get;} 
        public String EnterCategory{get;set;}
        public String itemid{get;set;}
        public string closingId{get;set;}
        public string deleteCatID{get;set;}
        public String TabInFocus{get;set;}
        public String hiddenProductId{get;set;}
        
        private String sortDirection = 'ASC';
        private String sortExp = '';
    
        public string check{get;set;}
    
     public void MainPageAdmin_Ctrl_Action() {
       
        // for delete category
        setActiveTab();
        deleteCatID = ApexPages.currentPage().getParameters().get('deleteCatID');
        System.debug('deleteCatID/....'+deleteCatID);
        if(deleteCatID != null && deleteCatID != '')
        {
            this.doInactive(deleteCatID);
        } 
        
        ProductCategList = new List<Product_Category__c>();
        //ProductCategList = [Select isActive__c,Product_Category_Name__c,id, Name, Category_Icon__c From Product_Category__c where isActive__c = true order by createdDate];\
        ProductCategList = [Select isActive__c,Product_Category_Name__c,id, Name, Category_Icon__c From Product_Category__c order by createdDate];
        cateIdList =  new list<String>();   
        System.debug('ProductCategList.....'+ProductCategList);
        for(Product_Category__c catList : ProductCategList) {
                cateIdList.add(catList.Id);     
        }
        System.debug('cateIdList...'+cateIdList);
        
            List<Specifications_Meta__c> prdFeatureList = [SELECT Name,Specifications_Meta_Name__c,Product_Category__c,Product_Category__r.Name,Product_Category__r.Product_Category_Name__c,Product_Category__r.Id,(Select name,Product_Meta_Name__c from Products_Meta__r order by CreatedDate ASC) FROM Specifications_Meta__c WHERE isActive__c = true and Product_Category__c IN :cateIdList order by Product_Category__r.createdDate];
        
            if(prdFeatureList.size() > 0 && prdFeatureList != null)
            {
                metaListTemp = new list<ProductMetaCat>();
                catMetaMap = new map<String,list<ProductMetaCat>>();
                ProductMetaCat productMetaObj;
                
                list<string> specIDs = new list<string>();
                for(Specifications_Meta__c spec : prdFeatureList) {
                    specIDs.add(spec.id);
                }
                
                list<Product_Meta__c> metaListTotal = [select id,Product_Meta_Name__c,Specifications_Meta__c,Type__c,Name,isActive__c, CreatedDate from Product_Meta__c where isActive__c=true AND Specifications_Meta__c IN : specIDs order by CreatedDate ASC];
                
                 for(Specifications_Meta__c spec : prdFeatureList) {
                     if(!spec.Products_Meta__r.isEmpty()){
                        //list<Product_Meta__c> metaList = spec.Products_Meta__r;
                        list<Product_Meta__c> metaList = new list<Product_Meta__c>();//[select id,Product_Meta_Name__c,Specifications_Meta__c,Type__c,Name,isActive__c, CreatedDate from Product_Meta__c where isActive__c=true AND Specifications_Meta__c=:spec.id order by CreatedDate ASC];
                        
                        for(Product_Meta__c meta : metaListTotal) {
                            if (meta.Specifications_Meta__c == spec.id) metaList.add(meta);
                        }
                        
                        System.debug('metaList...'+metaList);
                        for(Product_Meta__c meta : metaList) {
                            productMetaObj = new ProductMetaCat();
                            productMetaObj.MetaName = meta.Product_Meta_Name__c;
                            productMetaObj.MetaCreatedDate = meta.CreatedDate;
                            
                            if(catMetaMap.containsKey(spec.Product_Category__r.Id)){
                                metaListTemp = catMetaMap.get(spec.Product_Category__r.Id);        
                            } else {
                                metaListTemp = new list<ProductMetaCat>();  
                        }
                        metaListTemp.add(productMetaObj);
                             catMetaMap.put(spec.Product_Category__r.Id, metaListTemp); 
                        }
                 }
                     }
                     
                     
                for(String a :catMetaMap.keySet()){
                    list<ProductMetaCat> unsortedList =  catMetaMap.get(a); 
                    unsortedList.sort();
                }
            }
        System.debug('prdFeatureList....'+ prdFeatureList);
        System.debug('catMetaMap....'+ catMetaMap);
        /*
        if(cateIdList !=null && cateIdList.size() > 0)
        {
                List<Product2> productList = [select Name,Id,Product_Category__c,Model__c,Manufacturer__c, Manufacturer__r.Manufacturer_Name__c ,(select Product_Value_Name__c,Value__c,Product_Meta_Formula__c from Product_Values__r) from product2 where Product_Category__c IN :cateIdList order by Product_Category__c,Product_Category__r.createdDate];
                System.debug('productList....'+productList);
                if(productList.size() > 0 && productList != null)
                {
                        productMap =  new map<String,list<ProductWrapper>>();
                        List<ProductWrapper> tempList;
                          
                          for(Product2 prod2 : productList) {
                              
                              // create ProductWrapper Object 
                              ProductWrapper wrapperObj = new ProductWrapper(); 
                              if(prod2.Manufacturer__c != null){
                                  wrapperObj.Manufacturer = prod2.Manufacturer__r.Manufacturer_Name__c;
                                  wrapperObj.ManufacturerID =prod2.Manufacturer__c;
                              }
                              
                               wrapperObj.Model = prod2.Model__c;
                               wrapperObj.ProdId =  prod2.Id;
                               wrapperObj.CatId = prod2.Product_Category__c;                                                
                               wrapperObj.Name = prod2.Name;  
                               if(!prod2.Product_Values__r.isEmpty()){
                                   wrapperObj.metaList = prod2.Product_Values__r;    
                               }
                               // create ProductWrapper Object 
                                
                               if(productMap.containsKey(prod2.Product_Category__c)){
                                   tempList = productMap.get(prod2.Product_Category__c);        
                               } else {
                                   tempList = new List<ProductWrapper>();  
                               }
                               
                               tempList.add(wrapperObj);
                               productMap.put(prod2.Product_Category__c, tempList);
                          }
                          System.debug('###productMap' + productMap);
                }
        } */
    }
    
   public Map<String,List<ProductWrapper>> getproductMap(){
        Map<String,List<ProductWrapper>> productMap =  new Map<String,List<ProductWrapper>>();
        
        if(cateIdList !=null && cateIdList.size() > 0)
        {
                String sortFullExp = '';
                List<Product2> productList;
                if(String.isNotEmpty(sortExpression)){
                    sortFullExp = sortExpression  + ' ' + sortDirection;
                    productList = Database.query('select RecordType.Name,Competitor_Model__c,Competitor_Manufacturer__c,family,Brand__c,Name,Id, position__c, Product_Category__c,Model__c,Manufacturer__c, Manufacturer__r.Manufacturer_Name__c,Manufacturer__r.Name ,(select Product_Value_Name__c,Value__c,Product_Meta_Formula__c from Product_Values__r) from product2 where isActive = true and Product_Category__c IN :cateIdList order by ' + sortFullExp); 
                } else {
                    productList = [select RecordType.Name,Competitor_Model__c,Competitor_Manufacturer__c,family,Brand__c,Name,Id, position__c, Product_Category__c,Model__c,Manufacturer__c, Manufacturer__r.Manufacturer_Name__c,Manufacturer__r.Name ,(select Product_Value_Name__c,Value__c,Product_Meta_Formula__c from Product_Values__r) from product2 where isActive = true and Product_Category__c IN :cateIdList order by  position__c,Product_Category__c,Product_Category__r.createdDate];
                }
                
                System.debug('productList....'+productList);
                
                if (productList.size() > 0 && productList != null)
                {
                    List<ProductWrapper> tempList;
                          
                      for(Product2 prod2 : productList) {
                          
                          /** create ProductWrapper Object **/
                          ProductWrapper wrapperObj = new ProductWrapper(); 
                          
                          wrapperObj.ProductName = prod2.Name;
                          wrapperObj.ProductType = prod2.RecordType.Name;
                          /*if(prod2.Manufacturer__c != null){
                              wrapperObj.Manufacturer = prod2.Manufacturer__r.Manufacturer_Name__c;
                            wrapperObj.ManufacturerID = prod2.Manufacturer__c;                            
                                             
                          } */ 
                           // added code ----                               
                          if(prod2.RecordType.Name == 'Competitor'){    
                                wrapperObj.Manufacturer = prod2.Competitor_Manufacturer__c;                                                     
                                wrapperObj.Model = prod2.Competitor_Model__c;
                          }
                          
                          if(prod2.RecordType.Name == 'AB Sciex' || prod2.RecordType.Name == 'NA PSM'){
                                wrapperObj.Manufacturer = prod2.Brand__c;                                                     
                                wrapperObj.Model = prod2.family;
                          }            
                           //------          
                           wrapperObj.ProdId =  prod2.Id;
                           wrapperObj.CatId = prod2.Product_Category__c;  
                                                                         
                           //wrapperObj.Name = prod2.Name; 
                           wrapperObj.Position = prod2.position__c;
                           
                           if(!prod2.Product_Values__r.isEmpty()){
                               wrapperObj.metaList = prod2.Product_Values__r;    
                           }
                           /** create ProductWrapper Object **/
                            
                           if(productMap.containsKey(prod2.Product_Category__c)){
                               tempList = productMap.get(prod2.Product_Category__c);        
                           } else {
                               tempList = new List<ProductWrapper>();  
                           }
                           
                           tempList.add(wrapperObj);
                           productMap.put(prod2.Product_Category__c, tempList);
                      }
                      System.debug('###productMap' + productMap);
                }
        }
        return productMap;   
   }
    
   public PageReference doInactive(String deletecatID){
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+closingId);
        PageReference pageRef = new PageReference('/apex/MainPageAdmin');
        System.debug('closingId....'+deletecatID);
        Product_Category__c prodobj = [Select isActive__c from Product_Category__c where id = :deletecatID];
        prodobj.isActive__c = false;
        try{
            //update prodobj;
            delete prodobj;
            return  pageRef;
        }
        catch(Exception Ex)
        {           system.debug(ex.getMessage());      
        }
        return  pageRef;
   }
   public class ProductWrapper {
        public String Model{set;get;}
        public String ProductName{set;get;}
        public String ProductType{set;get;}
        public String ProdId{set;get;}
        public String Manufacturer{set;get;}
        public String ManufacturerID{set;get;}
        public String Name{set;get;}
        public String CatId{set;get;}
        public Decimal Position{set;get;}
        public list<Product_Value__c> metaList{set;get;}
  }
  
  public class ProductMetaCat implements Comparable {
        public String MetaName{set;get;}
        public Datetime MetaCreatedDate{set;get;}
        
        public Integer compareTo(Object objToCompare) {
            ProductMetaCat compareMeta = (ProductMetaCat)objToCompare;
                if (MetaCreatedDate == compareMeta.MetaCreatedDate) return 0;
                if (MetaCreatedDate > compareMeta.MetaCreatedDate) return 1;
                return -1;   
        }
  }
  
  
   public PageReference delSelProduct(){
       if(String.isNotEmpty(hiddenProductId)){
            
          try {
                product2 prodObj = new product2();             
              
              prodObj.Id = hiddenProductId;
              delete prodObj;
              PageReference prodpage = new PageReference('/apex/MainPageAdmin');
              prodpage.getParameters().put('tabfocus','ProductTab');
              check = '';
              String selPrdtab = ApexPages.currentPage().getParameters().get('prodtab');
              if(String.isNotEmpty(selPrdtab)){
                  prodpage.getParameters().put('tab',selPrdtab);
              }
              prodpage.setRedirect(true);
              return prodpage;
          } catch(Exception e){   if(e.getmessage().contains(' could not be completed because it is associated with the following opportunity products.')) { check = 'True';
               system.debug('check>>>>>'+check);
                } 
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.INFO,e.getmessage().substringBetween('DELETE_FAILED,','[]')));                
            }        
       }
       hiddenProductId = null;
       return null;
   }
   
   public void val(){
        system.debug('value from page------>'+EnterCategory);
        Product_Category__c   pc = new Product_Category__c();
        pc.Product_Category_Name__c=EnterCategory;
        insert pc;
     
    }
    
    public void setActiveTab(){
       String activeTab = ApexPages.currentPage().getParameters().get('tabfocus');
       if(String.isNotEmpty(activeTab)){
           TabInFocus =  activeTab;  
       }
    }
    
    public PageReference sortProduct() {
         PageReference prodpage = new PageReference('/apex/MainPageAdmin');
         prodpage.getParameters().put('tabfocus','ProductTab');
         String selPrdtab = ApexPages.currentPage().getParameters().get('prodtab');
         
         if(String.isNotEmpty(selPrdtab)){
              prodpage.getParameters().put('tabfocus', selPrdtab);    
         }
         return prodpage;    
    }
    
    public String sortExpression
     {
         get
         {
            return sortExp;
         }
         set
         {
           //if the column is clicked on then switch between Ascending and Descending modes
           if (value == sortExp)
             sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
           else
             sortDirection = 'ASC';
           sortExp = value;
         }
     }
     
      public String getSortDirection(){
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
          return 'ASC';
        else
         return sortDirection;
      }
      
     public void setSortDirection(String value)
     {  
       sortDirection = value;
     }
    
}