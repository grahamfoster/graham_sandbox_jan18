// ===========================================================================
// Object: AbsorbAPIClient
// Company: Cloudware Connections
// Author: Reid Beckett
// Purpose: Logging for Absorb LMS integration functionality
// ===========================================================================
// Changes: 2016-02-01 Reid Beckett
//           Class created
// ===========================================================================
public with sharing class AbsorbLogger 
{
	public static void startMethod(String methodName) 
	{
		debug(methodName + '() :: start');
	}

	public static void endMethod(String methodName) 
	{
		debug(methodName + '() :: end');
	}
	
	public static void info(String message) 
	{
		log(LoggingLevel.INFO, message);
	}

	public static void debug(String message) 
	{
		log(LoggingLevel.DEBUG, message);
	}

	public static void error(String message) 
	{
		log(LoggingLevel.ERROR, message);
	}

	public static void log(LoggingLevel logLevel, String message) 
	{
		String logType = AbsorbUtil.getAbsorbLMSSettings().Log_Type__c;
		if(String.isBlank(logType) || logType == 'USER_DEBUG') System.debug(logLevel, getLogMessagePrefix() + message);
		//TODO: support for record creation when logType = PERSIST
	}

	private static String getLogMessagePrefix()
	{
		return AbsorbUtil.getAbsorbLMSSettings().Log_Message_Prefix__c + ' ';
	}

	public static void flush() 
	{
		String logType = AbsorbUtil.getAbsorbLMSSettings().Log_Type__c;
		if(logType == 'PERSIST')
		{
			//TODO: flush the persistent records
		}
	}
}