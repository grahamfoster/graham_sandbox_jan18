/**
** @author Reid Beckett, Cloudware Connections
** @created Jan 2/2015
**
** Schedulable impl to schedule the CostCurrencyBatchable job for upserting Cost records
** 
**/
global class CostCurrencySchedulable implements Schedulable {
	global void execute(SchedulableContext sc) {
		Decimal numHours = CostCurrencyBatchSettings__c.getInstance().Not_Updated_Since_Hours__c;
		DateTime notUpdatedSince = numHours != null ? DateTime.now().addHours(-Integer.valueOf(numHours)) : null;
		Decimal recordLimitDecimal = CostCurrencyBatchSettings__c.getInstance().Record_Limit__c;
		Integer recordLimit = recordLimitDecimal != null ? Integer.valueOf(recordLimitDecimal) : null;
		Decimal batchSizeDecimal = CostCurrencyBatchSettings__c.getInstance().Batch_Size__c;
		Integer batchSize = batchSizeDecimal != null ? Integer.valueOf(batchSizeDecimal) : null;
		Database.executeBatch(new CostCurrencyBatchable(recordLimit, notUpdatedSince), batchSize);
	}

	global static void scheduleJob(String jobTitle){
		System.schedule(jobTitle, '0 0 6 * * ?', new CostCurrencySchedulable());
	}
}