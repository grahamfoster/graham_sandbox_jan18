@isTest
public class CreateFastOpportunityControllerTests {

    @testSetup
    static void setUp(){
		Country_Mapping__c cm = new Country_Mapping__c(Name='United States',Permutations__c='UNITED STATES; USA; US; UNITED STATES OF AMERICA',
                Country_Code__c='US',Country__c='United States',Support_Region__c='AMERICAS',Default_Service_Price_Book__c=Test.getStandardPricebookId());
        insert cm;
        
        State_Mapping__c sm = new State_Mapping__c(Name = 'Massachusetts', Country_Mapping__c = cm.Id, State_Province_Permutations__c = 'MA; MASSACHUSETTS');
        insert sm;
        
		Product2[] testProducts = new Product2[]{
			new Product2(ProductCode = 'TRNLP004', IsActive = true, Name = 'Test Training Product')
		};
		insert testProducts;
        
        insert new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(), Product2Id = testProducts.get(0).Id, UnitPrice = 10000, IsActive = true, CurrencyIsoCode='USD');

        Account[] accounts = new Account[]{
        	new Account(Name = 'Test Account', BillingCountry = 'United States'),    
        	new Account(Name = 'Test Account', BillingCountry = 'United States', BillingState = 'MA')    
        };
        insert accounts;
        
        Asset[] assets = new Asset[]{
            new Asset(Name = 'test asset', AccountId = accounts[0].Id, SerialNumber = '123456'),
            new Asset(Name = 'test asset', AccountId = accounts[1].Id, SerialNumber = '123457')   
		};
        insert assets;
        
        Contact[] contacts = new Contact[]{
            new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = accounts[0].Id),
            new Contact(FirstName = 'Test', LastName = 'MA Contact', AccountId = accounts[1].Id)
		};
        insert contacts;
        
		LMS_Course__c lmsCourse = new LMS_Course__c(Name = 'Test Course 1', LMS_Course_ID__c = AbsorbUtil.generateGUID(), Active__c = true,
                                                   Product_Number__c = 'TRNLP004');
		insert lmsCourse;
        
        LMS_Course_Enrollment__c[] lmsCourseEnrollments = new LMS_Course_Enrollment__c[]{
            new LMS_Course_Enrollment__c(
				LMS_Course__c = lmsCourse.Id, LMS_User__c = contacts.get(0).Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
				Name = 'Test Course Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0, Asset__c = assets[0].Id),
            new LMS_Course_Enrollment__c(
				LMS_Course__c = lmsCourse.Id, LMS_User__c = contacts.get(1).Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
				Name = 'Test MA Enrollment', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0, Asset__c = assets[1].Id),
            new LMS_Course_Enrollment__c(
				LMS_Course__c = lmsCourse.Id, LMS_User__c = contacts.get(0).Id, LMS_Enrollment_ID__c  = AbsorbUtil.generateGUID(), 
				Name = 'Test No Asset', LMS_Status__c = 'NotStarted', LMS_Progress__c = 0)
        };
		insert lmsCourseEnrollments;
    }

    @isTest static void test_success(){
		LMS_Course_Enrollment__c e = [select Id from LMS_Course_Enrollment__c where Name = 'Test Course Enrollment' order by createdDate desc limit 1];
        CreateFastOpportunityController c = new CreateFastOpportunityController(new ApexPages.StandardController(e));
        system.assertNotEquals(null, c.init());
    }

    @isTest static void test_success_MA(){
		LMS_Course_Enrollment__c e = [select Id from LMS_Course_Enrollment__c where Name = 'Test MA Enrollment' order by createdDate desc limit 1];
        CreateFastOpportunityController c = new CreateFastOpportunityController(new ApexPages.StandardController(e));
        system.assertNotEquals(null, c.init());
    }

    @isTest static void test_fail_no_asset(){
		LMS_Course_Enrollment__c e = [select Id from LMS_Course_Enrollment__c where Name = 'Test No Asset' order by createdDate desc limit 1];
        CreateFastOpportunityController c = new CreateFastOpportunityController(new ApexPages.StandardController(e));
        system.assertEquals(null, c.init());
    }
}