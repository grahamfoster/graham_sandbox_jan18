/*
 *	AccountTriggerHandler
 *	
 *		Trigger Handler class that implements Trigger Framework.
 *			https://developer.salesforce.com/page/Trigger_Frameworks_and_Apex_Trigger_Best_Practices
 *			
 *		According to the Trigger Framework, all trigger logic should be in trigger handler class.
 *		The Trigger Handler class should extend TriggerHandler class.
 * 
 * 	Created by Yong Chen on 2016-03-08
 *
 *	[Modification history]
 *	[Name] [Date] Description
 *
 */
public with sharing class AccountTriggerHandler extends TriggerHandler {
    
	public AccountTriggerHandler() {
		//this.setMaxLoopCount(1);
	}
	
	// **************************************************************************
	// 		context overrides 
	// **************************************************************************
	override protected void afterUpdate() {
    	// Outside of the Trigger itself, statics like Trigger.new and Trigger.newMap always contain raw sObjects or Maps of sObjects respectively.
    	OpportunityMarketVerticalTriggerHandler.onAccountUpdate(Trigger.new,(Map<Id, Account>)Trigger.oldMap);

    }
    
    override protected void afterInsert(){

    }
    
    override protected void afterDelete(){        

    }
        
    override protected void beforeInsert(){        
	AccountSetCountryMap();
    }    
    override protected void beforeUpdate(){        
	AccountSetCountryMap();
    }

/*******
 * Created by = Daniel 
 * Purpose of 'AccountSetCountryMap' = to assign Accounts to State and Country mapping records
 * See https://sciexbase.atlassian.net/wiki/display/SFDC/Country+and+State+Mapping
 * *****/    
private void AccountSetCountryMap()
{
        List<Account> AccountNeedMapping = new List<Account>();

	for( Account newAccount : (List<Account>)Trigger.new){
   Account oldAccount = NULL;

   //If update, grab old record for comparison
   if(Trigger.IsUpdate) 
   { 
      oldAccount = ((Map<Id, Account>)Trigger.oldMap).get(newAccount.ID);  
   }
   
   //Add to list only if NEW or Country/State field changes
   if(Trigger.IsInsert || oldAccount.BillingCountry != newAccount.BillingCountry ||  oldAccount.BillingState != newAccount.BillingState)       
   {    
     AccountNeedMapping.add(newAccount);
   }
	}  
    
if(AccountNeedMapping.size()>0)
    {
        List<Country_Mapping__c> allCountries = [Select Name, id,Permutations__c, Country_Code__c, Sales_Region__c, Forecast_Territory__c, 
                                                 	Forecast_Region__c, Forecasting_Country__c, Country_Grouping__c, Regional_Grouping__c
                                                 from Country_Mapping__c];        
        List<State_Mapping__c> allStates = [Select Name, id, State_Province_Permutations__c, Country_Mapping__c 
                                            from State_Mapping__c];         
        Map<String,Country_Mapping__c> countryAlias = new Map<String,Country_Mapping__c>();  
        Map<String,State_Mapping__c> stateAlias = new Map<String,State_Mapping__c>(); 
        
        for(Country_Mapping__c country :allCountries){
            // add country code, we will be treating it the same way as permutations
          countryAlias.put(country.Country_Code__c.toUpperCase(),country);

            // split all permutations
          if(!String.isBlank(country.Permutations__c)){
              if(country.Permutations__c.contains(';')){
                for(String alias : country.Permutations__c.split(';')) {
            if(!String.isBlank(alias.trim())) 
                      countryAlias.put(alias.trim().toUpperCase(),country);
                    }
              } else {
                      countryAlias.put(country.Permutations__c.trim().toUpperCase(),country);
                }
        }
            
      }
        for(State_Mapping__c state :allStates){
                    
                    if(!String.isBlank(state.State_Province_Permutations__c)){
                  
                        if(state.State_Province_Permutations__c.contains(';') ){
                    
                            for(String alias : state.State_Province_Permutations__c.split(';')) {
                                if(!String.isBlank(alias.trim())  )
                                    stateAlias.put(alias.trim().toUpperCase(),state);
                            }
                                } else {
                                    stateAlias.put(state.State_Province_Permutations__c.trim().toUpperCase(),state);
                                }
                            }  
                        }        
    
for(Account Accountrecord : AccountNeedMapping){
        
    ID countryID;
    String countryName;
    ID stateID;
    String stateName; 

    if(Accountrecord.BillingCountry != null){  
                
        IF(countryAlias.containsKey(Accountrecord.BillingCountry.trim().toUpperCase()))
        {
            countryID = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).id;
        }       
                //Country MAP FOUND --> Populate the Country and Country Mapping fields
                if(countryID != NULL)
                {   
                    Accountrecord.Country_Mapping__c = countryID;
                    countryName = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).name;
                
                    if(String.isNotBlank(countryName))
                    {
                        Accountrecord.BillingCountry = countryName;
                        Accountrecord.Sales_Region__c = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).Sales_Region__c;
                        Accountrecord.ENGLISH_Country_Grouping__c = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).Country_Grouping__c;
                        Accountrecord.ENGLISH_Forecast_Country__c = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).Forecasting_Country__c;
                        Accountrecord.ENGLISH_Forecast_Region__c = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).Forecast_Region__c;
                        Accountrecord.ENGLISH_Forecast_Territory__c = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).Forecast_Territory__c;
                        Accountrecord.ENGLISH_Regional_Grouping__c = countryAlias.get(Accountrecord.BillingCountry.trim().toUpperCase()).Regional_Grouping__c;
                    }
                } 
                
                // Country MAP NOT FOUND --> NULL out the Country and Country Mapping fields
                if(countryID == NULL)
                { 
                    Accountrecord.Country_Mapping__c = NULL;
                    //Accountrecord.BillingCountry = NULL;
                    Accountrecord.Sales_Region__c = NULL;
                    Accountrecord.ENGLISH_Country_Grouping__c = NULL;
                    Accountrecord.ENGLISH_Forecast_Country__c = NULL;
                    Accountrecord.ENGLISH_Forecast_Region__c = NULL;
                    Accountrecord.ENGLISH_Forecast_Territory__c = NULL;
                    Accountrecord.ENGLISH_Regional_Grouping__c = NULL;
                }
                
            }

    if(countryID != null && Accountrecord.BillingState != null)   {  
    
        IF(stateAlias.containsKey(Accountrecord.BillingState.trim().toUpperCase()))
        {
            stateID = stateAlias.get(Accountrecord.BillingState.trim().toUpperCase()).id;
        }         
        
        if(stateID != NULL && countryID == stateAlias.get(Accountrecord.BillingState.trim().toUpperCase()).Country_Mapping__c)
            {   
                Accountrecord.State_Mapping__c = stateID;
                stateName = stateAlias.get(Accountrecord.BillingState.trim().toUpperCase()).name;
                  
                if(String.isNotBlank(stateName))
                {
                    Accountrecord.BillingState = stateName;
                }
            }
    }     

    // Country BLANK --> NULL out the Country Mapping fields
    if(Accountrecord.BillingCountry == null){  
        Accountrecord.Country_Mapping__c = NULL;
		Accountrecord.Sales_Region__c = NULL;
        Accountrecord.ENGLISH_Country_Grouping__c = NULL;
        Accountrecord.ENGLISH_Forecast_Country__c = NULL;
        Accountrecord.ENGLISH_Forecast_Region__c = NULL;
        Accountrecord.ENGLISH_Forecast_Territory__c = NULL;
        Accountrecord.ENGLISH_Regional_Grouping__c = NULL;
    }
            
    // State BLANK --> NULL out the State Mapping fields
    if(Accountrecord.BillingState == null  || String.isBlank(stateName)){  
        Accountrecord.State_Mapping__c = NULL;
    }
    } 
  }
    }//END OF AccountSetCountryMap    
    
	
}