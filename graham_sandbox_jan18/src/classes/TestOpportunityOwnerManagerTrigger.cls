/*
 *  Class to unit test the Asset trigger updateAssetTrigger.trigger.
 *
 *  There are two scenarios to test:
 *  1. When an asset account is changed, unrelate the department from the asset and delete all asset_multiplecontacts__c 
 *  2. When an asset department is changed delete all asset_multiplecontacts__c that are related to the old department. Confirm that contacts remain if not associated to old dept.
 */ 
public class TestOpportunityOwnerManagerTrigger {

    //create asset, account, department, asset_multiplecontacts__c, contacts records
    
    static testMethod void testUpdateOwner() {
     //User owner1 = new User( firstname = 'owner',lastname = 'one');
     //insert owner1;
       Contact CC = new Contact();   
       CC.LastName = 'sdfsdfs';
       insert CC;        
     Opportunity o = new Opportunity( name = 'Hello',StageName='tst',CloseDate=date.newinstance(1960, 2, 17),Primary_Contact__c = CC.id);
     insert o;
     update o;
     User u = new User(
                       alias = 'stUser', 
                       email='ADFfmpsfm@asdadasdad.com',
                       emailencodingkey='UTF-8',
                       firstname='TestUser15',
                       lastname='TestUser15',
                       languagelocalekey='en_US',
                       localesidkey='en_US', 
                       profileid = '00eA0000000Vgh9',
    timezonesidkey='America/Los_Angeles',
    username='TestUser15@trmp.com'
     );
    test.startTest();
    insert u;
    test.stopTest();
    }
        
 
}