@isTest
public with sharing class Application_Unittest {
	Static TestMethod void Application_Unittest1()
	{
		Application__c appRecObj = Util.createApplication();
		
		ApexPages.StandardController controller=new ApexPages.StandardController(appRecObj);
		ApexPages.currentPage().getParameters().put('DataTab','1');
        Application_Ctrl appObj = new Application_Ctrl(controller); 
        appObj.doSave();
        appObj.doSaveNew();
        appObj.doCancel();
        appObj.doDelete();
        appObj.redirectadmin();
	    
        Application__c App = new Application__c();
        insert App;
        
        ApexPages.StandardController controller1=new ApexPages.StandardController(App);
        ApexPages.currentPage().getParameters().put('DataTab','1');
        Application_Ctrl appObj1 = new Application_Ctrl(controller1);
        appObj1.doSave();
        
    }
	
	Static TestMethod void Application_Unittest3()
	{
		Application__c appRecObj = Util.createApplication();
		
		Attachment att = new Attachment(Name = 'Test',Body = blob.valueOf('test1'),ParentId = appRecObj.Id);
		insert att; 
		
		ApexPages.StandardController controller=new ApexPages.StandardController(appRecObj);		
        Application_Ctrl appObj = new Application_Ctrl(controller); 
        appObj.file1 = blob.valueOf('It is a test');
        appObj.contentype = 'image/jpg';
        appObj.attachid= att.Id;
        appObj.doSave();
        appObj.doSaveNew();
        ApexPages.currentPage().getParameters().put('retURL',appRecObj.id);
        appObj.doCancel();
        appObj.doDelete();
	}
	
	
}