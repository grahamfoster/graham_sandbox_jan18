@isTest
public class TestCaseUpdateTrigger2 {

    static void setUp() {

    }
    
    static testMethod void testCaseUpdateTrigger(){  

        // Get CIC Case record type id
        Id cicRecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('CIC Case').getRecordTypeId();

        //Get an existing contact        
        Contact c1= [Select Id, accountId From Contact  where accountid <> null limit 1];
       

        // Create a product 
        Product2 p1 = new Product2(Name='dna analyzer');
        insert p1;

        // Create an asset and linke to product and account
        Asset a1 = new Asset(Name='dna analyzer 12432134', Product2Id = p1.Id, AccountId=c1.accountId);
        insert a1;
        
        Case[] cases = new Case[]{
            new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId ,  AssetId=a1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine'),
            new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId , AssetId=a1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine'),
            new Case(  ContactId=c1.Id,RecordTypeId=cicRecordTypeId , Product__c=p1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine'),
            new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId , Non_AB_Product__c=true, Case_Type__c='Troubleshoot', Subject='Reagent is too thick', Description='How do it make it less viscous?')          
        };                 
        // Insert: Case with Asset 
//        Case c1a = new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId ,  AssetId=a1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
        
//        insert c1a;   
                        
        // Insert: Case with Asset and Active Contract
//        Case c1b = new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId , AssetId=a1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
        
//        insert c1b;   


        // Insert: Case with Product
//        Case c2 = new Case(  ContactId=c1.Id,RecordTypeId=cicRecordTypeId , Product__c=p1.Id, Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine');
        
 //       insert c2;                   

        // Insert: Case with non-AB Product
//        Case c3 = new Case(  ContactId=c1.Id, RecordTypeId=cicRecordTypeId , Non_AB_Product__c=true, Case_Type__c='Troubleshoot', Subject='Reagent is too thick', Description='How do it make it less viscous?');
        
//        insert c3; 


        // Get non-Case record type id
        //RecordType[] fas_record_types = [select id from RecordType where name = 'FAS' and sObjectType = 'Case' and IsActive = TRUE];
        //Id fasRecordTypeId = fas_record_types.size() > 0 ? fas_record_types[0].Id : null;
        //Id fasRecordTypeId = Case.SObjectType.getDescribe().getRecordTypeInfosByName().get('FAS').getRecordTypeId();

            // Insert: non-CIC Case
        //cases.add( new Case(  ContactId=c1.Id, RecordTypeId=fasRecordTypeId , Case_Type__c='Troubleshoot', Subject='There is a warning light', Description='Unable to find problem with machine') );
        
        insert cases;

       // Update: Case with Asset 
        cases[0].Description=cases[0].Description+' problem disappeared.';       

        
       // Update: Case with Asset and Active Contract
        cases[1].Description=cases[1].Description+' problem disappeared.';       
        //update c1b;
                
       // Update: Case with Product 
        cases[2].Description=cases[2].Description+' problem disappeared.';           
        //update c2;
       
        // Insert: Case with non-AB Product
        cases[3].Description=cases[3].Description+' problem disappeared.';
        update cases;       

    }
}