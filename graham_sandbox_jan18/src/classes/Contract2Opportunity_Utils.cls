public virtual class Contract2Opportunity_Utils {

    public Set<String> ceProducts;
	public Map<String, Map <String,c2oWarranty__c>> warrantyMap;
    
    public boolean validOpp(Opportunity o){
        if( string.isNotBlank(o.RecordTypeId) && string.isNotBlank(o.AccountId)
          && string.isNotBlank(o.Pricebook2Id) && string.isNotBlank(o.CurrencyIsoCode)
          && string.isNotBlank(o.Previous_Contract__c)){
            return TRUE; 
         } else {
			System.debug('** Invalid Opportunity Data : ' + o);
            return FALSE;  
         }
    }   
  
    public boolean validLine(OpportunityLineItem ol){    
		 if( string.isNotBlank(ol.OpportunityId) && string.isNotBlank(ol.PricebookEntryId)){
            return TRUE; 
         } else {
            return FALSE;  
        }
    }     
    
    public Map<String, c2oSettings__c> customSettings(){    //   Query all custom settings, and populate them as a Map <Contract_Type , Object>
        List<c2oSettings__c> allsettings = new List<c2oSettings__c>([SELECT Id, Name, Contract_Type__c, Naming_Convention__C, Product_Type__c, RecordType__c, Stage_Name__c FROM c2oSettings__c WHERE isDeleted = FALSE ]);  
        Map<String, c2oSettings__c> settings = new Map<String, c2oSettings__c>();
        for (c2oSettings__c i :allsettings){
            settings.put(i.Contract_Type__c,i);
        }
        return settings;
    }   
    
    public Contract2Opportunity_Configuration__c c2oConfiguration(){    //   Query all Logger settings, and populate them as a Map <Name , Object>
        List<Contract2Opportunity_Configuration__c> config = new List<Contract2Opportunity_Configuration__c>([SELECT Id, Name, Log_Level__c FROM Contract2Opportunity_Configuration__c WHERE isDeleted = FALSE  LIMIT 1]);  

        return config[0];
    } 
    
    public Set<String> ceSettings(){    //   Query all CE products, and populate them in a Set
        List<c2oExceptions__c> allExceptions = new List<c2oExceptions__c>([SELECT Id, Name, Exception_Type__c, SalesRepID__c FROM c2oExceptions__c WHERE isDeleted = FALSE ]);  
		Set<String> ceP = new Set<String>();
       	for(c2oExceptions__c i :allExceptions){
            if(i.Exception_Type__c == 'CE'){
              ceP.add(i.name);
            }
        }
      	return ceP;
    }  
    
    public boolean isCE(String prod){
        If(ceProducts.contains(prod)){
            return TRUE;
        } else {
        	return FALSE;
        }
    }  
    
    public String warrantyLookup(String Plan, String Region, String Product){
        if(warrantyMap.containsKey(Region)){
            if(warrantyMap.get(Region).containsKey(Plan)){
                if(isCE(Product)){
        			return  warrantyMap.get(Region).get(Plan).Default_CE_Plan__c + '|' + Product; 
				}  else {
 		           return  warrantyMap.get(Region).get(Plan).Default_Plan__c + '|' + Product; 
				}
            } else {
                if(isCE(Product)){
        			return  warrantyMap.get(Region).get(NULL).Default_CE_Plan__c + '|' + Product; 
				}  else {
 		           return  warrantyMap.get(Region).get(NULL).Default_Plan__c + '|' + Product; 
				}                
            }
        } else {
            if(warrantyMap.get(NULL).containsKey(Plan)){
                if(isCE(Product)){
        			return  warrantyMap.get(NULL).get(Plan).Default_CE_Plan__c + '|' + Product; 
				}  else {
 		           return  warrantyMap.get(NULL).get(Plan).Default_Plan__c + '|' + Product; 
				}
            } else {
                if(isCE(Product)){
        			return  warrantyMap.get(NULL).get(NULL).Default_CE_Plan__c + '|' + Product; 
				}  else {
 		            return  warrantyMap.get(NULL).get(NULL).Default_Plan__c + '|' + Product; 
				}                
            }    
        }
    }        
    
    public Map<String, Map <String,c2oWarranty__c>>  wLookups(){    //   Query all Warranty Lookups
        List<c2oWarranty__c> warrantyList = new List<c2oWarranty__c>([SELECT Id, Warranty__c, Region__c, Default_Plan__c, Default_CE_Plan__c  FROM c2oWarranty__c WHERE isDeleted = FALSE ]);  
		Map<String, Map <String,c2oWarranty__c>> mapWarranty = new Map<String, Map <String,c2oWarranty__c>>();
        Map<String, c2oWarranty__c> mapByPlan = new Map<String, c2oWarranty__c>();
        for (c2oWarranty__c i :warrantyList){
            if(!mapWarranty.containsKey(i.Region__c)){
                mapWarranty.put(i.Region__c, new Map <String,c2oWarranty__c>{i.Warranty__c => i} );
            } else {
                mapWarranty.get(i.Region__c).put(i.Warranty__c,i);
            }
        }   
        return mapWarranty;
    }  
    
}